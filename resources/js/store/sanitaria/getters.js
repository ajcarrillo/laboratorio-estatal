export default {
    countOrigenes: state => {
        return state.origenes.length
    },
    getOrigenesByTipo: state => id => {
        return state.origenes.filter(function (el) {
            return el.tipo === id
        })
    }
}
