export default {
    fetchOrigenes({commit}, payload) {
        commit('SET_ORIGENES', payload)
    },
    agregaMuestra({commit}, payload) {
        commit('AGREGAR_MUESTRA', payload)
    },
    fetchEstudios({commit}, payload) {
        commit('SET_ESTUDIOS', payload)
    },
    fetchPaquetes({commit}, payload) {
        commit('SET_PAQUETES', payload)
    },
    fetchCategorias({commit}, payload) {
        commit('SET_CATEGORIAS', payload)
    },
    fetchRecipientes({commit}, payload) {
        commit('SET_RECIPIENTES', payload)
    },
    fetchSubcategorias({commit}, payload) {
        commit('SET_SUBCATEGORIAS', payload)
    },
    fetchCapturistasSanitaria({commit}, payload) {
        commit('SET_CAPTURISTASSANITARIA', payload)
    },
    fetchTiposMuestreo({commit}, payload) {
        commit('SET_TIPOS_MUESTREO', payload)
    },
    setPantallaRecepcion({commit}, payload) {
        commit('SET_PANTALLA_RECEPCION', payload)
    },
    updateOrCreateSolicitud({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.post(route('api.v1.solicitudes.store'), payload)
                .then(res => {
                    commit('SET_SOLICITUD', res.data.solicitud)
                    resolve(res.data.solicitud);
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                })
        })
    },
    setPaqueteSelected({commit}, payload) {
        commit('SET_PAQUETE_SELECTED', payload);
    },
    fetchSolicitudes({commit}, payload) {
        return new Promise((resolve, reject) => {
            let {search, from, to, page} = payload;
            let params = {search, from, to, page}
            axios.get(route('api.v1.solicitudes.index'), {
                params
            })
                .then(res => {
                    commit('SET_SOLICITUDES', res.data)
                    resolve(res);
                })
                .catch(err => {
                    console.log(err)
                    reject(err);
                })
        });
    },
    setSolicitud({commit}, payload) {
        commit('SET_SOLICITUD', payload);
    },
    showSolicitud({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.get(route('api.v1.solicitudes.show', payload))
                .then(res => {
                    commit('SET_SOLICITUD', res.data.solicitud)
                    resolve(res)
                })
                .catch(reason => {
                    console.log(reason)
                    reject(reason)
                })
        })
    },
    setEstudiosMuestra({commit}, payload) {
        commit('SET_ESUTDIOS_MUESTRA', payload)
    },
    updateOrCreateMuestra({commit}, payload) {
        let {solicitud, data} = payload
        return new Promise((resolve, reject) => {
            axios.post(route('api.v1.solicitudes.muestras-sanitarias.store', solicitud), data)
                .then(res => {
                    commit('PUSH_SOLICITUD_MUESTRAS', res.data.muestra)
                    resolve(res.data.muestra);
                })
                .catch(reason => {
                    console.log(reason)
                    reject(reason)
                })
        })
    },
    terminarFolio({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.post(route('api.v1.terminar.folio', payload))
                .then(res => {
                    commit('TERMINAR_FOLIO', payload)
                    resolve(res)
                })
                .catch(reason => {
                    console.log(reason)
                    reject(reason)
                })
        })
    },
    async storeOrigen({commit}, payload) {
        let result = await axios.post(route('api.v1.origenes.store'), payload)
        commit('PUSH_ORIGEN', result.data.item)

        return result
    }
}
