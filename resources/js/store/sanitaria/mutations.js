export default {
    SET_ORIGENES(state, payload) {
        state.origenes = payload
    },
    AGREGAR_MUESTRA(state, payload) {
        state.muestras.push(payload)
    },
    SET_ESTUDIOS(state, payload) {
        state.estudios = payload
    },
    SET_PAQUETES(state, payload) {
        state.paquetes = payload
    },
    SET_CATEGORIAS(state, payload) {
        state.categorias = payload
    },
    SET_RECIPIENTES(state, payload) {
        state.recipientes = payload
    },
    SET_SUBCATEGORIAS(state, payload) {
        state.subcategorias = payload
    },
    SET_CAPTURISTASSANITARIA(state, payload) {
        state.capturistas_sanitaria = payload
    },
    SET_TIPOS_MUESTREO(state, payload) {
        state.tipos_muestreo = payload
    },
    SET_PANTALLA_RECEPCION(state, payload) {
        state.pantallaSelected = payload
    },
    SET_PAQUETE_SELECTED(state, payload) {
        state.paqueteSelected = payload
    },
    SET_SOLICITUDES(state, payload) {
        state.solicitudes = payload.items
    },
    SET_SOLICITUD(state, payload) {
        state.solicitud = payload
    },
    SET_ESUTDIOS_MUESTRA(state, payload) {
        state.estudios_a_la_muestra = payload
    },
    PUSH_SOLICITUD_MUESTRAS(state, payload) {
        let index = state.solicitud.muestras.findIndex(function (el) {
            return el.numero_muestra === payload.numero_muestra
        })

        if (index >= 0) {
            Vue.set(state.solicitud.muestras, index, payload)
        } else {
            state.solicitud.muestras.push(payload)
        }
    },
    TERMINAR_FOLIO(state, payload) {
        state.solicitud.terminado = true
    },
    PUSH_ORIGEN(state, payload) {
        state.origenes.push(payload)
    }
}
