export default {
    SET_ENTRADA_ARTICULOS(state, payload) {
        state.entrada_articulos = payload
    },
    SET_SOLICITUD_ARTICULOS(state, payload) {
        state.solicitud_articulos = payload
    },
    SET_TRANSFERENCIA_ARTICULOS(state, payload) {
        state.transferencia_articulos = payload
    },
    SET_TRANSFERENCIA_ORIGEN_ID(state, payload) {
        state.transferencia_origen_id = payload
    },
    SET_ARTICULOS(state, payload) {
        state.articulos = payload
    },
}
