export default {
    getEntradaArticulos: state => {
        return state.entrada_articulos
    },
    getSolicitudArticulos: state => {
        return state.solicitud_articulos
    },
    getTransferenciaArticulos: state => {
        return state.transferencia_articulos
    },
    getTransferenciaOrigenId: state => {
        return state.transferencia_origen_id
    },
    getArticulos: state => {
        return state.articulos
    },
}
