export default {
    fetchEntradaArticulos({commit}, payload) {
        commit('SET_ENTRADA_ARTICULOS', payload)
    },
    fetchSolicitudArticulos({commit}, payload) {
        commit('SET_SOLICITUD_ARTICULOS', payload)
    },
    fetchTransferenciaArticulos({commit}, payload) {
        commit('SET_TRANSFERENCIA_ARTICULOS', payload)
    },
    fetchArticulos({commit}, payload) {
        commit('SET_ARTICULOS', payload)
    },
    fetchTransferenciaOrigenId({commit}, payload) {
        commit('SET_TRANSFERENCIA_ORIGEN_ID', payload)
    },
}
