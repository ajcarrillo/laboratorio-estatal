export default {
    getMenu: state => {
        return state.menu
    },
    getAplication: state => {
        return state.aplication
    },
    getAplications: state => {
        return state.aplications
    },
    getLoading: state => {
        return state.loading
    },
}
