export default {
    fetchMenu({commit}, payload) {
        commit('SET_MENU', payload)
    },
    fetchAplications({commit}, payload) {
        commit('SET_APLICATIONS', payload)
    },
    fetchAplication({commit}, payload) {
        commit('SET_APLICATION', payload)
    },
    setSnackBar({commit}, payload) {
        commit('SET_SNACK_BAR', payload)
    },
    setLoading({commit}, payload) {
        commit('SET_LOADING', payload)
    }
}
