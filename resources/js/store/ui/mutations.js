export default {
    SET_MENU(state, payload) {
        state.menu = payload
    },
    SET_APLICATION(state, payload) {
        state.aplication = payload
    },
    SET_SNACK_BAR(state, payload) {
        state.snackBar = payload
    },
    SET_APLICATIONS(state, payload) {
        state.aplications = payload
    },
    SET_LOADING(state, payload) {
        state.loading = payload
    },
}
