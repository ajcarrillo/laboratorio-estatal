import Vue from 'vue'
import Vuex from 'vuex'

import Auth from './auth/store'
import Catalogos from './catalogos/store'
import Sanitaria from './sanitaria/store'
import Inventarios from './inventarios/store';
import UI from './ui/store';
import Administracion from './administracion/store'
import Calidad from './calidad/store'

Vue.use(Vuex)

const vx = new Vuex.Store({
    modules: {
        auth: Auth,
        ui: UI,
        catalogos: Catalogos,
        sanitaria: Sanitaria,
        inventarios: Inventarios,
        administracion: Administracion,
        calidad: Calidad
    }
})

export default vx;
