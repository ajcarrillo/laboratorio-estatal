export const SET_USUARIOS = (state, payload) => {
    state.usuarios = payload
}

export const PUSH_USUARIO = (state, payload) => {
    state.usuarios.push(payload);
}

export const UPDATE_USUARIO = (state, payload) => {
    let index = state.usuarios.findIndex(function (el) {
        return el.id === payload.id
    })

    Vue.set(state.usuarios, index, payload)
}
