export const fetchUsuarios = async ({commit}, payload) => {
    let res = await axios.get(route('api.v1.usuarios.index'))
    commit('SET_USUARIOS', res.data.usuarios)
}

export const storeUsuario = async ({commit}, payload) => {
    let res = await axios.post(route('api.v1.usuarios.store'), payload)
    commit('PUSH_USUARIO', res.data.user)
}

export const updateUsuario = async ({commit}, payload) => {
    let {id, data} = payload
    let res = await axios.patch(route('api.v1.usuarios.update', id), data)
    commit('UPDATE_USUARIO', res.data.user)
}
