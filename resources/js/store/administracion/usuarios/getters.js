export const getUsuarioById = state => id => {
    let item = state.usuarios.find(function (el) {
        return el.id === id
    })

    item.roles = item.roles.map(function (el) {
        return el.name
    })

    item.permissions = item.permissions.map(function (el) {
        return el.name
    })

    return item;
}
