import {fetchSistemas, storeSistema, syncRoles, updateSistema} from './sistemas/actions'
import {fetchUsuarios, storeUsuario, updateUsuario} from './usuarios/actions'
import {deleteMenu, deleteSubMenu, fetchMenus, storeMenu, storeSubMenu, updateMenu, updateSubMenu} from './menu/actions'

export default {
    fetchSistemas,
    storeSistema,
    updateSistema,
    fetchUsuarios,
    storeUsuario,
    updateUsuario,
    fetchMenus,
    storeMenu,
    updateMenu,
    storeSubMenu,
    updateSubMenu,
    deleteMenu,
    deleteSubMenu,
    syncRoles
}
