export const fetchMenus = async ({commit}, payload) => {
    let res = await axios.get(route('api.v1.menu.index'))
    commit('SET_MENUS', res.data.menus);
}

export const storeMenu = async ({commit}, payload) => {
    let res = await axios.post(route('api.v1.menu.store'), payload)
    commit('PUSH_MENU', res.data.menu)
}

export const updateMenu = async ({commit}, payload) => {
    let {id, data} = payload
    let res = await axios.patch(route('api.v1.menu.update', id), data)
    commit('UPDATE_MENU', res.data.menu)
}

export const storeSubMenu = async ({commit}, payload) => {
    let res = await axios.post(route('api.v1.menu.store'), payload)
    commit('PUSH_SUBMENU', res.data.menu)
}

export const updateSubMenu = async ({commit}, payload) => {
    let {id, data} = payload
    let res = await axios.patch(route('api.v1.menu.update', id), data)
    commit('UPDATE_SUBMENU', res.data.menu)
}

export const deleteMenu = async ({commit}, payload) => {
    let res = await axios.delete(route('api.v1.menu.destroy', payload.id))
    commit('REMOVE_MENU', payload)
}

export const deleteSubMenu = async ({commit}, payload) => {
    let res = await axios.delete(route('api.v1.menu.destroy', payload.id))
    commit('REMOVE_SUBMENU', payload)
}
