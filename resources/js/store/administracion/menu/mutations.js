export const SET_MENUS = (state, payload) => {
    state.menus = payload
}

export const PUSH_MENU = (state, payload) => {
    state.menus.push(payload)
}

export const UPDATE_MENU = (state, payload) => {
    let index = state.menus.findIndex(function (el) {
        return el.id === payload.id
    })

    Vue.set(state.menus, index, payload)
}

export const PUSH_SUBMENU = (state, payload) => {
    let index = state.menus.findIndex(function (el) {
        return el.id === payload.parent_id
    })
    state.menus[index].children_menu.push(payload)
}

export const UPDATE_SUBMENU = (state, payload) => {
    let indexMenu = state.menus.findIndex(function (el) {
        return el.id === payload.parent_id
    })

    let index = state.menus[indexMenu].children_menu.findIndex(function (el) {
        return el.id === payload.id
    })

    Vue.set(state.menus[indexMenu].children_menu, index, payload)
}

export const REMOVE_MENU = (state, payload) => {
    let index = state.menus.findIndex(function (el) {
        return el.id === payload.id
    })

    Vue.delete(state.menus, index)
}

export const REMOVE_SUBMENU = (state, payload) => {
    let indexMenu = state.menus.findIndex(function (el) {
        return el.id === payload.parent_id
    })

    let index = state.menus[indexMenu].children_menu.findIndex(function (el) {
        return el.id === payload.id
    })

    Vue.delete(state.menus[indexMenu].children_menu, index)
}

