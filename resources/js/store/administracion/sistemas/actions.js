export const fetchSistemas = async ({commit}, payload) => {
    let result = await axios.get(route('api.v1.sistemas.index'))
    commit('SET_SISTEMAS', result.data.sistemas)
}

export const storeSistema = async ({commit}, payload) => {
    let res = await axios.post(route('api.v1.sistemas.store'), payload)
    commit('PUSH_SISTEMA', res.data.sistema)
}

export const updateSistema = async ({commit}, payload) => {
    let {id, data} = payload
    let res = await axios.patch(route('api.v1.sistemas.update', id), data)
    commit('UPDATE_SISTEMA', res.data.sistema)
}

export const syncRoles = async ({commit}, payload) => {
    let {sistema, roles} = payload

    let res = await axios.post(route('api.v1.sistemas.roles', sistema.id), {roles})

    commit('SYNC_ROLES', {id: sistema.id, roles: res.data.roles})
}
