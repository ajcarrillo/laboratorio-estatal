export const SET_SISTEMAS = (state, payload) => {
    state.sistemas = payload
}
export const SET_SISTEMA = (state, payload) => {
}

export const PUSH_SISTEMA = (state, payload) => {
    state.sistemas.push(payload)
}

export const UPDATE_SISTEMA = (state, payload) => {
    let index = state.sistemas.findIndex(function (el) {
        return el.id === payload.id
    })

    Vue.set(state.sistemas, index, payload)
}

export const SYNC_ROLES = (state, payload) => {
    let {id, roles} = payload

    let index = state.sistemas.findIndex(function (el) {
        return el.id === id
    })

    Vue.set(state.sistemas[index], 'roles', roles)
}
