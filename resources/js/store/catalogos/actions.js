export default {
    fetchMunicipios({commit}, payload) {
        return new Promise((resolve, reject) => {
            axios.get(route('api.v1.catalogos.municipios'), {
                params: {
                    cve_ent: payload
                }
            })
                .then(res => {
                    commit('SET_MUNICIPIOS', res.data.items);
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        })
    },
    fetchLocalidades({commit}, payload) {
        return new Promise((resolve, reject) => {
            let {cve_ent, cve_mun} = payload
            axios.get(route('api.v1.catalogos.localidades'), {
                params: {
                    cve_ent,
                    cve_mun
                }
            })
                .then(res => {
                    commit('SET_LOCALIDADES', res.data.items);
                    resolve(res);
                })
                .catch(err => {
                    console.log(err);
                    reject(err);
                })
        })
    },
    fetchPacientes({commit}, payload) {
        return new Promise((resolve, reject) => {
            let {nombreapellidos} = payload

            axios.get(route('api.v1.pacientes.index'), {
                params: {nombreapellidos}
            })
                .then(res => {
                    commit('SET_PACIENTES', res.data.items)
                    resolve(res)
                })
                .catch(err => {
                    console.log(err)
                    reject(err)
                })
        })
    },
    async fetchRoles({commit}, payload) {
        let res = await axios.get(route('api.v1.catalogos.roles'));
        commit('SET_ROLES', res.data.roles);
    },
    async fetchPermissions({commit}, payload) {
        let res = await axios.get(route('api.v1.catalogos.permissions'));
        commit('SET_PERMISSIONS', res.data.permissions);
    },
    async fetchOrigenes({commit}, payload) {
        let result = await axios.get(route('api.v1.origenes.index'))
        commit('SET_ORIGENES', result.data.items)
    },
    async getPacientes({commit}, payload) {
        let result = await axios.get(route('api.v1.pacientes.index'))
        commit('SET_PACIENTES', result.data.items)
    },
    async storePaciente({commit}, payload) {
        let result = await axios.post(route('api.v1.pacientes.store'), payload)
        commit('ADD_PACIENTE', result.data.item)
        return result
    },
    async updateStore({commit}, payload) {
        let result = await axios.patch(route('api.v1.pacientes.update', payload.id), payload)
        commit('UPDATE_PACIENTE', result.data.item)
    },
    async storeOrigen({commit}, payload) {
        let result = await axios.post(route('api.v1.origenes.store'), payload)
        commit('ADD_ORIGEN', result.data.item)

        return result
    },
    async fetchEstudios({commit}, payload) {
        let result = await axios.get(route('api.v1.catalogos.estudios.index'))

        commit('SET_ESTUDIOS', result.data.items)
    },
    async storeEstudios({commit}, payload) {
        let result = await axios.post(route('api.v1.catalogos.estudios.store'), payload)
        let estudio = result.data.estudio

        commit('ADD_ESTUDIO', estudio)

        return estudio
    },
    async updateEstudio({commit}, payload) {
        let result = await axios.patch(route('api.v1.catalogos.estudios.update', payload.id), payload)
        let estudio = result.data.estudio

        commit('UPDATE_ESTUDIO', estudio)
    }
}
