export default {
    getOrigenById: state => id => {
        return state.origenes.find(function (el) {
            return el.id === id
        })
    },
    countOrigenes: state => {
        return state.origenes.length
    },
    countPacientes: state => {
        return state.pacientes.length
    },
    getPacienteById: state => id => {
        return state.pacientes.find(function (el) {
            return el.id === id
        })
    },
    countEstudios: state => {
        return state.estudios.length
    },
    getEstudioById: state => id => {
        return state.estudios.find(function (el) {
            return el.id === id
        })
    }
}
