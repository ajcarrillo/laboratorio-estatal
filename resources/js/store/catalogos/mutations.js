export default {
    SET_MUNICIPIOS(state, payload) {
        state.municipios = payload
    },
    SET_LOCALIDADES(state, payload) {
        state.localidades = payload
    },
    SET_PACIENTES(state, payload) {
        state.pacientes = payload
    },
    SET_ROLES(state, payload) {
        state.roles = payload
    },
    SET_PERMISSIONS(state, payload) {
        state.permissions = payload
    },
    SET_ORIGENES(state, payload) {
        state.origenes = payload
    },
    ADD_PACIENTE(state, payload) {
        state.pacientes.push(payload)
    },
    UPDATE_PACIENTE(state, payload) {
        let index = state.pacientes.findIndex(function (el) {
            return el.id === payload.id
        })

        Vue.set(state.pacientes, index, payload)
    },
    ADD_ORIGEN(state, payload) {
        state.origenes.push(payload)
    },
    SET_ESTUDIOS(state, payload) {
        state.estudios = payload
    },
    SET_LABORATORIOS(state, payload) {
        state.laboratorios = payload
    },
    ADD_ESTUDIO(state, payload) {
        state.estudios.push(payload)
    },
    UPDATE_ESTUDIO(state, payload) {
        let index = state.estudios.findIndex(function (el) {
            return el.id === payload.id
        })

        Vue.set(state.estudios, index, payload)
    }
}
