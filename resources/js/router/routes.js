import Areas from '../views/Inventarios/Almacen/Areas'
import CatArticulos from '../views/Inventarios/Almacen/CatArticulos'
import DashboardInventarios from '../views/Inventarios/Dashboard'
import Entradas from '../views/Inventarios/Almacen/Entradas'
import Existencias from '../views/Inventarios/Almacen/Existencias'
import Marcas from '../views/Inventarios/Almacen/Marcas'
import Proveedores from '../views/Inventarios/Almacen/Proveedores'
import TiposInsumo from '../views/Inventarios/Almacen/TiposInsumo'
import Transferencias from '../views/Inventarios/Almacen/Transferencias'
import Laboratorio from '../views/Epidemiologia/Lab/Home';
import Analistas from '../views/Epidemiologia/Analistas/List';
import AnalistasTrabajo from '../views/Epidemiologia/Analistas/ListTrabajo';
import addLaboratorio from '../views/Epidemiologia/Lab/New';
import MuestrasLab from '../views/Epidemiologia/Muestras/List';
import ResponsableLab from '../views/Epidemiologia/ResponsableLab/List';
import JefeLab from '../views/Epidemiologia/JefeLab/List';
import Diagnosticos from '../views/Epidemiologia/Diagnosticos/List';
import ListMetodosReferencia from '../views/Epidemiologia/MetodosReferencia/List';
import ListTipoMuestra from '../views/Epidemiologia/TipoMuestra/List';
import Solicitudes from '../views/Inventarios/Almacen/Solicitudes'
import OrdenesCompra from '../views/Inventarios/Almacen/OrdenesCompra'
import Remisiones from '../views/Inventarios/Almacen/Remisiones'
import Descargas from '../views/Inventarios/Almacen/Descargas'
import Abastecimientos from '../views/Inventarios/Almacen/TransferenciasSolicitudes'
import DashboardCalidad from '../views/Calidad/Dashboard'
import Registros from '../views/Calidad/Registros/Registros'
import Documentos from '../views/Calidad/Documentos/Documentos'
import NoConformidades from '../views/Calidad/NoConformidades/NoConformidades'

const NotFound = () => import('../components/NotFound')
const Forbidden = () => import('../components/Forbidden')
const Home = () => import('../views/Home')
const SolicitudesIndex = () => import('../views/sanitaria/Index')
const SolicitudesCreate = () => import('../views/sanitaria/Create')
const SolicitudesEdit = () => import('../views/sanitaria/Edit')
const UsuariosIndex = () => import('../views/administracion/usuarios/Index')
const UsuariosCreate = () => import('../views/administracion/usuarios/Create')
const UsuariosEdit = () => import('../views/administracion/usuarios/Edit')
const ModulosIndex = () => import('../views/administracion/modulos/Index')
const NumeroMuestrasIndex = () => import('../views/administracion/numero_muestras/Index')
const MenusIndex = () => import('../views/administracion/menus/Index')
const MuestrasIndex = () => import('../views/sanitaria/muestras/Index')
const OrigenesIndex = () => import('../views/sanitaria/catalogos/origenes/Index')
const OrigenesEdit = () => import('../views/sanitaria/catalogos/origenes/Edit')
const OrigenesCreate = () => import('../views/sanitaria/catalogos/origenes/Create')
const PacientesIndex = () => import('../views/sanitaria/catalogos/pacientes/Index')
const PacientesCreate = () => import('../views/sanitaria/catalogos/pacientes/Create')
const PacientesEdit = () => import('../views/sanitaria/catalogos/pacientes/Edit')
const EstudiosIndex = () => import('../views/sanitaria/catalogos/estudios/Index')
const EstudiosCreate = () => import('../views/sanitaria/catalogos/estudios/Create')
const EstudiosEdit = () => import('../views/sanitaria/catalogos/estudios/Edit')
const EpidemiologiaDashboard = () => import('../views/dashboards/Epidemiologia')
const SanitariaDashboard = () => import('../views/dashboards/Sanitaria')
const RespLaboMuestrasIndex = () => import('../views/sanitaria/responsable_laboratorio/MuestraIndex')
const HojaDeTrabajo = () => import('../views/sanitaria/analistas/HojaDeTrabajo')
const Perfil = () => import('../views/users/Profile')
const RolesIndex = () => import('../views/administracion/roles/Index')

export default {
    mode: 'history',
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/app',
            component: Home,
            name: 'home',
            children: [
                {
                    path: 'epidemiologia',
                    component: EpidemiologiaDashboard,
                    name: 'dash-epidemiologia'
                },
                {
                    path: 'epidemiologia/jefe',
                    name: 'JefeLab',
                    component: JefeLab,
                    meta: {
                        role: [
                            'JEFE DEPARTAMENTO EPIDEMIOLOGIA',
                        ]
                    }
                },
                {
                    path: 'epidemiologia/responsable',
                    name: 'ResponsableLab',
                    component: ResponsableLab,
                    meta: {
                        role: [
                            'RESPONSABLE LABORATORIO EPIDEMIOLOGIA',
                        ]
                    }
                },
                {
                    path: 'no-autorizado',
                    name: 'no-autorizado',
                    component: Forbidden
                },
                {
                    path: 'epidemiologia/analistas/muestras',
                    name: 'AnalistasTrabajo',
                    component: AnalistasTrabajo,
                    meta: {
                        role: [
                            'ANALISTA EPIDEMIOLOGIA',
                        ]
                    }
                },
                {
                    path: 'epidemiologia/muestras',
                    name: 'MuestrasLab',
                    component: MuestrasLab,
                    meta: {
                        role: [
                            'CAPTURISTA EPIDEMIOLOGIA',
                        ]
                    }
                },
                //MÓDULO DE USUARIOS
                {
                    path: 'usuario/',
                    component: Perfil,
                    name: 'user-profile'
                },
                // MÓDULO RIESGOS SANITARIOS
                {
                    path: 'riesgos-sanitarios/',
                    component: Home,
                    children: [
                        {
                            path: 'analistas/',
                            component: Home,
                            children: [
                                {
                                    path: 'muestras/',
                                    component: HojaDeTrabajo,
                                    name: 'analistas-hoja-trabajo',
                                    meta: {
                                        role: ['ANALISTA SANITARIA']
                                    }
                                }
                            ]
                        },
                        {
                            path: '',
                            component: SanitariaDashboard,
                            name: 'sanitaria-dashboard'
                        },
                        {
                            path: 'asignar-muestras',
                            component: RespLaboMuestrasIndex,
                            name: 'asignar-muestras'
                        },
                        {
                            path: 'catalogos/',
                            component: Home,
                            children: [
                                {
                                    path: 'origenes',
                                    component: Home,
                                    children: [
                                        {
                                            path: '',
                                            component: OrigenesIndex,
                                            name: 'origenes-index'
                                        },
                                        {
                                            path: 'nuevo',
                                            component: OrigenesCreate,
                                            name: 'origenes-create'
                                        },
                                        {
                                            path: ':id/editar',
                                            component: OrigenesEdit,
                                            name: 'origenes-edit',
                                            props: true
                                        },
                                    ]
                                },
                                {
                                    path: 'pacientes',
                                    component: Home,
                                    children: [
                                        {
                                            path: '',
                                            component: PacientesIndex,
                                            name: 'pacientes-index'
                                        },
                                        {
                                            path: 'nuevo',
                                            component: PacientesCreate,
                                            name: 'pacientes-create'
                                        },
                                        {
                                            path: ':id/editar',
                                            component: PacientesEdit,
                                            name: 'pacientes-edit',
                                            props: true
                                        },
                                    ]
                                },
                                {
                                    path: 'estudios',
                                    component: Home,
                                    children: [
                                        {
                                            path: '',
                                            component: EstudiosIndex,
                                            name: 'estudios-index'
                                        },
                                        {
                                            path: 'nuevo',
                                            component: EstudiosCreate,
                                            name: 'estudios-create'
                                        },
                                        {
                                            path: ':id/editar',
                                            component: EstudiosEdit,
                                            name: 'estudios-edit',
                                            props: true
                                        }
                                    ]
                                }
                            ],
                        },
                        {
                            path: 'solicitudes',
                            component: Home,
                            children: [
                                {
                                    path: '',
                                    component: SolicitudesIndex,
                                    name: 'solicitudes-index',
                                    meta: {
                                        role: [
                                            'CAPTURISTA SANITARIA',
                                            'RESPONSABLE LABORATORIO SANITARIA',
                                            'JEFE DEPARTAMENTO SANITARIA',
                                        ]
                                    }
                                },
                                {
                                    path: 'nueva',
                                    component: SolicitudesCreate,
                                    name: 'solicitudes-create',
                                    meta: {
                                        role: [
                                            'CAPTURISTA SANITARIA',
                                            'RESPONSABLE LABORATORIO SANITARIA',
                                            'JEFE DEPARTAMENTO SANITARIA',
                                        ]
                                    }
                                },
                                {
                                    path: ':id/editar',
                                    component: SolicitudesEdit,
                                    name: 'solicitudes-editar',
                                    props: true,
                                    meta: {
                                        role: [
                                            'CAPTURISTA SANITARIA',
                                            'RESPONSABLE LABORATORIO SANITARIA',
                                            'JEFE DEPARTAMENTO SANITARIA',
                                        ]
                                    }
                                },
                            ]
                        },
                        {
                            path: 'muestras',
                            component: Home,
                            children: [
                                {
                                    path: '',
                                    component: MuestrasIndex,
                                    name: 'muestras-index',
                                    meta: {
                                        role: [
                                            'ANALISTA SANITARIA',
                                            'RESPONSABLE LABORATORIO SANITARIA',
                                            'JEFE DEPARTAMENTO SANITARIA',
                                        ]
                                    }
                                }
                            ]
                        }
                    ],
                },
                // MÓDULO RIESGOS SANITARIOS
                {
                    path: 'administracion/',
                    component: Home,
                    children: [
                        {
                            path: 'usuarios',
                            component: Home,
                            children: [
                                {
                                    path: '',
                                    component: UsuariosIndex,
                                    name: 'usuarios-index',
                                    meta: {
                                        role: [
                                            'SYSADMIN',
                                            'SUPER',
                                        ]
                                    },
                                },
                                {
                                    path: 'nuevo',
                                    component: UsuariosCreate,
                                    name: 'usuarios-create',
                                    meta: {
                                        role: [
                                            'SYSADMIN',
                                            'SUPER',
                                        ]
                                    },
                                },
                                {
                                    path: ':id/editar',
                                    component: UsuariosEdit,
                                    name: 'usuarios-edit',
                                    props: true,
                                    meta: {
                                        role: [
                                            'SYSADMIN',
                                            'SUPER',
                                        ]
                                    },
                                },
                            ]
                        },
                        {
                            path: 'modulos',
                            component: ModulosIndex,
                            name: 'modulos-index',
                            meta: {
                                role: [
                                    'SYSADMIN',
                                    'SUPER',
                                ]
                            },
                        },
                        {
                            path: 'menus',
                            component: MenusIndex,
                            name: 'menus-index',
                            meta: {
                                role: [
                                    'SYSADMIN',
                                    'SUPER',
                                ]
                            },
                        },
                        {
                            path: 'numero-muestras',
                            component: NumeroMuestrasIndex,
                            name: 'numero-muestras-index',
                            meta: {
                                role: [
                                    'SYSADMIN',
                                    'SUPER',
                                ]
                            },
                        },
                        {
                            path: 'laboratorios',
                            name: 'Laboratorio',
                            component: Laboratorio
                        },
                        {
                            path: 'laboratorios/nuevo',
                            name: 'addLaboratorio',
                            component: addLaboratorio
                        },
                        {
                            path: 'analistas',
                            name: 'Analistas',
                            component: Analistas
                        },
                        {
                            path: 'diagnosticos',
                            name: 'Diagnosticos',
                            component: Diagnosticos
                        },
                        {
                            path: 'tipo-muestra',
                            name: 'ListTipoMuestra',
                            component: ListTipoMuestra
                        },
                        {
                            path: 'metodo-referencia',
                            name: 'ListMetodosReferencia',
                            component: ListMetodosReferencia
                        },
                        {
                            path: 'roles',
                            component: RolesIndex,
                            name: 'roles-index',
                            meta: {
                                role: [
                                    'SUPER',
                                    'SYSADMIN'
                                ]
                            }
                        }
                    ]
                },
                // Modulos
                {
                    path: 'inventarios',
                    component: Home,
                    children: [
                        {
                            path: '',
                            component: DashboardInventarios,
                            name: 'inventarios.dashboard'
                        },
                        {
                            path: 'existencias',
                            name: 'inventarios.existencias',
                            component: Existencias
                        },
                        {
                            path: 'entradas',
                            name: 'inventarios.entradas',
                            component: Entradas
                        },
                        {
                            path: 'ordenes',
                            name: 'inventarios.ordenes',
                            component: OrdenesCompra
                        },
                        {
                            path: 'consumo',
                            name: 'inventarios.consumo',
                            component: Descargas
                        },
                        {
                            path: 'solicitudes',
                            name: 'inventarios.solicitudes',
                            component: Solicitudes
                        },
                        {
                            path: 'transferencias',
                            name: 'inventarios.transferencias',
                            component: Transferencias
                        },
                        {
                            path: 'abastecimientos',
                            name: 'inventarios.abastecimientos',
                            component: Abastecimientos
                        },
                        {
                            path: 'remisiones',
                            name: 'inventarios.remisiones',
                            component: Remisiones
                        },
                        // Catálogos
                        {
                            path: 'areas',
                            name: 'inventarios.areas',
                            component: Areas
                        },
                        {
                            path: 'cat_articulos',
                            name: 'inventarios.cat_articulos',
                            component: CatArticulos
                        },
                        {
                            path: 'marcas',
                            name: 'inventarios.marcas',
                            component: Marcas
                        },
                        {
                            path: 'proveedores',
                            name: 'inventarios.proveedores',
                            component: Proveedores
                        },
                        {
                            path: 'tipos_insumo',
                            name: 'inventarios.tipos_insumo',
                            component: TiposInsumo
                        },

                    ],
                },

                {
                    path: 'calidad',
                    component: Home,
                    children: [
                        {
                            path: '',
                            component: DashboardCalidad,
                            name: 'calidad.dashboard'
                        },
                        {
                            path: 'registros',
                            component: Registros,
                            name: 'calidad.registros'
                        },
                        {
                            path: 'documentos',
                            component: Documentos,
                            name: 'calidad.documentos'
                        },
                        {
                            path: 'no_conformidades',
                            component: NoConformidades,
                            name: 'calidad.no_conformidades'
                        },
                    ],
                },
            ]
        }
    ]
}
