import axios from '@/config/axios.js'

export default {

    setApplication(data) {
        return axios.post('ui/set_application', data)
    },

    unsetApplication() {
        return axios.post('ui/unset_application')
    },
}
