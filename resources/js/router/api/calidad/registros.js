import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('calidad/registros')
    },

    get(id) {
        return axios.get('calidad/registros/' + id)
    },

    add(data) {
        return axios.post(
            'calidad/registros', data)
    },

    upload(data) {
        return axios.post({headers:{'Content-Type': 'multipart/form-data'}}
        ,'calidad/registros', data)
    },

    edit(id, data) {
        return axios.post('calidad/registros/' + id, data)
    },

    delete(id) {
        return axios.delete('calidad/registros/' + id)
    },


}
