import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('calidad/no_conformidades')
    },

    get(id) {
        return axios.get('calidad/no_conformidades/' + id)
    },

    add(data) {
        return axios.post(
            'calidad/no_conformidades', data)
    },

    upload(data) {
        return axios.post({headers:{'Content-Type': 'multipart/form-data'}}
        ,'calidad/no_conformidades', data)
    },

    subirEvidencias(data) {
        return axios.post('calidad/no_conformidades/evidencias', data)
    },

    edit(id, data) {
        return axios.put('calidad/no_conformidades/' + id, data)
    },

    delete(id) {
        return axios.delete('calidad/no_conformidades/' + id)
    },

    getSources() {
        return axios.post('calidad/no_conformidades/sources')
    },

}
