import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('calidad/documentos')
    },

    get(id) {
        return axios.get('calidad/documentos/' + id)
    },

    add(data) {
        return axios.post(
            'calidad/documentos', data)
    },

    upload(data) {
        return axios.post({headers:{'Content-Type': 'multipart/form-data'}}
        ,'calidad/documentos', data)
    },

    edit(id, data) {
        return axios.post('calidad/documentos/' + id, data)
    },

    delete(id) {
        return axios.delete('calidad/documentos/' + id)
    },


}
