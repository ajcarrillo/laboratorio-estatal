import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/transferencias')
    },

    get(id) {
        return axios.get('inventarios/transferencias/' + id)
    },

    getByTipo(tipo) {
        return axios.get('inventarios/transferencias/tipo/' + tipo)
    },

    add(data) {
        return axios.post('inventarios/transferencias', data)
    },

    edit(id, data) {
        return axios.put('inventarios/transferencias/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/transferencias/' + id)
    },

    getTiposTransferencia() {
        return axios.post('inventarios/transferencias/tipos_transferencia')
    },

    getAreas() {
        return axios.post('inventarios/areas/areas')
    },

    getArticulos(data) {
        return axios.post('inventarios/transferencias/articulos', data)
    },

    cambiarEstado(id, estado) {
        return axios.post('inventarios/transferencias/cambiar_estado/' + id, estado)
    },

    getSolicitudes() {
        return axios.post('inventarios/transferencias/get_solicitudes')
    },

    reporte(id) {
        return axios.get('inventarios/transferencias/reporte/' + id)
    },

}
