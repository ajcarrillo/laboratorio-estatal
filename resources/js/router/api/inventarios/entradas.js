import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/entradas')
    },

    get(id) {
        return axios.get('inventarios/entradas/' + id)
    },

    add(data) {
        return axios.post('inventarios/entradas', data)
    },

    addRemision(data) {
        return axios.post('inventarios/entradas/remision', data)
    },

    edit(id, data) {
        return axios.put('inventarios/entradas/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/entradas/' + id)
    },

    getTiposEntrada() {
        return axios.post('inventarios/entradas/tipos_entrada')
    },

    getByTipo(tipo) {
        return axios.get('inventarios/entradas/tipo/' + tipo)
    },

    getTiposInsumo() {
        return axios.post('inventarios/entradas/tipos_insumo')
    },

    getProveedores() {
        return axios.post('inventarios/entradas/proveedores')
    },

    getAreas() {
        return axios.post('inventarios/areas/areas')
    },

    getCatArticulos() {
        return axios.post('inventarios/entradas/cat_articulos')
    },

    addArticulo(data) {
        return axios.post('inventarios/entradas/guardar_articulo', data)
    },

    cambiarEstado(id, estado){
        return axios.post('inventarios/entradas/cambiar_estado/'+id, estado)
    },

    pdf(id){
        return axios.post('inventarios/entradas/pdf/'+id)
    }

}
