import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/existencias')
    },

    get(id) {
        return axios.get('inventarios/cat_articulos/' + id)
    },

    add(data) {
        return axios.post('inventarios/cat_articulos', data)
    },

    edit(id, data) {
        return axios.put('inventarios/cat_articulos/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/cat_articulos/' + id)
    },


}
