import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/areas')
    },

    get(id) {
        return axios.get('inventarios/areas/' + id)
    },

    add(data) {
        return axios.post('inventarios/areas', data)
    },

    edit(id, data) {
        return axios.put('inventarios/areas/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/areas/' + id)
    },

    getClues() {
        return axios.post('inventarios/areas/clues')
    },
    getAreas() {
        return axios.post('inventarios/areas/areas')
    },

}
