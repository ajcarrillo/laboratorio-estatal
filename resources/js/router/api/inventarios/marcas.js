import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/marcas')
    },

    get(id) {
        return axios.get('inventarios/marcas/' + id)
    },

    add(data) {
        return axios.post('inventarios/marcas', data)
    },

    edit(id, data) {
        return axios.put('inventarios/marcas/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/marcas/' + id)
    }
}
