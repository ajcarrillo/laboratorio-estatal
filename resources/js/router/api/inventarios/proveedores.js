import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/proveedores')
    },

    get(id) {
        return axios.get('inventarios/proveedores/' + id)
    },

    add(data) {
        return axios.post('inventarios/proveedores', data)
    },

    edit(id, data) {
        return axios.put('inventarios/proveedores/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/proveedores/' + id)
    }
}
