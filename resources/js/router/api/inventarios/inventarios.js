import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/tipos_insumo')
    },

    get(id) {
        return axios.get('inventarios/tipos_insumo/' + id)
    },

    add(data) {
        return axios.post('inventarios/tipos_insumo', data)
    },

    edit(id, data) {
        return axios.put('inventarios/tipos_insumo/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/tipos_insumo/' + id)
    }
}
