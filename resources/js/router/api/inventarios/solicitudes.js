import axios from '@/config/axios.js'

export default {

    gets() {
        return axios.get('inventarios/solicitudes/')
    },

    get(id) {
        return axios.get('inventarios/solicitudes/' + id)
    },

    getByTipo(tipo) {
        return axios.get('inventarios/solicitudes/tipo/' + tipo)
    },

    add(data) {
        return axios.post('inventarios/solicitudes', data)
    },

    edit(id, data) {
        return axios.put('inventarios/solicitudes/' + id, data)
    },

    delete(id) {
        return axios.delete('inventarios/solicitudes/' + id)
    },

    getTiposSolicitud() {
        return axios.post('inventarios/solicitudes/tipos_solicitud')
    },

    getProveedores() {
        return axios.post('inventarios/entradas/proveedores')
    },

    getTiposInsumo() {
        return axios.post('inventarios/solicitudes/tipos_insumo')
    },

    getAreas() {
        return axios.post('inventarios/areas/areas')
    },

    getCatArticulos() {
        return axios.post('inventarios/solicitudes/cat_articulos')
    },

    addArticulo(data) {
        return axios.post('inventarios/solicitudes/guardar_articulo', data)
    },

    cambiarEstado(id, estado) {
        return axios.post('inventarios/solicitudes/cambiar_estado/' + id, estado)
    },

    pdf(id) {
        return axios.post('inventarios/solicitudes/pdf/' + id)
    }

}
