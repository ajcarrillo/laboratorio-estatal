import Vue from 'vue'
import VueRouter from 'vue-router';
import routes from './routes';
import store from '../store'

Vue.use(VueRouter);

const router = new VueRouter(routes);

router.beforeEach((to, from, next) => {

    store.dispatch('auth/fetchRoles', window.user.roles)

    if (!to.meta.role) {
        next();
    } else {
        let {role} = to.meta
        let userRoles = store.getters['auth/getRoles']

        if (userRoles.some(function (el) {
            return role.includes(el)
        })) {
            next();
        } else {
            next({name: 'no-autorizado'})
        }
    }
})

export default router
