require('./bootstrap')

window.Vue = require('vue')

import vuetify from './vuetify'
import store from './store';
import router from './router';
import VeeValidate from 'vee-validate'
import VeeValidateEs from './utilities/vee-validate-es'
import LaravelPermissions from 'laravel-permissions'
import './filters/filters'
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faBookMedical,
    faCaretDown,
    faCaretUp,
    faCog,
    faDiagnoses,
    faFileSignature,
    faMicroscope,
    faNotesMedical,
    faPrescriptionBottle,
    faUser
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

import {mapActions, mapGetters, mapState} from 'vuex'

library.add(
    faCog,
    faUser,
    faCaretUp,
    faCaretDown,
    faDiagnoses,
    faMicroscope,
    faBookMedical,
    faNotesMedical,
    faFileSignature,
    faPrescriptionBottle,
)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(LaravelPermissions)

Vue.use(VeeValidate, {
    locale: 'es',
    dictionary: {
        es: {messages: VeeValidateEs}
    }
})
Vue.filter('prettyBytes', function (num) {

    if (typeof num !== 'number' || isNaN(num)) {
        throw new TypeError('Expected a number');
    }

    var exponent;
    var unit;
    var neg = num < 0;
    var units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

    if (neg) {
        num = -num;
    }

    if (num < 1) {
        return (neg ? '-' : '') + num + ' B';
    }

    exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1);
    num = (num / Math.pow(1000, exponent)).toFixed(2) * 1;
    unit = units[exponent];

    return (neg ? '-' : '') + num + ' ' + unit;
});

Vue.config.productionTip = false

const app = new Vue({
    vuetify,
    router,
    store,
    data: () => ({
        drawer: null,
    }),
    created() {
        this['auth/fetchUser'](window.user);
        this['auth/fetchRoles'](window.user.roles);
        this['auth/fetchPermissions'](window.user.permissions);

        this['ui/fetchAplication'](window.aplication);
        this['ui/fetchAplications'](window.aplications);
        this['ui/fetchMenu'](window.menu);

        this['sanitaria/fetchOrigenes'](window.origenes)
        this['sanitaria/fetchEstudios'](window.estudios)
        this['sanitaria/fetchPaquetes'](window.paquetes)
        this['sanitaria/fetchCategorias'](window.categorias)
        this['sanitaria/fetchRecipientes'](window.recipientes)
        this['sanitaria/fetchSubcategorias'](window.subcategorias)
        this['sanitaria/fetchTiposMuestreo'](window.tipoMuestreo)

        store.commit('catalogos/SET_LABORATORIOS', window.laboratorios)

        this.$laravel.setRoles(this['auth/getRoles'])
        this.$laravel.setPermissions(this['auth/getPermissions'])
    },
    methods: {
        closeSnackBar() {
            this['ui/setSnackBar']({text: '', color: '', show: false})
        },
        logout() {
            document.getElementById('logout-form').submit();
        },
        cambiar() {
            window.location.replace('/elegir-aplicacion')
        },
        goToProfile() {
            this.$router.push({name: 'user-profile'})
        },
        ...mapActions([
            'auth/fetchUser',
            'auth/fetchRoles',
            'auth/fetchPermissions',
            'sanitaria/fetchOrigenes',
            'sanitaria/fetchEstudios',
            'sanitaria/fetchPaquetes',
            'sanitaria/fetchCategorias',
            'sanitaria/fetchRecipientes',
            'sanitaria/fetchSubcategorias',
            'inventarios/fetchEntradaArticulos',
            'sanitaria/fetchTiposMuestreo',
            'sanitaria/fetchCapturistasSanitaria',
            'ui/fetchMenu',
            'ui/fetchAplication',
            'ui/fetchAplications',
            'ui/setSnackBar'
        ]),
        ...mapGetters([
            'auth/getUserName',
            'auth/getUserEmail',
            'auth/getEntradaArticulos',
            'ui/getMenu',
            'ui/getAplication',
            'ui/getAplications',
        ])
    },
    computed: {
        userRoles() {
            return this['auth/getRoles']
        },
        userName() {
            return this['auth/getUserName']()
        },
        email() {
            return this['auth/getUserEmail']()
        },
        menu() {
            return this['ui/getMenu']()
        },
        aplication() {
            return this['ui/getAplication']()
        },
        aplications() {
            return this['ui/getAplications']()
        },
        ...mapGetters([
            'auth/getRoles',
            'auth/getPermissions',
        ]),
        ...mapState({
            snackBar: state => state.ui.snackBar
        })
    }
}).$mount('#app');
