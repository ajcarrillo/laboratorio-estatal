export {mapActions} from 'vuex'
export {Toast} from '../../mixins/toast';
import ListResTableObj from './ListResTable';
import ListJefeTableObj from './ListJefeTable';
import ListDiagnosticoTableObj from './ListDiagnosticoTable';
import ListMetReferenciaTableObj from './ListMetReferenciaTable';
import ListTipMuestraTableObj from './ListTipMuestraTable';


export const ListResTable = ListResTableObj;
export const ListJefeTable = ListJefeTableObj;
export const ListDiagnosticoTable = ListDiagnosticoTableObj;
export const ListMetReferenciaTable = ListMetReferenciaTableObj;
export const ListTipMuestraTable = ListTipMuestraTableObj;