export const Toast = {
    methods: {
        toastSuccess(text) {
            this['ui/setSnackBar']({text, color: 'success', show: true})
        },
        toastError(text) {
            this['ui/setSnackBar']({text, color: 'error', show: true})
        }
    }
}
