<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!--Import Google Icon Font-->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="{{ 'css/materialize.min.css' }}">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>{{ config('app.name', 'Laravel') }}</title>

        <style>
            html {
                font-family: 'Roboto', sans-serif !important;
            }

            body {
                background-color: #E5E8EB;
            }

            header {
                text-align: center;
                padding: 30px 0 140px 0;
            }

            button,
            input,
            optgroup,
            select,
            textarea {
                font-family: 'Roboto', sans-serif !important;
                /* 1 */
                font-size: 100%;
                /* 1 */
                line-height: 1.15;
                /* 1 */
                margin: 0;
                /* 2 */
            }

            .page-footer {
                padding-top: 20px;
                color: #000;
                background-color: #fff;
            }

            .page-footer .footer-copyright {
                color: inherit;
                background-color: inherit;
            }

            @font-face {
                font-family: 'Material Icons';
                font-style: normal;
                font-weight: 400;
                src: url({{ asset('fonts/vendor/meterialicons/MaterialIcons-Regular.eot') }}); /* For IE6-8 */
                src: local('Material Icons'),
                local('MaterialIcons-Regular'),
                url({{asset('fonts/vendor/meterialicons/MaterialIcons-Regular.woff2')}}) format('woff2'),
                url({{asset('fonts/vendor/meterialicons/MaterialIcons-Regular.woff')}}) format('woff'),
                url({{asset('fonts/vendor/meterialicons/MaterialIcons-Regular.ttf')}}) format('truetype');
            }

            .material-icons {
                font-family: 'Material Icons';
                font-weight: normal;
                font-style: normal;
                font-size: 24px; /* Preferred icon size */
                display: inline-block;
                line-height: 1;
                text-transform: none;
                letter-spacing: normal;
                word-wrap: normal;
                white-space: nowrap;
                direction: ltr;

                /* Support for all WebKit browsers. */
                -webkit-font-smoothing: antialiased;
                /* Support for Safari and Chrome. */
                text-rendering: optimizeLegibility;

                /* Support for Firefox. */
                -moz-osx-font-smoothing: grayscale;

                /* Support for IE. */
                font-feature-settings: 'liga';
            }
        </style>
    </head>

    <body>
        <header class="teal darken-3" style="color: white">
            <h4 style="font-family: 'Roboto', sans-serif; font-weight: 300">Sistema Integral de Laboratorio Estatal de Salud Pública</h4>
            <h5 style="font-family: 'Roboto', sans-serif; font-weight: 300">Coordinación de Informática</h5>
        </header>
        <div class="container" style="margin-top: -115px">
            <div class="row" style="margin-bottom: 0">
                <div class="col s12" style="text-align: right">
                    {{--<a href="app/usuario" style="color: white; font-size: 1.2rem">Perfil</a>
                    <span style="color: white; font-size: 1.2rem">|</span>--}}
                    <a class="" href="{{ route('logout') }}"
                       style="color: white; font-size: 1.2rem"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        Cerrar sesión
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </div>
            @foreach($sistemas as $sistema)
                <div class="row">
                    <div class="col s12">
                        <div class="card horizontal">
                            <div class="card-image">
                                <img src="{{ asset("img/{$sistema->picture}") }}">
                            </div>
                            <div class="card-stacked">
                                <div class="card-content">
                                    <p>{{ mb_strtoupper($sistema->titulo, 'UTF-8') }}</p>
                                </div>
                                <div class="card-action">
                                    <a href="{{ $sistema->route }}"
                                       onclick="event.preventDefault();
                                           document.getElementById('set-app-form-{{$sistema->id}}').submit();"
                                    >
                                        {{ $sistema->subtitulo }}
                                    </a>
                                    <form id="set-app-form-{{$sistema->id}}" action="{{ route('elegir.aplicacion') }}" method="POST" style="display: none;">
                                        @csrf
                                        <input type="hidden" name="app_id" value="{{ $sistema->id }}">
                                        <input type="hidden" name="app_route" value="{{ $sistema->route }}">
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <footer class="page-footer">
            <div class="footer-copyright">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div style="display: flex; justify-content: space-between; align-items: center">
                                <div style="width: 25%;"><img src="{{ asset('img/logo.jpg') }}" style="width: 100%" alt="SESA"></div>

                                ® {{ \Carbon\Carbon::now()->year }} Secretaría de Salud Quintana Roo
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!--JavaScript at end of body for optimized loading-->
        <script src="{{ asset('js/materialize.min.js') }}"></script>
    </body>
</html>
