<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        @yield('extra-meta')

        <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

        <!-- Styles -->
        @yield('extra-head')
        <style>
            [v-cloak] {
                display: none;
            }

            .swal2-container {
                font-family: "Roboto", sans-serif;
            }
        </style>
    </head>
    <body>
        <div id="app" v-cloak>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @yield('content', 'Default Content')
        </div>

        @routes
        @yield('beforeScripts')
        <script src="{{ mix('js/app.js') }}"></script>
        @yield('afeterScripts')
    </body>
</html>
