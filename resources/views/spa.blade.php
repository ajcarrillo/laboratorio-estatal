@extends('layouts.app')

@section('extra-meta')
    @auth
        <meta name="api-token" content="{{ Auth::user()->api_token }}">
    @endauth
@endsection

@section('extra-head')
    <style>
        .v-navigation-drawer--clipped:not(.v-navigation-drawer--temporary):not(.v-navigation-drawer--is-mobile) {
            z-index: 3 !important;
        }
    </style>
@endsection

@section('content')
    @verbatim
        <v-app id="inspire">
            <v-navigation-drawer
                v-if="aplication"
                v-model="drawer"
                :clipped="$vuetify.breakpoint.lgAndUp"
                app
            >
                <v-list-item>
                    <v-list-item-content>
                        <v-list-item-title class="title">
                            {{ aplication.titulo }}
                        </v-list-item-title>
                        <v-list-item-subtitle>
                            {{ aplication.subtitulo }}
                        </v-list-item-subtitle>
                    </v-list-item-content>
                </v-list-item>

                <v-divider></v-divider>
                <v-list>
                    <v-list-item
                        v-for="item in menu"
                        v-if="!item.children_menu.length"
                        :key="item.id"
                        exact
                        link
                        @click=""
                        :to="{name: item.path}"
                    >
                        <v-list-item-icon>
                            <v-icon>{{ item.icon }}</v-icon>
                        </v-list-item-icon>
                        <v-list-item-content>
                            <v-list-item-title v-text="item.descripcion"></v-list-item-title>
                        </v-list-item-content>
                    </v-list-item>

                    <v-list-group
                        v-for="item in menu"
                        v-if="item.children_menu.length"
                        :key="item.id"
                        v-model="item.active"
                        :prepend-icon="item.icon"
                        no-action
                    >
                        <template v-slot:activator>
                            <v-list-item-content>
                                <v-list-item-title v-text="item.descripcion"></v-list-item-title>
                            </v-list-item-content>
                        </template>

                        <v-list-item
                            v-for="subItem in item.children_menu"
                            :key="subItem.id"
                            :to="{name: subItem.path}"
                            @click=""
                        >
                            <v-list-item-content>
                                <v-list-item-title v-text="subItem.descripcion"></v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                    </v-list-group>
                </v-list>

            </v-navigation-drawer>

            <v-app-bar
                :clipped-left="$vuetify.breakpoint.lgAndUp"
                app
                color="teal"
                dark
            >
                <v-toolbar-title
                    class="ml-0 pl-3"
                >
                    <span class="d-flex align-center">
                        <v-app-bar-nav-icon @click.stop="drawer = !drawer" class="mr-4"></v-app-bar-nav-icon>

                        <span class="hidden-sm-and-down">
                            Sistema Integral de Salud Pública
                        </span>
                    </span>
                </v-toolbar-title>
                <v-spacer></v-spacer>
                <v-menu v-if="true" offset-y>
                    <template v-slot:activator="{ on }">
                        <v-btn
                            text
                            v-on="on"
                        >
                            <span class="d-flex flex-column">
                                <span class="text-left">{{ userName }}</span>
                                <span class="text-left overline">{{ userRoles[0] }}</span>
                            </span>
                            <v-icon right>mdi-menu-down</v-icon>
                        </v-btn>
                    </template>
                    <v-list dense>
                        <v-list-item @click="goToProfile">
                            <v-list-item-icon>
                                <v-icon>mdi-account-circle</v-icon>
                            </v-list-item-icon>
                            <v-list-item-content>
                                <v-list-item-title>Perfil</v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                        <v-list-item @click="cambiar">
                            <v-list-item-icon>
                                <v-icon>mdi-swap-horizontal</v-icon>
                            </v-list-item-icon>
                            <v-list-item-content>
                                <v-list-item-title> Cambiar de app</v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                        <v-list-item @click="logout">
                            <v-list-item-icon>
                                <v-icon>mdi-logout</v-icon>
                            </v-list-item-icon>
                            <v-list-item-content>
                                <v-list-item-title> Cerrar sesión</v-list-item-title>
                            </v-list-item-content>
                        </v-list-item>
                    </v-list>
                </v-menu>
            </v-app-bar>
            <v-content class="mb-12">
                <router-view></router-view>
            </v-content>
            <v-snackbar
                v-model="snackBar.show"
                :color="snackBar.color"
                :timeout="5000"
                bottom
            >
                {{ snackBar.text }}
                <v-btn
                    color="white"
                    text
                    @click="closeSnackBar"
                >
                    Cerrar
                </v-btn>
            </v-snackbar>
            <v-footer
                app
                color="white"
                class=""
                padless
            >

                    <v-img left class="pt-5 pl-5">
                        <img :src="'/img/logo.jpg'" style="height: 35px" alt="LOGO SESA">
                    </v-img>
                    <p class="text-right pt-5 pr-5" style="font-size: 12px;">® {{ new Date().getFullYear() }} Secretaría de Salud Quintana Roo</p>

            </v-footer>
        </v-app>
    @endverbatim
@endsection

@section('beforeScripts')
    <script type="text/javascript">
        window.user = {!! json_encode($user) !!};

        window.aplication = {!! json_encode($aplication) !!};
        window.aplications = {!! json_encode($aplications) !!};
        window.menu = {!! json_encode($menu) !!};

        window.origenes = {!! json_encode($origenes) !!};
        window.estudios = {!! json_encode($estudios) !!};
        window.paquetes = {!! json_encode($paquetes) !!};
        window.pantallas = {!! json_encode($pantallas) !!};
        window.categorias = {!! json_encode($categorias) !!};
        window.recipientes = {!! json_encode($recipientes) !!};
        window.tipoMuestreo = {!! json_encode($tipoMuestreo) !!};
        window.subcategorias = {!! json_encode($subcategorias) !!};
        window.laboratorios = {!! json_encode($laboratorios) !!};
    </script>
@endsection
