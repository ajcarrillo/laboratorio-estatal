<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <!--Import Google Icon Font-->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="{{ 'css/materialize.min.css' }}">

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <title>{{ config('app.name', 'Laravel') }}</title>

        <style>
            html {
                font-family: 'Roboto', sans-serif !important;
            }

            body {
                background-color: #E5E8EB;
            }

            header {
                text-align: center;
                padding: 30px 0 140px 0;
            }

            button,
            input,
            optgroup,
            select,
            textarea {
                font-family: 'Roboto', sans-serif !important;
                /* 1 */
                font-size: 100%;
                /* 1 */
                line-height: 1.15;
                /* 1 */
                margin: 0;
                /* 2 */
            }

            @font-face {
                font-family: 'Material Icons';
                font-style: normal;
                font-weight: 400;
                src: url({{ asset('fonts/vendor/meterialicons/MaterialIcons-Regular.eot') }}); /* For IE6-8 */
                src: local('Material Icons'),
                local('MaterialIcons-Regular'),
                url({{asset('fonts/vendor/meterialicons/MaterialIcons-Regular.woff2')}}) format('woff2'),
                url({{asset('fonts/vendor/meterialicons/MaterialIcons-Regular.woff')}}) format('woff'),
                url({{asset('fonts/vendor/meterialicons/MaterialIcons-Regular.ttf')}}) format('truetype');
            }

            .material-icons {
                font-family: 'Material Icons';
                font-weight: normal;
                font-style: normal;
                font-size: 24px;  /* Preferred icon size */
                display: inline-block;
                line-height: 1;
                text-transform: none;
                letter-spacing: normal;
                word-wrap: normal;
                white-space: nowrap;
                direction: ltr;

                /* Support for all WebKit browsers. */
                -webkit-font-smoothing: antialiased;
                /* Support for Safari and Chrome. */
                text-rendering: optimizeLegibility;

                /* Support for Firefox. */
                -moz-osx-font-smoothing: grayscale;

                /* Support for IE. */
                font-feature-settings: 'liga';
            }
        </style>
    </head>

    <body>
        <header class="teal darken-3" style="color: white">
            <h4 style="font-family: 'Roboto', sans-serif; font-weight: 300">Sistema Integral de Laboratorio Estatal de Salud Pública</h4>
            <h5 style="font-family: 'Roboto', sans-serif; font-weight: 300">Coordinación de Informática</h5>
        </header>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    <div style="">
                        <div class="card z-depth-3" style="padding: 1rem; width: 100%; max-width: 500px; margin: -115px auto 0 auto; box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24); border-radius: 3px;">
                            <div class="row">
                                <div class="col s12">
                                    <div class="teal darken-3 valign-wrapper" style="width: 100px; height: 100px; border-radius: 50%; justify-content: center; margin: 0 auto">
                                        <i class="material-icons" style="color: white; font-size: 5rem">person</i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <form method="POST"
                                          action="{{ route('login') }}"
                                    >
                                        @csrf
                                        <div class="input-field">

                                            <input id="email" type="email"
                                                   class="@error('email') invalid @enderror"
                                                   name="email"
                                                   value="{{ old('email') }}"
                                                   autocomplete="new-password"
                                                   required
                                                   autofocus
                                            >
                                            <label for="email">Correo electrónico</label>
                                            @error('email')
                                            <span class="helper-text" data-error="{{ $message }}" data-success="right">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <div class="input-field">
                                            <input id="password"
                                                   type="password"
                                                   class="@error('password') invalid @enderror"
                                                   name="password"
                                                   autocomplete="new-password"
                                                   required
                                            >
                                            <label for="password">Contraseña</label>
                                            @error('password')
                                            <span class="helper-text" data-error="{{ $message }}" data-success="right">{{ $message }}</span>
                                            @enderror
                                        </div>

                                        <button type="submit" class="btn teal darken-3 waves-effect waves-light">
                                            Entrar
                                        </button>
                                    </form>
                                </div>
                            </div>
                            <div style="display: flex; padding: 1rem 0;">
                                <div style="width: 50%; padding-right: 0.5rem"><img src="{{ asset('img/Logo_Salud_transparente_HZ-jun19.png') }}" style="width: 100%" alt="SESA">
                                </div>
                                <div style="width: 50%; padding-left: 0.5rem"><img src="{{ asset('img/logo.jpg') }}" style="width: 100%" alt="SESA"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--JavaScript at end of body for optimized loading-->
        <script src="{{ asset('js/materialize.min.js') }}"></script>
    </body>
</html>
