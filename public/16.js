(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[16],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sanitaria/EstatusMuestra.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sanitaria/EstatusMuestra.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'EstatusMuestra',
  props: {
    item: {
      type: Object,
      required: true
    }
  },
  data: function data() {
    return {};
  },
  computed: {
    color: function color() {
      return {
        teal: this.item.estatus === 'ASIGNADA',
        primary: this.item.estatus === 'PENDIENTE',
        orange: this.item.estatus === 'RECIBIDA',
        indigo: this.item.estatus === 'EN PROCESO',
        'cyan darken-4': this.item.estatus === 'RESULTADOS',
        'light-green darken-1': this.item.estatus === 'VALIDADA',
        success: this.item.estatus === 'AUTORIZADA'
      };
    }
  },
  template: '<v-chip :class="color" text-color="white" small>{{ item.estatus }}</v-chip>'
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'FechaAlerta',
  components: {},
  props: {
    fechaRecepcion: {
      type: String,
      required: true
    },
    addDays: {
      type: Number,
      required: true
    }
  },
  data: function data() {
    return {};
  },
  methods: {},
  computed: {
    diaAlerta: function diaAlerta() {
      return moment(this.fechaRecepcion).add(this.addDays, 'days');
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mixins_toast__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../mixins/toast */ "./resources/js/mixins/toast.js");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _components_sanitaria_FechaAlerta__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../components/sanitaria/FechaAlerta */ "./resources/js/components/sanitaria/FechaAlerta.vue");
/* harmony import */ var _components_sanitaria_EstatusMuestra__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../components/sanitaria/EstatusMuestra */ "./resources/js/components/sanitaria/EstatusMuestra.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





var ResultadosForm = function ResultadosForm() {
  return __webpack_require__.e(/*! import() */ 12).then(__webpack_require__.bind(null, /*! ../../../components/sanitaria/forms/ReportarForm */ "./resources/js/components/sanitaria/forms/ReportarForm.vue"));
};

var ResultadoGlobalForm = function ResultadoGlobalForm() {
  return __webpack_require__.e(/*! import() */ 27).then(__webpack_require__.bind(null, /*! ../../../components/sanitaria/forms/ReportarResultadosGlobalesForm */ "./resources/js/components/sanitaria/forms/ReportarResultadosGlobalesForm.vue"));
};

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'HojaDeTrabajo',
  mixins: [_mixins_toast__WEBPACK_IMPORTED_MODULE_1__["Toast"]],
  components: {
    ResultadoGlobalForm: ResultadoGlobalForm,
    ResultadosForm: ResultadosForm,
    EstatusMuestra: _components_sanitaria_EstatusMuestra__WEBPACK_IMPORTED_MODULE_4__["default"],
    FechaAlerta: _components_sanitaria_FechaAlerta__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  props: {},
  data: function data() {
    return {
      fetchingMuestras: false,
      processingSamples: false,
      muestras: [],
      selectedItems: [],
      params: {
        muestra: '',
        estado: ''
      },
      estatusEstudios: ['EN PROCESO', 'VALIDADO', 'RESULTADOS', 'AUTORIZADO', 'PENDIENTE'],
      openResultadosModal: false
    };
  },
  created: function created() {},
  methods: _objectSpread({
    closeDialog: function () {
      var _closeDialog = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                this.openResultadosModal = false;
                _context.next = 3;
                return this.getMuestras();

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function closeDialog() {
        return _closeDialog.apply(this, arguments);
      }

      return closeDialog;
    }(),
    getMuestras: function () {
      var _getMuestras = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var params, response;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                this.fetchingMuestras = true;
                this.muestras = this.selectedItems = [];
                _context2.prev = 2;
                params = this.params;
                _context2.next = 6;
                return axios.get(route('api.v1.analistas.muestras.sanitarias.index'), {
                  params: params
                });

              case 6:
                response = _context2.sent;
                this.muestras = response.data.data;
                _context2.next = 13;
                break;

              case 10:
                _context2.prev = 10;
                _context2.t0 = _context2["catch"](2);
                console.log(_context2.t0);

              case 13:
                _context2.prev = 13;
                this.fetchingMuestras = false;
                return _context2.finish(13);

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, this, [[2, 10, 13, 16]]);
      }));

      function getMuestras() {
        return _getMuestras.apply(this, arguments);
      }

      return getMuestras;
    }(),
    ProcessSamples: function () {
      var _ProcessSamples = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        var estudios;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                this.processingSamples = true;
                estudios = this.samplesToProcess.map(function (item) {
                  return item.id;
                });
                _context3.prev = 2;
                _context3.next = 5;
                return axios.patch(route('api.v1.analistas.muestras.sanitarias.procesar'), {
                  estudios: estudios
                });

              case 5:
                _context3.next = 7;
                return this.getMuestras();

              case 7:
                this.toastSuccess('Los estudios se procesaron correctamente');
                _context3.next = 14;
                break;

              case 10:
                _context3.prev = 10;
                _context3.t0 = _context3["catch"](2);
                console.log(_context3.t0);
                this.toastError('Lo sentimos algo ha salido mal, intente de nuevo');

              case 14:
                _context3.prev = 14;
                this.processingSamples = false;
                return _context3.finish(14);

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, this, [[2, 10, 14, 17]]);
      }));

      function ProcessSamples() {
        return _ProcessSamples.apply(this, arguments);
      }

      return ProcessSamples;
    }()
  }, Object(vuex__WEBPACK_IMPORTED_MODULE_2__["mapActions"])(['ui/setSnackBar'])),
  computed: {
    samplesToProcess: function samplesToProcess() {
      return this.selectedItems.filter(function (item) {
        return item.estatus === 'PENDIENTE';
      });
    },
    hasSamplesToProcess: function hasSamplesToProcess() {
      return this.samplesToProcess.length > 0;
    },
    samplesToProcessCount: function samplesToProcessCount() {
      return this.samplesToProcess.length;
    },
    samplesToReport: function samplesToReport() {
      return this.selectedItems.filter(function (item) {
        return item.estatus === 'EN PROCESO';
      });
    },
    hasSamplesToReport: function hasSamplesToReport() {
      return this.samplesToReport.length > 0;
    },
    samplesToReportCount: function samplesToReportCount() {
      return this.samplesToReport.length;
    }
  },
  watch: {}
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true&":
/*!************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true& ***!
  \************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("span", [_vm._v(_vm._s(_vm._f("formatDate")(_vm.diaAlerta)))])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true&":
/*!*******************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true& ***!
  \*******************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "v-container",
    { attrs: { fluid: "" } },
    [
      _c(
        "v-row",
        [
          _c(
            "v-col",
            [
              _c(
                "v-card",
                [
                  _c("v-card-text", [
                    _c(
                      "form",
                      {
                        on: {
                          submit: function($event) {
                            $event.preventDefault()
                            return _vm.getMuestras($event)
                          }
                        }
                      },
                      [
                        _c(
                          "v-layout",
                          { staticClass: "pa-2", attrs: { row: "", wrap: "" } },
                          [
                            _c(
                              "v-flex",
                              { attrs: { md2: "", xs12: "" } },
                              [
                                _c("v-text-field", {
                                  staticClass: "mr-4",
                                  attrs: {
                                    autofocus: "",
                                    "data-vv-name": "params.muestra",
                                    label: "Núm. muestra",
                                    type: "text"
                                  },
                                  model: {
                                    value: _vm.params.muestra,
                                    callback: function($$v) {
                                      _vm.$set(_vm.params, "muestra", $$v)
                                    },
                                    expression: "params.muestra"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-flex",
                              { attrs: { md4: "", xs12: "" } },
                              [
                                _c("v-select", {
                                  attrs: {
                                    items: _vm.estatusEstudios,
                                    clearable: "",
                                    label: "Estatus estudio"
                                  },
                                  model: {
                                    value: _vm.params.estado,
                                    callback: function($$v) {
                                      _vm.$set(_vm.params, "estado", $$v)
                                    },
                                    expression: "params.estado"
                                  }
                                })
                              ],
                              1
                            ),
                            _vm._v(" "),
                            _c(
                              "v-flex",
                              { attrs: { xs12: "" } },
                              [
                                _c(
                                  "v-btn",
                                  {
                                    attrs: {
                                      loading: _vm.fetchingMuestras,
                                      color: "primary"
                                    },
                                    on: { click: _vm.getMuestras }
                                  },
                                  [
                                    _vm._v(
                                      "\n                                    Buscar\n                                "
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ])
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "v-row",
        [
          _c(
            "v-col",
            [
              _c(
                "v-card",
                [
                  _c("v-data-iterator", {
                    attrs: {
                      items: _vm.muestras,
                      loading: _vm.fetchingMuestras,
                      "hide-default-footer": ""
                    },
                    scopedSlots: _vm._u([
                      {
                        key: "no-data",
                        fn: function() {
                          return [
                            _c("div", { staticClass: "pa-2" }, [
                              _vm._v("No hay datos disponibles")
                            ])
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "loading",
                        fn: function() {
                          return [
                            _c("div", { staticClass: "pa-2" }, [
                              _vm._v("Buscando...")
                            ])
                          ]
                        },
                        proxy: true
                      },
                      {
                        key: "default",
                        fn: function(ref) {
                          var items = ref.items
                          var isSelected = ref.isSelected
                          var select = ref.select
                          return _vm._l(items, function(item) {
                            return _c(
                              "div",
                              [
                                _c(
                                  "v-layout",
                                  { staticClass: "pa-2", attrs: { wrap: "" } },
                                  [
                                    _c(
                                      "v-flex",
                                      { attrs: { md2: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Solicitud")]
                                        ),
                                        _vm._v(" "),
                                        item.estatus === "PENDIENTE" ||
                                        item.estatus === "EN PROCESO"
                                          ? _c("v-checkbox", {
                                              staticClass: "mt-0 pt-0",
                                              attrs: {
                                                "input-value": isSelected(item),
                                                "hide-details": ""
                                              },
                                              on: {
                                                change: function(v) {
                                                  return select(item, v)
                                                }
                                              },
                                              scopedSlots: _vm._u(
                                                [
                                                  {
                                                    key: "label",
                                                    fn: function() {
                                                      return [
                                                        _c(
                                                          "span",
                                                          {
                                                            staticClass:
                                                              "black--text"
                                                          },
                                                          [
                                                            _vm._v(
                                                              _vm._s(
                                                                item.muestra
                                                                  .solicitud
                                                                  .folio
                                                              )
                                                            )
                                                          ]
                                                        )
                                                      ]
                                                    },
                                                    proxy: true
                                                  }
                                                ],
                                                null,
                                                true
                                              )
                                            })
                                          : [
                                              _c(
                                                "span",
                                                { staticClass: "black--text" },
                                                [
                                                  _vm._v(
                                                    _vm._s(
                                                      item.muestra.solicitud
                                                        .folio
                                                    )
                                                  )
                                                ]
                                              )
                                            ]
                                      ],
                                      2
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-flex",
                                      { attrs: { md1: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Muestra")]
                                        ),
                                        _vm._v(" "),
                                        _c("span", {}, [
                                          _vm._v(
                                            _vm._s(item.muestra.numero_muestra)
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-flex",
                                      { attrs: { md3: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Estudio")]
                                        ),
                                        _vm._v(" "),
                                        _c("span", {}, [
                                          _vm._v(
                                            _vm._s(item.estudio.descripcion)
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-flex",
                                      { attrs: { md2: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Fecha recepción")]
                                        ),
                                        _vm._v(" "),
                                        _c("span", {}, [
                                          _vm._v(
                                            _vm._s(
                                              _vm._f("formatDate")(
                                                item.muestra.solicitud
                                                  .fecha_recepcion
                                              )
                                            )
                                          )
                                        ])
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-flex",
                                      { attrs: { md2: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Tiempo alerta")]
                                        ),
                                        _vm._v(" "),
                                        _c("fecha-alerta", {
                                          attrs: {
                                            "add-days": parseInt(
                                              item.estudio.tiempo_alerta
                                            ),
                                            "fecha-recepcion":
                                              item.muestra.solicitud
                                                .fecha_recepcion
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "v-chip",
                                          {
                                            attrs: {
                                              color: "warning",
                                              small: "",
                                              "text-color": "white"
                                            }
                                          },
                                          [
                                            _c(
                                              "v-avatar",
                                              {
                                                staticClass: "warning darken-4",
                                                attrs: { left: "" }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                            " +
                                                    _vm._s(
                                                      item.estudio.tiempo_alerta
                                                    ) +
                                                    "\n                                        "
                                                )
                                              ]
                                            ),
                                            _vm._v(
                                              "\n                                        días\n                                    "
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-flex",
                                      { attrs: { md2: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Tiempo proceso")]
                                        ),
                                        _vm._v(" "),
                                        _c("fecha-alerta", {
                                          attrs: {
                                            "add-days": parseInt(
                                              item.estudio.tiempo_proceso
                                            ),
                                            "fecha-recepcion":
                                              item.muestra.solicitud
                                                .fecha_recepcion
                                          }
                                        }),
                                        _vm._v(" "),
                                        _c(
                                          "v-chip",
                                          {
                                            attrs: {
                                              color: "error",
                                              small: "",
                                              "text-color": "white"
                                            }
                                          },
                                          [
                                            _c(
                                              "v-avatar",
                                              {
                                                staticClass: "error darken-4",
                                                attrs: { left: "" }
                                              },
                                              [
                                                _vm._v(
                                                  "\n                                            " +
                                                    _vm._s(
                                                      item.estudio
                                                        .tiempo_proceso
                                                    ) +
                                                    "\n                                        "
                                                )
                                              ]
                                            ),
                                            _vm._v(
                                              "\n                                        días\n                                    "
                                            )
                                          ],
                                          1
                                        )
                                      ],
                                      1
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "v-flex",
                                      { attrs: { md2: "", xs6: "" } },
                                      [
                                        _c(
                                          "div",
                                          {
                                            staticClass:
                                              "text-capitalize overline text--secondary text--darken-3"
                                          },
                                          [_vm._v("Estatus")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "span",
                                          {},
                                          [
                                            _c("estatus-muestra", {
                                              attrs: { item: item }
                                            })
                                          ],
                                          1
                                        )
                                      ]
                                    )
                                  ],
                                  1
                                ),
                                _vm._v(" "),
                                _c("v-divider")
                              ],
                              1
                            )
                          })
                        }
                      }
                    ]),
                    model: {
                      value: _vm.selectedItems,
                      callback: function($$v) {
                        _vm.selectedItems = $$v
                      },
                      expression: "selectedItems"
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.hasSamplesToReport || _vm.hasSamplesToProcess
        ? _c(
            "v-row",
            [
              _c(
                "v-col",
                [
                  _c(
                    "v-card",
                    [
                      _c(
                        "v-card-text",
                        [
                          _vm.hasSamplesToProcess
                            ? _c(
                                "v-btn",
                                {
                                  attrs: {
                                    loading: _vm.processingSamples,
                                    color: "primary"
                                  },
                                  on: { click: _vm.ProcessSamples }
                                },
                                [
                                  _c("font-awesome-icon", {
                                    staticClass: "mr-1",
                                    attrs: { icon: "microscope" }
                                  }),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(
                                        "Procesar " + _vm.samplesToProcessCount
                                      ) +
                                      "\n                    "
                                  )
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.hasSamplesToReport
                            ? _c(
                                "v-btn",
                                {
                                  staticClass: "white--text",
                                  attrs: { color: "indigo" },
                                  on: {
                                    click: function($event) {
                                      _vm.openResultadosModal = true
                                    }
                                  }
                                },
                                [
                                  _c("font-awesome-icon", {
                                    staticClass: "mr-1",
                                    attrs: { icon: "file-signature" }
                                  }),
                                  _vm._v(
                                    "\n                        " +
                                      _vm._s(
                                        "Reportar " + _vm.samplesToReportCount
                                      ) +
                                      "\n                    "
                                  )
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _c(
                            "v-dialog",
                            {
                              attrs: {
                                persistent: true,
                                fullscreen: "",
                                "hide-overlay": "",
                                scrollable: "",
                                transition: "dialog-bottom-transition"
                              },
                              on: {
                                keydown: function($event) {
                                  if (
                                    !$event.type.indexOf("key") &&
                                    _vm._k(
                                      $event.keyCode,
                                      "esc",
                                      27,
                                      $event.key,
                                      ["Esc", "Escape"]
                                    )
                                  ) {
                                    return null
                                  }
                                  return _vm.closeDialog($event)
                                }
                              },
                              model: {
                                value: _vm.openResultadosModal,
                                callback: function($$v) {
                                  _vm.openResultadosModal = $$v
                                },
                                expression: "openResultadosModal"
                              }
                            },
                            [
                              _c(
                                "v-card",
                                [
                                  _c(
                                    "v-toolbar",
                                    {
                                      staticStyle: {
                                        "flex-basis": "auto",
                                        "flex-direction": "row",
                                        "flex-grow": "0",
                                        "flex-shrink": "1",
                                        "flex-wrap": "nowrap",
                                        "-webkit-box-flex": "0"
                                      },
                                      attrs: { color: "primary", dark: "" }
                                    },
                                    [
                                      _c(
                                        "v-btn",
                                        {
                                          attrs: { dark: "", icon: "" },
                                          on: { click: _vm.closeDialog }
                                        },
                                        [_c("v-icon", [_vm._v("mdi-close")])],
                                        1
                                      ),
                                      _vm._v(" "),
                                      _c("v-toolbar-title", [
                                        _vm._v("Reportar resultados")
                                      ]),
                                      _vm._v(" "),
                                      _c("v-spacer"),
                                      _vm._v(" "),
                                      _c(
                                        "v-toolbar-items",
                                        [
                                          _c(
                                            "v-btn",
                                            {
                                              attrs: { dark: "", text: "" },
                                              on: { click: _vm.closeDialog }
                                            },
                                            [_vm._v("Cerrar")]
                                          )
                                        ],
                                        1
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "v-card-text",
                                    { staticStyle: { "padding-top": "20px" } },
                                    [
                                      _vm._l(_vm.samplesToReport, function(
                                        estudio
                                      ) {
                                        return [
                                          _c(
                                            "v-card",
                                            { staticClass: "mb-4" },
                                            [
                                              _c("v-card-title", [
                                                _c(
                                                  "span",
                                                  { staticClass: "headline" },
                                                  [
                                                    _vm._v(
                                                      _vm._s(
                                                        "Número de muestra " +
                                                          estudio.muestra
                                                            .numero_muestra
                                                      )
                                                    )
                                                  ]
                                                )
                                              ]),
                                              _vm._v(" "),
                                              _c("v-divider"),
                                              _vm._v(" "),
                                              _c(
                                                "v-card-text",
                                                {
                                                  staticClass: "grey lighten-3"
                                                },
                                                [
                                                  _c(
                                                    "v-container",
                                                    {
                                                      staticClass: "pa-0",
                                                      attrs: { fluid: "" }
                                                    },
                                                    [
                                                      _c(
                                                        "v-row",
                                                        [
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "2"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    value: _vm._f(
                                                                      "formatDateHour"
                                                                    )(
                                                                      estudio
                                                                        .muestra
                                                                        .solicitud
                                                                        .fecha_recepcion
                                                                    ),
                                                                    label:
                                                                      "Fecha/Hora de recepción",
                                                                    readonly: ""
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "2"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    value: estudio
                                                                      .muestra
                                                                      .solicitud
                                                                      .es_convenio
                                                                      ? "Si"
                                                                      : "No",
                                                                    label:
                                                                      "Convenio",
                                                                    readonly: ""
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "2"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    value: estudio
                                                                      .muestra
                                                                      .es_subrogado
                                                                      ? "Si"
                                                                      : "No",
                                                                    label:
                                                                      "Subrogado",
                                                                    readonly: ""
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "2"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    value: _vm._f(
                                                                      "formatDateHour"
                                                                    )(
                                                                      estudio
                                                                        .muestra
                                                                        .fecha_muestreo
                                                                    ),
                                                                    label:
                                                                      "Fecha/Hora de muestreo",
                                                                    readonly: ""
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          ),
                                                          _vm._v(" "),
                                                          _c(
                                                            "v-col",
                                                            {
                                                              attrs: {
                                                                cols: "4"
                                                              }
                                                            },
                                                            [
                                                              _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    value:
                                                                      estudio
                                                                        .muestra
                                                                        .descripcion_muestra,
                                                                    label:
                                                                      "Descripción de la muestra",
                                                                    readonly: ""
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "v-row",
                                                        [
                                                          _c(
                                                            "v-col",
                                                            {
                                                              staticClass:
                                                                "pt-0"
                                                            },
                                                            [
                                                              _c(
                                                                "v-text-field",
                                                                {
                                                                  attrs: {
                                                                    value:
                                                                      estudio
                                                                        .muestra
                                                                        .observaciones,
                                                                    label:
                                                                      "Observaciones",
                                                                    readonly: ""
                                                                  }
                                                                }
                                                              )
                                                            ],
                                                            1
                                                          )
                                                        ],
                                                        1
                                                      )
                                                    ],
                                                    1
                                                  )
                                                ],
                                                1
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "v-card-text",
                                                [
                                                  _c("resultados-form", {
                                                    attrs: {
                                                      "populate-with": estudio
                                                    }
                                                  })
                                                ],
                                                1
                                              )
                                            ],
                                            1
                                          )
                                        ]
                                      })
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c("v-divider"),
                                  _vm._v(" "),
                                  _c(
                                    "v-card-actions",
                                    [_c("resultado-global-form")],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/sanitaria/EstatusMuestra.vue":
/*!**************************************************************!*\
  !*** ./resources/js/components/sanitaria/EstatusMuestra.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EstatusMuestra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EstatusMuestra.vue?vue&type=script&lang=js& */ "./resources/js/components/sanitaria/EstatusMuestra.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");
var render, staticRenderFns




/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_1__["default"])(
  _EstatusMuestra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"],
  render,
  staticRenderFns,
  false,
  null,
  "b1311a88",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/sanitaria/EstatusMuestra.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/sanitaria/EstatusMuestra.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/sanitaria/EstatusMuestra.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EstatusMuestra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./EstatusMuestra.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sanitaria/EstatusMuestra.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_EstatusMuestra_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/sanitaria/FechaAlerta.vue":
/*!***********************************************************!*\
  !*** ./resources/js/components/sanitaria/FechaAlerta.vue ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _FechaAlerta_vue_vue_type_template_id_19f3d28c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true& */ "./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true&");
/* harmony import */ var _FechaAlerta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./FechaAlerta.vue?vue&type=script&lang=js& */ "./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _FechaAlerta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _FechaAlerta_vue_vue_type_template_id_19f3d28c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _FechaAlerta_vue_vue_type_template_id_19f3d28c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "19f3d28c",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/sanitaria/FechaAlerta.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=script&lang=js&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=script&lang=js& ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FechaAlerta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./FechaAlerta.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_FechaAlerta_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true&":
/*!******************************************************************************************************!*\
  !*** ./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true& ***!
  \******************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FechaAlerta_vue_vue_type_template_id_19f3d28c_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/sanitaria/FechaAlerta.vue?vue&type=template&id=19f3d28c&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FechaAlerta_vue_vue_type_template_id_19f3d28c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_FechaAlerta_vue_vue_type_template_id_19f3d28c_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue":
/*!******************************************************************!*\
  !*** ./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HojaDeTrabajo_vue_vue_type_template_id_de1cd3da_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true& */ "./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true&");
/* harmony import */ var _HojaDeTrabajo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HojaDeTrabajo.vue?vue&type=script&lang=js& */ "./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _HojaDeTrabajo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HojaDeTrabajo_vue_vue_type_template_id_de1cd3da_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HojaDeTrabajo_vue_vue_type_template_id_de1cd3da_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  "de1cd3da",
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HojaDeTrabajo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/babel-loader/lib??ref--4-0!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HojaDeTrabajo.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HojaDeTrabajo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true&":
/*!*************************************************************************************************************!*\
  !*** ./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true& ***!
  \*************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HojaDeTrabajo_vue_vue_type_template_id_de1cd3da_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../../node_modules/vue-loader/lib??vue-loader-options!./HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/views/sanitaria/analistas/HojaDeTrabajo.vue?vue&type=template&id=de1cd3da&scoped=true&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HojaDeTrabajo_vue_vue_type_template_id_de1cd3da_scoped_true___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HojaDeTrabajo_vue_vue_type_template_id_de1cd3da_scoped_true___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);