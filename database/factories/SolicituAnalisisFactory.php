<?php

/** @var Factory $factory */

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use RiesgosSanitarios\Models\Origen;
use RiesgosSanitarios\Models\Solicitud;

$factory->define(Solicitud::class, function (Faker $faker) {
    $carbon = Carbon::now();
    $carbon->addMinute();
    $carbon->addSecond();

    return [
        'year'               => $carbon->year,
        'numero'             => 0,
        'folio'              => "{$carbon->format('y')}{$carbon->format('m')}{$carbon->format('d')}{$carbon->hour}{$carbon->format('u')}{$carbon->second}",
        'fecha_recepcion'    => $faker->dateTimeBetween('now', '+30 days'),
        'origen_id'          => Origen::query()->inRandomOrder()->value('id'),
        'pantalla_recepcion' => Solicitud::getPantallas()[0],
        'terminado'          => rand(0, 1),
    ];
});
