<?php

/** @var Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

$factory->define(MuestraSanitariaEstudio::class, function (Faker $faker) {

    return [
        'estudio_id'        => \RiesgosSanitarios\Models\Paquete::query()->inRandomOrder()->value('id'),
        'unidad_id'         => \RiesgosSanitarios\Models\Unidad::query()->inRandomOrder()->value('id'),
        'norma'             => 'XXXXX',
        'limite_permisible' => '1',
        'orden_impresion'   => '0',
        'fecha_analisis'    => NULL,
        'fecha_resultados'  => NULL,
        'resultados'        => NULL,
        'observaciones'     => NULL,
    ];
});
