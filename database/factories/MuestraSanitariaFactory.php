<?php

/** @var Factory $factory */

use App\Models\Revision;
use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;
use RiesgosSanitarios\Models\MuestraSanitaria;
use RiesgosSanitarios\Models\NumeroMuestra;

$factory->define(MuestraSanitaria::class, function (Faker $faker) {
    return [
        'es_subrogado'        => false,
        'year'                => \Carbon\Carbon::now()->year,
        'numero_muestra'      => NumeroMuestra::getNumeroMuestra(),
        'recibio_id'          => 1,
        'entrego'             => $faker->name(),
        'fecha_muestreo'      => \Carbon\Carbon::now(),
        'marca_comercial'     => $faker->company,
        'descripcion_muestra' => $faker->text(140),
        'recipiente_id'       => \RiesgosSanitarios\Models\Recipiente::query()->inRandomOrder()->value('id'),
        'temperatura'         => 1,
        'peso'                => 1,
        'volumen'             => 1,
        'lote'                => 1,
        'punto_muestreo'      => 'punto_muestreo',
        'cve_ent'             => '23',
        'cve_mun'             => '001',
        'cve_loc'             => '0001',
        'nombre_entidad'      => '',
        'nombre_municipio'    => '',
        'nombre_localidad'  => '',
        'numero_acta'       => 1,
        'tipo_muestreo'     => array( 'GENERAL' ),
        'caducidad'         => \Carbon\Carbon::now(),
        'envasado_por'      => $faker->company,
        'lugar_muestreo'    => 'lugar_muestreo',
        'ph'                => '1',
        'cloro_residual'    => '1',
        'paciente_id'       => NULL,
        'subcategoria_id'   => \RiesgosSanitarios\Models\Subcategoria::query()->inRandomOrder()->value('id'),
        'paquete_id'        => 9,
        'observaciones'     => '',
        'jurisdiccion'      => '01',
        'tamano_lote'       => '1',
        'fecha_recoleccion' => \Carbon\Carbon::now(),
    ];
});

$factory->afterCreating(MuestraSanitaria::class, function ($m, $faker) {
    $revision = new Revision([
        'fecha_apertura'      => Carbon::now(),
        'usuario_apertura_id' => \App\User::find(1)->id,
        'estatus'             => 'PENDIENTE',
    ]);

    $m->statuses()->save($revision);

    NumeroMuestra::query()
        ->where('year', Carbon::now()->year)
        ->update([ 'numero_muestra' => $m->numero_muestra ]);
});
