<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $this->call(RolesTableSeeder::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AnalistasTableSeeder::class);
        $this->call(GenerosTableSeeder::class);
        $this->call(TiposMuestreoTableSeeder::class);
        $this->call(SistemasTableSeeder::class);
        $this->call(AccesosMenuTableSeeder::class);
        $this->call(RoleSistemaTableSeeder::class);
        $this->call(CluesTableSeeder::class);
        Schema::enableForeignKeyConstraints();

        $files = [
            'database/queries/laboratorio_estatal_laboratorios.sql',
            'database/queries/laboratorio_estatal_limites_permisibles.sql',
            'database/queries/laboratorio_estatal_categorias.sql',
            'database/queries/laboratorio_estatal_subcategorias.sql',
            'database/queries/laboratorio_estatal_estudios.sql',
            'database/queries/laboratorio_estatal_unidades.sql',
            'database/queries/laboratorio_estatal_paquetes.sql',
            'database/queries/laboratorio_estatal_paquete_estudio.sql',
            'database/queries/laboratorio_estatal_recipientes.sql',
            'database/queries/laboratorio_estatal_origenes.sql',
            'database/queries/laboratorio_estatal_domicilios.sql',
            'database/queries/laboratorio_estatal_pacientes.sql',
            'database/queries/diagnosticos.sql',
        ];

        Eloquent::unguard();

        $this->command->info('Ejecutando scripts... espere un momento...');

        foreach ($files as $file) {
            $this->command->info("Seeding: {$file}");
            DB::unprepared(file_get_contents($file));
            $this->command->info("El arhivo {$file} se ejecutó correctamente");
        };
    }
}
