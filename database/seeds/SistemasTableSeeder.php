<?php

use App\Models\Sistema;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SistemasTableSeeder extends Seeder
{
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('sistemas')->truncate();
        $items = [
            [
                "id"        => 1,
                "picture"   => "inventarios.jpeg",
                "titulo"    => "Gestión administrativa",
                "subtitulo" => "Sistema de Inventarios",
                "route"     => "/app/inventarios",
            ],
            [
                "id"        => 2,
                "picture"   => "epidemiologia.jpg",
                "titulo"    => "Epidemiología",
                "subtitulo" => "Epidemiología",
                "route"     => "/app/epidemiologia",
            ],
            [
                "id"        => 3,
                "picture"   => "riesgos_sanitarios_1.jpg",
                "titulo"    => "Riesgos Sanitarios",
                "subtitulo" => "Riesgos Sanitarios",
                "route"     => "/app/riesgos-sanitarios",
            ],
            [
                "id"        => 4,
                "picture"   => "administracion.png",
                "titulo"    => "Administración",
                "subtitulo" => "Administración",
                "route"     => "/app/administracion",
            ],
            [
                "id"        => 5,
                "picture"   => "calidad.jpg",
                "titulo"    => "Gestión de la calidad",
                "subtitulo" => "Gestión de calidad",
                "route"     => "/app/calidad",
            ],
        ];

        Sistema::insert($items);
    }
}
