<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class CalidadDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $this->call(ClasificacionesNCTableSeeder::class);
        $this->call(FuentesNCTableSeeder::class);
        $this->call(DepartamentosTableSeeder::class);

        Schema::enableForeignKeyConstraints();
    }
}
