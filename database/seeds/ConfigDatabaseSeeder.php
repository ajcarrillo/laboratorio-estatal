<?php

use Illuminate\Database\Seeder;

class ConfigDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        $this->dropTables();

        $this->command->info('Las tablas se truncaron correctamente');

        $this->call(RolesTableSeeder::class);
        $this->call(SistemasTableSeeder::class);
        $this->call(AccesosMenuTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(AnalistasTableSeeder::class);
        $this->call(RoleSistemaTableSeeder::class);

        Schema::enableForeignKeyConstraints();
    }

    protected function dropTables()
    {
        $tables = [
            'sistema_menus',
            'sistemas',
            'roles',
            'users',
            'role_sistema',
        ];

        foreach ($tables as $table) {
            DB::table($table)->truncate();
        }
    }
}
