<?php

use App\Models\Domicilio;
use Illuminate\Database\Seeder;
use RiesgosSanitarios\Models\Origen;

class OrigenDomicilioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $exports = DB::table('origenes_exports')
            ->orderBy('origen')
            ->get();

        $origenes = Origen::query()
            ->orderBy('origen')
            ->get();

        $i = 0;
        foreach ($origenes as $origen) {
            $domicilio = new Domicilio([
                'direccion'     => $exports[ $i ]->Direccion == NULL ? 'CONOCIDA' : $exports[ $i ]->Direccion,
                'codigo_postal' => $exports[ $i ]->CP,
            ]);

            $origen->domicilio()->save($domicilio);
            $i = $i + 1;
        }
    }
}
