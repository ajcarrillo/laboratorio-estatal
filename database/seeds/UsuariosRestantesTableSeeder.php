<?php

use App\User;
use Illuminate\Database\Seeder;

class UsuariosRestantesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usuarios = [
            [ 'titulo' => 'C.', 'name' => 'ANA LUISA', 'last_name' => 'MENDEZ LOZANO' ],
            [ 'titulo' => 'C.', 'name' => 'ANDY', 'last_name' => 'PACHECHO PUC' ],
            [ 'titulo' => 'TLC.', 'name' => 'ANTONIA', 'last_name' => 'MORENO LOPEZ' ],
            [ 'titulo' => 'QFB.', 'name' => 'ARIANNE RUBI', 'last_name' => 'TORRES CARDOSO' ],
            [ 'titulo' => 'TLC.', 'name' => 'EMANUEL', 'last_name' => 'GOMEZ JARAMILLO' ],
            [ 'titulo' => 'BR.', 'name' => 'FABIOLA', 'last_name' => 'LEON LANDERO' ],
            [ 'titulo' => 'ENFCIT.', 'name' => 'FERNANDO', 'last_name' => 'GONGORA CAN' ],
            [ 'titulo' => 'DR.', 'name' => 'FRANCISCO DE JESUS', 'last_name' => 'RIVERA AGUILAR' ],
            [ 'titulo' => 'BIOL.', 'name' => 'GLADEYZA PAOLA', 'last_name' => 'CEBALLOS VILLAFAÑA' ],
            [ 'titulo' => 'TLC.', 'name' => 'GUADALUPE', 'last_name' => 'ESTRELLA MORALES' ],
            [ 'titulo' => 'TLC.', 'name' => 'IRLA YARITZA', 'last_name' => 'CAMBRANIS GOMEZ' ],
            [ 'titulo' => 'TI.', 'name' => 'IRMA GUADALUPE', 'last_name' => 'ESTRADA LLAMAS' ],
            [ 'titulo' => 'QFB.', 'name' => 'LENNY DE JESUS', 'last_name' => 'GUSMAN CANUL' ],
            [ 'titulo' => 'BR.', 'name' => 'MARIA GUADALUPE', 'last_name' => 'ESCALANTE FRANCO' ],
            [ 'titulo' => 'TAE.', 'name' => 'MARICELA', 'last_name' => 'LOPEZ LEAL' ],
            [ 'titulo' => 'BR.', 'name' => 'NORMA MARISOL', 'last_name' => 'AGUILAR CEN' ],
            [ 'titulo' => 'LEF.', 'name' => 'ROGER GIOVANNI', 'last_name' => 'TEC  AZUETA' ],
            [ 'titulo' => 'BR.', 'name' => 'SILVIA GEANINA', 'last_name' => 'GARCIA SALA' ],
            [ 'titulo' => 'BR.', 'name' => 'SUSANA YAZMIN', 'last_name' => 'ORTEGON BLANCO' ],
            [ 'titulo' => 'QBP.', 'name' => 'VIANEY', 'last_name' => 'VINALAY GONZALEZ' ],

            [ 'titulo' => 'TL.', 'name' => 'ADDY MARGELY', 'last_name' => 'RAMÍREZ POOL' ],
            [ 'titulo' => 'BIÓL.', 'name' => 'ALEJANDRINA', 'last_name' => 'DUARTE AC' ],
            [ 'titulo' => 'QBB.', 'name' => 'ANDRÉS ABRAHAM', 'last_name' => 'CHAY VARGAS' ],
            [ 'titulo' => 'TL.', 'name' => 'ÁNGEL EDUARDO', 'last_name' => 'BENÍTEZ POOT' ],
            [ 'titulo' => 'IA.', 'name' => 'ANÍBAL ADÁN', 'last_name' => 'BRAVO MEDRANO' ],
            [ 'titulo' => 'TL.', 'name' => 'AYCELA ANILU', 'last_name' => 'YAA OSORIO' ],
            [ 'titulo' => 'TL.', 'name' => 'CARLOS ABRAHAM', 'last_name' => 'POOT OCEJO' ],
            [ 'titulo' => 'BIÓL.', 'name' => 'CARLOS FERNANDO', 'last_name' => 'CHABLE MENDICUTI' ],
            [ 'titulo' => 'QFB.', 'name' => 'CINTHIA GUADALUPE', 'last_name' => 'PECH CHIM' ],
            [ 'titulo' => 'TL.', 'name' => 'DAVID', 'last_name' => 'AGUILAR AYALA' ],
            [ 'titulo' => 'QFB.', 'name' => 'DIANA ZULEIKA', 'last_name' => 'CONCHA MEDINA' ],
            [ 'titulo' => 'TL.', 'name' => 'EDUARDO RAMBEL', 'last_name' => 'PECH CHULIM' ],
            [ 'titulo' => 'BIÓL.', 'name' => 'ERIKA ADRIANA', 'last_name' => 'ALAMILLA HOYOS' ],
            [ 'titulo' => 'QA.', 'name' => 'GEOVANI', 'last_name' => 'RAMÍREZ HERNÁNDEZ' ],
            [ 'titulo' => 'BIÓL.', 'name' => 'GREGORIO', 'last_name' => 'POOT MEX' ],
            [ 'titulo' => 'QFB.', 'name' => 'IRWIN ERNESTO', 'last_name' => 'RODRÍGUEZ POXTAN' ],
            [ 'titulo' => 'BIÓL.', 'name' => 'JORGE LUIS', 'last_name' => 'OVANDO GÁMEZ' ],
            [ 'titulo' => 'QFB.', 'name' => 'LINNET ESMERALDA', 'last_name' => 'CUPUL NOH' ],
            [ 'titulo' => 'QFB.', 'name' => 'MARÍA DE LOURDES', 'last_name' => 'GARCÍA COLÍN' ],
            [ 'titulo' => 'BIÓL.', 'name' => 'MARICRUZ', 'last_name' => 'MORENO CÁRDENAS' ],
            [ 'titulo' => 'TC.', 'name' => 'MAYRA MAGDALENA', 'last_name' => 'MARTÍNEZ MUÑOS' ],
            [ 'titulo' => 'QFB.', 'name' => 'NANCY NORA', 'last_name' => 'ISTE MARTÍNEZ' ],
            [ 'titulo' => 'IBQ.', 'name' => 'RAÚL ALBERTO', 'last_name' => 'MEDINA ESQUILIANO' ],
            [ 'titulo' => 'QFB.', 'name' => 'VERÓNICA', 'last_name' => 'OVIEDO ARREDONDO' ],
        ];

        foreach ($usuarios as $usuario) {
            factory(User::class, 1)->create($usuario);
        }
    }
}
