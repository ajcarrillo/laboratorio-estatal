<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create([
            'titulo'           => 'LIC.',
            'name'             => 'ANDRÉS',
            'last_name'        => 'CARRILLO HERRERA',
            'nombre_apellidos' => 'ANDRÉS CARRILLO HERRERA',
            'apellidos_nombre' => 'CARRILLO HERRERA ANDRÉS',
            'email'            => 'andresjch2804@gmail.com',
            'api_token'        => Str::random(60),
            'password'         => '$2y$10$QOaYZDf2pE3D5Zwm3OP.r.HVv/5c4VFG0eWfmPijbskTKewOMg6ym',
        ])->each(function ($u) {
            $u->assignRole('SUPER');
            $u->assignRole('SYSADMIN');
        });

        factory(User::class, 1)->create([
            'titulo'           => 'LIC.',
            'name'             => 'NARCISO',
            'last_name'        => 'COCOM EK',
            'nombre_apellidos' => 'NARCISO COCOM EK',
            'apellidos_nombre' => 'COCOM EK NARCISO',
            'email'            => 'lesp_Inf@hotmail.com',
            'api_token'        => Str::random(60),
            'password'         => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        ])->each(function ($u) {
            $u->assignRole('SYSADMIN');
        });

        factory(User::class, 1)->create([
            'titulo'           => 'C.',
            'name'             => 'RODOLFO',
            'last_name'        => 'DZUL CETINA',
            'nombre_apellidos' => 'RODOLFO DZUL CETINA',
            'apellidos_nombre' => 'DZUL CETINA RODOLFO',
            'email'            => 'rafael.cetina@sispro.mx',
            'api_token'        => Str::random(60),
            'password'         => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
        ])->each(function ($u) {
            $u->assignRole('SUPER');
            $u->assignRole('CALIDAD');
            $u->assignRole('INVENTARIOS');
            $u->assignRole('SYSADMIN');
        });
    }
}
