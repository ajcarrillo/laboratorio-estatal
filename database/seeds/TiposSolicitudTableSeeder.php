<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\TiposSolicitud;

class TiposSolicitudTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_solicitud')->truncate();
        $items = [
            [
                'descripcion' => 'SOLICITUD DE ABASTECIMIENTO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'descripcion' => 'ORDEN DE COMPRA',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];
        TiposSolicitud::insert($items);
    }
}
