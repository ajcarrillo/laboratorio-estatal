<?php

use Calidad\Models\FuenteNC;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FuentesNCTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('fuentes_nc')->truncate();
        $items = [
            [
                'clave' => 'AI',
                'descripcion' => 'AUDITORÍA INTERNA',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'AE',
                'descripcion' => 'AUDITORÍA EXTERNA',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'S',
                'descripcion' => 'SUPERVISIÓN',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'Q',
                'descripcion' => 'QUEJA',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'RD',
                'descripcion' => 'REVISIÓN POR LA DIRECCIÓN',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];

        FuenteNC::insert($items);
    }
}
