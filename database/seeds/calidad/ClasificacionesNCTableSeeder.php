<?php

use Calidad\Models\ClasificacionNC;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClasificacionesNCTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('clasificaciones_nc')->truncate();
        $items = [
            [
                'clave' => 'NC',
                'descripcion' => 'NO CONFORMIDAD',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'SNC',
                'descripcion' => 'SALIDA NO CONFORME',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'NCP',
                'descripcion' => 'NO CONFORMIDAD POTENCIAL',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'M',
                'descripcion' => 'MEJORA',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];

        ClasificacionNC::insert($items);
    }
}
