<?php

use Calidad\Models\Departamento;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartamentosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('departamentos')->truncate();
        $items = [
            [
                'clave' => 'DL',
                'descripcion' => 'DIRECCIÓN DE LABORATORIO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'SUD',
                'descripcion' => 'SUBDIRECCIÓN TÉCNICA',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'DAD',
                'descripcion' => 'DEPARTAMENTO DE ADMINISTRACIÓN',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clave' => 'DGC',
                'descripcion' => 'DEPARTAMENTO DE GESTIÓN DE CALIDAD',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];

        Departamento::insert($items);
    }
}
