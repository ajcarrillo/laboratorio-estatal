<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class InventariosDatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        $this->call(AreasTableSeeder::class);
        $this->call(ArticulosTableSeeder::class);
        $this->call(EstadosEntradaTableSeeder::class);
        $this->call(EstadosTransferenciaTableSeeder::class);
        $this->call(TiposEntradaTableSeeder::class);
        $this->call(TiposTransferenciaTableSeeder::class);
        $this->call(TiposInsumoTableSeeder::class);
        $this->call(ProveedoresTableSeeder::class);
        $this->call(TiposSolicitudTableSeeder::class);
        $this->call(EstadosSolicitudTableSeeder::class);
        Schema::enableForeignKeyConstraints();
    }
}
