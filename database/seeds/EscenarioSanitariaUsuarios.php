<?php

use Illuminate\Database\Seeder;

class EscenarioSanitariaUsuarios extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Analista
        factory(\App\User::class, 1)->create([
            'email'    => 'analistaF@analista.com',
            'password' => Hash::make('password'),
        ])
            ->each(function ($u) {
                $labFisico = \App\Models\Laboratorio::find(1);
                $u->assignRole('ANALISTA SANITARIA');

                $u->Analistas()->attach($labFisico->id, [
                    'inicio' => \Carbon\Carbon::now(),
                    'activo' => true,
                ]);
            });

        factory(\App\User::class, 1)->create([
            'email'    => 'analistaM@analista.com',
            'password' => Hash::make('password'),
        ])
            ->each(function ($u) {
                $labFisico = \App\Models\Laboratorio::find(2);
                $u->assignRole('ANALISTA SANITARIA');

                $u->Analistas()->attach($labFisico->id, [
                    'inicio' => \Carbon\Carbon::now(),
                    'activo' => true,
                ]);
            });

        //Responsable
        factory(\App\User::class, 1)->create([
            'email'    => 'responsableF@responsable.com',
            'password' => Hash::make('password'),
        ])
            ->each(function ($u) {
                $labFisico = \App\Models\Laboratorio::find(1);
                $u->assignRole('RESPONSABLE LABORATORIO SANITARIA');

                $u->Laboratorios()->attach($labFisico->id, [
                    'fecha_inicio' => \Carbon\Carbon::now(),
                    'activo'       => true,
                ]);
            });

        factory(\App\User::class, 1)->create([
            'email'    => 'responsableM@responsable.com',
            'password' => Hash::make('password'),
        ])
            ->each(function ($u) {
                $labFisico = \App\Models\Laboratorio::find(2);
                $u->assignRole('RESPONSABLE LABORATORIO SANITARIA');

                $u->Laboratorios()->attach($labFisico->id, [
                    'fecha_inicio' => \Carbon\Carbon::now(),
                    'activo'       => true,
                ]);
            });

        //Jefe sanitaria
        factory(\App\User::class, 1)->create([
            'email'    => 'jefe@jefe.com',
            'password' => Hash::make('password'),
        ])
            ->each(function ($u) {
                $u->assignRole('JEFE DEPARTAMENTO SANITARIA');
            });
    }
}
