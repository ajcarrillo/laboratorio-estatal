<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            'ANALISTA EPIDEMIOLOGIA',
            'ANALISTA SANITARIA',
            'CAPTURISTA EPIDEMIOLOGIA',
            'CAPTURISTA SANITARIA',
            'EPIDEMIOLOGO ESTATAL',
            'EPIDEMIOLOGO JURISDICCIONAL',
            'INVENTARIOS',
            'JEFE DEPARTAMENTO EPIDEMIOLOGIA',
            'JEFE DEPARTAMENTO SANITARIA',
            'RESPONSABLE LABORATORIO EPIDEMIOLOGIA',
            'RESPONSABLE LABORATORIO SANITARIA',
            'SUPER',
            'SYSADMIN',
            'CALIDAD',
        ];

        foreach ($roles as $role) {
            Role::create([ 'name' => $role ]);
        }
    }
}
