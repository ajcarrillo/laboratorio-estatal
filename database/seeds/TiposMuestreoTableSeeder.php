<?php

use Illuminate\Database\Seeder;
use RiesgosSanitarios\Models\TipoMuestreo;

class TiposMuestreoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TipoMuestreo::create([ 'descripcion' => 'ORDINARIO' ]);
        TipoMuestreo::create([ 'descripcion' => 'POR DENUNCIA' ]);
        TipoMuestreo::create([ 'descripcion' => 'BROTE' ]);
        TipoMuestreo::create([ 'descripcion' => 'EXTRAORDINARIO' ]);
        TipoMuestreo::create([ 'descripcion' => 'OPERATIVO' ]);
        TipoMuestreo::create([ 'descripcion' => 'SEGUIMIENTO' ]);
        TipoMuestreo::create([ 'descripcion' => 'QUEJA SANITARIA' ]);
        TipoMuestreo::create([ 'descripcion' => 'URGENTE' ]);
        TipoMuestreo::create([ 'descripcion' => 'CONTINGENCIA' ]);
        TipoMuestreo::create([ 'descripcion' => 'PRIORIZADO' ]);
        TipoMuestreo::create([ 'descripcion' => 'PROGRAMA DE CUARESMA' ]);
        TipoMuestreo::create([ 'descripcion' => 'ASEGURAMIENTO' ]);
        TipoMuestreo::create([ 'descripcion' => 'INTOXICACIÓN' ]);
        TipoMuestreo::create([ 'descripcion' => 'OTRO' ]);
    }
}
