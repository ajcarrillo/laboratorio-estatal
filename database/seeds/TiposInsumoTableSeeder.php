<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\TiposInsumo;

class TiposInsumoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_insumo')->truncate();
        $items = [
            [
                'clave' => 'REA',
                'descripcion' => 'REACTIVOS',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];
        TiposInsumo::insert($items);
    }
}
