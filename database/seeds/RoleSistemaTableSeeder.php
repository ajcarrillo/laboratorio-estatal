<?php

use Illuminate\Database\Seeder;

class RoleSistemaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'sistema_id' => 1,
                'roles'      => [ 'INVENTARIOS' ],
            ],
            [
                'sistema_id' => 2,
                'roles'      => [
                    'ANALISTA EPIDEMIOLOGIA',
                    'CAPTURISTA EPIDEMIOLOGIA',
                    'EPIDEMIOLOGO ESTATAL',
                    'EPIDEMIOLOGO JURISDICCIONAL',
                    'JEFE DEPARTAMENTO EPIDEMIOLOGIA',
                    'RESPONSABLE LABORATORIO EPIDEMIOLOGIA',
                ],
            ],
            [
                'sistema_id' => 3,
                'roles'      => [
                    'CAPTURISTA SANITARIA',
                    'JEFE DEPARTAMENTO SANITARIA',
                    'RESPONSABLE LABORATORIO SANITARIA',
                    'ANALISTA SANITARIA',
                ],
            ],
            [
                'sistema_id' => 4,
                'roles'      => [
                    'SYSADMIN',
                    'SUPER',
                ],
            ],
            [
                'sistema_id' => 5,
                'roles'      => [
                    'SYSADMIN',
                    'SUPER',
                    'CALIDAD',
                ],
            ],
        ];

        foreach ($data as $item) {
            $s = \App\Models\Sistema::query()
                ->where('id', $item['sistema_id'])
                ->first();

            $r = \Spatie\Permission\Models\Role::query()
                ->whereIn('name', $item['roles'])
                ->pluck('id');

            $s->roles()->sync($r);
        }
    }
}
