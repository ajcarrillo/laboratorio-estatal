<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('permissions')
            ->truncate();
        Schema::enableForeignKeyConstraints();

        $permissions = [
            'add solicitudes',
            'edit solicitudes',
        ];

        foreach ($permissions as $permission) {
            Permission::create([ 'name' => $permission ]);
        }

        $role = Role::query()
            ->where('name', 'CAPTURISTA SANITARIA')
            ->first();

        $role->givePermissionTo('add solicitudes');
        $role->givePermissionTo('edit solicitudes');
    }
}
