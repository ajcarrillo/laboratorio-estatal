<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\Area;
class AreasTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('areas')->truncate();
        $items = [
            [
                'clues_id' => 21511,
                'nombre' => 'LABORATORIO ESTATAL',
                'tipo' => 'LABORATORIO',
                'comportamiento' => 'LABORATORIO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clues_id' => 21511,
                'nombre' => 'ALMACÉN LABORATORIO ESTATAL',
                'tipo' => 'ALMACEN',
                'comportamiento' => 'CEDIS',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clues_id' => 21511,
                'nombre' => 'LABORATORIO ESTATAL PLAYA',
                'tipo' => 'LABORATORIO',
                'comportamiento' => 'LABORATORIO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'clues_id' => 21511,
                'nombre' => 'ALMACÉN LABORATORIO ESTATAL PLAYA',
                'tipo' => 'ALMACEN',
                'comportamiento' => 'CEDIS',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];

        Area::insert($items);
    }
}
