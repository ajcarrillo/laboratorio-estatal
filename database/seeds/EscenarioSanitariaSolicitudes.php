<?php

use App\Models\Revision;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use RiesgosSanitarios\Models\MuestraSanitaria;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;
use RiesgosSanitarios\Models\Paquete;
use RiesgosSanitarios\Models\Solicitud;

class EscenarioSanitariaSolicitudes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MuestraSanitariaEstudio::flushEventListeners();
        factory(Solicitud::class, 1)
            ->create([ 'terminado' => 1 ])
            ->each(function ($s) {
                $m = factory(MuestraSanitaria::class)
                    ->make();

                $s->muestras()->save($m);

                factory(MuestraSanitaria::class)->callAfterCreating($m);

                $paquete = Paquete::query()->where('id', $m->paquete_id)->first();

                $estudios = $paquete->estudios;

                foreach ($estudios as $estudio) {
                    $e = factory(MuestraSanitariaEstudio::class)
                        ->make([
                            'estudio_id' => $estudio->id,
                            'unidad_id'  => $estudio->pivot->unidad_id,
                        ]);

                    $m->estudios()->save($e);

                    $revision = new Revision([
                        'fecha_apertura'      => Carbon::now(),
                        'usuario_apertura_id' => User::find(1)->id,
                        'estatus'             => 'PENDIENTE',
                    ]);

                    $e->statuses()->save($revision);
                }
            });
    }
}
