<?php

use Illuminate\Database\Seeder;

class GenerosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Genero::query()->create([ 'descripcion' => 'MASCULINO' ]);
        \App\Models\Genero::query()->create([ 'descripcion' => 'FEMENINO' ]);
        \App\Models\Genero::query()->create([ 'descripcion' => 'NO DEFINIDO' ]);
    }
}
