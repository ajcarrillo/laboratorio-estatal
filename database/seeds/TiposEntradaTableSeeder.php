<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\TiposEntrada;

class TiposEntradaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_entrada')->truncate();
        $items = [
            [
                'descripcion' => 'ENTRADA AL ALMACÉN',
                'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            ],
            [
                'descripcion' => 'ORDEN COMPRA',
                'created_at'  => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at'  => Carbon::now()->format('Y-m-d H:i:s'),
            ],
        ];
        TiposEntrada::insert($items);
    }
}
