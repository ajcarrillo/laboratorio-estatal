<?php

use Illuminate\Database\Seeder;

class CluesTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $file = 'database/queries/clues2.sql';

        Eloquent::unguard();

        $this->command->info('Ejecutando scripts... espere un momento...');

        DB::unprepared(file_get_contents($file));
        $this->command->info("El arhivo {$file} se ejecutó correctamente");

    }
}
