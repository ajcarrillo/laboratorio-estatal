<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\TiposTransferencia;

class TiposTransferenciaTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('tipos_transferencia')->truncate();
        $items = [
            [
                'descripcion' => 'TRANSFERENCIA A ALMACEN',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'descripcion' => 'DESCARGA DE CONSUMO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];
        TiposTransferencia::insert($items);
    }
}
