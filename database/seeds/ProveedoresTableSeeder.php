<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\Proveedor;

class ProveedoresTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('proveedores')->truncate();
        $items = [
            [
                'rfc' => 'DUCR950319E89',
                'razon_social' => 'LABORATORIOS PATITO SA DE CV',
                'nombre_comercial' => 'LABORATORIOS PATITO',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];
        Proveedor::insert($items);
    }
}
