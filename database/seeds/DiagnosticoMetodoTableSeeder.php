<?php

use Illuminate\Database\Seeder;

class DiagnosticoMetodoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $files = [
            'database/queries/laboratorio_estatal_cat_diagnosticos.sql',
            'database/queries/laboratorio_estatal_metodos_referencia.sql',
        ];

        $this->command->info('Ejecutando scripts... espere un momento...');

        Schema::disableForeignKeyConstraints();

        foreach ($files as $file) {
            $this->command->info("Seeding: {$file}");
            DB::unprepared(file_get_contents($file));
            $this->command->info("El arhivo {$file} se ejecutó correctamente");
        }

        Schema::enableForeignKeyConstraints();
    }
}
