<?php

use App\User;
use Illuminate\Database\Seeder;

class AnalistasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $analistas = [
            [
                'titulo'    => 'QFB.',
                'name'      => 'DAGOBERTO ROMAN',
                'last_name' => 'CANUL AGUILAR',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'IRLANDA DEL CARMEN',
                'last_name' => 'CASTILLO RODRIGUEZ',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'FERNANDO',
                'last_name' => 'ORTIZ CAMARA',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'PAMELA',
                'last_name' => 'RAMIREZ MEDINA',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'XINIA YAZMIN',
                'last_name' => 'LOPEZ SOSA',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'MIRIAM CONCEPCION',
                'last_name' => 'MOO EB',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'FELIPE D.',
                'last_name' => 'VAZQUEZ ANCONA',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'HERBERT HERNRY',
                'last_name' => 'LOPEZ GOMEZ',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'MIGUEL ANGEL',
                'last_name' => 'BORGES AC',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'SARA',
                'last_name' => 'BAACK CAAMAL',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'ISAAC',
                'last_name' => 'MUÑOZ MONTERO',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'NELLY',
                'last_name' => 'RUEDA ALVARADO',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'ZULMA VIRGINIA',
                'last_name' => 'GONGORA PECH',
            ],
            [
                'titulo'    => 'TLC.',
                'name'      => 'VIRGINIA DEL ROSARIO',
                'last_name' => 'KU GIL',
            ],
            [
                'titulo'    => 'TL.',
                'name'      => 'SIOMARA GUADALUPE',
                'last_name' => 'TOSTA ALAMILLA',
            ],
            [
                'titulo'    => 'TL.',
                'name'      => 'EYFI YESENIA',
                'last_name' => 'MENESES PADILLA',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'MILDRED JAZMIN',
                'last_name' => 'YELADAQUI SANGUINO',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'EFRAIN MARCELO',
                'last_name' => 'LUNA LEON',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'SERGIO ARMANDO',
                'last_name' => 'MOLINA PEREZ',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'GERARDO ISIDRO',
                'last_name' => 'BALAN MEDINA',
            ],
            [
                'titulo'    => 'MSP.',
                'name'      => 'MIGUEL ANTONIO',
                'last_name' => 'OCAMPO OSORIO',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'VIELKA MARIELA',
                'last_name' => 'TUZ PAREDES',
            ],
            [
                'titulo'    => 'TL.',
                'name'      => 'ELIAS R',
                'last_name' => 'CUPUL NOVELO',
            ],
            [
                'titulo'    => 'BIOL.',
                'name'      => 'JENNY KARINA',
                'last_name' => 'PEREZ CANTO',
            ],
            [
                'titulo'    => 'TLC.',
                'name'      => 'REBECA DE JESUS',
                'last_name' => 'ROBLES HERNADEZ',
            ],
            [
                'titulo'    => 'TLC.',
                'name'      => 'JOAQUIN EDMUNDO',
                'last_name' => 'MAY POOT',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'MARISOL',
                'last_name' => 'GALINDO GALINDO',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'GUADALUPE DEL CARMEN',
                'last_name' => 'HERRERA RAMIREZ',
            ],
            [
                'titulo'    => 'LCTA.',
                'name'      => 'MISAEL',
                'last_name' => 'CANUL PACHECO',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'ARGELIA PATRICIA',
                'last_name' => 'URZUA RODRIGUEZ',
            ],
            [
                'titulo'    => 'TLA.',
                'name'      => 'JOSE BERNALDO',
                'last_name' => 'COCOM YAH',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'LITZIAN VIANEY',
                'last_name' => 'CEME SANTIAGO',
            ],
            [
                'titulo'    => 'QFB.',
                'name'      => 'SILVIA LUCRECIA',
                'last_name' => 'HERNANDEZ RIVERA',
            ],
            [
                'titulo'    => 'TLC.',
                'name'      => 'DIANA LAURA',
                'last_name' => 'VILLANUEVA RUIZ',
            ],
        ];

        foreach ($analistas as $analista) {
            factory(User::class, 1)
                ->create([
                    'name'             => $analista['name'],
                    'last_name'        => $analista['last_name'],
                    'titulo'           => $analista['titulo'],
                    'nombre_apellidos' => "{$analista['name']} {$analista['last_name']}",
                    'apellidos_nombre' => "{$analista['last_name']} {$analista['name']}",
                ])
                ->each(function ($u) {
                    $u->assignRole('ANALISTA EPIDEMIOLOGIA');
                });
        }
    }

}
