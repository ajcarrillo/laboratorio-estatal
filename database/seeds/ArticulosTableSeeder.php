<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Inventarios\Models\CatArticulo;

class ArticulosTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cat_articulos')->truncate();
        $items = [
            [
                'clave' => 'R001',
                'descripcion' => 'REACTIVO DE PRUEBA',
                'unidad_medida' => 'ML',
                'presentacion' => 'TUBO 25 ML',
                'tipo_insumo_id' => 1,
                'lote_obligatorio' => 1,
                'caducidad_obligatoria' => 1,
                'iva_default' => 0,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
        ];
        CatArticulo::insert($items);
    }
}
