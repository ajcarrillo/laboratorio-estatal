<?php

use Illuminate\Database\Seeder;
use RiesgosSanitarios\Models\Solicitud;

class SolicitudAnalisisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Solicitud::class, 350)->create();
    }
}
