<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeOrderImpresionNameFromPaqueteEstudioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paquete_estudio', function (Blueprint $table) {
            $table->renameColumn('order_impresion', 'orden_impresion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquete_estudio', function (Blueprint $table) {
            $table->renameColumn('orden_impresion', 'order_impresion');
        });
    }
}
