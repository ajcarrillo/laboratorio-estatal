<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatCluesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clues', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clues', 11)->index()->nullable();
            $table->string('cve_institucion_salud', 15)->nullable();
            $table->string('cve_corta_inst_salud', 3)->nullable();
            $table->string('nombre_inst_salud', 200)->nullable();
            $table->string('cve_entidad', 2)->nullable();
            $table->string('nombre_entidad', 200)->nullable();
            $table->string('cve_jurisdiccion', 2)->nullable();
            $table->string('nombre_jurisdiccion', 200)->nullable();
            $table->string('cve_municipio', 3)->nullable();
            $table->string('nombre_municipio', 200)->nullable();
            $table->string('cve_localidad', 2)->nullable();
            $table->string('nombre_localidad', 200)->nullable();
            $table->string('cvercve', 2)->nullable();
            $table->string('cinscve', 1)->nullable();
            $table->string('ctuncve', 1)->nullable();
            $table->string('ccomcve', 3)->nullable();
            $table->string('cve_tipo_estab', 2)->nullable();
            $table->string('nombre_tipo_estab', 150)->nullable();
            $table->string('tipologia', 1)->nullable();
            $table->string('cve_tipologia', 3)->nullable();
            $table->string('nombre_tipologia', 100)->nullable();
            $table->string('nombre_unidad', 250)->nullable();
            $table->string('domicilio', 250)->nullable();
            $table->string('colonia', 150)->nullable();
            $table->string('num_interior', 50)->nullable();
            $table->string('num_exterior', 50)->nullable();
            $table->smallInteger('cp')->nullable();
            $table->integer('corta_estancia')->nullable();
            $table->integer('cve_estatus_unidad')->nullable();
            $table->string('estatus_unidad', 50)->nullable();
            $table->string('clave', 2)->nullable();
            $table->string('nivel_atencion', 30)->nullable();
            $table->string('lada_telefono', 20)->nullable();
            $table->string('telefono', 10)->nullable();
            $table->string('correo_electronico', 250)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clues');
    }
}
