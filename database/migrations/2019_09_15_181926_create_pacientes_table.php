<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePacientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pacientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('expediente_id')->nullable()->index();
            $table->string('nombre', 50);
            $table->string('apellidos', 50);
            $table->string('nombre_apellidos', 100);
            $table->string('apellidos_nombre', 100);
            $table->date('fecha_nacimiento')->nullable();
            $table->string('genero', 50);
            $table->foreign('genero')->references('descripcion')->on('generos');
            $table->text('telefono')->nullable();
            $table->string('email')->nullable();
            $table->string('edad', 3)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacientes');
    }
}
