<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTerminadoToSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->boolean('terminado')
                ->default(0)
                ->after('pantalla_recepcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->dropColumn('terminado');
        });
    }
}
