<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiagnosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnosticos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion', 300)->index();
            $table->string('clave', 4)->index()->nullable();
            $table->string('cie_superior', 3)->index()->nullable();
            $table->string('cie_clave_x', 4)->index()->nullable();
            $table->string('cie_categoria', 4)->index()->nullable();
            $table->string('lsex', 50)->nullable();
            $table->string('linf', 50)->nullable();
            $table->string('lsup', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diagnosticos');
    }
}
