<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoMuestraIdToMuestrasEpidemiologicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->unsignedBigInteger('tipo_muestra_id')
                ->after('unidad_medica_id');
            $table->foreign('tipo_muestra_id')->references('id')->on('tipos_muestra');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->dropForeign([ 'tipo_muestra_id' ]);
            $table->dropColumn('tipo_muestra_id');
        });
    }
}
