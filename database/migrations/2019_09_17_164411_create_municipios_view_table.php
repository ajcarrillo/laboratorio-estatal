<?php

use Illuminate\Database\Migrations\Migration;

class CreateMunicipiosViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());

        DB::statement($this->createView());
    }

    private function dropView()
    {
        return "DROP VIEW IF EXISTS `municipios_view`;";
    }

    private function createView()
    {
        return <<< SQL
CREATE VIEW `municipios_view` AS
SELECT cve_ent,
       cve_mun,
       nom_mun
FROM estados_municipios_localidades
GROUP BY cve_ent, cve_mun, nom_mun;
SQL;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropView());
    }
}
