<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddObservacionesToMuestraSanitariaEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestra_sanitaria_estudios', function (Blueprint $table) {
            $table->text('observaciones')
                ->after('resultados')
                ->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestra_sanitaria_estudios', function (Blueprint $table) {
            $table->dropColumn('observaciones');
        });
    }
}
