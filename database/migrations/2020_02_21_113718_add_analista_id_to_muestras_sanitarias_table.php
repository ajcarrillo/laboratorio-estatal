<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAnalistaIdToMuestrasSanitariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->unsignedBigInteger('analista_id')
                ->after('estatus')
                ->nullable();
            $table->foreign('analista_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->dropForeign([ 'analista_id' ]);
            $table->dropColumn('analista_id');
        });
    }
}
