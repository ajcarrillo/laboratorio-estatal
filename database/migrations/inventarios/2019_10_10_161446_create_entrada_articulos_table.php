<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradaArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entrada_articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('entrada_id')->nullable();
            $table->foreign('entrada_id')->references('id')->on('entradas')->onDelete('cascade');
            $table->unsignedBigInteger('cat_articulo_id')->nullable();
            $table->foreign('cat_articulo_id')->references('id')->on('cat_articulos');
            $table->unsignedBigInteger('solicitud_articulo_id')->nullable();
            $table->foreign('solicitud_articulo_id')->references('id')->on('solicitud_articulos');
            $table->string('descripcion_adicional', 300)->nullable();
            $table->string('serie', 300)->nullable();
            $table->string('lote', 300)->nullable();
            $table->string('color', 300)->nullable();
            $table->date('fecha_caducidad')->nullable();
            $table->integer('cantidad')->nullable();
            $table->integer('cantidad_entregada')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->decimal('iva', 10, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entrada_articulos');
    }
}
