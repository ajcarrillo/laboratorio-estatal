<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('clues_id');
            $table->foreign('clues_id')->references('id')->on('laboratorio_estatal.clues');
            $table->string('nombre', 300);
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('areas');
            $table->enum('tipo', ['PUCON', 'DEPARTAMENTO', 'ALMACEN', 'LABORATORIO', 'UNIDAD', 'PACIENTE']);
            $table->enum('comportamiento', ['CEDIS', 'PUCON', 'LABORATORIO']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}
