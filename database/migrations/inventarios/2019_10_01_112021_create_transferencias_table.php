<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transferencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('tipo_transferencia_id')->nullable();
            $table->foreign('tipo_transferencia_id')->references('id')->on('tipos_transferencia');
            $table->unsignedBigInteger('estado_transferencia_id')->nullable();
            $table->foreign('estado_transferencia_id')->references('id')->on('estados_transferencia');
            $table->unsignedBigInteger('area_origen_id')->nullable();
            $table->foreign('area_origen_id')->references('id')->on('areas');
            $table->unsignedBigInteger('area_almacen_destino_id')->nullable();
            $table->foreign('area_almacen_destino_id')->references('id')->on('areas');
            $table->unsignedBigInteger('area_destino_id')->nullable();
            $table->foreign('area_destino_id')->references('id')->on('areas');

            $table->date('fecha')->nullable();
            $table->string('observaciones', 300)->nullable();
            $table->smallInteger('numero_registros')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transferencias');
    }
}
