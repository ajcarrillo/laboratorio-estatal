<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cat_articulo_id')->nullable();
            $table->foreign('cat_articulo_id')->references('id')->on('cat_articulos');
            $table->unsignedBigInteger('area_id')->nullable();
            $table->foreign('area_id')->references('id')->on('areas');
            $table->string('descripcion_adicional', 300)->nullable();
            $table->string('serie', 300)->nullable();
            $table->string('lote', 300)->nullable();
            $table->string('color', 300)->nullable();
            $table->date('fecha_ingreso')->nullable();
            $table->date('fecha_caducidad')->nullable();
            $table->date('fecha_baja')->nullable();
            $table->integer('cantidad_original')->nullable();
            $table->integer('cantidad_existente')->nullable();
            $table->integer('cantidad_transito')->nullable();
            $table->integer('cantidad_desagregada')->nullable();
            $table->decimal('precio', 10, 2)->nullable();
            $table->decimal('iva', 10, 2)->nullable();
            $table->boolean('desagregado')->default(0);
            $table->boolean('activo')->default(1);

            $table->nullableMorphs('origenable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articulos');
    }
}
