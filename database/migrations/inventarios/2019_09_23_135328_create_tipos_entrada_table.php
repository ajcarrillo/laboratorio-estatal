<?php
// php artisan make:migration create_transferencia_articulos_table --create=transferencia_articulos --path=database/migrations/inventarios
// php artisan migrate:fresh --database=inventarios --path=database/migrations/inventarios
// php artisan db:seed --class=InventariosDatabaseSeeder --database=inventarios

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTiposEntradaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tipos_entrada', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion', 300);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tipos_entrada');
    }
}
