<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_articulos', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('solicitud_id')->nullable();
            $table->foreign('solicitud_id')->references('id')->on('solicitudes')->onDelete('cascade');
            $table->unsignedBigInteger('cat_articulo_id')->nullable();
            $table->foreign('cat_articulo_id')->references('id')->on('cat_articulos');
            $table->string('descripcion_adicional', 300)->nullable();

            $table->integer('cantidad_solicitada')->nullable();
            $table->integer('cantidad_autorizada')->nullable();
            $table->integer('cantidad_entregada')->nullable()->default(0);
            $table->integer('cantidad_entregar')->nullable()->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_articulos');
    }
}
