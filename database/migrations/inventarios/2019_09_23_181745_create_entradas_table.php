<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEntradasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entradas', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('tipo_entrada_id')->nullable();
            $table->foreign('tipo_entrada_id')->references('id')->on('tipos_entrada');
            $table->unsignedBigInteger('estado_entrada_id')->nullable();
            $table->foreign('estado_entrada_id')->references('id')->on('estados_entrada');
            $table->unsignedBigInteger('tipo_insumo_id')->nullable();
            $table->foreign('tipo_insumo_id')->references('id')->on('tipos_insumo');
            $table->unsignedBigInteger('proveedor_id')->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');
            $table->unsignedBigInteger('area_origen_id')->nullable();
            $table->foreign('area_origen_id')->references('id')->on('areas');
            $table->unsignedBigInteger('area_almacen_destino_id')->nullable();
            $table->foreign('area_almacen_destino_id')->references('id')->on('areas');
            $table->unsignedBigInteger('area_destino_id')->nullable();
            $table->foreign('area_destino_id')->references('id')->on('areas');

            $table->date('fecha')->nullable();
            $table->date('fecha_entrega')->nullable();
            $table->string('observaciones', 300)->nullable();
            $table->smallInteger('numero_registros')->nullable();

            $table->decimal('total_sin_iva', 12, 2)->nullable();
            $table->decimal('total_iva', 12, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entradas');
    }
}
