<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCatArticulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_articulos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave', 100)->index();
            $table->string('descripcion', 300)->nullable();
            $table->string('unidad_medida', 300)->nullable();
            $table->string('presentacion', 300)->nullable();
            $table->unsignedBigInteger('marca_id')->nullable();
            $table->foreign('marca_id')->references('id')->on('marcas');
            $table->string('modelo', 300)->nullable();
            $table->unsignedBigInteger('tipo_insumo_id')->nullable();
            $table->foreign('tipo_insumo_id')->references('id')->on('tipos_insumo');
            $table->boolean('serie_obligatorio')->default(0);
            $table->boolean('lote_obligatorio')->default(0);
            $table->boolean('caducidad_obligatoria')->default(0);
            $table->boolean('marca_obligatoria')->default(0);
            $table->boolean('inventariable')->default(0);
            $table->boolean('activo')->default(1);
            $table->unsignedBigInteger('cat_articulo_desagregado_id')->nullable();
            $table->foreign('cat_articulo_desagregado_id')->references('id')->on('cat_articulos');
            $table->integer('num_unidades_desagregadas')->nullable();
            $table->smallInteger('iva_default')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_articulos');
    }
}
