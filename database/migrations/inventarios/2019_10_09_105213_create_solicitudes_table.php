<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('tipo_solicitud_id')->nullable();
            $table->foreign('tipo_solicitud_id')->references('id')->on('tipos_solicitud');
            $table->unsignedBigInteger('tipo_insumo_id')->nullable();
            $table->foreign('tipo_insumo_id')->references('id')->on('tipos_insumo');
            $table->unsignedBigInteger('estado_solicitud_id')->nullable();
            $table->foreign('estado_solicitud_id')->references('id')->on('estados_solicitud');
            $table->unsignedBigInteger('area_origen_id')->nullable();
            $table->foreign('area_origen_id')->references('id')->on('areas');
            $table->unsignedBigInteger('area_almacen_destino_id')->nullable();
            $table->foreign('area_almacen_destino_id')->references('id')->on('areas');
            $table->unsignedBigInteger('area_destino_id')->nullable();
            $table->foreign('area_destino_id')->references('id')->on('areas');

            $table->date('fecha')->nullable();
            $table->string('observaciones', 300)->nullable();
            $table->smallInteger('numero_registros')->nullable();

            $table->unsignedBigInteger('proveedor_id')->nullable();
            $table->foreign('proveedor_id')->references('id')->on('proveedores');

            $table->decimal('total_solicitado_sin_iva', 12, 2)->nullable();
            $table->decimal('total_solicitado_iva', 12, 2)->nullable();
            $table->decimal('total_autorizado_sin_iva', 12, 2)->nullable();
            $table->decimal('total_autorizado_iva', 12, 2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes');
    }
}
