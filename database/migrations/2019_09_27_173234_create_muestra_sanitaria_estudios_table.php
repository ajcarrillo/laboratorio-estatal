<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuestraSanitariaEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muestra_sanitaria_estudios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('estudio_id');
            $table->foreign('estudio_id')->references('id')->on('estudios');
            $table->unsignedBigInteger('unidad_id')->nullable();
            $table->foreign('unidad_id')->references('id')->on('unidades');
            $table->string('norma')->nullable();
            $table->string('limite_permisible')->nullable();
            $table->string('orden_impresion')->nullable();
            $table->date('fecha_analisis')->nullable();
            $table->date('fecha_resultados')->nullable();
            $table->string('resultados')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muestra_sanitaria_estudios');
    }
}
