<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeMetodoReferenciaIdInMuestrasEpidemiologicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->dropForeign([ 'metodo_referencia_id' ]);
            $table->dropColumn('metodo_referencia_id');

            $table->text('metodo_referencia')->after('resultado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->dropColumn('metodo_referencia');
            $table->unsignedInteger('metodo_referencia_id')
                ->after('resultado')
                ->nullable();
            $table->foreign('metodo_referencia_id')
                ->references('id')
                ->on('metodos_referencias');
        });
    }
}
