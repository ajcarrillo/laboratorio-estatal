<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrigenesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('origenes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tipo', 20)
                ->index()
                ->comment('Puede ser INSTITUCIONAL o PARTICULAR');
            $table->string('clave')->nullable();
            $table->string('iniciales')->nullable();
            $table->string('origen')->index();
            $table->string('razon_social')->nullable();
            $table->text('telefono')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('director_nombre')->nullable();
            $table->string('director_direccion')->nullable();
            $table->text('director_telefono')->nullable();
            $table->string('director_email')->nullable();
            $table->string('director_cargo')->nullable();
            $table->string('director_preposicion')->nullable();
            $table->string('director_estado')->nullable();
            $table->string('director_municipio')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('origenes');
    }
}
