<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSolicitudAnalisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_analisis', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->year('year');
            $table->integer('numero')->nullable();
            $table->string('folio', 12)->unique();
            $table->dateTime('fecha_recepcion');
            $table->unsignedBigInteger('origen_id');
            $table->foreign('origen_id')->references('id')->on('origenes');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_analisis');
    }
}
