<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPantallaRecepcionToSolicitudAnalisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->string('pantalla_recepcion')->after('origen_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->dropColumn('pantalla_recepcion');
        });
    }
}
