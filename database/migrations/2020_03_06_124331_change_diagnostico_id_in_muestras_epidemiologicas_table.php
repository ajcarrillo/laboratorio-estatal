<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeDiagnosticoIdInMuestrasEpidemiologicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->dropForeign([ 'diagnostico_id' ]);
            $table->foreign('diagnostico_id')->references('id')->on('cat_diagnosticos');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->dropForeign([ 'diagnostico_id' ]);
            $table->foreign('diagnostico_id')->references('id')->on('diagnosticos');
        });
    }
}
