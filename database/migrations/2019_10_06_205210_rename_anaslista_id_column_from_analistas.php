<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RenameAnaslistaIdColumnFromAnalistas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('analistas', function (Blueprint $table) {
            $table->dropForeign([ 'analista_id' ]);
            $table->dropUnique([ 'analista_id', 'laboratorio_id', 'activo' ]);
            $table->renameColumn('analista_id', 'user_id');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('analistas', function (Blueprint $table) {
            $table->dropForeign([ 'user_id' ]);
            $table->renameColumn('user_id', 'analista_id');
            $table->unique([ 'analista_id', 'laboratorio_id', 'activo' ]);
            $table->foreign('analista_id')->references('id')->on('users');
        });
    }
}
