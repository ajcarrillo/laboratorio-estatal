<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeColumnFolioNameFromTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->renameColumn('folio', 'numero_muestra');
            $table->year('year')->after('pantalla_recepcion');
            $table->unique([ 'numero_muestra', 'year' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->renameColumn('numero_muestra', 'folio');
            $table->dropUnique([ 'numero_muestra', 'year' ]);
            $table->dropColumn('year');
        });
    }
}
