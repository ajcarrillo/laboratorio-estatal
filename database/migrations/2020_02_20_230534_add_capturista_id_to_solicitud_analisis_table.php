<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCapturistaIdToSolicitudAnalisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->unsignedBigInteger('capturista_id')
                ->after('estatus');
            $table->foreign('capturista_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->dropForeign([ 'capturista_id' ]);
            $table->dropColumn('capturista_id');
        });
    }
}
