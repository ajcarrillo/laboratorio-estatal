<?php

use Illuminate\Database\Migrations\Migration;

class CreateEstadosViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());

        DB::statement($this->createView());
    }

    private function dropView()
    {
        return "DROP VIEW IF EXISTS `estados_view`;";
    }

    private function createView()
    {
        return <<< SQL
CREATE VIEW `estados_view` AS
SELECT cve_ent,
       nom_ent
FROM estados_municipios_localidades
GROUP BY cve_ent, nom_ent;
SQL;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropView());
    }
}
