<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTipoMuestreoFromMuestrasSanitariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->text('tipo_muestreo')
                ->nullable()
                ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->string('tipo_muestreo')
                ->nullable()
                ->change();
        });
    }
}
