<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUnidadAdminUnidadMedicaToMuestrasEpidemiologicasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->unsignedBigInteger('unidad_administrativa_id')
                ->after('id')
                ->comment('Las unidades administrativas pueden ser cualquiera de las 3 jurisdicciones');
            $table->foreign('unidad_administrativa_id')->references('id')->on('clues');
            $table->unsignedBigInteger('unidad_medica_id')->after('unidad_administrativa_id');
            $table->foreign('unidad_medica_id')->references('id')->on('clues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_epidemiologicas', function (Blueprint $table) {
            $table->dropForeign([ 'unidad_administrativa_id' ]);
            $table->dropForeign([ 'unidad_medica_id' ]);
            $table->dropColumn('unidad_administrativa_id');
            $table->dropColumn('unidad_medica_id');
        });
    }
}
