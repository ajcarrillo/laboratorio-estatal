<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRevisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revisiones', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('reviewable');
            $table->dateTime('fecha_apertura')->index();
            $table->unsignedBigInteger('usuario_apertura_id');
            $table->foreign('usuario_apertura_id')->references('id')->on('users');
            $table->dateTime('fecha_revision')->nullable()->index();
            $table->unsignedBigInteger('usuario_revision_id')->nullable();
            $table->foreign('usuario_revision_id')->references('id')->on('users');
            $table->string('estatus', 20)->index();
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revisiones');
    }
}
