<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEstatusToMuestraSanitariaEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestra_sanitaria_estudios', function (Blueprint $table) {
            $table->string('estatus', 30)
                ->nullable()
                ->index()
                ->after('observaciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestra_sanitaria_estudios', function (Blueprint $table) {
            $table->dropIndex([ 'estatus' ]);
            $table->dropColumn('estatus');
        });
    }
}
