<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpidemiologiaMuestrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muestras_epidemiologicas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero_muestra')->unique()->index();
            $table->unsignedBigInteger('diagnostico_id');
            $table->foreign('diagnostico_id')->references('id')->on('diagnosticos');
            $table->unsignedBigInteger('analista_id');
            $table->foreign('analista_id')->references('id')->on('users');
            $table->date('fecha_toma');
            $table->date('fecha_recepcion');
            $table->unsignedBigInteger('paciente_id');
            $table->foreign('paciente_id')->references('id')->on('pacientes');

            $table->date('fecha_analisis')->nullable();
            $table->date('fecha_resultados')->nullable();
            $table->text('resultado')->nullable();
            $table->unsignedInteger('metodo_referencia_id')->nullable();
            $table->foreign('metodo_referencia_id')->references('id')->on('metodos_referencias');
            $table->text('comentarios')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epidemiologia_muestras');
    }
}
