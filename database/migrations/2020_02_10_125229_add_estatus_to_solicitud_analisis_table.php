<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddEstatusToSolicitudAnalisisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->string('estatus', 30)
                ->nullable()
                ->index()
                ->after('terminado');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitud_analisis', function (Blueprint $table) {
            $table->dropIndex([ 'estatus' ]);
            $table->dropColumn('estatus');
        });
    }
}
