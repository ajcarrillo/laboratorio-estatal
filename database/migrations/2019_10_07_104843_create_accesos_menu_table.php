<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccesosMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sistema_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion', 300)->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->foreign('parent_id')->references('id')->on('sistema_menus');
            $table->string('icon', 300)->nullable();
            $table->string('path', 300)->nullable();
            $table->tinyInteger('orden')->nullable();
            $table->unsignedBigInteger('sistema_id')->nullable();
            $table->foreign('sistema_id')->references('id')->on('sistemas');
            $table->boolean('activo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accesos_menu');
    }
}
