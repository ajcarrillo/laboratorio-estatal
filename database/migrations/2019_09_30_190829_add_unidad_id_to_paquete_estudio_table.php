<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnidadIdToPaqueteEstudioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paquete_estudio', function (Blueprint $table) {
            $table->dropColumn('unidad');
            $table->unsignedBigInteger('unidad_id')
                ->nullable()
                ->after('estudio_id');
            $table->foreign('unidad_id')->references('id')->on('unidades');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paquete_estudio', function (Blueprint $table) {
            $table->string('unidad')->nullable();
            $table->dropForeign([ 'unidad_id' ]);
            $table->dropColumn('unidad_id');
        });
    }
}
