<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMuestrasSanitariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('muestras_sanitarias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('es_subrogado')->default(0);
            $table->unsignedBigInteger('solicitud_analisis_id');
            $table->foreign('solicitud_analisis_id')->references('id')->on('solicitud_analisis');
            $table->string('pantalla_recepcion');
            $table->string('folio');
            $table->unsignedBigInteger('recibio_id');
            $table->foreign('recibio_id')->references('id')->on('users');
            $table->string('entrego');
            $table->dateTime('fecha_muestreo')->nullable();
            $table->string('marca_comercial')->nullable();
            $table->text('descripcion_muestra')->nullable();
            $table->unsignedBigInteger('recipiente_id')->nullable();
            $table->foreign('recipiente_id')->references('id')->on('recipientes');
            $table->string('temperatura')->nullable();
            $table->string('peso')->nullable();
            $table->string('volumen')->nullable();
            $table->string('lote')->nullable();
            $table->string('punto_muestreo')->nullable();
            $table->string('cve_ent', 2)->nullable();
            $table->string('cve_mun', 3)->nullable();
            $table->string('cve_loc', 4)->nullable();
            $table->string('nombre_entidad')->nullable();
            $table->string('nombre_municipio')->nullable();
            $table->string('nombre_localidad')->nullable();
            $table->string('numero_acta')->nullable();
            $table->string('tipo_muestreo')->nullable();
            $table->date('caducidad')->nullable();
            $table->string('envasado_por')->nullable();
            $table->string('lugar_muestreo')->nullable();
            $table->string('ph')->nullable();
            $table->string('cloro_residual')->nullable();
            $table->unsignedBigInteger('paciente_id')->nullable();
            $table->foreign('paciente_id')->references('id')->on('pacientes');
            $table->unsignedBigInteger('subcategoria_id')->nullable();
            $table->foreign('subcategoria_id')->references('id')->on('subcategorias');
            $table->text('observaciones')->nullable();
            $table->string('jurisdiccion', 2)->nullable();
            $table->string('tamano_lote')->nullable();
            $table->date('fecha_recoleccion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('muestras_sanitarias');
    }
}
