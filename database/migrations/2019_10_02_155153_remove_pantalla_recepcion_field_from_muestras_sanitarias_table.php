<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemovePantallaRecepcionFieldFromMuestrasSanitariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->dropColumn('pantalla_recepcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->string('pantalla_recepcion');
        });
    }
}
