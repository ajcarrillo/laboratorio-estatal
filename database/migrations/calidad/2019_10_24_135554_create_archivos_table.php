<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArchivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('archivos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('folder', '300')->nullable();
            $table->string('filename', '300')->nullable();
            $table->string('path', '300')->nullable();
            $table->string('type', '50')->nullable();
            $table->string('mime', '100')->nullable();
            $table->string('size', '20')->nullable();
            $table->nullableMorphs('origenable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('archivos');
    }
}
