<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registros', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave', '300')->nullable();
            $table->string('nombre', '300')->nullable();
            $table->tinyInteger('version')->default(0);
            $table->set('medio_soporte', ['IMPRESO', 'ELECTRÓNICO', 'FÍSICO']);
            $table->set('medio_proteccion',
                ['MICA DE ACRÍLICO', 'FOLDER', 'CARPETA', 'ENGARGOLADO', 'DISCO COMPACTO', 'DVD', 'PROTECCIÓN CONTRA HUMEDAD', 'USUARIO Y CONTRASEÑA']);
            $table->set('medio_almacenamiento', ['ARCHIVERO', 'GAVETA', 'MUEBLE', 'CPU', 'DISCO DURO', 'SERVIDOR DE BASE DE DATOS']);
            $table->set('medio_recuperacion', ['CLAVE', 'NO DE FOLIO', 'FECHA']);
            $table->text('responsable')->nullable();
            $table->string('tiempo_retencion', '300')->nullable();
            $table->set('medio_disposicion', ['ARCHIVO MUERTO', 'DESTRUCCIÓN', 'RECICLADO']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registros');
    }
}
