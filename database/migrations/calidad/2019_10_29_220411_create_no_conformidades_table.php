<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoConformidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('no_conformidades', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave', '100')->nullable();
            $table->text('hallazgo')->nullable();
            $table->unsignedBigInteger('clasificacion_id')->nullable();
            $table->foreign('clasificacion_id')->references('id')->on('clasificaciones_nc');
            $table->unsignedBigInteger('fuente_id')->nullable();
            $table->foreign('fuente_id')->references('id')->on('fuentes_nc');
            $table->unsignedBigInteger('departamento_id')->nullable();
            $table->foreign('departamento_id')->references('id')->on('departamentos');
            $table->unsignedBigInteger('area_id')->nullable();
            $table->foreign('area_id')->references('id')->on('departamentos');
            $table->date('fecha_deteccion')->nullable();
            //$table->text('acciones')->nullable();
            //$table->date('fecha_acciones')->nullable();
            //$table->string('observaciones', '300')->nullable();
            $table->enum('resolucion', ['EFICAZ', 'INEFICAZ'])->nullable();
            $table->date('fecha_cierre')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('no_conformidades');
    }
}
