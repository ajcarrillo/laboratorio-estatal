<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccionesNcTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acciones_nc', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('no_conformidad_id')->nullable();
            $table->foreign('no_conformidad_id')->references('id')->on('no_conformidades')->onDelete('cascade');
            $table->text('acciones')->nullable();
            $table->date('fecha_acciones')->nullable();
            $table->string('observaciones', '300')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acciones_nc');
    }
}
