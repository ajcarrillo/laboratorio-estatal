<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnalistasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('analistas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('analista_id');
            $table->foreign('analista_id')->references('id')->on('users');
            $table->unsignedBigInteger('laboratorio_id');
            $table->foreign('laboratorio_id')->references('id')->on('laboratorios');
            $table->date('inicio');
            $table->date('fin')->nullable();
            $table->boolean('activo')->default(1);
            $table->unique([ 'analista_id', 'laboratorio_id', 'activo' ]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('analistas');
    }
}
