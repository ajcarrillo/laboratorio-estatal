<?php

use Illuminate\Database\Migrations\Migration;

class CreateEstadosMunicipiosLocalidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement($this->dropView());

        DB::statement($this->createView());
    }

    private function dropView(): string
    {
        return "DROP VIEW IF EXISTS `estados_municipios_localidades`;";
    }

    private function createView()
    {
        return <<< SQL
CREATE TABLE `estados_municipios_localidades` (
    `CVE_ENT`   VARCHAR(2)   DEFAULT NULL,
    `NOM_ENT`   VARCHAR(255) DEFAULT NULL,
    `NOM_ABR`   VARCHAR(255) DEFAULT NULL,
    `CVE_MUN`   VARCHAR(3)   DEFAULT NULL,
    `NOM_MUN`   VARCHAR(255) DEFAULT NULL,
    `CVE_LOC`   VARCHAR(4)   DEFAULT NULL,
    `NOM_LOC`   VARCHAR(255) DEFAULT NULL,
    `AMBITO`    VARCHAR(255) DEFAULT NULL,
    `LATITUD`   VARCHAR(255) DEFAULT NULL,
    `LONGITUD`  VARCHAR(255) DEFAULT NULL,
    `LAT_DEC`   VARCHAR(255) DEFAULT NULL,
    `LON_DEC`   VARCHAR(255) DEFAULT NULL,
    `ALTITUD`   VARCHAR(255) DEFAULT NULL,
    `CVE_CARTA` VARCHAR(255) DEFAULT NULL,
    `PLANO`     VARCHAR(255) DEFAULT NULL,
    KEY `estados_municipios_localidades_cve_ent_index` (`CVE_ENT`),
    KEY `estados_municipios_localidades_cve_mun_index` (`CVE_MUN`),
    KEY `estados_municipios_localidades_cve_loc_index` (`CVE_LOC`)
)
    ENGINE = InnoDB
    DEFAULT CHARSET = utf8
    ROW_FORMAT = DYNAMIC 
SQL;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement($this->dropView());
    }
}
