<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTipoToLaboratoriosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('laboratorios', function (Blueprint $table) {
            $table->dropForeign([ 'responsable_id' ]);
            $table->dropColumn('responsable_id');
            $table->dropColumn('fecha_inicio');
            $table->dropColumn('historial_responsables');
            $table->enum('tipo', [ 'EPIDEMIOLOGIA', 'SANITARIA' ])->after('descripcion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('laboratorios', function (Blueprint $table) {
            $table->unsignedBigInteger('responsable_id')->nullable();
            $table->foreign('responsable_id')->references('id')->on('users');
            $table->date('fecha_inicio')->nullable();
            $table->text('historial_responsables')->nullable();
            $table->dropColumn('tipo');
        });
    }
}
