<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDiagnosticoIdToMetodosReferenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('metodos_referencias', function (Blueprint $table) {
            $table->unsignedBigInteger('diagnostico_id')
                ->after('descripcion');
            $table->foreign('diagnostico_id')
                ->references('id')
                ->on('cat_diagnosticos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('metodos_referencias', function (Blueprint $table) {
            $table->dropForeign([ 'diagnostico_id' ]);
            $table->dropColumn('diagnostico_id');
        });
    }
}
