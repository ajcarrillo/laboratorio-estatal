<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estudios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('clave');
            $table->string('clave_alterna')->nullable();
            $table->string('formato')->default('GENERAL');
            $table->string('descripcion');
            $table->string('cofrepris')->nullable();
            $table->string('referencia')->nullable();
            $table->string('tipo_limite')->nullable();
            $table->boolean('dias_naturales')->default(1);
            $table->unsignedBigInteger('laboratorio_id')->nullable();
            $table->foreign('laboratorio_id')->references('id')->on('laboratorios');
            $table->unsignedBigInteger('recipiente_id')->nullable();
            $table->foreign('recipiente_id')->references('id')->on('recipientes');
            $table->string('tiempo_alerta')->nullable();
            $table->string('tiempo_proceso')->nullable();
            $table->boolean('validar_resultado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estudios');
    }
}
