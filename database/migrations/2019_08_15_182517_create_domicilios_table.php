<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomiciliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->morphs('addressable');
            $table->text('direccion');
            $table->string('codigo_postal')->nullable();
            $table->string('cve_ent', 2)->nullable();
            $table->string('nombre_entidad')->nullable();
            $table->string('cve_mun', 3)->nullable();
            $table->string('nombre_municipio')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilios');
    }
}
