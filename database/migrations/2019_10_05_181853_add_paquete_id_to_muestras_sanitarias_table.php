<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaqueteIdToMuestrasSanitariasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->unsignedBigInteger('paquete_id')->after('subcategoria_id');
            $table->foreign('paquete_id')->references('id')->on('paquetes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('muestras_sanitarias', function (Blueprint $table) {
            $table->dropForeign([ 'paquete_id' ]);
            $table->dropColumn('paquete_id');
        });
    }
}
