INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (1, 'AGUA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (2, 'ALFARERÍA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (3, 'ALIMENTO', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (4, 'ARTÍCULOS DE LIMPIEZA Y HOGAR', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (5, 'ARTÍCULOS ESCOLARES', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (6, 'BEBIDA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (7, 'CARNE COCIDA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (8, 'CARNE CRUDA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (9, 'CEREALES Y/O LEGUMINOSAS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (10, 'CONDIMENTO', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (11, 'CONFITERÍA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (12, 'DISPOSITIVOS MÉDICOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (13, 'EMBUTIDOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (14, 'ENDULZANTE', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (15, 'ENSALADA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (16, 'ENSALADA DE FRUTAS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (17, 'ENSALADA VEGETAL', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (18, 'FRUTA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (19, 'GRASA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (20, 'HARINA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (21, 'HUEVO', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (22, 'JUGUETES', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (23, 'LÁCTEOS Y DERIVADOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (24, 'MATERIAL DE CURACIÓN', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (25, 'MEDICAMENTOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (26, 'MOLUSCOS BIVALVOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (27, 'MOLUSCOS CEFALÓPODOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (28, 'MOLUSCOS GASTERÓPODOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (29, 'PANADERÍA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (30, 'PESCADOS Y MARISCOS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (31, 'SALSAS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (32, 'SOPAS Y/O CREMAS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (33, 'SUPERFICIES INERTES', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (34, 'SUPERFICIES VIVAS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (35, 'VEGETAL', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (36, 'VISERAS', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (37, 'SUERO/SANGRE', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (38, 'ORINA', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (39, 'VOMITO', NULL, NULL);
INSERT INTO laboratorio_estatal.categorias (id, descripcion, created_at, updated_at)
VALUES (40, 'MUESTRA FORTIFICADA', NULL, NULL);
