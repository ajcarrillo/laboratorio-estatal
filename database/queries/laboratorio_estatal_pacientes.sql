INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (1, NULL, 'LUIS HUMBERTO BEDRIÑANA BARBOZA', 'LUIS HUMBERTO BEDRIÑANA BARBOZA', 'LUIS HUMBERTO BEDRIÑANA BARBOZA', 'LUIS HUMBERTO BEDRIÑANA BARBOZA', '1975-11-10',
        'MASCULINO', '9838360714', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (2, NULL, 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', '1973-01-10', 'MASCULINO',
        '9831190176', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (3, NULL, 'C.CARLOS MANUEL MENDICUTI PINTO', 'C.CARLOS MANUEL MENDICUTI PINTO', 'C.CARLOS MANUEL MENDICUTI PINTO', 'C.CARLOS MANUEL MENDICUTI PINTO', '1977-01-01',
        'MASCULINO', '9831047663', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (4, NULL, 'C. MARTHA BEATRIZ ANGULO CHI', 'C. MARTHA BEATRIZ ANGULO CHI', 'C. MARTHA BEATRIZ ANGULO CHI', 'C. MARTHA BEATRIZ ANGULO CHI', '1981-01-01', 'FEMENINO',
        '9831119348', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (5, NULL, 'C. LUIS FERNANDO ORTEGA CANCHE', 'C. LUIS FERNANDO ORTEGA CANCHE', 'C. LUIS FERNANDO ORTEGA CANCHE', 'C. LUIS FERNANDO ORTEGA CANCHE', '1978-01-01', 'MASCULINO',
        '9831101591', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (6, NULL, 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', '1960-01-01',
        'MASCULINO', '9831206453', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (7, NULL, 'C. JOSUE JOEL CU UC', 'C. JOSUE JOEL CU UC', 'C. JOSUE JOEL CU UC', 'C. JOSUE JOEL CU UC', '1981-01-01', 'MASCULINO', '9831489077', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (8, NULL, 'C. ROQUE EDUIN DURAN NOVELO', 'C. ROQUE EDUIN DURAN NOVELO', 'C. ROQUE EDUIN DURAN NOVELO', 'C. ROQUE EDUIN DURAN NOVELO', '1965-01-01', 'MASCULINO',
        '8372332/9831123381', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (9, NULL, 'C. JESUS JULIAN DURAN LIZAMA', 'C. JESUS JULIAN DURAN LIZAMA', 'C. JESUS JULIAN DURAN LIZAMA', 'C. JESUS JULIAN DURAN LIZAMA', '1987-01-01', 'MASCULINO',
        '9831350700', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (10, NULL, 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', '1979-01-01',
        'FEMENINO', '9831258387', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (11, NULL, 'C. JORGE VAZQUEZ PEREZ', 'C. JORGE VAZQUEZ PEREZ', 'C. JORGE VAZQUEZ PEREZ', 'C. JORGE VAZQUEZ PEREZ', '1980-01-01', 'MASCULINO', '9831068643', NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (12, NULL, 'C. ROGELIO CASTILLO BRITO', 'C. ROGELIO CASTILLO BRITO', 'C. ROGELIO CASTILLO BRITO', 'C. ROGELIO CASTILLO BRITO', '1962-05-19', 'MASCULINO', '9831043628',
        'brito_21245@hotmail.com', '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (13, NULL, 'C. RAUL GERONIMO JIMENEZ MARTINEZ', 'C. RAUL GERONIMO JIMENEZ MARTINEZ', 'C. RAUL GERONIMO JIMENEZ MARTINEZ', 'C. RAUL GERONIMO JIMENEZ MARTINEZ', '1981-01-01',
        'MASCULINO', '9831205679', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (14, NULL, 'C. RODOLFO CANTE UCAN', 'C. RODOLFO CANTE UCAN', 'C. RODOLFO CANTE UCAN', 'C. RODOLFO CANTE UCAN', '1990-01-01', 'MASCULINO', '9831263763', NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (15, NULL, 'C. MARIO A. GONZALEZ MOGUEL', 'C. MARIO A. GONZALEZ MOGUEL', 'C. MARIO A. GONZALEZ MOGUEL', 'C. MARIO A. GONZALEZ MOGUEL', '1959-01-01', 'MASCULINO',
        '9837325402', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (16, NULL, 'C. SEBASTIANA RUIZ CRUZ', 'C. SEBASTIANA RUIZ CRUZ', 'C. SEBASTIANA RUIZ CRUZ', 'C. SEBASTIANA RUIZ CRUZ', '1980-01-01', 'FEMENINO', '9831162947',
        'acatic51@hotmail.com', '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (17, NULL, 'C. LUIS ENRIQUE CUXIM ADORNO', 'C. LUIS ENRIQUE CUXIM ADORNO', 'C. LUIS ENRIQUE CUXIM ADORNO', 'C. LUIS ENRIQUE CUXIM ADORNO', '1989-01-01', 'MASCULINO',
        '9831760883', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (18, NULL, 'C. MAURICIO CABALLERO E.', 'C. MAURICIO CABALLERO E.', 'C. MAURICIO CABALLERO E.', 'C. MAURICIO CABALLERO E.', '1980-01-01', 'MASCULINO', '8331522', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (19, NULL, 'C. LUIS ENRIQUE TORRES VALDEZ', 'C. LUIS ENRIQUE TORRES VALDEZ', 'C. LUIS ENRIQUE TORRES VALDEZ', 'C. LUIS ENRIQUE TORRES VALDEZ', '1977-01-01', 'MASCULINO',
        '9831251825', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (20, NULL, 'C. HECTOR BUSTILLO ANGULO', 'C. HECTOR BUSTILLO ANGULO', 'C. HECTOR BUSTILLO ANGULO', 'C. HECTOR BUSTILLO ANGULO', '1969-03-02', 'MASCULINO', '8327165', NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (21, NULL, 'C. JULIAN GUALBERTO KEB MIS', 'C. JULIAN GUALBERTO KEB MIS', 'C. JULIAN GUALBERTO KEB MIS', 'C. JULIAN GUALBERTO KEB MIS', '1974-01-01', 'MASCULINO', '8335896',
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (22, NULL, 'C.HUGO ORELLANA RAMIREZ', 'C.HUGO ORELLANA RAMIREZ', 'C.HUGO ORELLANA RAMIREZ', 'C.HUGO ORELLANA RAMIREZ', '1978-01-01', 'MASCULINO', '9831131972', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (23, NULL, 'NEMIAS MARTINEZ CHAN', 'NEMIAS MARTINEZ CHAN', 'NEMIAS MARTINEZ CHAN', 'NEMIAS MARTINEZ CHAN', '1979-01-01', 'MASCULINO', '9981599657', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (24, NULL, 'C. UVENCIO XIU MIS', 'C. UVENCIO XIU MIS', 'C. UVENCIO XIU MIS', 'C. UVENCIO XIU MIS', '1980-01-01', 'MASCULINO', '9838095204', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (25, NULL, 'SANTIAGO KU UC', 'SANTIAGO KU UC', 'SANTIAGO KU UC', 'SANTIAGO KU UC', '1966-01-01', 'MASCULINO', '9831067097', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (26, NULL, 'C. GUADALUPE ARANDA ROMERO', 'C. GUADALUPE ARANDA ROMERO', 'C. GUADALUPE ARANDA ROMERO', 'C. GUADALUPE ARANDA ROMERO', '1980-01-01', 'FEMENINO', '9841695779',
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (27, NULL, 'C. JOSUE JOEL CU UC', 'C. JOSUE JOEL CU UC', 'C. JOSUE JOEL CU UC', 'C. JOSUE JOEL CU UC', '1981-01-01', 'MASCULINO', '9831489077', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (28, NULL, 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', '1959-01-01',
        'MASCULINO', '9831206453', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (29, NULL, 'C. ARTURO EDUARDO', 'C. ARTURO EDUARDO', 'C. ARTURO EDUARDO', 'C. ARTURO EDUARDO', '1963-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (30, NULL, 'C. ARTURO EDUARDO MARRUFO RIVEROL', 'C. ARTURO EDUARDO MARRUFO RIVEROL', 'C. ARTURO EDUARDO MARRUFO RIVEROL', 'C. ARTURO EDUARDO MARRUFO RIVEROL', '1963-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (31, NULL, 'C.JULIO C. CASTILLO QUINTAL', 'C.JULIO C. CASTILLO QUINTAL', 'C.JULIO C. CASTILLO QUINTAL', 'C.JULIO C. CASTILLO QUINTAL', '1968-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (32, NULL, 'C. PEDRO ESCOBEDO MEDINA', 'C. PEDRO ESCOBEDO MEDINA', 'C. PEDRO ESCOBEDO MEDINA', 'C. PEDRO ESCOBEDO MEDINA', '1945-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (33, NULL, 'C. GERARDO REYES MENDEZ', 'C. GERARDO REYES MENDEZ', 'C. GERARDO REYES MENDEZ', 'C. GERARDO REYES MENDEZ', '1963-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (34, NULL, 'C. PEDRO MARIANO MEX MAY', 'C. PEDRO MARIANO MEX MAY', 'C. PEDRO MARIANO MEX MAY', 'C. PEDRO MARIANO MEX MAY', '1987-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (35, NULL, 'C. FELIPE CONCEPCION LEAL CALDERON', 'C. FELIPE CONCEPCION LEAL CALDERON', 'C. FELIPE CONCEPCION LEAL CALDERON', 'C. FELIPE CONCEPCION LEAL CALDERON',
        '1967-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (36, NULL, 'C. DIANA MENA BE', 'C. DIANA MENA BE', 'C. DIANA MENA BE', 'C. DIANA MENA BE', '1982-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (37, NULL, 'C. CORNELIO MOO CHALE', 'C. CORNELIO MOO CHALE', 'C. CORNELIO MOO CHALE', 'C. CORNELIO MOO CHALE', '1971-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (38, NULL, 'C. MIGUEL IXTEPAN TOTO', 'C. MIGUEL IXTEPAN TOTO', 'C. MIGUEL IXTEPAN TOTO', 'C. MIGUEL IXTEPAN TOTO', '1965-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (39, NULL, 'C.RUBEN ERNESTO CRUZ PEREZ', 'C.RUBEN ERNESTO CRUZ PEREZ', 'C.RUBEN ERNESTO CRUZ PEREZ', 'C.RUBEN ERNESTO CRUZ PEREZ', '1988-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (40, NULL, 'C. ARIEL BRICEÑO PACHECO', 'C. ARIEL BRICEÑO PACHECO', 'C. ARIEL BRICEÑO PACHECO', 'C. ARIEL BRICEÑO PACHECO', '1995-06-18', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (41, NULL, 'C. SIGREDA ALEJANDRA CHABLE COOL', 'C. SIGREDA ALEJANDRA CHABLE COOL', 'C. SIGREDA ALEJANDRA CHABLE COOL', 'C. SIGREDA ALEJANDRA CHABLE COOL', '1976-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (42, NULL, 'C. JOSE ARTURO CAMPOS FLORES', 'C. JOSE ARTURO CAMPOS FLORES', 'C. JOSE ARTURO CAMPOS FLORES', 'C. JOSE ARTURO CAMPOS FLORES', '1989-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (43, NULL, 'C. EMILIO MATA RODRIGUEZ', 'C. EMILIO MATA RODRIGUEZ', 'C. EMILIO MATA RODRIGUEZ', 'C. EMILIO MATA RODRIGUEZ', '1964-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (44, NULL, 'C. LILIA CONCEPCION SILVA GONZALEZ', 'C. LILIA CONCEPCION SILVA GONZALEZ', 'C. LILIA CONCEPCION SILVA GONZALEZ', 'C. LILIA CONCEPCION SILVA GONZALEZ',
        '1979-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (45, NULL, 'C. IRMA ESCAMILLA ZAPATA', 'C. IRMA ESCAMILLA ZAPATA', 'C. IRMA ESCAMILLA ZAPATA', 'C. IRMA ESCAMILLA ZAPATA', '1966-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (46, NULL, 'C. JOSE ALBERTO PECH GONZALEZ', 'C. JOSE ALBERTO PECH GONZALEZ', 'C. JOSE ALBERTO PECH GONZALEZ', 'C. JOSE ALBERTO PECH GONZALEZ', '1968-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (47, NULL, 'C. LOURDES H. CAMPOS LUNA', 'C. LOURDES H. CAMPOS LUNA', 'C. LOURDES H. CAMPOS LUNA', 'C. LOURDES H. CAMPOS LUNA', '1970-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (48, NULL, 'C. MAGALY JAZMIN VALLE SANTIAGO', 'C. MAGALY JAZMIN VALLE SANTIAGO', 'C. MAGALY JAZMIN VALLE SANTIAGO', 'C. MAGALY JAZMIN VALLE SANTIAGO', '1980-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (49, NULL, 'C. DONACIANO RICALDE LOPEZ', 'C. DONACIANO RICALDE LOPEZ', 'C. DONACIANO RICALDE LOPEZ', 'C. DONACIANO RICALDE LOPEZ', '1956-06-18', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (50, NULL, 'C. MATILDE PERES DE LA CRUZ', 'C. MATILDE PERES DE LA CRUZ', 'C. MATILDE PERES DE LA CRUZ', 'C. MATILDE PERES DE LA CRUZ', '1980-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (51, NULL, 'C. JUAN RICARDO BOLAÑOS RIOS', 'C. JUAN RICARDO BOLAÑOS RIOS', 'C. JUAN RICARDO BOLAÑOS RIOS', 'C. JUAN RICARDO BOLAÑOS RIOS', '1986-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (52, NULL, 'C. FAUSTO A. VAZQUEZ BRICEÑO', 'C. FAUSTO A. VAZQUEZ BRICEÑO', 'C. FAUSTO A. VAZQUEZ BRICEÑO', 'C. FAUSTO A. VAZQUEZ BRICEÑO', '1970-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (53, NULL, 'C. MIGUEL ANGEL VILLA HERNANDEZ', 'C. MIGUEL ANGEL VILLA HERNANDEZ', 'C. MIGUEL ANGEL VILLA HERNANDEZ', 'C. MIGUEL ANGEL VILLA HERNANDEZ', '1977-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (54, NULL, 'C. ROSALIA MUL MENDO', 'C. ROSALIA MUL MENDO', 'C. ROSALIA MUL MENDO', 'C. ROSALIA MUL MENDO', '1969-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (55, NULL, 'C. JUAN FELIPE ORTEGON MENDOZA', 'C. JUAN FELIPE ORTEGON MENDOZA', 'C. JUAN FELIPE ORTEGON MENDOZA', 'C. JUAN FELIPE ORTEGON MENDOZA', '1979-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (56, NULL, 'C. CHARLY JOEL SERRANO MARTINEZ', 'C. CHARLY JOEL SERRANO MARTINEZ', 'C. CHARLY JOEL SERRANO MARTINEZ', 'C. CHARLY JOEL SERRANO MARTINEZ', '1989-06-18',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (57, NULL, 'C. ALBERTO ROSAS CORTES', 'C. ALBERTO ROSAS CORTES', 'C. ALBERTO ROSAS CORTES', 'C. ALBERTO ROSAS CORTES', '1969-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (58, NULL, 'C. PEDRO ALBERTO VALLEJO NUÑEZ', 'C. PEDRO ALBERTO VALLEJO NUÑEZ', 'C. PEDRO ALBERTO VALLEJO NUÑEZ', 'C. PEDRO ALBERTO VALLEJO NUÑEZ', '1985-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (59, NULL, 'C. ASAEL CAAMAL ACEVEDO', 'C. ASAEL CAAMAL ACEVEDO', 'C. ASAEL CAAMAL ACEVEDO', 'C. ASAEL CAAMAL ACEVEDO', '1974-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (60, NULL, 'C. JOSE EDUARDO RICALDE PECH', 'C. JOSE EDUARDO RICALDE PECH', 'C. JOSE EDUARDO RICALDE PECH', 'C. JOSE EDUARDO RICALDE PECH', '1983-10-13', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (61, NULL, 'C. JOSE FRANCISCO MENDOZA LOPEZ', 'C. JOSE FRANCISCO MENDOZA LOPEZ', 'C. JOSE FRANCISCO MENDOZA LOPEZ', 'C. JOSE FRANCISCO MENDOZA LOPEZ', '1967-02-04',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (62, NULL, 'C. WILLIAM RAUL GONGORA TALAVERA', 'C. WILLIAM RAUL GONGORA TALAVERA', 'C. WILLIAM RAUL GONGORA TALAVERA', 'C. WILLIAM RAUL GONGORA TALAVERA', '1962-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (63, NULL, 'C. LILI CHAN LANDERO', 'C. LILI CHAN LANDERO', 'C. LILI CHAN LANDERO', 'C. LILI CHAN LANDERO', '1984-03-15', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (64, NULL, 'C. JORGE ALBERTO RODRIGUEZ GUTIERREZ', 'C. JORGE ALBERTO RODRIGUEZ GUTIERREZ', 'C. JORGE ALBERTO RODRIGUEZ GUTIERREZ', 'C. JORGE ALBERTO RODRIGUEZ GUTIERREZ',
        '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (65, NULL, 'C.MORENO PEREZ GILBERTO ADOLFO', 'C.MORENO PEREZ GILBERTO ADOLFO', 'C.MORENO PEREZ GILBERTO ADOLFO', 'C.MORENO PEREZ GILBERTO ADOLFO', '1984-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (66, NULL, 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ', 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ', 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ', 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ',
        '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (67, NULL, 'C. MOISES PEREZ ORTIZ', 'C. MOISES PEREZ ORTIZ', 'C. MOISES PEREZ ORTIZ', 'C. MOISES PEREZ ORTIZ', '1966-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (68, NULL, 'C. JUAN AGUSTIN APODACA NAFARRATE', 'C. JUAN AGUSTIN APODACA NAFARRATE', 'C. JUAN AGUSTIN APODACA NAFARRATE', 'C. JUAN AGUSTIN APODACA NAFARRATE', '1964-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (69, NULL, 'C. MAURO JOAQUIN ALVARADO TEJERO', 'C. MAURO JOAQUIN ALVARADO TEJERO', 'C. MAURO JOAQUIN ALVARADO TEJERO', 'C. MAURO JOAQUIN ALVARADO TEJERO', '1988-08-15',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (70, NULL, 'C. MAXIMILIANO MOO POOL', 'C. MAXIMILIANO MOO POOL', 'C. MAXIMILIANO MOO POOL', 'C. MAXIMILIANO MOO POOL', '1949-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (71, NULL, 'C. OSCAR OMAR MENDEZ BETETA', 'C. OSCAR OMAR MENDEZ BETETA', 'C. OSCAR OMAR MENDEZ BETETA', 'C. OSCAR OMAR MENDEZ BETETA', '1977-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (72, NULL, 'C. MARTIN RICALDE PECH', 'C. MARTIN RICALDE PECH', 'C. MARTIN RICALDE PECH', 'C. MARTIN RICALDE PECH', '1978-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (73, NULL, 'C. ROGELIO CARMONA NAVA', 'C. ROGELIO CARMONA NAVA', 'C. ROGELIO CARMONA NAVA', 'C. ROGELIO CARMONA NAVA', '1961-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (74, NULL, 'C. NOGUERA ROJAS MIDEY', 'C. NOGUERA ROJAS MIDEY', 'C. NOGUERA ROJAS MIDEY', 'C. NOGUERA ROJAS MIDEY', '1982-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (75, NULL, 'C. LLI CHAN LANDERO', 'C. LLI CHAN LANDERO', 'C. LLI CHAN LANDERO', 'C. LLI CHAN LANDERO', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (76, NULL, 'C. ELDA YUSELMI MEDINA BARAJAS', 'C. ELDA YUSELMI MEDINA BARAJAS', 'C. ELDA YUSELMI MEDINA BARAJAS', 'C. ELDA YUSELMI MEDINA BARAJAS', '1979-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (77, NULL, 'C. GABRIEL ARCANGEL GONZALEZ', 'C. GABRIEL ARCANGEL GONZALEZ', 'C. GABRIEL ARCANGEL GONZALEZ', 'C. GABRIEL ARCANGEL GONZALEZ', '1973-06-19', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (78, NULL, 'C. GERMAN DANIEL MOO CUPUL', 'C. GERMAN DANIEL MOO CUPUL', 'C. GERMAN DANIEL MOO CUPUL', 'C. GERMAN DANIEL MOO CUPUL', '1992-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (79, NULL, 'C. GEOVANNY ULISES COUOH JIMENEZ', 'C. GEOVANNY ULISES COUOH JIMENEZ', 'C. GEOVANNY ULISES COUOH JIMENEZ', 'C. GEOVANNY ULISES COUOH JIMENEZ', '1994-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (80, NULL, 'C. ANDRES HERNANDEZ GOMEZ', 'C. ANDRES HERNANDEZ GOMEZ', 'C. ANDRES HERNANDEZ GOMEZ', 'C. ANDRES HERNANDEZ GOMEZ', '1963-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (81, NULL, 'C. WENDER IVAN MAY RIVERO', 'C. WENDER IVAN MAY RIVERO', 'C. WENDER IVAN MAY RIVERO', 'C. WENDER IVAN MAY RIVERO', '1987-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (82, NULL, 'C. RICARDO JAVIER CASTILLO CORDOVA', 'C. RICARDO JAVIER CASTILLO CORDOVA', 'C. RICARDO JAVIER CASTILLO CORDOVA', 'C. RICARDO JAVIER CASTILLO CORDOVA',
        '1988-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (83, NULL, 'C. CARLOS COLONIA GOMEZ', 'C. CARLOS COLONIA GOMEZ', 'C. CARLOS COLONIA GOMEZ', 'C. CARLOS COLONIA GOMEZ', '1973-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (84, NULL, 'C. SICELA FANNY HERNANDEZ GARCIA', 'C. SICELA FANNY HERNANDEZ GARCIA', 'C. SICELA FANNY HERNANDEZ GARCIA', 'C. SICELA FANNY HERNANDEZ GARCIA', '1974-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (85, NULL, 'C. CLAUDIA IVET DZUL HERNANDEZ', 'C. CLAUDIA IVET DZUL HERNANDEZ', 'C. CLAUDIA IVET DZUL HERNANDEZ', 'C. CLAUDIA IVET DZUL HERNANDEZ', '1986-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (86, NULL, 'C. PATRICK STERLING AGUIRRE SALAZAR', 'C. PATRICK STERLING AGUIRRE SALAZAR', 'C. PATRICK STERLING AGUIRRE SALAZAR', 'C. PATRICK STERLING AGUIRRE SALAZAR',
        '1993-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (87, NULL, 'C. GLORIA MAY MANZANILLA', 'C. GLORIA MAY MANZANILLA', 'C. GLORIA MAY MANZANILLA', 'C. GLORIA MAY MANZANILLA', '1987-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (88, NULL, 'C. YOEL IXTEPAN PUCHETA', 'C. YOEL IXTEPAN PUCHETA', 'C. YOEL IXTEPAN PUCHETA', 'C. YOEL IXTEPAN PUCHETA', '1988-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (89, NULL, 'C. VICTOR MANUEL NAVARRETE TAMAYO', 'C. VICTOR MANUEL NAVARRETE TAMAYO', 'C. VICTOR MANUEL NAVARRETE TAMAYO', 'C. VICTOR MANUEL NAVARRETE TAMAYO', '1947-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (90, NULL, 'C. MINERVA INES HERRERA SANTANA', 'C. MINERVA INES HERRERA SANTANA', 'C. MINERVA INES HERRERA SANTANA', 'C. MINERVA INES HERRERA SANTANA', '1973-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (91, NULL, 'C. JESUS JOHNATAN PAZ ANGULO', 'C. JESUS JOHNATAN PAZ ANGULO', 'C. JESUS JOHNATAN PAZ ANGULO', 'C. JESUS JOHNATAN PAZ ANGULO', '1993-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (92, NULL, 'C. RADIEL POOL POOL', 'C. RADIEL POOL POOL', 'C. RADIEL POOL POOL', 'C. RADIEL POOL POOL', '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (94, NULL, 'C. JOSE MANUEL MUNDO DE LOS REYES', 'C. JOSE MANUEL MUNDO DE LOS REYES', 'C. JOSE MANUEL MUNDO DE LOS REYES', 'C. JOSE MANUEL MUNDO DE LOS REYES', '1983-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (95, NULL, 'C. GUADALUPE PEREZ MENDEZ', 'C. GUADALUPE PEREZ MENDEZ', 'C. GUADALUPE PEREZ MENDEZ', 'C. GUADALUPE PEREZ MENDEZ', '1983-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (96, NULL, 'C. LAURA ALEJANDRA FLOTA BALAM', 'C. LAURA ALEJANDRA FLOTA BALAM', 'C. LAURA ALEJANDRA FLOTA BALAM', 'C. LAURA ALEJANDRA FLOTA BALAM', '1990-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (97, NULL, 'C. JOSE LUIS RODRIGUEZ BOBADILLA', 'C. JOSE LUIS RODRIGUEZ BOBADILLA', 'C. JOSE LUIS RODRIGUEZ BOBADILLA', 'C. JOSE LUIS RODRIGUEZ BOBADILLA', '1986-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (98, NULL, 'C. MARTHA NOEMI FLORES GONZALEZ', 'C. MARTHA NOEMI FLORES GONZALEZ', 'C. MARTHA NOEMI FLORES GONZALEZ', 'C. MARTHA NOEMI FLORES GONZALEZ', '1985-06-18',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (99, NULL, 'C. CARMEN EDELMIRA ONOFRE MAY', 'C. CARMEN EDELMIRA ONOFRE MAY', 'C. CARMEN EDELMIRA ONOFRE MAY', 'C. CARMEN EDELMIRA ONOFRE MAY', '1966-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (100, NULL, 'C. FELIPE HERRERA KETZ', 'C. FELIPE HERRERA KETZ', 'C. FELIPE HERRERA KETZ', 'C. FELIPE HERRERA KETZ', '1961-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (101, NULL, 'C. ENRIQUE SANCHEZ PAT', 'C. ENRIQUE SANCHEZ PAT', 'C. ENRIQUE SANCHEZ PAT', 'C. ENRIQUE SANCHEZ PAT', '1965-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (102, NULL, 'C. BRICEÑO CANO ALFREDO ALUDONARIO', 'C. BRICEÑO CANO ALFREDO ALUDONARIO', 'C. BRICEÑO CANO ALFREDO ALUDONARIO', 'C. BRICEÑO CANO ALFREDO ALUDONARIO',
        '1954-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (103, NULL, 'C. BRICEÑO MEDINA JESUS ORLANDO', 'C. BRICEÑO MEDINA JESUS ORLANDO', 'C. BRICEÑO MEDINA JESUS ORLANDO', 'C. BRICEÑO MEDINA JESUS ORLANDO', '1978-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (104, NULL, 'C. CAAMAL RAMIREZ JOSE AGUSTIN', 'C. CAAMAL RAMIREZ JOSE AGUSTIN', 'C. CAAMAL RAMIREZ JOSE AGUSTIN', 'C. CAAMAL RAMIREZ JOSE AGUSTIN', '1973-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (105, NULL, 'C. CANUL AMARO GUILLERMO JOSE', 'C. CANUL AMARO GUILLERMO JOSE', 'C. CANUL AMARO GUILLERMO JOSE', 'C. CANUL AMARO GUILLERMO JOSE', '1982-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (106, NULL, 'C. COCOM UTRILLA FERNANDO JOSE', 'C. COCOM UTRILLA FERNANDO JOSE', 'C. COCOM UTRILLA FERNANDO JOSE', 'C. COCOM UTRILLA FERNANDO JOSE', '1987-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (107, NULL, 'C. DOMINGUEZ GALERA JULIO CESAR', 'C. DOMINGUEZ GALERA JULIO CESAR', 'C. DOMINGUEZ GALERA JULIO CESAR', 'C. DOMINGUEZ GALERA JULIO CESAR', '1970-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (108, NULL, 'C. DZUL SANTIAGO RODOLFO', 'C. DZUL SANTIAGO RODOLFO', 'C. DZUL SANTIAGO RODOLFO', 'C. DZUL SANTIAGO RODOLFO', '1979-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (109, NULL, 'C. GOMEZ MAIJE LUIS IGNACIO', 'C. GOMEZ MAIJE LUIS IGNACIO', 'C. GOMEZ MAIJE LUIS IGNACIO', 'C. GOMEZ MAIJE LUIS IGNACIO', '1953-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (110, NULL, 'C. IUIT DEL ANGEL AARON SANTIAGO', 'C. IUIT DEL ANGEL AARON SANTIAGO', 'C. IUIT DEL ANGEL AARON SANTIAGO', 'C. IUIT DEL ANGEL AARON SANTIAGO', '1994-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (111, NULL, 'C. KAHUIL CURZ JORGE GABRIEL', 'C. KAHUIL CURZ JORGE GABRIEL', 'C. KAHUIL CURZ JORGE GABRIEL', 'C. KAHUIL CURZ JORGE GABRIEL', '1982-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (112, NULL, 'C. MAGIL CANUL ARMANDO ANTONIO', 'C. MAGIL CANUL ARMANDO ANTONIO', 'C. MAGIL CANUL ARMANDO ANTONIO', 'C. MAGIL CANUL ARMANDO ANTONIO', '1988-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (113, NULL, 'C. OJEDA LABASTIDA ENRIQUE', 'C. OJEDA LABASTIDA ENRIQUE', 'C. OJEDA LABASTIDA ENRIQUE', 'C. OJEDA LABASTIDA ENRIQUE', '1971-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (114, NULL, 'C. OSORIO JIMENEZ HIPOLITO', 'C. OSORIO JIMENEZ HIPOLITO', 'C. OSORIO JIMENEZ HIPOLITO', 'C. OSORIO JIMENEZ HIPOLITO', '1978-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (115, NULL, 'C. PALOMO WILBERTH JAVIER', 'C. PALOMO WILBERTH JAVIER', 'C. PALOMO WILBERTH JAVIER', 'C. PALOMO WILBERTH JAVIER', '1978-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (116, NULL, 'C. PEREZ AVILEZ INES MARISOL', 'C. PEREZ AVILEZ INES MARISOL', 'C. PEREZ AVILEZ INES MARISOL', 'C. PEREZ AVILEZ INES MARISOL', '1976-06-18', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (117, NULL, 'C. POOT TURRIZA WILMER ARTEMIO', 'C. POOT TURRIZA WILMER ARTEMIO', 'C. POOT TURRIZA WILMER ARTEMIO', 'C. POOT TURRIZA WILMER ARTEMIO', '1982-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (118, NULL, 'C. ROSADO SANCHEZ LUIS ALFONSO', 'C. ROSADO SANCHEZ LUIS ALFONSO', 'C. ROSADO SANCHEZ LUIS ALFONSO', 'C. ROSADO SANCHEZ LUIS ALFONSO', '1987-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (119, NULL, 'C. GERARDO ALEJANDRO YAM CARDENAS', 'C. GERARDO ALEJANDRO YAM CARDENAS', 'C. GERARDO ALEJANDRO YAM CARDENAS', 'C. GERARDO ALEJANDRO YAM CARDENAS', '1982-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (120, NULL, 'C. WILLIAM RAUL GONGORA JIMENEZ', 'C. WILLIAM RAUL GONGORA JIMENEZ', 'C. WILLIAM RAUL GONGORA JIMENEZ', 'C. WILLIAM RAUL GONGORA JIMENEZ', '1983-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (121, NULL, 'C. JESUS ENRIQUE CASTILLO PACHECO', 'C. JESUS ENRIQUE CASTILLO PACHECO', 'C. JESUS ENRIQUE CASTILLO PACHECO', 'C. JESUS ENRIQUE CASTILLO PACHECO', '1972-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (122, NULL, 'C. UH US ALVARO', 'C. UH US ALVARO', 'C. UH US ALVARO', 'C. UH US ALVARO', '1972-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (123, NULL, 'C. UH US JULIO CESAR', 'C. UH US JULIO CESAR', 'C. UH US JULIO CESAR', 'C. UH US JULIO CESAR', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (124, NULL, 'C. UN CHIMAL JUAN ANTONIO', 'C. UN CHIMAL JUAN ANTONIO', 'C. UN CHIMAL JUAN ANTONIO', 'C. UN CHIMAL JUAN ANTONIO', '1967-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (125, NULL, 'C. UN PAT NORMA E.', 'C. UN PAT NORMA E.', 'C. UN PAT NORMA E.', 'C. UN PAT NORMA E.', '1989-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (126, NULL, 'C. VAZQUEZ MEJIA JUAN ANTONIO', 'C. VAZQUEZ MEJIA JUAN ANTONIO', 'C. VAZQUEZ MEJIA JUAN ANTONIO', 'C. VAZQUEZ MEJIA JUAN ANTONIO', '1984-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (127, NULL, 'C. JOSE ARIEL BRICEÑO HERNANDEZ', 'C. JOSE ARIEL BRICEÑO HERNANDEZ', 'C. JOSE ARIEL BRICEÑO HERNANDEZ', 'C. JOSE ARIEL BRICEÑO HERNANDEZ', '1959-06-18',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (128, NULL, 'C. FRANCISCO ALEJANDRO AC GARRIDO', 'C. FRANCISCO ALEJANDRO AC GARRIDO', 'C. FRANCISCO ALEJANDRO AC GARRIDO', 'C. FRANCISCO ALEJANDRO AC GARRIDO', '1979-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (129, NULL, 'C. CARLOS ALBERTO CORTES BLANQUET', 'C. CARLOS ALBERTO CORTES BLANQUET', 'C. CARLOS ALBERTO CORTES BLANQUET', 'C. CARLOS ALBERTO CORTES BLANQUET', '1962-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (130, NULL, 'C. ORLANDO DE JESUS REYES ANDRADE', 'C. ORLANDO DE JESUS REYES ANDRADE', 'C. ORLANDO DE JESUS REYES ANDRADE', 'C. ORLANDO DE JESUS REYES ANDRADE', '1970-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (131, NULL, 'C. CHARLES CAMBRANO VELUETA', 'C. CHARLES CAMBRANO VELUETA', 'C. CHARLES CAMBRANO VELUETA', 'C. CHARLES CAMBRANO VELUETA', '1976-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (132, NULL, 'C. HUGO MIGUEL CACERES FUENTES', 'C. HUGO MIGUEL CACERES FUENTES', 'C. HUGO MIGUEL CACERES FUENTES', 'C. HUGO MIGUEL CACERES FUENTES', '1979-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (133, NULL, 'C. JOSE WILBERTH RODRIGUEZ AGUILAR', 'C. JOSE WILBERTH RODRIGUEZ AGUILAR', 'C. JOSE WILBERTH RODRIGUEZ AGUILAR', 'C. JOSE WILBERTH RODRIGUEZ AGUILAR',
        '1966-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (134, NULL, 'C. JESUS PAZ NAVARRO', 'C. JESUS PAZ NAVARRO', 'C. JESUS PAZ NAVARRO', 'C. JESUS PAZ NAVARRO', '1969-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (135, NULL, 'C. JULIO MARCIAL SALAZAR VAZQUEZ', 'C. JULIO MARCIAL SALAZAR VAZQUEZ', 'C. JULIO MARCIAL SALAZAR VAZQUEZ', 'C. JULIO MARCIAL SALAZAR VAZQUEZ', '1967-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (136, NULL, 'C. JOSE ANTONIO BOJORGUEZ HERRERA', 'C. JOSE ANTONIO BOJORGUEZ HERRERA', 'C. JOSE ANTONIO BOJORGUEZ HERRERA', 'C. JOSE ANTONIO BOJORGUEZ HERRERA', '1971-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (137, NULL, 'C. MANUEL JESUS MOO PEREZ', 'C. MANUEL JESUS MOO PEREZ', 'C. MANUEL JESUS MOO PEREZ', 'C. MANUEL JESUS MOO PEREZ', '1971-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (138, NULL, 'C. JORGE AGUIRRE HERNANDEZ', 'C. JORGE AGUIRRE HERNANDEZ', 'C. JORGE AGUIRRE HERNANDEZ', 'C. JORGE AGUIRRE HERNANDEZ', '1971-06-18', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (139, NULL, 'C. JESUS ALFONSO CAAMAL ACEVEDO', 'C. JESUS ALFONSO CAAMAL ACEVEDO', 'C. JESUS ALFONSO CAAMAL ACEVEDO', 'C. JESUS ALFONSO CAAMAL ACEVEDO', '1977-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (140, NULL, 'C. EDWIN GEOVANNI GOMEZ RAMIREZ', 'C. EDWIN GEOVANNI GOMEZ RAMIREZ', 'C. EDWIN GEOVANNI GOMEZ RAMIREZ', 'C. EDWIN GEOVANNI GOMEZ RAMIREZ', '1979-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (141, NULL, 'C. ISRAEL BAXIN ANTELE', 'C. ISRAEL BAXIN ANTELE', 'C. ISRAEL BAXIN ANTELE', 'C. ISRAEL BAXIN ANTELE', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (142, NULL, 'C. ALFONSO MOO PEREZ', 'C. ALFONSO MOO PEREZ', 'C. ALFONSO MOO PEREZ', 'C. ALFONSO MOO PEREZ', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (143, NULL, 'C. JOSE ELIAS GOMEZ VILLANUEVA', 'C. JOSE ELIAS GOMEZ VILLANUEVA', 'C. JOSE ELIAS GOMEZ VILLANUEVA', 'C. JOSE ELIAS GOMEZ VILLANUEVA', '1978-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (144, NULL, 'C. MANUEL ENRIQUE POOT HAW', 'C. MANUEL ENRIQUE POOT HAW', 'C. MANUEL ENRIQUE POOT HAW', 'C. MANUEL ENRIQUE POOT HAW', '1983-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (145, NULL, 'C. GUADALUPE BRICEÑO PACHECO', 'C. GUADALUPE BRICEÑO PACHECO', 'C. GUADALUPE BRICEÑO PACHECO', 'C. GUADALUPE BRICEÑO PACHECO', '1989-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (146, NULL, 'C. KARINA VERA SOLIS', 'C. KARINA VERA SOLIS', 'C. KARINA VERA SOLIS', 'C. KARINA VERA SOLIS', '1978-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (147, NULL, 'C. EDUARDO ARTURO VAZQUEZ MARTINEZ', 'C. EDUARDO ARTURO VAZQUEZ MARTINEZ', 'C. EDUARDO ARTURO VAZQUEZ MARTINEZ', 'C. EDUARDO ARTURO VAZQUEZ MARTINEZ',
        '1975-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (148, NULL, 'C. MARIA ELIZABETH BROCA QUIROZ', 'C. MARIA ELIZABETH BROCA QUIROZ', 'C. MARIA ELIZABETH BROCA QUIROZ', 'C. MARIA ELIZABETH BROCA QUIROZ', '1968-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (149, NULL, 'C. JUDITH ROSALIA CUERVO MARRUFO', 'C. JUDITH ROSALIA CUERVO MARRUFO', 'C. JUDITH ROSALIA CUERVO MARRUFO', 'C. JUDITH ROSALIA CUERVO MARRUFO', '1973-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (150, NULL, 'C. JUAN PABLO ORTEGON TUN', 'C. JUAN PABLO ORTEGON TUN', 'C. JUAN PABLO ORTEGON TUN', 'C. JUAN PABLO ORTEGON TUN', '1983-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (151, NULL, 'C. JUAN ALBERTO BOLAÑOS RIOS', 'C. JUAN ALBERTO BOLAÑOS RIOS', 'C. JUAN ALBERTO BOLAÑOS RIOS', 'C. JUAN ALBERTO BOLAÑOS RIOS', '1986-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (152, NULL, 'C. LEONARDO HERMENEGILDO CHIN CHABLE', 'C. LEONARDO HERMENEGILDO CHIN CHABLE', 'C. LEONARDO HERMENEGILDO CHIN CHABLE', 'C. LEONARDO HERMENEGILDO CHIN CHABLE',
        '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (153, NULL, 'C. ROMUALDO MARTINEZ JIMENEZ', 'C. ROMUALDO MARTINEZ JIMENEZ', 'C. ROMUALDO MARTINEZ JIMENEZ', 'C. ROMUALDO MARTINEZ JIMENEZ', '1988-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (154, NULL, 'C. JULIO PEREZ MANZANILLA', 'C. JULIO PEREZ MANZANILLA', 'C. JULIO PEREZ MANZANILLA', 'C. JULIO PEREZ MANZANILLA', '1975-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (155, NULL, 'C. CHAVEZ VAZQUEZ NAYELI YESENIA', 'C. CHAVEZ VAZQUEZ NAYELI YESENIA', 'C. CHAVEZ VAZQUEZ NAYELI YESENIA', 'C. CHAVEZ VAZQUEZ NAYELI YESENIA', '1997-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (156, NULL, 'C. DR. PEDRO CHRISTIAN MIS AVILA', 'C. DR. PEDRO CHRISTIAN MIS AVILA', 'C. DR. PEDRO CHRISTIAN MIS AVILA', 'C. DR. PEDRO CHRISTIAN MIS AVILA', '1978-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (157, NULL, 'C. GERARDO ALBERTO RODRIGUEZ MARTINEZ', 'C. GERARDO ALBERTO RODRIGUEZ MARTINEZ', 'C. GERARDO ALBERTO RODRIGUEZ MARTINEZ',
        'C. GERARDO ALBERTO RODRIGUEZ MARTINEZ', '1981-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (158, NULL, 'C. RAQUEL BAACK VALLE', 'C. RAQUEL BAACK VALLE', 'C. RAQUEL BAACK VALLE', 'C. RAQUEL BAACK VALLE', '1986-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (159, NULL, 'DR. MARCO ANTONIO DOMINGUEZ GALERA', 'DR. MARCO ANTONIO DOMINGUEZ GALERA', 'DR. MARCO ANTONIO DOMINGUEZ GALERA', 'DR. MARCO ANTONIO DOMINGUEZ GALERA',
        '1971-06-19', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (160, NULL, 'C. MARIBEL MARTINEZ BURGOS', 'C. MARIBEL MARTINEZ BURGOS', 'C. MARIBEL MARTINEZ BURGOS', 'C. MARIBEL MARTINEZ BURGOS', '1995-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (161, NULL, 'C. LILIANA MACIAS ADAME', 'C. LILIANA MACIAS ADAME', 'C. LILIANA MACIAS ADAME', 'C. LILIANA MACIAS ADAME', '1985-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (162, NULL, 'C. LEODEGARIO GRACIA GARCIA', 'C. LEODEGARIO GRACIA GARCIA', 'C. LEODEGARIO GRACIA GARCIA', 'C. LEODEGARIO GRACIA GARCIA', '1967-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (163, NULL, 'C.PERSEO CHICLIN RAMOS', 'C.PERSEO CHICLIN RAMOS', 'C.PERSEO CHICLIN RAMOS', 'C.PERSEO CHICLIN RAMOS', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (164, NULL, 'C. FREDDY A. DOMINGUEZ GARCIA', 'C. FREDDY A. DOMINGUEZ GARCIA', 'C. FREDDY A. DOMINGUEZ GARCIA', 'C. FREDDY A. DOMINGUEZ GARCIA', '1986-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (165, NULL, 'C. KEREN GOMEZ RODRIGUEZ', 'C. KEREN GOMEZ RODRIGUEZ', 'C. KEREN GOMEZ RODRIGUEZ', 'C. KEREN GOMEZ RODRIGUEZ', '1991-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (166, NULL, 'C. LUIS CONTRERAS FLORES', 'C. LUIS CONTRERAS FLORES', 'C. LUIS CONTRERAS FLORES', 'C. LUIS CONTRERAS FLORES', '1993-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (167, NULL, 'C. RAHUEL JEREMIAS CHAN CHABLE', 'C. RAHUEL JEREMIAS CHAN CHABLE', 'C. RAHUEL JEREMIAS CHAN CHABLE', 'C. RAHUEL JEREMIAS CHAN CHABLE', '1991-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (168, NULL, 'C. EDGARDO BALAM POOT', 'C. EDGARDO BALAM POOT', 'C. EDGARDO BALAM POOT', 'C. EDGARDO BALAM POOT', '1983-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (169, NULL, 'C. VANESSA CONTRERAS LUGO', 'C. VANESSA CONTRERAS LUGO', 'C. VANESSA CONTRERAS LUGO', 'C. VANESSA CONTRERAS LUGO', '1990-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (170, NULL, 'C.GUILLERMO ESCALANTE POOT', 'C.GUILLERMO ESCALANTE POOT', 'C.GUILLERMO ESCALANTE POOT', 'C.GUILLERMO ESCALANTE POOT', '1992-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (171, NULL, 'C. GEMMA MACIAS ADAME', 'C. GEMMA MACIAS ADAME', 'C. GEMMA MACIAS ADAME', 'C. GEMMA MACIAS ADAME', '1987-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (172, NULL, 'C. GONZALO PEREZ BAÑOS', 'C. GONZALO PEREZ BAÑOS', 'C. GONZALO PEREZ BAÑOS', 'C. GONZALO PEREZ BAÑOS', '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (173, NULL, 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', '1981-01-01', 'MASCULINO', '9871007560',
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (174, NULL, 'C. JOSE ALFREDO JIMENEZ TUN', 'C. JOSE ALFREDO JIMENEZ TUN', 'C. JOSE ALFREDO JIMENEZ TUN', 'C. JOSE ALFREDO JIMENEZ TUN', '1978-01-01', 'MASCULINO',
        '9871189542', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (175, NULL, 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', '1972-01-01', 'MASCULINO',
        '9831190176', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (176, NULL, 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', 'C. EFRAIN ATOCHA REJON SANTOS', '1972-01-01', 'MASCULINO',
        '9831190176', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (177, NULL, 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', '1955-01-01', 'MASCULINO',
        '9831022273', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (178, NULL, 'C. RODRIGUEZ AREVALO REYNALDO', 'C. RODRIGUEZ AREVALO REYNALDO', 'C. RODRIGUEZ AREVALO REYNALDO', 'C. RODRIGUEZ AREVALO REYNALDO', '1969-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (179, NULL, 'C. CAAMAL POOL OMAR ALEJANDRO', 'C. CAAMAL POOL OMAR ALEJANDRO', 'C. CAAMAL POOL OMAR ALEJANDRO', 'C. CAAMAL POOL OMAR ALEJANDRO', '1987-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (180, NULL, 'C. EUAN CHAN JOSE AURELIO', 'C. EUAN CHAN JOSE AURELIO', 'C. EUAN CHAN JOSE AURELIO', 'C. EUAN CHAN JOSE AURELIO', '1986-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (181, NULL, 'C. CETINA SUASTE PEDRO FLORENCIO', 'C. CETINA SUASTE PEDRO FLORENCIO', 'C. CETINA SUASTE PEDRO FLORENCIO', 'C. CETINA SUASTE PEDRO FLORENCIO', '1983-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (182, NULL, 'C. UC KU GABRIEL ARCANGEL', 'C. UC KU GABRIEL ARCANGEL', 'C. UC KU GABRIEL ARCANGEL', 'C. UC KU GABRIEL ARCANGEL', '1970-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (183, NULL, 'C. XOOL CHIMAL MANEUL JESUS', 'C. XOOL CHIMAL MANEUL JESUS', 'C. XOOL CHIMAL MANEUL JESUS', 'C. XOOL CHIMAL MANEUL JESUS', '1970-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (184, NULL, 'C. DZIB CHAN ELIO ADRIANO', 'C. DZIB CHAN ELIO ADRIANO', 'C. DZIB CHAN ELIO ADRIANO', 'C. DZIB CHAN ELIO ADRIANO', '1990-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (185, NULL, 'C. CANUL BALAM JUAN GUALBERTO', 'C. CANUL BALAM JUAN GUALBERTO', 'C. CANUL BALAM JUAN GUALBERTO', 'C. CANUL BALAM JUAN GUALBERTO', '1971-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (186, NULL, 'C. SALINAS ORDAS EDUARDO', 'C. SALINAS ORDAS EDUARDO', 'C. SALINAS ORDAS EDUARDO', 'C. SALINAS ORDAS EDUARDO', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (187, NULL, 'C. KOYOC PECH ANGEL GUSTAVO', 'C. KOYOC PECH ANGEL GUSTAVO', 'C. KOYOC PECH ANGEL GUSTAVO', 'C. KOYOC PECH ANGEL GUSTAVO', '1996-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (188, NULL, 'C. ALDECUA ABAN GENNY TERESA', 'C. ALDECUA ABAN GENNY TERESA', 'C. ALDECUA ABAN GENNY TERESA', 'C. ALDECUA ABAN GENNY TERESA', '1994-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (189, NULL, 'C. XOOC HAU CLEMENTE', 'C. XOOC HAU CLEMENTE', 'C. XOOC HAU CLEMENTE', 'C. XOOC HAU CLEMENTE', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (190, NULL, 'C. KULMUL TUZ MIGUEL ANGEL', 'C. KULMUL TUZ MIGUEL ANGEL', 'C. KULMUL TUZ MIGUEL ANGEL', 'C. KULMUL TUZ MIGUEL ANGEL', '1991-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (191, NULL, 'C. EUAN PECH JUAN DE LA CRUZ', 'C. EUAN PECH JUAN DE LA CRUZ', 'C. EUAN PECH JUAN DE LA CRUZ', 'C. EUAN PECH JUAN DE LA CRUZ', '1978-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (192, NULL, 'C. PECH PANTI JOSE NICOLAS', 'C. PECH PANTI JOSE NICOLAS', 'C. PECH PANTI JOSE NICOLAS', 'C. PECH PANTI JOSE NICOLAS', '1974-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (193, NULL, 'C. UC HCI EITER PASCUAL', 'C. UC HCI EITER PASCUAL', 'C. UC HCI EITER PASCUAL', 'C. UC HCI EITER PASCUAL', '1978-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (194, NULL, 'C. COLLI POMOL ADA ISELA', 'C. COLLI POMOL ADA ISELA', 'C. COLLI POMOL ADA ISELA', 'C. COLLI POMOL ADA ISELA', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (195, NULL, 'C. TORRES PEREZ MANUEL', 'C. TORRES PEREZ MANUEL', 'C. TORRES PEREZ MANUEL', 'C. TORRES PEREZ MANUEL', '1959-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (196, NULL, 'C. JIMENEZ SALVADOR ANGEL', 'C. JIMENEZ SALVADOR ANGEL', 'C. JIMENEZ SALVADOR ANGEL', 'C. JIMENEZ SALVADOR ANGEL', '1971-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (197, NULL, 'C. ROSADO MEDINA WILBERTH GUALBERTO', 'C. ROSADO MEDINA WILBERTH GUALBERTO', 'C. ROSADO MEDINA WILBERTH GUALBERTO', 'C. ROSADO MEDINA WILBERTH GUALBERTO',
        '1972-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (198, NULL, 'C. SUNZA POOT ELBERTH GEDEONY', 'C. SUNZA POOT ELBERTH GEDEONY', 'C. SUNZA POOT ELBERTH GEDEONY', 'C. SUNZA POOT ELBERTH GEDEONY', '1992-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (199, NULL, 'C. MONTEJO AVENDAÑO FRANCISCO', 'C. MONTEJO AVENDAÑO FRANCISCO', 'C. MONTEJO AVENDAÑO FRANCISCO', 'C. MONTEJO AVENDAÑO FRANCISCO', '1977-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (200, NULL, 'C. GONZALEZ VARGAS EDWIN MANUEL', 'C. GONZALEZ VARGAS EDWIN MANUEL', 'C. GONZALEZ VARGAS EDWIN MANUEL', 'C. GONZALEZ VARGAS EDWIN MANUEL', '1991-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (201, NULL, 'C. ADOLFO TORRES ONTIVEROS', 'C. ADOLFO TORRES ONTIVEROS', 'C. ADOLFO TORRES ONTIVEROS', 'C. ADOLFO TORRES ONTIVEROS', '1974-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (202, NULL, 'C. GARCIA GONZALEZ RENE', 'C. GARCIA GONZALEZ RENE', 'C. GARCIA GONZALEZ RENE', 'C. GARCIA GONZALEZ RENE', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (203, NULL, 'C. MENESES PADILLA JOSE LUIS', 'C. MENESES PADILLA JOSE LUIS', 'C. MENESES PADILLA JOSE LUIS', 'C. MENESES PADILLA JOSE LUIS', '1972-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (204, NULL, 'C. ABRAHAM MACIAS ROJAS', 'C. ABRAHAM MACIAS ROJAS', 'C. ABRAHAM MACIAS ROJAS', 'C. ABRAHAM MACIAS ROJAS', '1983-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (205, NULL, 'C. MARIO ALBERTO ITZA CHAN', 'C. MARIO ALBERTO ITZA CHAN', 'C. MARIO ALBERTO ITZA CHAN', 'C. MARIO ALBERTO ITZA CHAN', '1988-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (206, NULL, 'C. MARCIAL TUN CANUL', 'C. MARCIAL TUN CANUL', 'C. MARCIAL TUN CANUL', 'C. MARCIAL TUN CANUL', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (207, NULL, 'C. JONATHAN NIETO GONZALEZ', 'C. JONATHAN NIETO GONZALEZ', 'C. JONATHAN NIETO GONZALEZ', 'C. JONATHAN NIETO GONZALEZ', '1989-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (208, NULL, 'C. JOSE BARTOLO MAS ESTRELLA', 'C. JOSE BARTOLO MAS ESTRELLA', 'C. JOSE BARTOLO MAS ESTRELLA', 'C. JOSE BARTOLO MAS ESTRELLA', '1990-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (209, NULL, 'C,TENOLIO CARDONA JUANA MARIA', 'C,TENOLIO CARDONA JUANA MARIA', 'C,TENOLIO CARDONA JUANA MARIA', 'C,TENOLIO CARDONA JUANA MARIA', '1967-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (210, NULL, 'MANUEL IGNACIO CHIMAL GONZALEZ', 'MANUEL IGNACIO CHIMAL GONZALEZ', 'MANUEL IGNACIO CHIMAL GONZALEZ', 'MANUEL IGNACIO CHIMAL GONZALEZ', '1975-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (211, NULL, 'C. MORENO GONZALEZ GABRIELA', 'C. MORENO GONZALEZ GABRIELA', 'C. MORENO GONZALEZ GABRIELA', 'C. MORENO GONZALEZ GABRIELA', '1975-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (212, NULL, 'C. BERENICE SULUB CORONADO', 'C. BERENICE SULUB CORONADO', 'C. BERENICE SULUB CORONADO', 'C. BERENICE SULUB CORONADO', '1982-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (213, NULL, 'C. GRACIELA MORALES SALAYA', 'C. GRACIELA MORALES SALAYA', 'C. GRACIELA MORALES SALAYA', 'C. GRACIELA MORALES SALAYA', '1988-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (214, NULL, 'C. DIAZ GOMEZ MERCEDES', 'C. DIAZ GOMEZ MERCEDES', 'C. DIAZ GOMEZ MERCEDES', 'C. DIAZ GOMEZ MERCEDES', '1961-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (215, NULL, 'C. SERGIO MAYO GARDUZA', 'C. SERGIO MAYO GARDUZA', 'C. SERGIO MAYO GARDUZA', 'C. SERGIO MAYO GARDUZA', '1958-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (216, NULL, 'C. KU PECH MARIA TERESITA', 'C. KU PECH MARIA TERESITA', 'C. KU PECH MARIA TERESITA', 'C. KU PECH MARIA TERESITA', '1977-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (217, NULL, 'C. MARIO RIVADENEYRA CORTES', 'C. MARIO RIVADENEYRA CORTES', 'C. MARIO RIVADENEYRA CORTES', 'C. MARIO RIVADENEYRA CORTES', '1975-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (218, NULL, 'C. ZARAGOZA MARIA', 'C. ZARAGOZA MARIA', 'C. ZARAGOZA MARIA', 'C. ZARAGOZA MARIA', '1967-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (219, NULL, 'C. JOAQUIN VALENCIA QUIROZ', 'C. JOAQUIN VALENCIA QUIROZ', 'C. JOAQUIN VALENCIA QUIROZ', 'C. JOAQUIN VALENCIA QUIROZ', '1974-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (220, NULL, 'C. LUIS DANIEL NAAL CALDERON', 'C. LUIS DANIEL NAAL CALDERON', 'C. LUIS DANIEL NAAL CALDERON', 'C. LUIS DANIEL NAAL CALDERON', '1961-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (221, NULL, 'C. ALMEIDA GARCIA LEYSIS', 'C. ALMEIDA GARCIA LEYSIS', 'C. ALMEIDA GARCIA LEYSIS', 'C. ALMEIDA GARCIA LEYSIS', '1980-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (222, NULL, 'C. WILBERTH CRUZ FLOTA', 'C. WILBERTH CRUZ FLOTA', 'C. WILBERTH CRUZ FLOTA', 'C. WILBERTH CRUZ FLOTA', '1969-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (223, NULL, 'C. CHI UICAB LUCIA GUADALUPE', 'C. CHI UICAB LUCIA GUADALUPE', 'C. CHI UICAB LUCIA GUADALUPE', 'C. CHI UICAB LUCIA GUADALUPE', '2018-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (224, NULL, 'C. ADRIAN DURAN FLORES', 'C. ADRIAN DURAN FLORES', 'C. ADRIAN DURAN FLORES', 'C. ADRIAN DURAN FLORES', '1986-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (225, NULL, 'C. CHAN BALAM MARTINA DEL ROSARIO', 'C. CHAN BALAM MARTINA DEL ROSARIO', 'C. CHAN BALAM MARTINA DEL ROSARIO', 'C. CHAN BALAM MARTINA DEL ROSARIO', '1965-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (226, NULL, 'C. CAAMAL POOL GABRIELA AURORA', 'C. CAAMAL POOL GABRIELA AURORA', 'C. CAAMAL POOL GABRIELA AURORA', 'C. CAAMAL POOL GABRIELA AURORA', '1983-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (227, NULL, 'C. LUIS MANUEL SALCEDO', 'C. LUIS MANUEL SALCEDO', 'C. LUIS MANUEL SALCEDO', 'C. LUIS MANUEL SALCEDO', '1973-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (228, NULL, 'C. CESAR SABIDO ABAN', 'C. CESAR SABIDO ABAN', 'C. CESAR SABIDO ABAN', 'C. CESAR SABIDO ABAN', '1983-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (229, NULL, 'C. RAUL GALAZ VALEZ', 'C. RAUL GALAZ VALEZ', 'C. RAUL GALAZ VALEZ', 'C. RAUL GALAZ VALEZ', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (230, NULL, 'C. BLANCA PATRICIA MANZO BENITEZ', 'C. BLANCA PATRICIA MANZO BENITEZ', 'C. BLANCA PATRICIA MANZO BENITEZ', 'C. BLANCA PATRICIA MANZO BENITEZ', '1968-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (231, NULL, 'C. DALIA MADRIGAL MADRIGAL', 'C. DALIA MADRIGAL MADRIGAL', 'C. DALIA MADRIGAL MADRIGAL', 'C. DALIA MADRIGAL MADRIGAL', '1975-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (232, NULL, 'C. GABRIELA MILAN SILVA', 'C. GABRIELA MILAN SILVA', 'C. GABRIELA MILAN SILVA', 'C. GABRIELA MILAN SILVA', '1982-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (233, NULL, 'C. MARIA JOSE CRUZ', 'C. MARIA JOSE CRUZ', 'C. MARIA JOSE CRUZ', 'C. MARIA JOSE CRUZ', '1995-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (234, NULL, 'C. MARCEILINE AQUINO VAZQUEZ', 'C. MARCEILINE AQUINO VAZQUEZ', 'C. MARCEILINE AQUINO VAZQUEZ', 'C. MARCEILINE AQUINO VAZQUEZ', '1996-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (235, NULL, 'C. RODRIGUEZ SANCHEZ GASTON JOSE', 'C. RODRIGUEZ SANCHEZ GASTON JOSE', 'C. RODRIGUEZ SANCHEZ GASTON JOSE', 'C. RODRIGUEZ SANCHEZ GASTON JOSE', '1972-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (236, NULL, 'C. ALEJANDRO AGUILAR YAM', 'C. ALEJANDRO AGUILAR YAM', 'C. ALEJANDRO AGUILAR YAM', 'C. ALEJANDRO AGUILAR YAM', '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (237, NULL, 'C. VAZQUEZ JUAREZ DIDIER MAURICIO', 'C. VAZQUEZ JUAREZ DIDIER MAURICIO', 'C. VAZQUEZ JUAREZ DIDIER MAURICIO', 'C. VAZQUEZ JUAREZ DIDIER MAURICIO', '1985-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (238, NULL, 'C. DIANA GUEVARA FARIAS', 'C. DIANA GUEVARA FARIAS', 'C. DIANA GUEVARA FARIAS', 'C. DIANA GUEVARA FARIAS', '1979-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (239, NULL, 'C, INTERIAN EK GILBERTO', 'C, INTERIAN EK GILBERTO', 'C, INTERIAN EK GILBERTO', 'C, INTERIAN EK GILBERTO', '1978-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (240, NULL, 'C. MARCO ANTONIO COTA OCHOA', 'C. MARCO ANTONIO COTA OCHOA', 'C. MARCO ANTONIO COTA OCHOA', 'C. MARCO ANTONIO COTA OCHOA', '1958-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (241, NULL, 'C. GOMEZ BOLIO AMELIA', 'C. GOMEZ BOLIO AMELIA', 'C. GOMEZ BOLIO AMELIA', 'C. GOMEZ BOLIO AMELIA', '1960-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (242, NULL, 'C. TANIA TUYUB PACHECO', 'C. TANIA TUYUB PACHECO', 'C. TANIA TUYUB PACHECO', 'C. TANIA TUYUB PACHECO', '1989-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (243, NULL, 'C. LOPEZ BAEZA WILLIAM AUDIEL', 'C. LOPEZ BAEZA WILLIAM AUDIEL', 'C. LOPEZ BAEZA WILLIAM AUDIEL', 'C. LOPEZ BAEZA WILLIAM AUDIEL', '1985-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (244, NULL, 'C. LENY PECH MOO', 'C. LENY PECH MOO', 'C. LENY PECH MOO', 'C. LENY PECH MOO', '1985-11-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (245, NULL, 'C. CANCHE SANCHEZ SUEMY ADELARDO', 'C. CANCHE SANCHEZ SUEMY ADELARDO', 'C. CANCHE SANCHEZ SUEMY ADELARDO', 'C. CANCHE SANCHEZ SUEMY ADELARDO', '1976-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (246, NULL, 'C. ELVIRA ROMERO MACARIO', 'C. ELVIRA ROMERO MACARIO', 'C. ELVIRA ROMERO MACARIO', 'C. ELVIRA ROMERO MACARIO', '1985-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (247, NULL, 'C. PEREZ DUQUE FRANCISCA', 'C. PEREZ DUQUE FRANCISCA', 'C. PEREZ DUQUE FRANCISCA', 'C. PEREZ DUQUE FRANCISCA', '1978-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (248, NULL, 'C. MIGUEL SOTO RIVERO', 'C. MIGUEL SOTO RIVERO', 'C. MIGUEL SOTO RIVERO', 'C. MIGUEL SOTO RIVERO', '1994-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (249, NULL, 'C, CHAN CHOC ALBERTO', 'C, CHAN CHOC ALBERTO', 'C, CHAN CHOC ALBERTO', 'C, CHAN CHOC ALBERTO', '1978-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (250, NULL, 'C. GABRIEL FRANCISCO TAPIA CHABLE', 'C. GABRIEL FRANCISCO TAPIA CHABLE', 'C. GABRIEL FRANCISCO TAPIA CHABLE', 'C. GABRIEL FRANCISCO TAPIA CHABLE', '1992-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (251, NULL, 'C. SANDRO GABRIEL SUAREZ XOOL', 'C. SANDRO GABRIEL SUAREZ XOOL', 'C. SANDRO GABRIEL SUAREZ XOOL', 'C. SANDRO GABRIEL SUAREZ XOOL', '1976-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (252, NULL, 'C. LOPEZ MENDEZ EVERARDO', 'C. LOPEZ MENDEZ EVERARDO', 'C. LOPEZ MENDEZ EVERARDO', 'C. LOPEZ MENDEZ EVERARDO', '1974-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (253, NULL, 'C. JONATHAN ANDRES CASTILLO MAGAÑA', 'C. JONATHAN ANDRES CASTILLO MAGAÑA', 'C. JONATHAN ANDRES CASTILLO MAGAÑA', 'C. JONATHAN ANDRES CASTILLO MAGAÑA',
        '1996-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (254, NULL, 'C. CERON CHALE VICTOR MENDEZ', 'C. CERON CHALE VICTOR MENDEZ', 'C. CERON CHALE VICTOR MENDEZ', 'C. CERON CHALE VICTOR MENDEZ', '1992-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (255, NULL, 'C. ESTRELLA PEREZ JUAN CARLOS', 'C. ESTRELLA PEREZ JUAN CARLOS', 'C. ESTRELLA PEREZ JUAN CARLOS', 'C. ESTRELLA PEREZ JUAN CARLOS', '1971-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (256, NULL, 'C. GARCIA GOMEZ DAGOBERTO', 'C. GARCIA GOMEZ DAGOBERTO', 'C. GARCIA GOMEZ DAGOBERTO', 'C. GARCIA GOMEZ DAGOBERTO', '1984-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (257, NULL, 'C. LUISA ESTHER PEREZ SARABIA', 'C. LUISA ESTHER PEREZ SARABIA', 'C. LUISA ESTHER PEREZ SARABIA', 'C. LUISA ESTHER PEREZ SARABIA', '2000-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (258, NULL, 'C. DE LA CRUZ VELAZQUEZ JORGE', 'C. DE LA CRUZ VELAZQUEZ JORGE', 'C. DE LA CRUZ VELAZQUEZ JORGE', 'C. DE LA CRUZ VELAZQUEZ JORGE', '1976-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (259, NULL, 'C. CELINA SARABIA MARTINEZ', 'C. CELINA SARABIA MARTINEZ', 'C. CELINA SARABIA MARTINEZ', 'C. CELINA SARABIA MARTINEZ', '1979-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (260, NULL, 'C. REYNA MENDEZ CARDENAS', 'C. REYNA MENDEZ CARDENAS', 'C. REYNA MENDEZ CARDENAS', 'C. REYNA MENDEZ CARDENAS', '1980-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (261, NULL, 'C. RODRIGUEZ BACELIS MAURO', 'C. RODRIGUEZ BACELIS MAURO', 'C. RODRIGUEZ BACELIS MAURO', 'C. RODRIGUEZ BACELIS MAURO', '1994-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (262, NULL, 'C. HAU SANTIAGO ABRAHAM MISAEL', 'C. HAU SANTIAGO ABRAHAM MISAEL', 'C. HAU SANTIAGO ABRAHAM MISAEL', 'C. HAU SANTIAGO ABRAHAM MISAEL', '1997-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (263, NULL, 'C. ROICER MARTINEZ SEL', 'C. ROICER MARTINEZ SEL', 'C. ROICER MARTINEZ SEL', 'C. ROICER MARTINEZ SEL', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (264, NULL, 'C. ELIASAR MONTALVO CABRERA', 'C. ELIASAR MONTALVO CABRERA', 'C. ELIASAR MONTALVO CABRERA', 'C. ELIASAR MONTALVO CABRERA', '1979-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (265, NULL, 'C. OCHOA GOMEZ JORGE CARLOS', 'C. OCHOA GOMEZ JORGE CARLOS', 'C. OCHOA GOMEZ JORGE CARLOS', 'C. OCHOA GOMEZ JORGE CARLOS', '1991-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (266, NULL, 'C. FELIPE DE JESUS TAPIA CHABLE', 'C. FELIPE DE JESUS TAPIA CHABLE', 'C. FELIPE DE JESUS TAPIA CHABLE', 'C. FELIPE DE JESUS TAPIA CHABLE', '1978-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (267, NULL, 'C. DZUL CHIMAL RENE', 'C. DZUL CHIMAL RENE', 'C. DZUL CHIMAL RENE', 'C. DZUL CHIMAL RENE', '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (268, NULL, 'C. MARTINEZ MONTES JAZMIN', 'C. MARTINEZ MONTES JAZMIN', 'C. MARTINEZ MONTES JAZMIN', 'C. MARTINEZ MONTES JAZMIN', '1973-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (269, NULL, 'C. C. CITUK ESTRELLA JANNETH', 'C. C. CITUK ESTRELLA JANNETH', 'C. C. CITUK ESTRELLA JANNETH', 'C. C. CITUK ESTRELLA JANNETH', '1987-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (270, NULL, 'C. LUGO MOGUEL GERARADO', 'C. LUGO MOGUEL GERARADO', 'C. LUGO MOGUEL GERARADO', 'C. LUGO MOGUEL GERARADO', '1978-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (271, NULL, 'C. LUIS FERNANDO CHI URIBE', 'C. LUIS FERNANDO CHI URIBE', 'C. LUIS FERNANDO CHI URIBE', 'C. LUIS FERNANDO CHI URIBE', '1978-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (272, NULL, 'C. GOMEZ HIDALGO TRINIDAD', 'C. GOMEZ HIDALGO TRINIDAD', 'C. GOMEZ HIDALGO TRINIDAD', 'C. GOMEZ HIDALGO TRINIDAD', '1969-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (273, NULL, 'C. AMBAR GONZALEZ PERERA', 'C. AMBAR GONZALEZ PERERA', 'C. AMBAR GONZALEZ PERERA', 'C. AMBAR GONZALEZ PERERA', '1999-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (274, NULL, 'C. ANA LUISA VAQUERO ARELLANO', 'C. ANA LUISA VAQUERO ARELLANO', 'C. ANA LUISA VAQUERO ARELLANO', 'C. ANA LUISA VAQUERO ARELLANO', '1978-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (275, NULL, 'C. MADERO SANSORES JULIO ALFONSO', 'C. MADERO SANSORES JULIO ALFONSO', 'C. MADERO SANSORES JULIO ALFONSO', 'C. MADERO SANSORES JULIO ALFONSO', '1951-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (276, NULL, 'C. TINAL GORDILLO VICTOR RODOLFO', 'C. TINAL GORDILLO VICTOR RODOLFO', 'C. TINAL GORDILLO VICTOR RODOLFO', 'C. TINAL GORDILLO VICTOR RODOLFO', '1976-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (277, NULL, 'C. INELVA HERLINDA CAAMAL REYES', 'C. INELVA HERLINDA CAAMAL REYES', 'C. INELVA HERLINDA CAAMAL REYES', 'C. INELVA HERLINDA CAAMAL REYES', '1979-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (278, NULL, 'C. CESAR CHACON CHABLE', 'C. CESAR CHACON CHABLE', 'C. CESAR CHACON CHABLE', 'C. CESAR CHACON CHABLE', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (279, NULL, 'C. COHUO KU RENE', 'C. COHUO KU RENE', 'C. COHUO KU RENE', 'C. COHUO KU RENE', '1986-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (280, NULL, 'C. DORA MARIA LOPES ALVAREZ', 'C. DORA MARIA LOPES ALVAREZ', 'C. DORA MARIA LOPES ALVAREZ', 'C. DORA MARIA LOPES ALVAREZ', '1979-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (281, NULL, 'C. LEON BUENFIL JOSE MANUEL', 'C. LEON BUENFIL JOSE MANUEL', 'C. LEON BUENFIL JOSE MANUEL', 'C. LEON BUENFIL JOSE MANUEL', '1985-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (282, NULL, 'C. JORGE FRANCISCO DIAZ REYES', 'C. JORGE FRANCISCO DIAZ REYES', 'C. JORGE FRANCISCO DIAZ REYES', 'C. JORGE FRANCISCO DIAZ REYES', '1990-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (283, NULL, 'C. LORIA QUIJANO MIGUEL ALBERTO', 'C. LORIA QUIJANO MIGUEL ALBERTO', 'C. LORIA QUIJANO MIGUEL ALBERTO', 'C. LORIA QUIJANO MIGUEL ALBERTO', '1977-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (284, NULL, 'C. LETICIA DEL ROSARIO TORRES CERVANTES', 'C. LETICIA DEL ROSARIO TORRES CERVANTES', 'C. LETICIA DEL ROSARIO TORRES CERVANTES',
        'C. LETICIA DEL ROSARIO TORRES CERVANTES', '1988-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (285, NULL, 'C. JOSE ESCAMILLA AGUILAR', 'C. JOSE ESCAMILLA AGUILAR', 'C. JOSE ESCAMILLA AGUILAR', 'C. JOSE ESCAMILLA AGUILAR', '1970-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (286, NULL, 'C. CANO CAAMAL FRANCISCO ALEJANDRO', 'C. CANO CAAMAL FRANCISCO ALEJANDRO', 'C. CANO CAAMAL FRANCISCO ALEJANDRO', 'C. CANO CAAMAL FRANCISCO ALEJANDRO',
        '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (287, NULL, 'C. DAMIAN RAMOS PEREZ', 'C. DAMIAN RAMOS PEREZ', 'C. DAMIAN RAMOS PEREZ', 'C. DAMIAN RAMOS PEREZ', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (288, NULL, 'C. FERRAEZ EK JOSE ALBERTO', 'C. FERRAEZ EK JOSE ALBERTO', 'C. FERRAEZ EK JOSE ALBERTO', 'C. FERRAEZ EK JOSE ALBERTO', '1978-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (289, NULL, 'C. LUIS ALBERTO CASTILLO DZIB', 'C. LUIS ALBERTO CASTILLO DZIB', 'C. LUIS ALBERTO CASTILLO DZIB', 'C. LUIS ALBERTO CASTILLO DZIB', '1986-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (290, NULL, 'C. ROMAN MANCILLA JOSE ANTONIO MIGUEL', 'C. ROMAN MANCILLA JOSE ANTONIO MIGUEL', 'C. ROMAN MANCILLA JOSE ANTONIO MIGUEL',
        'C. ROMAN MANCILLA JOSE ANTONIO MIGUEL', '1994-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (291, NULL, 'C. DANIEL ALEJANDRO KUYOC LORIA', 'C. DANIEL ALEJANDRO KUYOC LORIA', 'C. DANIEL ALEJANDRO KUYOC LORIA', 'C. DANIEL ALEJANDRO KUYOC LORIA', '1986-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (292, NULL, 'C. TUZ VEGA KARINA MARISOL', 'C. TUZ VEGA KARINA MARISOL', 'C. TUZ VEGA KARINA MARISOL', 'C. TUZ VEGA KARINA MARISOL', '1978-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (293, NULL, 'C. VICTOR ALCOCER ARZAPALO', 'C. VICTOR ALCOCER ARZAPALO', 'C. VICTOR ALCOCER ARZAPALO', 'C. VICTOR ALCOCER ARZAPALO', '1975-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (294, NULL, 'C. ANGELICA TORRES CERVANTES', 'C. ANGELICA TORRES CERVANTES', 'C. ANGELICA TORRES CERVANTES', 'C. ANGELICA TORRES CERVANTES', '1986-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (295, NULL, 'C. MARIA LUGO RICALDE', 'C. MARIA LUGO RICALDE', 'C. MARIA LUGO RICALDE', 'C. MARIA LUGO RICALDE', '1988-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (296, NULL, 'C. RODRIGUEZ MAGAÑA JAVIER', 'C. RODRIGUEZ MAGAÑA JAVIER', 'C. RODRIGUEZ MAGAÑA JAVIER', 'C. RODRIGUEZ MAGAÑA JAVIER', '1991-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (297, NULL, 'C. MANUEL KU CAB', 'C. MANUEL KU CAB', 'C. MANUEL KU CAB', 'C. MANUEL KU CAB', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (298, NULL, 'C. CASTILLO ANDRADE RAFAEL ANGEL', 'C. CASTILLO ANDRADE RAFAEL ANGEL', 'C. CASTILLO ANDRADE RAFAEL ANGEL', 'C. CASTILLO ANDRADE RAFAEL ANGEL', '1990-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (299, NULL, 'C. ALVARO MARTINEZ GOMEZ', 'C. ALVARO MARTINEZ GOMEZ', 'C. ALVARO MARTINEZ GOMEZ', 'C. ALVARO MARTINEZ GOMEZ', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (300, NULL, 'C. MARIA ISABEL NIC QUIJANO', 'C. MARIA ISABEL NIC QUIJANO', 'C. MARIA ISABEL NIC QUIJANO', 'C. MARIA ISABEL NIC QUIJANO', '1972-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (301, NULL, 'C. ALVAREZ RIVERA VICTOR MANUEL', 'C. ALVAREZ RIVERA VICTOR MANUEL', 'C. ALVAREZ RIVERA VICTOR MANUEL', 'C. ALVAREZ RIVERA VICTOR MANUEL', '1975-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (302, NULL, 'C. OMAR RODRIGUEZ REYES', 'C. OMAR RODRIGUEZ REYES', 'C. OMAR RODRIGUEZ REYES', 'C. OMAR RODRIGUEZ REYES', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (303, NULL, 'C. JORGE ALBERTO YEH CAB', 'C. JORGE ALBERTO YEH CAB', 'C. JORGE ALBERTO YEH CAB', 'C. JORGE ALBERTO YEH CAB', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (304, NULL, 'C. ZULMA YADIRA CASTILLO COB', 'C. ZULMA YADIRA CASTILLO COB', 'C. ZULMA YADIRA CASTILLO COB', 'C. ZULMA YADIRA CASTILLO COB', '1978-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (305, NULL, 'C. MARICRUZ HERNANDEZ MORALES', 'C. MARICRUZ HERNANDEZ MORALES', 'C. MARICRUZ HERNANDEZ MORALES', 'C. MARICRUZ HERNANDEZ MORALES', '1988-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (306, NULL, 'C. GONZALEZ PECH WILLIAM', 'C. GONZALEZ PECH WILLIAM', 'C. GONZALEZ PECH WILLIAM', 'C. GONZALEZ PECH WILLIAM', '1951-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (307, NULL, 'C. MOGUEL NOVELO GASPAR', 'C. MOGUEL NOVELO GASPAR', 'C. MOGUEL NOVELO GASPAR', 'C. MOGUEL NOVELO GASPAR', '1964-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (308, NULL, 'C. COUOH OCH JOSE SANTIAGO', 'C. COUOH OCH JOSE SANTIAGO', 'C. COUOH OCH JOSE SANTIAGO', 'C. COUOH OCH JOSE SANTIAGO', '1962-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (309, NULL, 'C. CAAMAL Y ANGULO TELESFORO', 'C. CAAMAL Y ANGULO TELESFORO', 'C. CAAMAL Y ANGULO TELESFORO', 'C. CAAMAL Y ANGULO TELESFORO', '1943-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (310, NULL, 'C. VILLEGAS GALLEGOS ANDRES', 'C. VILLEGAS GALLEGOS ANDRES', 'C. VILLEGAS GALLEGOS ANDRES', 'C. VILLEGAS GALLEGOS ANDRES', '1989-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (311, NULL, 'C. LLANES PEREZ GABRIEL', 'C. LLANES PEREZ GABRIEL', 'C. LLANES PEREZ GABRIEL', 'C. LLANES PEREZ GABRIEL', '1987-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (312, NULL, 'C. MOO HOIL WILFRIDO', 'C. MOO HOIL WILFRIDO', 'C. MOO HOIL WILFRIDO', 'C. MOO HOIL WILFRIDO', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (313, NULL, 'C. RIVERO MOO ANGEL GUALBERTO', 'C. RIVERO MOO ANGEL GUALBERTO', 'C. RIVERO MOO ANGEL GUALBERTO', 'C. RIVERO MOO ANGEL GUALBERTO', '1954-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (314, NULL, 'C. LAZARO RICARDEZ EDILIA', 'C. LAZARO RICARDEZ EDILIA', 'C. LAZARO RICARDEZ EDILIA', 'C. LAZARO RICARDEZ EDILIA', '1969-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (315, NULL, 'C. CEBALLOS CHABLE MARTHA LETICIA', 'C. CEBALLOS CHABLE MARTHA LETICIA', 'C. CEBALLOS CHABLE MARTHA LETICIA', 'C. CEBALLOS CHABLE MARTHA LETICIA', '1988-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (316, NULL, 'C. CERVERA CAUICH SERGIO ALBERTO', 'C. CERVERA CAUICH SERGIO ALBERTO', 'C. CERVERA CAUICH SERGIO ALBERTO', 'C. CERVERA CAUICH SERGIO ALBERTO', '1989-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (317, NULL, 'C. EUAN QUIÑONES WEYMAR LIZANDRO', 'C. EUAN QUIÑONES WEYMAR LIZANDRO', 'C. EUAN QUIÑONES WEYMAR LIZANDRO', 'C. EUAN QUIÑONES WEYMAR LIZANDRO', '1973-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (318, NULL, 'C. CAMPOS ESPINOSA DENIS', 'C. CAMPOS ESPINOSA DENIS', 'C. CAMPOS ESPINOSA DENIS', 'C. CAMPOS ESPINOSA DENIS', '1978-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (319, NULL, 'C. CRUZ PUJOL ADELFO ISMAEL', 'C. CRUZ PUJOL ADELFO ISMAEL', 'C. CRUZ PUJOL ADELFO ISMAEL', 'C. CRUZ PUJOL ADELFO ISMAEL', '1977-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (320, NULL, 'C. OJEDA CHI DANIEL ANTONIO', 'C. OJEDA CHI DANIEL ANTONIO', 'C. OJEDA CHI DANIEL ANTONIO', 'C. OJEDA CHI DANIEL ANTONIO', '1983-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (321, NULL, 'C. MANRIQUE CANTO JACINTO EFREN', 'C. MANRIQUE CANTO JACINTO EFREN', 'C. MANRIQUE CANTO JACINTO EFREN', 'C. MANRIQUE CANTO JACINTO EFREN', '1975-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (322, NULL, 'C. CAHUICH AVILEZ WILBERTH ANTONIO', 'C. CAHUICH AVILEZ WILBERTH ANTONIO', 'C. CAHUICH AVILEZ WILBERTH ANTONIO', 'C. CAHUICH AVILEZ WILBERTH ANTONIO',
        '1981-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (323, NULL, 'C. DIAZ LOPEZ RUBICEL FELIPE', 'C. DIAZ LOPEZ RUBICEL FELIPE', 'C. DIAZ LOPEZ RUBICEL FELIPE', 'C. DIAZ LOPEZ RUBICEL FELIPE', '1981-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (324, NULL, 'C. MOLINA SANCHEZ MARELI', 'C. MOLINA SANCHEZ MARELI', 'C. MOLINA SANCHEZ MARELI', 'C. MOLINA SANCHEZ MARELI', '1980-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (325, NULL, 'C. PECH CAAMAL ORLANDO', 'C. PECH CAAMAL ORLANDO', 'C. PECH CAAMAL ORLANDO', 'C. PECH CAAMAL ORLANDO', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (326, NULL, 'C. CHIN PECH MIRIAM DEL ROSARIO', 'C. CHIN PECH MIRIAM DEL ROSARIO', 'C. CHIN PECH MIRIAM DEL ROSARIO', 'C. CHIN PECH MIRIAM DEL ROSARIO', '1988-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (327, NULL, 'C. GONZALEZ VARGAS MANUEL', 'C. GONZALEZ VARGAS MANUEL', 'C. GONZALEZ VARGAS MANUEL', 'C. GONZALEZ VARGAS MANUEL', '1989-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (328, NULL, 'C. PEREZ CHI MELCHOR ARTURO', 'C. PEREZ CHI MELCHOR ARTURO', 'C. PEREZ CHI MELCHOR ARTURO', 'C. PEREZ CHI MELCHOR ARTURO', '1983-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (329, NULL, 'C. ANDRADE SILVA ROGELIO', 'C. ANDRADE SILVA ROGELIO', 'C. ANDRADE SILVA ROGELIO', 'C. ANDRADE SILVA ROGELIO', '1986-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (330, NULL, 'C. SILVA RIZO MARISOL', 'C. SILVA RIZO MARISOL', 'C. SILVA RIZO MARISOL', 'C. SILVA RIZO MARISOL', '1996-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (331, NULL, 'C. MIS CANUL MARIA', 'C. MIS CANUL MARIA', 'C. MIS CANUL MARIA', 'C. MIS CANUL MARIA', '1982-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (332, NULL, 'C. RIVERA AGUILAR ALFREDO', 'C. RIVERA AGUILAR ALFREDO', 'C. RIVERA AGUILAR ALFREDO', 'C. RIVERA AGUILAR ALFREDO', '1978-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (333, NULL, 'C. OJEDA CHI MELCHOR ADAIN', 'C. OJEDA CHI MELCHOR ADAIN', 'C. OJEDA CHI MELCHOR ADAIN', 'C. OJEDA CHI MELCHOR ADAIN', '1985-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (334, NULL, 'C. TADEO ANTONIO ARANDA ROMERO', 'C. TADEO ANTONIO ARANDA ROMERO', 'C. TADEO ANTONIO ARANDA ROMERO', 'C. TADEO ANTONIO ARANDA ROMERO', '1983-01-01',
        'MASCULINO', '9847453971', 'control_exterminador@yahoo.com.mx', '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (335, NULL, 'C. AGUSTIN EK DIAZ', 'C. AGUSTIN EK DIAZ', 'C. AGUSTIN EK DIAZ', 'C. AGUSTIN EK DIAZ', '1967-01-01', 'MASCULINO', '9831378406', 'agudiazy@hotmail.com', '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (336, NULL, 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', '1955-01-01', 'MASCULINO',
        '9831022273', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (337, NULL, 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', '1981-01-01', 'MASCULINO', '9871007560',
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (338, NULL, 'C. OLEGARIO REJON SANTOS', 'C. OLEGARIO REJON SANTOS', 'C. OLEGARIO REJON SANTOS', 'C. OLEGARIO REJON SANTOS', '1954-01-01', 'MASCULINO', '9981228711', NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (339, NULL, 'C. MANUEL JESUS CARRILO HUCHIN', 'C. MANUEL JESUS CARRILO HUCHIN', 'C. MANUEL JESUS CARRILO HUCHIN', 'C. MANUEL JESUS CARRILO HUCHIN', '1979-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (340, NULL, 'C. CARLOS ENRIQUE CANCINO NEGRON', 'C. CARLOS ENRIQUE CANCINO NEGRON', 'C. CARLOS ENRIQUE CANCINO NEGRON', 'C. CARLOS ENRIQUE CANCINO NEGRON', '1977-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (341, NULL, 'C. JORGE ENRIQUE GOMEZ CICERO', 'C. JORGE ENRIQUE GOMEZ CICERO', 'C. JORGE ENRIQUE GOMEZ CICERO', 'C. JORGE ENRIQUE GOMEZ CICERO', '1957-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (342, NULL, 'C. JOSE ALBERTO GONOGRA PEREZ', 'C. JOSE ALBERTO GONOGRA PEREZ', 'C. JOSE ALBERTO GONOGRA PEREZ', 'C. JOSE ALBERTO GONOGRA PEREZ', '1976-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (343, NULL, 'C. ISIDRO RUFINO LUGO CASTILLO', 'C. ISIDRO RUFINO LUGO CASTILLO', 'C. ISIDRO RUFINO LUGO CASTILLO', 'C. ISIDRO RUFINO LUGO CASTILLO', '1954-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (344, NULL, 'C. CARLOS EZEQUIEL XIX CANUL', 'C. CARLOS EZEQUIEL XIX CANUL', 'C. CARLOS EZEQUIEL XIX CANUL', 'C. CARLOS EZEQUIEL XIX CANUL', '1980-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (345, NULL, 'C. FELIPE DE JESUS URICH COLLI', 'C. FELIPE DE JESUS URICH COLLI', 'C. FELIPE DE JESUS URICH COLLI', 'C. FELIPE DE JESUS URICH COLLI', '1975-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (346, NULL, 'C. ROSALIA MARTINEZ CHAN', 'C. ROSALIA MARTINEZ CHAN', 'C. ROSALIA MARTINEZ CHAN', 'C. ROSALIA MARTINEZ CHAN', '1965-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (347, NULL, 'MARIO JOAQUIN PONCE SOLIS', 'MARIO JOAQUIN PONCE SOLIS', 'MARIO JOAQUIN PONCE SOLIS', 'MARIO JOAQUIN PONCE SOLIS', '1973-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (348, NULL, 'C. JUAN AY PAT', 'C. JUAN AY PAT', 'C. JUAN AY PAT', 'C. JUAN AY PAT', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (349, NULL, 'C. JULIAN JESUS CUA PECH', 'C. JULIAN JESUS CUA PECH', 'C. JULIAN JESUS CUA PECH', 'C. JULIAN JESUS CUA PECH', '1981-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (350, NULL, 'C. ARTURO YAM UC', 'C. ARTURO YAM UC', 'C. ARTURO YAM UC', 'C. ARTURO YAM UC', '1970-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (351, NULL, 'C. LIZARDO CHAB MAY', 'C. LIZARDO CHAB MAY', 'C. LIZARDO CHAB MAY', 'C. LIZARDO CHAB MAY', '1985-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (352, NULL, 'C. MARCO ANTONIO COLORADO NOH', 'C. MARCO ANTONIO COLORADO NOH', 'C. MARCO ANTONIO COLORADO NOH', 'C. MARCO ANTONIO COLORADO NOH', '1986-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (353, NULL, 'C.CARLOS RAUL HERNANDEZ ANGULO', 'C.CARLOS RAUL HERNANDEZ ANGULO', 'C.CARLOS RAUL HERNANDEZ ANGULO', 'C.CARLOS RAUL HERNANDEZ ANGULO', '1983-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (354, NULL, 'C. EMANUEL DE JESUS LUGO MOO', 'C. EMANUEL DE JESUS LUGO MOO', 'C. EMANUEL DE JESUS LUGO MOO', 'C. EMANUEL DE JESUS LUGO MOO', '1986-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (355, NULL, 'C. JUANA M. DEL SOCORRO', 'C. JUANA M. DEL SOCORRO', 'C. JUANA M. DEL SOCORRO', 'C. JUANA M. DEL SOCORRO', '1969-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (356, NULL, 'MANUEL JESUS CASTILLO QUINTA', 'MANUEL JESUS CASTILLO QUINTA', 'MANUEL JESUS CASTILLO QUINTA', 'MANUEL JESUS CASTILLO QUINTA', '1970-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (357, NULL, 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', '1987-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (358, NULL, 'C. AMALIA CHAN GARCIA', 'C. AMALIA CHAN GARCIA', 'C. AMALIA CHAN GARCIA', 'C. AMALIA CHAN GARCIA', '1966-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (359, NULL, 'C. AMARO GUADALUPE UC VICENTE', 'C. AMARO GUADALUPE UC VICENTE', 'C. AMARO GUADALUPE UC VICENTE', 'C. AMARO GUADALUPE UC VICENTE', '1994-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (360, NULL, 'C. ANDRES BALAM PECH', 'C. ANDRES BALAM PECH', 'C. ANDRES BALAM PECH', 'C. ANDRES BALAM PECH', '1981-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (361, NULL, 'C. ANGEL ALEJANDRO CAMAL URICH', 'C. ANGEL ALEJANDRO CAMAL URICH', 'C. ANGEL ALEJANDRO CAMAL URICH', 'C. ANGEL ALEJANDRO CAMAL URICH', '1992-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (362, NULL, 'JOSE FRANCISCO CHAN SANTOS', 'JOSE FRANCISCO CHAN SANTOS', 'JOSE FRANCISCO CHAN SANTOS', 'JOSE FRANCISCO CHAN SANTOS', '1985-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (363, NULL, 'JOSE LUIS GOMEZ MARTINEZ', 'JOSE LUIS GOMEZ MARTINEZ', 'JOSE LUIS GOMEZ MARTINEZ', 'JOSE LUIS GOMEZ MARTINEZ', '1993-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (364, NULL, 'LUCY ALEJANDRINA LUGO MOO', 'LUCY ALEJANDRINA LUGO MOO', 'LUCY ALEJANDRINA LUGO MOO', 'LUCY ALEJANDRINA LUGO MOO', '1988-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (365, NULL, 'DAVID CALDERON SANCHEZ', 'DAVID CALDERON SANCHEZ', 'DAVID CALDERON SANCHEZ', 'DAVID CALDERON SANCHEZ', '1973-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (366, NULL, 'LUIS CESAR LARA CASAS', 'LUIS CESAR LARA CASAS', 'LUIS CESAR LARA CASAS', 'LUIS CESAR LARA CASAS', '1992-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (367, NULL, 'C. ONESIMO BALAM PECH', 'C. ONESIMO BALAM PECH', 'C. ONESIMO BALAM PECH', 'C. ONESIMO BALAM PECH', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (368, NULL, 'C. LUIS GUILLERMO COUOH PAT', 'C. LUIS GUILLERMO COUOH PAT', 'C. LUIS GUILLERMO COUOH PAT', 'C. LUIS GUILLERMO COUOH PAT', '1985-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (369, NULL, 'C. MARIA GUADALUPE MORAN SERRANO', 'C. MARIA GUADALUPE MORAN SERRANO', 'C. MARIA GUADALUPE MORAN SERRANO', 'C. MARIA GUADALUPE MORAN SERRANO', '1989-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (370, NULL, 'C. ATOCHA SALGADO AMARO', 'C. ATOCHA SALGADO AMARO', 'C. ATOCHA SALGADO AMARO', 'C. ATOCHA SALGADO AMARO', '1975-01-01', 'MASCULINO', '9831205679', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (371, NULL, 'C. LUIS ENRIQUE CUXIM ADORNO', 'C. LUIS ENRIQUE CUXIM ADORNO', 'C. LUIS ENRIQUE CUXIM ADORNO', 'C. LUIS ENRIQUE CUXIM ADORNO', '1988-01-01', 'MASCULINO',
        '9831760883', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (372, NULL, 'C. SEBASTIANA RUIZ CRUZ', 'C. SEBASTIANA RUIZ CRUZ', 'C. SEBASTIANA RUIZ CRUZ', 'C. SEBASTIANA RUIZ CRUZ', '1980-01-01', 'FEMENINO', '9831162947', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (373, NULL, 'C.JORGE VAZQUEZ PEREZ', 'C.JORGE VAZQUEZ PEREZ', 'C.JORGE VAZQUEZ PEREZ', 'C.JORGE VAZQUEZ PEREZ', '1980-01-01', 'MASCULINO', '9831068643', NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (374, NULL, 'C. MELITON RIOS MONTERO', 'C. MELITON RIOS MONTERO', 'C. MELITON RIOS MONTERO', 'C. MELITON RIOS MONTERO', '1960-01-01', 'MASCULINO', '9838325910', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (375, NULL, 'C. IRVING F. CAUICH PACHECO', 'C. IRVING F. CAUICH PACHECO', 'C. IRVING F. CAUICH PACHECO', 'C. IRVING F. CAUICH PACHECO', '1990-01-01', 'MASCULINO',
        '9838325910', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (376, NULL, 'C. DANIEL REYNALDO MEZQUITA B.', 'C. DANIEL REYNALDO MEZQUITA B.', 'C. DANIEL REYNALDO MEZQUITA B.', 'C. DANIEL REYNALDO MEZQUITA B.', '1963-01-01',
        'MASCULINO', '8325910', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (377, NULL, 'C. ADIN BARRIOS SANTIAGO', 'C. ADIN BARRIOS SANTIAGO', 'C. ADIN BARRIOS SANTIAGO', 'C. ADIN BARRIOS SANTIAGO', '1962-01-01', 'MASCULINO', '9838325910', NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (378, NULL, 'C. MARCOS ANTONIO POOT CAAMAL', 'C. MARCOS ANTONIO POOT CAAMAL', 'C. MARCOS ANTONIO POOT CAAMAL', 'C. MARCOS ANTONIO POOT CAAMAL', '1979-01-01', 'MASCULINO',
        '9831114670', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (379, NULL, 'C. GUSTAVO GARCIA ZUNZA', 'C. GUSTAVO GARCIA ZUNZA', 'C. GUSTAVO GARCIA ZUNZA', 'C. GUSTAVO GARCIA ZUNZA', '1995-01-01', 'MASCULINO', '9831308414', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (380, NULL, 'C. LUIS HUMBERTO BEDRIÑANA BARBOSA', 'C. LUIS HUMBERTO BEDRIÑANA BARBOSA', 'C. LUIS HUMBERTO BEDRIÑANA BARBOSA', 'C. LUIS HUMBERTO BEDRIÑANA BARBOSA',
        '1975-02-06', 'MASCULINO', '9838360714', 'fumigasacancun@hotmail.com', '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (381, NULL, 'C. MAURICIO CABALLERO ESTUPIÑAN', 'C. MAURICIO CABALLERO ESTUPIÑAN', 'C. MAURICIO CABALLERO ESTUPIÑAN', 'C. MAURICIO CABALLERO ESTUPIÑAN', '1980-01-01',
        'MASCULINO', '9838331522', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (382, NULL, 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', '1979-01-01',
        'FEMENINO', '9831258387', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (383, NULL, 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', 'C. LUIS GERARDO JIMENEZ ALVAREZ', '1960-01-01',
        'MASCULINO', '9831206453', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (384, NULL, 'C. ATOCHA SALGADO AMARO', 'C. ATOCHA SALGADO AMARO', 'C. ATOCHA SALGADO AMARO', 'C. ATOCHA SALGADO AMARO', '1976-01-01', 'MASCULINO', '9831076119', NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (385, NULL, 'C. LUIS ENRIQUE PEREZ FERNANDEZ', 'C. LUIS ENRIQUE PEREZ FERNANDEZ', 'C. LUIS ENRIQUE PEREZ FERNANDEZ', 'C. LUIS ENRIQUE PEREZ FERNANDEZ', '1957-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (386, NULL, 'C. GILBERTO ADOLFO MORENO PEREZ', 'C. GILBERTO ADOLFO MORENO PEREZ', 'C. GILBERTO ADOLFO MORENO PEREZ', 'C. GILBERTO ADOLFO MORENO PEREZ', '1984-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (387, NULL, 'C. DANIEL ALEJANDRO PASTRANA PEREZ', 'C. DANIEL ALEJANDRO PASTRANA PEREZ', 'C. DANIEL ALEJANDRO PASTRANA PEREZ', 'C. DANIEL ALEJANDRO PASTRANA PEREZ',
        '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (388, NULL, 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ', 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ', 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ', 'C. JOSE GUADALUPE RAMIREZ HERNANDEZ',
        '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (389, NULL, 'C. JOEL ISAIAS UH ESQUIVEL', 'C. JOEL ISAIAS UH ESQUIVEL', 'C. JOEL ISAIAS UH ESQUIVEL', 'C. JOEL ISAIAS UH ESQUIVEL', '1981-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (390, NULL, 'C. HERNANDEZ JIMENEZ VICENTE', 'C. HERNANDEZ JIMENEZ VICENTE', 'C. HERNANDEZ JIMENEZ VICENTE', 'C. HERNANDEZ JIMENEZ VICENTE', '1985-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (391, NULL, 'C. LILIANA UC PEÑA', 'C. LILIANA UC PEÑA', 'C. LILIANA UC PEÑA', 'C. LILIANA UC PEÑA', '1976-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (392, NULL, 'C. FRANCISCO MENDOZA LOPEZ', 'C. FRANCISCO MENDOZA LOPEZ', 'C. FRANCISCO MENDOZA LOPEZ', 'C. FRANCISCO MENDOZA LOPEZ', '1968-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (393, NULL, 'C. GAUDENCIO MAR HERNANDEZ', 'C. GAUDENCIO MAR HERNANDEZ', 'C. GAUDENCIO MAR HERNANDEZ', 'C. GAUDENCIO MAR HERNANDEZ', '1967-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (394, NULL, 'C. AMADO MARIANO MEX PAT', 'C. AMADO MARIANO MEX PAT', 'C. AMADO MARIANO MEX PAT', 'C. AMADO MARIANO MEX PAT', '1958-01-01', 'MASCULINO', NULL,
        '*A@HOTMAIL.COM', '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (395, NULL, 'C. SANTIAGO ANTONIO KU- UC', 'C. SANTIAGO ANTONIO KU- UC', 'C. SANTIAGO ANTONIO KU- UC', 'C. SANTIAGO ANTONIO KU- UC', '1966-01-01', 'MASCULINO', '9831067097',
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (396, NULL, 'C. JOSE ANTONIO CAMPOS FLORES', 'C. JOSE ANTONIO CAMPOS FLORES', 'C. JOSE ANTONIO CAMPOS FLORES', 'C. JOSE ANTONIO CAMPOS FLORES', '1989-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (397, NULL, 'C. NORBERTO IXTEPAN ALONZO', 'C. NORBERTO IXTEPAN ALONZO', 'C. NORBERTO IXTEPAN ALONZO', 'C. NORBERTO IXTEPAN ALONZO', '1959-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (398, NULL, 'C. PASTRANA PEREZ ARMANDO ANTONIO', 'C. PASTRANA PEREZ ARMANDO ANTONIO', 'C. PASTRANA PEREZ ARMANDO ANTONIO', 'C. PASTRANA PEREZ ARMANDO ANTONIO', '1978-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (399, NULL, 'C. JULIO ALBERTO PREN MANZANILLA', 'C. JULIO ALBERTO PREN MANZANILLA', 'C. JULIO ALBERTO PREN MANZANILLA', 'C. JULIO ALBERTO PREN MANZANILLA', '1975-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (400, NULL, 'C. JOSE ISMAEL TEC', 'C. JOSE ISMAEL TEC', 'C. JOSE ISMAEL TEC', 'C. JOSE ISMAEL TEC', '1970-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (401, NULL, 'C. MIGUEL ARCANGEL SALAZAR CAS', 'C. MIGUEL ARCANGEL SALAZAR CAS', 'C. MIGUEL ARCANGEL SALAZAR CAS', 'C. MIGUEL ARCANGEL SALAZAR CAS', '1983-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (402, NULL, 'C. ISRAEL D. BAXAN ANTELE', 'C. ISRAEL D. BAXAN ANTELE', 'C. ISRAEL D. BAXAN ANTELE', 'C. ISRAEL D. BAXAN ANTELE', '1977-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (403, NULL, 'C. JOSE E VAZQUEZ SOLIS', 'C. JOSE E VAZQUEZ SOLIS', 'C. JOSE E VAZQUEZ SOLIS', 'C. JOSE E VAZQUEZ SOLIS', '1953-09-09', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (404, NULL, 'C. RIGOBERTO M. CAAMAL ACEVEDO', 'C. RIGOBERTO M. CAAMAL ACEVEDO', 'C. RIGOBERTO M. CAAMAL ACEVEDO', 'C. RIGOBERTO M. CAAMAL ACEVEDO', '1965-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (405, NULL, 'C. RUBEN ERNESTO CRUZ PEREZ', 'C. RUBEN ERNESTO CRUZ PEREZ', 'C. RUBEN ERNESTO CRUZ PEREZ', 'C. RUBEN ERNESTO CRUZ PEREZ', '1988-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (406, NULL, 'C. WENDER CAAMAL ACEVEDO', 'C. WENDER CAAMAL ACEVEDO', 'C. WENDER CAAMAL ACEVEDO', 'C. WENDER CAAMAL ACEVEDO', '1971-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (407, NULL, 'C. FREDDY ISMAEL BALAM HAU', 'C. FREDDY ISMAEL BALAM HAU', 'C. FREDDY ISMAEL BALAM HAU', 'C. FREDDY ISMAEL BALAM HAU', '1971-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (408, NULL, 'LUIS E. CUXIM ADORNO', 'LUIS E. CUXIM ADORNO', 'LUIS E. CUXIM ADORNO', 'LUIS E. CUXIM ADORNO', '1989-06-16', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (409, NULL, 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', 'C. ADAN JAVIER REJON SANTOS', '1959-01-01', 'MASCULINO',
        '9831022273', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (410, NULL, 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', 'C. MANUEL JACOBO HAAS NAAL', '1981-01-01', 'MASCULINO', '9831007560',
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (411, NULL, 'C. TADEO ANTONIO ARANDA ROMERO', 'C. TADEO ANTONIO ARANDA ROMERO', 'C. TADEO ANTONIO ARANDA ROMERO', 'C. TADEO ANTONIO ARANDA ROMERO', '1983-01-01',
        'MASCULINO', '9847453971', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (412, NULL, 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', '1987-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (413, NULL, 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', 'C. MIGUEL ANGEL JIMENEZ XOOL', '1987-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (414, NULL, 'C. MANUEL CASTILLO QUINTAL', 'C. MANUEL CASTILLO QUINTAL', 'C. MANUEL CASTILLO QUINTAL', 'C. MANUEL CASTILLO QUINTAL', '1971-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (415, NULL, 'C. AMPARO GUADALUPE UC VICENTE', 'C. AMPARO GUADALUPE UC VICENTE', 'C. AMPARO GUADALUPE UC VICENTE', 'C. AMPARO GUADALUPE UC VICENTE', '1994-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (416, NULL, 'C. MARCO ANTONIO SUAREZ MARTINEZ', 'C. MARCO ANTONIO SUAREZ MARTINEZ', 'C. MARCO ANTONIO SUAREZ MARTINEZ', 'C. MARCO ANTONIO SUAREZ MARTINEZ', '1983-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (417, NULL, 'CHRISTIAN GONGORA SANCHEZ', 'CHRISTIAN GONGORA SANCHEZ', 'CHRISTIAN GONGORA SANCHEZ', 'CHRISTIAN GONGORA SANCHEZ', '1985-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (418, NULL, 'ALEX OMAR MEDINA', 'ALEX OMAR MEDINA', 'ALEX OMAR MEDINA', 'ALEX OMAR MEDINA', '1975-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (419, NULL, 'FRANCISCO CARRILLO ESTRELLA', 'FRANCISCO CARRILLO ESTRELLA', 'FRANCISCO CARRILLO ESTRELLA', 'FRANCISCO CARRILLO ESTRELLA', '1986-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (420, NULL, 'C. WENDY BAAS CEN', 'C. WENDY BAAS CEN', 'C. WENDY BAAS CEN', 'C. WENDY BAAS CEN', '1991-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (421, NULL, 'FRANCISCO DE ASIS POOT POOT', 'FRANCISCO DE ASIS POOT POOT', 'FRANCISCO DE ASIS POOT POOT', 'FRANCISCO DE ASIS POOT POOT', '1979-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (422, NULL, 'C. JORGE E. GOMEZ CICERO', 'C. JORGE E. GOMEZ CICERO', 'C. JORGE E. GOMEZ CICERO', 'C. JORGE E. GOMEZ CICERO', '1958-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (423, NULL, 'CHRISTIAN GONGORA GARDUÑO', 'CHRISTIAN GONGORA GARDUÑO', 'CHRISTIAN GONGORA GARDUÑO', 'CHRISTIAN GONGORA GARDUÑO', '1993-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (424, NULL, 'C. CARLOS EZEQUIEL XIX CANUL', 'C. CARLOS EZEQUIEL XIX CANUL', 'C. CARLOS EZEQUIEL XIX CANUL', 'C. CARLOS EZEQUIEL XIX CANUL', '1981-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (425, NULL, 'C. EDILBERTO AC TUYU', 'C. EDILBERTO AC TUYU', 'C. EDILBERTO AC TUYU', 'C. EDILBERTO AC TUYU', '1986-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (426, NULL, 'C. JUANA MARIA DEL SOCORRO SALAZAR PERAZA', 'C. JUANA MARIA DEL SOCORRO SALAZAR PERAZA', 'C. JUANA MARIA DEL SOCORRO SALAZAR PERAZA',
        'C. JUANA MARIA DEL SOCORRO SALAZAR PERAZA', '1969-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (427, NULL, 'ELIZABETH OSORIO DE LA CRUZ', 'ELIZABETH OSORIO DE LA CRUZ', 'ELIZABETH OSORIO DE LA CRUZ', 'ELIZABETH OSORIO DE LA CRUZ', '1987-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (428, NULL, 'C. MARIA GUADALUPE MORAN SERRANO', 'C. MARIA GUADALUPE MORAN SERRANO', 'C. MARIA GUADALUPE MORAN SERRANO', 'C. MARIA GUADALUPE MORAN SERRANO', '1989-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (429, NULL, 'JIMI ANTONI CIMA PECH', 'JIMI ANTONI CIMA PECH', 'JIMI ANTONI CIMA PECH', 'JIMI ANTONI CIMA PECH', '1983-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (430, NULL, 'C. GUSTAVO VASQUEZ LOPEZ', 'C. GUSTAVO VASQUEZ LOPEZ', 'C. GUSTAVO VASQUEZ LOPEZ', 'C. GUSTAVO VASQUEZ LOPEZ', '1993-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (431, NULL, 'MIGUEL BRISEÑO BALAM', 'MIGUEL BRISEÑO BALAM', 'MIGUEL BRISEÑO BALAM', 'MIGUEL BRISEÑO BALAM', '1987-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (432, NULL, 'C. LUCI ALEJANDRINA LUGO MOO', 'C. LUCI ALEJANDRINA LUGO MOO', 'C. LUCI ALEJANDRINA LUGO MOO', 'C. LUCI ALEJANDRINA LUGO MOO', '1988-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (433, NULL, 'C. DELFINA KUMUL DZIB', 'C. DELFINA KUMUL DZIB', 'C. DELFINA KUMUL DZIB', 'C. DELFINA KUMUL DZIB', '1971-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (434, NULL, 'ISIDRO LUGO CASTILLO', 'ISIDRO LUGO CASTILLO', 'ISIDRO LUGO CASTILLO', 'ISIDRO LUGO CASTILLO', '1954-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (435, NULL, 'LUIS GUILLERMO COHUO PAT', 'LUIS GUILLERMO COHUO PAT', 'LUIS GUILLERMO COHUO PAT', 'LUIS GUILLERMO COHUO PAT', '1986-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (436, NULL, 'LEOPOLDO MAXIMILIANO CASTILLO RIVAS', 'LEOPOLDO MAXIMILIANO CASTILLO RIVAS', 'LEOPOLDO MAXIMILIANO CASTILLO RIVAS', 'LEOPOLDO MAXIMILIANO CASTILLO RIVAS',
        '2000-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (437, NULL, 'ANDRES BALAM PECH', 'ANDRES BALAM PECH', 'ANDRES BALAM PECH', 'ANDRES BALAM PECH', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (438, NULL, 'C. ANGEL ALEJANDRO CAAMAL URICH', 'C. ANGEL ALEJANDRO CAAMAL URICH', 'C. ANGEL ALEJANDRO CAAMAL URICH', 'C. ANGEL ALEJANDRO CAAMAL URICH', '1993-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (439, NULL, 'FELIPE URICH CALLE', 'FELIPE URICH CALLE', 'FELIPE URICH CALLE', 'FELIPE URICH CALLE', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (440, NULL, 'C. CARLOS ENRIQUE CANSINO NEGRON', 'C. CARLOS ENRIQUE CANSINO NEGRON', 'C. CARLOS ENRIQUE CANSINO NEGRON', 'C. CARLOS ENRIQUE CANSINO NEGRON', '1976-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (441, NULL, 'C. JUAN AY PAT', 'C. JUAN AY PAT', 'C. JUAN AY PAT', 'C. JUAN AY PAT', '1979-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (442, NULL, 'C. ARTURO YAM PUC', 'C. ARTURO YAM PUC', 'C. ARTURO YAM PUC', 'C. ARTURO YAM PUC', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (443, NULL, 'C. EMANUEL DE JESUS LAGO MOO', 'C. EMANUEL DE JESUS LAGO MOO', 'C. EMANUEL DE JESUS LAGO MOO', 'C. EMANUEL DE JESUS LAGO MOO', '1987-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (444, NULL, 'C. MARCO ANTONIO COLORADA NOH', 'C. MARCO ANTONIO COLORADA NOH', 'C. MARCO ANTONIO COLORADA NOH', 'C. MARCO ANTONIO COLORADA NOH', '1986-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (445, NULL, 'C. VICTOR MANUEL NOH CANUL', 'C. VICTOR MANUEL NOH CANUL', 'C. VICTOR MANUEL NOH CANUL', 'C. VICTOR MANUEL NOH CANUL', '1981-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (446, NULL, 'C. RUBEN CHI CARVAJAL', 'C. RUBEN CHI CARVAJAL', 'C. RUBEN CHI CARVAJAL', 'C. RUBEN CHI CARVAJAL', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (447, NULL, 'C. JOSE MATILDE CAAMAL PEREZ', 'C. JOSE MATILDE CAAMAL PEREZ', 'C. JOSE MATILDE CAAMAL PEREZ', 'C. JOSE MATILDE CAAMAL PEREZ', '1991-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (448, NULL, 'C. ONESIMO BALAM PECH', 'C. ONESIMO BALAM PECH', 'C. ONESIMO BALAM PECH', 'C. ONESIMO BALAM PECH', '1985-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (449, NULL, 'C. MARIO PONCE SOLIS', 'C. MARIO PONCE SOLIS', 'C. MARIO PONCE SOLIS', 'C. MARIO PONCE SOLIS', '1972-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (450, NULL, 'C. JOSE ALBERTO GONGORA PEREZ', 'C. JOSE ALBERTO GONGORA PEREZ', 'C. JOSE ALBERTO GONGORA PEREZ', 'C. JOSE ALBERTO GONGORA PEREZ', '1976-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (451, NULL, 'C. WILLIAM UC CAMOL', 'C. WILLIAM UC CAMOL', 'C. WILLIAM UC CAMOL', 'C. WILLIAM UC CAMOL', '1972-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (452, NULL, 'C. FRANCISCA CHAN SANTOS', 'C. FRANCISCA CHAN SANTOS', 'C. FRANCISCA CHAN SANTOS', 'C. FRANCISCA CHAN SANTOS', '1986-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (453, NULL, 'C. CARRILLO HUCHIN MANUEL JESUS', 'C. CARRILLO HUCHIN MANUEL JESUS', 'C. CARRILLO HUCHIN MANUEL JESUS', 'C. CARRILLO HUCHIN MANUEL JESUS', '1979-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (454, NULL, 'C.CARLOS MANUEL MENDICUTI PINTO', 'C.CARLOS MANUEL MENDICUTI PINTO', 'C.CARLOS MANUEL MENDICUTI PINTO', 'C.CARLOS MANUEL MENDICUTI PINTO', '1977-01-01',
        'MASCULINO', '9831047663', NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (455, NULL, 'C. JOSE AURELIO EUAN CHAN', 'C. JOSE AURELIO EUAN CHAN', 'C. JOSE AURELIO EUAN CHAN', 'C. JOSE AURELIO EUAN CHAN', '1985-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (456, NULL, 'C. OMAR ALEJANDRO CAAMAL POOL', 'C. OMAR ALEJANDRO CAAMAL POOL', 'C. OMAR ALEJANDRO CAAMAL POOL', 'C. OMAR ALEJANDRO CAAMAL POOL', '1986-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (457, NULL, 'C. GUALBERTO CANUL BALAM', 'C. GUALBERTO CANUL BALAM', 'C. GUALBERTO CANUL BALAM', 'C. GUALBERTO CANUL BALAM', '1970-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (458, NULL, 'C. GABRIELA MORENO GONZALEZ', 'C. GABRIELA MORENO GONZALEZ', 'C. GABRIELA MORENO GONZALEZ', 'C. GABRIELA MORENO GONZALEZ', '1976-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (459, NULL, 'C. DAGOBERTO GARCIA GOMEZ', 'C. DAGOBERTO GARCIA GOMEZ', 'C. DAGOBERTO GARCIA GOMEZ', 'C. DAGOBERTO GARCIA GOMEZ', '1984-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (460, NULL, 'C. CRISTIAN RICARDO GONZALEZ VARGAZ', 'C. CRISTIAN RICARDO GONZALEZ VARGAZ', 'C. CRISTIAN RICARDO GONZALEZ VARGAZ', 'C. CRISTIAN RICARDO GONZALEZ VARGAZ',
        '1991-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (461, NULL, 'C. FRANCISCO PAREDES CANUL', 'C. FRANCISCO PAREDES CANUL', 'C. FRANCISCO PAREDES CANUL', 'C. FRANCISCO PAREDES CANUL', '1971-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (462, NULL, 'C.JANET CITUK ESTRELLA', 'C.JANET CITUK ESTRELLA', 'C.JANET CITUK ESTRELLA', 'C.JANET CITUK ESTRELLA', '1987-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (463, NULL, 'C. JOSE NICOLAS PECH PANTI', 'C. JOSE NICOLAS PECH PANTI', 'C. JOSE NICOLAS PECH PANTI', 'C. JOSE NICOLAS PECH PANTI', '1976-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (464, NULL, 'C. TRINIDAD GOMEZ HIDALGO', 'C. TRINIDAD GOMEZ HIDALGO', 'C. TRINIDAD GOMEZ HIDALGO', 'C. TRINIDAD GOMEZ HIDALGO', '1969-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (465, NULL, 'C. JUAN DE LA CRUZ EUAN PECH', 'C. JUAN DE LA CRUZ EUAN PECH', 'C. JUAN DE LA CRUZ EUAN PECH', 'C. JUAN DE LA CRUZ EUAN PECH', '1979-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (466, NULL, 'C.RENE COHUO KU', 'C.RENE COHUO KU', 'C.RENE COHUO KU', 'C.RENE COHUO KU', '1987-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (467, NULL, 'C. MARTIN DE JESUS DURAN ESCALANTE', 'C. MARTIN DE JESUS DURAN ESCALANTE', 'C. MARTIN DE JESUS DURAN ESCALANTE', 'C. MARTIN DE JESUS DURAN ESCALANTE',
        '1965-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (468, NULL, 'C. DENIS CAMPOS ESPINOZA', 'C. DENIS CAMPOS ESPINOZA', 'C. DENIS CAMPOS ESPINOZA', 'C. DENIS CAMPOS ESPINOZA', '1978-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (469, NULL, 'C. RAFAEL ANGEL CASTILLO ANDRADE', 'C. RAFAEL ANGEL CASTILLO ANDRADE', 'C. RAFAEL ANGEL CASTILLO ANDRADE', 'C. RAFAEL ANGEL CASTILLO ANDRADE', '1990-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (470, NULL, 'C. WEYMAR LIZANDRO EUAN QUIÑONES', 'C. WEYMAR LIZANDRO EUAN QUIÑONES', 'C. WEYMAR LIZANDRO EUAN QUIÑONES', 'C. WEYMAR LIZANDRO EUAN QUIÑONES', '1972-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (471, NULL, 'C. RUBICEL DIAZ LOPEZ', 'C. RUBICEL DIAZ LOPEZ', 'C. RUBICEL DIAZ LOPEZ', 'C. RUBICEL DIAZ LOPEZ', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (472, NULL, 'C. FRANCISCO CANO CAAMAL', 'C. FRANCISCO CANO CAAMAL', 'C. FRANCISCO CANO CAAMAL', 'C. FRANCISCO CANO CAAMAL', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (473, NULL, 'C. MARTHA LETICIA CEBALLOS CHABLE', 'C. MARTHA LETICIA CEBALLOS CHABLE', 'C. MARTHA LETICIA CEBALLOS CHABLE', 'C. MARTHA LETICIA CEBALLOS CHABLE', '1987-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (474, NULL, 'C. MELCHOR OJEDA CHI', 'C. MELCHOR OJEDA CHI', 'C. MELCHOR OJEDA CHI', 'C. MELCHOR OJEDA CHI', '1985-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (475, NULL, 'C. ROGELIO RAYMUNDO ANDRADE SILVA', 'C. ROGELIO RAYMUNDO ANDRADE SILVA', 'C. ROGELIO RAYMUNDO ANDRADE SILVA', 'C. ROGELIO RAYMUNDO ANDRADE SILVA', '1986-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (476, NULL, 'C. JULIO CESAR ORTEGA G.', 'C. JULIO CESAR ORTEGA G.', 'C. JULIO CESAR ORTEGA G.', 'C. JULIO CESAR ORTEGA G.', '1967-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (477, NULL, 'C. ELIO ADRIANO DZIB CHAN', 'C. ELIO ADRIANO DZIB CHAN', 'C. ELIO ADRIANO DZIB CHAN', 'C. ELIO ADRIANO DZIB CHAN', '1990-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (478, NULL, 'C. CLEMENTE XOOL HAU', 'C. CLEMENTE XOOL HAU', 'C. CLEMENTE XOOL HAU', 'C. CLEMENTE XOOL HAU', '1980-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (479, NULL, 'C. MERCEDES DIAZ GOMEZ', 'C. MERCEDES DIAZ GOMEZ', 'C. MERCEDES DIAZ GOMEZ', 'C. MERCEDES DIAZ GOMEZ', '1962-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (480, NULL, 'C. FRANCISCA PEREZ DUQUE', 'C. FRANCISCA PEREZ DUQUE', 'C. FRANCISCA PEREZ DUQUE', 'C. FRANCISCA PEREZ DUQUE', '1978-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (481, NULL, 'C. SUEMY CANCHE SANCHEZ', 'C. SUEMY CANCHE SANCHEZ', 'C. SUEMY CANCHE SANCHEZ', 'C. SUEMY CANCHE SANCHEZ', '1976-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (482, NULL, 'C. GASTON RODRIGUEZ SANCHEZ', 'C. GASTON RODRIGUEZ SANCHEZ', 'C. GASTON RODRIGUEZ SANCHEZ', 'C. GASTON RODRIGUEZ SANCHEZ', '1972-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (483, NULL, 'C. EVERARDO LOPEZ MENDOZA', 'C. EVERARDO LOPEZ MENDOZA', 'C. EVERARDO LOPEZ MENDOZA', 'C. EVERARDO LOPEZ MENDOZA', '1958-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (484, NULL, 'C. RENE DZUL CHIMAL', 'C. RENE DZUL CHIMAL', 'C. RENE DZUL CHIMAL', 'C. RENE DZUL CHIMAL', '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (485, NULL, 'C. JORGE DE LA CRUZ VELAZQUEZ', 'C. JORGE DE LA CRUZ VELAZQUEZ', 'C. JORGE DE LA CRUZ VELAZQUEZ', 'C. JORGE DE LA CRUZ VELAZQUEZ', '1976-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (486, NULL, 'C. REINALDO RODRIGUEZ AREVALO', 'C. REINALDO RODRIGUEZ AREVALO', 'C. REINALDO RODRIGUEZ AREVALO', 'C. REINALDO RODRIGUEZ AREVALO', '1969-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (487, NULL, 'C. ANGEL JIMENEZ SALVADOR', 'C. ANGEL JIMENEZ SALVADOR', 'C. ANGEL JIMENEZ SALVADOR', 'C. ANGEL JIMENEZ SALVADOR', '1972-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (488, NULL, 'C. JESUS TULE VALDELAMAR', 'C. JESUS TULE VALDELAMAR', 'C. JESUS TULE VALDELAMAR', 'C. JESUS TULE VALDELAMAR', '1967-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (489, NULL, 'C. JOSE GASPAR MOGUEL NOVELO', 'C. JOSE GASPAR MOGUEL NOVELO', 'C. JOSE GASPAR MOGUEL NOVELO', 'C. JOSE GASPAR MOGUEL NOVELO', '1961-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (490, NULL, 'C. FRANCISCO JAVIER RODRIGUEZ MAGAÑA', 'C. FRANCISCO JAVIER RODRIGUEZ MAGAÑA', 'C. FRANCISCO JAVIER RODRIGUEZ MAGAÑA', 'C. FRANCISCO JAVIER RODRIGUEZ MAGAÑA',
        '1991-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (491, NULL, 'C. JOSE ALBERTO FERRAEZ EK', 'C. JOSE ALBERTO FERRAEZ EK', 'C. JOSE ALBERTO FERRAEZ EK', 'C. JOSE ALBERTO FERRAEZ EK', '1979-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (492, NULL, 'C. MELCHOR ARTURO PEREZ CHI', 'C. MELCHOR ARTURO PEREZ CHI', 'C. MELCHOR ARTURO PEREZ CHI', 'C. MELCHOR ARTURO PEREZ CHI', '1983-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (493, NULL, 'C. ANGEL RIVERO M.', 'C. ANGEL RIVERO M.', 'C. ANGEL RIVERO M.', 'C. ANGEL RIVERO M.', '1954-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (494, NULL, 'C. MIGUEL ALBERTO LORIA QUIJANO', 'C. MIGUEL ALBERTO LORIA QUIJANO', 'C. MIGUEL ALBERTO LORIA QUIJANO', 'C. MIGUEL ALBERTO LORIA QUIJANO', '1971-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (495, NULL, 'C. MARIA GUADALUPE BARRERA DURAN', 'C. MARIA GUADALUPE BARRERA DURAN', 'C. MARIA GUADALUPE BARRERA DURAN', 'C. MARIA GUADALUPE BARRERA DURAN', '1960-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (496, NULL, 'C. EDUARDO SALINAS ORDAZ', 'C. EDUARDO SALINAS ORDAZ', 'C. EDUARDO SALINAS ORDAZ', 'C. EDUARDO SALINAS ORDAZ', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (497, NULL, 'C. FERNANDO HU POOT', 'C. FERNANDO HU POOT', 'C. FERNANDO HU POOT', 'C. FERNANDO HU POOT', '1947-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (498, NULL, 'C. GONZALEZ VARGAS EDWIN MANUEL', 'C. GONZALEZ VARGAS EDWIN MANUEL', 'C. GONZALEZ VARGAS EDWIN MANUEL', 'C. GONZALEZ VARGAS EDWIN MANUEL', '1989-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (499, NULL, 'C. SERGIO ALBERTO CERVERA', 'C. SERGIO ALBERTO CERVERA', 'C. SERGIO ALBERTO CERVERA', 'C. SERGIO ALBERTO CERVERA', '1988-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (500, NULL, 'C. MARELI MOLINA SANCHEZ', 'C. MARELI MOLINA SANCHEZ', 'C. MARELI MOLINA SANCHEZ', 'C. MARELI MOLINA SANCHEZ', '1980-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (501, NULL, 'C. RUBEN GONZALEZ BALTAZAR', 'C. RUBEN GONZALEZ BALTAZAR', 'C. RUBEN GONZALEZ BALTAZAR', 'C. RUBEN GONZALEZ BALTAZAR', '1960-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (502, NULL, 'C. PEDRO CETINA SUASTE', 'C. PEDRO CETINA SUASTE', 'C. PEDRO CETINA SUASTE', 'C. PEDRO CETINA SUASTE', '1983-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (503, NULL, 'C. GENNY TERESA ALDECUA ABAN', 'C. GENNY TERESA ALDECUA ABAN', 'C. GENNY TERESA ALDECUA ABAN', 'C. GENNY TERESA ALDECUA ABAN', '1994-01-01', 'FEMENINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (504, NULL, 'C. MANUEL XOOL CHIMAL', 'C. MANUEL XOOL CHIMAL', 'C. MANUEL XOOL CHIMAL', 'C. MANUEL XOOL CHIMAL', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (505, NULL, 'C. JUANA M. TENORIO CARDONA', 'C. JUANA M. TENORIO CARDONA', 'C. JUANA M. TENORIO CARDONA', 'C. JUANA M. TENORIO CARDONA', '1967-01-01', 'FEMENINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (506, NULL, 'C. GABRIELA AURORA CAAMAL POOL', 'C. GABRIELA AURORA CAAMAL POOL', 'C. GABRIELA AURORA CAAMAL POOL', 'C. GABRIELA AURORA CAAMAL POOL', '1970-01-01', 'FEMENINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (507, NULL, 'C. ALBERTO CHAN CHOC', 'C. ALBERTO CHAN CHOC', 'C. ALBERTO CHAN CHOC', 'C. ALBERTO CHAN CHOC', '1989-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (508, NULL, 'C. GERARDO VENTURA LUGO M.', 'C. GERARDO VENTURA LUGO M.', 'C. GERARDO VENTURA LUGO M.', 'C. GERARDO VENTURA LUGO M.', '1977-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (509, NULL, 'C. JORGE CARLOS OCHOA GOMEZ', 'C. JORGE CARLOS OCHOA GOMEZ', 'C. JORGE CARLOS OCHOA GOMEZ', 'C. JORGE CARLOS OCHOA GOMEZ', '1991-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (510, NULL, 'C. JAZMIN MARTINEZ MONTES', 'C. JAZMIN MARTINEZ MONTES', 'C. JAZMIN MARTINEZ MONTES', 'C. JAZMIN MARTINEZ MONTES', '1973-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (511, NULL, 'C. MANUEL TORRES PEREZ', 'C. MANUEL TORRES PEREZ', 'C. MANUEL TORRES PEREZ', 'C. MANUEL TORRES PEREZ', '1959-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (512, NULL, 'C. WILFRIDO MOO HOIL', 'C. WILFRIDO MOO HOIL', 'C. WILFRIDO MOO HOIL', 'C. WILFRIDO MOO HOIL', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (513, NULL, 'C. JULIO ALFONSO MADERO SANSORES', 'C. JULIO ALFONSO MADERO SANSORES', 'C. JULIO ALFONSO MADERO SANSORES', 'C. JULIO ALFONSO MADERO SANSORES', '1951-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (514, NULL, 'C. FRANCISCO MONTEJO AVENDAÑO', 'C. FRANCISCO MONTEJO AVENDAÑO', 'C. FRANCISCO MONTEJO AVENDAÑO', 'C. FRANCISCO MONTEJO AVENDAÑO', '1976-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (515, NULL, 'C. DANIEL A. OJEDA CHI', 'C. DANIEL A. OJEDA CHI', 'C. DANIEL A. OJEDA CHI', 'C. DANIEL A. OJEDA CHI', '1983-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (516, NULL, 'C. ALEJANDRO RODRIGUEZ CHAN', 'C. ALEJANDRO RODRIGUEZ CHAN', 'C. ALEJANDRO RODRIGUEZ CHAN', 'C. ALEJANDRO RODRIGUEZ CHAN', '1989-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (517, NULL, 'C. TELESFORO ANGULO CAAMAL', 'C. TELESFORO ANGULO CAAMAL', 'C. TELESFORO ANGULO CAAMAL', 'C. TELESFORO ANGULO CAAMAL', '1943-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (518, NULL, 'C. JEFREN MANRIQUE CANTO', 'C. JEFREN MANRIQUE CANTO', 'C. JEFREN MANRIQUE CANTO', 'C. JEFREN MANRIQUE CANTO', '1976-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (519, NULL, 'C. WILBERTH ANTONIO CAHUICH AVILA', 'C. WILBERTH ANTONIO CAHUICH AVILA', 'C. WILBERTH ANTONIO CAHUICH AVILA', 'C. WILBERTH ANTONIO CAHUICH AVILA', '1981-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (520, NULL, 'C. ADELFO CRUZ PUJOL', 'C. ADELFO CRUZ PUJOL', 'C. ADELFO CRUZ PUJOL', 'C. ADELFO CRUZ PUJOL', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (521, NULL, 'C. VICTOR MANUEL ALVAREZ R.', 'C. VICTOR MANUEL ALVAREZ R.', 'C. VICTOR MANUEL ALVAREZ R.', 'C. VICTOR MANUEL ALVAREZ R.', '1975-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (522, NULL, 'C. ANGEL GUSTAVO COYOC PECH', 'C. ANGEL GUSTAVO COYOC PECH', 'C. ANGEL GUSTAVO COYOC PECH', 'C. ANGEL GUSTAVO COYOC PECH', '1995-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (523, NULL, 'C. AMELIA GOMEZ BOLIO', 'C. AMELIA GOMEZ BOLIO', 'C. AMELIA GOMEZ BOLIO', 'C. AMELIA GOMEZ BOLIO', '1960-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (524, NULL, 'C. YASURI GONGORA JIMENEZ', 'C. YASURI GONGORA JIMENEZ', 'C. YASURI GONGORA JIMENEZ', 'C. YASURI GONGORA JIMENEZ', '1987-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (525, NULL, 'C. MARIA ZARRAGOSA', 'C. MARIA ZARRAGOSA', 'C. MARIA ZARRAGOSA', 'C. MARIA ZARRAGOSA', '1967-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (526, NULL, 'C. LEYSIS ALMEYDA GARCIA', 'C. LEYSIS ALMEYDA GARCIA', 'C. LEYSIS ALMEYDA GARCIA', 'C. LEYSIS ALMEYDA GARCIA', '1980-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (527, NULL, 'C. WILLIAM LOPEZ BAESA', 'C. WILLIAM LOPEZ BAESA', 'C. WILLIAM LOPEZ BAESA', 'C. WILLIAM LOPEZ BAESA', '1984-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (528, NULL, 'C. JULIO UBALDO ANDRADE MOJICA', 'C. JULIO UBALDO ANDRADE MOJICA', 'C. JULIO UBALDO ANDRADE MOJICA', 'C. JULIO UBALDO ANDRADE MOJICA', '1995-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (529, NULL, 'C. MAURO RODRIGUEZ BACELES', 'C. MAURO RODRIGUEZ BACELES', 'C. MAURO RODRIGUEZ BACELES', 'C. MAURO RODRIGUEZ BACELES', '1994-01-01', 'MASCULINO', NULL, NULL,
        '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (530, NULL, 'C. ARTURO ARMENTA NAVARRETE', 'C. ARTURO ARMENTA NAVARRETE', 'C. ARTURO ARMENTA NAVARRETE', 'C. ARTURO ARMENTA NAVARRETE', '1953-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (531, NULL, 'C. ADA ISELA COLLI POMOL', 'C. ADA ISELA COLLI POMOL', 'C. ADA ISELA COLLI POMOL', 'C. ADA ISELA COLLI POMOL', '1985-01-01', 'FEMENINO', NULL, NULL, '0', NULL,
        NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (532, NULL, 'C. CARLOS ANTONIO UICAB SANTOS', 'C. CARLOS ANTONIO UICAB SANTOS', 'C. CARLOS ANTONIO UICAB SANTOS', 'C. CARLOS ANTONIO UICAB SANTOS', '1968-01-01',
        'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (533, NULL, 'C. SANTIAGO COHUO OCH', 'C. SANTIAGO COHUO OCH', 'C. SANTIAGO COHUO OCH', 'C. SANTIAGO COHUO OCH', '1962-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (534, NULL, 'C. WILBERTH ROSADO MEDINA', 'C. WILBERTH ROSADO MEDINA', 'C. WILBERTH ROSADO MEDINA', 'C. WILBERTH ROSADO MEDINA', '1972-01-01', 'MASCULINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (535, NULL, 'C. EDILIA LAZARO RICARDEZ', 'C. EDILIA LAZARO RICARDEZ', 'C. EDILIA LAZARO RICARDEZ', 'C. EDILIA LAZARO RICARDEZ', '1969-01-01', 'FEMENINO', NULL, NULL, '0',
        NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (536, NULL, 'C. ELBERTH GEDEONI SUNZA POOT', 'C. ELBERTH GEDEONI SUNZA POOT', 'C. ELBERTH GEDEONI SUNZA POOT', 'C. ELBERTH GEDEONI SUNZA POOT', '1992-01-01', 'MASCULINO',
        NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (537, NULL, 'C. JOSE ROMERO POLANCO', 'C. JOSE ROMERO POLANCO', 'C. JOSE ROMERO POLANCO', 'C. JOSE ROMERO POLANCO', '1974-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (538, NULL, 'C. MIRZA CAHUICH POOL', 'C. MIRZA CAHUICH POOL', 'C. MIRZA CAHUICH POOL', 'C. MIRZA CAHUICH POOL', '1971-01-01', 'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (539, NULL, 'C. ANDRES VILLEGAS GALLEGOS', 'C. ANDRES VILLEGAS GALLEGOS', 'C. ANDRES VILLEGAS GALLEGOS', 'C. ANDRES VILLEGAS GALLEGOS', '1988-01-01', 'MASCULINO', NULL,
        NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (540, NULL, 'C. ORLANDO PECH CAAMAL', 'C. ORLANDO PECH CAAMAL', 'C. ORLANDO PECH CAAMAL', 'C. ORLANDO PECH CAAMAL', '1982-01-01', 'MASCULINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (541, NULL, 'C. MIRIAM DEL ROSARIO CHIN PECH', 'C. MIRIAM DEL ROSARIO CHIN PECH', 'C. MIRIAM DEL ROSARIO CHIN PECH', 'C. MIRIAM DEL ROSARIO CHIN PECH', '1988-01-01',
        'FEMENINO', NULL, NULL, '0', NULL, NULL);
INSERT INTO laboratorio_estatal.pacientes (id, expediente_id, nombre, apellidos, nombre_apellidos, apellidos_nombre, fecha_nacimiento, genero, telefono, email, edad, created_at,
                                           updated_at)
VALUES (542, NULL, 'C. RENE GARCIA GONZALEZ', 'C. RENE GARCIA GONZALEZ', 'C. RENE GARCIA GONZALEZ', 'C. RENE GARCIA GONZALEZ', '1977-01-01', 'MASCULINO', NULL, NULL, '0', NULL,
        NULL);
