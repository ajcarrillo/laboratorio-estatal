INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (1, 'AGUA DE CONTACTO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (2, 'AGUA DE FUENTE NATURAL', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (3, 'AGUA DESIONIZADA', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (4, 'AGUA POTABLE', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (5, 'AGUA PURIFICADA', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (6, 'AGUA RESIDUAL', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (7, 'ALCOHÓLICA', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (8, 'ANIMAL', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (9, 'BORREGO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (10, 'CARMELO MACIZO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (11, 'CARMELO SUAVE', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (12, 'COCIDO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (13, 'CONGELADO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (14, 'CRUDO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (15, 'DULCE', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (16, 'ENLATADO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (17, 'FRESCO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (18, 'HELADO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (19, 'HIELO DE CONSUMO HUMANO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (20, 'HIELO DE CONTACTO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (21, 'LECHE', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (22, 'LECHE EN POLVO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (23, 'MIXTO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (24, 'NA', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (25, 'NO ALCOHÓLICA', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (26, 'OTROS', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (27, 'POSTRE', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (28, 'POSTRE LÁCTEO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (29, 'POLLO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (30, 'PUERCO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (31, 'QUESOS FRESCOS', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (32, 'QUESOS MADURADOS', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (33, 'RES', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (34, 'SALADO', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (35, 'VEGETAL', NULL, NULL);
INSERT INTO laboratorio_estatal.subcategorias (id, descripcion, created_at, updated_at)
VALUES (36, 'SUERO/SANGRE', NULL, NULL);
