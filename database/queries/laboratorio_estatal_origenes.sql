INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1, 'INSTITUCIONAL', 'EM3', NULL, 'COORDINACION SANITARIA NO. 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (2, 'INSTITUCIONAL', 'EM4', NULL, 'HOSPITAL GENERAL CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (3, 'PARTICULAR', 'EM6', NULL, 'HECTOR BUSTILLOS ANGULO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (4, 'INSTITUCIONAL', 'EM9', NULL, 'DIRECTOR DEL HOSPITAL MATERNO INFANTIL MORELOS', NULL, '9838321588', NULL, 'DR. JUAN EZEQUIEL AGUILAR MUCIÑO',
        'AVENIDA JUAREZ #141, COLONIA CENTRO', '9838321588', 'admhmim@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (5, 'INSTITUCIONAL', 'EM17', NULL, 'HOSPITAL GENERAL CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (6, 'INSTITUCIONAL', 'EM21', NULL, 'JURISDICCION SANITARIA # 1', NULL, ' 983 1292708', NULL, NULL, NULL, ' 983 1292708', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (7, 'INSTITUCIONAL', 'EM22', NULL, 'SECRETARIA DE ECOLOGIA Y MEDIO AMBIENTE', NULL, ' 983 1292187', NULL, NULL, NULL, ' 983 1292187', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (8, 'INSTITUCIONAL', '491', NULL, 'COORDINADOR DE PROTECCIÓN CONTRA RIESGOS SANITARIOS ZONA NORTE', 'COORDINADOR DE PROTECCIÓN CONTRA RIESGOS SANITARIOS ZONA NORTE',
        '998884192', NULL, 'C. JULIO EDUARDO MENDOZA ALVAREZ', 'SUPER MANZANA 63, MANZANA 10, LOTE 20', '998884192', 'ventanillacun@hotmail.com', NULL, NULL, 'Quintana Roo',
        'BENITO JUAREZ', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (9, 'INSTITUCIONAL', 'EM26', NULL, 'SEGURIDAD PUBLICA DEL ESTADO DE QUINTANA ROO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (10, 'INSTITUCIONAL', 'EM27', NULL, 'SEGURIDAD PUBLICA DEL ESTADO DE QUINTANA ROO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (11, 'INSTITUCIONAL', 'EM29', NULL, 'HOSPITAL GENERAL FELIPE CARRILLO PUERTO', NULL, ' 983 8340092', NULL, NULL, NULL, ' 983 8340092', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (12, 'INSTITUCIONAL', 'EM32', NULL, 'C.S.R. CHAN SANTA CRUZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (13, 'INSTITUCIONAL', 'EM34', NULL, 'C.S.R. CHANCA DERREPENTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (14, 'INSTITUCIONAL', 'EM35', NULL, 'HOSPITAL INTEGRAL JOSE MARIA MORELOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (15, 'INSTITUCIONAL', 'EM37', NULL, 'BANCO DE SANGRE CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (16, 'PARTICULAR', 'EM36', NULL, 'CLIDDA ISSSTE CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (17, 'INSTITUCIONAL', 'EM38', NULL, 'C.S.R. DAVID GUSTAVO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (18, 'INSTITUCIONAL', 'EM39', NULL, 'C.S.U. 1 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (19, 'INSTITUCIONAL', 'EM41', NULL, 'HOSPITAL INTEGRAL ISLA MUJERES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (20, 'INSTITUCIONAL', 'EM42', NULL, 'DIRECTOR DEL HOSPITAL GENERAL', 'DR. FRANCISCO JOSE GRANADOS NAVA', '9842061690', NULL, 'DR. FRANCISCO JOSE GRANADOS NAVA',
        'AV. CONSTITUYENTES, ESQUINA 135 AV., COLONIA EJIDO, PLAYA DEL CARMEN, Q.ROO', '9842061690', NULL, NULL, NULL, 'Quintana Roo', 'SOLIDARIDAD', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (21, 'INSTITUCIONAL', 'EM43', NULL, 'HOSPITAL INTEGRAL KANTUNILKIN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (22, 'INSTITUCIONAL', 'EM44', NULL, 'C.S.U. 13', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (23, 'INSTITUCIONAL', 'EM45', NULL, 'HOSPITAL INTEGRAL BACALAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (24, 'INSTITUCIONAL', 'EM46', NULL, 'C.S.U. 2 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (25, 'INSTITUCIONAL', 'EM47', NULL, 'C.S.R. BUENA VISTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (26, 'INSTITUCIONAL', 'EM48', NULL, 'C.S.R. ALTOS DE SEVILLA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (27, 'INSTITUCIONAL', 'EM49', NULL, 'CERESO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (28, 'INSTITUCIONAL', 'EM50', 'LESP', 'LABORATORIO ESTATAL DE SALUD PÚBLICA', 'LABORATORIO ESTATAL DE SALUD PUBLICA', NULL, NULL, 'M.Pl. YOLANDA EK SOLIS',
        'AVENIDA MAXUXAC S/N., FRACC. RESIDENCIAL CHETUMAL', NULL, NULL, 'DIRECTORA', NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (29, 'INSTITUCIONAL', 'EM53', NULL, 'CCAYB CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (30, 'INSTITUCIONAL', 'EM54', NULL, 'UNEME CAPASITS CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (31, 'INSTITUCIONAL', 'EM55', NULL, 'C.S.U. 6 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (32, 'INSTITUCIONAL', 'EM57', NULL, 'C.S.R. NACHICOCOM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (33, 'INSTITUCIONAL', 'EM59', NULL, 'DR HOMERO LEON PEREZ', 'JEFE DE LA JURISDICCION SANITARIA # 2', ' 998 8886640', NULL, 'DR HOMERO LEON PEREZ',
        'CALLE 35 Y MIGUEL HIDALGO, REGION 93', ' 998 8886640', NULL, NULL, NULL, 'Quintana Roo', 'BENITO JUAREZ', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (34, 'INSTITUCIONAL', 'EM60', NULL, 'C.S.U. 1 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (35, 'INSTITUCIONAL', 'EM61', NULL, 'C.S.R. LIMONES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (36, 'INSTITUCIONAL', 'EM62', NULL, 'C.S.R. XUL-HA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (37, 'INSTITUCIONAL', 'EM63', NULL, 'C.S.R. JAVIER ROJO GOMEZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (38, 'INSTITUCIONAL', 'EM64', NULL, 'C.S.R. CAAN LUMIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (39, 'INSTITUCIONAL', 'EM65', NULL, 'C.S.R. BLANCA FLOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (40, 'INSTITUCIONAL', 'EM67', NULL, 'HOSPITAL GENERAL COZUMEL', NULL, ' 9878725182', NULL, NULL, NULL, ' 9878725182', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (41, 'INSTITUCIONAL', 'EM68', NULL, 'C.S.U. 10 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (42, 'INSTITUCIONAL', 'EM69', NULL, 'C.S.R. DZIUCHE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (43, 'INSTITUCIONAL', 'EM70', NULL, 'BRIGADA DE SALUD J3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (44, 'INSTITUCIONAL', 'EM75', NULL, 'C.S.R. RAMONAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (45, 'INSTITUCIONAL', 'EM78', NULL, 'C.S.R. ANDRES Q.ROO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (46, 'INSTITUCIONAL', 'EM79', NULL, 'C.S.R. PUCTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (47, 'INSTITUCIONAL', 'EM80', NULL, 'C.S.R. HUAY-PIX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (48, 'INSTITUCIONAL', 'EM81', NULL, 'C.S.R. X-PICHIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (49, 'INSTITUCIONAL', 'EM82', NULL, 'C.S.R. NUEVO ISRAEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (50, 'INSTITUCIONAL', 'EM83', NULL, 'C.S.R. SEÑOR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (51, 'INSTITUCIONAL', 'EM86', NULL, 'C.S.R. PUERTO MORELOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (52, 'INSTITUCIONAL', 'EM87', NULL, 'BRIGADA DE SALUD J1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (53, 'INSTITUCIONAL', 'EM88', NULL, 'C.S.R. MIGUEL ALEMAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (54, 'INSTITUCIONAL', 'EM90', NULL, 'C.S.R. SAN RAMON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (55, 'INSTITUCIONAL', 'EM92', NULL, 'C.S.U. 7 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (56, 'INSTITUCIONAL', 'EM93', NULL, 'C.S.R. DZULA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (57, 'INSTITUCIONAL', 'EM94', NULL, 'C.S.U. 1 DR. FRANCISCO ARANA TUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (58, 'INSTITUCIONAL', 'EM95', NULL, 'C.S.R. TUZIC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (59, 'INSTITUCIONAL', 'EM96', NULL, 'C.S.R. LA LIBERTAD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (60, 'INSTITUCIONAL', 'EM97', NULL, 'C.S.R. LAZARO CARDENAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (61, 'INSTITUCIONAL', 'EM98', NULL, 'C.S.R. RIO VERDE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (62, 'INSTITUCIONAL', 'EM99', NULL, 'C.S.R. NICOLAS BRAVO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (63, 'INSTITUCIONAL', 'EM101', NULL, 'C.S.R. TULUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (64, 'INSTITUCIONAL', 'EM104', NULL, 'C.S.R. COCOYOL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (65, 'INSTITUCIONAL', 'EM105', NULL, 'C.S.R. ALVARO OBREGON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (66, 'INSTITUCIONAL', 'EM107', NULL, 'C.S.R. KUCHUMATAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (67, 'INSTITUCIONAL', 'EM108', NULL, 'C.S.R. SERGIO BUTRON CASAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (68, 'INSTITUCIONAL', 'EM109', NULL, 'C.S.U. 3 COLOSIO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (69, 'INSTITUCIONAL', 'EM110', NULL, 'C.S.U. 9 CANCUN R-85', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (70, 'INSTITUCIONAL', 'EM112', NULL, 'C.S.R. TOMAS GARRIDO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (71, 'INSTITUCIONAL', 'EM113', NULL, 'C.S.R. CALDERITAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (72, 'INSTITUCIONAL', 'EM114', NULL, 'C.S.R. SAN PEDRO PERALTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (73, 'INSTITUCIONAL', 'EM115', NULL, 'C.S.R. MANUEL CRECENCIO REJON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (74, 'INSTITUCIONAL', 'EM116', NULL, 'C.S.R. SAN PEDRO PERALTA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (75, 'INSTITUCIONAL', 'EM117', NULL, 'C.S.R. FRANCISCO VILLA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (76, 'INSTITUCIONAL', 'EM118', NULL, 'C.S.R. LA LAGUNA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (77, 'INSTITUCIONAL', 'EM119', NULL, 'C.S.R. ROVIROSA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (78, 'INSTITUCIONAL', 'EM120', NULL, 'C.S.R. BUENA ESPERANZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (79, 'INSTITUCIONAL', 'EM121', NULL, 'C.S.R. PETCACAB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (80, 'INSTITUCIONAL', 'EM122', NULL, 'C.S.R. TEPICH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (81, 'INSTITUCIONAL', 'EM123', NULL, 'C.S.U. 7 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (82, 'INSTITUCIONAL', 'EM124', NULL, 'C.S.U. #16 TRES REYES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (83, 'INSTITUCIONAL', 'EM125', NULL, 'C.S.R. FELIPE ANGELES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (84, 'INSTITUCIONAL', 'EM126', NULL, 'C.S.R. OTILIO MONTAÑO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (85, 'INSTITUCIONAL', 'EM127', NULL, 'C.S.R. MOROCOY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (86, 'INSTITUCIONAL', 'EM128', NULL, 'CERESO CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (87, 'INSTITUCIONAL', 'EM130', NULL, 'C.S.R. MANUEL AVILA CAMACHO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (88, 'INSTITUCIONAL', 'EM131', NULL, 'C.S.R. ZAMORA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (89, 'INSTITUCIONAL', 'EM133', NULL, 'COOPERATIVA ESCOLAR ESCUELA PRIMARIA FORJADORES DE QUINTANA ROO', NULL, ' 983 1237342', NULL, NULL, NULL, ' 983 1237342', NULL, NULL,
        NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (90, 'INSTITUCIONAL', 'EM134', NULL, 'C.S.R. 3 GARANTIAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (91, 'INSTITUCIONAL', 'EM135', NULL, 'C.S.R. MAHAHUAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (92, 'INSTITUCIONAL', 'EM136', NULL, 'C.S.U. 4 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (93, 'INSTITUCIONAL', 'EM137', NULL, 'C.S.R. HOL-BOX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (94, 'INSTITUCIONAL', 'EM138', NULL, 'C.S.U. 3 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (95, 'INSTITUCIONAL', 'EM139', NULL, 'C.S.U. 6 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (96, 'INSTITUCIONAL', 'EM140', NULL, 'C.S.R. MELCHOR OCAMPO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (97, 'INSTITUCIONAL', 'EM141', NULL, 'C.S.R. LA UNION', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (98, 'INSTITUCIONAL', 'EM143', NULL, 'C.S.R. MARGARITA MAZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (99, 'INSTITUCIONAL', 'EM145', NULL, 'C.S.R. X-YATIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (100, 'INSTITUCIONAL', 'EM146', NULL, 'C.S.R. UCUM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (101, 'INSTITUCIONAL', 'EM147', NULL, 'C.S.R. PALMAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (102, 'INSTITUCIONAL', 'EM148', NULL, 'C.S.R. SABIDOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (103, 'INSTITUCIONAL', 'EM149', NULL, 'C.S.R. ALLENDE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (104, 'INSTITUCIONAL', 'EM150', NULL, 'C.S.R. LAGUNA GUERRERO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (105, 'INSTITUCIONAL', 'EM151', NULL, 'C.S.R. FILOMENO MATA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (106, 'INSTITUCIONAL', 'EM152', NULL, 'C.S.R. CHUNHUHUB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (107, 'INSTITUCIONAL', 'EM153', NULL, 'HOSPITAL NAVAL DE CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (108, 'INSTITUCIONAL', 'EM156', NULL, 'C.S.R. NUEVO JERUSALEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (109, 'INSTITUCIONAL', 'EM157', NULL, 'C.S.R. TINTAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (110, 'INSTITUCIONAL', 'EM159', NULL, 'DIRECCION DE PROTECCIÓN CONTRA RIESGOS SANITARIOS', 'DIRECTOR DE PROTECCIÓN CONTRA RIESGOS SANITARIOS', ' 983 8351948', NULL,
        'LIC. MIGUEL ALEJANDRO PINO MURILLO', 'AVENIDA CHAPULTEPEC #287 ESQUINA MORELOS, COLONIA CENTRO', ' 983 8351948', 'miguel.pino@salud.qroo.gob.mx', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (111, 'INSTITUCIONAL', 'EM160', NULL, 'C.S.R. HUATUSCO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (112, 'INSTITUCIONAL', 'EM163', NULL, 'C.S.R. CACAO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (113, 'INSTITUCIONAL', 'EM164', NULL, 'C.S.R. JUAN SARABIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (114, 'INSTITUCIONAL', 'EM166', NULL, 'C.S.U. 1 FELIPE CARRILLO PUERTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (115, 'INSTITUCIONAL', 'EM167', NULL, 'C.S.R. VALLEHERMOSO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (116, 'INSTITUCIONAL', 'EM168', NULL, 'BRIGADAS DE SALUD J2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (117, 'INSTITUCIONAL', 'EM169', NULL, 'C.S.R. FRANCISCO J. MUJICA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (118, 'INSTITUCIONAL', 'EM170', NULL, 'C.S.R. POL-YUC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (119, 'INSTITUCIONAL', 'EM173', NULL, 'C.S.R. JOAQUIN CETINA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (120, 'INSTITUCIONAL', 'EM174', NULL, 'C.S.R. CAOBAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (121, 'INSTITUCIONAL', 'EM176', NULL, 'C.S.R. NARANJAL PONIENTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (122, 'INSTITUCIONAL', 'EM177', NULL, 'C.S.U. 5 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (123, 'INSTITUCIONAL', 'EM178', NULL, 'HOSPITAL GENERAL DE CHETUMAL', NULL, ' 983 8321977', NULL, NULL, NULL, ' 983 8321977', NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (124, 'PARTICULAR', 'EM12', NULL, 'CAMPECHANO ISIDORO PETRA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (125, 'INSTITUCIONAL', 'EM181', NULL, 'HOSPITAL MILITAR DE ZONA', NULL, ' 9831219325', NULL, NULL, NULL, ' 9831219325', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (126, 'INSTITUCIONAL', 'EM185', NULL, 'C.S.R. ESTEBAN BACA CALDERON', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (127, 'INSTITUCIONAL', 'EM186', NULL, 'C.S.U. 4 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (128, 'INSTITUCIONAL', 'EM189', NULL, 'C.S.R. FRANCISCO MAY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (129, 'INSTITUCIONAL', 'EM191', NULL, 'C.S.R. TESORO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (130, 'INSTITUCIONAL', 'EM194', NULL, 'C.S.R. RIO ESCONDIDO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (131, 'INSTITUCIONAL', 'EM195', NULL, 'C.S.R. EL TESORO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (132, 'INSTITUCIONAL', 'EM196', NULL, 'C.S.R. REFORMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (133, 'INSTITUCIONAL', 'EM198', NULL, 'C.S.U. 11 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (134, 'INSTITUCIONAL', 'EM201', NULL, 'C.S.R. SAC-XAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (135, 'INSTITUCIONAL', 'EM202', NULL, 'C.S.U. 1 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (136, 'INSTITUCIONAL', 'EM203', NULL, 'C.S.R. PRESIDENTE JUAREZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (137, 'INSTITUCIONAL', 'EM205', NULL, 'C.S.R. KAMPOCOLCHE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (138, 'INSTITUCIONAL', 'EM206', NULL, 'C.S.R. PLAN DE LA NORIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (139, 'INSTITUCIONAL', 'EM207', NULL, 'C.S.U. 2 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (140, 'INSTITUCIONAL', 'EM208', NULL, 'C.S.R A.V. BONFIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (141, 'INSTITUCIONAL', 'EM209', NULL, 'C.S.U. 12 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (142, 'INSTITUCIONAL', 'EM210', NULL, 'C.S.R. SANTA ROSA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (143, 'INSTITUCIONAL', 'EM211', NULL, 'CAPASITS CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (144, 'INSTITUCIONAL', 'EM212', NULL, 'C.S.U. #16 TRES REYES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (145, 'INSTITUCIONAL', 'EM213', NULL, 'C.S.R. MAYA BALAM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (146, 'INSTITUCIONAL', 'EM215', NULL, 'C.S.R. CHACCHOBEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (147, 'INSTITUCIONAL', 'EM216', NULL, 'SAIS PLAYA DEL CARMEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (148, 'INSTITUCIONAL', 'EM220', NULL, 'C.S.U. 15 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (149, 'INSTITUCIONAL', 'EM243', NULL, 'C.S.R. GONZALEZ ORTEGA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (150, 'INSTITUCIONAL', 'EM246', NULL, 'C.S.R. RAUDALES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (151, 'INSTITUCIONAL', 'EM252', NULL, 'C.S.R. MIGUEL HIDALGO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (152, 'INSTITUCIONAL', 'EM254', NULL, 'C.S.U. #11', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (153, 'INSTITUCIONAL', 'EM258', NULL, 'C.S.R. SAN ISIDRO LA LAGUNA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (154, 'INSTITUCIONAL', 'EM260', NULL, 'C.S.R. LEONA VICARIO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (155, 'INSTITUCIONAL', 'EM262', NULL, 'C.S.U. 3 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (156, 'INSTITUCIONAL', 'EM263', NULL, 'C.S.R. LA ESPERANZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (157, 'INSTITUCIONAL', 'EM265', NULL, 'C.S.R. NUEVO BECAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (158, 'INSTITUCIONAL', 'EM274', NULL, 'C.S.R. CHUN POM', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (159, 'INSTITUCIONAL', 'EM276', NULL, 'C.S.R. LUIS ECHEVERRIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (160, 'INSTITUCIONAL', 'EM277', NULL, 'C.S.R. LA PANTERA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (161, 'INSTITUCIONAL', 'EM280', NULL, 'C.S.R. DZOYOLA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (162, 'INSTITUCIONAL', 'EM284', NULL, 'C.S.R. DIVORCIADOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (163, 'INSTITUCIONAL', 'EM304', NULL, 'C.S.R. LOS DIVORCIADOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (164, 'INSTITUCIONAL', 'EM308', NULL, 'C.S.R. KOMPOKOLCHE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (165, 'PARTICULAR', 'EM19', NULL, 'Nuevo Cliente', NULL, ' 7894561', NULL, NULL, NULL, ' 7894561', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (166, 'PARTICULAR', 'EM20', NULL, 'Nombre Clientes', NULL, ' 1245698', NULL, NULL, NULL, ' 1245698', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (167, 'INSTITUCIONAL', 'EM24', NULL, 'LABORATORIO ESTATAL DE SALUD PUBLICA DE TAMAULIPAS', NULL, '8343126636', NULL, 'Q.F.B. BERNARDITA DE LOURDES REYES BERRONES',
        'CENTRO EDUC. LIC. ADOLFO LOPEZ MATEOS', '8343126636', NULL, 'DIRECTORA', NULL, 'Tamaulipas', 'Victoria', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (168, 'PARTICULAR', 'EM25', NULL, 'C. YASSMINA MARYL PERALES PACHECO', 'GRUPO EXTERMINADOR DE LA RIVIERA MAYA', ' 9831194283', NULL, 'C. YASSMINA MARYLPERALES PACHECO',
        'CALLE CORNELIO LIZARRAGA, LOTE 19, MANZANA 44, COLONIA FORJADORES', ' 9831194283', 'control_exterminador@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (169, 'PARTICULAR', 'EM33', NULL, 'ISSSTE FELIPE CARRILLO PUERTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (170, 'PARTICULAR', 'EM40', NULL, 'IMSS 17 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (171, 'PARTICULAR', 'EM51', NULL, 'C. JUDITH ADRIANA SANCHEZ AGUILAR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (172, 'PARTICULAR', 'EM52', NULL, 'IMSS 18 PLAYA DEL CARMEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (173, 'PARTICULAR', 'EM58', NULL, 'AGROQUIMICOS EL CAMPO', NULL, ' 983 8323307', NULL, NULL, NULL, ' 983 8323307', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (174, 'PARTICULAR', 'EM66', NULL, 'IMSS CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (175, 'PARTICULAR', 'EM71', NULL, 'IMSS # 7 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (176, 'PARTICULAR', 'EM72', NULL, 'PURIFICADORA ECOBLUE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (177, 'PARTICULAR', 'EM74', NULL, 'ANTOJITOS LAS MARIAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (178, 'PARTICULAR', 'EM76', NULL, 'C. JOSE DEL CARMEN UICAB ESTRELLA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (179, 'PARTICULAR', 'EM77', NULL, 'AGUA INMACULADA.COM', NULL, ' 998241355', NULL, NULL, NULL, ' 998241355', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (180, 'PARTICULAR', 'EM84', NULL, 'ISSSTE CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (181, 'PARTICULAR', 'EM89', NULL, 'CLINICA CARRANZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (182, 'PARTICULAR', 'EM91', NULL, 'ISSSTE CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (183, 'PARTICULAR', 'EM100', NULL, 'C. SANDY GABRIELA MOSQUEDA GARCIA', 'TORTILLERIA BAYUL', ' 9831731963, 9831090955', NULL, 'C. SANDY GABRIELA MOSQUEDA GARCÍA',
        'CALLE TEPICH #200, MANZANA 270, LOTE 16, COL. PROTERRITORIO', ' 9831731963, 9831090955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (184, 'PARTICULAR', 'EM102', NULL, 'PISCICOLA OCHOA HA S.A. DE C.V.', NULL, ' 983 1051087', NULL, NULL, NULL, ' 983 1051087', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (185, 'PARTICULAR', 'EM103', NULL, 'SERGIO BUTRON CASAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (186, 'PARTICULAR', 'EM111', NULL, 'ENFERMERIA MILITAR DE ZONA CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (187, 'PARTICULAR', 'EM129', NULL, 'IMSS 11 PLAYA DEL CARMEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (188, 'PARTICULAR', 'EM132', NULL, 'IMSS 03 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (189, 'PARTICULAR', 'EM142', NULL, 'IMSS 13 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (190, 'PARTICULAR', 'EM144', NULL, 'TIENDAS SORIANA S.A. DE C.V.', NULL, '2673360', NULL, 'TIENDAS SORIANA S.A. DE C.V.',
        'ALEJANDRO DE RODAS #3102-A, COLONIA CUMBRES 8VO SECTOR, MONTERREY', '2673360', 'gerencias527@soriana.com', NULL, NULL, 'Nuevo León', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (191, 'PARTICULAR', 'EM154', 'MICHELER', 'MICHELERIA MAXCANU', NULL, ' 983 7009230', NULL, NULL, NULL, ' 983 7009230', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (192, 'PARTICULAR', 'EM158', NULL, 'IMSS PUCTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (193, 'PARTICULAR', 'EM161', NULL, 'BABUTE BAR', NULL, ' 983 1520800', NULL, NULL, NULL, ' 983 1520800', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (194, 'PARTICULAR', 'EM162', NULL, 'POLLOS ASADOS Y EMPANIZADOS VALENTINO', NULL, ' 9831213759', NULL, NULL, NULL, ' 9831213759', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (195, 'PARTICULAR', 'EM171', NULL, 'BAR OCTOPUSIS', NULL, ' 983 1663169', NULL, NULL, NULL, ' 983 1663169', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (196, 'PARTICULAR', 'EM172', NULL, 'C. Y C. FRONTERIZA, S.A. DE C.V. HOTEL CAPITAL PLAZA', NULL, ' 983 8350400', NULL, NULL, NULL, ' 983 8350400', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (197, 'PARTICULAR', 'EM175', NULL, 'PANADERIA LOS NOVELO', NULL, ' 983 1034073', NULL, NULL, NULL, ' 983 1034073', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (198, 'PARTICULAR', 'EM179', NULL, 'PASTELERIA MARILUI', NULL, ' 983 8321242', NULL, NULL, NULL, ' 983 8321242', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (199, 'PARTICULAR', 'EM180', NULL, 'NOVEDADES TAI-PEI', NULL, ' 983 8334630', NULL, NULL, NULL, ' 983 8334630', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (200, 'PARTICULAR', 'EM182', NULL, 'TORTILLERIA ARIDAY', NULL, ' 983 1371483', NULL, NULL, NULL, ' 983 1371483', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (201, 'PARTICULAR', 'EM183', NULL, 'TORTILLERIA CHELITO', NULL, ' 9831192797', NULL, NULL, NULL, ' 9831192797', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (202, 'PARTICULAR', 'EM184', NULL, 'PANADERIA BELEM', NULL, ' 983 8327592 / 9831205621', NULL, NULL, NULL, ' 983 8327592 / 9831205621', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (203, 'PARTICULAR', 'EM187', NULL, 'LABORATORIO CENTRAL DE BELICE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (204, 'PARTICULAR', 'EM188', NULL, 'C. VIRGILIO MENDOZA CIAU', NULL, ' 983 1258732', NULL, NULL, NULL, ' 983 1258732', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (205, 'PARTICULAR', 'EM192', NULL, 'TORTILLERIA MORALES', NULL, ' 983 1340413', NULL, NULL, NULL, ' 983 1340413', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (206, 'PARTICULAR', 'EM193', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.', NULL, ' 9831776099', NULL, NULL, NULL, ' 9831776099', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (207, 'PARTICULAR', 'EM197', NULL, 'PROCESADORA DE CARNES LA ALIANZA S.A. DE C.V.', NULL, ' 983 1324623', NULL, NULL, NULL, ' 983 1324623', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (208, 'PARTICULAR', 'EM199', NULL, 'IMSS COZUMEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (209, 'PARTICULAR', 'EM200', NULL, 'CAPASITS CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (210, 'PARTICULAR', 'EM204', NULL, 'CARNITAS EL TABASQUEÑO', NULL, ' 9831050806', NULL, NULL, NULL, ' 9831050806', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (211, 'PARTICULAR', 'EM214', NULL, 'FUMIGACIONES INTEGRALES DE QUINTANA ROO', NULL, ' 983 1162947', NULL, 'C. SEBASTIANA RUIZ CRUZ', 'AVENIDA ERICK PAOLO MARTINEZ #100',
        ' 983 1162947', 'acatic51@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (212, 'PARTICULAR', 'EM217', NULL, 'RESTAURANT TACO VIVA', NULL, ' 983 7005324', NULL, NULL, NULL, ' 983 7005324', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (213, 'PARTICULAR', 'EM218', NULL, 'C. NOE ESAU PALOMO CANTO', NULL, ' 983 1035000', NULL, NULL, NULL, ' 983 1035000', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (214, 'PARTICULAR', 'EM219', NULL, 'PANADERIA VICKY', NULL, ' 983 1641965', NULL, NULL, NULL, ' 983 1641965', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (215, 'PARTICULAR', 'EM221', NULL, 'RESTAURANTE EL ZURDITO', NULL, ' 9831027335', NULL, NULL, NULL, ' 9831027335', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (216, 'PARTICULAR', 'EM222', NULL, 'IMPORTACIONES BAROUDI S.A. DE C.V.', NULL, ' 98320587', NULL, NULL, NULL, ' 98320587', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (217, 'PARTICULAR', 'EM223', NULL, 'DESPACHADOR DE AGUA PURIFICADA HIDROLIVE', NULL, ' 9831138332', NULL, NULL, NULL, ' 9831138332', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (218, 'PARTICULAR', 'EM224', NULL, 'COCINA ECONOMICA LUPITA', NULL, ' 9837533956', NULL, NULL, NULL, ' 9837533956', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (219, 'PARTICULAR', 'EM225', NULL, 'RESTAURANTE EL CHARALITO', NULL, ' 2850027', NULL, NULL, NULL, ' 2850027', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (220, 'PARTICULAR', 'EM226', NULL, 'RESTAURANTE BAR MAR CARIBE', NULL, ' 8326289', NULL, NULL, NULL, ' 8326289', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (221, 'PARTICULAR', 'EM227', NULL, 'C. JORGE HUMBERTO CALDERON RAMOS', 'MARISQUERIA JORCH EL GEMELO', ' 9831180277', NULL, 'C. JORGE HUMBERTO CALDERON RAMOS',
        'LAGUNA DE BACALAR #386 ESQUINA MILÁN', ' 9831180277', 'chris_21_hillfox@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (222, 'PARTICULAR', 'EM228', NULL, 'GV DE LA PENÍNSULA S.C. DE R.L. DE C.V.', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'GV DE LA PENÍNSULA S.C. DE R.L. DE C.V.',
        'CALLE FLAMBOYAN S/N. ENTRE MACULIX Y GUAYACAN, COL. ARBOLEDAS', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (223, 'PARTICULAR', 'EM229', NULL, 'C. ZOILA PEREZ DE LA CRUZ', 'CARNICERÍA, FRUTAS Y VERDURAS EL CEPILLO II', ' 9831180277', NULL, 'C. ZOILA PEREZ DE LA CRUZ',
        'PETCACAB, MANZANA 128, LOTE 4, COLONIA SOLIDARIDAD', ' 9831180277', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (224, 'PARTICULAR', 'EM230', NULL, 'G.V. DE LA PENINSULA S.C. DE R.L. DE C.V.', NULL, ' 9831180277', NULL, NULL, NULL, ' 9831180277', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (225, 'PARTICULAR', 'EM231', NULL, 'POLLO DORADO', NULL, ' 9838376060', NULL, NULL, NULL, ' 9838376060', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (226, 'PARTICULAR', 'EM232', NULL, 'TAQUERIA TERRAMAR', NULL, ' 9831541351', NULL, NULL, NULL, ' 9831541351', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (227, 'PARTICULAR', 'EM233', NULL, 'DESPACHADOR DE AGUA LA REJOLLADA', NULL, ' 8326997', NULL, NULL, NULL, ' 8326997', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (228, 'PARTICULAR', 'EM234', NULL, 'C. JAVIER ALEJANDRO BASTO PATRACA', 'DISPENSADOR DE AGUA AQUACLYVA', '9832107201', NULL, 'C. JAVIER ALEJANDRO BASTO PATRACA',
        'ISLAS VIRGENES #444 ENTRE NICOLAS BRAVO Y NIZUC', '9832107201', 'alex_ab100@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (229, 'PARTICULAR', 'EM235', NULL, 'AGUA PURIFICADA MAJAGUAL', NULL, ' 9838345757', NULL, NULL, NULL, ' 9838345757', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (230, 'PARTICULAR', 'EM236', NULL, 'LONCHERIA DOÑA PAZ', NULL, ' 9831077692', NULL, NULL, NULL, ' 9831077692', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (231, 'PARTICULAR', 'EM237', NULL, 'MR.CAMARON', NULL, ' 9831551908', NULL, NULL, NULL, ' 9831551908', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (232, 'PARTICULAR', 'EM238', NULL, 'TORITLLERIA EL MICHOACANO', NULL, ' 9831544747', NULL, NULL, NULL, ' 9831544747', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (233, 'PARTICULAR', 'EM239', NULL, 'MOTEBURGER S.A. DE C.V.', 'BURGUER KING', '1275505', NULL, 'MOTEBURGER S.A. DE C.V.', 'AVENIDA INSURGENTES CON CALLE DZISAUCHE',
        '1275505', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (234, 'PARTICULAR', 'EM240', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.', NULL, ' 9831131075', NULL, NULL, NULL, ' 9831131075', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (235, 'PARTICULAR', 'EM241', NULL, 'COCINA ECONOMICA SI CHABE RICO', NULL, ' 2087008 CEL.9838391001', NULL, NULL, NULL, ' 2087008 CEL.9838391001', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (236, 'PARTICULAR', 'EM242', NULL, 'RESTAURANTE APPLEBEES', NULL, ' 9831232138/9831271269', NULL, NULL, NULL, ' 9831232138/9831271269', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (237, 'PARTICULAR', 'EM244', NULL, 'PURIFICADORA LA ECONOMICA', NULL, ' 9837532571', NULL, NULL, NULL, ' 9837532571', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (238, 'PARTICULAR', 'EM245', NULL, 'LONCHERIA LA CENTRAL', NULL, ' 9838366370', NULL, NULL, NULL, ' 9838366370', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (239, 'PARTICULAR', 'EM247', NULL, 'PURIFICADORA EL PONTON CHACTEMAL', NULL, ' 9831169583', NULL, NULL, NULL, ' 9831169583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (240, 'PARTICULAR', 'EM248', NULL, 'DESPACHADOR LA CALENTANA', NULL, ' 9831642810', NULL, NULL, NULL, ' 9831642810', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (241, 'PARTICULAR', 'EM249', NULL, 'SAIS HG FELIPE CARRILLO PUERTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (242, 'PARTICULAR', 'EM250', NULL, 'COCINA ECONOMICA LA MUÑECA', NULL, ' 9838399104', NULL, NULL, NULL, ' 9838399104', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (243, 'PARTICULAR', 'EM251', NULL, 'PURIFICADORA MANATI EXPRESS', NULL, ' 9831224365', NULL, NULL, NULL, ' 9831224365', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (244, 'PARTICULAR', 'EM253', NULL, 'DESPACHADOR AUTOMATICO MANATI EXPRESS 1', NULL, ' 9831224365', NULL, NULL, NULL, ' 9831224365', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (245, 'INSTITUCIONAL', 'EM313', NULL, 'C.S.R. NUEVO VERACRUZ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (246, 'INSTITUCIONAL', 'EM314', NULL, 'C.R.S DOS AGUADAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (247, 'PARTICULAR', 'EM315', NULL, 'C. ROSALINDA HEREDIA LLANOS', 'ESCUELA HIDALGO TIENDA ESCOLAR', ' 8320872', NULL, 'C. ROSALINDA HEREDIA LLANOS',
        'AVENIDA 16 DE SEPTIEMBRE N°28', ' 8320872', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (248, 'PARTICULAR', 'EM316', NULL, 'C. ALAN ROSADO CARBALLO', NULL, ' 9838323692', NULL, NULL, NULL, ' 9838323692', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (249, 'INSTITUCIONAL', 'EM318', NULL, 'C.S.U. #14 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (250, 'INSTITUCIONAL', 'EM322', NULL, 'C.S.R. CHUN-YAH', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (251, 'INSTITUCIONAL', 'EM330', NULL, 'C.S.R. PUERTO AVENTURAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (252, 'INSTITUCIONAL', 'EM343', NULL, 'COMITE ESTATAL DE SANIDAD VEGETAL DE QUINTANA ROO', NULL, ' 9838326739', NULL, NULL, NULL, ' 9838326739', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (253, 'INSTITUCIONAL', 'EM352', NULL, 'C.S.R. NOH BEC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (254, 'INSTITUCIONAL', 'EM380', NULL, 'C.S.R. SAN ROMAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (255, 'INSTITUCIONAL', 'EM384', NULL, 'C.S.U. 15 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (256, 'INSTITUCIONAL', 'EM387', NULL, 'C.A.C. COZUMEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (257, 'INSTITUCIONAL', 'EM392', NULL, 'C.S.R. SAN DIEGO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (258, 'INSTITUCIONAL', 'EM395', NULL, 'CENTRO DE DESARROLLO INFANTIL (CENDI II)', NULL, ' 983 1271610', NULL, NULL, NULL, ' 983 1271610', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (259, 'INSTITUCIONAL', 'EM403', NULL, 'C.S.R. SAN ANGEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (260, 'INSTITUCIONAL', 'EM425', NULL, 'C.S.R. TIXCACAL GUARDIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (261, 'INSTITUCIONAL', 'EM429', NULL, 'COMISIÓN PARA LA JUVENTUD Y EL DEPORTE DE QUINTANA ROO', NULL, ' 9831685877', NULL, NULL, NULL, ' 9831685877', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (262, 'INSTITUCIONAL', 'EM436', NULL, 'C.S.R. SAN SILVERIO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (263, 'INSTITUCIONAL', 'EM439', NULL, 'C.S.R. LA PRESUMIDA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (264, 'INSTITUCIONAL', 'EM444', NULL, 'C.S.R. YAXLEY', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (265, 'INSTITUCIONAL', 'EM452', NULL, 'C.S.R. TIHOSUCO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (266, 'INSTITUCIONAL', 'EM465', NULL, 'C.S.R. YOACTUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (267, 'INSTITUCIONAL', 'EM466', NULL, 'C.S.R. RAMONAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (268, 'INSTITUCIONAL', 'EM571', NULL, 'C.S.R. ANDRES Q.ROO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (269, 'INSTITUCIONAL', 'EM605', NULL, 'C.S.R. 18 DE MARZO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (270, 'INSTITUCIONAL', 'EM626', NULL, 'C.S.R. LAGUNA KANA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (271, 'INSTITUCIONAL', 'EM628', NULL, 'C.S.R. FRANCISCO I MADERO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (272, 'INSTITUCIONAL', 'EM640', NULL, 'C.S.R. COBA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (273, 'INSTITUCIONAL', 'EM661', NULL, 'C.S.U 5 CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (274, 'INSTITUCIONAL', 'EM698', NULL, 'CENDI 2 DIF', NULL, ' 9831271610', NULL, NULL, NULL, ' 9831271610', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (275, 'INSTITUCIONAL', 'EM699', NULL, 'DEPARTAMENTO DE ENFERMEDADES TRANSMITIDAS POR VECTOR Y ZOONOSIS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (276, 'INSTITUCIONAL', 'EM710', NULL, 'JURISDICCIÓN # 1 CONTROL VENEREO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (277, 'INSTITUCIONAL', 'EM712', NULL, 'CENTRO MEDICO DE COZUMEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (278, 'INSTITUCIONAL', 'EM727', NULL, 'C.S.R. GAVILANES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (279, 'INSTITUCIONAL', 'EM739', NULL, 'C.S.U. # 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (280, 'INSTITUCIONAL', 'EM740', NULL, 'C.S.U. # 5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (281, 'INSTITUCIONAL', 'EM753', NULL, 'C.S.R. CHUN HUAS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (282, 'INSTITUCIONAL', 'EM765', NULL, 'C.S.U. # 2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (283, 'INSTITUCIONAL', 'EM782', NULL, 'C.S.R. EL IDEAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (284, 'INSTITUCIONAL', 'EM786', NULL, 'C.S.R. CARLOS A. MADRAZO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (285, 'INSTITUCIONAL', 'EM787', NULL, 'C.S.R. SABAN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (286, 'INSTITUCIONAL', 'EM796', NULL, 'C.S.R. CHIQUILA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (287, 'INSTITUCIONAL', 'EM807', NULL, 'C.S.U. 16 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (288, 'INSTITUCIONAL', 'EM841', NULL, 'C.S.R. KANCABCHEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (289, 'INSTITUCIONAL', 'EM982', NULL, 'C.S.R. CANDELARIA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (290, 'INSTITUCIONAL', 'EM1063', NULL, 'C.S.R. DZOYUMA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (291, 'INSTITUCIONAL', 'EM1102', NULL, 'C.S.R. PEDRO A. SANTOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (292, 'INSTITUCIONAL', 'EM1115', NULL, 'C.S.R. IGNACIO ZARAGOZA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (293, 'INSTITUCIONAL', 'J1', NULL, 'COORDINACIÓN DE PROTECCIÓN CONTRA RIESGOS SANITARIOS ZONA SUR', NULL, ' 1292708', NULL, 'LIC. OSCAR BARRADAS MARTINEZ',
        'AVENIDA ANDRES QUINTANA ROO ESQUINA CHAPULTEPEC, COLONIA CENTRO', ' 1292708', 'oscarbarradasmartinez@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (294, 'INSTITUCIONAL', 'EM1124', NULL, 'COORDINADOR DE PROTECCIÓN CONTRA RIESGOS SANITARIOS ZONA CENTRO', 'COORDINADOR DE PROTECCIÓN CONTRA RIESGOS SANITARIOS ZONA CENTRO',
        ' 9848032526', NULL, 'C. JAVIER FRANCISCO TOLEDO ALVARADO', 'AVENIDA BENITO JUÁREZ ESQUINA CON AVENIDA 15 SUR, COLONIA CENTRO', ' 9848032526',
        'javierfrancisco_toledoalvarado@hotmail.c', NULL, NULL, 'Quintana Roo', 'SOLIDARIDAD', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (295, 'INSTITUCIONAL', 'EM1125', NULL, 'COORDINA', NULL, ' 9988841892', NULL, NULL, NULL, ' 9988841892', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (296, 'INSTITUCIONAL', 'EM1126', NULL, 'C.S.R. SAN MARTINIANO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (297, 'INSTITUCIONAL', 'EM1130', NULL, 'C.S.R. SAN FELIPE I', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (298, 'INSTITUCIONAL', 'EM1129', NULL, 'C.S.R. OTHON P. BLANCO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (299, 'INSTITUCIONAL', 'EM1180', NULL, 'DIRECTOR DE PROTECCIÓN CONTRA RIESGOS SANITARIOS JS3', NULL, ' 983 8351948', NULL, NULL, NULL, ' 983 8351948', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (300, 'INSTITUCIONAL', 'EM1188', NULL, 'C.S.U. 1 COZUMEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (301, 'INSTITUCIONAL', 'EM1189', NULL, 'CENDI DIF 1', NULL, ' 9837534149', NULL, NULL, NULL, ' 9837534149', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (302, 'INSTITUCIONAL', 'EM1220', NULL, 'H. AYUNTAMIENTO OTHON P. BLANCO MUNICIPIO', NULL, ' 9831215699', NULL, NULL, NULL, ' 9831215699', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (303, 'INSTITUCIONAL', 'EM1263', NULL, 'C.S.R. EJIDO ISLA MUJERES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (304, 'INSTITUCIONAL', 'EM1272', NULL, 'C.S.R. ADOLFO LOPEZ MATEOS', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (305, 'INSTITUCIONAL', 'EM1282', NULL, 'C.S.R. SACALACA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (306, 'INSTITUCIONAL', 'EM1315', NULL, 'PANADERIA DEL DIF', NULL, ' 9831383565', NULL, NULL, NULL, ' 9831383565', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (307, 'INSTITUCIONAL', 'EM1338', NULL, 'C.S.R. SAN FELIPE II', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (308, 'INSTITUCIONAL', 'EM1387', NULL, 'C.S.R. ZETINA GASCA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (309, 'INSTITUCIONAL', 'EM1427', NULL, 'C.S.R.POZO PIRATA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (310, 'INSTITUCIONAL', 'EM1455', NULL, 'C.S.R. NARANJAL ORIENTE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (311, 'INSTITUCIONAL', 'EM1472', NULL, 'C.S.U. 2 PLAYA DEL CARMEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (312, 'INSTITUCIONAL', 'PC', NULL, 'Playa Casitas', NULL, NULL, NULL, NULL, 'Direccion 1', NULL, NULL, NULL, NULL, 'Quintana Roo', 'COZUMEL', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (313, 'PARTICULAR', '405', 'RZD', 'C. ZHANG DAREN', 'RESTAURANTE DE COMIDA CHINA ZHANG DAREN', '9831577049', NULL, 'C. ZHANG DAREN',
        'AVENIDA INSURGENTES #486 LOCAL B COLONIA 20 DE NOVIEMBRE', '9831577049', 'clemente.gutierrez23@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (314, 'PARTICULAR', 'EM255', NULL, 'RESTAURANTE EL RINCON DE LAS TORTUGAS', NULL, ' 2850155', NULL, NULL, NULL, ' 2850155', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (315, 'PARTICULAR', 'EM256', NULL, 'ISSSTE COZUMEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (316, 'PARTICULAR', 'EM257', NULL, 'XXXXXXXXXXXXXXXXXXXXXXXXXXXX', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (317, 'PARTICULAR', 'EM259', NULL, 'RESTAURANT LA CHIAPANECA', NULL, ' 9838358802', NULL, NULL, NULL, ' 9838358802', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (318, 'PARTICULAR', 'EM261', NULL, 'CANUL RUIZ ROBERTO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (319, 'PARTICULAR', 'EM264', NULL, 'RESTAURANTE KELLYS CAJUN GRILL', NULL, ' 8372433', NULL, NULL, NULL, ' 8372433', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (320, 'PARTICULAR', 'EM266', NULL, 'PURIFICADORA BELEM', NULL, ' 9837522050', NULL, NULL, NULL, ' 9837522050', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (321, 'PARTICULAR', 'EM267', NULL, 'TORTILLERIA EL MICHOACANO', NULL, ' 9831596830', NULL, NULL, NULL, ' 9831596830', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (322, 'PARTICULAR', 'EM268', NULL, 'AGUA PURIFICADA BELEM', NULL, ' 9831324522', NULL, NULL, NULL, ' 9831324522', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (323, 'PARTICULAR', 'EM269', NULL, 'PLANTA PURIFICADORA AQUA ACTIVA', NULL, ' 1182115', NULL, NULL, NULL, ' 1182115', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (324, 'PARTICULAR', 'EM270', NULL, 'DIRECTOR DE SERVICIOS DE SALUD DE LA SECRETARIA DE SALUD', NULL, ' 9838351927', NULL, NULL, NULL, ' 9838351927', NULL, NULL, NULL, NULL,
        NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (325, 'PARTICULAR', 'EM271', NULL, 'FUMIGACIONES CLEAN HOUSE', NULL, ' 9831161209', NULL, NULL, NULL, ' 9831161209', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (326, 'PARTICULAR', 'EM272', NULL, 'COCINA ECONOMICA POLLO DORADO', NULL, ' 8376060', NULL, NULL, NULL, ' 8376060', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (327, 'PARTICULAR', 'EM273', NULL, 'PURIFICADORA EL PALMAR', NULL, ' 9831120345', NULL, NULL, NULL, ' 9831120345', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (328, 'PARTICULAR', 'EM275', NULL, 'DESPACHADOR DE AGUA ECOVITA', NULL, ' 9831546859', NULL, NULL, NULL, ' 9831546859', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (329, 'PARTICULAR', 'EM278', NULL, 'PURIFICADORA ECOVITA ECOG2-FRA010', NULL, ' 9992227818', NULL, NULL, NULL, ' 9992227818', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (330, 'PARTICULAR', 'EM279', NULL, 'RESTAURANTE EL RINCON DE ANDALUZ', NULL, ' 2854259', NULL, NULL, NULL, ' 2854259', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (331, 'PARTICULAR', 'EM281', NULL, 'PURIFICADORA QUE BUENA', NULL, ' 2855707', NULL, NULL, NULL, ' 2855707', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (332, 'PARTICULAR', 'EM282', NULL, 'RESTAURANT Y MARISQUERIA MI VIEJO', NULL, ' 9831262363', NULL, NULL, NULL, ' 9831262363', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (333, 'PARTICULAR', 'EM283', NULL, 'BOY´S PIZZA', NULL, ' 98312220648', NULL, NULL, NULL, ' 98312220648', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (334, 'PARTICULAR', 'EM285', NULL, 'MULTISERVICIOS INMOBILIARIOS', NULL, ' 984060172', NULL, NULL, NULL, ' 984060172', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (335, 'PARTICULAR', 'EM286', NULL, 'MARISQUERIA EL COSTEÑITO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (336, 'PARTICULAR', 'EM287', NULL, 'COCTELERIA RINCON DE ACAPULCO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (337, 'PARTICULAR', 'EM288', NULL, 'C. PEDRO PEREZ RODRIGUEZ', 'RESTAURANTE EL SANTUARIO DEL MANATI', ' 9838677106', NULL, 'C. PEDRO PEREZ RODRIGUEZ',
        'CALLE OAXACA, PALAPA 16 ENTRE TABASCO Y TLAXCALA, CALDERITAS, Q.ROO', ' 9838677106', 'pedrop_periko@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (338, 'PARTICULAR', 'EM289', NULL, 'TACOS Y MARISCOS INDEPENDENCIA', NULL, ' 1442023 / 9831321424', NULL, NULL, NULL, ' 1442023 / 9831321424', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (339, 'PARTICULAR', 'EM290', NULL, 'COCTELERIA EL MUELLECITO', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (340, 'PARTICULAR', 'EM291', NULL, 'BAR 2 LEOS', NULL, ' 9831664318', NULL, NULL, NULL, ' 9831664318', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (341, 'PARTICULAR', 'EM292', NULL, 'TORTILLERIA LA ASUNCION', NULL, ' 9831589393', NULL, NULL, NULL, ' 9831589393', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (342, 'PARTICULAR', 'EM293', NULL, 'RESTAURANTE VENTOLERA', NULL, ' 2678370 ext 3', NULL, NULL, NULL, ' 2678370 ext 3', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (343, 'PARTICULAR', 'EM294', NULL, 'TAQUERIA 3 CHEREPITOS', NULL, ' 9831198569', NULL, NULL, NULL, ' 9831198569', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (344, 'PARTICULAR', 'EM295', NULL, 'DISPENSADOR DE AGUA PURIFICADA AQUADIA', NULL, ' 9831106065 / 1441513', NULL, NULL, NULL, ' 9831106065 / 1441513', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (345, 'PARTICULAR', 'EM296', NULL, 'BARBACOA HIDALGO', NULL, ' 9831045099', NULL, NULL, NULL, ' 9831045099', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (346, 'PARTICULAR', 'EM297', NULL, 'CEBIAM PLAYA DEL CARMEN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (347, 'PARTICULAR', 'EM298', NULL, 'BABU TE BAR', NULL, ' 983129266', NULL, NULL, NULL, ' 983129266', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (348, 'PARTICULAR', 'EM299', NULL, 'RESTAURANTE BAR LICOS', NULL, ' 9831431717', NULL, NULL, NULL, ' 9831431717', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (349, 'PARTICULAR', 'EM300', NULL, 'C. MARIA JESUS MATUS CORAL', 'TAQUERIA MARY', ' 9831302476', NULL, 'MARIA JESUS MATUS CORAL',
        'INTERIOR MERCADO IGNACIO ALTAMIRANO, LOCAL 110', ' 9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (350, 'PARTICULAR', 'EM301', NULL, 'C. HUMBERTO VELAZQUEZ JAIMES', NULL, '9831432561', NULL, 'HUMBERTO VELAZQUEZ JAIMES', 'MANUEL ACUÑA #78, COLONIA JARDINES', '9831432561',
        'oyargo1621@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (351, 'PARTICULAR', 'EM302', NULL, 'CAT´S BURGER', NULL, ' 9831362488', NULL, NULL, NULL, ' 9831362488', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (352, 'PARTICULAR', 'EM303', NULL, 'LONCHERIA GUISSO RICO', NULL, ' 9831393940', NULL, NULL, NULL, ' 9831393940', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (353, 'PARTICULAR', 'EM305', NULL, 'TAQUERIA EL PAISANO JR.', NULL, ' 9831025304', NULL, NULL, NULL, ' 9831025304', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (354, 'PARTICULAR', 'EM306', NULL, 'PLANTA PURIFICADORA DE AGUA NATURAL', NULL, ' 9831207620 / 9831184792', NULL, NULL, NULL, ' 9831207620 / 9831184792', NULL, NULL, NULL,
        NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (355, 'PARTICULAR', 'EM307', NULL, 'PURIFICADORA AGUAPURA', NULL, ' 9971104205', NULL, NULL, NULL, ' 9971104205', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (356, 'PARTICULAR', 'EM309', NULL, 'DISPENSADOR DE AGUA PURIFICADA SAN DIEGO', NULL, ' 9831332308', NULL, NULL, NULL, ' 9831332308', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (357, 'PARTICULAR', 'EM310', NULL, 'PURIFICADORA ACTIVA', NULL, ' 1182115', NULL, NULL, NULL, ' 1182115', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (358, 'PARTICULAR', 'EM311', NULL, 'DISPENSADOR DE AGUA PURIFICADA AQUA ACTIVA', NULL, ' 1182115', NULL, NULL, NULL, ' 1182115', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (359, 'PARTICULAR', 'EM317', NULL, 'PLANTA PURIFICADORA DE AGUA SAN RAFAEL', NULL, ' 9831144853', NULL, NULL, NULL, ' 9831144853', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (360, 'PARTICULAR', 'EM319', NULL, 'HOTEL RANCHO ENCANTADO', NULL, ' 9831017376', NULL, NULL, NULL, ' 9831017376', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (361, 'PARTICULAR', 'EM320', NULL, 'PURIFICADORA EL PONTON CHACTEMAL', NULL, ' 9831169583', NULL, NULL, NULL, ' 9831169583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (362, 'PARTICULAR', 'EM321', NULL, 'TIENDA ESCOLAR DAVID ALFARO SIQUEIROS', NULL, ' 9831307129', NULL, NULL, NULL, ' 9831307129', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (363, 'PARTICULAR', 'EM323', NULL, 'TORTAS EL CAMPEON', NULL, ' 9831230965', NULL, NULL, NULL, ' 9831230965', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (364, 'PARTICULAR', 'EM324', NULL, 'PURIFICADORA LA GUADALUPANA', NULL, ' 9831195257', NULL, NULL, NULL, ' 9831195257', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (365, 'PARTICULAR', 'EM325', NULL, 'C. ALEJANDRINA O. FLOTA VELASQUEZ', 'COCINA ECONOMICA GETSEMANI', ' 9831174498', NULL, 'C. ALEJANDRINA O. FLOTA VELASQUEZ',
        'CALLE MANUEL EVIA CÁMARA #476 ENTRE MANUEL SEVILLA Y ESTADOS UNIDOS', ' 9831174498', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (366, 'PARTICULAR', 'EM326', NULL, 'PURIFICADORA ECOBLUE', NULL, ' 9831174498', NULL, NULL, NULL, ' 9831174498', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (367, 'PARTICULAR', 'EM327', NULL, 'PURIFICADORA ECOBLUE', NULL, ' 9831174498', NULL, NULL, NULL, ' 9831174498', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (368, 'PARTICULAR', 'EM328', NULL, 'ESTANCIA INFANTIL MI MUNDO MAGICO', NULL, ' 9831112277', NULL, NULL, NULL, ' 9831112277', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (369, 'PARTICULAR', 'EM329', NULL, 'C. CRISTINA GUADALUPE LARA QUIÑONES', 'ESTANCIA INFANTIL DA VINCI', ' 9832853544, 9831079017', NULL, 'CRISTINA GUADALUPE LARA QUIÑONES',
        'AV. NAPOLES #359 ENTRE ISLA CANCUN Y LAGUNA DE BACALAR, COL. BENITO JUAREZ', ' 9832853544, 9831079017', 'crizzty@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (370, 'PARTICULAR', 'EM331', NULL, 'TORTILLERIA MAYA PAKAL', NULL, ' 9831142710', NULL, NULL, NULL, ' 9831142710', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (371, 'PARTICULAR', 'EM332', NULL, 'TORTILLERIA NORMITA 2', NULL, ' 9831142710', NULL, NULL, NULL, ' 9831142710', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (372, 'PARTICULAR', 'EM333', NULL, 'TORTILLERIA NORMITA 1', NULL, ' 9831142710', NULL, NULL, NULL, ' 9831142710', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (373, 'PARTICULAR', 'EM334', NULL, 'COCINA ECONOMICA LOS 2 PORTALES', NULL, ' 9838372260', NULL, NULL, NULL, ' 9838372260', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (374, 'PARTICULAR', 'EM335', NULL, 'PANADERIA LA INVENCIBLE', NULL, ' 8320685', NULL, NULL, NULL, ' 8320685', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (375, 'PARTICULAR', 'EM336', NULL, 'COCINA ECONOMICA LAS BANDERITAS', NULL, ' 9838356523', NULL, NULL, NULL, ' 9838356523', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (376, 'PARTICULAR', 'EM337', NULL, 'PANADERIA LA COLONIA', NULL, ' 9831147181', NULL, NULL, NULL, ' 9831147181', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (377, 'PARTICULAR', 'EM338', NULL, 'BABU TE BAR', NULL, ' 9831292667', NULL, NULL, NULL, ' 9831292667', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (378, 'PARTICULAR', 'EM339', NULL, 'RESTAURANT BAR LA TRAVIESA', NULL, ' 9831133940', NULL, NULL, NULL, ' 9831133940', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (379, 'PARTICULAR', 'EM340', NULL, 'TORTILLERIA FIDEL', NULL, ' 9831002980', NULL, NULL, NULL, ' 9831002980', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (380, 'PARTICULAR', 'EM341', NULL, 'PLANTA PURIFICADORA DE LA PENINSULA DE YUCATAN A.C.', 'PURIFICADORA MANANTIAL DE VIDA ETERNA', ' 9838091326', NULL,
        'PLANTA PURIFICADORA DE LA PENINSULA DE YUCATAN A.C.', 'POBLADO MAYA BALAM, Q.ROO', ' 9838091326', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (381, 'PARTICULAR', 'EM342', NULL, 'TORTILLERIA EL MICHOACANO', NULL, ' 9831022054', NULL, NULL, NULL, ' 9831022054', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (382, 'PARTICULAR', 'EM344', NULL, 'IMSS 14 CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (383, 'PARTICULAR', 'EM345', NULL, 'COCINA ECONOMICA MI LINDO MICHOACAN', NULL, ' 9831255439', NULL, NULL, NULL, ' 9831255439', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (384, 'PARTICULAR', 'EM346', NULL, 'PURIFICADORA EL PALMAR', NULL, ' 9831120343', NULL, NULL, NULL, ' 9831120343', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (385, 'PARTICULAR', 'EM347', NULL, 'TORTILLERIA JAIR', NULL, ' 9831431578', NULL, NULL, NULL, ' 9831431578', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (386, 'PARTICULAR', 'EM348', NULL, 'TORTAS Y HAMBURGUESAS LAS MISMAS', NULL, ' 9838334635', NULL, NULL, NULL, ' 9838334635', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (387, 'PARTICULAR', 'EM349', NULL, 'C. FELIX SEDACY', NULL, ' 516289110', NULL, NULL, NULL, ' 516289110', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (388, 'PARTICULAR', 'EM350', NULL, 'FUMIGACIONES TULUM CARIBE', NULL, ' 9831250276', NULL, NULL, NULL, ' 9831250276', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (389, 'PARTICULAR', 'EM351', NULL, 'CENTRO ONCOLOGICO CHETUMAL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (390, 'PARTICULAR', 'EM353', NULL, 'MOLINO Y OTROS LOS CUATRO HERMANOS', NULL, ' 9831366811', NULL, NULL, NULL, ' 9831366811', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (391, 'PARTICULAR', 'EM354', NULL, 'C. BERENICE PEREZ LARIOS', 'ESTANCIA INFANTIL LA CASITA DE GUSSI', '9831207811', NULL, NULL,
        'AV. BELICE # 426, COL. SAHOP ENTRE MARCIANO GONZALEZ Y ANTONIO CORIA', '9831207811', 'bere_nice7501@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (392, 'PARTICULAR', 'EM355', NULL, 'ESTANCIA INFANTIL CARITAS', NULL, ' 8375670 o 9837005522', NULL, NULL, NULL, ' 8375670 o 9837005522', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (393, 'PARTICULAR', 'EM356', NULL, 'CAFETERIA THE ITALIAN COFFEE COMPANY', NULL, ' 9831198471', NULL, NULL, NULL, ' 9831198471', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (394, 'PARTICULAR', 'EM357', NULL, 'JUGUERIA SUPERFRUTY', NULL, ' 9831000402', NULL, NULL, NULL, ' 9831000402', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (395, 'INSTITUCIONAL', 'LECH', NULL, 'LABORATORIO ESTATAL DE CHIHUAHUA', NULL, NULL, NULL, NULL, 'CALLE JIMENEZ #4203, COLONIA CUARTELES', NULL, NULL, NULL, NULL,
        'Chihuahua', 'CHIHUAHUA', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (396, 'PARTICULAR', 'EM358', NULL, 'VILEX FUMIGACIONES S. DE R.L. DE C.V.', NULL, ' 983 8360714', NULL, NULL, NULL, ' 983 8360714', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (397, 'PARTICULAR', 'EM01', NULL, 'C. ALAN ISMAEL ROSADO CARBALLO', 'PURIFICADORA FONTE', '9831226637', NULL, 'C. ALAN ISMAEL ROSADO CARBALLO',
        'AVENIDA JAVIER ROJO GOMEZ ESQUNA TEJON # 729', '9831226637', 'alan_rosado@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (398, 'PARTICULAR', 'EM359', NULL, 'C. FRANCISCA AMERICA BRICEÑO CALDERON', 'TORTILLERIA CENTER´S', '9831090955', NULL, NULL,
        'RETORNO 50, MANZANA 73, LOTE 4, COLONIA PAYO OBISPO II', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (399, 'PARTICULAR', 'EM360', NULL, 'ESTANCIA MI PEQUEÑO MUNDO', NULL, ' 9831239692', NULL, NULL, NULL, ' 9831239692', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (400, 'PARTICULAR', 'EM361', NULL, 'ESTANCIA INFANTIL CARITAS FELICES', NULL, ' 9831023779', NULL, NULL, NULL, ' 9831023779', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (401, 'PARTICULAR', 'EM362', NULL, 'PURIFICADORA SAN JORGE', NULL, ' 983 1100423', NULL, NULL, NULL, ' 983 1100423', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (402, 'PARTICULAR', 'EM363', NULL, 'ESTANCIA INFANTIL SONRISAS Y SORPRESAS', NULL, ' 983 1309972', NULL, NULL, NULL, ' 983 1309972', NULL, NULL, NULL, NULL, NULL, NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (403, 'PARTICULAR', 'EM364', NULL, 'PURIFICADORA LA GOTA AZUL', NULL, ' 9831142476', NULL, NULL, NULL, ' 9831142476', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (404, 'PARTICULAR', 'EM365', NULL, 'FABRICA DE HIELO CUBIFRESH', NULL, ' 9831131473', NULL, NULL, NULL, ' 9831131473', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (405, 'PARTICULAR', 'EM366', NULL, 'TORTILLERIA EL MOLINITO', NULL, ' 98311582260', NULL, NULL, NULL, ' 98311582260', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (406, 'PARTICULAR', 'EM367', NULL, 'SERVICIO DE EXTINGUIDORES 3 ESTRELLAS', NULL, ' 8372332', NULL, NULL, NULL, ' 8372332', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (407, 'PARTICULAR', 'EM368', NULL, 'ESTANCIA INFANTIL CAMPESTRE', NULL, ' 9831270378', NULL, NULL, NULL, ' 9831270378', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (408, 'PARTICULAR', 'EM369', NULL, 'PURIFICADORA EL GEMELO', NULL, ' 9831246177', NULL, NULL, NULL, ' 9831246177', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (409, 'PARTICULAR', 'EM02', NULL, 'SANTA INES IROLA RAMIREZ', 'MARACAS JUICE BAR JUGUERIA Y SANDWICHES', '9831251163', NULL, 'SANTA INES IROLA RAMIREZ',
        'AV. 4 DE MARZO AND. 19-A #53 ENTRE TELA Y TABI', '9831251163', 'santy-80@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (410, 'PARTICULAR', 'EM370', NULL, 'ESTANCIA INFANTIL CEEST', NULL, ' 9831226244', NULL, NULL, NULL, ' 9831226244', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (411, 'PARTICULAR', 'EM371', NULL, 'TORTILLERIA LA TABASQUEÑA', NULL, ' 9831376321', NULL, NULL, NULL, ' 9831376321', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (412, 'PARTICULAR', '326544', NULL, 'C. GIOVANNA MARISOL GONGORA PUERTO', 'POLLOS ASADOS MIKE', '9838395069, 9831335509', NULL, 'C. GIOVANNA MARISOL GONGORA PUERTO',
        'CALZADA VERACRUZ #423 ENTRE C.N.C. Y MARCIANO GONZALES', '9838395069, 9831335509', 'patymena9@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (413, 'PARTICULAR', '411877', NULL, 'C. FLORA BAENA CHIRINOS', 'TORTILLERIA FLOWER', '9831074584', NULL, 'C. FLORA BAENA CHIRINOS',
        'CALLE LAGUNA DE BACALAR #439 ESQUINA SICILIA', '9831074584', 'floweribocina@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (414, 'PARTICULAR', '8486', NULL, 'C. MARIA OBDULIA COCOM', 'ELABORACIÓN DE EMBUTIDOS Y CARNES FRÍAS ARTEZANAL CERVANTES', '1440843', NULL, 'C. MARIA OBDULIA COCOM',
        'TOMAS AZNAR ENTRE COROZAL Y CONSTITUYENTES, LOTE 24, MANZANA 498', '1440843', 'victorfacc@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (415, 'PARTICULAR', '66141', NULL, 'C. PAULINO CANCHE CATZIN', 'GALOS PIZZA', '9831109014', NULL, NULL, 'CALLE CONSTITUYENTES DEL 74 # 282 ENTRE CELUL', '9831109014',
        'rcortes_2011@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (416, 'PARTICULAR', '4867S', NULL, 'C. PAULINO CANCHE CATZIN', 'ANTOJITOS GLORIA MARIA', '9832672052', NULL, 'C. PAULINO CANCHE CATZIN',
        'MANUEL AVILA CAMACHO #129 ENTRE PRIMERO DE MAYO Y 27 DE SEPTIEMBRE,MANZANA 500,', '9832672052', 'carlos_2011@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (417, 'PARTICULAR', 'GSDG1', NULL, 'C. ROBERTO GENE MARRUFO', 'RESTAURANTE ALABAMA MAMMA', '9993226413', NULL, 'C. ROBERTO GENE MARRUFO',
        'AVENIDA INSURGENTES KM 5.025 POR TORCASA Y TZUISACHE, LOCAL G-07 CENTRO COMERCIA', '9993226413', 'albama.chetumal@mcred.com.mx', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (418, 'PARTICULAR', '706', NULL, 'C. JUSTA KAUIL UN', 'COCINA ECONÓMICA LA PALAPA', '9838782172, 98316668682', NULL, 'C. JUSTA CAHUIL UN', 'POBLADO NICOLAS BRAVO, Q.ROO',
        '9838782172, 98316668682', 'justakauil15@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (419, 'PARTICULAR', 'EM372', NULL, 'ESTANCIA INFANTIL MUNDO DE JUGUETE', NULL, ' 9831305598', NULL, NULL, NULL, ' 9831305598', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (420, 'PARTICULAR', 'EM373', NULL, 'ESTANCIA INFANTIL LA CASITA DE POCOYO', NULL, ' 9831130951', NULL, NULL, NULL, ' 9831130951', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (421, 'PARTICULAR', 'EM374', NULL, 'ESTANCIA INFANTIL CARITA DE ANGEL', NULL, ' 9831364336', NULL, NULL, NULL, ' 9831364336', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (422, 'PARTICULAR', 'EM375', NULL, 'ESTANCIA INFANTIL ELIZABETH', NULL, ' 9831545390', NULL, NULL, NULL, ' 9831545390', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (423, 'PARTICULAR', 'EM376', NULL, 'ESTANCIA INFANTIL GABRIELA MISTRAL', NULL, ' 9831377789', NULL, NULL, NULL, ' 9831377789', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (424, 'PARTICULAR', 'EM377', NULL, 'PURIFICADORA XAMAN -HA', NULL, ' 9831028477', NULL, NULL, NULL, ' 9831028477', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (425, 'PARTICULAR', 'EM378', NULL, 'PURIFICADORA SAN MIGUEL', NULL, ' 9837332453', NULL, NULL, NULL, ' 9837332453', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (426, 'PARTICULAR', 'EM379', NULL, 'TORTILLERIA YAZMIN', NULL, ' 9831547523', NULL, NULL, NULL, ' 9831547523', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (427, 'PARTICULAR', 'EM381', NULL, 'TORTILLERIA EL CERRITO', NULL, ' 9838380900', NULL, NULL, NULL, ' 9838380900', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (428, 'PARTICULAR', 'EM382', NULL, 'PURIFICADORA LA FUENTE', NULL, ' 9831438510', NULL, NULL, NULL, ' 9831438510', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (429, 'PARTICULAR', '707', NULL, 'C. VIRGINIA NARVAEZ GARCIA', 'RESTAURANTE LA FELICIDAD', '9831263212, 9838367717', NULL, 'C. VIRGINIA NARVAEZ GARCIA',
        'AVENIDA OAXACA #13, COL. CENTRO, CALDERITAS, Q. ROO', '9831263212, 9838367717', 'roslinlurline@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (430, 'PARTICULAR', '5G14DFG', NULL, 'C. ROBERTO POOT AVILEZ', 'RESTAURANTE EL ACUARIO', '9838359697', NULL, 'C. ROBERTO POOT AVILEZ',
        'AVENIDA OAXACA #12, CALDERITAS, Q. ROO', '9838359697', 'ruslinruline@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (431, 'PARTICULAR', '54314', NULL, 'C. LILIANA ISABEL DIAZ RODRIGUEZ', 'TAQUERIA LA POPULAR', '9831302476', NULL, 'C. LILIANA ISABEL DIAZ RODRIGUEZ',
        'AVENIDA SICILIA #233 POR CARRANZA, COL. ITALIA', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (432, 'PARTICULAR', 'SE4VDS', NULL, 'ADMINISTRADORA MEXICANA DE HIPODROMO', 'SPORTS BOOK Y YAK', '9831403524', NULL, 'ADMINISTRADORA MEXICANA DE HIPODROMO',
        'AVENIDA INSURGENTES KM 5.025 LOCAL 5-11, COLONIA EMANCIPACION', '9831403524', 'sala.chetumal@codere.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (433, 'PARTICULAR', 'A66C5G', NULL, 'C. NARDO CHULIM GALAN', 'PANADERIA LAUREL', '9831232862', NULL, 'C. NARDO CHULIM GALAN',
        'FELIPE CARRILLO PUERTO #456 CON MARCIANO GONZALEZ', '9831232862', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (434, 'PARTICULAR', '629', NULL, 'C. PEDRO HERNANDEZ HERNANDEZ', 'TORTILLERIA LAS 3 CRUCES', '9837520343', NULL, 'C. PEDRO HERNANDEZ HERNANDEZ',
        'CALLE POLYUC ENTRE RAMONAL Y CELUL, LOTE 17, MANZANA 192, COL. SOLIDARIDAD', '9837520343', 'natylinda_ali@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (435, 'PARTICULAR', '68YGR', NULL, 'C. MARIA ELENA AVILA CONDE', 'TORTILLERIA DOÑA MARY', '9831010907', NULL, 'C. MARIA ELENA AVILA CONDE',
        'CALLE REFORMA AGRARIA, MANZANA 187, LOTE 2 ENTRE 24 DE NOVIEMBRE', '9831010907', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (436, 'PARTICULAR', '1', NULL, 'C. EDILBERTO VAZQUEZ VILLATORO', 'RESTAURANTE LA CHOZA DE VILLATORO', '9831031482', NULL, 'C. EDILBERTO VAZQUEZ VILLATORO',
        'CALLE PRIMO DE VERDAD #24, COLONIA PRIMERA LEGISLATURA', '9831031482', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (437, 'PARTICULAR', 'G654D6', NULL, 'C. ALVARO ALVAREZ AGUILAR', 'PURIFICADORA COSTA MAYA', '9831169802', NULL, 'C. ALVARO ALVAREZ AGUILAR',
        'CARRETERA MAHAHUAL-XCALAK KM 4.5', '9831169802', 'asdchantal@todito.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (438, 'PARTICULAR', '2', NULL, 'C. ALVARO ALVAREZ AGUILAR', 'PURIFICADORA COSTA MAYA', '9831169802', NULL, 'C. ALVARO ALVAREZ AGUILAR',
        'CARRETERA FEDERAL MAHAHUAL-XCALAK KM 4.5', '9831169802', 'asdchantal@todito.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (439, 'PARTICULAR', 'G77G6', NULL, 'C. JORGE MARIO ESQUIVEL AVILA', 'INSTITUTO LAMAT', '9831450564, 9831321289', NULL, 'C.JORGE MARIO ESQUIVEL AVILA',
        'AV. HEROES #343 ENTRE ISLA CANCUN Y BUGAMBILIAS', '9831450564, 9831321289', 'institutolamt_admon@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (440, 'PARTICULAR', '3', NULL, 'C. JORGE MARIO ESQUIVEL AVILA', 'INSTITUTO LAMAT', '9831450564', NULL, 'C. JORGE MARIO ESQUIVEL AVILA',
        'HEROES #343 ENTRE ISLA CANCUN Y BUGAMBILIAS', '9831450564', 'institutolamat_admon@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (441, 'PARTICULAR', '4', NULL, 'C. TERESA ZENDEJAS AMEZCUA', 'COLEGIO DE BACHILLERES I', '9831205469', NULL, 'C. TERESA ZENDEJAS AMEZCUA',
        'AVENIDA INSURGENTES S/N. ESQ. CORNELIO LIZARRAGA', '9831205469', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (442, 'PARTICULAR', '5', NULL, 'C. GENARO VAZQUEZ DOMINGUEZ', 'LA EMPANADA DORADA', '9831437444', NULL, 'C. GENARO VAZQUEZ DOMINGUEZ',
        'AVENIDA ROJO GOMEZ MANZANA191, LOTE 13, COLONIA PAYO OBISPO', '9831437444', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (443, 'PARTICULAR', '6', NULL, 'C. SERGIO ESTEBAN BAENA', 'TORTILLERIA FLOWER III', '9831355486', NULL, 'C. SERGIO ESTEBAN BAENA',
        'AV. CHETUMAL #559, LOTE 18, MANZANA 137 ENTRE SUBTENIENTE LOPEZ Y 3 GARANTÍAS', '9831355486', 'tortilleriaflower3@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (444, 'PARTICULAR', '7', NULL, 'C. MAYRA ALEJANDRA RIVERO DOMINGUEZ', 'RESTAURANT CYNTHIAS', '9831293456', NULL, 'C. MAYRA ALEJANDRA RIVERO DOMINGUEZ',
        'AV. CALZADA VERACRUZ #287, COL. LOPEZ MATEOS ENTRE LUCIO BLANCO Y JUAN SARABIA', '9831293456', 'riverosrestaurant@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (445, 'PARTICULAR', '8', NULL, 'C. SUEMI ALICIA GOMEZ MARTINEZ', 'PESCADOS Y MARISCOS EL PEZ FELIZ', '9831215329', NULL, 'C. SUEMI ALICIA GOMEZ MARTINEZ',
        'AVENIDA CHETUMAL S/N. , MANZANA 382, LOTE 13, COLONIA AMPLIACION PROTERRITORIO', '9831215329', 'tejoj19@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (446, 'PARTICULAR', 'EM383', NULL, 'BANCO DE SANGRE REGIONAL DE CANCUN', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (447, 'PARTICULAR', 'EM385', NULL, 'C. JUAN CARLOS AGUILAR CAAMAL', 'PURIFICADORA AQUA LIFE', ' 9838385245', NULL, NULL,
        'CALLE TLAXCALA NUM. 12, COLONIA VERACRUZ, CALDERITAS, Q.ROO', ' 9838385245', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (448, 'PARTICULAR', 'EM386', NULL, 'PURIFICADORA LA PEÑA DE HOREB', NULL, ' 9831194216', NULL, NULL, NULL, ' 9831194216', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (449, 'PARTICULAR', 'EM388', NULL, 'TORTILLERIA JALIED', NULL, ' 9831263676', NULL, NULL, NULL, ' 9831263676', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (450, 'PARTICULAR', 'EM389', NULL, 'TORTILLERIA EBEN EZER', NULL, ' 9831161568 / 9838361704', NULL, NULL, NULL, ' 9831161568 / 9838361704', NULL, NULL, NULL, NULL, NULL,
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (451, 'PARTICULAR', 'EM390', NULL, 'ESCUELA SECUNDARIA OTHON P BLANCO', NULL, ' 9831405132', NULL, NULL, NULL, ' 9831405132', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (452, 'PARTICULAR', 'EM391', NULL, 'ESTANCIA INFANTIL PEQUES CLUB', NULL, ' 9831131428', NULL, NULL, NULL, ' 9831131428', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (453, 'PARTICULAR', 'EM394', NULL, 'PANADERIA SIETE HERMANOS', NULL, ' 9831301076', NULL, NULL, NULL, ' 9831301076', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (454, 'PARTICULAR', 'EM396', NULL, 'ESTANCIA INFANTIL SHALOM', NULL, ' 9831073090', NULL, NULL, NULL, ' 9831073090', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (455, 'PARTICULAR', 'EM397', NULL, 'ESTANCIA INFANTIL LIBEMAR', NULL, ' 1181845', NULL, NULL, NULL, ' 1181845', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (456, 'PARTICULAR', 'EM398', NULL, 'DESPACHADORA DE AGUA SAL HA', NULL, ' 9831074117', NULL, NULL, NULL, ' 9831074117', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (457, 'PARTICULAR', 'EM399', NULL, 'TORTILLERIA YAREYMI', NULL, ' 9837335680', NULL, NULL, NULL, ' 9837335680', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (458, 'PARTICULAR', 'EM400', NULL, 'ESTANCIA INFANTIL JUGANDO A CRECER', NULL, ' 9831112686', NULL, NULL, NULL, ' 9831112686', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (459, 'PARTICULAR', 'EM401', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.', NULL, ' 9831777099', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.',
        'AVENIDA CONSTITUYENTES DEL 74 #254, COLONIA EL ENCANTO', ' 9831777099', 'jnorendoni8@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (460, 'PARTICULAR', 'EM402', NULL, 'RESTAURANT BOY´S PIZZA', NULL, ' 9838367755', NULL, NULL, NULL, ' 9838367755', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (461, 'PARTICULAR', 'EM405', NULL, 'ESTANCIA INFANTIL JUGACI', NULL, ' 9831050868', NULL, NULL, NULL, ' 9831050868', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (462, 'PARTICULAR', 'EM406', NULL, 'C. LUIS ANGELINO CANUL CANUL', 'PANADERIA LA ESPIGA DE ORO', ' 9831077886', NULL, 'C. LUIS ANGELINO CANUL CANUL',
        'CALLE CHABLE ENTRE SACXAN Y NIZUC, COL. SOLIDARIDAD', ' 9831077886', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (463, 'PARTICULAR', 'EM407', NULL, 'TORTILLERIA PLUTARCOS', NULL, ' 9831130245', NULL, NULL, NULL, ' 9831130245', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (464, 'PARTICULAR', 'EM408', NULL, 'VETERINARIA EL RANCHITO', NULL, ' 8327165 o´ 35896', NULL, NULL, NULL, ' 8327165 o´ 35896', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (465, 'PARTICULAR', '9', NULL, 'C. MANUEL ARSEMIO CANCHE CANCHE', 'PIZZERIA ANGELES', '9831250703', NULL, 'C,. MANUEL ARSEMIO CANCHE CANCHE',
        'AVENIDA ISLA CANCUN #467 ENTRE CORCEGA SERDEÑA, COLONIA 20 DE NOVIEMBRE', '9831250703', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (466, 'PARTICULAR', '10', NULL, 'C. GLADYS MAGALY FLORES QUIÑONES', 'SERVIPOLLOS MAGGY', '9831072680', NULL, 'C. GLADYS MAGALY FLORES QUIÑONES', 'CALZADA VERACRUZ #177',
        '9831072680', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (467, 'PARTICULAR', '11', NULL, 'C. CHEN PI YUN', 'BABU (VENTA DE BEBIDAS REFRESCANTES)', '9837325046', NULL, 'C. CHEN PI YUN',
        'AV. SAN SALVADOR #575-A ENTRE GABRIEL LEYVA Y ROJO GOMEZ', '9837325046', 'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (468, 'PARTICULAR', '12', NULL, 'C. CHEN PI YUN', 'BABU / POLLITO 8', '9837325046', NULL, 'C. CHEN PI YUN',
        'AV. BOULEVARD BAHIA #75 ENTRE COZUMEL Y JOSEFA ORTIZ, COL. BARRIO BRAVO', '9837325046', 'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (469, 'PARTICULAR', '13', NULL, 'C. PI YUN CHEN', 'BABU', '9831162915', NULL, NULL, 'AV. ALVARO OBREGON # 204-C', '9831162915', 'lilihsiland@gmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (470, 'PARTICULAR', '14', NULL, 'C. CHEN PI YUN', 'BABUTE BAR', '9831162915', NULL, NULL, 'AVENIDA CONSTITUYENTES DEL 74, COLONIA SOLIDARIDAD', '9831162915', NULL, NULL,
        NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (471, 'PARTICULAR', '15', NULL, 'C. WEN LIANG LAI', 'BABUTE BAR', '9831162915', NULL, NULL, 'AV. INSURGENTES, INTERIOR PLAZA LAS AMERICAS, LOCAL G-13, COL. INFONAVIT',
        '9831162915', 'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (472, 'PARTICULAR', '16', NULL, 'C. LAI I-TSEN', 'BABUTE BAR', '9837325046', NULL, NULL, 'AV. JOSE MARÍA MORELOS #370 CON ISLA CANCUN, COL. LEONA VICARIO', '9837325046',
        'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (473, 'PARTICULAR', '17', NULL, 'HSILAND BIO TECH S.A DE C.V.', 'HSILAND BIO TECH S.A DE C.V.', '9831182915', NULL, 'HSILAND BIO TECH S.A DE C.V.',
        'CALLE SINALOA CON VALLADOLID Y COAHUILA, POBLADO DE CALDERAS', '9831182915', 'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (474, 'PARTICULAR', '18', NULL, 'C. PI YUN CHEN', 'RESTAURANT POLLITO 8', '9831162915', NULL, NULL, 'AV. ALVARO OBREGON #204-C ENTRE JUAREZ Y HEROES COL. CENTRO',
        '9831162915', 'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (475, 'PARTICULAR', '19', NULL, 'C. EDGAR OROZCO PRIETO', 'RESTAURANT LA UVA', '9831045858, 9831102294', NULL, 'C. OROZCO PRIETO EDGAR',
        'AV. OAXACA, LOCAL No. 10, COL. CENTRO, CALDERITAS, Q.ROO', '9831045858, 9831102294', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (476, 'PARTICULAR', '20', NULL, 'C. JOSEFINA TRINIDAD RODRIGUEZ', 'TORTILLERIA LA POBLANITA II', '9831090955', NULL, 'C. JOSEFINA TRINIDAD RODRIGUEZ',
        'AVENIDA ERICK PAOLO MARTINEZ #888, COLONIA MAYA REAL', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (477, 'PARTICULAR', 'GXFJDIM', NULL, 'C. MILEDI RUBI BRICEÑO CALDERON', 'TORTILLERIA MILEDI RUBI', '9831090955', NULL, NULL,
        'AVENIDA IGNACIO COMONFORT #98, COLONIA 5 DE ABRIL', '9831090955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (478, 'PARTICULAR', '21', NULL, 'C. SAHIRELY YOHANA CONSTANTINO SANTOS', 'YAZID PIZZA', '9831587517', NULL, NULL, 'CALZADA VERACRUZ ESQUINA ISLA CANCUN', '9831587517', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (479, 'PARTICULAR', '22', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE', 'PANADERIA LA TARTALETA', '9831329103', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE',
        'AVENIDA HEROES ENTRE ALVARO OBREGON', '9831329103', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (480, 'PARTICULAR', '23', NULL, 'C. CELIA SOTRES HECTOR', 'LA TARTALETA', '9831547676', NULL, 'C. CELIA SOTRES HECTOR', 'CALLE SIERRA S/N. , MAHAHUAL, Q. ROO', '9831547676',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (481, 'PARTICULAR', '24', NULL, 'C. ZOILA CUSTODIO CRUZ', 'LONCHERIA ZOILA', '9831857393', NULL, 'C. ZOILA CUSTODIO CRUZ',
        'INTERIOR DEL MERCADO LÁZARO CÁRDENAS DEL RÍO, 3 ETAPA', '9831857393', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (482, 'PARTICULAR', '25', NULL, 'C. FELICIANO VALENCIA SALGADO', 'GV DE LA PENINSULA S.C. DE R.L. DE C.V. (EL MICHOACANO)', '9831180277', NULL,
        'C. FELICIANO VALENCIA SALGADO', 'AV. LAZARO CARDENAS #278 ESQUINA MORELOS, COL. CENTRO', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (483, 'PARTICULAR', '26', NULL, 'C. FELICIANO VALENCIA SALGADO', 'TORITLLERIA EL MICHOACANO', '9831180277', NULL, 'C. FELICIANO VALENCIA SALGADO',
        'AVENIDA CONFEDERACION NACIONAL CAMPESINA #135-B ENTRE AVENIDA SAN SALVADOR ALVAR', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (484, 'PARTICULAR', '27', NULL, 'C. FELICIANO VALENCIA SALGADO', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. FELICIANO VALENCIA SALGADO',
        'CALLE COAHUILA #94, COLONIA CENTRO ENTRE QUERETARO Y GUANAJUATO', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (485, 'PARTICULAR', '28', NULL, 'C. FELICIANO VALENCIA SALGADO', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. FELICIANO VALENCIA SALGADO',
        'AVENIDA LEONA VICARIO ENTRE ANDADOR GUADALUPE VICTORIA', '9831180277', 'facturagvchetumal@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (486, 'PARTICULAR', '29', NULL, 'C. FELICIANO VALENCIA SALGADO', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. FELICIANO VALENCIA SALGADO',
        'CALLE FLAMBOYAN S/N. ENTRE MACULUX Y HUAYACAN', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (487, 'PARTICULAR', '3', NULL, 'C. FELICIANO VALENCIA SALGADO', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. FELICIANO VALENCIA SALGADO',
        'AVENIDA AARON MERINO LOTE 1, MANZANA 35 ESQUINA CARMINCHEL', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (488, 'PARTICULAR', '31', NULL, 'C. EMILIA JANNETTE CONSTANTINO TEJERO', 'RESTAURANT RIVEROS', '9838344635', NULL, NULL,
        'AV. OAXACA #06 ENTRE AGUAS CALIENTES Y CAMPECHE, COL. CENTRO, CALDERITAS, Q.ROO', '9838344635', 'riverosrestaurant@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (489, 'PARTICULAR', '735', NULL, 'C. MARTÍN MORALES SANCHEZ', 'RESTAURANT PEJE LAGARTO', '9831255450', NULL, 'C. MARTIN MORALES SANCHEZ',
        'AVENIDA 19 S/N. POR CALLE 38 Y 40', '9831255450', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (490, 'PARTICULAR', '33', NULL, 'C. LAURA ILEANA MEDINA ARGUETA', 'CARNITAS Y CHICHARRONES ISIS', '8335559, 9831020844', NULL, 'C. LAURA ILEANA MEDINA ARGUETA',
        'AVENIDA JOSE MARÍA MORELOS # 402-A', '8335559, 9831020844', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (491, 'PARTICULAR', '34', NULL, 'C. RUBI DEL PILAR NUÑEZ SANTANA', 'CHICHARRONES Y CARNITAS HAKUNA MATATA´S', '9831003469', NULL, 'C. RUBI DEL PILAR NUÑEZ SANTANA',
        'RAMÓN DEL VALLE INCLAN # 275', '9831003469', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (492, 'PARTICULAR', '35', NULL, 'C. LAUREANA DE LOS ANGELES DZUL TEC', 'COCINA ECONOMICA EL RINCON DE LOS SABORES', '9831163731', NULL,
        'C. LAUREANA DE LOS ANGELES DZUL TEC', 'CALLE REFORMA, LOTE 15, MANZANA 287, COL. PROTERRITORIO', '9831163731', 'laurisdzul@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (493, 'PARTICULAR', '36', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA',
        'CALLE FRANCISCO J. MUJICA #672 ESQUINA CEIBA, COLONIA BOSQUE', '9831180277', 'dicomsureste@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (494, 'PARTICULAR', '37', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA',
        'CALLE LUCIO BLANCO #87 ENTRE HERIBERTO FRIAS, COLONIA ADOLFO LOPEZ MATEOS', '9831180277', 'dicomsureste@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (495, 'PARTICULAR', '38', NULL, 'C. ERICA ALEJANDRA GONZALEZ ORTEGA', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. ERICA ALEJANDRA GONZALEZ ORTEGA',
        'CALLE CIRCUITO 3 SUR #11, LOTE 1, MANZANA 3, AVENIDA PACTO OBRERO, COLONIA PACTO', '9831180277', 'dicomsureste@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (496, 'PARTICULAR', '39', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA',
        'CALLE MANUEL BORGE MARTIN #20 POR FRANCISCO VILLA Y VICENTE GUERRERO', '9831180277', 'dicomsureste@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (497, 'PARTICULAR', '40', NULL, 'C. ERICA ALEJANDRA GONZALEZ GUZMAN', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'C. ERICA ALEJANDRA GONZALEZ GUZMAN',
        'SAN SALVADOR #439 LOCAL 4 ENTRE SAN SALVADOR, COLONIA ASERRADERO', '9831180277', 'dicomsureste@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (498, 'PARTICULAR', '41', NULL, 'C. HAU ZOILA', 'LONCHERIA LISSET', '9831307963', NULL, 'C. HAU ZOILA', 'HERIBERTO FRIAS #240', '9831307963', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (499, 'PARTICULAR', '42', NULL, 'C. LIMBER ARTURO ESCALANTE CASTRO', 'RESTAURANT BAHIA CHETUMAL', '9831546524', NULL, 'C. LIMBER ARTURO ESCALANTE CASTRO',
        'AVENIDA UNVERSIDAD #532-A', '9831546524', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (500, 'PARTICULAR', '43', NULL, 'C. ALVARO ALFONSO MARRUFO CHIU', 'PURIFICADORA REHOBOT', '9838678041', NULL, 'C. ALVARO ALFONSO MARRUFO CHIU',
        'JOSE MARRUFO HERNANDEZ #984 ENTRE LAGUNA NEGRA , FRACC. LOS ALMENDROS', '9838678041', 'alvaroalfonsomc@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (501, 'PARTICULAR', '44', NULL, 'C. BERENICE PEREZ LARIOS', 'ESTANCIA INFANTIL CASITA DE GUSSI', '9831207811', NULL, 'C. BERENICE PEREZ LOPEZ',
        'AVENIDA BELICE #426 ENTRE MARCIANO GONZALEZ Y ANTONIO CORIA, COLONIA SAHOP', '9831207811', 'BERE_NICE7501@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (502, 'PARTICULAR', '45', NULL, 'C. YULIO MICHEL SALAZAR CAMACHO', 'RESTAURANT EL RINCON CUBANO', '9831174069', NULL, 'C. YULIO MICHEL SALAZAR CAMACHO',
        'CALZADA VERACRUZ #368 ENTRE ISLA CANCUN Y LUIS CABRERA', '9831174069', 'yuliom77@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (503, 'PARTICULAR', '46', NULL, 'C. LUIS HUMBERTO BEDRIÑANA BARBOZA', 'VILEX FUMIGACIONES S. DE R.L. DE C.V.', '9838360714', NULL, 'C. LUIS HUMBERTO BEDRIÑANA BARBOZA',
        'CALLE SICILIA #224-A, FRACCIONAMIENTO FLAMBOYANES', '9838360714', 'fumigasacancun@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (504, 'PARTICULAR', '47', NULL, 'C. IRLA BRAMBILA PRIETO', 'RESTAURANT PADILLA', '9831026978', NULL, 'C. IRLA BRAMBILA PRIETO',
        'CARRETERA FEDERAL CHETUMAL-HUAY-PIX S/N. COLONIA INDUSTRIAL', '9831026978', 'irlabp@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (505, 'PARTICULAR', '48', NULL, 'C. BLANCA SUGEY PAYAN VAZQUEZ', 'PALETERIA LA MICHOACANA', '9991987255', NULL, 'C. BLANCA SUGEY DAYAN VAZQUEZ',
        'HEROES #31 ENTRE OTHON P. BLANCO Y CARMEN OCHOA DE MERINO', '9991987255', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (506, 'PARTICULAR', '11', NULL, 'C. CAAMAL SALDIVAR ELIZABETH', 'ABARROTES LILI', '9831304388', NULL, 'C. CAAMAL SALDIVAR ELIZABETH', 'AVENIDA 9 ENTRE CALLES 28 Y 30',
        '9831304388', 'quiroz-4388@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (507, 'PARTICULAR', '12', NULL, 'C, HERNANDEZ QUIROZ ROSALBA', 'ANTOJITOS GILMER', '9831076568', NULL, 'C, HERNANDEZ QUIROZ ROSALBA',
        'AVENIDA LIBRAMIENTO, MANZANA 6, LOTE 1 ENTRE CALLES 24 Y 26', '9831076568', 'quiroz-4388@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (508, 'PARTICULAR', '16', NULL, 'C. NELSY LETICIA VELAZQUEZ MIAN', 'MISS CHELAS', '9831572693', NULL, 'C. NELSY LETICIA VELAZQUEZ MIAN',
        'ZARAGOZA #166 ENTRE MIQUEL HIDALGO Y 16 DE SEPTIEMBRE', '9831572693', 'art-cp78@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (509, 'PARTICULAR', '106', NULL, 'C. VERONICA DE JESUS PECH PECH', 'EXTERMAN FUMIGACIONES', '9831190176', NULL, NULL,
        'LAZARO CARDENAS #21 ENTRE ISLA CONTOY, COL. BARRIO BRAVO', '9831190176', 'exterman1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (510, 'PARTICULAR', '123', NULL, 'C. JUAN RAMON ZUÑIGA ALVARADO', 'COCTELERIA MICHE MAR', '9831175186', NULL, 'C. JUAN RAMON ZUÑIGA ALVARADO', 'MARIANO ESCOBEDO #399',
        '9831175186', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (511, 'PARTICULAR', '0.01455', NULL, 'C. WILBERT GAMBOA CORAL', 'ANTOJITOS BIBY', '9831121014', NULL, 'C. WILBERT GAMBOA CORAL',
        'CALLE ZARAGOZA #68 ENTRE COZUMEL Y JOSEFA ORTIZ', '9831121014', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (512, 'PARTICULAR', '289', NULL, 'C. JOSEPH TANNOUS ABI GEBRAEL', 'RESTAURANTE BAR BANDIDOS STEAKHOUSE', '9831163870', NULL, 'C. JOSEPH TANNOUS ABI GEBRAEL',
        'AVENIDA 4 DE MARZO S/N., LOCAL 18 COLONIA INDUSTRIAL', '9831163870', 'carloscsoto@ccysdespacho.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (513, 'PARTICULAR', '1269', NULL, 'C. FADI AZIZ NEHME', 'RESTAURANTE MR. KILO', '9831162923', NULL, 'C. FADI AZIZ MEHME', 'AVENIDA JAVIER ROJO GOMEZ S/N. LOCAL 09',
        '9831162923', 'carlos-csoto@ccysdespacho.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (514, 'PARTICULAR', '999', NULL, 'C. ALEJANDRO AGUILAR LAGUARDIA', 'RESTAURANTE LA BOTANA DE PELICANOS', '9831163870', NULL, 'C. ALEJANDRO AGUILAR LAGUARDIA',
        'AV. BOULEVARD BAHIA # 70, COL. BARRIO BRAVO', '9831163870', 'carlos.csoto@ccysdespacho.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (515, 'PARTICULAR', 'Y4S6YV', NULL, 'C. BULMARO ESPINOZA HERNANDEZ', 'POLLERIA ABI', '9831090955', NULL, 'C. BULMARO ESPINOZA HERNANDEZ',
        'CALLE PRIMERO DE MAYO, LOTE 11, MANZANA 241, COL. PROTERRITORIO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (516, 'PARTICULAR', 'SHSH5', NULL, 'C. EDWIN DANIEL MORALES MOO', 'TORTILLERIA DANIEL', '9831090955, 9831831070', NULL, 'C. EDWIN DANIEL MORALES MOO',
        'CAYENA #482, COLONIA LAS AMERICAS III', '9831090955, 9831831070', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (517, 'PARTICULAR', '1245', NULL, 'C. CESAR ASTERIO TUN CHE', 'TAQUERIA LAS BRASAS', '9831090955', NULL, 'C. CESAR ASTERIO TUN CHE',
        'CALLE ISAAC MEDINA, MANZANA 60, LOTE 04, COL. GUADALUPE VICTORIA', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (518, 'PARTICULAR', '364', NULL, 'C. MARIA PETRONA CHAN CANUL', 'TORTILLERIA VAZQUEZ', '9831058650', NULL, 'C. MARIA PETRONA CHAN CANUL',
        'AVENIDA CONSTITUYENTES #872, COL. CARIBE', '9831058650', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (519, 'PARTICULAR', '385', NULL, 'C. MARTHA INES HOY AYALA', 'LONCHERIA ANGELITO', '9831847131', NULL, 'C. MARTHA INES HOY AYALA',
        'CALLE GUADALUPE VICTORIA S/N., INGENIO SAN RAFAEL DE PUCTE, Q.ROO', '9831847131', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (520, 'PARTICULAR', '366', NULL, 'C. ILMER HERRERA CANTO', 'PURIFICADORA VITALY WATER', '9831311807', NULL, 'C. ILMER HERRERA CANTO',
        'CALLE EFRAIN AGUILAR #264-C, COLONIA CENTRO', '9831311807', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (521, 'PARTICULAR', '367', NULL, 'C. ILMER HERRERA CANTO', 'PURIFICADORA VITALY WATER', '9831311807', NULL, 'C. ILMER HERRERA CANTO',
        'PRIMO DE VERDAD #66, COLONIA PRIMERA LEGISLATURA', '9831311807', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (522, 'PARTICULAR', '368', NULL, 'C. ILMER HERRERA CANTO', 'PURIFICADORA VITALY WATER', '9831311807', NULL, 'C. ILMER HERRERA CANTO',
        'AVENIDA BELICE #420 CON ANTONIO CORIA, COLONIA SAHOP', '9831311807', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (523, 'PARTICULAR', '369', NULL, 'C. DELMY NOEMI CEME CUPUL', 'TORTILLERIA QUINTANA ROO', '9838338850', NULL, 'C. DELMY NOEMI CEME CUPUL',
        'AVENIDA CHETUMAL #605, MANZANA 174, LOTE 16 PLANTA BAJA , COLONIA SOLIDARIDAD', '9838338850', 'delmny1102@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (524, 'INSTITUCIONAL', '538', NULL, 'COMISIONADO DE OPERACION SANITARIA', NULL, NULL, NULL, 'C. ALVARO ISRAEL PEREZ VEGA', 'OKLAHOMA #14, COLONIA NAPOLES, CUIDAD DE MEXICO',
        NULL, NULL, NULL, NULL, 'México', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (525, 'PARTICULAR', '1125', NULL, 'C. DARIS DEL CARMEN GALERA HERNANDEZ', 'LONCHERIA DARIS', '9831584907', NULL, 'C. DARIS DEL CARMEN GALERA HERNANDEZ',
        'FRANCISCO J MUJICA, MERCADITO 5 DE ABRIL, LOCAL 3,4,5', '9831584907', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (526, 'PARTICULAR', '145', NULL, 'C. CLAUDIA ANTONIA MAGIL DIAZ', 'MARISQUERIA EL COMPICH', '9831214758', NULL, 'C. CLAUDIA ANTONIO MAGIL DIAZ',
        'AV. MAXUXAC S/N., MANZANA 557, LOTE 3, COLONIA NUEVO PROGRESO', '9831214758', 'chapiz7502@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (527, 'PARTICULAR', '657', NULL, 'SIGMA ALIMENTOS CENTRO S.A. DE C.V.', NULL, NULL, NULL, NULL, 'AVENIDA MORELOS S/N., COLONIA CENTRO', NULL, 'cbezares@sigma-alimentos.com',
        NULL, NULL, 'Quintana Roo', 'JOSE MARIA MORELOS', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (528, 'PARTICULAR', '376', NULL, 'C. FLORES VALDEZ TIRSO', 'ANTOJITOS FLORES', '1440301', NULL, 'C. FLORES VALDEZ TIRZO', 'CALLE 38 S/N. ENTRE 13 Y 19', '1440301',
        'ivergaraislas@htomail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (529, 'PARTICULAR', '370', NULL, 'C. ROSALIA AMANDA TORRES LEON', 'ANTOJITOS YUCATECOS LA ROSITA', '9831396251', NULL, 'C. ROSALIA AMANDA TORRES LEON',
        'CALZADA VERACRUZ, MERCADO LAZARO CARDENAS, LOCAL 39', '9831396251', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (530, 'PARTICULAR', '390', NULL, 'C. GUADALUPE SALOME OLIVERA CASTILLO', 'CLEAN HOUSE FUMIGACIONES', '9831161209', NULL, 'C. GUADALUPE SALOME OLIVERA CASTILLO',
        'CALLE 3 GARANTIAS ENTRE POLYUC Y PETCACAB', '9831161209', 'fumigacionescleanhouse@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (531, 'INSTITUCIONAL', '361', NULL, 'DIRECTOR DEL HOSPITAL GENERAL', 'DR. ARTURO ARCOS APONTE', '9838321977', NULL, 'DR. ARTURO ARCOS APONTE',
        'AV. ANDRES QUINTANA ROO ENTRE JUAN JOSE SIORDIA E ISLA CANCUN', '9838321977', 'jrabuxapqui@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (532, 'PARTICULAR', '352', NULL, 'C. LEONIDES BATUN BAEZA', 'ANTOJITOS Y COCINA ECONOMICA LEO', '9831762872', NULL, 'C. LEONIDES BATUN BAEZA',
        'AVENIDA MAGISTERIAL ENTRE CHABLE Y CALLE REFORMA, MANZANA 70, LOTE 11', '9831762872', 'leobatuna123@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (533, 'PARTICULAR', '353', NULL, 'C. ROXANA EMILIA BAÑOS RAMIREZ', 'POLLO ASADOS DOÑA ROSY', '9831012471', NULL, 'C. ROXANA EMILIA BAÑOS RAMIREZ', 'BUGAMBILIAS #641',
        '9831012471', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (534, 'PARTICULAR', '388', NULL, 'C. PABLO HERNANDEZ EUGENIO', 'UNIDAD DE HEMODIALISIS DE LA CLÍNICA CARRANZA', '9831264150', NULL, 'C. PABLO HERNANDEZ EUGENIO',
        'CALLE VENUSTIANO CARRANZA #366, COLONIA ITALIA', '9831264150', 'peketraviesanive@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (535, 'PARTICULAR', '389', NULL, 'C. FELICIANA GONZALEZ MARTINEZ', 'LONCHERIA MARTHA', '9831403519', NULL, 'C. FELICIANA GONZALEZ MARTINEZ',
        'MERCADO LAZARO CARDENAS, LOCAL 402 Y 403 ENTRE CALZADA VERACRUZ Y CNC', '9831403519', 'gonmarfel@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (536, 'PARTICULAR', '386', NULL, 'C. ABIGAIL ALONZO BARRADAS', 'RESTAURANTE EL ARBOLITO', '931046151, 9831375937', NULL, 'C. ABIGAIL ALONZO BARRADAS',
        'AV. MAXUXAC #128 ENTRE JACINTO PAT Y 5 DE FEBRERO, MANZANA 242, LOTE 6', '931046151, 9831375937', 'abialonzo@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (537, 'PARTICULAR', '387', NULL, 'C. KATIA ISELA BERMEJO MORTEO', 'ANTOJITOS LA GUADALUPANA', '9831051887', NULL, 'C. KATIA ISELA BERMEJO MORTEO',
        'CARRETERA FEDERAL, POBLADO DE UCUM, Q. ROO', '9831051887', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (538, 'PARTICULAR', '539', NULL, 'C. MELVA ARMINDA ARANA ESQUIVEL', 'SUPER PASTELERIA SILYMEL', '8329668, 983166016, 8331316', NULL, 'C. MELVA ARMINDA ARANA ESQUIVEL',
        'AVENIDA FRANCISCO I. MADERO #205, COLONIA CENTRO', '8329668, 983166016, 8331316', 'facturasilymel@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (539, 'PARTICULAR', '540', NULL, 'C. LUIS ENRIQUE BASTO PAT', 'C. BAPAL ABARRATERA S. DE R.L. DE C.V.', '8330825', NULL, 'C. LUIS ENRIQUE BASTO PAT',
        'CALLE VICENTE GUERRERO S/N. , MANZANA 19, LOTE 20, LOCALIDAD JAVIER ROJO GOMEZ', '8330825', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (540, 'PARTICULAR', '541', NULL, 'C. ESPERANZA LOPEZ CORONA', 'RESTAURANTE LANCHUA PASTA, PASTA Y ALGO MAS', '28,561,389,831,240,000', NULL, 'C. ESPERANZA LOPEZ CORONA',
        'AV. ALVARON OBREGON #332, COL. CENTRO', '28,561,389,831,240,000', 'lanchuapastapastayalgomas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (541, 'PARTICULAR', '542', NULL, 'C. IRMA ARACELY OLMEDO SANCHEZ', 'CASA OLMEDO', '9831659999', NULL, 'C. IRMA ARACELY OLMEDO SANCHEZ',
        'AV. ERICK PAOLO MARTINEZ, MANZANA 67, LOTE 10, COL. PAYO OBISPO 2', '9831659999', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (542, 'PARTICULAR', '543', NULL, 'C. LIMBERTH CASTRO AGUILAR', 'COCINA ECONOMICA POLLO DORADO', '98376060', NULL, 'C. LIMBERTH CASTRO AGUILAR', 'BELIZE #269 A', '98376060',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (543, 'PARTICULAR', '544', NULL, 'C. LIMBERTH CASTRO AGUILAR', 'COCINA ECONOMICA POLLO DORADO', '8376060', NULL, 'C. LIMBERTH CASTRO AGUILAR', 'BUGAMBILIAS #515', '8376060',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (544, 'PARTICULAR', '545', NULL, 'C. SIMON DIAZ REYES', 'LONCHERIA COLON', '1292048', NULL, 'C. SIMON DIAZ REYES', 'HIDALGO #21 ESQUINA CARMEN OCHOA DE MERINO', '1292048',
        'adiazt@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (545, 'PARTICULAR', '546', NULL, 'C. MARIA ANALIA KU PUC', 'KALIKUS PIZZERIA', '9831833658', NULL, 'C. MARIA ANALIA KU PUC', 'ORQUIDEAS #107, COLONIA JARDINES',
        '9831833658', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (546, 'PARTICULAR', '547', NULL, 'C. ALEJANDRO HERNANDEZ MARIN', 'RESTAURANTE BROASTER CHICKEN', '9838360359', NULL, 'C. ALEJANDRO HERNANDEZ MARIN',
        'AVENIDA ANDRES QUINTANA ROO ENTRE ISLA CANCUN # 386', '9838360359', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (547, 'PARTICULAR', '548', NULL, 'C. FRANCISCO ARPAIS LANDEROS', 'TORTILLERIA MAYA PAKAL', '9831142710', NULL, 'C. FRANCISCO ARPAIS LANDERO',
        'NICOLAS BRAVO #720, MANZANA 756, LOTE 04', '9831142710', 'andreshitoo14@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (548, 'PARTICULAR', '549', NULL, 'C. FRANCISCO ARPAIS LANDEROS', 'TORTILLERIA NORMITA II', '9831142710', NULL, 'C. FRANCISCO ARPAIS LANDERO',
        'CALLE CELUL # 636, MANZANA 201, LOTE 5, ESQ. ALFREDO V. BONFIL', '9831142710', 'andreshitoo@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (549, 'PARTICULAR', '550', NULL, 'C. FRANCISCO ARPAIS LANDEROS', 'TORTILLERIA NORMITA I', '9831142710', NULL, 'C. FRANCISCO ARPAIS LANDERO',
        'AV. BELICE # 295-A ESQUINA JUSTO SIERRA', '9831142710', 'andreshitoo14@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (550, 'PARTICULAR', '551', NULL, 'C. JAVIER ESPARZA ROBLES', 'PURIFICADORA OLIVER', '9831068877', NULL, 'C. JAVIER ESPARZA ROBLES',
        'CARRETERA A ALVARO OBREGON AL INGENIO, Q.ROO, POR POZO #15', '9831068877', 'rio-hondoclo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (551, 'PARTICULAR', '552', NULL, 'C. JULIA HOIL NOH', 'TAQUERIA CHAPIS', '9831572104, 9838367717', NULL, 'C. JULIA HOIL NOH',
        'AV. CALZADA DEL CENTENARIO, MANZANA 49, LOTE 14, COL. PACTO OBRERO', '9831572104, 9838367717', 'taqueriachapis2017@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (552, 'PARTICULAR', '553', NULL, 'C. DANIEL HERNANDEZ ALONSO', 'MOLINO Y TORTILLERIA EBEN EZER', '9831021298', NULL, 'C. DANIEL HERNANDEZ ALONSO',
        'CONSTITUYENTES DEL 74 ESQUINA MARGARITA MASA, COLONIA PROTERRITORIO', '9831021298', 'areli-1094@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (553, 'PARTICULAR', '580', NULL, 'C. VICTORIANO LORIA PECH', 'PURIFICADORA', '9831659118', NULL, 'C. VICTORIANO LORIA PECH', 'CARRETERA CAFETAL MAHAHUAL KM. 55',
        '9831659118', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (554, 'PARTICULAR', '581', NULL, 'C. ROMULA DEL CARMEN VAZQUEZ VILLANUEVA', 'ANTOJITOS DOÑA CARMEN', '8334147', NULL, 'C. ROMULA DEL CARMEN VAZQUEZ VILLANUEVA',
        'AV. IGNACIO ZARAGOZA #65 ENTRE CALLE COZUMEL Y JOSEFA ORTIZ', '8334147', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (555, 'PARTICULAR', '582', NULL, 'C. MARTHA BEATRIZ ANGULO CHI', 'MULTISERVICIOS K´OXOL', '9831119348, 9831101591', NULL, 'C. MARTHA BEATRIZ ANGULO CHI',
        'CERRADA DE GARZA, MANZANA 2, LOTE 4 , COLONIA TUMBENCUXTAL', '9831119348, 9831101591', 'arbe_fc@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (556, 'PARTICULAR', '590', NULL, 'C. BISMARCK OCAÑA LANDERO', 'TORTILLERIA CIELITO', '9831196635', NULL, 'C. BISMARCK OCAÑA LANDERO',
        'CALZADA VERACRUZ CON IGNACIO RAMIREZ #571', '9831196635', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (557, 'PARTICULAR', '591', NULL, 'C. MARITN MORALES SANCHEZ', 'PEJELAGARTO DE BACALAR', '9831255450', NULL, 'C. MARITN MORALES SANCHEZ', 'AVENIDA 19 POR CALLE 38',
        '9831255450', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (558, 'PARTICULAR', '592', NULL, 'C. REFUGIO HERNANDEZ CABRERA', 'TORTILLERIA SALMO 91', '9837330235', NULL, 'C. REFUGIO HERNANDEZ CABRERA',
        'AV. CHETUMAL ENTRE TEPICH Y TIHOSUCO, MANZANA 206, LOTE 19, COL. SOLIDARIDAD', '9837330235', 'yireh94@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (559, 'PARTICULAR', '593', NULL, 'C. REFUGIO HERNANDEZ CABRERA', 'TORITLLERIA Y MOLINO YIREH', '9837330235', NULL, 'C. REFUGIO HERNANDEZ CABRERA',
        'AV, JAVIER ROJO GOMEZ , MANZANA 50, LOTE 2 ENTRE CHABLE Y RETORNO 32, COL. PAYO', '9837330235', 'yireh94@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (560, 'PARTICULAR', '601', NULL, 'C. LETICIA EFIGENIA MARTINEZ LOPEZ', 'POLLOS ASADOS SERGIO 2', '9831442545', NULL, 'C. LETICIA EFIGENIA MARTINEZ LOPEZ',
        'GRACIANO SANCHEZ, MANZANA 467, LOTE 15, COLONIA PROTERRITORIO', '9831442545', 'polleria.ser@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (561, 'PARTICULAR', '602', NULL, 'C. JUANITA QUINTANILLA KANTUN', 'POLLOS ASADOS SERGIO', '9838334009, 9831453873', NULL, 'C. JUANITA QUINTANILLA KANTUN',
        'AV. CALZADA VERACRUZ #629 ESQUINA CARACOLILLO, COL. DEL BOSQUE', '9838334009, 9831453873', 'polleria.ser@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (562, 'PARTICULAR', '603', NULL, 'C. CLAUDIA GOMEZ DIAZ', 'POLLO ASADOS', '9831391136', NULL, 'C. CLAUDIA GOMEZ DIAZ',
        'CARRETERA LA UNION S/N. POBLADO CARLOS A. MADRAZO, Q.ROO', '9831391136', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (563, 'PARTICULAR', '604', NULL, 'INGENIO SAN RAFAEL DE PUCTE S.A. DE C.V.', 'PURIFICADORA DE AGUA SAN RAFAEL', '9831144855', NULL, 'INGENIO SAN RAFAEL DE PUCTE',
        'EJIDO PUCTE-ALVARO OBREGON, POBLADO JAVIER ROJO GOMEZ, Q.ROO', '9831144855', 'adolfos@bsm.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (564, 'PARTICULAR', '605', NULL, 'C. LAURA VERONICA GRANADOS SOBERANIS', 'RESTAURANTE NUIT BISTRO BAR', '9837522488', NULL, 'C. LAURA VERONICA GRANADOS SOBERANIS',
        'CARMEN OCHOA DE MERINO #191, LOCAL 2 ENTRE AV. HÉROES Y 5 DE MAYO', '9837522488', 'lauver92@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (565, 'PARTICULAR', '606', NULL, 'C. DANIEL RIVEROLL COTO', 'TORTILLERIA CHELITO II', '9831100316', NULL, 'C. DANIEL RIVEROLL COTO',
        'CALLE BICENTENARIO ESQUINA PEDRO MARIA ANAYA, COL. BICENTENARIO', '9831100316', 'uss-coto@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (566, 'PARTICULAR', '608', NULL, 'C. MARTINIANO XIX LEON', 'LONCHERIA TERESITA', '9831095749, 9837333581', NULL, 'C. MARTINIANO XIX LEON', 'CALZADA VERACRUZ #627',
        '9831095749, 9837333581', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (567, 'PARTICULAR', '609', NULL, 'C. ANASTACIA CUXIM PECH', 'LONCHERIA EL BUEN SABOR DEL CARIBE', '9831336146', NULL, 'C.ANASTACIA CUXIM PECH',
        'MERCADO LAZARO CARDENAS LOCAL 415-416-417', '9831336146', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (568, 'PARTICULAR', '611', NULL, 'C. DEYNER TOMAS CORDOVA ESTRELLA', 'LA TERRAZA DEL CAMARON', '9831620824', NULL, 'C. DEYNER TOMAS CORDOVA ESTRELLA',
        'AV. JOSE MARIA MORELOS #402-A, COL. LEONA VICARIO ESQUINA JUAN JOSE SIORDIA', '9831620824', 'deyner.cordoba@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (569, 'PARTICULAR', '610', NULL, 'C. VIRGINIA DOMINGUEZ MONTALVO', 'TORTAS EL AMIGO', '9831086142', NULL, 'C. VIRGINIA DOMINGUEZ MONTALVO',
        'CARRANZA #212 ENTRE BELIZE Y JUAREZ', '9831086142', 'vickydomont@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (570, 'PARTICULAR', '612', NULL, 'C. MARIA DEL CARMEN MORATO CASTRO', 'PURIFICADORA SAN ANTONIO', '9882672121', NULL, 'C. MARIA DEL CARMEN MORATO CASTRO',
        'AVENIDA ERICK PAOLO MARTINEZ , LOTE 8 Y 9 , COLONIA SOLIDARIDAD', '9882672121', 'sanantoniobebidas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (571, 'PARTICULAR', '615', NULL, 'C. MARIA DEL CARMEN MORATO CASTRO', 'PURIFICADORA SAN ANTONIO', '9882672121', NULL, 'C. MARIA DEL CARMEN MORATO CASTRO',
        'AV. ERICK PAOLO MARTINEZ , LOTE 8 Y 9, MANZANA 131, COL. SOLIDARIDAD', '9882672121', 'sanantoniobebidas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (572, 'PARTICULAR', '614', NULL, 'C. INDRA MAYANIN ALONZO HERNANDEZ', 'AH_ZUCAR (RESPOSTERIA Y CAFE)', '9831240392', NULL, 'C. INDRA MAYANIN ALONZO HERNANDEZ',
        'AVENIDA HEROES #413, COLONIA ADOLFO LOPEZ MATEOS', '9831240392', 'inmayalo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (573, 'PARTICULAR', '618', NULL, 'C. EMIR SALVADOR CONTRERAS BAZAN', 'RESTAURANTE LA JAIBA BORRACHA', '9831621185', NULL, 'C. EMIR SALVADOR CONTRERAS BAZAN',
        'AV. LAGUNA MILAGROS Y ADOLFO LOPEZ MATEOS, HUAY-PIX, Q.ROO', '9831621185', 'emirbazan@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (574, 'PARTICULAR', '619', NULL, 'C. MARTIN ANDRES MEDINA JIMENEZ', 'CARNITAS MEDINA', '9831311174', NULL, 'C. MEDINA JIMENEZ ANDRES MARTIN',
        'AVENIDA 5 DE MAYO # 96, COL. PLUTARCO ELIAS CALLES', '9831311174', 'dacoh94@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (575, 'PARTICULAR', '650', NULL, 'C. ESMERALDA PAREDES MAY', 'TORTILLERIA Y ABARROTES DURANGO', '9831050977', NULL, 'C. ESMERALDA PAREDES MAY',
        'LUIS MANUEL SEVILLA SANCHEZ #1105, FRACCIONAMIENTO LAS AMERICAS 1', '9831050977', 'moniriv204@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (576, 'PARTICULAR', '651', NULL, 'C. CHRISTIAN ADAN ZAPATA YAMA', 'ANTOJITOS CHAPIS', '9831399715', NULL, 'C. CHRISTIAN ADAN ZAPATA YAMA',
        'AV. CHETUMAL # 953 ENTRE BERMUDAS E ISLAS VIRGENES, FRACC. CARIBE', '9831399715', 'chrisadan2015@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (577, 'PARTICULAR', '652', NULL, 'C. INRY RODOLFO ZAMUDIO GUTIERREZ', 'TORTILLERIA GETSEMANI', '9838393249', NULL, 'C. INRY RODOLFO ZAMUDIO GUTIERREZ',
        'COL. SOLIDARIDAD, CALLE PETCACAB ENTRE RAUDALES Y DOS AGUADAS', '9838393249', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (578, 'PARTICULAR', '653', NULL, 'C. HILDA BRICEIDA MARTIN ESCAMILLA', 'CEVICHES Y COCTELES EL CAYUCO', '9831173907', NULL, 'C. HILDA BRICEIDA MARTIN ESCAMILLA',
        'CALLE BUGAMBILIAS #742, COLONIA JARDINES', '9831173907', 'briceida_martin@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (579, 'PARTICULAR', '652', NULL, 'C. YHIARA DE LOS ANGELES ARIAS JIMENEZ', 'TORTILLERIA CHARY', '9831090955', NULL, 'C. YHIARA DE LOS ANGELES ARIAS JIMENEZ',
        'ISLA CANCUN #301, COL. DAVID. G. GUTIERREZ', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (580, 'PARTICULAR', '653', NULL, 'C. RAMIRO XOOL COCOM', 'RESTAURANTE EL RINCON DE LAS TORTUGAS', '8320155', NULL, 'C. RAMIRO XOOL COCOM',
        'AVENIDA YUCATAN # 8, COL. YUCATAN, CALDERITAS, Q.ROO', '8320155', 'yasbeth678@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (581, 'PARTICULAR', '654', NULL, 'C. ROSSANA MARIA GONZALEZ LARA', 'RESTAURANT LALAPA LAPA', '9997463360', NULL, 'C. ROSSANA MARIA GONZALEZ LARA',
        'AVENIDA ERICK PAOLO MARTINEZ #499', '9997463360', 'rossana.gonzalez@grupo cielo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (582, 'PARTICULAR', '655', NULL, 'C.SILVIA LETICIA JUAREZ DIAZ', 'PURIFICADORA MIS TRES GOTITAS', '9831866562', NULL, 'C.SILVIA LETICIA JUAREZ DIAZ',
        'CALLE CUBA #365, FRACCIONAMIENTO CARIBE', '9831866562', 'silvia651109@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (583, 'PARTICULAR', '656', NULL, 'ABARROTERA EL DUERO S.A. DE C.V.', 'BODEGA LA FAVORITA', '9831199177', NULL, 'ABARROTERA EL DUERO S.A. DE C.V.',
        'CALLE SACXAN, MANZANA 181, LOTE 16 ENTRE 2 AGUADAS Y RAMONAL, COL. SOLIDARIDAD', '9831199177', 'kopool@merza.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (584, 'PARTICULAR', '658', NULL, 'C. DINA MERCEDES PACHECO BAZAN', 'AGROQUIMICOS EL CAMPO', '9838323307', NULL, NULL, 'AVENIDA HEROES #207-A', '9838323307',
        'elcampo_cfdi@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (585, 'PARTICULAR', '661', NULL, 'C. VICTOR MANUEL RAMIREZ LEÓN', 'HELADOS EL TESORO DEL PIRATA', '9831369316', NULL, 'C. VICTOR MANUEL RAMIREZ LEON',
        'CORCEGA S/N., MANZANA 15, LOTE 7, COL. JOSEFA ORTIZ ENTRE CAMELIAS Y ANDADOR 21', '9831369316', 'helados_el pirata@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (586, 'PARTICULAR', '662', NULL, 'C.ROQUE EDUIN DURAN NOVELO', 'EXTINGUIDORES 3 ESTRELLAS', '8372332/9831123381', NULL, 'C.ROQUE EDUIN DURAN NOVELO', 'LAGUNA LA VIRTUD #41',
        '8372332/9831123381', 'extinguidores_3_estrellas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (587, 'PARTICULAR', '665', NULL, 'C. CELIANA TZIU POOT', 'PANADERIA CELIANA', '9831816414', NULL, 'C. CELIANA TZIU POOT',
        'CALLE RAMONAL, MANZANA 276, LOTE 6, COL. PROTERRITORIO ENTRE JACINTO PAT', '9831816414', 'CELIATZIU21@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (588, 'PARTICULAR', '666', NULL, 'C. NELLY BEATRIZ VILLANUEVA YAH', 'PURIFICADORA ECOPURA', '9831544607', NULL, 'C. NELLY BEATRIZ VILLANUEVA YAH',
        'ANGUILLA #278 ENTRE DOMINICA Y CHETUMAL, FRACC. CARIBE', '9831544607', 'nellyvillanuevayahz@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (589, 'PARTICULAR', '667', NULL, 'C. JOSE ANGEL SANTELIZ CRUZ', 'RESTAURANTE BUFFALO BURGER', '9831385553', NULL, 'C. JOSE ANGEL SANTELIZ CRUZ',
        'AVENIDA HEROES # 203 ENTRE SAN SALVADOR Y CAMELIAS', '9831385553', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (590, 'PARTICULAR', '668', NULL, 'C. LORENA PEREZ GARCIA', 'LONCHERIA LA CHARCA DE LAS RANAS VERDES', '8323441', NULL, 'C. LORENA PEREZ GARCIA',
        'FRANCISCO I. MADERO #6 ENTRE 22 DE ENERO Y BOULEVARD BAHIA', '8323441', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (591, 'PARTICULAR', '669', NULL, 'C. ALONDRA GUADALUPE CARRILLO RIOS', 'ANTOJITOS LA GUADALUPANA', '9831178841', NULL, 'C. ALONDRA GUADALUPE CARRILLO RIOS',
        'AVENIDA BUGAMBILIAS #134, COLONIA ADOLFO LOPEZ MATEOS', '9831178841', 'alondra_cr@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (592, 'PARTICULAR', '745', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO', 'DISPENSADOR LA GOTA CRISTALINA', '9838390722', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO',
        'AVENIDA VENUSTIANO CARRANZA #242 POR FLORES MAGON , COLONIA CASITAS', '9838390722', 'impregrafic1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (593, 'PARTICULAR', '671', NULL, 'C. LIZBETH ANDREA ROSADO MEDINA', 'PURIFICADORA GOTA CRISTALINA', '9838390722', NULL, 'C. LIZBETH ANDREA ROSADO MEDINA',
        'AVENIDA JOSE MARIA MORELOS #401 POR AVENIDA JUAN JOSE SIORDIA', '9838390722', 'impregrafic1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (594, 'PARTICULAR', '672', NULL, 'C.LAWRENCE ALEJANDRO PADILLA DIAZ', 'ANTOJITOS ARLETT', '9831031488', NULL, 'C.LAWRENCE ALEJANDRO PADILLA DIAZ',
        'CALZADA VERACRUZ #299, ESQUINA JUAN SARABIA', '9831031488', 'alvaro_canche@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (595, 'PARTICULAR', '673', NULL, 'C. PABLO EUAN HERRERA', 'RESTAURANTE LA PEQUEÑA ROCA DE ORO', '9831074734', NULL, NULL, 'AVENIDA OAXACA #2, CALDERITAS', '9831074734',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (596, 'PARTICULAR', '674', NULL, 'C. PABLO EUAN HERRERA', 'RESTAURANTE LA PEQUEÑA ROCA DE ORO', '9831074734', NULL, 'C. PABLO EUAN HERRERA',
        'AVENIDA OAXACA #2, CALDERITAS, Q. ROO', '9831074734', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (597, 'PARTICULAR', '675', NULL, 'C. PABLO PEDRO DORANTES NUÑEZ', 'RESTAURANT BAR EL MILAGRO', '9831620381, 9831302476', NULL, 'C. PEDRO PABLO DORANTEZ NUÑEZ',
        'JUAREZ #331 POR LAGUNA MILAGROS, COL. DAVID GUSTAVO GUTIERREZ', '9831620381, 9831302476', 'leo190405330@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (598, 'PARTICULAR', '676', NULL, 'C. MARIA JESUS MATUS CORAL', 'TAQUERIA Y TORTERIA MARY', '9831308347', NULL, 'C. MARIA JESUS MATUS CORAL',
        'INTERIOR DEL MERCADO ALTAMIRANO LOCAL 110, COLONIA CENTRO', '9831308347', 'fbarrerasarabia@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (599, 'PARTICULAR', '677', NULL, 'C. ESTELA CUEVAS OCAMPO', 'ANTOJITOS DOÑA ESTELA', '7531309553', NULL, 'C. ESTELA CUEVAS OCAMPO',
        'ALFREDO V BONFIL, MANZANA 234, LOTE 15, COLONIA SOLIDARIDAD', '7531309553', 'estela_c063@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (600, 'PARTICULAR', '678', NULL, 'C. MEDARDO GIL SANTIAGO', 'RESTAURANTE LOS CERROS DE IZAMAL', '9831828969, 9831237356', NULL, 'C. MEDARDO GIL SANTIAGO',
        'AV. OAXACA #1 ENTRE CAMPECHE Y 24 DE FEBRERO, CALDERITAS, Q. ROO', '9831828969, 9831237356', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (601, 'PARTICULAR', '679', NULL, 'C. LOURDES MARIA MATOS HERRERA', 'TORTILLERIA HANNITA', '8375920', NULL, 'C. LOURDES MARIA MATOS HERRERA',
        'TUMBENCUXTAL, LOTE 9, MANZANA 3 A', '8375920', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (602, 'PARTICULAR', '680', NULL, 'C. MARIA MARCELINA CHI DZUL', 'TIENDA ESCOLAR HIDALGO', '20872', NULL, 'C. MARIA MARCELINA CHI DZUL',
        'AVENIDA 16 DE SEPTIEMBRE # 28, COLONIA CENTRO', '20872', 'marsscjp@outlook.es', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (603, 'PARTICULAR', '682', NULL, 'IMPULSORA PLAZA TORREON S.A. DE C.V.', 'HOTEL CITYEXPRESS CHETUMAL', '8351980', NULL, 'IMPULSORA PLAZA TORREON S.A. DE C.V.',
        'CALLE PERDIZ #442, COLONIA EMANCIPACION INFONAVIT', '8351980', 'cectm.contador@cityexpress.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (604, 'PARTICULAR', '683', NULL, 'C. LETICIA DEL CARMEN SALAZAR MAY', 'POLLOS ASADOS SALAZAR', '98313434771', NULL, 'C. LETICIA DEL CARMEN SALAZAR MAY',
        'NICOLAS BRAVO ESQUINA DOS AGUADAS, COLONIA SOLIDARIDAD', '98313434771', 'pollosasados.salazar@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (605, 'PARTICULAR', '684', NULL, 'C. RICARDA HERNANDEZ MARTINEZ', 'POLLOS ASADOS AL CARBÓN EL DRAGÓN', '8371023', NULL, 'C. RICARDA HERNANDEZ MARTINEZ',
        'AV. AARON MERINO FERNANDEZ #115, COL. FOVISSSTE CUARTA ETAPA', '8371023', 'ricarda_hernandezmx@hotmail', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (606, 'PARTICULAR', '685', NULL, 'C. MANUEL DE JESUS RUIZ CHAN', 'PESCADOS Y MARISCOS EL MERO MERO', '9831542596', NULL, 'C. MANUEL DE JESUS RUIZ CHAN',
        'CALLE REFORMA AGRARIA # 15-B, MANZANA 57, LOTE 19', '9831542596', 'mruiz.18@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (607, 'PARTICULAR', '686', NULL, 'C. ROSA ONGAY GARCIA', 'LONCHERIA MIRADA DE MUJER', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (608, 'PARTICULAR', '687', NULL, 'C. JOSE GARCIA GUILHARAY', 'PURIFICADORA NATURAL', NULL, NULL, 'C. JOSE GARCIA GUILHARAY', 'MACHUCHAC #216', NULL, NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (609, 'PARTICULAR', '688', NULL, 'C. RICARDO NOVELO CHALE', 'PANADERIA LOS NOVELOS', '9831034073', NULL, 'C. RICARDO NOVELO CHABLE',
        'RIO VERDE, MANZANA 222, LOTE 24, COLONIA SOLIDARIDAD', '9831034073', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (610, 'PARTICULAR', '689', NULL, 'C. LUIS DOMINGUEZ RENDON', 'PURIFICADORA NAT HA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (611, 'PARTICULAR', '690', NULL, 'C. ILEANA HOIL CASTILLO', 'PESCADERIA EL MARLIN', '9838328568', NULL, NULL, 'NICOLAS BRAVO, LOTE 19, MANZANA 193, COLONIA SOLIDARIDAD',
        '9838328568', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (612, 'PARTICULAR', '691', NULL, 'C. JUAN CASTILLO PINZON', 'COCINA ECONOMICA LA CASA DE LAS COMIDAS', NULL, NULL, 'C. JUAN CASTILLO PINZON',
        'ENRIQUE BAROCIO #1031, COLONIA MILENIO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (613, 'PARTICULAR', '692', NULL, 'C. NENIDELIA JIMENEZ ARIAS', 'TORTAS Y HAMBURGUESAS LAS MISMAS', '9838334635', NULL, 'C. NENIDELIA JIMENEZ ARIAS',
        'AVENIDA HEROES ESQUINA ZARAGOZA LOCAL 2, COLONIA CENTRO', '9838334635', 'busho60@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (614, 'PARTICULAR', '693', NULL, 'C. MARIA LUZ SANTOS CARRILLO', 'ANTOJITOS SAN RAFAEL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (615, 'PARTICULAR', '695', NULL, 'C. ARIEL VALENCIA ARRONTE', 'LONCHERIA Y JUGUERIA SUPER FRUTY', '9831000402', NULL, 'C. ARIEL VALENCIA ARRONTE',
        'EFRAIN AGUILAR #156 ENTRE FRANCISCO I. MADERO Y CHAPULTEPEC', '9831000402', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (616, 'PARTICULAR', '695', NULL, 'C. ILMER HERRERA CANTO', 'PURIFICADORA VITALITY WATER', NULL, NULL, 'C. ILMER HERRERA CANTO',
        'PRIMO DE VERDAD #66, COLONIA PRIMERA LEGISLATURA', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (617, 'PARTICULAR', '696', NULL, 'C. ILMER HERRERA CANTO', 'PURIFICADORA VITALITY WALTER', NULL, NULL, 'C. ILMER HERRERA CANTO', 'BELICE #420, COLONIA SAITAP', NULL, NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (618, 'PARTICULAR', '697', NULL, 'C. ILMER HERRERA CANTO', 'PURIFICADORA VITALITY WATER', NULL, NULL, 'C. ILMER HERRERA CANTO', 'EFRAIN AGUILAR #145, COLONIA CENTRO', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (619, 'PARTICULAR', '698', NULL, 'C. GENOVEVA LARA ALONDRA', 'LONCHERIA LAS JAROCHAS', NULL, NULL, 'C. GENOVEVA LARA ALONDRA', 'ROJO GOMEZ #299 ESQUINA JUSTO SIERRA', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (620, 'PARTICULAR', '699', NULL, 'C. LEOPOLDO OROZCO FIGUEROA', 'TORTILLERIA FORJADORES', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (621, 'PARTICULAR', 'A1', NULL, 'C. MARTHA PABLO ARTONAL', 'LONCHERIA VERACRUZ', '9831582062', NULL, 'C. MARTHA PABLO ARTONAL', 'AV. 19 ENTRE CALLE 30 Y 22, COL. 5 DE MAYO',
        '9831582062', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (622, 'PARTICULAR', '700', NULL, 'C. AURORA REINA BALAM UH', 'RESTAURANTE LA AURORA', '9831863008', NULL, 'C. AURORA REINA BALAM UH',
        'AVENIDA OAXACA # 08, COL. CENTRO, CALDERITAS, Q. ROO', '9831863008', 'fer_silveira16@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (623, 'PARTICULAR', '701', NULL, 'C. DARCEL YURIDIA BRICEÑO VALADEZ', 'ANTOJITOS LETY', '9838387938, 9831546760', NULL, 'C. DARCEL YURIDIA BRICEÑO VALADES',
        'FERNANDO ESPINOZA #449', '9838387938, 9831546760', 'roberto_pat47@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (624, 'PARTICULAR', '701', NULL, 'C. VICENTE GONZALEZ ROSALES', 'MACHAFRUTS', '9831239896', NULL, 'C. VICENTE GONZALEZ ROSALES',
        'AVENIDA BELICE #307-A ENTRE BUGAMBILIAS Y JUSTO SIERRA', '9831239896', 'vicenteg/2383@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (625, 'PARTICULAR', '702', NULL, 'C. JULIO OLEGARIO REJON TORRES', 'CEVICHES Y COCTELES LOS COCOS', '9838398222', NULL, 'C. JULIO OLEGARIO REJON TORRES',
        'AVENIDA FRANCISCO I MADERO #398, COLONIA DAVID GUSTAVO GUTIERREZ', '9838398222', 'cevichesloscocs@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (626, 'PARTICULAR', '703', NULL, 'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'MAKO SOLUCIONES INTEGRALES Y EXTERMINACIÓN DE PLAGAS', '9831258387', NULL,
        'C. ELSA GABRIELA ESCOBEDO JIMENEZ', 'CALLE LAGUNA GUERRERO, LOTE 11, MANZANA 3 ENTRE OM Y NOH BEC, COL. LAGUNITAS', '9831258387', 'makochetumal@gmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (627, 'PARTICULAR', '704', NULL, 'C. OSCAR JIMENEZ RUIZ', 'TAQUERIA TEN-TEN PIE', '9831579412, 9831302476', NULL, 'C. OSCAR JIMENEZ RUIZ', 'AVENIDA HEROES #232',
        '9831579412, 9831302476', 'jimmlopez74@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (628, 'PARTICULAR', '708', NULL, 'C. LEOPOLDO OROZCO FIGUEROA', 'TORTILLERIA FORJADORES', '8373765', NULL, 'C. LEOPOLDO OROZCO FIGUEROA',
        'DARIO GUERRERO, MANZANA 50, LOTE 26, COL. FORJADORES', '8373765', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (629, 'PARTICULAR', '709', NULL, 'C. EPIFANIA TLAPA GUERRERO', 'TAQUERIA CHIHUAHUA', NULL, NULL, 'C. EPIFANIA TLAPA GUERRERO',
        'CRISTOBAL COLON ENTRE HEROES Y 16 DE SEPTIEMBRE', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (630, 'PARTICULAR', '710', NULL, 'C. VERONICA DIAZ DE LA CRUZ', 'PIZZAS Y PASTELES MELYVER', '9831383803', NULL, 'C. VERONICA DIAZ DE LA CRUZ',
        'BUGAMBILIAS #323 ENTRE FLORES MAGON Y RAFAEL E. MELGAR', '9831383803', 'robertoseg@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (631, 'PARTICULAR', '711', NULL, 'C. MATUSALEM GOMEZ SAENZ', 'PANADERIA LA SELVA', '983 1309771', NULL, 'C. MATUSALEM GOMEZ SAENZ', 'CALLE POLYUC ENTRE DOMINICA Y BON AIRE',
        '983 1309771', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (632, 'PARTICULAR', '712', NULL, 'C. ELIZABETH ALONSO ANGULO', 'COLEGIO PARTICULA PRIMITIVO ALONSO', NULL, NULL, 'C. ELIZABETH ALONSO ANGULO',
        'CENTENARIO #595 ENTRE COMONFORT', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (633, 'PARTICULAR', '713', NULL, 'C. MAXIMA CAHUM TAMAY', 'PANADERIA GLORIA', NULL, NULL, 'C. MAXIMA CAHUM TAMAY', 'JUSTO SIERRA CON FLORENCIA #298', NULL, NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (634, 'PARTICULAR', '714', NULL, 'C. ISOLINA DEL SOCORRO RAMIREZ RODRIGUEZ', 'LONCHERIA CARMITA', '1444320', NULL, 'C. ISOLINA SOCORRO RAMIREZ RODRIGUEZ',
        'ISAAC MEDINA, MANZANA 62, LOTE 2 , COL. GUADALUPE VICTORIA', '1444320', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (635, 'PARTICULAR', '715', NULL, 'C. ESTANIS EVANGELINA LARA VALENCIA', 'MARISQUERIA XCALAK', '9831302476', NULL, 'C. ESTANIS EVANGELINA LARA VALENCIA',
        'ANTONIO CORIA #226 ENTRE JUAREZ E INDEPENDENCIA, COL. DAVID GUSTAVO', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (636, 'PARTICULAR', '716', NULL, 'C. ESTHER GUTIERREZ MAGAÑA', 'PURIFICADORA VITAL PUREZA', '983 1684882', NULL, 'C. ESTHER GUTIERREZ MAGAÑA',
        'AV. NICOLAS BRAVO # 957 ESQUINA ISLAS VIRGENES, FRACC. CARIBE II', '983 1684882', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (637, 'PARTICULAR', '717', NULL, 'C. DAVID ANDRADE JIMENEZ', 'TAQUERIA EL PAISANO', '9831090955', NULL, 'C. DAVID ANDRADE JIMENEZ',
        'CALZADA VERACRUZ #489 CON BENJAMIN HILL, COL. ADOLFO LOPEZ MATEOS', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (638, 'PARTICULAR', '718', NULL, 'C. NAVOR LOPEZ TOLENTINO', 'TAQUERIA LA CAPITAL', '9831111185', NULL, 'C. NAVOR LOPEZ TOLENTINO',
        'AV. ANDRES Q.ROO #351 ENTRE ISLA CANCUN Y LAGUNA DE BACALAR', '9831111185', 'YESENNIA_R@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (639, 'PARTICULAR', '719', NULL, 'C. MANUELA JESUS CHE CHAN', 'PANADERIA MIRAFLORES', NULL, NULL, 'C. MANUELA JESUS CHE CHAN', 'RAMON DEL VALLE #304 CON BUGAMBILIAS', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (640, 'PARTICULAR', '720', NULL, 'C. YARELI SARAIM MENDEZ COB', 'ANTOJITOS VICTORIA', NULL, NULL, 'C.YARELI SARAIM MENDEZ COB', 'AVENIDA 19 POR LIBRAMIENTO 18 Y 20', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (641, 'PARTICULAR', '721', NULL, 'C. CARLOS DE LA FUENTE Y L', 'ESTANCIA INFANTIL EDUCARE PLAYGROUND', NULL, NULL, 'C. CARLOS DE LA FUENTE Y L',
        'HERMINIO AHMADA #38 , COLONIA GONZALO GUERRERO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (642, 'PARTICULAR', '722', NULL, 'C. ANAHI ILEANA PEREZ VIANA', 'CAFETERIA CET MAR', NULL, NULL, 'C. ANAHI ILEANA PEREZ VIANA',
        'PROLONGACION BOULEVARD BAHIA CON UNIVERSIDAD', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (643, 'PARTICULAR', '723', NULL, 'C. JOSE ANTONIO ORTEGA HERNANDEZ', 'RESTAURANTE SAVORA CAFE', NULL, NULL, 'C. JOSE ANTONIO ORTEGA HERNANDEZ', 'VENUSTIANO CARRANZA', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (644, 'PARTICULAR', '724', NULL, 'C. BEGOÑA COLLADO AROZAMENTA', 'BAR EJECUTIVO NIGH CLUB LA QUINTA', NULL, NULL, 'C. BEGOÑA COLLADO AROZAMENTA', '5 DE MAYO #96-B', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (645, 'PARTICULAR', '725', NULL, 'C. JOSE MARIA MORENO CARDENAS', 'RESTAURANTE Y MARISQUERIA EL PARGO', '9831071738', NULL, 'C. JOSE MARIA MORENO CARDENAS',
        'KM. 8 A UN COSTADO DEL PUENTE', '9831071738', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (646, 'PARTICULAR', '726', NULL, 'C. BLANCA SUGEY PAYAN VAZQUEZ', 'PALETERIA LA MICHOACANA', '9991987285', NULL, 'C. BLANCA SUGEY PAYAN VAZQUEZ',
        'AV. INSURGENTES #41 PLAZA BAHIA, LOCAL 25 ENTRE UNIVERSIDAD Y HERIBERTO FRIAS', '9991987285', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (647, 'PARTICULAR', '727', NULL, 'C. OFELIA GALLEGOS GUTIERREZ', 'TORTILLERIA MICHELLE', '8335820, 9832108564', NULL, 'C. OFELIA GALLEGOS GUTIERREZ',
        'JUAN DE LA BARRERA #61 ESQUINA JOSEFA ORTIZ', '8335820, 9832108564', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (648, 'PARTICULAR', '728', NULL, 'C. LUCIA DEL CARMEN PUC CHIMAL', 'TORTILLERIA LA MILPA', '9838366258', NULL, 'C. LUCIA DEL CARMEN PUC CHIMAL',
        'JESUS MARTINEZ ROSS ENTRE OTHON P BLANCO Y CARRETERA FEDERAL, UCUM Q. ROO', '9838366258', 'lucia_puc@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (649, 'PARTICULAR', '729', NULL, 'C. ANGEL HERNAN BAEZA VIVAS', 'PESCADOS Y MARISCOS LA LUZ DEL DIA 2', '9831680757', NULL, 'C. ANGEL HERNAN BAEZA VIVAS',
        'CALLE 11 ENTRE 7 Y 8, COL. GUADALUPE VICTORIA', '9831680757', 'laluzdeldia79@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (650, 'PARTICULAR', '731', NULL, 'C. ENRIQUE FERNANDEZ MARTINEZ', 'POLLOS ASADOS ENRIQUEZ', '9831130185', NULL, 'C. ENRIQUE FERNANDEZ MARTINEZ', 'AVENIDA CHICOZAPOTE # 240',
        '9831130185', 'yoyofdez@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (651, 'PARTICULAR', '731', NULL, 'C. CANDIE ANGULO CANTO', 'CAFETERIA LA CANDELARIA', '9831076511', NULL, 'C. CANDIE ANGULO CANTO',
        'AVENIDA HEROES #167 ENTRE EFRAIN AGUILAR Y MAHATMA GHANDI', '9831076511', 'candieangulo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (652, 'PARTICULAR', '732', NULL, 'C. GIOVANNA GUADALUPE UH ALONZO', 'RESTAURANT MIRAMAR', '9837524924', NULL, 'C. GIOVANNA GUADALUPE UH ALONZO',
        'AVENIDA OAXACA CON 24 DE FEBRERO, LOCAL 4, CALDERITAS, Q. ROO', '9837524924', 'giovannaalonzo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (653, 'PARTICULAR', '733', NULL, 'C. CARMEN ZARATE GONZALEZ', 'TORTILLERIA LA OAXAQUEÑA', '9831036315', NULL, 'C. CARMEN ZARATE GONZALEZ',
        'CALLE ISLAS VIRGENES #429, COLONIA NUEVA GENERACION', '9831036315', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (654, 'PARTICULAR', '734', NULL, 'C. ABRAHAM DAVID HOIL AZNAR', 'TORTILLERIA LOS DOS ANGELITOS', '9831199503', NULL, 'C. ABRAHAM DAVID HOIL AZNAR',
        'COAHUILA, MANZANA 28, LOTE 3, CALDERITAS, Q. ROO', '9831199503', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (655, 'PARTICULAR', '736', NULL, 'C. RAQUEL DZUL CHAZARRETA', 'TORTILLERIA FIDEL', '9831831425', NULL, 'C. RAQUEL DZUL CHAZARRETA', 'MACHUXAC, MANZANA 554, LOTE 11',
        '9831831425', 'rakel-dc@hotmail.cm', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (656, 'PARTICULAR', '737', NULL, 'C. RICARDO AMARO ESCALANTE', 'PURIFICADORA AQUACLYVA', '9831028375', NULL, 'C. RICARDO AMARO ESCALANTE',
        'CALLE GRACIANO SANCHEZ, MANZANA 385, LOTE 23 COLONIA PROTERRITORIO', '9831028375', 'gabriel0104080@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (657, 'PARTICULAR', '738', NULL, 'C. ANA MARIA ROSAS TZEC', 'PESCADERIA LAS YULIS', '9838332601', NULL, 'C. ANA MARIA ROSAS TZEC',
        'AV. NICOLAS BRAVO #704, LOTE 4, MANZANA 750, ESQ. OTILIO MONTAÑO', '9838332601', 'anamariarosas2019@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (658, 'PARTICULAR', '739', NULL, 'C. TOMASA SIMBRON DELGADO', 'LONCHERIA DOÑA TOMY', '9838359293', NULL, 'C. TOMASA SIMBRON DELGADO',
        'BENITO JUAREZ, LOTE 4, POBLADO JAVIER ROJO GOMEZ', '9838359293', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (659, 'PARTICULAR', '740', NULL, 'C. JUAN ENRIQUE PARRA CALDERON', 'MARISQUERIA RESTAURANTE DONDE SEA', '9831090955', NULL, 'C. JUAN ENRIQUE PARRA CALDERON',
        'AV. JUSTO SIERRA #286, MANZANA 14, LOTE 13, COL. LEONA VICARIO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (660, 'PARTICULAR', '753', NULL, 'C.LUIS MONTALVO DOMINGUEZ RENDON', 'PURIFICADORA NAT HA', '9831341618', NULL, 'C.LUIS MONTALVO DOMINGUEZ RENDON',
        'CALLE 14 SUR, LOTE 15, PACTO OBRERO CAMPESINO', '9831341618', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (661, 'PARTICULAR', '743', NULL, 'C. ERNESTO ESTEBAN PECH TEH', 'TAQUERIA DBC', '9831669922', NULL, 'C. ERNESTO ESTEBAN PECH TAH',
        'AVENIDA EFRAIN AGUILAR #154, COLONIA CENTRO', '9831669922', 'car_cielo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (662, 'PARTICULAR', '744', NULL, 'C.FRANCISCO GARCIA ONTIVEROS', 'FRUTERIA Y POLLERIA EL PATRON', '9831860559', NULL, 'C.FRANCISCO GARCIA ONTIVEROS', 'AVENIDA JUAREZ #315',
        '9831860559', 'lluisgarciaxd1996@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (663, 'PARTICULAR', '746', NULL, 'C. AIDA AMELIA SANTELIZ RODRIGUEZ', 'RESTAURANT FORASTEROS', '8331500, 9831179775', NULL, 'C. AIDA AMELIA SANTELIZ RODRIGUEZ',
        'FRANCISCO I. MADERO #318, ESQUINA BUGAMBILIAS', '8331500, 9831179775', 'forasteros1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (664, 'PARTICULAR', '747', NULL, 'C. ROGELIO CASTILLO BRITO', 'T-CONTROL DE PLAGAS', '8323727', NULL, 'C. ROGELIO CASTILLO BRITO',
        'CALLE LUIS CABRERA #107 , COLONIA ADOLFO LOPEZ MATEOS', '8323727', 'brito_21245@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (665, 'PARTICULAR', '748', NULL, 'C. ROBERT ELIU ANGULO CORAL', 'TAQUERIA CORAL', '9831266477', NULL, 'C. ROBERT ELIU ANGULO CORAL',
        'AVENIDA ALVARO OBREGON#330 ENTRE RAFAEL E MELGAR Y EMILIANO ZAPATA', '9831266477', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (666, 'PARTICULAR', '749', NULL, 'C. HUMBERTO CAB SABIDO', 'TORTILLERIA LA HIGIENE', '9831861894', NULL, 'C. HUMBERTO CAB SABIDO', 'CALLE 8 ENTRE 7 Y 9', '9831861894', NULL,
        NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (667, 'PARTICULAR', '750', NULL, 'C. DALIA NOEMI UN CHIMAL', 'TORTILLERIA LA HIGIENICA', '9831861894', NULL, 'C. DALIA NOEMIO UN CHIMAL',
        'AVENIDA 7 POR 30, BACALAR, Q. ROO', '9831861894', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (668, 'PARTICULAR', '750', NULL, 'C. DALIA NOEMI UN CHIMAL', 'TORTILLERIA LA HIGIENICA', '9831861894', NULL, 'C. DALIA NOEMI UN CHIMAL',
        'CALLE 30 POR AVENIDA 7 COLONIA GONZALO GUERRERO, BACALAR, Q. ROO', '9831861894', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (669, 'PARTICULAR', '751', NULL, 'C. ADORALIDA MENDOZA ZETINA', 'TORTILLERIA LA BENDICION DE DIOS', '9838342381, 9831820423, 9831840446', NULL,
        'C. ADORALIDA MENDOZA ZETINA', 'CALLE 4 S/N. ENTRE 19, MANZANA 37, LOTE 10, COL. DIEGO ROJAS ZAPATA', '9838342381, 9831820423, 9831840446', NULL, NULL, NULL,
        'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (670, 'PARTICULAR', '752', NULL, 'C. ANTONIO DE ATOCHA SALGADO MENDOZA', 'FUMIGADORA EL MATADOR DE PLAGAS DE Q.ROO', '9831205679', NULL,
        'C. ANTONIO DE ATOCHA SALGADO MENDOZA', 'CALLE MILAN #246 ENTRE V. CARRANZA Y SAN SALVADOR', '9831205679', 'antonio.salgado@live.com.mx', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (671, 'PARTICULAR', '753', NULL, 'C. MAGDALENA MISS LISCANO', 'TIENDA ESCOLAR TECNICA #15', '983161830, 9831261630', NULL, 'C. MAGDALENA MISS LISCANO',
        'CALLE JAZMINES S/N., COLONIA JARDINES', '983161830, 9831261630', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (672, 'PARTICULAR', '754', NULL, 'C. WILLIAM ALBERTO CARRILLO INTERIAN', 'PURIFICADORA DE AGUA AZUL CIELO', '9831067526', NULL, 'C. WILLIAM ALBERTO CARRILLO INTERIAN',
        'CALLE LAGUNA DE MILAGROS #206 ENTRE AV. BELICE Y AV. JUAREZ', '9831067526', 'waci@live.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (673, 'PARTICULAR', '755', NULL, 'C. VICTOR MANUEL HUH COLLI', 'ANTOJITOS ARACELY', '9831043612', NULL, 'C. VICTOR MANUEL HUH COLLI',
        'NAPOLES #250 ESQUINA BOLONIA , COLONIA ITALIA', '9831043612', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (674, 'PARTICULAR', '756', NULL, 'C. MARTIN GURGUA ESPINOZA', 'EMPANADAS LAS FAVORITAS', '9832654259', NULL, 'C. MARTIN GURGUA ESPINOZA', 'AVENIDA CALZADA VERACRUZ S/N.',
        '9832654259', 'kodashy_13@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (675, 'PARTICULAR', '757', NULL, 'C. ALEX CHEN', 'SUBWAY LAS AMERICAS', '9831564623', NULL, 'C. ALEX CHEN',
        'AV. INSURGENTES KM 5.025, INTERIOR LOCAL G-9, COL. EMANCIPACION INFONAVIT', '9831564623', 'subway.chetumal@grupomandarin.com.mx', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (676, 'PARTICULAR', '795', NULL, 'C. EDDIE KWOK CHUN CHAN', 'RESTAURANT LE PETIT PARIS', '9838314129', NULL, 'C. EDDIE KWOK CHUN CHAN',
        'AVENIDA INSURGENTES KM 5.025, LOCAL G-4 INTERIOR PLAZA LAS AMERICAS', '9838314129', 'chetumal@grupomandarin.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (677, 'PARTICULAR', '759', NULL, 'C. AURORA MARTINEZ MEX', 'RESTAURANTE EL ABUELO', '9831171536', NULL, 'C. AURORA MARTINEZ MEX',
        'AVENIDA LAGUNA MILAGROS , POBLADO HUAY-PIX', '9831171536', 'auro0604@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (678, 'PARTICULAR', '760', NULL, 'C. MAGALLON ZEFERINA ANGELA', 'RESTAURANTE THE DRUNK BURRITO', '9831653092', NULL, 'C. MAGALLON ZEFERINA ANGELA',
        'AVENIDA MAHAHUAL S/N., COLONIA MAHAHUAL, MAHAHUAL, Q. ROO', '9831653092', 'chewe@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (679, 'PARTICULAR', '761', NULL, 'C. DORANTES MAGALLON ISAIAS', 'RESTAURANTE BIG MAMA', '9831653092', NULL, 'C. DORANTES MAGALLON ISAIAS',
        'AVENIDA MAHAHUAL S/N, COLONIA MAHAHUAL, MAHAHUAL, Q. ROO', '9831653092', 'chewe@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (680, 'PARTICULAR', '762', NULL, 'C. FIDELA SANCHEZ HERNANDEZ', 'RESTAURANT EL DELFIN', '9831163612', NULL, 'C. FIDELY SANCHEZ HERNANDEZ',
        'OAXACA, LOCAL #15, CALDERITAS, Q. ROO', '9831163612', 'fide927@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (681, 'PARTICULAR', '763', NULL, 'C. SEBASTIANA RUIZ CRUZ', 'FUMIGACIONES INTEGRALES DE Q. ROO', '9831162947', NULL, 'C. SEBASTIANA RUIZ CRUZ',
        'AVENIDA ERICK PAOLO MARTINEZ #100', '9831162947', 'acatic51@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (682, 'PARTICULAR', '764', NULL, 'C. ERWIN EDILBERTO CANO SIERRA', 'PURIFICADORA VITALY WATER', '9831311807', NULL, 'C. ERWIN EDILBERTO CANO SIERRA',
        'MARIA CRISTINA SANGRI S/N. ESQUINA ESTADOS UNIDOS, AMERICAS III', '9831311807', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (683, 'PARTICULAR', '765', NULL, 'C. NESLY TERESITA DE JESUS SAMOS CANUL', 'PANADERIA LOS PICAPIEDRAS II', '9831343184, 1443031', NULL,
        'C. NESLY TERESITA DE JESUS SAMOS CANUL', 'AV. CHETUMAL #690 ENTRE GRACIANO SANCHEZ Y MANUEL CRECENCIO R., LOTE 2, MANZANA', '9831343184, 1443031',
        'elmarro_paicapiedras@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (684, 'PARTICULAR', '766', NULL, 'C. CONSUELO GARCIA LIRA', 'ANTOJITOS DOÑA CHELO', '9838780145', NULL, 'C. CONSUELO GARCIA LIRA',
        'FRENTE AL MERCADO BENITO JUAREZ, POBLADO JAVIER ROJO GOMEZ', '9838780145', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (685, 'PARTICULAR', '767', NULL, 'INMOBILIARIA OMAÑA S.A. DE C.V.', 'HOTEL HACIENDA BAHIA', '9831344178', NULL, 'INMOBILIARIA OMAÑA S.A. DE C.V.',
        'PROLONGACION BOULEVARD BAHIA FRACCION 13 S/N.', '9831344178', 'recepcionhaciendabahia@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (686, 'PARTICULAR', '768', NULL, 'C. SIGFRIED ALAIN OSORIO LEYVA', 'JUGUERIA EL NIÑO DE ORO', '9831394812', NULL, 'C. SIGFRIED ALAIN OSORIO LEYVA',
        'AVENIDA ROJO GOMEZ #325', '9831394812', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (687, 'PARTICULAR', '769', NULL, 'C. CRISTOBAL RAMOS VIDAL', 'PURIFICADORA LA GOTA DE LA VIDA', '9838357606', NULL, 'C. CRISTOBAL RAMOS VIDAL', 'EJIDO CACAO, Q.ROO # 296',
        '9838357606', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (688, 'PARTICULAR', '770', NULL, 'C. JOSE LUIS SALA MONTEJO', 'MATERIAS PRIMAS SAHI S. DE R.L. MI.', '9831240204', NULL, 'C. JOSE LUIS SALA MONTEJO',
        'AVENIDA 102, MANZANA 1A, LOTE 1, PARQUE INDUSTRIAL', '9831240204', 'corporativo_proplas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (689, 'PARTICULAR', '771', NULL, 'C. MAURICIO CABALLERO ESTUPIÑAN', 'COMERCIALIZADORA MALEJO S.A. DE C.V. "ECOPLAGA"', '9838331522', NULL, 'C. MAURICIO CABALLERO ESTUPIÑAN',
        'CALLE 1 POR AV. 3, LOTE 11, COL. ANTIGUA ZONA INDUSTRIAL', '9838331522', 'mauricio_caballero@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (690, 'PARTICULAR', '772', NULL, 'C. CLARA DEL SOCORRO SUAREZ PALOMO', 'LONCHERIA CLASILJE', '9831176428', NULL, 'C. CLARA DEL SOCORRO SUAREZ PALOMO',
        'CALLE GUILLERMO PEYRIFFITE, MANZANA 36, LOTE 3, COL. FORJADORES', '9831176428', 'clasilje@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (691, 'PARTICULAR', '773', NULL, 'C. ANEL DE LA CRUZ GONGORA GONZALEZ', 'TAQUERIA EL PAISANO JR', '9831025304', NULL, 'C. ANEL DE LA CRUZ GONGORA GONZALEZ',
        'AVENIDA MAXUXAC, MANZANA 450, LOTE 9, COLONIA PROTERRITORIO', '9831025304', 'anel602@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (692, 'PARTICULAR', '774', NULL, 'C. DAVID DE LA CRUZ POMPEYO GARCIA', 'TORTILLERIA LA ASUNCION', '9831589393', NULL, 'C. DAVID DE LA CRUZ POMPEYO GARCIA',
        'CELUL, MANZANA 47, LOTE 13 B POR CHACHALACAS, COLONIA PAYO OBISPO', '9831589393', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (693, 'PARTICULAR', '775', NULL, 'C. EDUARDO ISABEL IC GUARDIA', 'RESTAURANTE LA JUNGLA', '8322292', NULL, 'C. EDUARDO ISABEL IC GUARDIA',
        'CALLE 24 #117 ENTRE AVENIDA 5 Y AVENIDA 7', '8322292', 'despacho.cancino@live.com.mx', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (694, 'PARTICULAR', '777', NULL, 'C. ROSA CRUZ VELAZCO', 'POLLOS ASADOS GENESIS', '9831409719', NULL, 'C. ROSA CRUZ VELAZCO',
        'AVENIDA CHETUMAL S/N., LOTE 02, MANZANA 200, COLONIA SOLIDARIDAD', '9831409719', 'rosa-pollito@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (695, 'PARTICULAR', '777', NULL, 'C. ROSA MANUELA CRUZ VELAZCO', 'POLLOS ASADOS GENESIS', '9831409719', NULL, 'C. ROSA MANUELA CRUZ VELAZCO',
        'AV. CHETUMAL S/N., LOTE 2, MANZANA 200, COL. SOLIDARIDAD', '9831409719', 'rosa-pollito@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (696, 'PARTICULAR', '778', NULL, 'C. ROBERTO BELTRAN MEDINA', 'DISPENSADOR AGUA PURIFICADA RIO AZUL', '9831269640', NULL, 'C. ROBERTO BELTRAN MEDINA',
        'CALLE COAHUILA #235 ENTRE QUERETARO Y GUANAJUATO, CALDERITAS, Q. ROO', '9831269640', 'chepo.android@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (697, 'PARTICULAR', '779', NULL, 'C. HECTOR BUSTILLOS ESCALANTE', 'VETERINARIA EL RANCHITO', '9838327165', NULL, 'C. HECTOR BUSTILLOS ESCALANTE', 'AVENIDA HEROES, LOCAL 1',
        '9838327165', 'agrovet_ranchito@hayoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (698, 'PARTICULAR', '780', NULL, 'C. JUANA MARIA DOMINGUEZ LOPEZ', 'LONCHERIA LA NEGRITA', '9831389738', NULL, 'C. JUANA MARIA DOMINGUEZ LOPEZ',
        'AVENIDA 19 ENTRE 40 Y 42, COLONIA COLOSIO', '9831389738', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (699, 'PARTICULAR', '781', NULL, 'C. EVARISTO TOLENTINO ALDANA', 'RESTAURANT BAR LOS COCOS', '9831403911', NULL, 'C. TOLENTINO ALDANA EVARISTO',
        'AVENIDA LIBRAMIENTO ENTRE 4 Y 6, COL. DIEGO ROJAS ZAPATA', '9831403911', 'magalena.carballo@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (700, 'PARTICULAR', '782', NULL, 'C. LUIS EDUARDO AREVALO CORTEZ', 'EL SEÑOR DE LAS ALITAS', '9831568433', NULL, 'C. LUIS EDUARDO AREVALO CORTEZ',
        'AV. MAXUXAC #138 ENTRE 1 DE MAYO Y 5 DE FEBRERO, COL. PROTERRITORIO', '9831568433', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (701, 'PARTICULAR', '783', NULL, 'C. VALENTINA KANTUN CUPUL', 'DISPENSADOR DE AGUA PURIFICADA SIIJIL HA', '9831269230', NULL, 'C. VALENTINA KANTUN CUPUL',
        'AVENIDA MAGISTERIAL, MANZANA 57, LOTE 13, COL. SOLIDARIDAD', '9831269230', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (702, 'PARTICULAR', '784', NULL, 'C. NELIDA CARINA CORDOVA LIZAMA', 'PURIFICADORA PREMIER', '9837533610', NULL, 'C. NELIDA CARINA CORDOVA LIZAMA',
        'CALLE BUGAMBILIAS #609 ESQUINA JUANA DE ASBAJE, COL. MIRAFLORES', '9837533610', 'cari.014@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (703, 'PARTICULAR', '785', NULL, 'C. ARACELY POOT AGUILAR', 'TAQUERIA EL TROMPITO', '9831090955', NULL, 'C. ARACELY POOT AGUILAR',
        'AVENIDA LUIS M. SEVILLA SANCHEZ #1054, COLONIA AMERICAS I, MANZANA 241, LOTE 3', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (704, 'PARTICULAR', '786', NULL, 'C. BERTHA REYES ARREOLA', 'RESTAURANT CHETUMAL', '9831090955', NULL, 'C. BERTHA REYES ARREOLA',
        'ANDRES QUINTANA ROO #257, COLONIA LAS CASITAS', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (705, 'PARTICULAR', '787', NULL, 'C. MAGDIEL LARA LOZANO', 'TORTILLERIA ADRIEL', '9831090955', NULL, 'C. MAGDIEL LARA LOZANO',
        'CALLE ISLAS VIRGENES #278, FRACCIONAMIENTO CARIBE', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (706, 'PARTICULAR', '789', NULL, 'C. ELDA MARIA CANCHE GARCIA', 'ASADERO RC', '9831090955', NULL, 'C. ELDA MARIA CANCHE GARCIA',
        'AVENIDA JUSTO SIERRA #562, COLONIA 8 DE OCTUBRE', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (707, 'PARTICULAR', '790', NULL, 'C. JANET DEL CARMEN AGUILAR BAÑOS', 'RESTAURANTE EL NAVEGANTE', '9831012471', NULL, 'C. JANET DEL CARMEN AGUILAR BAÑOS',
        'KM 55 CALLE TABLERO 3, MANZANA 18, LOTE 10, MAHAHUAL, Q. ROO', '9831012471', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (708, 'PARTICULAR', '791', NULL, 'C. OLGA VALDEZ HERNANDEZ', 'RESTAURANTE MAISON LA PETITE CREPE', '9831033227', NULL, 'C. OLGA VALDEZ HERNANDEZ',
        'AVENIDA ALVARO OBREGON #141 ESQUINA MIGUEL HIDALGO', '9831033227', 'la.petite.crepe.mx@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (709, 'PARTICULAR', '792', NULL, 'C. LAURA GRISELDA LOPEZ MARTINEZ', 'LONCHERIA LUCY', '9831230925', NULL, 'C. LAURA GRISELDA LOPEZ MARTINEZ',
        'INTERIOR DEL MERCADO MANUEL ALTAMIRANO LOCAL #85', '9831230925', 'nena_copelio1605@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (710, 'PARTICULAR', '793', NULL, 'C. SINFORIANO POOT CAAMAL', 'PURIFICADORA SAN FRANCISCO', '9831111240', NULL, 'C. SINFORIANO POOT CAAMAL',
        'AV. SANTIAGO PACHECO ESQUINA CALLE 68, COLONIA JUAN BAUTISTA', '9831111240', 'sinfo22@hotmail.com', NULL, NULL, 'Quintana Roo', 'FELIPE CARRILLO PUERTO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (711, 'PARTICULAR', '795', NULL, 'C. LETICIA YAÑEZ VILLANUEVA', 'RESTAURANTE LOS PORTALES', '9831547526', NULL, 'C. LETICIA YANEZ VILLANUEVA',
        'MERCADO LAZARO CARDENAS DEL RIO LOCAL 330-331, COL. ADOLFO LOPEZ MATEOS', '9831547526', 'jesus-omar800@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (712, 'PARTICULAR', '796', NULL, 'C. JUAN HERNANDEZ JIMENEZ', 'POLLOS ASADOS CHETUS', '9837324741', NULL, 'C. JUAN HERNANDEZ JIMENEZ',
        'CALZADA VERACRUZ ENTRE SAN SALVADOR Y CAMELIAS #261', '9837324741', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (713, 'PARTICULAR', '797', NULL, 'C. CARMEN ZARATE GONZALEZ', 'TORTILLERIA LA OAXAQUEÑA', '9831036315', NULL, 'C.CARMEN ZARATE GONZALEZ',
        'CALLE ISLAS VIRGENES #429, COLONIA NUEVA GENERACION', '9831036315', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (714, 'PARTICULAR', '798', NULL, 'C. REYNA CERVANTES HERNANDEZ', 'ANTOJITOS LAS PALMITAS', '9831385279', NULL, 'C. REYNA CERVANTES HERNANDEZ',
        'CALLE DURANGO ESQUINA TABASCO #121, CALDERITAS, Q.ROO', '9831385279', 'raquelramcer@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (715, 'PARTICULAR', '799', NULL, 'SILVER MOON S.A. DE C.V.', 'HOTEL RESTAURANT LUNA DE PLATA', '9831253999', NULL, 'C. DARIO PARADISO',
        'AVENIDA MAHAHUAL S/N., MANZANA 25, LOTE 2, MAHAHUAL, Q. ROO', '9831253999', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (716, 'PARTICULAR', '800', NULL, 'C. JOSE ALONSO RIVAS CISNEROS', 'CAFETERIA Y RESTAURANTE LOS MILAGROS', '9831206214', NULL, 'C. JOSE ALONSO RIVAS CISNEROS',
        'CALLE ZARAGOZA EDIFICIO CONSTITUYENTES, LOCAL 14 ENTRE HÉROES Y 5 DE MAYO', '9831206214', 'cafe_milagros@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (717, 'PARTICULAR', '801', NULL, 'C. MARTHA ELENA VELA CAMBRANIS', 'LONCHERIA DOÑA MARTHA', '9831139931, 9831769555', NULL, 'C. MARTHA ELENA VELA CAMBRANIS',
        'MERCADO MANUEL ALTAMIRANO, LOCAL 120 Y 121 ENTRE HÉROES Y JUAREZ', '9831139931, 9831769555', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (718, 'PARTICULAR', '802', NULL, 'C. MARIA DE LOS ANGELES RUIZ VAZQUEZ', 'CAFETERIA ESCOLAR SALVADOR LIZARRAGA', '9831657000', NULL, 'C. MARIA DE LOS ANGELES RUIZ VAZQUEZ',
        'CALLE SURINAM S/N., MANZANA 146 ENTRE PANAMA Y LUIS MANUEL SEVILLA', '9831657000', 'albertocob25@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (719, 'PARTICULAR', '803', NULL, 'C. MARGARITA DEL ROCIO BAEZA GUERRERO', 'LONCHERIA EL PARADERO', '8321302, 983 1131811', NULL, 'C. MARGARITA DEL ROCIO BAEZA GUERRERO',
        'AVENIDA EFRAIN AGUILAR #209', '8321302, 983 1131811', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (720, 'PARTICULAR', '803', NULL, 'C. MARGARITA DEL ROCIO BAEZA GUERRERO', 'LONCERIA EL PARADERO', '8321302', NULL, NULL, 'AVENIDA EFRAIN AGUILAR #209', '8321302', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (721, 'PARTICULAR', '804', NULL, 'C. CAROLINA ABALOS TORRES', 'ANTOJITOS NICOLLE', '9831327758', NULL, 'C. CAROLINA ABALOS TORRES', 'AVENIDA JUAREZ #61-C, COLONIA CENTRO',
        '9831327758', 'danielhernandezzabelosdany@outlook.es', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (722, 'PARTICULAR', '805', NULL, 'C. ERICA ALEJANDRA GONZALEZ ACOSTA', 'DISTRUIDORA DEL SURESTE S.A. DE C.V. (EL MICHOACANO)', '9831180277', NULL,
        'C. ERICA ALEJANDRA GONZALEZ ACOSTA', 'MANUEL MARTIN # 20 ENTRE FRANCISCO VILLA Y VICENTE GUERRERO, HUAY-PIX, Q.ROO', '9831180277', 'docmsureste@gmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (723, 'PARTICULAR', '806', NULL, 'C. FRANCISCO EMMANUEL MOGUEL CARO', 'RESTAURANTE EL EMPORIO', '9832854373', NULL, 'C. FRANCISCO EMMANUEL MOGUEL CARO',
        'AVENIDA CARMEN OCHOA DE MERINO #106 ENTRE REFORMA Y CALZADA VERACRUZ', '9832854373', 'elemporiochetumal@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (724, 'PARTICULAR', '807', NULL, 'C. DAFNE VIANEY ALMEIDA KUMUL', 'RESTAURANTE EL CAFELITO OBREGON', '9831923125', NULL, 'C. DAFNE VIANEY ALMEIDA KUMUL',
        'AVENIDA ALVARO OBREGON #379-A, COLONIA CENTRO', '9831923125', 'elcafelitoobregon@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (725, 'PARTICULAR', '807', NULL, 'C. DAFNE VIANEY ALMEIDA KUMUL', 'RESTAURANTE EL CAFELITO OBREGON', '9831923125', NULL, 'C. DAFNE VIANEY ALMEIDA KUMUL',
        'AVENIDA ALVARO OBREGON #379-A, COLONIA CENTRO', '9831923125', 'elcafelitoobregon@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (726, 'PARTICULAR', '808', NULL, 'C. DALI GUADALUPE MEDINA VILLANUEVA', 'NIÑO DE ATOCHA (TACOS DE CABEZA)', '9831568765', NULL, 'C. DALI GUADALUPE MEDINA VILLANUEVA',
        'AVENIDA NICOLAS BRAVO, ENTRE CHABLÉ Y DOS AGUADAS, MANZANA 166, LOTE 19', '9831568765', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (727, 'PARTICULAR', '809', NULL, 'C. LAURA ALICIA PENAGOS RAMIREZ', 'ANTOJITOS LAS MARIAS', '9831766490', NULL, 'C. LAURA ALICIA PENAGOS RAMIREZ',
        'AV. BENITO JUAREZ #197-A ENTRE C. COLON Y M. GANDHI, COL. CENTRO', '9831766490', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (728, 'PARTICULAR', '810', NULL, 'C. VIOLETA ANGELICA PAAT NAH', 'ANTOJITOS DOÑA MARCE', '9831103860', NULL, 'C. VIOLETA ANGELICA PAAT NAH',
        'CALLE FELIPE CARRILLO PUERTO #422 ESQ. C.N.C., COL. ADOLFO LOPEZ MATEOS', '9831103860', 'violeta.paat@hotmail.es', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (729, 'PARTICULAR', '811', NULL, 'C. ILSE BERENICE COSSIO LUGO', 'MI SUSHI KIN (ALIMENTOS)', '1442889 /9831442889', NULL, 'C. ILSE BERENICE COSSIO LUGO',
        'AVENIDA CARRANZA CON EMILIANO ZAPATA #241', '1442889 /9831442889', 'mi_sushikin@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (730, 'PARTICULAR', '812', NULL, 'C. MAURO ORTEGA HERNANDEZ', 'TAQUERIA ORTEGA', '9837539107', NULL, 'C. MAURO ORTEGA HERNANDEZ', 'AVENIDA 19 ENTRE 36 Y 38', '9837539107',
        NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (731, 'PARTICULAR', '813', NULL, 'C. LAURIANO SIMA Y CAN', 'FRUTERIA ROSY', '9831841485', NULL, 'C. LAURIANO SIMA Y CAN',
        'AV. BUGAMBILIAS INTERIOR MERCADO ANDRES QUINTANA ROO, COL. JARDINES', '9831841485', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (732, 'PARTICULAR', '814', NULL, 'C. BLANCA JUDITH SIMA POOL', 'COCINA ECONOMICA SHIA', '9831683920', NULL, 'C. BLANCA JUDITH SIMA POOL',
        'AV. BUGAMBILIAS S/N. INTERIOR MERCADO ANDRES QUINTANA ROO, LOCAL # 118', '9831683920', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (733, 'PARTICULAR', '815', NULL, 'C. LUCIO CANTÉ EK', 'DISPENSADOR DE AGUA PURIFICADA EL OASIS', '9831196755', NULL, 'C. LUCIO CANTÉ EK',
        'CALLE MANUEL MA. LOMBARDINI #483, FRACC. BICENTENARIO, LOTE 08, MANZANA 101', '9831196755', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (734, 'PARTICULAR', '816', NULL, 'C. EVANGELINA PAT CANCHE', 'LONCHERIA LA ROSITA', '9831090955', NULL, 'C. EVANGELINA PAT CANCHE',
        'MERCADO IGNACIO MANUEL ALTAMIRANO LOCAL 2 Y 3 COLONIA CENTRO.', '9831090955', 'marcemix_197@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (735, 'PARTICULAR', '817', NULL, 'C. SUEMI KARIME CASTILLO BRICEÑO', 'TORTILLERIA EL TABASQUEÑO', '9831090955', NULL, 'C. SUEMI KARIME CASTILLO BRICEÑO',
        'CALLE RAMON DEL VALLE INCLAN #311 COLONIA MIRAFLORES.', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (736, 'PARTICULAR', '818', NULL, 'C. ALVIN OSEAS GONZALEZ DORANTES', 'TORTILLERIA PLUTARCO', '9831090955', NULL, 'C. ALVIN OSEAS GONZALEZ DORANTES',
        'AVENIDA PLUTARCO ELIAS CALLES # 168 COLONIA CENTRO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (737, 'PARTICULAR', '819', NULL, 'C. KAREN ISABEL HERRERA CASTAÑEDA', 'TORTILLERIA ROJO GOMEZ', '9831090955', NULL, 'C. KAREN ISABEL HERRERA CASTAÑEDA',
        'AV. JAVIER ROJO GOMEZ # 365, MANZANA 191, LOTE 13, COL. PAYO OBISPO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (738, 'PARTICULAR', '820', NULL, 'C. ELIAS ALBERTO CHAVEZ RUESGA', 'ASADERO EL PECOSITO', '9831673940', NULL, 'C. ELIAS ALBERTO CHAVEZ RUESGA',
        'AV. CHETUMAL S/N. ENTRE TRINIDAD Y TOBAGO Y PUERTO RICO, FRACC. FELIX CONZALEZ', '9831673940', 'chavez.ruesga@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (739, 'PARTICULAR', '821', NULL, 'C. AIDA AMELIA SANTELIZ RODRIGUEZ', 'RESTAURANTE FORASTEROS', '8331500/9831035552', NULL, 'C. AIDA AMELIA SANTELIZ RODRIGUEZ',
        'AVENIDA FRANCISCO I. MADERO #318', '8331500/9831035552', 'forasteros1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (740, 'PARTICULAR', '822', NULL, 'C. BIBIANO GOMEZ DIAZ', 'CARNITAS ESTILO MICHOACAN LOS 3 COMPADRES', '9831240988', NULL, 'C. BIBIANO GOMEZ DIAZ',
        'AV. LUIS DONALDO COLOSIO S/N. CARRETERA FEDERAL HUAY PIX, Q.ROO', '9831240988', 'carnitaslostrescompadres@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (741, 'PARTICULAR', '823', NULL, 'C. MARIA DE LOURDES ROMERO DIOSDADO', 'CARNITAS ESTILO MICHOACAN EL COMPADRE', '9831025219', NULL, 'C. MARIA DE LOURDES ROMERO D.',
        'AVENIDA MAXUXAC, LOTE 6, MANZANA 451, COLONIA PROTERRITORIO', '9831025219', 'grsnataly@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (742, 'PARTICULAR', '824', NULL, 'C. NELLY ISABEL DZIB CAUICH', 'PIZZERIA LITTLE CAESARS', '9832088870', NULL, 'C. NELLY ISABEL DZIB CAUICH',
        'AVENIDA INSURGENTES ESQUINA PALERMO #514 COLONIA GONZALO GUERRERO.', '9832088870', 'nellyisabel71@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (743, 'PARTICULAR', '825', NULL, 'C. VANESA NOEMI BAÑOS CHI', 'COCINA ECONOMICA LA OLLA DE ROSY', '0449837506512, 9831454855', NULL, 'C. VANESA NOEMI BAÑOS CHI',
        'CALLE CELUL ESQUINA XTACAY, COL. PAYO OBISPO 2, MANZANA 70, LOTE 16', '0449837506512, 9831454855', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (744, 'PARTICULAR', '826', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.', NULL, '9838375770 EXT 166', NULL, NULL, 'AV. INSURGENTES S/N. KM 5.025, COL. EMANCIPACIÓN DE MÉXICO',
        '9838375770 EXT 166', 'iboch26@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (745, 'PARTICULAR', '827', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.', 'TIENDAS CHEDRAUI S.A. DE C.V.', '9831776614', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.',
        'AVENIDA CONSTITUYENTES #254, COLONIA EL ENCANTO', '9831776614', 'jnorendon18@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (746, 'PARTICULAR', '826', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V .', NULL, '9838380683', NULL, 'TIENDAS CHEDRUI S.A. DE C.V .',
        'AVENIDA INSURGENTES S/N. KM 5.025, COLONIA EMANCIPACION DE MEXICO', '9838380683', 'iboch26@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (747, 'PARTICULAR', '827', NULL, 'TIENDAS CHEDRAUI S.A. DE C.V.', NULL, '9838380683', NULL, NULL, 'AVENIDA INSURGENTES S/N. KM 5.025, COL. EMANCIPACION DE MEXICO',
        '9838380683', 'iboch26@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (748, 'PARTICULAR', '828', NULL, 'C. LAIZA ANGELINA RAMIREZ RUELAS', 'COCINA ECONOMICA DELICIAS', '8333688, 9831554013', NULL, 'C. LAIZA ANGELINA RAMIREZ RUELAS',
        'AV. ERICK PAOLO MARTINEZ, MANZANA 126, LOTE 8, COL. SOLIDARIDAD', '8333688, 9831554013', 'ceag_2005@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (749, 'PARTICULAR', '829', NULL, 'C. JORGE ARMANDO VALENCIA VALLADARES', 'TORTILLERIA EL MICHOACANO', '9831866120', NULL, 'C. JORGE ARMANDO VALENCIA VALLADARES',
        'CALLE 22 ENTRE 7 Y 9, COLONIA CENTRO', '9831866120', 'jorgevalen.89@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (750, 'PARTICULAR', '830', NULL, 'C. JORGE ARMANDO VALENCIA VALLADARES', 'TORTILLERIA EL MICHOACANO', '9831866120', NULL, 'C. JORGE ARMANDO VALENCIA VALLADARES',
        'AVENIDA 19 ENTRE 36 Y 38, COLONIA COLOSIO', '9831866120', 'jorgevalen.89@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (751, 'PARTICULAR', '830', NULL, 'C. HECTOR CELIO SOTRES', 'PANADERIA LA TARTALETA MAHAHUAL', '9831547676', NULL, 'C. HECTOR CELIO SOTRES',
        'CALLE HUACHINANGO S/N. ESQUINA AV. MAHAHUAL, MAHAHUAL, Q.ROO', '9831547676', 'hector-celio@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (752, 'PARTICULAR', '830', NULL, 'C. BELEM CHAN SERVIN', 'PESCADERIA LAS GEMELAS (VENTA DE EMPANADAS)', '9831030952', NULL, 'C. CHAN SERVIN BELEM',
        'CALLE JUANA DE ASBAJE #80 ENTRE ROJO GOMEZ Y CARLOS A. MADRAZO', '9831030952', 'belemchan92@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (753, 'PARTICULAR', '832', NULL, 'ANTOJITOS LAS 3 HERMANAS', 'ANTOJITOS LAS 3 HERMANAS', '9831646453', NULL, 'C. ELIZABETH FLORES GUTIERREZ', 'INSURGENTES # 18',
        '9831646453', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (754, 'PARTICULAR', '832', NULL, 'C. ELIZABETH FLORES GUTIERREZ', 'ANTOJITOS LAS 3 HERMANAS', '9831646453', NULL, 'C. ELIZABETH FLORES GUTIERREZ',
        'INSURGENTES #18 ENTRE DR. JOSE SIURUB Y FLOR DE MAYO, FOVISSSTE 1 ETAPA', '9831646453', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (755, 'PARTICULAR', '834', NULL, 'C. LUIS ENRIQUE RIVERA GONZALEZ', 'RESTAURANTE SPEZIAS PRIME STEAK HOUSE WINE BAR', '9831292774', NULL, 'C. LUIS ENRIQUE RIVERA GONZALEZ',
        'PROLONGACION BOULEVARD BAHIA S/N., EDIFICIO A, LOCAL 1, COL. CENTRO', '9831292774', 'oswaldorivera_17@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (756, 'PARTICULAR', '835', NULL, 'C. AMIR ABRAHAM RIVERA GONZALEZ', 'CAJUN GRILL FAST FOOD', '9838372433', NULL, 'C. AMIR ABRAHAM RIVERA GONZALEZ',
        'AVENIDA INSURGENTES S/N., LOCAL G 11, COLONIA EMANCIPACION', '9838372433', 'cajungrill2014@live.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (757, 'PARTICULAR', '837', NULL, 'C. MANUELA DEL JESUS DIAZ CHUMBA', 'LONCHERIA DOÑA MANUELA', '9831375832', NULL, 'C. MANUELA DEL JESUS DIAZ CHUMBA',
        'CALLE CHERNA S/N., MAHAHUAL, Q. ROO', '9831375832', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (758, 'PARTICULAR', '838', NULL, 'C. BLANCA SOFIA OLGUIN RODRIGUEZ', 'RESTAURANT ITALIAN STEAK HOUSE', '9831302476', NULL, 'C. BLANCA SOFIA OLGUIN RODRIGUEZ',
        'AV. INSURGENTES, KM. 5.025, INT. PLAZA LAS AMÉRICAS, COL. EMANCIPACIÓN', '9831302476', 'sofialguinmerida@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (759, 'PARTICULAR', '838', NULL, 'C. PAULINA QUIROZ ALCANTARA', 'RESTAURAN LOS DE CHETUS', '9831715138', NULL, 'C. PAULINA QUIROZ ALCANTARA',
        'AV. INSURGENTES KM 5.025, COL. EMANCIPACIÓN, INTERIOR PLAZA LAS AMÉRICAS', '9831715138', 'paulinaquirozalcantara23@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (760, 'PARTICULAR', '839', NULL, 'C. SUJEYDY CHULIM MORALES', 'TORTILLERIA YOVANI', '9831385865', NULL, 'C. SUJEYDY CHULIM MORALES',
        'CALLE TIZIMIN # 11 ESQUINA SOCONUZCO Y JALISCO, CALDERITAS, Q. ROO', '9831385865', 'sujeydi89@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (761, 'PARTICULAR', '840', NULL, 'C. LIZBETH RUBI MONTALVO MARTIN', 'TACOS Y TORTAS LULU', '9831467108', NULL, 'C. LIZBETH RUBI MONTALVO MARTIN',
        'CALZADA VERACRUZ #557, COLONIA ADOLFO LOPEZ MATEOS', '9831467108', 'danielgarciamontalvo@hotmail', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (762, 'PARTICULAR', '840', NULL, 'C. SALVATORE TANZELLI', 'RISTORANTE POSITANO', '9831323730', NULL, 'C. SALVATORE TANZELLI',
        'CALLE SICILIA #433, COLONIA ITALIA ENTRE SAN SALVADOR Y CARRANZA', '9831323730', 'stdulcemex@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (763, 'PARTICULAR', '841', NULL, 'C. ANGEL HERNAN BAEZA VIVAS', 'PESCADOS Y MARISCOS LA LUZ DEL DIA', '9831686757', NULL, 'C. ANGEL HERNAN BAEZA VIVAS',
        'AVENIDA BUGAMBILIAS #637 ENTRE MANUEL ACUÑA Y FLOR DE LIZ', '9831686757', 'laluzdeldia79@hotmail', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (764, 'PARTICULAR', '842', NULL, 'C. IRMA UCAN VARGUEZ', 'TORTILLERIA ISRAEL', '9831857955', NULL, 'C. IRMA UCAN VARGUEZ',
        'COLONIA LAZARO CARDENAS, MANZANA 15, LOTE 1, AVENIDA CHETUMAL', '9831857955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (765, 'PARTICULAR', '843', NULL, 'C. GILDARDA HAU MAY', 'LONCHERIA LA TIA', '9831671896', NULL, 'C. GILDARDA HAU MAY',
        'AV. HEROES # 208 TIANGUIS ENTRE CRISTOBAL COLON Y PRIMO DE VERDAD', '9831671896', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (766, 'PARTICULAR', '844', NULL, 'C. SUGEYLI CERVANTES MAY', 'TORTILLERIA YIRETH', '9837335680', NULL, 'C. SUGEYLI CERVANTES MAY', 'CALLE ECOSUR, COLONIA ANTORCHISTA',
        '9837335680', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (767, 'PARTICULAR', '845', NULL, 'C. SUGEYLI CERVANTES MAY', 'TORTILLERIA YAREYMI', '9837335680', NULL, 'C. SUGEYLI CERVANTES MAY',
        'CONSTITUYENTES ESQUINA REFORMA #218, COL. PROTERRITORIO', '9837335680', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (768, 'PARTICULAR', '846', NULL, 'C. MARGARITA GREGORIA SANCHEZ ZUMBARDA', 'COCINA ECONOMICA EL DIVINO NIÑO', '9831664828', NULL, 'C. MARGARITA GREGORIA SANCHEZ ZUMBARDA',
        'AV. HEROES #208, LOCAL 107 Y 108 ENTRE CRISTOBAL COLON Y PRIMO DE VERDAD', '9831664828', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (769, 'PARTICULAR', '847', NULL, 'C. GUSTAVO VAZQUEZ PEREZ', 'POLLOS HECHIZADOS', '9831005928', NULL, 'C. GUSTAVO VAZQUEZ PEREZ',
        'AVENIDA CHETUMAL ESQUINA DOS AGUADAS, MANZANA 169, LOTE 19', '9831005928', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (770, 'PARTICULAR', '848', NULL, 'C. FRANCISCO JAVIER MCLIVERTY PACHECO', 'COCINA ECONOMICA MAC´S', '9838672518', NULL, 'C. FRANCISCO JAVIER MC LIVERTY PACHECO',
        'CARMEN OCHOA DE MERINO #191, COL. PLUTARCO E. CALLES ENTRE AV. DE LOS HEROES', '9838672518', 'pollosmacs@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (771, 'PARTICULAR', '849', NULL, 'C. MARTHA PATRICIA CERVANTES MAYA', 'PIANO BAR FREE HOUSE BEER', '9838316451', NULL, 'C. MARTHA PATRICIA CERVANTES MAYA',
        'AVENIDA OTHON P BLANCO #32, COLONIA BARRIO BRAVO', '9838316451', 'marcerma@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (772, 'PARTICULAR', '850', NULL, 'C. GIBRAM GUILLERMO CASTRO SANTELIZ', 'MARISQUERIA BUCANEROS', '9831196401', NULL, 'C. GIBRAM GUILLERMO CASTRO SANTELIZ',
        'FRANCISCO I. MADERO ESQUINA BUGAMBILIAS # 261, COL. DAVID GUSTAVO GUTIERREZ', '9831196401', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (773, 'PARTICULAR', '850', NULL, 'C. GIBRAM GUILLERMO CASTRO SANTELIZ', 'MARISQUERIA BUCANEROS', '9831196401', NULL, 'C. GIBRAM GUILLERMO CASTRO SANTELIZ',
        'FRANCISCO I. MADERO ESQUINA BUGAMBILIAS, COL. DAVID GUSTAVO GUTIERREZ', '9831196401', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (774, 'PARTICULAR', '851', NULL, 'C. BLANCA ROSA PEREZ MORENO', 'PURIFICADORA PROFE CHUY', '9831642382', NULL, 'C. BLANCA ROSA PEREZ MORENO',
        'CALLE ANTONIO ZACARIAS, POBLADO SERGIO BUTRON CASAS, Q.ROO', '9831642382', 'nenita_dl1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (775, 'PARTICULAR', '852', NULL, 'SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA DEL ESTADO DE Q.ROO', 'PANADERIA LA FAMILIA', '2853923, 9982426604', NULL,
        'SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA DEL ESTADO DE QUINTANA ROO.', 'AV. OTHON P. BLANCO #161-A ENTRE 5 DE MAYO Y 16 DE SEPTIEMBRE', '2853923, 9982426604',
        'panaderiaeldif@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (776, 'PARTICULAR', '853', NULL, 'C. LEONOR ELENA MARZUCA ESQUIVEL', 'RESTAURANT BAR LA CURVA', '9831107930', NULL, 'C. LEONOR ELENA MARZUCA ESQUIVEL',
        'AV. OTHON P. BLANCO #14 ENTRE ESQUINA BOULEVARD BAHIA E ISLA CONTOY', '9831107930', 'hadad07@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (777, 'PARTICULAR', '854', NULL, 'C. LEONOR ELENA MARZUCA ESQUIVEL', 'RESTAURANT-BAR LA CURVA', '9831107930', NULL, 'C. LEONOR ELENA MARZUCA ESQUIVEL',
        'AV. OTHON P. BLANCO #14 ENTRE ISLA CONTOY Y BOULEVARD BAHIA, COL. BARRIO BRAVO', '9831107930', 'hadad07@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (778, 'PARTICULAR', '855', NULL, 'C. MARBELLA PEREZ CRUZ', 'TORTILLERIA MARIA JOSE', '9838313121', NULL, 'C. MARBELLA PEREZ CRUZ',
        'CALLE GABRIEL GUEVARA #65, COLONIA SANTA MARIA', '9838313121', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (779, 'PARTICULAR', '856', NULL, 'C. BEATRIZ EUGENIA FLORES PEREZ', 'LONCHERIA LA OAXAQUEÑA', '9831195707', NULL, 'C. BEATRIZ EUGENIA FLORES PEREZ',
        'AV. MORELOS #133, ESQ. CHAPULTEPEC, COL. CENTRO', '9831195707', 'arturo_gonzalez92@live.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (780, 'PARTICULAR', '857', NULL, 'VILLACRUZ GAVIAA S.A. DE C.V.', 'RESTAURANTE BAR NOHOCH KAY BIG FISH', '9831256610, 9831346190', NULL, 'VILLACRUZ GAVIAA S.A. DE C.V.',
        'AV. MAHAHUAL, MANZANA 20, LOTE 3 ENTRE CAZON Y LIZA, MAHAHUAL Q. ROO', '9831256610, 9831346190', 'and_gc27@live.com.mx', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (781, 'PARTICULAR', '858', NULL, 'C. PETRA FERNANDEZ HERNANDEZ', 'EL ASADERO DE HUAY PIX', '9831393621', NULL, 'C. PETRA FERNANDEZ HERNANDEZ',
        'CARRETERA CHETUMAL-BACALAR KM. 13, AMPLIACIÓN HUAY PIX, Q.ROO', '9831393621', '1620895@UQROO.MX', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (782, 'PARTICULAR', '859', NULL, 'C. PETRA FERNANDEZ HERNANDEZ', 'POLLOS ASADOS EL ASADERO', '9831393621', NULL, 'C. PETRA FERNANDEZ HERNANDEZ',
        'CARRETERA CHETUMAL-BACALAR KM 13 AMPLIACION HUAY-PIX, Q.ROO', '9831393621', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (783, 'PARTICULAR', '861', NULL, 'C. OLIVA ESTHER VARGUEZ CANUL', 'COCINA ECONÓMICA VARGUEZ', '9831233042', NULL, 'C. OLIVIA ESTHER VARGUEZ CANUL',
        'CALLE FRANCISCO I. MADERO, POBLADO NICOLAS BRAVO, Q.ROO', '9831233042', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (784, 'PARTICULAR', '862', NULL, 'C. YOHANA MARGARITA CHAN VARGUEZ', 'AGROVETERINARIA EL HABANERO', '983116015', NULL, 'C. YOHANA MARGARITA CHAN VARGUEZ',
        'CALLE 30 POR 9 Y 11, COLONIA 5 DE MAYO', '983116015', 'gogobasss@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (785, 'PARTICULAR', '863', NULL, 'C. MARIA LEYDI CHAN CAB', 'ANTOJITOS LA GUADALUPANA', '9831836421', NULL, 'C. MARIA LEYDI CHAN CAB',
        'AVENIDA ANDRES QUINTANA ROO #243 ENTRE VENUSTIANO CARRANZA', '9831836421', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (786, 'PARTICULAR', '864', NULL, 'C. ROSA YAZMIN HERNANDEZ CASTILLO', 'ANTOJITOS 2 HERMANOS', '9831119264', NULL, 'C. ROSA YAZMIN HERNANDEZ CASTILLO',
        'AV. LUIS DONALDO COLOSIO, MANZANA 11, LOTE 7, LOCALIDAD HUAY-PIX, Q.ROO', '9831119264', 'pelonvar@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (787, 'PARTICULAR', '865', NULL, 'C. CONSUELO COTO MORENO', 'TORTILLERIA CHELITO II', '9831051226', NULL, 'C. CONSUELO COTO MORENO',
        'CALLE BICENTENARIO ESQUINA PEDRO MARIA ANAYA, COLONIA BICENTENARIO', '9831051226', 'daniel.-rd@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (788, 'PARTICULAR', '866', NULL, 'TTE. COR. M.C. PABLO CESAR BAUTISTA DINA', 'HOSPITAL MILITAR DE ZONA', '9831207315', NULL, 'TTE. COR. M.C. PABLO CESAR BAUTISTA DINA',
        'AVENIDA BOULEVARD BAHIA #418, COLONIA ZONA DE GRANJAS', '9831207315', 'med_prev_mil@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (789, 'PARTICULAR', '867', NULL, 'C. PEDRO MARCIAL RODRIGUEZ COBOS', 'COCTELERIA EL TIO DROPE', '9831048486', NULL, 'C. PEDRO MARCIAL RODRIGUEZ COBOS',
        'CALLE BACALAR, MANZANA 5, LOTE 43, MAHAHUAL, Q. ROO', '9831048486', 'eltiodrope6707@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (790, 'PARTICULAR', '868', NULL, 'C. PETRA FERNANDEZ HERNANDEZ', 'POLLOS ASADOS EL ASADERO DE HUAY-PIX', '9831393621', NULL, 'C. PETRA FERNANDEZ HERNANDEZ',
        'CARRETERA CHETUMAL- BACALAR KM 13, AMPLIACION HUAY-PIX', '9831393621', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (791, 'PARTICULAR', '869', NULL, 'C. EMILIA PAREDES PACHECO', 'RESTAURANTE Y MARISQUERIA KAREN EDITH', '9831324141', NULL, 'C. EMILIA PAREDES PACHECO',
        'KM. 8 CARRETERA CHETUMAL-BACALAR', '9831324141', 'dgga2908@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (792, 'PARTICULAR', '870', NULL, 'C. JOSE SALVADOR FRANCO CORONEL', 'RESTAURANT EL TIBURON', '9831304945', NULL, 'C. JOSE SALVADOR FRANCO CORONEL',
        'AVENIDA VICENTE GUERRERO JUNTO AL RIO RAUDALES, Q.ROO', '9831304945', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (793, 'PARTICULAR', '891', NULL, 'C. MOISES PANTING TREVIÑO', 'DISPENSADOR DE AGUA PURIFICADA AQUADIA', '9831106065', NULL, 'C. MOISES PANTING TREVIÑO',
        'ANDADOR CARLOS PLNK#11, COLONIA SANTA MARIA', '9831106065', 'comerciochetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (794, 'PARTICULAR', '872', NULL, 'C. JOSE ROSENDO CORREA CANTO', 'PIZERIA CHEPY´S PIZZA', '9831040085', NULL, 'C. JOSE ROSENDO CORREA CANTO',
        'CALLE CALZADA VERACRUZ #443 POR MARCIANO GONZALEZ', '9831040085', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (795, 'PARTICULAR', '873', NULL, 'C. FADI AZIZ NEHME', 'RESTAURANTE FADIS GRILL', '9831075572', NULL, 'C. FADI AZIZ NEHME',
        'AVENIDA 4 DE MARZO S/N., LOCAL 20, COLONIA INDUSTRIAL', '9831075572', 'carloscsoto@ccysdespacho.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (796, 'PARTICULAR', '875', NULL, 'C. LOURDES GAHONA', 'RESTAURANT THE ITALIAN COFFEE COMPANY', '9831318317', NULL, 'C. LOURDES GAHONA',
        'PLAZA LAS AMÉRICAS, AV. INSURGENTES KM. 5.025, LOCAL 59, COL. EMANCIPACIÓN', '9831318317', 'italianch_5@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (797, 'PARTICULAR', '876', NULL, 'C. ADELAIDA ALVAREZ PUC', 'PRIMARIA NIÑOS HEROES DE CHAPULTEPEC', '9831540281', NULL, 'C. ADELAIDA ALVAREZ PUC',
        'AVENIDA BUGAMBILIAS CON FLOR DE LIZ', '9831540281', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (798, 'PARTICULAR', '877', NULL, 'C. JOSE FERNANDO MENDEZ CHAN', 'HAMBURGUESAS MENDEZ', '9831414280', NULL, 'C. JOSE FERNANDO MENDEZ CHAN',
        'AV. MAXUXAC ESQUINA ROJO GOMEZ, LOTE 15, MANZANA 5', '9831414280', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (799, 'PARTICULAR', '878', NULL, 'C. NIDIA HORTENCIA ALONZO HOIL', 'CAFETERÍA Y COCINA ECONÓMICA CHETUMAL', '9831293096', NULL, 'C. NIDIA HORTENCIA ALONZO HOIL',
        'AV. INSURGENTES #518 ENTRE PALERMO Y 4 DE MARZO, COL. 20 DE NOVIEMBRE', '9831293096', 'cafeteria.chetu@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (800, 'PARTICULAR', '879', NULL, 'C. AYDEE DEL SOCORRO POTENCIANO PEREZ', 'RESTAURANT BAR SIAN´KAAN', '9831032497', NULL, 'C. AYDEE DEL SOCORRO POTENCIANO PEREZ',
        'CALLE 10 POR 3 Y 5', '9831032497', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (801, 'PARTICULAR', '880', NULL, 'C. MARINA ISOLDA FUENTES SOSA', 'LONCHERIA OTOCH ALUXE', '9831102503', NULL, 'C. MARINA ISOLDA FUENTES SOSA',
        'HEROICA ESCUELA NAVAL #95 ESQUINA CALZADA VERACRUZ', '9831102503', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (802, 'PARTICULAR', '881', NULL, 'C. VILMA CANUL MARIN', 'PANADERIA LOS PICAPIEDRAS', '9831455767', NULL, 'C. VILMA CANUL MARIN',
        'ORQUIDEAS #114 ENTRE FLOR DE LIZ Y AZUCENAS, COLONIA JARDINES', '9831455767', 'samos.picapiedrasjardines@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (803, 'PARTICULAR', '882', NULL, 'C. ALVARO GONZALEZ', 'MARISQUERIA EL COSTEÑITO', NULL, NULL, 'C. ALVARO GONZALEZ', 'CALLE ROJO GOMEZ S/N.', NULL, NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (804, 'PARTICULAR', '883', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE', 'HELADOS Y ALIMENTOS LA DOLCE VITA', '9831036013', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE',
        'AV. MAHAHUAL S/N. LOTE 2, MANZANA 12 ENTRE CORONADO Y MARTILLO, MAHAHUAL, Q.ROO', '9831036013', 'iimm254hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (805, 'PARTICULAR', '884', NULL, 'C. MANUEL JESUS ALCOCER DOMINGUEZ', 'FABRICA DE HIELO EN LAS ROCAS', '9838388962', NULL, 'C. MANUEL JESUS ALCOCER DOMINGUEZ',
        'BENITO JUAREZ 268 Y 282-A, COLONIA DAVID GUSTAVO GUTIERREZ RUIZ', '9838388962', 'rosilia.alcocer@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (806, 'PARTICULAR', '885', NULL, 'C. SOFIA JUAREZ ESCOBEDO', 'ESCUELA PRIMARIA ANDRES QUINTANA ROO', '9838368357', NULL, 'C. SOFIA JUAREZ ESCOBEDO',
        'CARLOS A. VIDAL S/N., FOVISSSTE 3 ETAPA', '9838368357', 'sofia.jua.sje@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (807, 'PARTICULAR', '866', NULL, 'C. ANA LAURA GUEMES CHI', 'TIENDA ESCOLAR JESUS CETINA SALAZAR', '9831089256', NULL, 'C. ANA LAURA GUEMES CHI', 'RAFAEL E MELGAR #56',
        '9831089256', 'anaguemes19@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (808, 'PARTICULAR', '888', NULL, 'C. MARIA ESTHER CHI FLOTA', 'LONCHERIA LOS PRIMOS', '9837333425', NULL, 'C. MARIA ESTHER CHI FLOTA', 'RAFAEL E MELGAR CON ZARAGOZA #342',
        '9837333425', 'shertguemes@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (809, 'PARTICULAR', '889', NULL, 'C. ALMA OSORIO MARTINEZ', 'ABARROTES, POSTRES, PASTAS, ENSALADAS Y SANDWICHONES HERNANDEZ', '9838325709', NULL, NULL,
        'CALLE ISLA CANCUN #189-A ENTRE AVENIDA BELIZE Y COROZAL', '9838325709', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (810, 'PARTICULAR', '890', NULL, 'C. GINER ALVARO LEON', 'LONCHERIA ANTOJITOS DEL BOSQUE', '9838315145', NULL, 'C. GINER ALVARO LEON', 'CALZADA VERACRUZ#672-B',
        '9838315145', 'giner_leon@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (811, 'PARTICULAR', '891', NULL, 'C. LIDIA MERCEDES AGUILAR CAUICH', 'TORTILLERIA EL PASO', '9831863749', NULL, 'C. LIDIA MERCEDES AGUILAR CAUICH',
        'JUAN DE DIOS PEZA #31, COLONIA JARDINES', '9831863749', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (812, 'PARTICULAR', '892', NULL, 'C. CELESTINO MEDRANO GRAJALES', 'BAR LA ESCONDIDA', '9831367376', NULL, 'C. CELESTINO MEDRANO GRAJALES',
        'CALLE # 3, MANZANA 9, LOTE 10, COL. NUEVA ESPERANZA', '9831367376', 'chapyss_5@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (813, 'PARTICULAR', '893', NULL, 'C. HILARIO HUERTA ASCENCION', 'RESTAURANT Y MARISQUERÍA MICHELADAS CHINGONAS', '9837520324', NULL, 'C. HUERTA ASCENCION HILARIO',
        'AV. LUIS DONALDO COLOSIO S/N., MANZANA 9, LOTE 3 , POBLADO HUAY PIX, Q.ROO', '9837520324', 'huertahilario@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (814, 'PARTICULAR', '894', NULL, 'C. MELISA STEFANI TOX CANUL', 'TAQUERIA EL CHINITO', '9831113002, 9831160933', NULL, 'C. MELISA STEFANI TOX CANUL',
        'CALLE 4 DE MARZO #271 ENTRE JUSTO SIERRA Y SAN SALVADOR, COL. 8 DE OCTUBRE', '9831113002, 9831160933', 'taqueria_el_chinito@hotmail.com/melissa_', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (815, 'PARTICULAR', '895', NULL, 'C. MIGUEL ALEJANDRO FUENTES PEREZ', 'MARISQUERIA LOS LIMONCITOS', '9831810796', NULL, 'C. MIGUEL ALEJANDRO FUENTES PEREZ',
        'CALLE JOSEFA ORTIZ DE DOMINGUEZ #125', '9831810796', 'mikealejan639@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (816, 'PARTICULAR', '896', NULL, 'C. SILVIA CANDELARIA MOGUEL BETANCOURT', 'ASADERO DOÑA CANDE', '9831379668', NULL, 'C. SILVIA CANDELARIA MOGUEL BETANCOURT',
        'CALLE 20 DE NOVIEMBRE, MANZANA 13, LOTE 3, POBLADO HUAY PIX, Q. ROO', '9831379668', 'chica1_silvia@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (817, 'PARTICULAR', '896', NULL, 'SANDALS AND S. KIS DE R.L. DE C.V.', 'RESTAURANTE TROPICANTE AMERI-MEX-GRILL', '9831382302', NULL, 'SANDALS AND S. KIS DE R.L. DE C.V.',
        'AVENIDA MAHAHUAL, MANZANA 7, LOTE 8, COLONIA CENTRO, MAHAHUAL, Q. ROO', '9831382302', 'maggiarpa@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (818, 'PARTICULAR', '897', NULL, 'C. MARTINIANO FRANCO CAMBROM', 'CARNITAS ESTILO MICHOACAN', '9831342454', NULL, 'C. MARTINIANO FRANCO CAMBROM',
        'CALZADA VERACRUZ Y BENJAMIN HILL #488 B', '9831342454', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (819, 'PARTICULAR', '898', NULL, 'MERCADO MUNICIPAL ANDRES QUINTANA ROO', NULL, '8375877, 1441914', NULL, 'MERCADO MUNICIPAL ANDRES QUINTANA ROO',
        'CALLE BUGAMBILIAS ENTRE FLOR DE LIZ Y FLOR DE MAYO', '8375877, 1441914', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (820, 'PARTICULAR', '899', NULL, 'C. EDDY MODESTA CANUL MENA', 'ANTOJITOS PAYO OBISPO', '9837325741', NULL, 'C. EDDY MODESTA CANUL MENA',
        'CALLE FAISAN ENTRE CELUL Y MAXUXAC', '9837325741', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (821, 'PARTICULAR', '900', NULL, 'C. DANA MARIA MARRUFO ESPEJO', 'AGUA PURIFICADA CELESTIAL 1', '9831040397', NULL, 'C. DANA MARIA MARRUFO ESPEJO',
        'CALLE LUIS MANUEL SEVILLA SANCHEZ #1076 ENTRE GUYANA Y PARAGUAY, AMERICAS 1', '9831040397', 'gmc311@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (822, 'PARTICULAR', '901', NULL, 'C. EMMA ADELA AREVALO CORTEZ', 'MARISQUERIA EL TACO LOCO', '9831192540, 9831292090', NULL, 'C. EMMA ADELA AREVALO CORTEZ',
        'AV. JOSE MARIA MORELOS #87 ENTRE PLUTARCO E. CALLES E IGNACIO ZARAGOZA', '9831192540, 9831292090', 'tacolocochetumal@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (823, 'PARTICULAR', '902', NULL, 'C. JOSE VICTOR GARCIA GUILHARRY', 'PURIFICADORA NATURA´L', '9831207620', NULL, 'C. JOSE VICTOR GARCIA GUILHARRY',
        'AVENIDA MACHUXAC #216, MANZANA 237,LOTE 02', '9831207620', 'purificadora_natura@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (824, 'PARTICULAR', '902', NULL, 'C. REYNA ESTHER COOL CANUL', 'LONCHERIA LA REYNA', '9831150434, 1180434', NULL, 'C. REYNA ESTHER COOL CANUL',
        'CALLE RIO VERDE, MANZANA 250, LOTE 12, COLONIA PROTERRITORIO', '9831150434, 1180434', 'aaronde12@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (825, 'PARTICULAR', '903', NULL, 'GRUPO OPERACIONES UNIDAS SAPI DE C.V.', 'RESTAURANT SENSEI SUSHI', '9831762721', NULL, 'GRUPO OPERACIONES UNIDAS SAPI DE C.V.',
        'AVENIDA ERICK PAOLO MARTINEZ Y AVENIDA 4 DE MARZO', '9831762721', 'operacionesunidas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (826, 'PARTICULAR', '904', NULL, 'C. SERGIO GAMIÑO VILLA', 'POLLOS ASADOS DON SERGIO', '9831218321', NULL, 'C. SERGIO GAMIÑO VILLA',
        'AVENIDA NAPOLES #321 ESQUINA BUGAMBILIAS', '9831218321', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (827, 'PARTICULAR', '905', NULL, 'C. MARIA JUANA CAAMAL CHUC', 'LONCHERIA JUANITA', '9831012471', NULL, 'C. MARIA JUANA CAAMAL CHUC',
        'CALZADA VERACRUZ ENTRE SEGUNDO CIRCUITO, LOCAL 31-32-33', '9831012471', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (828, 'PARTICULAR', '906', NULL, 'C. FERNANDO DIDIER MARTINEZ VALENCIA', 'MARISQUERIA ISLA TORTUGA', '9831116221', NULL, 'C. FERNANDO DIDIER MARTINEZ VALENCIA',
        'AVENIDA FRANCISCO I. MADERO #100-A LOCAL 1, COLONIA CENTRO', '9831116221', 'ferchovalencia19@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (829, 'PARTICULAR', '907', NULL, 'C. MIGUEL ARCOS PEREZ', 'LONCHERIA TACOLANDIA', '9831041653', NULL, 'C. MIGUEL ARCOS PEREZ',
        'AV. CONSTITUYENTES ENTRE RAMONAL Y CELUL, COL. PROTERRITORIO', '9831041653', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (830, 'PARTICULAR', '908', NULL, 'C. GUSTAVO GARCIA BRADLEY', 'CASA HABITACIÓN', '9831548946', NULL, 'C. GUSTAVO GARCIA BRADLEY', 'DOMICILIO CONOCIDO CHETUMAL',
        '9831548946', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (831, 'PARTICULAR', '905', NULL, 'C. EDUARDO FELIPE PERAZA MCLIBERTY', 'BUBBLE WAFFLE HAIKO', '9831655804', NULL, 'C. EDUARDO FELIPE PERAZA MCLIBERTY',
        'CALLE 22 DE ENERO #161, COLONIA PLUTARCO E CALLES', '9831655804', 'ed_mliberty@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (832, 'PARTICULAR', '909', NULL, 'C. SILVIA MORALES MORALES', 'TORTILLERIA YAZMIN', '9831090955', NULL, 'C. SILVIA MORALES MORALES', 'CLAVELES #112, COLONIA JARDINES',
        '9831090955', 'marcemix_1476@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (833, 'PARTICULAR', '906', NULL, 'C. JOSE JUAN CUELLAR SAMOS', 'ABARROTES, POLLERIA Y FRUTERIA EL MANA', '9831090955', NULL, 'C. JOSE JUAN CUELLAR SAMOS',
        'CLAVELES #112, COLONIA JARDINES', '9831090955', 'marcemix_1476@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (834, 'PARTICULAR', '910', NULL, 'C. FABIOLA CONCEPCION LOPEZ ROMERO', 'HELADOS EL TESORO DEL PIRATA', '9992430506', NULL, 'C. FABIOLA CONCEPCION LOPEZ ROMERO',
        'AV. BOULEVARD BAHIA #130-A ENTRE HEROES DE CHAPULTEPEC Y LAZARO CARDENAS', '9992430506', 'fabiolaclopezr86@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (835, 'PARTICULAR', '911', NULL, 'C. AGUEDA EUNICE ESQUIVEL GONZALEZ', 'TORTILLERIA CHETUMAIZ', '9831213144', NULL, 'C. AGUEDA EUNICE ESQUIVEL GONZALEZ',
        'CALLE RIO VERDE LOTE 1, MANZANA 233, COLONIA SOLIDARIDAD', '9831213144', 'agueda2380@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (836, 'PARTICULAR', '911', NULL, 'C. AGUEDA EUNICE ESQUIVEL GONZALEZ', 'TORTILLERIA CHETUMAIZ', '9831213144', NULL, 'C. AGUEDA EUNICE ESQUIVEL GONZALEZ',
        'CALLE RIO VERDE, LOTE 1, MANZANA 233, COL. SOLIDARIDAD', '9831213144', 'aguedaz2380@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (837, 'PARTICULAR', '912', NULL, 'OPERADORA VIPS S. DE R.L. DE C.V.', 'RESTAURANTE EL PORTON', '9831044256', NULL, 'OPERADORA VIPS S. DE R.L. DE C.V.',
        'BOULEVARD BAHIA #301, COLONIA CENTRO', '9831044256', 'porton81568@porton.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (838, 'PARTICULAR', '921', NULL, 'C. MARIA MERCEDES MARTIN DOMINGUEZ', 'COCINA ECONÓMICA ANDREA', '9831681113', NULL, 'C. MARIA MERCEDES MARTIN DOMINGUEZ',
        'AV. CONSTITUYENTES DEL 74, LOTE 12, MANZANA 286, ENTRE REFORMA Y RAMONAL', '9831681113', 'elizab_c_m@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (839, 'PARTICULAR', '922', NULL, 'C. EDITH BEATRIZ MAY PECH', 'TAQUERIA BETY', '9831302476', NULL, 'C. EDITH BEATRIZ MAY PECH',
        'AVENIDA CARMEN OCHOA DE MERINO N° 226, COLONIA CENTRO', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (840, 'PARTICULAR', '906', NULL, 'C. MARIA HILDA MARTINEZ POOT', 'ANTOJITOS YUCATECOS', '9831305875', NULL, 'C. MARIA HILDA MARTINEZ POOT', 'FLOR DE LIZ # 656',
        '9831305875', 'rivero..cristian,cbtis253@gmail', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (841, 'PARTICULAR', '923', NULL, 'C. NELY KARINA MEDINA AGUILAR', 'COCINA ECONOMICA LA ECONOMICA', '9831838638', NULL, 'C. NELY KARINA MEDINA AGUILAR',
        'AVENIDA CHETUMAL N°895 FRACCIONAMIENTO CARIBE', '9831838638', 'ratmyarisol@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (842, 'PARTICULAR', '913', NULL, 'C. MARIA LUISA CASTAÑEDA GALERA', 'PURIFICADORA MANATI EXPRESS', '9831228656', NULL, 'C. MARIA LUISA CASTAÑEDA GALERA',
        'AVENIDA 19 ENTRE 26 Y 28', '9831228656', 'luisa_88c@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (843, 'PARTICULAR', '914', NULL, 'C. MARIA LUISA CASTAÑEDA GALERA', 'PURIFICADORA MANATI EXPRESS II', '983122465', NULL, 'C. MARIA LUISA CASTAÑEDA GALER',
        'AVENIDA 19 ENTRE 12 Y 14', '983122465', 'luisa_88@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (844, 'PARTICULAR', '915', NULL, 'C. JOCABET HERNANDEZ LOPEZ', 'ANTOJITOS MELANIE', '983152725', NULL, 'C.JOCABET HERNANDEZ LOPEZ', 'POBLADO NICOLAS BRAVO, Q.ROO',
        '983152725', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (845, 'PARTICULAR', '924', NULL, 'C. ANTONIO GARCIA CASTRO', 'LONCHERIA CAFE COLONIAL', '9831373705', NULL, 'C. ANTONIO GARCIA CASTRO',
        'CALLE SIERRA, LOCAL 3 S/N. ENTRE HUACHINANGO, COL. CENTRO, MAHAHUAL, Q.ROO', '9831373705', 'antoniogrc768@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (846, 'PARTICULAR', '1', NULL, 'C. CANDELARIA ISABEL OSORIO ZAPATA', 'TAQUERIA POC CHUC 1', '9832855957', NULL, 'C. CANDELARIA ISABEL OSORIO ZAPATA',
        'CALZADA VERACRUZ # 252 ENTRE MARCIANO GONZALEZ E INSURGENTES', '9832855957', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (847, 'PARTICULAR', '2', NULL, 'C. ALEXIS ALBERTO DE LA CRUZ MORALES', 'PASTELERIA Y DELICATESES CANDY', '9831669922', NULL, 'C. ALEXIS ALBERTO DE LA CRUZ MORALES',
        'AV. JAVIER ROJO GOMEZ, MANZANA 34, LOTE 18, COL. PAYO OBISPO', '9831669922', 'car_cielo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (848, 'PARTICULAR', '3', NULL, 'C. SOCORRO RODRIGUEZ SONORA', 'ANTOJITOS VERACRUZANOS DOÑA SOCO', '9831392354', NULL, 'C. SOCORRO RODRIGUEZ SONORA',
        'AVENIDA HEROES #365 ENTRE ISLA CANCUN Y LUIS CABRERA', '9831392354', 'new_rafaelnovelo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (849, 'PARTICULAR', '5', NULL, 'C. YAMIRA SOLEDAD KU VAZQUEZ', 'RESTAURANT EL RINCON ANDALUZ', '9831217263', NULL, 'C. KU VAZQUEZ YAMIRA SOLEDAD',
        'AVENIDA IGNACIO ZARAGOZA #321-A', '9831217263', 'kodashy_13@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (850, 'PARTICULAR', '4', NULL, 'C. BEYRA CARMINA AGUIRRE VILLEGAS', 'POLLO BRUJO', '8374747, 8375246', NULL, 'C. BEYRA CARMINA AGUIRRE VILLEGAS',
        'CALLE CAMELIAS #425 ESQ. NAPOLES CON JUSTO SIERRA, COL. JOSEFA ORTIZ DE D.', '8374747, 8375246', 'chetumal@pollobrujo.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (851, 'PARTICULAR', '5', NULL, 'C. FABIAN AGUIRRE LUNA', 'POLLO BRUJO', '8322713', NULL, 'C. FABIAN AGUIRRE LUNA',
        'AVENIDA ALVARO OBREGON #208-A ENTRE HEROES Y JUAREZ, COL. CENTRO', '8322713', 'chetumal@pollobrujo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (852, 'PARTICULAR', '6', NULL, 'C. RITA NOVELO CIME', 'ANTOJITOS DOÑA GORDIS', '9831213800/9831151230', NULL, 'C. RITA NOVELO CIME',
        'AVENIDA JUSTO SIERRA #588, ENTRE DONATO GUERRA Y ROJO GOMEZ.', '9831213800/9831151230', 'rita@hotmail.com / balam_16@hotmail.es', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (853, 'PARTICULAR', '7', NULL, 'C. SINDY YAMILY CORAL LANZ', 'ASADERO LAS GUERITAS', '9831118856', NULL, 'C. SINDY YAMILY CORAL LANZ',
        'CALLE MAXUXAC #29-B ESQUINA 30 DE NOVIEMBRE', '9831118856', 'sindycoral@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (854, 'PARTICULAR', '8', NULL, 'C. ANA BERTHA RAMON PACHECO', 'COCINA ECONOMICA LA RANITA', '9831677275', NULL, 'C. ANA BERTHA RAMON PACHECO',
        'CALLE CAMELIAS #305 ESQUINA PABLO GONZALEZ', '9831677275', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (855, 'PARTICULAR', '9', NULL, 'C. MIGUEL ANGEL GARCIA HERRERA', 'BABUTE BAR', '983156294', NULL, 'C. MIGUEL ANGEL GARCIA HERRERA',
        'AVENIDA HEROES #33-B ENTRE CARMEN OCHOA Y OTHON P BLANCO', '983156294', 'trazy-gh@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (856, 'PARTICULAR', '10', NULL, 'C. LETICIA RODRIGUEZ ALONSO', 'LONCHERIA LA CHACHI', '9831022900', NULL, 'C. LETICIA RODRIGUEZ ALONSO',
        'AVENIDA NAPOLES #318 ENTRE BUGAMBILIAS Y JUSTO SIERRA', '9831022900', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (857, 'PARTICULAR', '11', NULL, 'C. JOSE ALBERTO OSORIO ZAPATA', 'TAQUERIAS POC CHUC 2', '9831675013', NULL, 'C. JOSE ALBERTO OSORIO ZAPATA',
        'CALZADA VERACRUZ #473-A ENTRE BENJAMIN HILL Y VALENTIN GOMEZ FARIAS', '9831675013', 'taqueriaspoc-chuc@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (858, 'PARTICULAR', '12', NULL, 'C. JUANA ARAGON GUZMAN', 'RESTAURANTE BAR LOS 6 HERMANOS', '9831304828', NULL, 'C. JUANA ARAGON GUZMAN',
        'AV. COSTERA CON CALLE 28, COL. MARIO VILLANUEVA', '9831304828', 'vagadonnaego@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (859, 'PARTICULAR', '13', NULL, 'C. GLORIA LARA ALMENDRA', 'CAFETERIA ESCOLAR CBTIS # 253', '9831450505', NULL, 'C. GLORIA LARA ALMENDRA',
        'TRES GARANTÍAS ENTRE SACXAN Y NICOLAS BRAVO S/N., COL. SOLIDARIDAD', '9831450505', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (860, 'PARTICULAR', '14', NULL, 'C. JOEL ABRAHAM ANCONA PECH', 'DON PAY', '9831833319', NULL, 'C. JOEL ABRAHAM ANCONA PECH',
        'INTERIOR DE LA UNIVERSIDAD DE QUINTAN ROO, COLONIA DEL BOSQUE', '9831833319', 'yoe1508@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (861, 'PARTICULAR', '15', NULL, 'C. FLOR LETICIA TZEC MEX', 'RESTAURANTE LA TERRAZA DE TITO', '8324717', NULL, 'C. FLOR LETICIA TZEC MEX',
        'SALVADOR ALVARADO #276 ENTRE LUCIO BLANCO Y BELISARIO DOMINGUEZ', '8324717', 'terraza1@live.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (862, 'PARTICULAR', '16', NULL, 'C. TEODOCIO CAAMAL PECH', 'RESTAURANTE LA TERRAZA DE TITO', '2853668', NULL, 'C. TEODOCIO CAAMAL PECH',
        'BOULEVARD BAHIA #91, MANZANA 1, LOTE 10, COLONIA BARRIO BRAVO', '2853668', 'terraza1@live.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (863, 'PARTICULAR', '17', NULL, 'C. SANTIAGO AGUIRRE CANTO', 'RESTAURANTE POLLO BRUJO', '9831404050', NULL, 'C. SANTIAGO AGUIRRE CANTO',
        'AV. MAXUXAC, MANZANA 380, LOTE 7 ENTRE POLYUC Y PETCACAB', '9831404050', 'carlosaguirre@pollobrujo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (864, 'PARTICULAR', '914', NULL, 'C. MARTINA DE LOS ANGELES CASTILLO BARRERA', 'COKTELES Y CEVICHES QUIRINO', '9831309903', NULL,
        'C. MARTINA DE LOS ANGELES CASTILLO BARRERA', 'AVENIDA CARRANZA ESQUINA HIDALGO N°248', '9831309903', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (865, 'PARTICULAR', '915', NULL, 'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'TORTILLERIAS TOSTIMAYA SUCURSAL CENTENARIO', '9831079347', NULL,
        'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'AV. MARIANO ANGULO BASTO #267, LOCAL # 4, LOTE 29, MANZANA 101, COL. CENTENARIO', '9831079347', 'tostimaya@hotmail.com',
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (866, 'PARTICULAR', '916', NULL, 'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'TORTILLERIAS TOSTIMAYA MATRIZ', '9831079347', NULL,
        'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'CALLE LAGUNA DE BACALAR #410 ESQ. GENOVA, COL. BENITO JUAREZ', '9831079347', 'tostimaya@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (867, 'PARTICULAR', '917', NULL, 'C. MARIA DEL CARMEN MARTIN BARBOSA', 'TORTILLERIA LAS MEXICANISIMAS 2', '9831079347', NULL, 'C. MARIA DEL CARMEN MARTIN BARBOSA',
        'AV. HÉROES #207 ENTRE CRISTOBAL COLON Y PRIMO DE VERDAD, COL. CENTRO', '9831079347', 'tostimaya@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (868, 'PARTICULAR', '918', NULL, 'C. MARIA DEL CARMEN MARTIN BARBOSA', 'TORTILLERIA LAS MEXICANISIMAS', '9831079347', NULL, 'C. MARIA DEL CARMEN MARTIN BARBOSA',
        'AV. CALZADA VERACRUZ #405 ENTRE LUIS CABRERA Y C.N.C. COL. ADOLFO LOPEZ MATEOS', '9831079347', 'tostimaya@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (869, 'PARTICULAR', '18', NULL, 'C. TERESA NATALY HERNANDEZ BAENA', 'TORTILLERIA ANDREA', '9831403200 9837520343', NULL, 'C. TERESA NATALY HERNANDEZ BAENA',
        'ALTOS DE SEVILLA ENTRE NICOLAS BRAVO y POLYUK, MANZANA 225, LOTE 15', '9831403200 9837520343', 'natylinda_ali@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (870, 'PARTICULAR', '19', NULL, 'C. MARICELA ROQUE PRIETO', 'TORTILLERIA YARETZY', '9838368562', NULL, 'C. MARICELA ROQUE PRIETO',
        'AVENIDA CHETUMAL ESQUINA GRANADA #906 FRACCIONAMIENTO CARIBE', '9838368562', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (871, 'PARTICULAR', '20', NULL, 'C. DAVID ARGUELLEZ ESPINO', 'TORTILLERIA BHASMAN', '9831680769', NULL, 'C. DAVID ARGUELLEZ ESPINO',
        'AVENIDA ROJO GOMEZ, MANZANA 45, LOTE 7 ENTRE CELUL Y RETORNO 27', '9831680769', 'tito.panda@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (872, 'PARTICULAR', '919', NULL, 'SUBTTE. AUX. M.C. RICARDO DANIEL TLAHUITZO JUAREZ', 'PELOTÓN DE SANIDAD 7/o REGIMIENTO CABALLERIA MOTORIZADO', '2411314462', NULL,
        'SUBTTE. AUX. M.C. RICARDO DANIEL TLAHUITZO JUAREZ', 'AVENIDA EFRAIN AGUILAR S/N. COLONIA CENTRO', '2411314462', 'r_jurez@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (873, 'PARTICULAR', '21', NULL, 'C. RICARDO AMARO ESCALANTE', 'CENTRO DE LLENADO AQUACLIVA PROTERRITORIO', '9831028375', NULL, 'C. RICARDO AMARO ESCALANTE',
        'CALLE GRACIANO SANCHEZ, MANZANA 385, LOTE 23, COL. PROTERRITORIO', '9831028375', 'gabriel010480@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (874, 'INSTITUCIONAL', '925', NULL, 'JEFE DE LA JURISDICCIÓN SANITARIA # 2', 'DR. HOMERO LEON PEREZ', '9988886640 Y 9988886641', NULL, 'DR. HOMERO LEON PEREZ',
        'CALLE 35 Y MIGUEL HIDALGO, REGIÓN 93', '9988886640 Y 9988886641', NULL, NULL, NULL, 'Quintana Roo', 'BENITO JUAREZ', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (875, 'PARTICULAR', '22', NULL, 'PAVIA HERMANOS Y ASOCIADOS S.A. DE C.V.', 'HOTEL HACIENDA CAMPESTRE', '8330288', NULL, 'PAVIA HERMANOS Y ASOCIADOS S.A. DE C.V.',
        'AVENIDA CENTENARIO #680-A ENTRE CEDRO Y CIRICOTE, COL. DEL BOSQUE', '8330288', 'info@haciendacampestre.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (876, 'PARTICULAR', '23', NULL, 'C. BRENDA VERONICA CARVALLO GUZMAN', 'PURIFICADORA LA GOTA', '9831255505', NULL, 'C. BRENDA VERONICA CARVALLO GUZMAN',
        'CALLE SUBTENIENTE LOPEZ, MANZANA 139, LOTE 23, COL. PROTERRITORIO', '9831255505', 'verok2408@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (877, 'PARTICULAR', '24', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO', 'DISPENSADOR LA GOTA CRISTALINA', '9838390722', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO',
        'AVENIDA ISAAC MEDINA #230 ESQUINA CALLE G, COLONIA FORJADORES', '9838390722', 'impregrafic1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (878, 'PARTICULAR', '25', NULL, 'C. LIZBETH GRISELL GARCIA ROSADO', 'LA GOTA CRISTALINA', '9838390722', NULL, 'C. LIZBETH GRISEL GARCIA ROSADO',
        'AVENIDA CHETUMAL #840 POR HAITI FRACCIONAMIENTO CARIBE', '9838390722', 'impregrafic1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (879, 'PARTICULAR', '26', NULL, 'C. LIZBETH GRISELL GARCIA ROSADO', 'DISPENSADOR LA GOTA CRISTALINA', '9838390722', NULL, 'C. LIZBETH GRISELL GARCIA ROSADO',
        'AV. FLAMBOYANES ESQUINA HUAYACAN, FRACC. ARBOLEDAS', '9838390722', 'impregrafic1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (880, 'PARTICULAR', '27', NULL, 'C. LEONILA PEREZ ESCOBAR', 'ABARROTES FRUTAS Y VERDURAS FERNANDO', '1270448', NULL, 'C. LEONILA PEREZ ESCOBAR',
        'JAVIER ROJO GOMEZ, LOTE 6, MANZANA 58, COLONIA PAYO OBISPO', '1270448', 'frut-fer@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (881, 'PARTICULAR', '27', NULL, 'C. LEONILA PEREZ ESCOBAR', 'ABARROTES FRUTAS Y VERDURAS FERNANDO', '1270448', NULL, 'C. LEONILA PEREZ ESCOBAR',
        'JAVIER ROJO GOMEZ, LOTE 6, MANZANA 38, COLONIA PAYO OBISPO', '1270448', 'frut-fer@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (882, 'PARTICULAR', '28', NULL, 'C. ALBERTO RIOS TOVAR', 'CAFETERIA ITALIAN COFFEE', '9831521255', NULL, 'C. ALBERTO RIOS TOVAR',
        'BOULEVARD BAHIA S/N, ESQUINA INSURGENTES, LOTE 200, MANZANA 1', '9831521255', 'vander_sar@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (883, 'PARTICULAR', '29', NULL, 'C. RAUL SERGIO MORELOS MARTINEZ', 'HAMBURGUESAS NORTEÑAS AL CARBON Y HOT-DOG', '9831310073', NULL, 'C. RAUL SERGIO MORELOS MARTINEZ',
        'AVENIDA MAXUXAC, MANZANA 243, LOTE 2', '9831310073', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (884, 'PARTICULAR', '30', NULL, 'C. MARIA EUGENIA ISLAS SANDOVAL', 'COCINA ECONOMICA LOS 2 PORTALES', '9831656318/2722619290', NULL, 'C. MARIA EUGENIA ISLAS SANDOVAL',
        'CALLE FAISAN ESQUINA MAXUXAC, MANZANA 32, LOTE 1, COL. PAYO OBISPO II', '9831656318/2722619290', 'ma-eugenia-islas@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (885, 'PARTICULAR', '31', NULL, 'C. ELVIA CRUZ MOLLINEDO', 'RESTAURANTE JAPONES QUINTANA ROLL', '9831521255', NULL, 'C. ELVIA CRUZ MOLLINEDO',
        'BOULEVARD BAHIA S/N., ESQUINA INSURGENTES', '9831521255', 'quintana roll2017@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (886, 'PARTICULAR', '30', NULL, 'C. GLADIS ANGELICA CASTRO TEH', 'DISPENSADOR DE AGUA PURIFICADA LA REYNA', '9831779855', NULL, 'C. GLADIS ANGELICA CASTRO TEH',
        'CALLE SAN VICENTE #233, ESQUINA NARANJAL, FRACCIONAMIENTO CARIBE', '9831779855', 'gladisangelica@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (887, 'PARTICULAR', '31', NULL, 'C. ADRIANA HERNANDEZ M', 'TAQUERIA LA FERIA DEL SABOR', '9831167007', NULL, 'C. ADRIANA HERNANDEZ M', 'ANDRES QUINTANA ROO', '9831167007',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (888, 'PARTICULAR', '30', NULL, 'C. ADRIANA HERNANDEZ MIRANDA', 'TAQUERIA LA FERIA DEL SABOR', '9831167007', NULL, 'C. ADRIANA HERNANDEZ MIRANDA',
        'ANDRES QUINTANA ROO # 351', '9831167007', 'adrix.hdez.mde.@hotmail', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (889, 'PARTICULAR', '32', NULL, 'C. LILIA CHAGALA CONDE', 'LONCHERIA Y REFRESCOS LA VERACRUZANA', '9838335637', NULL, 'C. LILIA CHAGALA CONDE',
        'PLUTARCO E. CALLES #133 ENTRE REFORMA E HIDALGO', '9838335637', 'liliachagala@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (890, 'PARTICULAR', '33', NULL, 'C. LEYDI RUIZ CAAMAL', 'TORTILLERIA Y TENDEJON CHOKOJ HUA', '9831580473', NULL, 'C. LEYDI RUIZ CAAMAL',
        'CALLE SANTIAGO DE CHILE #505, COLONIA LAS AMERICAS III', '9831580473', 'leydi_ruizcaamal@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (891, 'PARTICULAR', '34', NULL, 'C. CLAUDIA DEL CARMEN CAJUN ANCHEVIDO', 'ASADERO EL POLLO', '9831127692', NULL, 'C. CLAUDIA DEL CARMEN CAJUN ANCHEVIDO',
        'AVENIDA 19 ENTRE 38 Y 40, COLONIA 5 DE MAYO', '9831127692', 'pollosinaloa@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (892, 'PARTICULAR', '35', NULL, 'C. GLORIA DEL CARMEN SILVIA RODRIGUEZ', 'ESTANCIA INFANTIL CAMPESTRE', '98314040046 1270378', NULL, 'C. GLORIA DEL CARMEN SILVIA RODRIGUEZ',
        'JUAN JOSE SIORDIA #266, COLONIA DAVID GUSTAVO GUTIERREZ', '98314040046 1270378', 'glosiro@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (893, 'PARTICULAR', '36', NULL, 'C. ISELA MARIA SCHIAPPAPIETRA PAEZ', 'PIZZA METRO', '9831372552/9831057323', NULL, 'C. ISELA MARIA SCHIAPPAPIETRA PAEZ',
        'PASEO DEL PUERTO #1031, MAHAHUAL, Q. ROO', '9831372552/9831057323', 'isch001@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (894, 'PARTICULAR', '37', NULL, 'C. NOELIA SHOEMI ANCIBAR GOMEZ', 'RESTAURANTE ÑAM ÑAM', '9831314417/9831372552/9831057323', NULL, 'C. NOELIA SHOEMI ANCIBAR GOMEZ',
        'CALLE CHERNA S/N., LOTE 18, MANZANA 9, MAHAHUAL, Q. ROO', '9831314417/9831372552/9831057323', 'noeliancibar@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (895, 'PARTICULAR', '38', NULL, 'C. LEONEL DE JESUS CHAN BUENFIL', 'COCO BAR', '9831027586', NULL, 'C. LEONEL DE JESUS CHAN BUENFIL',
        'BOULEVARD BAHIA ESQUINA OTHON P. BLANCO # 46', '9831027586', 'leodislite@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (896, 'PARTICULAR', '39', NULL, 'C. FERMINA SANTIAGO GONZALEZ', 'LONCHERIA FERMINA', '9838381739, 9831077703', NULL, 'C. FERMINA SANTIAGO GONZALEZ',
        'COLONIA CENTRO, JUAREZ CON CRISTOBAL COLON Y BELICE', '9838381739, 9831077703', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (897, 'PARTICULAR', '40', NULL, 'C. EDER MEMIJE LOZANO', 'TORTAS Y TACOS EDER', '8373717', NULL, 'C. EDER MEMIJE LOZANO',
        'CALLE CHABLE, LOTE 4, MANZANA 49, COLONIA PAYO OBISPO 2', '8373717', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (898, 'PARTICULAR', '41', NULL, 'C. SANDRA AIDE REY SOTO', 'HOTEL VILLA DIAMANTE', '9831145786', NULL, 'C. SANDRA AIDE REY SOTO', 'CARRETERA CHETUMAL-BACALAR KM 7.5',
        '9831145786', 'villa.diamante@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (899, 'PARTICULAR', '42', NULL, 'C. ALEJANDRO MARTINEZ RENDON', 'RESTAURANTE Y MARISQUERIA CHETUMALITO', '9831675482', NULL, 'C. ALEJANDRO MARTINEZ RENDON',
        'AVENIDA UNIVERSIDAD #44', '9831675482', 'radiadores_martinez@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (900, 'PARTICULAR', '43', NULL, 'C. ALEJANDRO MARTINEZ RENDON', 'RESTAURANTE Y MARISQUERIA CHETUMALITO', '9831675482', NULL, 'C. ALEJANDRO MARTINEZ RENDON',
        'AVENIDA UNIVERSIDAD #44', '9831675482', 'radiadores_martinez@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (901, 'PARTICULAR', '43', NULL, 'C. SILVIA G. HUITZ ROCA', 'PANADERIA PAN DE CADA DIA', '9831384598', NULL, 'C. SILVIA G. HUITZ ROCA',
        'COLONIA FORJADORES, MANZANA 18, LOTE 13', '9831384598', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (902, 'PARTICULAR', '44', NULL, 'C. MARIA ENRIQUETA GUADARRAMA GONZALEZ', 'PASTELERIA MARILUI', '8321242/9831255946', NULL, 'C. ENRIQUETA GUADARRAMA GONZALEZ',
        'AVENIDA JUAREZ #195, COLONIA CENTRO', '8321242/9831255946', 'marilui39@hotmail.com/facturaelectronico', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (903, 'PARTICULAR', '46', NULL, 'C. ENRIQUETA GUADARRAMA GONZALEZ', NULL, '9831441188', NULL, 'C. ENRIQUETA GUADARRAMA GONZALEZ',
        'CALLE XIABRE, MANZANA 760, LOTE 22, FRACCIONAMIENTO ANDARÁ', '9831441188', 'marilui39@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (904, 'PARTICULAR', '926', NULL, 'C. KARINA DEL SOCORRO SANTOS AZUETA', 'FUMIGACIONES CENTENARIO', '9831067097', NULL, 'C. KARINA DEL SOCORRO SANTOS AZUETA',
        'CALLE 93 S/N. POR 78 Y 80, COLONIA EMILIANO ZAPATA 2', '9831067097', 'terangel9994@hotmail.com', NULL, NULL, 'Quintana Roo', 'FELIPE CARRILLO PUERTO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (905, 'PARTICULAR', '928', NULL, 'C. GUADALUPE ARANDA ROMERO', NULL, '9841695779', NULL, NULL, 'FRANCISCO J. MUJICA ENTRE BENJAMIN HILL Y RAMON CORONA #804', '9841695779',
        'asist_germ@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (906, 'PARTICULAR', '48', NULL, 'C. RODRIGO SANTOS GONZALEZ', 'DC SUPER BURGER', '98324843, 9831041469', NULL, 'C. RODRIGO SANTOS GONZALEZ',
        'ERICK PAOLO MARTINEZ ENTRE NICOLAS BRAVO Y POLYUC #367', '98324843, 9831041469', 'bataco_182@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (907, 'PARTICULAR', '46', NULL, 'C. HECTOR MIGUEL YAH MAY', 'PURIFICADORA SPLASH', '9831026103', NULL, 'C. HECTOR MIGUEL YAH MAY',
        'BUGAMBILIAS #571-B ESQUINA GABRIEL LEYVA VELAZQUEZ', '9831026103', 'miguelsteviamay@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (908, 'PARTICULAR', '46', NULL, 'C. HECTOR MIGUEL YAH MAY', 'PURIFICADORA SPLASH', '9831026103', NULL, 'C. HECTOR MIGUEL YAH MAY',
        'BUGAMBILIAS #571-B ESQUINA GABRIEL LEYVA VELAZQUEZ', '9831026103', 'miguelsteviamay@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (909, 'PARTICULAR', '48', NULL, 'C. NALLELY GUADALUPE GOMEZ VILLAMONTE', 'PURIFICADORA SPLASH', '983522499', NULL, 'C. NALLELY GUADALUPE GOMEZ VILLAMONTE',
        'BUGAMBILIAS #322-A ESQUINA VENECIA', '983522499', 'oroguita31@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (910, 'PARTICULAR', '49', NULL, 'C. DINA MERCEDES PACHECO BAZAN', 'FUMIGACIONES EL CAMPO', '8323307', NULL, 'C. DINA MERCEDES PACHECO BAZAN', 'AVENIDA HEROES #207-A',
        '8323307', 'elcampo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (911, 'PARTICULAR', '50', NULL, 'C. FRANCEILA YAHAIRA CRUZ PACHECO', 'RESTAURANTE LA CASITA DEL CHEF', '9838360469', NULL, 'C. FRANCEILA YAHAIRA CRUZ PACHECO',
        'AVENIDA ALVARO OBREGON #163 ENTRE 16 DE SEPTIEMBRE', '9838360469', 'igfrance.cruz88@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (912, 'PARTICULAR', '50', NULL, 'C. FRANCELIA YAHAIRA CRUZ PACHECO', 'RESTAURANTE LA CASITA DEL CHEF', '1291954', NULL, 'C. FRANCEILA YAHAIRA CRUZ PACHECO',
        'AV. ALVARO OBREGON #163 ENTRE 16 DE SEPTIEMBRE Y 5 DE MAYO, COL. PLUTARCO ELIAS', '1291954', 'lgfrance.cruz88@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (913, 'PARTICULAR', '59', NULL, 'C. LUCIO CANTE EK', 'PURIFICADORA EL GRAN OASIS', '9831196755', NULL, 'C. LUCIO CANTE EK', 'ENRIQUE BAROCIO S/N., COL. FORJADORES',
        '9831196755', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (914, 'PARTICULAR', '51', NULL, 'C. DEBORAH ANGULO VILLANUEVA', 'RESTAURNTE VENTOLERA', '2673370', NULL, 'C. DEBORAH ANGULO VILLANUEVA',
        'AVENIDA CARMEN OCHOA DE MERINO #168, COLONIA CENTRO', '2673370', 'contabilidad@hotelvillanueva.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (915, 'PARTICULAR', '51', NULL, 'C. DEBORAH ANGULO VILLANUEVA', 'RESTAURANTE VENTOLERA', '2673370', NULL, 'C. DEBORAH ANGULO VILLANUEVA',
        'AVENIDA CARMEN OCHOA DE MERINO #166, COLONIA CENTRO', '2673370', 'contabilidad@hotelvillanueva.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (916, 'PARTICULAR', '52', NULL, 'C. JOSE PADILLA MENDOZA', 'SERGIOS PIZZA S.A. DE C.V.', '9838322399', NULL, 'C. JOSE PADILLA MENDOZA',
        'AVENIDA ALVARO OBREGON #182, COLONIA CENTRO', '9838322399', 'sregiospizza.facturacion@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (917, 'PARTICULAR', '52', NULL, 'C. JOSE PADILLA MENDOZA', 'SERGIOS PIZZA S.A. DE C.V.', '9838322399', NULL, 'C. JOSE PADILLA MENDOZA',
        'AVENIDA ALVARO OBREGON #182, COLONIA CENTRO', '9838322399', 'sergiospizza.facturacion@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (918, 'PARTICULAR', '53', NULL, 'C. SOPHIA VALERIA PADILLA CASTILLEJOS', 'SERGIOS PIZZA (PLAZA LAS AMERICAS)', '9838322399', NULL, 'C. SOPHIA VALERIA PADILLA CASTILLEJOS',
        'AVENIDA INSURGENTES KM 5.025 INTERIOR G-14', '9838322399', 'sergiospizza.facturacion@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (919, 'PARTICULAR', '54', NULL, 'C. CEILA KAREN MARQUEZ TAMAY', 'DULCE FRUTA', '9837537141', NULL, 'C. CEILA KAREN MARQUEZ TAMAY',
        'AV. CHETUMAL ENTRE REFORMA Y CHABLE, MANZANA 174, LOTE 19, COL. SOLIDARIDAD', '9837537141', 'cey13abril@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (920, 'PARTICULAR', '55', NULL, 'C. SERGIO ISRAEL PADILLA CASTILLEJOS', 'RESTAURANTE SERGIOS PIZZA', '9838322355', NULL, 'C. SERGIO ISRAEL PADILLA CASTILLEJOS',
        'AV. ADOLFO LOPEZ MATEOS #424-A ESQUINA NAPOLES, COL. CAMPESTRE', '9838322355', 'bnaiorcampestre@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (921, 'PARTICULAR', '56', NULL, 'C. ISAAC GOMEZ SAENZ', 'PANADERIA LOS II HERMANOS', '9831405234', NULL, 'C. ISAAC GOMEZ SAENZ', 'AVENIDA NICOLAS BRAVO ESQUINA CURACAO',
        '9831405234', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (922, 'PARTICULAR', '57', NULL, 'C. AIDA PANTI GONZALEZ', 'LA CASITA AZUL II', '9838390530', NULL, 'C. AIDA PANTI GONZALEZ', 'PRIVADA FRANCISCO J. MUJICA #97', '9838390530',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (923, 'PARTICULAR', '58', NULL, 'C. AIDA PANTI GONZALEZ', 'LA CASITA AZUL LONCHERIA', '9838390530', NULL, 'C. AIDA PANTI GONZALEZ', 'LAZARO CARDENAS #327-A', '9838390530',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (924, 'PARTICULAR', '59', NULL, 'C. ANGELA VAZQUEZ ABUNDEZ', 'CARNITAS EL PUERQUITO FELIZ Y PURIFICADORA TRES VALLES', '9831365329/9831307931', NULL,
        'C. ANGELA VAZQUEZ ABUNDEZ', 'CALLE CHABLE, MANZANA 303, LOTE 2, COLONIA PROTERRITORIO', '9831365329/9831307931', 'ljvergaraislas@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (925, 'PARTICULAR', '929', NULL, 'C. JORGE LUIS MARTINEZ SABIDO', 'HIELO CAMPESTRE', '9831172071', NULL, 'C. JORGE LUIS MARTINEZ SABIDO',
        'CALLE RETORNO 9 #474 COLONIA CAMPESTRE', '9831172071', 'jlmss@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (926, 'PARTICULAR', '941', NULL, 'C. AC BURGOS MARIA DEL CARMEN', 'TORTILLERIA MARY', '9831090955', NULL, 'C. AC BURGOS MARIA DEL CARMEN',
        'CALLE PROGRESO LOTE 106 MANZANA 563 COLONIA NUEVO PROGRESO', '9831090955', 'marcemix_1476@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (927, 'PARTICULAR', '930', NULL, 'C. GUADALUPE OLIVARES PEÑALOSA', 'TORTILLERIA CYNTHIA', '9831355287', NULL, 'C. GUADALUPE OLIVARES PEÑALOSA',
        'AVENIDA 22 ENTRE 23 Y 25, COLONIA BENITO JUAREZ', '9831355287', 'rossanaop28@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (928, 'PARTICULAR', '931', NULL, 'C. ROSSANA OLIVARES PEÑALOZA', 'TORTILLERIA EL MOLINITO MERCADO', '9821106211', NULL, 'C. ROSSANA OLIVARES PEÑALOZA',
        'AVENIDA 9 ENTRE 28 Y 30, COLONIA CENTRO', '9821106211', 'rossanaop28@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (929, 'PARTICULAR', '392', NULL, 'C. ROSSANA OLIVARES PEÑALOZA', 'TORTILLERIA EL MOLINITO LAS TORRES', '9821106211', NULL, 'C. ROSSANA OLIVARES PEÑALOZA',
        'AVENIDA 21 ENTRE 40 Y 42, COLONIA COLOSIO', '9821106211', 'rossanaop28@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (930, 'PARTICULAR', '60', NULL, 'C. PATRICIA OLIVARES PEÑALOSA', 'TORTILLERIA EL MOLINITO', '9831582260', NULL, 'C. PATRICIA OLIVARES PEÑALOSA',
        'CALLE 30 POR 23, COLONIA BENITO JUAREZ', '9831582260', 'rossanaop28@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (931, 'PARTICULAR', '61', NULL, 'C. ESTELA AC SUAREZ', 'DISPENSADOR DE AGUA PURIFICADA LA FUENTE', '9831438510, 9831814803', NULL, 'C. ESTELA AC SUAREZ',
        'CALLE RAUDALES 01-D ENTRE BELICE Y 10 DE ABRIL, COLONIA PROTERRITORIO', '9831438510, 9831814803', 'purificadoralafuente15@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (932, 'PARTICULAR', '62', NULL, 'C. RICARDO IVAN DZUL CHI', 'PURIFICADORA DEPURA AGUA', '9838676948', NULL, 'C. RICARDO IVAN DZUL CHI', 'CALLE LAGUNA DE BACALAR #482',
        '9838676948', 'rdzul_89@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (933, 'PARTICULAR', '63', NULL, 'HUXLEY DISTILLING S. DE R.L. DE C.V.', NULL, NULL, NULL, NULL, 'TABLAJE CATASTRAL #1.961 DE LA LOACLIDAD Y MUNICIPIO DE YAXKUKUL, YUCATAN',
        NULL, NULL, NULL, NULL, 'Yucatán', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (934, 'PARTICULAR', '64', NULL, 'C. DAVID ELJURE TERRAZAS', 'RESTAURANTE ALMINA', '9838360054', NULL, 'C. DAVID ELJURE TERRAZAS',
        'AVENIDA JOSE MARIA MORELOS #3, COLONIA CENTRO', '9838360054', 'deljure1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (935, 'PARTICULAR', '90', NULL, 'C. YUESHENG TAN', 'RESTAURANTE YING ZHOU', '9831577049', NULL, 'C. YUESHENG TAN',
        'AVENIDA ERICK PAOLO MARTINEZ, MANZANA 131, LOTE 5, COLONIA SOLIDARIDAD', '9831577049', 'clemente.gutierrez@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (936, 'PARTICULAR', '91', NULL, 'C. ZHANG DAREN', 'RESTAURANTE CHINA CHENS', '9831577049', NULL, 'C. ZHANG DAREN',
        'CONSTITUYENTES DEL 74 #254, LOCAL 30 Y 32, COLONIA EL ENCANTO', '9831577049', 'clemente.gutierrez23@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (937, 'PARTICULAR', '65', NULL, 'C. MARIA LUISA BAENA GOMEZ', 'RESTAURANT EL GALEON', '9831161056', NULL, 'C. MARIA LUISA BAENA GOMEZ',
        'AVENIDA OAXACA LOCAL 6, CALDERITAS, Q. ROO', '9831161056', 'leo_med_baca@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (938, 'PARTICULAR', '934', NULL, 'C. ROMAN PECH BORGES', 'POLLOS ASADOS LALO´S CHICKEN', '9831205698', NULL, 'C. ROMAN PECH BORGES',
        'AV. ROJO GOMEZ, LOTE 1, MANZANA 49 ENTRE CHABLE Y RETORNO 27', '9831205698', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (939, 'PARTICULAR', '935', NULL, 'C. MARIA CONCEPCION PECH BORGES', 'POLLOS ASADOS LALO´S CHICKEN', '9831205698', NULL, 'C. MARIA CONCEPCION PECH BORGES',
        'AV. CONSTITUYENTES, MANZANA 313, LOTE 12 ESQ. RAUDALES, COL. PROTERRITORIO', '9831205698', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (940, 'PARTICULAR', '936', NULL, 'C. ROMAN PECH BORGES', 'POLLOS ASADOS LALO´S CHICKEN', '9831205698', NULL, 'C. ROMAN PECH BORGES',
        'AVENIDA ROJO GÓMEZ, LOTE 1, MANZANA 49 ENTRE RETORNO 7 Y CHABLÉ', '9831205698', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (941, 'PARTICULAR', '66', NULL, 'C. RANULFO ABRAHAN GONZALEZ PALAFOX', 'TAQUERIA EL PASTORCITO', '9831206905', NULL, 'C. RANULFO ABRAHAN GONZALEZ PALAFOX',
        'CALLE HUACHINANGO S/N., MAHAHUAL, Q.ROO', '9831206905', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (942, 'PARTICULAR', '67', NULL, 'C. JUN ZHANG', 'PURIFICADORA CARIBEAN INTERNATIONAL', '501625868', NULL, 'C. JUN ZHANG', 'COROZAL, ORANGE WALK', '501625868', NULL, NULL,
        NULL, 'Quintana Roo', 'BELICE', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (943, 'PARTICULAR', '68', NULL, 'C. TAVITA GOMEZ GOMEZ', 'TAQUERIA GABI', '9831385626 9831259155 9832123172', NULL, 'C. TAVITA GOMEZ GOMEZ',
        'AV. BELICE # 223 ENTRE PRIMO DE VERDAD Y VENUSTIANO CARRANZA', '9831385626 9831259155 9832123172', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (944, 'PARTICULAR', '68', NULL, 'C. TAVITA GOMEZ GOMEZ', 'TAQUERIA GABY', '9831355202, 9831546760', NULL, 'C. TAVITA GOMEZ GOMEZ',
        'AV. BELICE # 223 ENTRE VENUSTIANO CARRANZA Y PRIMO DE VERDAD', '9831355202, 9831546760', 'soniagoretty@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (945, 'PARTICULAR', '70', NULL, 'C. JOSE LUIS DIAZ ALBARRAN', 'TAQUERIA DIAZ GRILL', '1441046', NULL, 'C. JOSE LUIS DIAZ ALBARRAN',
        'AVENIDA HEROES #261 ESQUINA SAN SALVADOR , COLONIA CENTRO', '1441046', 'diazgrill@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (946, 'PARTICULAR', '71', NULL, 'C. RUBÍ GUADALUPE CASTILLO LEÓN', 'LONCHERIA RUBI', '9831090955', NULL, 'C. RUBÍ GUADALUPE CASTILLO LEÓN',
        'AV. DE LOS HEROES INTERIOR MERCADO ALTAMIRANO, LOCAL # 84, COL. CENTRO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (947, 'PARTICULAR', '71', NULL, 'C. DIANA ODETT FABIAN VILLAMIL', 'LONCHERIA EL ANGELITO', '9831057994', NULL, 'C. DIANA ODETT FABIAN VILLAMIL',
        'CALLE ISLA CANCUN #374 ENTRE RAFAEL E. MELGAR, COL. JESUS MARTINEZ ROSS', '9831057994', 'fabian_1692@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (948, 'PARTICULAR', '937', NULL, 'C. JESUS MARTIN SANTOS GONZALEZ', 'PANADERIA SANTOS', '9831389123', NULL, 'C. JESUS MARTIN SANTOS GONZALEZ',
        'AVENIDA MAXUCHAC #122 ENTRE PALOMA Y CENZONTLE', '9831389123', 'anita0774@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (949, 'PARTICULAR', '938', NULL, 'C. XINGLING MA', 'B.B.Q. HOUSE', '9831302476', NULL, 'C. XINGLING MA', 'AV. INSURGENTES, KM 5.025 INT. PLAZA LAS AMÉRICAS, LOCAL G-17',
        '9831302476', '709309070@qq.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (950, 'PARTICULAR', '939', NULL, 'MOTEBURGER S.A. DE C.V.', 'BURGUER KING', '1270555', NULL, NULL, 'AVENIDA INSURGENTES ESQUINA ITZAUCHE', '1270555', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (951, 'PARTICULAR', '940', NULL, 'C. DAISY LIDIA CARDONA RAMOS', 'TORTILLERIA RAMOS', '9831356677', NULL, 'C. DAISY LIDIA CARDONA RAMOS',
        'AVENIDA ANDRES Q. ROO #259 CON SAN SALVADOR Y CARRANZA', '9831356677', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (952, 'PARTICULAR', '942', NULL, 'C. LIZBETH MERCEDES CARVAJAL GOMEZ', 'PASTELERIA Y PANIFICADORA MERCY S.A. DE C.V.', '8320567', NULL, 'C. LIZBETH M. CARVAJAL GOMEZ',
        'AVENIDA FRANCISCO I. MADERO # 206, COLONIA CENTRO', '8320567', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (953, 'PARTICULAR', '943', NULL, 'C. ZURISADAY BEATRIZ SALAZAR PACHECO', 'RESTAURANTE STAR SALUDABLE', '9831859400/ 8320567', NULL, 'C. ZURISADAY BEATRIZ SALAZAR PACHECO',
        'CALLE PALERMO N°315 ENTRE AV. BUGAMBILIAS Y JUSTO SIERRA, COL. JOSEFA ORTIZ', '9831859400/ 8320567', 'zuribsp@outlook.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (954, 'PARTICULAR', '944', NULL, 'C. PEDRO ADOLFO MANRIQUE MARTINEZ', 'POLLOS ASADOS MANRIQUE', '9831200253', NULL, 'C. PEDRO ADOLFO MANRIQUE MARTINEZ',
        'AVENIDA JUAREZ #209', '9831200253', 'emymanriquesamos@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (955, 'PARTICULAR', '945', NULL, 'C. ZHANG DAREN', 'RESTAURANTE COMIDA CHINA DRAGON TOWN', '9831577049', NULL, 'C. ZHANG DAREN',
        'AV. ERICK PAOLO MARTINEZ INTERIOR PLAZA SORIANA, COL. AMPLIACIÓN PROTERRITORIO', '9831577049', 'clemente.gutierrez23@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (956, 'PARTICULAR', '946', NULL, 'C. ZHANG DAREN', 'RESTAURANTE CHINA ZHANG DAREN', '9831577049', NULL, 'C. ZHANG DAREN',
        'AV. INSURGENTES INTERIOR PLAZA BAHIA, N° 41, COLONIA REFORMA', '9831577049', 'clemente.gutierrez23@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (957, 'PARTICULAR', '70', NULL, 'C. ZENAIDA YUNUEN MEDINA CRUZ', 'POLLERIA, FRUTAS Y VERDURAS ALEX', '9831352047', NULL, 'C. ZENAIDA YUNUEN MEDINA CRUZ',
        'AV. MAXUXAC ESQUINA CHACHALACAS, MANZANA 6, LOTE 71, COL. NUEVO PROGRESO', '9831352047', 'yunuen.zeny95@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (958, 'PARTICULAR', '71', NULL, 'C. MARIA BERNARDA KU CANO', 'TAQUERIA DE MARISCOS INDEPENDENCIA', '9831821408/983180644', NULL, 'C. MARIA BERNANRDA KU CANO',
        'CALZADA VERACRUZ #597', '9831821408/983180644', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (959, 'PARTICULAR', '71', NULL, 'C. MARIA BERNARDA KU CANO', 'TAQUERIA INDEPENDENCIA', '9831864084', NULL, 'C. MARIA BERNARDA KU CANO', 'CALZADA VERACRUZ #597',
        '9831864084', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (960, 'PARTICULAR', '73', NULL, 'C. RONG ZUJIN', 'RESTAURANT PALACIO CHINO', '8322292', NULL, 'C. RONG ZUJIN', 'HEROES #222 ENTRE PRIMO DE VERDAD Y VENUSTIANO CARRANZA',
        '8322292', 'despacho7scj@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (961, 'PARTICULAR', '74', NULL, 'C. QUERUBIN MORALES MORALES', 'TORTILLERIA MORALES', '9831090955', NULL, 'C. QUERUBIN MORALES MORALES',
        'AV. JAVIER ROJO GOMEZ #727, COL. NUEVO PROGRESO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (962, 'PARTICULAR', '75', NULL, 'C. ABRAHAM OCTAVIO MARTINEZ VAZQUEZ', 'LONCHERIA RAQUELITO', '8322982', NULL, 'C. ABRAHAM OCTAVIO MARTINEZ VASQUEZ',
        'AVENIDA HIDALGO #204, COLONIA CENTRO', '8322982', 'loncheriaraquelito@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (963, 'PARTICULAR', '75', NULL, 'C. LUCIA VICTORIA POOT UH', 'RESTAURANTE LA ABUELA JUANITA', '9831176902', NULL, 'C. LUCIA VICTORIA POOT UH',
        'AVENIDA YUCATAN #338, COLONIA YUCATAN, CALDERITAS, Q.ROO', '9831176902', 'lucia_victoria72@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (964, 'PARTICULAR', '77', NULL, 'C. MARIA EUGENIA ISLAS SANDOVAL', 'COCINA ECONOMICA LOS PORTALES', '9831656318', NULL, 'C. MARIA EUGENIA ISLAS SANDOVAL',
        'FAISAN CON MAXUXAC, LOTE 01, MANZANA 32, PAYO OBISPO 2', '9831656318', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (965, 'PARTICULAR', '77', NULL, 'C. ABRIL ITA NDEHUI GARCÍA RAMOS', 'DISPENSADOR DE AGUA AQUAPURIFIC', '9982248271', NULL, 'C. ABRIL ITA NDEHUI GARCIA RAMOS',
        'AV. CHETUMAL ESQ. CALLE SANTA CRUZ DE BRAVO, LOTE 10, MANZANA 672, LOCAL 2', '9982248271', 'tandy2006elit@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (966, 'PARTICULAR', '78', NULL, 'C. JUNLIN CHEN', 'RESTAURANTE COMIDA CHINA GUANG ZHOU', '8328068', NULL, 'C. JUNLIN CHEN',
        'AVENIDA SAN SALVADOR #501-A,, COLONIA AMPLIACION 8 DE OCTUBRE', '8328068', 'contador-rodolfo@live.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (967, 'PARTICULAR', '79', NULL, 'C. SILVIA JAENNETT PERAZA OCAMPO', 'NUTRIPANZA', '9831302452/9831915796', NULL, 'C. SILVIA JAENNETT PERAZA OCAMPO',
        'AV. HERIBERTO FRIAS ESQUINA CEDRO #672, COL. DEL BOSQUE', '9831302452/9831915796', 'silviaperaz@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (968, 'PARTICULAR', '80', NULL, 'C. ESTHER CARIDAD MONSRREAL PARRA', 'RESTAURANTE EL TAQUITO', '9831544190', NULL, 'C. ESTHER CARIDAD MONSRREAL PARRA',
        'PLUTARCO ELIAS CALLES #218-C', '9831544190', 'ltaquito_dedonjulio@outlokk.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (969, 'PARTICULAR', '81', NULL, 'C. FERNANDO RUIZ PEÑA', 'COCINA ECONOMICA POLLO VOLADOR', '9832120563', NULL, 'C. FERNANDO RUIZ PEÑA',
        'JUAN JOSE SIORDIA ENTRE MORELOS Y ZAPATA', '9832120563', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (970, 'PARTICULAR', '82', NULL, 'C. NESTOR GIBRAN CACHON GONZALEZ', 'CAFETERIA LA BAGUE SHOP', '9838360060', NULL, 'C. NESTOR GIBRAN CACHON GONZALEZ',
        'AVENIDA LAZARO CARDENAS #222', '9838360060', 'ignaciocachon59@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (971, 'PARTICULAR', '84', NULL, 'C. DULCE MARIA DEL SOCORRO BORGES CRUZ', 'JUGOS Y LICUADOS DOÑA DULCE', '9831657246', NULL, 'C. DULCE MARIA DEL SOCORRO BORGES CRUZ',
        'AV. 16 DE SEPTIEMBRE S/N. LOCAL 18, MANZANA 17, LOTE 40', '9831657246', 'dulce_borges43@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (972, 'PARTICULAR', '84', NULL, 'C. IRENE SALAS RIVERO', 'RESTAURANT LA CENTRAL', '9831715650', NULL, 'C. IRENE SALAS RIVERO',
        'MERCADO LAZARO CARDENAS DEL RIO LOCALES 11-12-13', '9831715650', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (973, 'PARTICULAR', '85', NULL, 'C. JUN ZHANG', 'CARIBBEAN INTERNATIONAL BREWERY CO.LTD', '5016293336', NULL, 'C. JUN ZHANG', 'CARMELITA VILLAGE ORANGE WALK DISTRICT',
        '5016293336', 'chinocdmx@163.com', NULL, NULL, 'Quintana Roo', 'BELICE', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (974, 'PARTICULAR', '86', NULL, 'C. MARIA ESPERANZA SANSORES MENDIBURO', 'TORTILLERIA EL ESFUERZO', '8327257', NULL, 'C. MARIA ESPERANZA SANSORES MENDIBURO',
        'OTHON P BLANCO #265 ENTRE MADERO Y MORELOS', '8327257', 'conchitis08@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (975, 'PARTICULAR', '87', NULL, 'C. GLADYS EUGENIA DE GUADALUPE DOMINGUEZ LOPEZ', 'RESTAURANT BOYS PIZZA', '9838367755', NULL,
        'C. GLADYS EUGENIA DE GUADALUPE DOMINGUEZ LOPEZ', 'JUAREZ #234 ENTRE V. CARRANZA Y SAN SALVADOR', '9838367755', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (976, 'PARTICULAR', '88', NULL, 'C. DULCE MARIA SOLIS', 'CARNICERIA Y CHICHARRONERIA EL DIVINO NIÑO 2', '9831027222', NULL, 'C. DULCE MARIA SOLIS',
        'AV. BUGAMBILIAS #672, COL. JARDINES ENTRE FLOR DE LIZ Y FLOR DE MAYO', '9831027222', 'mijangostrejoj@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (977, 'PARTICULAR', '947', NULL, 'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'TORTILLERIA TOSTIMAYA', '9831079347', NULL,
        'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'AVENIDA CHETUMAL #871 ESQUINA MARTINICA, MANZANA 108, LOTE 22', '9831079347', 'tostimaya@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (978, 'PARTICULAR', '89', NULL, 'C. FERNANDO RUIZ PEÑA', 'POLLOS ASADOS EL POLLO VOLADOR', '9837527970', NULL, 'C. FERNANDO RUIZ PEÑA',
        'AV. CONSTITUYENTES DEL 74 , MANZANA 298, LOTE 1 ENTRE REFORMA Y CHABLE', '9837527970', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (979, 'PARTICULAR', '90', NULL, 'CAMPAMENTO NUEVOS INICIOS', NULL, '9838357027', NULL, NULL, 'KM. 10 CARRETERA FEDERAL, BACALAR - XUL-HA, Q.ROO', '9838357027', NULL, NULL,
        NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (980, 'PARTICULAR', '90', NULL, 'C. MARIA ELENA SALINAS ORTIZ', 'CAFETERIA CAFECITO DE OLLA', '9831075492', NULL, 'C. MARIA ELENA SALINAS ORTIZ',
        'AV. INDEPENDENCIA #404 ENTRE JUAN JOSE SIORDIA Y ANASTACIO GUZMAN', '9831075492', 'cafecitodeolla@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (981, 'PARTICULAR', '91', NULL, 'C. JUAN ALBERTO CANTO BUENFIL', 'LONCHERIA LA TIA MATY', '9831193779', NULL, 'C. JUAN ALBERTO CANTO BUENFIL',
        'JABIN #144 ESQUINA CALZADA VERACRUZ', '9831193779', 'abelardo976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (982, 'PARTICULAR', '948', NULL, 'C. ISABEL DE LA CRUZ CRUZ', 'BAR EL ALMIRANTE', '9831302476', NULL, NULL, 'CALLE RAMON CORONA #116', '9831302476', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (983, 'PARTICULAR', '949', NULL, 'C. FLORA BAENA CHIRINOS', 'TORTILLERIA FLOWER', '9831069568', NULL, 'C. FLORA BAENA CHIRINOS',
        'CALLE ISAAC MEDINA N°. 242, COLONIA FORJADORES', '9831069568', 'floweribaena@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (984, 'PARTICULAR', '950', NULL, 'C. FRANCISCO JAVIER CANCHE EUAN', 'TAQUERIA LA GUADALUPANA', NULL, NULL, 'C. FRANCISCO JAVIER CANCHE EUAN',
        'AV. AARON MERINO FERNANDEZ, MANZANA 55, LOTE 2, COLONIA FORJADORES', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (985, 'PARTICULAR', '91', NULL, 'BACHOCO S.A. DE C.V.', NULL, '9838711207/9831858269', NULL, 'BACHOCO S.A. DE C.V.',
        'CARRETERA FEDERAL CHETUMAL - HUAY-PIX, Q.ROO, COL. PARQUE INDUSTRIAL', '9838711207/9831858269', 'reyna.gongora@bachoco.net', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (986, 'PARTICULAR', '92', NULL, 'C. CRISTINA YAREMI ZUÑIGA ALVARADO', 'TAQUERIA DOÑA MARY', '9831000109', NULL, 'C. CRISTINA YAREMI ZUÑIGA ALVARADO',
        'CARMEN OCHOA DE MERINO #230', '9831000109', 'yaremiza@hotmail', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (987, 'PARTICULAR', '93', NULL, 'C. MICHELA CALZOLARI', 'RESTAURANT BAR PIAZZA MAGGIORE', '9837323330', NULL, 'C. MICHELA CALZOLARI',
        'CALLE CAMPECHE #87 ENTRE GUERRERO Y COAHUILA, CALDERITAS, Q.ROO', '9837323330', 'chak-nah@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (988, 'PARTICULAR', '94', NULL, 'C. LUIS ALBERTO PAVIA MENDOZA', 'HOTEL Y RESTAURANT HACIENDA CAMPESTRE', '9838330288', NULL, 'C. LUIS ALBERTO PAVIA MENDOZA',
        'AVENIDA CENTENARIO #680-A, COLONIA DEL BOSQUE', '9838330288', 'info@haciendacampestre.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (989, 'PARTICULAR', '95', NULL, 'C. PEDRO MIAM LOPEZ', 'RESTAURANT BAR EL PAJARO AMARILLO', '9831074352', NULL, 'C. PEDRO MIAM LOPEZ',
        'AVENIDA MAXUXAC ENTRE PETCACAB Y POLYUC, MANZANA 170, LOTE 11', '9831074352', 'pedromiam@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (990, 'PARTICULAR', '96', NULL, 'C. GONZALEZ SEQUEDA EMMA', 'NUTRIVIDA LUNCH CAFE', '9831572952', NULL, 'C. GONZALEZ SEQUEDA EMMA',
        'AVENIDA MAXUXAC S/N, MANZANA 375, LOTE 01, LOCAL 2', '9831572952', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (991, 'PARTICULAR', '97', NULL, 'C. YOLANDA MARMOLEJO SALAS', 'POLLOS ASADOS SARITA', '9831572952', NULL, 'C. YOLANDA MARMOLEJO SALAS',
        'CARRETERA FEDERAL S/N. ,MANZANA 18, LOTE 01, LOCALIDAD UCUM, Q.ROO', '9831572952', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (992, 'PARTICULAR', '98', NULL, 'C. CLAUDIA YBETTE CANTO ESTRELLA', 'TORTILLERIA LA ESPIGA', '9831859890', NULL, 'C. CLAUDIA YBETTE CANTO ESTRELLA',
        'FLORES MAGON #144, COLONIA CENTRO', '9831859890', 'angel_x80@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (993, 'PARTICULAR', '99', NULL, 'PRESTADORA DE SERVICIOS GES S.A. DE C.V.', 'RESTAURANTE APPLEBEES', '9831272706', NULL, 'PRESTADORA DE SERVICIOS GES S.A. DE C.V.',
        'TZISAUCHE #2 ESQUINA INSURGENTES, COL. JARDINES DE PAYO OBISPO', '9831272706', 'applebees_chetumal@gruges.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (994, 'PARTICULAR', '100', NULL, 'C. WENDOLINE CECILIA ITURBE COB', 'POLLOS ASADOS Y EMPANIZADOS VALENTINO', '9831119902', NULL, 'C. WENDOLINE CECILIA ITURBE COB',
        'CALZADA VERACRUZ # 329 ENTRE MANUEL M. DIEGUEZ Y JUAN SARABIA', '9831119902', 'wen.iturbe@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (995, 'PARTICULAR', '101', NULL, 'LAS AMERICAS S.C.', 'PLAZA LAS AMERICAS CHETUMAL', '8376102 2087344', NULL, 'LAS AMERICAS S.C.',
        'AVENIDA INSURGENTES KM. 5.025, COLONIA EMANCIPACION', '8376102 2087344', 'isanche@105-americas.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (996, 'PARTICULAR', '951', NULL, 'C. YAMILI HERRERA CARRILLO', 'RESTAURANTE MUHE SALAD´S', NULL, NULL, 'C. YAMILI HERRERA CARRILLO', 'CALLE EFRAIN AGUILAR #246', NULL,
        'gotyta72@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (997, 'PARTICULAR', '952', NULL, 'C. CANDELARIA CHAN CHAY', 'LONCHERIA SAN JUDAS TADEO', '9831138115', NULL, 'C. CANDELARIA CHAN CHAY',
        'CALLE JUANA DE ASBAJE #319 ESQUINA BUGAMBILIAS, COL. MIRAFLORES', '9831138115', 'candelariachan_xml@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (998, 'PARTICULAR', '953', NULL, 'C. MARIA JOSE TUN CANUL', 'LONCHERIA MAJO', '9831563016', NULL, 'C. MARIA JOSE TUN CANUL',
        'CALLE RAFAEL E. MELGAR #342-A ESQUINA LAGUNA DE BACALAR', '9831563016', 'magtun95@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (999, 'PARTICULAR', '102', NULL, 'C. DEYANIRA GUTIERREZ BARRAGAN', 'COMIDA LIMA LIMON', '9831178685', NULL, 'C. DEYANIRA GUTIERREZ BARRAGAN',
        'JUAREZ ESQUINA LAZARO CARDENAS #117', '9831178685', 'deygt21@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1000, 'INSTITUCIONAL', '103', NULL, 'DIRECTORA DE SERVICIOS DE SALUD', 'DRA. MYRIAN ORTIZ ENRIQUEZ', '983 8351927', NULL, 'DRA. MYRIAN ORTIZ ENRIQUEZ',
        'AVENIDA CHAPULTEPEC #267 ESQUINA MORELOS, COLONIA CENTRO', '983 8351927', 'myriam.sesa@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1001, 'PARTICULAR', '104', NULL, 'C. ALEJANDRO CRUZ PALMA Y SECA', 'LONCHERIA LA PALMITA', '9831829742', NULL, 'C. ALEJANDRO CRUZ PALMA Y SECA',
        'PALERMO #398 A ESQ. JUAN JOSE SIORDIA, COL. 20 DE NOVIEMBRE', '9831829742', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1002, 'PARTICULAR', '121', NULL, 'C. VICTORIA MENDEZ GUZMAN', 'PANADERIA VICKY', '9831250409', NULL, 'C. VICTORIA MENDEZ GUZMAN',
        'CALLE TIHOSUCO ENTRE 12 DE OCTUBRE Y 24 DE NOVIEMBRE, MANZANA 252, LOTE 6', '9831250409', 'panaderiavicky12@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1003, 'PARTICULAR', '105', NULL, 'C. HERNAN HERRERA ESTRADA', 'BANQUETAKOS', '9831540748', NULL, 'C. HERNAN HERRERA ESTRADA', 'AVENIDA ANDRES QUINTANA ROO #261',
        '9831540748', 'hernan_herrer@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1004, 'PARTICULAR', '106', NULL, 'C. ADDY ANABELL AGUILAR MAY', 'POLLOS ASADOS NIKI', '9831240955', NULL, 'C. ADDY ANABELL AGUILAR MAY',
        'ROJO GOMEZ, MANZANA 49, LOTE 2, COLONIA PAYO OBISPO II', '9831240955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1005, 'PARTICULAR', '107', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO', 'PURIFICADORA LA GOTA CRISTALINA', '9838390722', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO',
        'AVENIDA ERICK PAOLO MARTINEZ, MANZANA 57, LOTE 7, COLONIA PAYO OBISPO', '9838390722', 'impregrafic1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1006, 'PARTICULAR', '108', NULL, 'C. MARTHA JACINTA DEL ROSARIO CANTO CASTILLO', 'PURIFICADORA AQUETZALI', '9831341208', NULL,
        'C.MARTHA JACINTA DEL ROSARIO CANTO CASTILLO', 'AV. CHETUMAL, LOCAL # 2, ENTRE YAXCOPOIL Y SUBTENIENTE LOPEZ', '9831341208', 'martha-bonita@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1007, 'PARTICULAR', '109', NULL, 'C. ANDREA PAOLA ESCALANTE BORJAS', 'RESTAURANTE PALO DE TINTE', '9837582049', NULL, 'C. ANDREA PAOLA ESCALANTE BORJAS',
        'AVENIDA 1 #714 ENTRE CALLE 18 Y 20, COLONIA CENTRO', '9837582049', 'elemporiochetumal@hotmail.,com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1008, 'PARTICULAR', '110', NULL, 'C. ALVARO ALVAREZ AGUILAR', 'DISPENSADOR DE AGUA PURIFICADA COSTA MAYA', '9831169802', NULL, 'C. ALVARO ALVAREZ AGUILAR',
        'CALLE JAIBA, KM. 55, MAHAHUAL, Q.ROO', '9831169802', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1009, 'PARTICULAR', '111', NULL, 'C. MARICRUZ PASTRANA SABIDO', 'LONCHERIA LUCI 3', '9831208610', NULL, 'C. MARICURZ PASTRANA SABIDO', 'NAPOLES #312', '9831208610', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1010, 'PARTICULAR', '110', NULL, 'C. MARICRUZ PASTRANA SABIDO', 'LONCHERIA LUCI III', '9831208610', NULL, 'C. MARICRUZ PASTRANA SABIDO', 'CALLE NAPOLES #312', '9831208610',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1011, 'PARTICULAR', '112', NULL, 'C. ZOILA HAU', 'LONCHERIA LISSET', '9831307963', NULL, 'C. ZOILA HAU', 'HERIBERTO FRIAS # 238, COL. LOPEZ MATEOS', '9831307963', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1012, 'PARTICULAR', '114', NULL, 'C. VICTOR HUGO PACHECO CHAVEZ', 'BAR PERAZA', '2855627', NULL, 'C. VICTOR HUGO PACHECO CHAVEZ', 'MAHATMA GHANDI #205', '2855627',
        'macachaso74@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1013, 'PARTICULAR', '115', NULL, 'C. EFRAIN UC UH', 'RESTAURANTE LAS OLAS DEL MAR', '9838367717', NULL, 'C. EFRAIN UC UH', 'AVENIDA OAXACA # 03, CALDERITAS, Q.ROO',
        '9838367717', 'roslinlurline@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1014, 'PARTICULAR', '116', NULL, 'C. BLANCA ESTELA ALFARO NAVA', 'ANTOJITOS DOÑA BLANCA', '9831207313', NULL, 'C. BLANCA ESTELA ALFARO NAVA',
        'CALLE FLOR DE LIZ #109 ENTRE BUGAMBILIAS Y TERESITAS, COL. JARDINES', '9831207313', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1015, 'PARTICULAR', '117', NULL, 'C.JOSE ALFREDO NAAL CETINA', 'PIZZERIA CHEPOS', '9831106218', NULL, 'C.JOSE ALFREDO NAAL CETINA',
        'AVENIDA ISLA CANCUN #422-A POR SICILIA Y CORCEGA', '9831106218', 'pizzachepo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1016, 'PARTICULAR', '117', NULL, 'C. JOSE LUIS VARGAS PEREZ', 'PANADERIA CHETUMAL', '9837007495/9831641582', NULL, 'C. JOSE LUIS VARGAS PEREZ',
        'AV. NICOLAS BRAVO S/N. MANZANA 764, LOTE 1, COL. TERRITORIO FEDERAL', '9837007495/9831641582', 'bcttovor87@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1017, 'PARTICULAR', '118', NULL, 'C. MARIA CONCEPCION KU JIMENEZ', 'RESTAURANT LA CONCHITA', '9831202770', NULL, 'C. MARIA CONCEPCION KU JIMENEZ',
        'AVENIDA OAXACA LOCAL 5, CALDERITAS Q.ROO', '9831202770', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1018, 'PARTICULAR', '954', NULL, 'C. GUILLERMO MANUEL GARCIA SEBASTIAN', 'PURIFICADORA ALLENDE', '9831105011', NULL, 'C. GUILLERMO MANUEL GARCIA SEBASTIAN',
        'DOMICILIO CONOCIDO, POBLADO ALLENDE, Q.ROO', '9831105011', 'gmanuel18@msn.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1019, 'PARTICULAR', 'EM2000', NULL, 'C. ELIA TOMASA BLANCO GAMERO', 'POLLERIA ELIA', '9831090955', NULL, 'C. ELIA TOMASA BLANCO GAMERO',
        'CIRCUITO 1 SUR #11 COLONIA PACTO OBRERO', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1020, 'PARTICULAR', 'EM2001', NULL, 'C. JORGE HERRERA CETINA', 'TAQUERIA LECHON DORADO', '9831090955', NULL, 'C. JORGE HERRERA CETINA',
        'CALZADA VERACRUZ #321. COL. ADOLFO LOPEZ MATEOS', '9831090955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1021, 'PARTICULAR', 'EM2002', NULL, 'C. JULIA MONTALVO MARIN', 'MOLINO Y TORTILLERIA JU-JU', NULL, NULL, 'C. JULIA MONTALVO MARIN',
        'AV. JUAREZ No. 330 ENTRE LAGUNA MILAGROS Y LAGUNA DE BACALAR.', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1022, 'PARTICULAR', 'EM2003', NULL, 'C. GUOHUA LIANG', 'SHUNG FENG COMIDA ASIATICA', '9831195785', NULL, 'C. GUOHUA LIANG',
        'AV. INSURGENTES S/N, LOCAL G8, INTERIOR PLAZA LAS AMERICAS', '9831195785', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1023, 'PARTICULAR', 'EM2004', NULL, 'C. VANNESA CHAN ESPINOZA', 'PANIFICADORA LA GRACIA DE DIOS', '9994158759', NULL, 'C. VANESA CHAN ESPINOZA',
        'AV. CONSTITUYENTES ESQUINA CHABLE, COL. PROTERRITORIO', '9994158759', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1024, 'PARTICULAR', '119', NULL, 'C. DORA MARIA CHAN CHAN', 'TORTILLERIA EDUARDOS', '9837536770', NULL, 'C. DORA MARIA CHAN CHAN',
        'YAXCOPOIL ESQUINA CHACHALACA, COL. PAYO OBISPO II, MANZANA 60, LOTE 13', '9837536770', 'doramariachan@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1025, 'PARTICULAR', '1', NULL, 'C. MIREYA DEL SOCORRO GONZALEZ MUGARTEGUI', 'LONCHERIA GALILEO''S CHETUMAL', '9831234441', NULL,
        'C. MIREYA DEL SOCORRO GONZALEZ MUGARTEGUI', 'AVENIDA INSURGENTES #835 POR TEPICH Y EKPEO', '9831234441', 'galileo.69@live.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1026, 'PARTICULAR', '120', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT', 'PURIFICADORA AQUA ACTIVA', '1182115', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT',
        'AV. CHETUMAL, MANZANA 110, LOTE 16, COL. ANDRES QUINTANA ROO', '1182115', 'aguaactiva2014@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1027, 'PARTICULAR', '121', NULL, 'C. LUZ DEL ALBA PEREZ DE LA CRUZ', 'PANADERIA ARILESS', '9831046716', NULL, 'C. LUZ DEL ALBA PEREZ DE LA CRUZ',
        'MILAN # 322 ENTRE BACALAR Y BUGAMBILIAS', '9831046716', 'quetzacoatl17070@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1028, 'PARTICULAR', '122', NULL, 'C. JORGE ANTONIO DZUL CHI', 'DISPENSADOR DE AGUA PURIFICADA DEPURAGUA', '9831142886', NULL, 'C. JORGE ANTONIO DZUL CHI',
        'AVENIDA LUIS MANUEL SEVILLA SANCHEZ #1310', '9831142886', 'depuragua_qroo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1029, 'PARTICULAR', '123', NULL, 'C. HECTOR RAUL GOMEZ DOMINGUEZ', 'BOYS PIZZA MAXUXAC', '9831162121', NULL, 'C. HECTOR RAUL GOMEZ DOMINGUEZ',
        'AV. MAXUXAC ESQUINA 1 DE MAYO, MANZANA 240, LOTE 2', '9831162121', 'manaanllo.22@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1030, 'PARTICULAR', '124', NULL, 'C. JESUS CERVANTES LOPEZ', 'PIZZERIA KREIZY PIZZA', '9831577049', NULL, 'C. JESUS CERVANTES LOPEZ',
        'AV. CONSTITUYENTES DEL 74 ENTRE ALTOS DE SEVILLA Y RIO VERDE COL. PROTERRITORIO', '9831577049', 'clemente.gutietrez23@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1031, 'PARTICULAR', '126', NULL, 'OPERADORA ECOTURISTICA ICHKABAL BACALAR S.C. DE R.L. DE C.V.', 'RESTAURANT BALNEARIO EJIDAL MAGICO BACALAR', '9831051636', NULL,
        'OPERADORA ECOTURISTICA ICHCABAL BACALAR S.C. DE R.L. DE C.V.', 'AVENIDA 1 POR CALLE 26 Y 28 #971, COLONIA MARIO VILLANUEVA', '9831051636', 'balneariomagicobaclar@gmail',
        NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1032, 'PARTICULAR', '1999', NULL, 'C. JOSE JAVIER PERAZA GONZALEZ', 'PIANO BAR PERAZA PEPOS', '2855627', NULL, 'C. JOSE JAVIER PERAZA GONZALEZ',
        'MAHATMA GANDHI # 205, COLONIA CENTRO', '2855627', 'vhpc9524@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1033, 'PARTICULAR', '123', NULL, 'C. FARID MEDINA N. DE CACERES', 'RESTAURANT EL JUNCO DE HONG KONG', '9831239970', NULL, 'C. FARID MEDINA N. DE CACERES',
        'CARMEN OCHOA DE MERINO # 121 ESQUINA REFORMA', '9831239970', 'mamete_mr@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1034, 'PARTICULAR', '127', NULL, 'INSTITUTO WOZNIAK', 'CAFETERIA ESCOLAR INTERIOR INSTITUTO WOZNIAK', '11830080', NULL, 'C. REYNALDO AGUILAR SANTOYO',
        'AVENIDA CENTENARIO S/N. ESQUINA I. COMONFORT, COLONIA DEL BOSQUE', '11830080', 'wozniak.finanzas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1035, 'PARTICULAR', '128', NULL, 'C. GUSTAVO ENRIQUE DE ATOCHA DOMINGUEZ LOPEZ', 'PIZZERIA LA PIZZA DE PAYO', '9831077645', NULL,
        'C. GUSTAVO ENRIQUE DE ATOCHA DOMINGUEZ LOPEZ', 'AVENIDA ERICK PAOLO MARTINEZ #149', '9831077645', 'gustavitodomm@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1036, 'PARTICULAR', '129', NULL, 'DOBLE YA S.A. DE C.V.', 'RESTAURANT PITAYA', '9831121063', NULL, 'DOBLE YA S.A. DE C.V.', 'AVENIDA MAHAHUAL, MANZANA 9, LOTE 7',
        '9831121063', 'gacborv.75@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1037, 'PARTICULAR', '100', NULL, 'C. ALMA DELFINA MARQUEZ GOLPE', 'POLLOS ASADOS EL CHINITO', '9831336260', NULL, 'C. ALMA DELFINA MARQUEZ GOLPE',
        'AV. ENRIQUE BAROCIO BARRIOS S/N., MANZANA 50, LOTE 1, COL. FORJADORES', '9831336260', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1038, 'PARTICULAR', '101', NULL, 'C. REINA GUADALUPE OSORIO FLORES', 'ANTOJITOS LA LUPITA', '9831345040', NULL, 'C. REINA GUADALUPE OSORIO FLORES',
        'BELIZARIO DOMINGUEZ #80 ENTRE LUIS MOYA Y HERIBERTO FRIAS', '9831345040', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1039, 'PARTICULAR', '102', NULL, 'C. LUIS ALBERTO MEDINA DIAZ', 'MICHELADAS EL MALECON', '9831070309', NULL, 'C. LUIS ALBERTO MEDINA DIAZ',
        'CALLE 22 DE ENERO # 145 ENTRE HIDALGO Y 16 DE SEPTIEMBRE', '9831070309', 'luamedi@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1040, 'PARTICULAR', '103', NULL, 'C. LOURDES GUADALUPE ESPADAS PATRÓN', 'PIZZERIA ROMANIS', '9831024697', NULL, 'C.LOURDES GUADALUPE ESPADAS PATRON',
        'AVENIDA BELICE #262-A, LOTE 3 ENTRE SAN SALVADOR', '9831024697', 'lou_mar28@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1041, 'PARTICULAR', '103', NULL, 'C. ZAAZIL SELENE ORTIZ DEL RIVERO', 'RESTAURANTE ZAYZU', '9831440104', NULL, 'C. ZAAZIL SELENE ORTIZ DEL RIVERO',
        'PRIMO DE VERDAD #78 ESQUINA COZUMEL, COLONIA PRIMERA LEGISLATURA', '9831440104', 'zayzu28@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1042, 'PARTICULAR', '107', NULL, 'C. JOSE ALFREDO JIMENEZ TUN', 'CONTROL DE PLAGAS COZUMEL', '9871189542', NULL, 'C. JOSE ALFREDO JIMENEZ TUN',
        'CALLE 37 ENTRE 95 AV. SUR Y 95 AV. BIS SUR, COLONIA TAXISTAS Y MARTILLOS', '9871189542', 'control_24@hotmail.com', NULL, NULL, 'Quintana Roo', 'COZUMEL', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1043, 'PARTICULAR', '130', NULL, 'C. JUAN ANTONIO CONG MORA', 'DISPENSADOR DE AGUA PURIFICADA LA NORIA', '9831397113', NULL, 'C. JUAN ANTONIO CONG MORA',
        'AV. CHETUMAL #312, MANZANA 105, LOTE 12 ENTRE TELA Y VALLEHERMOSO', '9831397113', 'tinytons@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1044, 'PARTICULAR', '131', NULL, 'C. SAHIRELY YOHANA CONSTANTINO SANTOS', 'YAZID PIZZA', '9831587517', NULL, 'C. SAHIRELY YOHANA CONSTANTINO SANTOS',
        'MAXUXAC ENTRE 30 DE NOVIEMBRE Y 10 DE ABRIL', '9831587517', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1045, 'PARTICULAR', '955', NULL, 'D. EN C. ARMIDA ZUÑIGA ESTRADA', 'COMISIÓN DE CONTROL ANALÍTICO Y AMPLIACIÓN DE COBERTURA', '50805200/ 018000335050', NULL,
        'D. EN C. ARMIDA ZUÑIGA ESTRADA', 'CALZADA DE TALPAN N° 4492, COLONIA TORIELLO GUERRA', '50805200/ 018000335050', 'avargas@cofepris.gob.mx / agalindo@cofe', NULL, NULL,
        'México', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1046, 'PARTICULAR', '102', NULL, 'C. MARGARITA HERNANDEZ GOMEZ', 'POLLOS ASADOS PIC NIC', '9831090259', NULL, 'C. MARGARITA HERNANDEZ GOMEZ',
        'ALTOS DE SEVILLA CON COROZAL Y 27 DE SEPTIEMBRE', '9831090259', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1047, 'PARTICULAR', '109', NULL, 'C. ERICK DANIEL CAUICH CERINO', 'ASADERO EDIER', '9831313981', NULL, 'C.ERICK DANIEL CAUICH CERINO',
        'AVENIDA NICOLAS BRAVO, MANZANA 229, LOTE 2, COLONIA SOLIDARIDAD', '9831313981', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1048, 'PARTICULAR', '105', NULL, 'C. ANA BERTHA MUÑOZ POOT', 'COCINA ECONOMICA ANITA', '9831317642', NULL, 'C. ANA BERTHA MUÑOZ POOT',
        'AVENIDA JUAREZ #209, COLONIA CENTRO', '9831317642', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1049, 'PARTICULAR', '133', NULL, 'C. ROSA MARIA GONZALEZ ROSALES', 'RESTAURANTE LA CHIAPANECA', '9838358802', NULL, 'C. ROSA MARIA GONZALEZ ROSALES',
        'MERCADO LÁZARO CÁRDENAS LOCALES 1-6, 14, 15, 20-30, 34-38', '9838358802', 'ferdinando060477@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1050, 'PARTICULAR', '135', NULL, 'C. FIDAH ELJURE ELJURE', 'RESTAURANTE BACUS STEAK HOUSE', '9831075572', NULL, 'C. FIDAH ELJURE ELJURE',
        'AVENIDA CARMEN OCHOA DE MERINO #101', '9831075572', 'carlos.csoto@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1051, 'PARTICULAR', '159', NULL, 'C. KARLA ISABEL VERNON MENDEZ', 'COCINA ECONOMICA LAS DELICIAS', '2854594', NULL, 'C. KARLA ISABEL VERNON MENDEZ',
        'CALZADA VERACRUZ #259', '2854594', 'isabelle77@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1052, 'PARTICULAR', '136', NULL, 'C. CYNTHIA GUADALUPE BELTRAN LOYA', 'PANADERIA LA GRACIA DE DIOS', '9831673153, 9831641582', NULL, 'C. CYNTHIA GUADALUPE BELTRAN LOYA',
        'AV. ISLA CANCUN #429, COL. 20 DE NOVIEMBRE', '9831673153, 9831641582', 'vanepinks2a@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1053, 'PARTICULAR', '137', NULL, 'C. ROXANA YULIETH ALAMILLA VARGAS', 'PANADERIA SAN JOSE', '9831673153', NULL, 'C. ROXANA YULIETH ALAMILLA VARGAS',
        'CALLE CHETUMAL ESQUINA RAUDALES, MANZANA 152, COL. SOLIDARIDAD', '9831673153', 'vanepink52a@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1054, 'PARTICULAR', '139', NULL, 'C. IDELFONSO RAYMUNDO CAMARA', 'PANADERIA CLARA LORENA', '9831772081', NULL, NULL, 'LUIS MOYA #480 ENTRE BENJAMIN HILL E INSURGENTES',
        '9831772081', 'Idelfonso_Camara@outlouk.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1055, 'PARTICULAR', '140', NULL, 'C. MANUELA AGUSTINA MAY BE', 'TORTILLERIA EMMANUEL', '9831866084', NULL, 'MANUELA AGUSTINA MAY BE', 'ARUBA #259 POR DOMINICA Y NARANJAL',
        '9831866084', 'h_martin68@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1056, 'PARTICULAR', '141', NULL, 'C. ALEJANDRA VENEGAS HERNANDEZ', 'COCINA ECONOMICA LA MUÑECA', '9838399104', NULL, 'C. ALEJANDRA VENEGAS HERNANDEZ',
        'CALZADA VERACRUZ #235-A, COLONIA ADOLFO LOPEZ MATEOS', '9838399104', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1057, 'PARTICULAR', '142', NULL, 'C. WILBERTH DAVID UC UH', 'ANTOJITOS Y EMPANADAS DOÑA MECHE', '9838367717', NULL, 'C. WILBERTH DAVID UC UH',
        'AVENIDA YUCATAN #44, CALDERITAS, Q. ROO', '9838367717', 'roslinlurline@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1058, 'PARTICULAR', '143', NULL, 'C. MARIA EMILIA ARANO RUIZ', 'POLLERIA EL PAISANO', '9838367717', NULL, 'C. MARIA EMILIA ARANO RUIZ',
        'AVENIDA YUCATAN #330, COL. YUCATAN, CALDERITAS, Q. ROO', '9838367717', 'roslinlurline@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1059, 'PARTICULAR', '144', NULL, 'C. MARIA ALEJANDRA VENEGAS HERNANDEZ', 'COCINA ECONOMICA LA MUÑECA', '9838399104', NULL, 'C. MARIA ALEJANDRA VENEGAS HERNANDEZ',
        'CALZADA VERACRUZ #235-A, COLONIA ADOLFO LOPEZ MATEOS', '9838399104', 'alejandra_venegas1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1060, 'PARTICULAR', '145', NULL, 'C. HILDA MARÍA HEREDIA GONZALEZ', 'TAQUERIA LA VECI', '9831641012', NULL, 'C. HILDA MARIA HEREDIA GONZALEZ',
        'AVENIDA MAHATMA GANDHI ENTRE JUAREZ', '9831641012', 'caifan99@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1061, 'PARTICULAR', '146', NULL, 'C. MARIA ELBA ELIZABETH CARRANZA AGUIRRE', 'SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA (CIPI)', '9837538611', NULL,
        'C. MARIA ELBA ELIZABETH CARRANZA AGUIRRE', 'AV. 19 O LIBRAMIENTO ENTRE CALLE 12 Y 20 S/N., COL. SERAPIO FLOTA MASS', '9837538611', 'jorgehernandecipi@gmail.com', NULL,
        NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1062, 'PARTICULAR', '147', NULL, 'C. GLORIA LIZABEL MORA CASTILLO', 'PURIFICADORA AGUA CELESTIAL 2', '9831040397', NULL, 'C. GLORIA LIZABEL MORA CASTILLO',
        'AVENIDA MAXUXAC #241 ENTRE GUILLERMO LOPEZ Y 8 DE OCTUBRE', '9831040397', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1063, 'PARTICULAR', '148', NULL, 'C. GLORIA LIZABEL MORA CASTILLO', 'PURIFICADORA AGUA CELESTIAL 3', '9831040397', NULL, 'C. GLORIA LIZABEL MORA CASTILLO',
        'AVENIDA BUGAMBILIAS #641 ENTRE FLOR DE MAYO Y FLOR DE LIZ', '9831040397', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1064, 'PARTICULAR', '149', NULL, 'C. SANTA INES IRULA RAMIREZ', 'MARACAS JUICE BAR', '9831251163', NULL, 'C. SANTA INES IRULA RAMIREZ',
        'ANDADOR 19 A # 53 SOBRE 4 DE MARZO ENTRE TABI Y TELA, COL. FIDEL VELAZQUEZ', '9831251163', 'santy_80@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1065, 'PARTICULAR', '150', NULL, 'C. SERGIA YOHANA PEREZ HERNANDEZ', 'ESTANCIA INFANTIL SHALOM', '9831073096', NULL, 'C. SERGIA YOHANA PEREZ HERNANDEZ',
        'CALLE CRECENCIO REJON #516', '9831073096', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1066, 'PARTICULAR', '150', NULL, 'C. SERGIA YOHANA PEREZ HERNANDEZ', 'ESTANCIA INFANTIL SHALOM', '9831073096', NULL, 'C. SERGIA YOHANA PEREZ HERNANDEZ',
        'CALLE MANUEL CRECENCIO REJÓN # 516 ENTRE MAGISTERIO Y MIGUEL ALEMÁN', '9831073096', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1067, 'PARTICULAR', '151', NULL, 'C. MARTINA RAMIREZ OCAMPO', 'ESTANCIA INFANTIL ELIZABETH', '9831545390', NULL, 'C. MARTINA RAMIREZ OCAMPO',
        'AVENIDA MACHUXAC, MANZANA 75, LOTE 22', '9831545390', 'martyna_6@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1068, 'PARTICULAR', '152', NULL, 'C. LAURA HAZEL GOMEZ RUIZ', 'ESTANCIA INFANTIL CASITA DE POCOYO', '9831130951', NULL, 'C. LAURA HAZEL GOMEZ RUIZ',
        'AVENIDA CEDRO #136-A, COLONIA DEL BOSQUE', '9831130951', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1069, 'PARTICULAR', '153', NULL, 'C. CRISTINA GUADALUPE LARA QUIÑONES', 'ESTANCIA INFANTIL PLAZA ACADÉMICA DA VINCI', '9831079017', NULL,
        'C. CRISTINA GUADALUPE LARA QUIÑONES', 'AV. NAPOLES #359 ENTRE ISLA CANCUN Y LAGUNA DE BACALAR, COL. BENITO JUAREZ', '9831079017', 'crizzty@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1070, 'PARTICULAR', '154', NULL, 'C. JOSE LUIS GOMEZ CANCHE', 'TORTILLERIA SAN JOSE', '9837530331', NULL, 'C. JOSE LUIS GOMEZ CANCHE',
        'AVENIDA NICOLAS BRAVO #457, FRACCIONAMIENTO CARIBE', '9837530331', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1071, 'PARTICULAR', '155', NULL, 'C. MARIA JOSE GARCIA MARTINEZ', 'TAQUERIA ZAPATA', '9832854259', NULL, 'C. MARIA JOSE GARCIA MARTINEZ', 'CALLE EMILIANO ZAPATA #51-A',
        '9832854259', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1072, 'PARTICULAR', '156', NULL, 'C. OMAR ALEJANDRO AGUILAR LOPEZ', 'JUGUERIA LOS 2 HERMANOS', '9831831899', NULL, 'C. AGUILAR LOPEZ OMAR ALEJANDRO',
        'AV. BELICE ESQUINA CNC #60-A, UNIDAD HAB. FRANCISCO J. MUJICA', '9831831899', 'omarlopez_0313@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1073, 'PARTICULAR', '157', NULL, 'C. LOURDES MARIA PATRON MORCILLO', 'PIZZERIA LA TERRAZA DE ROMANIS', '9831245427', NULL, 'C. LOURDES MARIA PATRON MORCILLO',
        'CALLE LUIS I. RODRIGUEZ #1, COLONIA GONZALO GUERRERO', '9831245427', 'lou_mar28@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1074, 'PARTICULAR', '158', NULL, 'C. JOSE MANUEL IGNACIO OSORIO ESTRELLA', 'DISTRIBUIDORA DE MARISCOS SAN JOSE', '9838386555', NULL,
        'C. JOSE MANUEL IGNACIO OSORIO ESTRELLA', 'AVENIDA INSURGENTES #132, COLONIA ADOLFO LOPEZ MATEOS', '9838386555', 'mariscossanjose@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1075, 'PARTICULAR', '159', NULL, 'C. CARLOS ALBERTO OJEDA DE LA FUENTE Y LEON', 'KINDERPLAY JAQUES DECORS ESCUELA PREESCOLAR', '9838360023', NULL,
        'C. CARLOS ALBERTO OJEDA DE LA FUENTE Y LEON', 'JESUS REYES HEROLES # 35, COLONIA GONZALO GUERRERO', '9838360023', 'kinder.play.delocs@gmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1076, 'PARTICULAR', '159', NULL, 'C. EVA LUNA GOMEZ', 'LONCHERIA CECI', '9838330516', NULL, 'C. EVA LUNA GOMEZ', 'POBLADO HUAY PIX, Q.ROO CARRETERA FEDERAL', '9838330516',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1077, 'PARTICULAR', '160', NULL, 'C. MANUEL GARCIA PANTOJA', 'RESTAURANT PANTOJA', NULL, NULL, 'C. MANUEL GARCIA PANTOJA', 'MAHATMA GANDHI #181', NULL, NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1078, 'PARTICULAR', '956', NULL, 'SRA. MARIA TERESA SANCHEZ GARCIA', 'COCINA Y LONCHERIA VENTA DE COMIDAS Y ANTOJITOS "LA TABASQUEÑA"', '9831179913', NULL,
        'SRA. MARIA TERESA SANCHEZ GARCIA', 'AVENIDA JUSTO SIERRA #607', '9831179913', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1079, 'PARTICULAR', '957', NULL, 'C. RANULFO ABRAHAM GONZALEZ PALAFOX', 'TAQUERIA EL BUEN FOGON', '9832119989', NULL, 'C. RANULFO ABRAHAM GONZALEZ PALAFOX',
        'AVENIDA JAVIER ROJO GOMEZ ESQUINA CHUNYAH # 481', '9832119989', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1080, 'PARTICULAR', '650', NULL, 'C. NADIA VANESSA FLOTA GONZALEZ', 'LONCHERIA LOS ABUELOS', '9831685162', NULL, 'C. NADIA VANESSA FLOTA GONZALEZ',
        'CALLE RIO VERDE ENTRE AVENIDA MAGISTERIO CON NIZUC', '9831685162', 'nati_qoocry@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1081, 'PARTICULAR', '649', NULL, 'C. ROCIO DZIB MEDINA', 'TORTILLERIA LOS PRIMOS', '9831238870', NULL, 'C. ROCIO DZIB MEDINA',
        'AVENIDA BUGAMBILIAS #324 COLONIA JESUS MARTINEZ ROSS', '9831238870', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1082, 'PARTICULAR', '648', NULL, 'C. ARIEL VALENCIA ARRONTE', 'JUGUERIA, LONCHERIA SUPERFRUTY', '9831000402', NULL, 'C. ARIEL VALENCIA ARRONTE',
        'AVENIDA EFRAIN AGUILAR N°264 COLONIA CENTRO', '9831000402', 'arielva21@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1083, 'PARTICULAR', '646', NULL, 'C. MARIA ISABEL GOMEZ PACHECO', 'TAQUERIA EL AMIGO', '9831120301', NULL, 'C. MARIA ISABEL GOMEZ PACHECO',
        'CALZADA VERACRUZ #455 ENTRE INSURGENTES Y MARCIANO GONZALEZ', '9831120301', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1084, 'PARTICULAR', '645', NULL, 'FUMIGACIONES ESPECIALIZADAS Y SERVICIOS INTEGRALES JIREH', NULL, '9841701864', NULL,
        'FUMIGACIONES ESPECIALIZADAS Y SERVICIOS INTEGRALES JIREH', 'AVENIDA 95 ENTRE 12 Y 14 LOCAL 2, PLAYA DEL CARMEN, Q.ROO', '9841701864', 'agudiazy@hotmail.com', NULL, NULL,
        'Quintana Roo', 'SOLIDARIDAD', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1085, 'PARTICULAR', '643', NULL, 'C. SONIA E. DIAZ LOPEZ', 'ANTOJITOS LAS NEGRITAS', '9838359944', NULL, 'C. SONIA E. DIAZ LOPEZ',
        'AVENIDA MAXUXAC ENTRE AVENIDA CHETUMAL Y ALFREDO B. BONFIL', '9838359944', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1086, 'PARTICULAR', '642', NULL, 'C. MARIA DORIS EUNICE HEREDIA CAAMAL', 'AGUA PURIFICADA EMMANUEL (DIOS CON NOSOTROS)', '9831118004', NULL,
        'C. MARIA DORIS EUNICE HEREDIA CAAMAL', 'AV. CORNELIO LIZARRAGA, MANZANA 44, LOTE 17, COL. FORJADORES', '9831118004', 'lombera_1972@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1087, 'PARTICULAR', '641', NULL, 'C. ABRAHAM ZAPATA CASTILLO', 'LONCHERIA LA CENTRAL CAMIONERA', '9831356607', NULL, 'C. ABRAHAM ZAPATA CASTILLO',
        'CALLE PRIVADA JUAN JOSE SIORDIA CON PALERMO', '9831356607', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1088, 'PARTICULAR', '640', NULL, 'C. RUBEN DARIO PEREZ MENDEZ', 'POLLERIA SAN RAFAEL', '9831090955', NULL, 'C. RUBEN DARIO PEREZ MENDEZ',
        'CALLE C.N.C. #127 COLONIA ADOLFO LOPEZ MATEOS', '9831090955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1089, 'PARTICULAR', '639', NULL, 'C. DIANA CAROLINA CARPIZO SANGUINO', 'CHACTE BEBIDAS', '9811604861', NULL, 'C. DIANA CAROLINA CARPIZO SANGUINO',
        'CALLE ISLA CAIMAN #330 MANZANA 4 FRACCIONAMIENTO CARIBE', '9811604861', 'dianacarpizo4@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1090, 'PARTICULAR', '638', NULL, 'C. JULIO CESAR AVILA RODRIGUEZ', 'ASADERO SAN RAFAEL', '9831047381', NULL, 'C. JULIO CESAR AVILA RODRIGUEZ',
        'CALLE RIO VERDE #317 COLONIA SOLIDARIDAD', '9831047381', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1091, 'PARTICULAR', '637', NULL, 'C. RICARDO ALBERTO PEREZ FLORES', 'TAQUERIA CHILES MEXICANOS', '9837337625', NULL, 'C. RICARDO ALBERTO PEREZ FLORES',
        'CALLE CHAPULTEPEC #2 ESQUINA BOULEVARD BAHIA', '9837337625', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1092, 'PARTICULAR', '636', NULL, 'C. GILDA RUEDA CABALLERO', 'RESTAURANT ECOTURISTICO CENOTE AZUL', NULL, NULL, 'C. GILDA RUEDA CABALLERO', 'BOULEVARD COSTERO I-610', NULL,
        NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1093, 'PARTICULAR', '635', NULL, 'C. MARISOL SANCHEZ VERA', 'ITACATE TAQUERIA', '9838332796', NULL, 'C. MARISOL SANCHEZ VERA', 'AVENIDA ANDRES Q. ROO #269', '9838332796',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1094, 'PARTICULAR', '634', NULL, 'C. MARIELA JIMENEZ LOPEZ', 'COCINA ECONOMICA EMMANUEL', '9831644605', NULL, 'C. MARIELA JIMENEZ LOPEZ',
        'AVENIDA JUAREZ #227 COLONIA CENTRO', '9831644605', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1095, 'PARTICULAR', '633', NULL, 'C. EDSON ARIEL HEREDIA PINEDA', 'TAQUERIA LOS ORIENTALES', '9831301476', NULL, 'C. EDSON ARIEL HEREDIA PINEDA',
        'SAN SALVADOR #453-A ENTRE SICILIA Y PALERMO', '9831301476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1096, 'PARTICULAR', '632', NULL, 'LOS ALUXES BACALAR S.A. DE C.V.', NULL, '9831098755', NULL, 'LOS ALUXES BACALAR S.A. DE C.V.', 'COSTERA N°69, COLONIA MAGISTERIAL',
        '9831098755', 'aluxes.bacalar@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1097, 'PARTICULAR', '631', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO', 'PURIFICADORA LA CASITA DEL AGUA', '9838390722', NULL, 'C. ANDRES LUCIANO GARCIA ROSADO',
        'AV. ERICK PAOLO #190 ENTRE XTACAY Y CHACHALACA, COLONIA PAYO OBISPO', '9838390722', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1098, 'PARTICULAR', '630', NULL, 'C. LETICIA GARCIA CRUZ', 'MISTER POLLO (POLLOS ASADOS Y EMPANIZADOS)', '9831162524', NULL, 'C. LETICIA GARCIA CRUZ',
        'AVENIDA NICOLAS BRAVO, LOTE 18, MANZANA 166, COLONIA SOLIDARIDAD', '9831162524', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1099, 'PARTICULAR', '628', NULL, 'C. MARIA ARGELIA NOH CHULIN', 'PASTELERIA ARGELIA', '9838338727, 8375207', NULL, 'C. MARIA ARGELIA NOH CHULIN',
        'CALLE 12, MANZANA 70, LOTE 16, COLONIA GUADALUPE VICTORIA', '9838338727, 8375207', 'mariaargelianoh@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1100, 'PARTICULAR', '102', NULL, 'C. JUAN MIGUEL NUÑEZ CUPIL', 'COCKTELERIA DE MARISCOS', '9831108543', NULL, 'C. JUAN MIGUEL NUÑEZ CUPIL',
        'AVENIDA CONSTITUYENTES #471 ENTRE REYES HEROLES Y LOTE 6', '9831108543', 'peluco.2410@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1101, 'PARTICULAR', '160', NULL, 'C. ELMER SOSA HERNANDEZ', NULL, '9831544790', NULL, 'C. ELMER SOSA HERNANDEZ',
        'CALLE TECNOLOGICO DE VERACRUZ #451 ENTRE MAGISTERIAL Y NIZUC', '9831544790', 'sh_elmer@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1102, 'PARTICULAR', '125', NULL, 'C. ROBERTO BELTRAN MEDINA', 'DISPENSADOR DE AGUA PURIFICADA RIO AZUL', '9831207172', NULL, 'C. ROBERTO BELTRAN MEDINA',
        'AV. ERICK PAOLO MTZ. # 384, MANZANA 117, LOTE 14, ENTRE NICOLAS BRAVO Y SACXAN', '9831207172', 'rbeltranm74@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1103, 'PARTICULAR', '128', NULL, 'C. ROBERTO BELTRAN MEDINA', 'DISPENSADOR DE AGUA PURIFICADA RIO AZUL', '9831207172', NULL, 'C. ROBERTO BELTRAN MEDINA',
        'AV. JUAREZ #344 ENTRE LAGUNA DE BACALAR Y ESTERO DE UCUM, COL. DAVID GUSTAVO', '9831207172', 'rbeltranm74@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1104, 'PARTICULAR', '129', NULL, 'C. ROBERTO BELTRAN MEDINA', 'DISPENSADOR DE AGUA PURIFICADA RIO AZUL', '9831207172', NULL, 'C. ROBERTO BELTRAN MEDINA',
        'AV. BUGAMBILIAS # 271-B POR MADERO Y MORELOS, COL. DAVID GUSTAVO', '9831207172', 'rbeltranm74@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1105, 'PARTICULAR', '130', NULL, 'C. ROBERTO BELTRAN MEDINA', 'DISPENSADOR DE AGUA PURIFICADA RIO AZUL', '9831207172', NULL, 'CALLE COAHUILA #235, CALDERITAS',
        'CALLE COAHUILA #235, CALDERITAS, Q.ROO', '9831207172', 'rbeltranm74@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1106, 'PARTICULAR', '131', NULL, 'C. ROBERTO BELTRAN MEDINA', 'DISPENSADOR DE AGUA PURIFICADA RIO AZUL', '9831207172', NULL, 'C. ROBERTO BELTRAN MEDINA',
        'AV. CHETUMAL, MANZANA 216, LOTE 5, COL. SOLIDARIDAD, ENTRE TIHOSUCO', '9831207172', 'rbeltranm74@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1107, 'PARTICULAR', '627', NULL, 'C. ANALLELY DIONICIO DE LA CRUZ', 'ESCUELA HIDALGO (TIENDA ESCOLAR)', '8320872', NULL, 'C. ANALLELY DIONICIO DE LA CRUZ',
        'AVENIDA 16 DE SEPTIEMBRE N° 28', '8320872', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1108, 'PARTICULAR', '199', NULL, 'C. JUDITH FELIGRANA KIM', 'LONCHERIA LICO´S', '9831665911', NULL, 'C. JUDITH FILIGRANA KIM',
        'HEROICO COLEGIO MILITAR #84 ENTRE CALZADA VERACRUZ Y COZUMEL', '9831665911', 'andre-contre-19@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1109, 'PARTICULAR', '625', NULL, 'C. FATIMA AGUILAR GUTIERREZ', 'PRIMARIA SANTIAGO PACHECO CRUZ (COOPERATIVA ESCOLAR)', '9831318958', NULL, 'C.FATIMA AGUILAR GUTIERREZ',
        'JUSTO SIERRA #192 ENTRE HEROES Y BELICE', '9831318958', 'pata73ag@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1110, 'PARTICULAR', '624', NULL, 'C. LIGIA ALINA CHUC PECH', 'TAQUERIA EL TROMPO', '9831108096', NULL, 'C.LIGIA ALINA CHUC PECH',
        'ERICK PAOLO MARTINEZ, MANZANA 21, LOTE 20, LOCAL 2', '9831108096', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1111, 'PARTICULAR', '623', NULL, 'C. ADOLFO RAFAEL PEREZ VALENCIA', 'GRUPO OPERACIONES UNIDAS SAPI DE C.V. (WINGS ARMY)', '9831762721', NULL,
        'ADOLFO RAFAEL PERAZ VALENCIA', 'AV. ERICK PAOLO MARTINEZ, LOCAL 36, COL. PARQUE INDUSTRIAL', '9831762721', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1112, 'PARTICULAR', '622', NULL, 'C. LUIS JAVIER PUC SANSORES', 'CAFETERIA ESCOLAR DE BACHILLERES DOS', '9831766989', NULL, 'LUIS JAVIER PUC SANSORES',
        'AVENIDA CONSTITUYENTES S/N. CON TELA', '9831766989', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1113, 'PARTICULAR', '753', NULL, 'C. HUIYAO CHEN', 'COMIDA CHINA ZHANG', '9831077208', NULL, 'C. HUIYAO CHEN', 'AVENIDA ALVARO OBREGON ENTRE MADERO Y MORELOS',
        '9831077208', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1114, 'PARTICULAR', '621', NULL, 'C. MARIA DEL CARMEN CHULIM GONZALEZ', 'TAQUERIA DON CALDO', '9838326144', NULL, 'MARIA CARMEN CHULIM GONZALEZ',
        'CALLE GRANADILLO # 334 ENTRE ISLA CANCUN Y BUGAMBILIAS, COL. ADOLFO LOPEZ MATEOS', '9838326144', 'pastorlopez62@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1115, 'PARTICULAR', '666', NULL, 'C. SILVIA HU PECH', 'TORTILLERIA CENTER II', '9831401222', NULL, 'C. SILVIA UH PECH',
        'CALLE CELUL, MANZANA 273, LOTE 6, COLONIA PROTERRITORIO', '9831401222', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1116, 'PARTICULAR', '620', NULL, 'TIENDAS SORIANA S.A. DE C.V.', NULL, '2673360', NULL, 'TIENDAS SORIANA S.A. DE C.V.',
        'AV. ERICK PAOLO MARTINEZ ENTRE CONSTITUYENTES # 120, COL. EL SOL', '2673360', 'GERENCIAS27@SORIANA.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1117, 'PARTICULAR', '667', NULL, 'C. ANGEL JESUS TUN ROSAS', 'RESTAURANTE LA TERRAZA', '9831238434', NULL, 'C. ANGEL JESUS TUN ROSAS',
        'AV. JOSE MARIA MORELOS #402-A, COL. LEONA VICARIO', '9831238434', 'lindaluz73@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1118, 'PARTICULAR', '161', NULL, 'C. JUAN ANTONIO CASTILLO PINZON', 'LA CASA DE LAS COMIDAS', NULL, NULL, 'C. JUAN ANTONIO CASTILLO PINZON',
        'ENRIQUE BAROCIO #1031 FRACCIONAMIENTO MILENIO', NULL, 'lindaluz73@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1119, 'PARTICULAR', '162', NULL, 'C. MARIA GUADALUPE PINEDA ORTIZ', 'DISPENSADOR DE AGUA PURIFICADA LA CALENTANA', '9831545459', NULL, 'C. MARIA GUADALUPE PINEDA ORTIZ',
        'LAGUNA DE BACALAR #468 ENTRE CORCEGA Y ANDADOR 2, COL. JOSEFA ORTIZ', '9831545459', 'maria guadalupe.pa20@icloud.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1120, 'PARTICULAR', '163', NULL, 'C. MARIA MILEDY RUBI BRICEÑO CALDERON', 'TORTILLERIA MILEDY RUBI', '9831731963/9831090955', NULL, 'C. MARIA MILEDY RUBI BRICEÑO CALDERON',
        'AVENIDA IGNACIO COMONFORT #98, COLONIA 5 DE ABRIL', '9831731963/9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1121, 'PARTICULAR', '619', NULL, 'C. JAVIER ESPARZA ROBLES', 'PLANTA PURIFICADORA DE AGUA OLIVER', '9831053394', NULL, 'JAVIER ESPARZA ROBLES',
        'CARRETERA ALVARO OBREGON AL INGENIO S/N., Q.ROO', '9831053394', 'rio_hondocb@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1122, 'PARTICULAR', '164', NULL, 'C. HELEN MABEL SEGURA GARCIA', 'PASTELERIA EL MERENGUE', '9831846367', NULL, 'C. HELEN MABEL SEGURA GARCIA',
        'PRIMO DE VERDAD #233, ENTRE INDEPENDENCIA Y JUAREZ, COL. VENUSTIANO CARRANZA', '9831846367', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1123, 'PARTICULAR', '165', NULL, 'C. MARIA NATIVIDAD ALCOCER SANCHEZ', 'COCINA ECONOMICA EL GOURMET', '9831824779', NULL, 'C. MARIA NATIVIDAD ALCOCER SANCHEZ',
        'CALZADA VERACRUZ #692, COLONIA DEL BOSQUE', '9831824779', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1124, 'PARTICULAR', '618', NULL, 'C. JOYTHER SALGADO REYES', 'JUGUERIA ALEX', '9831131162', NULL, 'JOYTHER SALGADO REYES',
        'AVENIDA BUGAMBILIAS # 440 ESQUINA AVENIDA SICILIA', '9831131162', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1125, 'INSTITUCIONAL', '617', NULL, 'DIRECTORA DEL LABORATORIO ESTATAL DE SALUD PÚBLICA', 'M. EN C. EN S.P. PILAR EUGENIA GRANJA PEREZ', '9999303050 EXT 45700', NULL,
        'M. EN C. EN S. P. PILAR EUGENIA GRANJA PÉREZ', 'CALLE 39-C POR SEGUNDA, COLONIA MAYAPAN', '9999303050 EXT 45700', NULL, NULL, NULL, 'Yucatán', 'Mérida', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1126, 'PARTICULAR', '616', NULL, 'C. MARGENY MAYANIN CHIN CHABLE', 'MOVIMIENTO EDUCATIVO VALLADOLID A.C.', NULL, NULL, 'MARGENY MAYANIN CHIN CHABLE',
        'CALLE MARCIANO GONZALEZ # 136 ENTRE HEROES, COLONIA ADOLFO LOPEZ MATEOS', NULL, 'CHETUMAL@SISTEMAVALLADOLID.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1127, 'PARTICULAR', '615', NULL, 'C. EMANUEL JONATAN LOPEZ VAZQUEZ', 'PARRIPOLLO', '9832855627', NULL, 'EMANUEL JONATAN LOPEZ VAZQUEZ',
        'AVENIDA CONSTITUYENTES DEL 74 S/N., MANZANA 498, LOTE 15', '9832855627', 'VHPC9524@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1128, 'PARTICULAR', '614', NULL, 'C. BENITO LÓPEZ PEREZ', 'PARRIPOLLO', '9831207523', NULL, 'BENITO LÓPEZ PEREZ', 'CALZADA VERACRUZ # 431, COLONIA ADOLFO LOPEZ MATEOS',
        '9831207523', 'VHPC9524@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1129, 'PARTICULAR', '613', NULL, 'C. OFELIA LOPEZ PEREZ', 'PARRIPOLLO', '9831207523', NULL, 'OFELIA LOPEZ PEREZ',
        'AV. MAXUXAC, MANZANA 381, LOTE 09, COLONIA PROTERRITORIO', '9831207523', 'vhpc.9594@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1130, 'PARTICULAR', '613', NULL, 'C. OFELIA LOPEZ PEREZ', 'PARRIPOLLO', '2855627', NULL, 'C. OFELIA LOPEZ PEREZ', 'AVENIDA MAXUXAC, MANZANA 381, LOTE 9', '2855627',
        'vhpc9524@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1131, 'PARTICULAR', '164', NULL, 'C. OFELIA LOPEZ PEREZ', 'PARRIPOLLO', '2855627', NULL, 'C. OFELIA LOPEZ PEREZ', 'AVENIDA MAXUXAC, MANZANA 381, LOTE 09', '2855627',
        'vhpc9524@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1132, 'PARTICULAR', '612', NULL, 'C. LUIS ALFONSO PROTONOTARIO SABIDO', 'PANADERÍA LA INVENCIBLE CARRANZA', '9831030875', NULL, 'LUIS ALFONSO PROTONOTARIO SABIDO',
        'AVENIDA VENUSTIANO CARRANZA # 390', '9831030875', 'LAINVENCIBLE.CARRANZA@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1133, 'PARTICULAR', '611', NULL, 'C. ELIA MARÍA FERIA ASCENSIO', 'COCINA ECONOMICA POLLOS ASADOS', '9837324414', NULL, 'ELIA MARÍA FERIA ASCENSIO',
        'AV. CHETUMAL, MANZANA 6, LOTE 10 ENTRE PASCUAL CORAL, COL. LÁZARO CÁRDENAS', '9837324414', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1134, 'PARTICULAR', '199', NULL, 'C. DULCE MARIA NOVEROLA UH', 'RESTAURANT CHUHUC', '9831209533', NULL, 'C.DULCE MARIA NOVELO UH', 'RAMON LOPEZ VELARDE #346', '9831209533',
        'chuhuc@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1135, 'PARTICULAR', '610', NULL, 'C. DULCE MARÍA ESPÍRITU SÁNCHEZ', 'TAQUERÍA LOS REYES DEL SABOR', '9831331696', NULL, 'DULCE MARÍA ESPÍRITU SÁNCHEZ',
        'AV. MAXUXAC S/N. ENTRE 10 DE ABRIL Y 30 DE NOVIEMBRE', '9831331696', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1136, 'PARTICULAR', '609', NULL, 'C. OMAR ALBERTO DIAZ TAH', 'LONCHERIA COLON', '983 129 2048', NULL, 'OMAR ALBERTO DIAZ TAH',
        'HIDALGO # 21, ESQ. CARMEN OCHOA DE MERINO, COL. CENTRO', '983 129 2048', 'adiazt@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1137, 'PARTICULAR', '165', NULL, 'C. KIM VARGUEZ KENNY KUAN SAN', 'CAFETERIA HOSPITAL GENERAL', '9831803181', NULL, 'C.KIM VARGUEZ KENNY KUAN SAN',
        'AV. ANDRES QUINTANA ROO #399 ENTRE ISLA CANCUN Y JUAN JOSE SIORDIA', '9831803181', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1138, 'PARTICULAR', '608', NULL, 'C. IMELDA SUAREZ PADILLA', 'TIENDA ESCOLAR PRIMARIA LUIS DONALDO COLOSIO', '9831663686', NULL, 'C. IMELDA SUAREZ PADILLA',
        'CALLE 5 DE FEBRERO ENTRE CELUL Y REFORMA, COL. PROTERRITORIO', '9831663686', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1139, 'PARTICULAR', '607', NULL, 'C. JOSE MARÍA MANUEL PADILLA LEAL', 'LA PLAYITA BACALAR', '9838343068', NULL, 'JOSE MARIA MANUEL PADILLA LEAL',
        'AVENIDA COSTERA # 765 ESQUINA CALLE 26', '9838343068', 'IVETHE@LAPLAYITABACALAR.COM', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1140, 'PARTICULAR', '166', NULL, 'C. MARIAN LORIELLY MEDINA PEREZ', 'PASTELERIA MARIAN´S', '9831192595', NULL, 'MARIAN LORIELLY MEDINA PEREZ',
        'ENRIQUE BAROCIO, LOTE 1, MANZANA 40, COLONIA FORJADORES', '9831192595', 'briellymedina@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1141, 'PARTICULAR', '167', NULL, 'C. LUIS ALFONSO PROTONOTARIO ENCALADA', 'PANADERIA LA INVENCIBLE', '8320685', NULL, 'C. LUIS ALFONSO PROTONOTARIO ENCALADA',
        'CARMEN OCHOA DE MERINO #203', '8320685', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1142, 'PARTICULAR', '167', NULL, 'OPERADORA PARASOL S. DE R.L. DE C.V.', 'RESTAURANTE KAI-PEZ', '9831017376', NULL, 'OPERADORA PARASOL S. DE R.L. DE C.V.',
        'CALLE 20 Y AVENIDA 1, COSTERA', '9831017376', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1143, 'PARTICULAR', '168', NULL, 'C. IVAN VILLANUEVA RAMIREZ', 'JUGUERIA VI-SALUD', '2854259', NULL, 'C.IVAN VILLANUEVA RAMIREZ', 'AVENIDA 5 DE MAYO #23', '2854259',
        'kodashy_13@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1144, 'PARTICULAR', '169', NULL, 'C. GEOVANNY JAVIER ROSALES CHAB', 'TORTILLERIA LUPITA', '9831366025', NULL, 'GEOVANNY JAVIER ROSALES CHAB',
        'LAGUNA DE BACALAR #465 A ENTRE CORCEGA Y PALERMO', '9831366025', 'fermin_Pb@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1145, 'PARTICULAR', '170', NULL, 'C. RODRIGO ALPUCHE SANCHEZ', 'CAFETERÍA KAAPEHTERIA', '1621240, 9982933404', NULL, 'C. RODRIGO ALPUCHE SANCHEZ',
        'AVENIDA ALVARO OBRAGON #171, COLONIA CENTRO', '1621240, 9982933404', 'ralpsan@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1146, 'PARTICULAR', '985', NULL, 'C. NELLY ISABEL DZIB CAUICH', 'COMESTIBLES Y ALIMENTOS INTERNACIONALES S.A. DE C.V. (PIZZERIA LITTLE CAESARS)', '9982165648', NULL,
        'C. NELLY ISABEL DZIB CAUICH', 'AV. INSURGENTES #514 ESQUINA PALERMO, MANZANA 1, COL. GONZALO GUERRERO', '9982165648', 'nellyisabel71@gmail.com facturas.lcpche', NULL,
        NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1147, 'PARTICULAR', '171', NULL, 'C. MANUEL JESUS CHABLE KOH', 'PANADERIA ZAMNA', '9838372025', NULL, 'MANUEL JESUS CHABLE KOH', 'CALLE BUGAMBILIAS #756', '9838372025',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1148, 'PARTICULAR', '172', NULL, 'C. SUSANA ELENA SALA GONGORA', 'TORTAS EL MENONA', '9831547544', NULL, 'JORGE EDUARDO ZOGBY CHELUZA MARTINEZ',
        'AVENIDA VENUSTIANO CARRANZA #371-A', '9831547544', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1149, 'PARTICULAR', '174', NULL, 'C. JOSE JESUS VERGARA PEREZ', 'PURIFICADORA INSURGENTES', '9831307931', NULL, 'C. JOSE JESUS VERGARA ISLAS',
        'AVENIDA INSURGENTES #1035, COL. LAGUNITAS', '9831307931', 'ijvergaraislas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1150, 'PARTICULAR', '175', NULL, 'C. JOSE JESUS VERGARA ISLAS', 'PURIFICADORA INSURGENTES', '9831307931', NULL, 'C. JOSE JESUS VERGARA ISLAS', 'AVENIDA INSURGENTES 1035',
        '9831307931', 'ljvergaraislas@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1151, 'PARTICULAR', '176', NULL, 'C. JORGE LUIS VARGAS VAZQUEZ', 'PURIFICADORA TRES VALLES', '9831365329, 9831307931', NULL, 'C. JORGE LUIS VARGAS VAZQUEZ',
        'CALLE CHABLE, MANZANA 303, LOTE 02, COLONIA PROTERRITORIO', '9831365329, 9831307931', 'ljvergaraislas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1152, 'PARTICULAR', '177', NULL, 'C. LUCIA CUPUL PAREDES', 'JUGOS Y LICUADOS A LO NATURAL MAXUXAC', '9982621177', NULL, 'C. LUCIA CUPUL PAREDES',
        'AVENIDA MAXUXAC, LOTE 4, MANZANA 376 ENTRE NIZUC', '9982621177', 'paredescupul01@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1153, 'PARTICULAR', '178', NULL, 'C. VERONICA VILLEGAS LARA', 'LONCHERIA JAROCHA', '9831440461', NULL, 'C.VERONICA VILLEGAS LARA', 'ROJO GOMEZ CON JUSTO SIERRA',
        '9831440461', 'curiosa_72@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1154, 'PARTICULAR', '667', NULL, 'C. BETI DEBORA MUÑOA AGUILAR', 'HOTEL ABH', '9831206370', NULL, 'C. BETI DEBORA MUÑOA AGUILAR',
        'AVENIDA LAZARO CARDENAS #201, COLONIA CENTRO', '9831206370', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1155, 'PARTICULAR', '669', NULL, 'C. NORMAN ANGULO MACLIBERTY', 'HOTEL LOS COCOS OPERADORA DE SERVICIOS TURÍSTICOS DEL SUR S.A. DE C.V.', '9831206570', NULL,
        'C. NORMAN ANGULO MACLIBERTY', 'AVENIDA HEROES #134, COLONIA CENTRO', '9831206570', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1156, 'PARTICULAR', '699', NULL, 'C. MARCOS JOSE SOSA CABRERA', 'PURIFICADORA VITALIX', '9981548469', NULL, 'C. MARCOS JOSE SOSA CABRERA',
        'AV. BELICE #463, COL. LEY FEDERAL DE AGUA ENTRE ISLA CANCUN Y LUIS CABRERA', '9981548469', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1157, 'PARTICULAR', '179', NULL, 'C. CLAUDIA CANTO MANZANILLA', 'TAQUERIA DE MARISCOS CARACOLEANDO', '9831236222', NULL, 'C. CLAUDIA CANTO MANZANILLA',
        'AVENIDA 4 DE MARZO #309 ESQUINA SEGUNDA PRIVADA DE PALERMO Y JUSTO SIERRA', '9831236222', 'bb_ere@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1158, 'PARTICULAR', '180', NULL, 'C. DULCE MARIA ESPIRITU SANCHEZ', 'TAQUERIA LOS REYES DEL SABOR', '9831331696', NULL, 'C. DULCE MARIA ESPIRITU SANCHEZ',
        'AVENIDA CONSTITUYENTES ESQUINA TERRITORIO FEDERAL', '9831331696', 'gorilon_89_16@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1159, 'PARTICULAR', '181', NULL, 'C. IGNACIO GOMEZ GOMEZ', 'POLLOS ASADOS NACHITO', '9831207523', NULL, 'C. IGNACIO GOMEZ GOMEZ',
        'CALLE CRISTOBAL COLON # 169, COLONIA CENTRO', '9831207523', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1160, 'PARTICULAR', '185', NULL, 'C. FIDELA SANHCEZ HERNANDEZ', 'RESTAURANT EL DELFIN', '9831163612', NULL, 'C. FIDELA SANHCEZ HERNANDEZ',
        'AVENIDA OAXACA LOCAL 15, CALDERITAS, Q. ROO', '9831163612', 'fide927@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1161, 'PARTICULAR', '190', NULL, 'C. FIDELA SANCHEZ HERNANDEZ', 'RESTAURANT EL DELFIN', '9831163612', NULL, 'C. FIDELA SANCHEZ HERNANDEZ',
        'AVENIDA OAXACA LOCAL 15, CALDERITAS, Q. ROO', '9831163612', 'fide927@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1162, 'PARTICULAR', '183', NULL, 'C. ALMA CONCEPCIÓN GONZALEZ GASPAR', 'RESTAURANT EL RINCON DE LA JAROCHITA', '9832854259', NULL, 'C. GONZALEZ GASPAR ALMA CONCEPCION',
        'AVENIDA BUGAMBILIAS #702', '9832854259', 'kodashi_13@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1163, 'PARTICULAR', '200', NULL, 'C. RICARDO ANTONIO ROMAN MARTINEZ', 'RESTAURANT BAR COCKTAIL', '9838360175', NULL, 'C. RICARDO ANTONIO ROMAN MARTINEZ',
        'AVENIDA OTHON P. BLANCO#32', '9838360175', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1164, 'PARTICULAR', '184', NULL, 'C. JOSEFINA TRINIDAD RODRIGUEZ', 'TORTILLERIA LA POBLANITA', '9831090955', NULL, 'C. JOSEFINA TRINIDAD RODRIGUEZ',
        'GENOVA # 318, COLONIA BENITO JUAREZ', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1165, 'PARTICULAR', '185', NULL, 'C. GRANIEL SALAZAR KANUL', 'ASADERO ALITAS SALAZAR', '9831142116', NULL, 'GRANIEL SALAZAR KANUL',
        'AV. 7 ENTRE CALLE 8 Y CALLE 10, COL. MAGISTERIAL', '9831142116', 'granielsalazar@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1166, 'PARTICULAR', '186', NULL, 'C. PASTOR DIEGO GUTIERREZ', 'PANADERIA DIEGO', '9831341864', NULL, 'PASTOR DIEGO GUTIERREZ',
        'CALLE 6, LOTE 10, MANZANA 63, COLONIA GUADALUPE VICTORIA', '9831341864', 'karendiegolorenzo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1167, 'PARTICULAR', '187', NULL, 'PRODUCTOS DE HARINA MICHUAQUE S.A. DE C.V.', 'TORTILLAS DE HARINA MICHUAQUE', '9831686754', NULL,
        'PRODUCTOS DE HARINA MICHUAQUE S.A DE C.V.', 'AVENIDA BUGAMBILIAS (FIDEL VELAZQUEZ SANCHEZ) #572', '9831686754', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1168, 'PARTICULAR', '188', NULL, 'C. AGRIPINO RODRIGUEZ MORALES', 'CASA SUJUYJA FABRICA DE VINOS', '9838336941', NULL, 'C. AGRIPINO RODRIGUEZ MORALES',
        'CALLE NIÑOS HÉROES DE CHAPULTPEC, LOCALIDAD BUENA ESPERANZA, Q.ROO', '9838336941', 'sujuyja7@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1169, 'PARTICULAR', '1', NULL, 'C. RICARDO JIMENEZ FAJARDO', 'PURIFICADORA UK JA', '9831810142', NULL, 'C. RICARDO JIMENEZ FAJARDO',
        'CALLE 23 #2268 ENTRE 30Y 32, COLONIA BENITO JUAREZ', '9831810142', 'frijerica@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1170, 'PARTICULAR', '1', NULL, 'C. RICARDO JIMENEZ FAJARDO', 'PURIFICADORA UK´JA´', '9831810142', NULL, 'C. RICARDO JIMENEZ FAJARDO',
        'CALLE 23 #2268 ENTRE 30 Y 32, COLONIA BENITO JUAREZ, BACALAR, Q. ROO', '9831810142', 'frinerica@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1171, 'PARTICULAR', '2', NULL, 'C. OSCAR GERARDO ARCE GUEVARA', 'SECUNDARIA GENERAL ARMANDO ESCOBAR NAVA', '9831205469', NULL, 'C. OSCAR GERARDO ARCE GUEVARA',
        'CALLE DOS AGUADAS S/N. ESQ. AV. CHETUMAL, COL. SOLIDARIDAD', '9831205469', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1172, 'PARTICULAR', '189', NULL, 'C. LAURA LEONOR ANDRADE CANCHE', 'ESCUELA IGNACIO RAMIREZ', '9831687235', NULL, 'C. LAURA LEONOR ANDRADE CANCHE',
        'CALLE CEDRO # 100, COL. DEL BOSQUE ENTRE J. MUJICA Y HERIBERTO FRIAS', '9831687235', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1173, 'PARTICULAR', '190', NULL, 'C. I-TSEN LAI', 'HSILAND BIO-TECH S.A. DE C.V.', '9831162943', NULL, 'C. I-TSEN LAI',
        'CALLE SINALOA CON VALLADOLID Y COAHUILA, CALDERITAS, Q. ROO', '9831162943', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1174, 'PARTICULAR', '191', NULL, 'C. SILVESTRE VARGAS VASQUEZ', 'CAFETERIA CONALEP', '9831333419', NULL, 'C. SILVESTRE VARGAS VASQUEZ', 'INSURGENTES CON FLOR DE MAYO',
        '9831333419', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1175, 'PARTICULAR', '191', NULL, 'C. SARA RANGEL MACIAS', 'CAFETERIA ESCOLAR FIDEL VELAZQUEZ', '9831130597', NULL, 'C. SARA RANGEL MACIAS',
        'ERICK PAOLO ENTRE 4 DE MARZO Y FAISAN', '9831130597', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1176, 'PARTICULAR', '192', NULL, 'C. SARA RANGEL MACIAS', 'CAFETERIA ESCOLAR FIDEL VELAZQUEZ', '9831130597', NULL, 'C. SARA RANGEL MACIAS',
        'ERICK PAOLO ENTRE 4 DE MARZO Y FAISAN', '9831130597', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1177, 'PARTICULAR', '193', NULL, 'C. LUIS ENRIQUE RIVERA GONZALEZ', 'SPEZIAS STEAK HOUSE WINE BAR', '9831191786, 9831269903', NULL, 'C. LUIS ENRIQUE RIVERA GONZALEZ',
        'PROLONGACIÓN B. BAHÍA ENTRE EMILIANO ZAPATA Y RAFAEL E. MELGAR COL. CENTRO', '9831191786, 9831269903', 'enriquerivera80@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1178, 'PARTICULAR', '194', NULL, 'C. MARIA ISABEL NAJERA PECH', 'CAFETERIA ESCOLAR 8 DE OCTUBRE', '9831847688', NULL, 'C. MARIA ISABEL NAJERA PECH',
        'BUGAMBILIAS ENTRE ROJO GOMEZ Y DONATO GUERRA, COL. 8 OCTUBRE', '9831847688', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1179, 'PARTICULAR', '197', NULL, 'C. ROGER MEDINA CAMPOS', 'TIENDA ESCOLAR AARON MERINO FERNANDEZ', '9831016324', NULL, 'C. ROGER MEDINA CAMPOS',
        'CALLE MANUEL ALTAMIRANO ENTRE FRANCISCO ZARCO Y FELIPE ANGELES', '9831016324', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1180, 'PARTICULAR', '196', NULL, 'C. WILMA MORALES MORALES', 'TORTILLERIA LA TABASQUEÑA', '9831090955, 9831731963', NULL, 'C. WILMA MORALES MORALES',
        'CALLE SANTA LUCIA, MANZANA 681, LOTE 15, COL. LAZARO CARDENAS', '9831090955, 9831731963', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1181, 'PARTICULAR', '198', NULL, 'C. CECILIA DE FATIMA HADAD ESTEFANO', 'RESTAURANT LAS ARRACHERAS DE DON JOSE', '9831012199', NULL, 'C. CECILIA DE FATIMA HADAD ESTEFANO',
        'AVENIDA OTHON P. BLANCO #62 CON BOULEVARD BAHIA', '9831012199', 'charlottemexico@live.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1182, 'PARTICULAR', '199', NULL, 'C. ANDRES RAMOS CABRERA', 'TORTILLERIA LA ANTORCHA', '9838356525', NULL, 'C. ANDRES RAMOS CABRERA',
        'CALLE GONZALO LOPEZ CID #18, LOTE 02, MANZANA 42, COL. UNIDAD ANTORCHISTA', '9838356525', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1183, 'PARTICULAR', '200', NULL, 'C.JABNEEL DE JESUS ARPAIS VELEZ', 'PIZZERIA PANCHOS', '9831142710', NULL, 'C.JABNEEL DE JESUS ARPAIS VELEZ',
        'AVENIDA NICOLAS BRAVO#720, LOTE 04, MANZANA 756', '9831142710', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1184, 'PARTICULAR', '202', NULL, 'C. LIGIA BEATRIZ OROZCO CHALE', 'TAQUERIA EL MILAGRO', NULL, NULL, 'C. LIGIA BEATRIZ OROZCO CHALE', 'CALZADA VERACRUZ #293', NULL, NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1185, 'PARTICULAR', '199', NULL, 'C. JOSE ALVARO MUÑOZ SABIDO', 'DISPENSADORA DE AGUA PURIFICADA WATER LAND', '9831039256', NULL, 'C. JOSE ALVARO MUÑOZ SABIDO',
        'EMILIO PORTES GIL #114 ENTRE JAIME VILLARUTIA Y LOPE DE VEGA, COL. MIRAFLORES', '9831039256', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1186, 'PARTICULAR', '200', NULL, 'C. ADOLFO ALBERTO AREVALO CORTEZ', 'LA CABAÑA DEL NEGRO', '9831669351', NULL, 'C. ADOLFO ALBERTO FLORES AREVALO',
        'AV. MIGUEL HIDALGO #65', '9831669351', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1187, 'PARTICULAR', '201', NULL, 'CAP.NAV. I.M. DEM. JOSE DE JESUS BONILA DOMINGUEZ', 'BATALLON DE INFANTERIA DE MARINA BIM-25', '9838322912', NULL,
        'CAP.NAV. I.M. DEM. JOSE DE JESUS BONILA DOMINGUEZ', 'AVENIDA INSURGENTES S/N. ESQUINA AVENIDA CHETUMAL, COLONIA LEONA VICARIO', '9838322912', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1188, 'PARTICULAR', '202', NULL, 'C. ENRIQUE PUC SANSOR', 'ESCUELA SECUNDARIA TECNICA 27', '9831322412', NULL, 'C. ENRIQUE PUC SANSOR',
        'URSULO GALVAN, MANZANA 415. LOTE 27, COLONIA LAZARO CARDENAS', '9831322412', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1189, 'PARTICULAR', '203', NULL, 'C. ALEJANDRO MONTALVO REYES', 'CAFETERIA ESCUELA DAVID ALFARO SIQUEIROS', '9831041460', NULL, 'C. ALEJANDRO MONTALVO REYES',
        'AVENIDA COMONFORT ENTRE CALZADA VERACRUZ Y CENTENARIO', '9831041460', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1190, 'PARTICULAR', '204', NULL, 'C. MARIA DEL CARMEN MORATO CASTRO', 'PURIFICADORA SAN ANTONIO', '9832672121, 9838670877', NULL, 'C. MARIA DEL CARMEN MORATO CASTRO',
        'AVENIDA ERICK PAOLO MARTINEZ, LOTE 8 Y 9, COLONIA SOLIDARIDAD', '9832672121, 9838670877', 'sanantoniobebidas@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1191, 'PARTICULAR', '205', NULL, 'C.ALMA ESTHER CASTRO LLANES', 'CAFETERIA SECUNDARIA ADOLFO LOPEZ MATEOS', '9831264019', NULL, 'C.ALMA ESTHER CASTRO LLANES',
        'ANDRES QUINTANA ROO ENTRE PLUTARCO ELIAS CALLES', '9831264019', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1192, 'PARTICULAR', '206', NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.', 'CARNICERIAS ABACSSA', NULL, NULL,
        'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.', 'FLOR DE LIZ # 111 Y BUGAMBILIAS, COLONIA JARDINES', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1193, 'PARTICULAR', '207', NULL, 'C. MONICA DE LOS ANGELES ARRIOLA ZEFERINO', 'CARNICERIA EL JAROCHITO', NULL, NULL, 'C. MONICA DE LOS ANGELES ARRIOLA ZEFERINO',
        'AVENIDA JAVIER ROJO GOMEZ S/N. , COL. PAYO OBISPO PRIMERA ETAPA', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1194, 'PARTICULAR', '208', NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.', 'CARNES DE CHETUMAL', NULL, NULL,
        'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V..', 'CALLE ARRECIFES # 321, COLONIA FELIX GONZALEZ CANTO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1195, 'PARTICULAR', '209', NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.', 'CARNICERIAS ABACSSA', NULL, NULL,
        'ABASTECEDORAS DE CARNICOS DEL SURESTE S.A. DE C.V.', 'PLUTARCO ELIAS CALLES #184, COLONIA CENTRO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1196, 'PARTICULAR', '210', NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.', 'CARNES DE CHETUMAL', NULL, NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.',
        'AVENIDA CONSTITUYENTES DEL 74, COLONIA PROTERRITORIO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1197, 'PARTICULAR', '211', NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.', 'CARNES DE CHETUMAL', NULL, NULL, 'ABASTECEDORA DE CARNICOS DEL SURESTE S.A. DE C.V.',
        'AV. CALZADA VERACRUZ #357 Y #359, COL. ADOLFO LOPEZ MATEOS', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1198, 'PARTICULAR', '212', NULL, 'C.SOFIA ABIGAIL XEC MARTIN', 'RESTAURANTE EL VATICANO', '8329174', NULL, 'C.SOFIA ABIGAIL XEC MARTIN',
        'MAHATMA GHANDI #198 ENTRE BELICE Y JUAREZ', '8329174', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1199, 'PARTICULAR', '213', NULL, 'C. RAGENEY ESPADAS PATRON', 'ROMANIS PIZZA', '9831024697', NULL, 'C. RAGENEY ESPACIOS PATRON',
        'AV. MAXUXAC, MANZANA 243, LOTE 8, COLONIA PROTERRITORIO', '9831024697', 'ney.espadas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1200, 'PARTICULAR', '214', NULL, 'C. GUSTAVO ALVAREZ HUERTA', 'LA CONDESA', '9831152890', NULL, 'C. GUSTAVO ALVAREZ HUERTA',
        'AV. CALZADA VERACRUZ # 44 ENTRE ALVARO OBREGON Y OTHON P. BLANCO', '9831152890', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1201, 'PARTICULAR', '215', NULL, 'C. LARISSA GABRIELA PEREZ ZAFRA', 'RESTAURANT BAR WINNER''S', '8321737', NULL, 'C. LARISSA GABRIELA PEREZ ZAFRA',
        'AVENIDA 22 DE ENERO #141', '8321737', 'ganador_facturas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1202, 'PARTICULAR', '216', NULL, 'C. MARIA BEATRIZ ROBLES MARTINEZ', 'DESPACHADOR DE AGUA PURIFICADA SAN DIEGO', '9831332308', NULL, 'C.MARIA BEATRIZ ROBLES MARTINEZ',
        'AV. AARON MERINO, LOTE 11, MANZANA 62 ESQUINA ONESIMO POOT', '9831332308', 'goyoqroo@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1203, 'PARTICULAR', '217', NULL, 'C. INES GAMBOA VELA', 'TORTILLERIA ARBOLEDAS', '9831046259', NULL, 'C. INES GAMBOA VELA', 'FLAMBOYAN #190, COLONIA ARBOLEDAS',
        '9831046259', 'tianechi@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1204, 'PARTICULAR', '219', NULL, 'C. GUADALUPE DE LA LUZ SOUZA MELENDEZ', 'JARDIN DE NIÑOS QUETZALCOATL', '9831433427', NULL, 'GUADALUPE DE LA LUZ SOUZA MELENDEZ',
        'AV. NAPOLES ESQUINA BUGAMBILIAS, COL. JOSEFA ORTIZ DE DOMINGUEZ S/N.', '9831433427', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1205, 'PARTICULAR', '220', NULL, 'C. ZDENKO BAKULA', 'BURGER TIME', '9831411556', NULL, 'ZDENKO BAKULA',
        'AV. BENITO JUAREZ #234-A, ENTRE VENUSTIANO CARRANZA Y PRIMO DE VERDAD', '9831411556', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1206, 'PARTICULAR', '221', NULL, 'C. SANDRA NAVA FABILA', 'CENTRO RECREATIVO ARCOIRIS', '983149980', NULL, 'C. SANDRA NAVA FABILA',
        'CALZADA DEL CENTENARIO #131 KM 131.4.5', '983149980', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1207, 'PARTICULAR', '222', NULL, 'LIC. NOEMI CAROLINA CONDE CANTO', 'HOTEL CAPITAL PLAZA', '9831548992', NULL, 'C. NOEMI CAROLINA CONDE CANTO',
        'AVENIDA HEROES #171-A, COLONIA CENTRO', '9831548992', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1208, 'PARTICULAR', '223', NULL, 'C. ADRIAN GUATEMALA CRUZ', 'ANTOJITOS TLAXCALLI', '9831302476', NULL, 'C. ADRIAN GUATEMALA CRUZ',
        'AVENIDA VENUSTIANO CARRANZA #211, COLONIA LAS CASITAS', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1209, 'PARTICULAR', '224', NULL, 'C. JOSE CAAMAL BALAM', 'VENTA DE ANTOJITOS', '9831801842', NULL, 'C. JOSE CAAMAL BALAM',
        'AVENIDA JUSTO SIERRA #688, MANZANA 26, LOTE 21, COLONIA 8 DE OCTUBRE', '9831801842', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1210, 'PARTICULAR', '225', NULL, 'C. MAXIMILIANA PECH MENDEZ', 'PANADERIA SAN ANTONIO', '9831838638', NULL, 'C. MAXIMILIANA PECH MENDEZ',
        'MARIANO ESCOBEDO #348 ENTRE MANUEL M. DIEGUEZ Y ESTEBAN B. CALDERON', '9831838638', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1211, 'PARTICULAR', '226', NULL, 'C. DIANA PATRICIA AGUILAR TORRES', 'LA VIA, RESTAURANT', '1273702, 98320354', NULL, 'C. DIANE PATRICIA AGUILAR TORRES',
        'AV. HEROES #83-4A', '1273702, 98320354', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1212, 'PARTICULAR', '227', NULL, 'C. CLELSI PINACHO NOLASCO', 'ANTOJITOS DOÑA ELSY', '1453338', NULL, 'CLELSI PINACHO NULASCO',
        'AV. CHETUMAL, MANZANA 206, LOTE 18, COLONIA SOLIDARIDAD', '1453338', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1213, 'PARTICULAR', '228', NULL, 'C. RAMIRO FERNANDO VAZQUEZ GOMEZ', NULL, '9831013492', NULL, 'C. RAMIRO FERNANDO VAZQUEZ GOMEZ',
        'AV. REVOLUCIÓN INTERIOR DEL AEROPUERTO INTERNACIONAL DE CHETUMAL', '9831013492', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1214, 'PARTICULAR', '228', NULL, 'C. ROGELIO RODRIGUEZ RODRIGUEZ', 'TORTILLERIA INNOMINADO', '9831716491', NULL, 'C. ROGELIO RODRIGUEZ RODRIGUEZ',
        'VICENTE GUERRERO, LOTE 14 #264', '9831716491', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1215, 'PARTICULAR', '228', NULL, 'C. ROGELIO RODRIGUEZ RODRIGUEZ', 'TORTILLERIA INNOMINADO', '9831716491', NULL, 'C. ROGELIO RODRIGUEZ RODRIGUEZ',
        'VICENTE GUERRERO # 264, LOTE 4', '9831716491', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1216, 'PARTICULAR', '230', NULL, 'C. CHRISTIAN SHARID TAPIA SALVATIERRA', 'SECUNDARIA TECNICA #2 TIENDA ESCOLAR', '9831070759', NULL,
        'C. CHRISTIAN SHARID TAPIA SALVATIERRA', 'AVENIDA ADOLFO LOPEZ MATEOS', '9831070759', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1217, 'PARTICULAR', '231', NULL, 'C. SUEMY DEL CARMEN PEREZ COLLI', 'ANTOJITOS LOS PUNCITOS', '8338331016', NULL, 'C. SUEMY DEL CARMEN PEREZ COLLI',
        'AVENIDA COROZAL #410 ESQUINA ANASTACIO GUZMAN', '8338331016', 'gtunhau@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1218, 'PARTICULAR', '0', NULL, 'C. ADELA REYES CAMARA', 'LONCHERIA LA FLOR DE NAZARETH', '9831406366', NULL, 'C. ADELA REYES CAMARA',
        'AV. ROJO GOMEZ ENTRE CELUL Y RETORNO 22', '9831406366', 'ADELE.REYES1@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1219, 'PARTICULAR', '11', NULL, 'C. GLADYS STEPHANY GOMEZ DOMINGUEZ', 'BOY´S PIZZA', '9831558934', NULL, 'C. GLADYS STEPHANY GOMEZ DOMINGUEZ',
        'CALZADA VERACRUZ #462 ENTRE INSURGENTES Y MARCIANO GONZALEZ', '9831558934', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1220, 'PARTICULAR', '229', NULL, 'C. ABIMAEL UC CORDOVA', 'PURIFICADORA AQUAMATICA', '9981597667', NULL, 'C. ABIMAEL UC CORDOVA',
        'RIO BRAVO ENTRE 20 DE NOVIEMBRE E INDEPENDENCIA, XUL-HA, Q.ROO', '9981597667', 'abja_1425@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1221, 'PARTICULAR', '230', NULL, 'C. ANDRES RODRIGUEZ MENESES', 'CARNICERIA LA GÜERA', '9831196192', NULL, 'C. ANDRES RODRIGUEZ MENESES', 'AVENIDA BUGAMBILIAS N°583',
        '9831196192', 'ELRUDOIRLANDES@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1222, 'PARTICULAR', '232', NULL, 'C. OLIVIA MATUS VAZQUEZ', 'POLLOS ASADOS DEL CARIBE', '9837531708', NULL, 'OLIVIA MATUS VAZQUEZ',
        'CURACAO #265 ENTRE DOMINICA Y NARANJAL, FRACC. CARIBE', '9837531708', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1223, 'PARTICULAR', '233', NULL, 'C. MARIA ELENA ASENCIO VILLAMIL', 'COLEGIO PARTICULAR INCORPORADO PRIMITIVO ALONSO S.C.', '98326419', NULL,
        'C. MARIA ELENA ASENCIO VILLAMIL', 'AVENIDA CENTENARIO #595', '98326419', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1224, 'PARTICULAR', '234', NULL, 'C. IRMA VERONICA KEB MONTERO', 'PURIFICADORA DE AGUA DEL BUEN DIA', '9831319861', NULL, 'C. IRMA VERONICA KEB MONTERO',
        'AVENIDA CONSTITUYENTES #872', '9831319861', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1225, 'PARTICULAR', '235', NULL, 'C. JOSE DIAZ N.', NULL, NULL, NULL, 'C. JOSE DIAZ N.', 'IGLESIA DE SAN JUDAS TADEO', NULL, NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1226, 'PARTICULAR', '236', NULL, 'C. CARLOS ALBERTO MONTALVO POOL', 'TAQUERIA EL FLACO', '9831228939', NULL, 'C. CARLOS ALBERTO MONTALVO POOL', 'AV. UNIVERSIDAD # 46',
        '9831228939', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1227, 'PARTICULAR', '237', NULL, 'C. GABRIEL FEDERICO MOYA TAMAY', 'TAQUERIA EL TORITO TAPATIO', '9831228939', NULL, 'GABRIEL F. MOYO TAMAY',
        'CRISTOBAL COLON #199 ESQUINA BELICE', '9831228939', 'IRVINGCOBC73@OUTLOOK.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1228, 'PARTICULAR', '238', NULL, 'C. INES UH BOTES', 'TORTILLERIA Y ABARROTES YGIMI', '9831217213', NULL, 'INES UH BOTES',
        'PASCUAL CORAL HEREDIA ESQ. NIZUC, LOTE 11, MANZANA 766, COL. TERRITORIO FEDERAL', '9831217213', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1229, 'PARTICULAR', '239', NULL, 'C. MANUEL DE JESÚS TAPIA ALCOCER', 'ANTOJITOS LA REUNIÓN', '9831142186', NULL, 'MANUEL DE JESUS TAPIA ALCOCER',
        'VALENTIN GOMEZ FARIAS #133 ESQUINA LIBRADO E RIVERA', '9831142186', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1230, 'PARTICULAR', '240', NULL, 'C. LUIS TEJEDA ALMANZA', 'JUGUERIA PIÑA COLADA', '9831097883', NULL, 'C. LUIS TEJEDA ALMANZA', 'OTHON P. BLANCO #223-B', '9831097883',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1231, 'PARTICULAR', '241', NULL, 'C. DELTA ARACELLY PECH SALAZAR', 'DON PAY ALIMENTOS', '9831833319', NULL, 'DELTA ARACELLY PECH SALAZAR',
        'AV. UNIVERSIDAD, INTERIOR UNIVERSIDAD DE QUINTANA ROO', '9831833319', 'YOE1508@OUTLOOK.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1232, 'PARTICULAR', '242', NULL, 'C. FREDDY RUBICEL CAHUICH CABRERA', 'LONCHERIA FREDDY', '8374998', NULL, 'FREDDY RUBICEL CAHUICH CABRERA',
        'CALLE CELUL, MANZANA 195, LOTE 18 S/N., COL. PAYO OBISPO', '8374998', 'FREDDY.CAHUICH@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1233, 'PARTICULAR', '243', NULL, 'C. WENDY BEATRIZ ZAPATA WITZ', NULL, '9831375004', NULL, 'WENDY BEATRIZ ZAPATA WITZ', 'COZUMEL S/N COLONIA BARRIO BRAVO', '9831375004',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1234, 'PARTICULAR', '243', NULL, 'C. WENDY BEATRIZ ZAPATA WITZ', 'TIENDA ESCOLAR PRIMARIA ALVARO OBREGON', '9831375004', NULL, 'WENDY BEATRIZ ZAPATA WITZ',
        'CALLE COZUMEL S/N., COLONIA BARRIO BRAVO', '9831375004', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1235, 'PARTICULAR', '245', NULL, 'C. LUZ MARIA GONZALEZ MANZANERO', 'PASTELERIA UNA LUZ DE DELICIAS', '9831272369', NULL, 'C. LUZ MARIA GONZALEZ MANZANERO',
        'AVENIDA ERICK PAOLO MARTINEZ N°125-A ENTRE ROJO GOMEZ Y CALLE 3', '9831272369', 'luzgonza68@icloud.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1236, 'PARTICULAR', '246', NULL, 'C. ANTONIO ANDRISANO', 'CONSORCIO LECHERO DE QUINTANA ROO S.A. DE C.V.', '9838711139/9831051004', NULL, 'C. ANTONIO ANDRISANO',
        'AVENIDA 101, MANZANA 1, LOTE 3, PARQUE INDUSTRIAL', '9838711139/9831051004', 'lamozarella@prodigy.net.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1237, 'PARTICULAR', '247', NULL, 'C. ALEX CHEN', 'SUBWAY COMIDA PARA LLEVAR', '9831302476', NULL, 'C. ALEX CHEN',
        'AV. ERICK PAOLO MARTINEZ, MANZANA 328, LOTE 1, (SORIANA LOCAL 1 Y 2)', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1238, 'PARTICULAR', '248', NULL, 'C. ALEX CHEN', 'SUBWAY (RESTAURANT COMIDA PARA LLEVAR)', '9831302476', NULL, 'C. ALEX CHEN',
        'AVENIDA INSURGENTES N°486 INTERIOR A.D.O. LOCAL 1', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1239, 'PARTICULAR', '249', NULL, 'C. ALEX CHEN', 'SUBWAY (RESTAURANT COMIDA PARA LLEVAR)', '9831302476', NULL, 'C. ALEX CHEN',
        'AVENIDA OTHON P. BLANCO N° 178, COLONIA CENTRO', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1240, 'PARTICULAR', '250', NULL, 'C. YIMI ULISES ITZA BAUTISTA', 'TORTILLERIA ITZA', '9831731963', NULL, 'C. YIMI ULISES ITZA BAUTISTA',
        'CALLE RAMONAL, MANZANA 280, LOTE 12, COL. PROTERRITORIO', '9831731963', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1241, 'PARTICULAR', '251', NULL, 'C. JOSE LUIS SANTIAGO MARTINEZ', 'TORTILLERIA ANAHI', '9831731963', NULL, 'C. JOSE LUIS SANTIAGO MARTINEZ',
        'CALLE CELUL, MANZANA 271, LOTE 15, INT. LOCAL 3, COLONIA PROTERRITORIO', '9831731963', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1242, 'PARTICULAR', '252', NULL, 'C. MARIA LEYDI CHAN CAB', 'ANTOJITOS GUADALUPANA', '9831836421', NULL, 'C. MARIA LEYDI CHAN CAB', 'ANDRES Q ROO. AVENIDA CARRANZA #243',
        '9831836421', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1243, 'PARTICULAR', '253', NULL, 'C. SOILA CATALINA PALOMAR GUTIERREZ', 'ANTOJITOS JIREH', '9831269978', NULL, 'C. SOILA CATALINA PALOMAR GUTIERREZ',
        'CALLE 10, MANZANA 16, LOTE 03, COLONIA LAZARO CARDENAS', '9831269978', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1244, 'PARTICULAR', '254', NULL, 'C. MARIA FILOMENA DZIB', 'TIENDA ESCOLAR BENITO JUAREZ', '9841803071', NULL, 'C. MARIA FILOMENA DZIB',
        'AVENIDA CALZADA VERACRUZ ENTRE LUIS CABRERA, COL. LOPEZ MATEOS', '9841803071', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1245, 'PARTICULAR', '255', NULL, 'C. ESTEFANIA ABIGAIL RAMOS SALAZAR', 'TIENDA ESCOLAR KOHUNLICH', '9831077266', NULL, 'C. ESTEFANIA ABIGAIL RAMOS SALAZAR',
        'CALLE ALFREDO V. BONFIL COLONIA SOLIDARIDAD', '9831077266', 'ABIRAMOS13@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1246, 'PARTICULAR', '256', NULL, 'C. CARLOS MANUEL YERVES LOPEZ', 'ESCUELA PRIMARIA CENTENARIO DE LA REVOLUCIÓN MEXICANA', '9831167443', NULL,
        'C. CARLOS MANUEL YERVES LOPEZ', 'CALLE ARUBA S/N. ENTRE AVENIDA CHETUMAL', '9831167443', 'CYERVESLOPEZ@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1247, 'PARTICULAR', '958', NULL, 'C. JESSICA SAAVEDRA TELLEZ', 'RESTAURANTE CAFE DEL PUERTO', '9832853898', NULL, 'C. JESSICA SAAVEDRA TELLEZ',
        'AVENIDA ALVARO OBREGON #165-A', '9832853898', 'administracion@30ytantos.com.mx / saaved', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1248, 'PARTICULAR', '959', NULL, 'C. GUSTAVO ADOLFO PECH GALERA', 'RESTAURANTE EL PATIO DEL 30', '9832856187', NULL, 'C. GUSTAVO ADOLFO PECH GALERA',
        'AVENIDA ALVARO OBREGON #165', '9832856187', 'administracion@30ytantos.com.mx / tavo_6', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1249, 'PARTICULAR', '257', NULL, 'C. JUANA TECOMAGUA SANTOS', 'PURIFICADORA DE AGUA CHAGUITA', '9831255757, 9831307931', NULL, 'C. JUANA SANTOS TECOMAGUA',
        'LA UNION, Q.ROO', '9831255757, 9831307931', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1250, 'PARTICULAR', '258', NULL, 'C. PEDRO DE LA FUENTE CANUL', 'TAQUERIA Y CHICHARRONERIA LA PALOMA', '9831254083', NULL, 'C. PEDRO DE LA FUENTE CANUL',
        'AVENIDA LAZARO CARDENAS #352 ESQUINA CECILIO CHI', '9831254083', 'de_lafuente20@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1251, 'PARTICULAR', '259', NULL, 'C. ARMANDO NARCIZO RUEDA PEREZ', 'RESTAURANTE BAR LAS LUNAS', '9831071314', NULL, 'C. ARMANDO NARCIZO RUEDA PEREZ',
        'AVENIDA BOULEVARD BAHIA #96', '9831071314', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1252, 'PARTICULAR', '260', NULL, 'C. SHEILA YOVANA DE LA CRUZ CALDERON', 'CHICHARRONERIA CALIN', '9831669922', NULL, 'C. SHEILA YOHANA DE LA CRUZ CALDERON',
        'AV. JAVIER ROJO GOMEZ #178 ENTRE CELUL Y BULUKAX, COL. PAYO OBISPO', '9831669922', 'agsc6113@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1253, 'PARTICULAR', '261', NULL, 'C. ALEXIS ALBERTO DE LA CRUZ MORALES', 'SUPER CARNICERIA PAYO OBISPO', '9831669922', NULL, 'C. ALEXIS ALBERTO DE LA CRUZ MORALES',
        'AV. ROJO GOMEZ # 183, LOTE 14, MANZANA 34, COL. PAYO OBISPO ENTRE CELUL Y ALONDR', '9831669922', 'agsc6113@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1254, 'PARTICULAR', '262', NULL, 'C. MARCELINA RAMIREZ RAMIREZ', 'QUESADILLAS CHELY', '9831003566', NULL, 'C. MARCELINA RAMIREZ RAMIREZ',
        'AV. DE LOS HEROES #390, INFONAVIT (UNIDAD HABITACIONAL) FRANCISCO J. MUJICA', '9831003566', 'dacoh94@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1255, 'PARTICULAR', '263', NULL, 'C. NANCY YADIRA AVILA CAAMAL', 'ESCUELA PATRIA', '9831689127', NULL, 'C. NANCY YADIRA AVILA CAAMAL',
        'RAFAEL E. MELGAR ESQUINA LAZARO CARDENAS S/N.', '9831689127', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1256, 'PARTICULAR', '264', NULL, 'C. KAREN ANAHI ZAVALETA PERAZA', 'LA EMPANADA DE ORO VI', '9838098047', NULL, 'C. KAREN ANAHI ZAVALETA PERAZA',
        'AV. CONSTITUYENTES DEL 74 #878 ESQUINA SANTA LUCIA, COL. CARIBE', '9838098047', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1257, 'PARTICULAR', '265', NULL, 'C. ANASTASIA MARIA DE LOS ANGELES ESPINOZA CRUZ', 'COCINA EL AZTECA', '9831441213', NULL,
        'C. ANASTASIA MARIA DE LOS ANGELES ESPINOZA CRUZ', 'AV. ERICK PAOLO MARTINEZ, MANZANA 119, LOTE 24 ENTRE POLYUC Y PETCACAB', '9831441213', 'azteca_factura@hotmail.com',
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1258, 'PARTICULAR', '268', NULL, 'C. MARCOS HERNANDEZ GONZALEZ', 'TAQUERIA LOS REYES DEL SABOR', '9838384593', NULL, 'C. MARCOS HERNANDEZ GONZALEZ',
        'AVENIDA LUIS MANUEL SEVILLA SANCHEZ ENTRE VENEZUELA Y URUGUAY', '9838384593', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1259, 'INSTITUCIONAL', '269', NULL, 'CENTRO DE SALUD RURAL', NULL, '9837521827', NULL, NULL, 'XUL-HA, Q. ROO', '9837521827', 'danieldoc_53@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1260, 'PARTICULAR', '270', NULL, 'C. URBELINA ABIGAIL ZAVALETA PERAZA', 'EMPANADA DE ORO', '9831044564', NULL, 'C. URBELINA ABIGAIL ZAVALETA PERAZA',
        'SAN SALVADOR #438 CALLE NAPOLES Y SICILIA', '9831044564', 'abby@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1261, 'PARTICULAR', '271', NULL, 'C. ELSI NOEMI N. CACERES', 'RESTAURANT CHEZ FAROUK', '8328575', NULL, 'C. ELSI NOEMI N. CACERES', 'BELICE #186', '8328575',
        'HotelrealAc/co@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1262, 'PARTICULAR', '272', NULL, 'C. ELDA MERCEDES ONGAY GARCIA', 'LONCHERIA MIRADA DE MUJER', '9831193779', NULL, 'C. ELDA MERCEDES ONGAY GARCIA',
        'MERCADO LAZARO CARDENAS DEL RIO, LOCALES 388-389-390', '9831193779', 'abelardo976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1263, 'PARTICULAR', '273', NULL, 'C. SIMONA HERNANDEZ', 'CARNITAS EL NORTEÑO', '98328082', NULL, 'C. SIMONA HERNANDEZ / CAROLINA GONZALEZ HERNANDEZ',
        'FRANCISCO I. MADERO # 382', '98328082', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1264, 'PARTICULAR', '274', NULL, 'C. JOSE PADILLA MENDOZA', 'SOPHIA VALERIA PADILLA CASTILLEJOS (COMIDA RAPIDA)', '8322399', NULL, 'C. JOSE PADILLA MENDOZA',
        'AV. INSURGENTES KM. 5.025 INT. G.14 PLAZA LAS AMÉRICAS, COL. EMANCIPACIÓN', '8322399', 'sergiospizza.facturacion@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1265, 'INSTITUCIONAL', '275', NULL, 'D. EN C. ARMIDA ZUÑIGA ESTRADA', 'COMISIÓN DE CONTROL ANALÍTICO Y AMPLIACIÓN DE COBERTURA', '5080-5200 etx. 2001', NULL,
        'COMISIÓN DE CONTROL ANALÍTICO Y AMPLIACIÓN DE COBERTURA', 'CALZADA DE TLALPAN 4492, COL. TORIELLO GUERRA, DELEGACIÓN TLALPAN', '5080-5200 etx. 2001',
        'www.cofepris.gob.mx', NULL, NULL, 'México', 'MÉXICO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1266, 'PARTICULAR', '276', NULL, 'C. BREEZE JAQUELINE LARA UCO', 'RESTAURANTE LOS COMALES', '9831292374', NULL, 'C. BREEZE JAQUELINE LARA UCO', 'AVENIDA ZARAGOZA #283',
        '9831292374', NULL, NULL, NULL, 'Quintana Roo', 'Chetumal', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1267, 'PARTICULAR', '277', NULL, 'C. JORGE MARIO SALAZAR NAHUAT', 'PANADERIA LA MEJOR', '2855627, 9831207523', NULL, 'C. JORGE MARIO SALAZAR NAHUAT',
        'CALLE ENRIQUE BAROCIO # 797, MANZANA 8, LOTE 10', '2855627, 9831207523', 'despachocontable@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1268, 'PARTICULAR', '278', NULL, 'C. CLAUDIA PAULINA CASTILLO COURTENAY', 'RESTAURANT BAR EL PEZ DE ORO', '9831024330', NULL, 'C. CLAUDIA PAULINA CASTILLO COURTENAY',
        'CALLE OAXACA #14, CALDERITAS Q. ROO', '9831024330', 'waldemar_63@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1269, 'PARTICULAR', '279', NULL, 'GRUPO CASAS NOVA ERA S.A. DE C.V.', 'RESTAURANT DE AUTOSERVICIO FRESH SEASON', '9831475070', NULL, 'GRUPO CASAS NOVA ERA S.A. DE C.V.',
        'BELICE ESQUINA LUIS CABRERA # 156, FRACC. BUGAMBILIAS', '9831475070', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1270, 'PARTICULAR', '280', NULL, 'C. CARLA YESENIA SANTOS MIRANDA', 'DISPENSADORA AGUA SANTA CLARA', '9838399572', NULL, 'C. CARLA YESENIA SANTOS MIRANDA',
        'CARRETERA UCUM - LA UNIÓN, Q.ROO, MANZANA 68, LOTE 1-A', '9838399572', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1271, 'PARTICULAR', '281', NULL, 'C. MARIA DEL CARMEN GONZALEZ LANDERO', 'RESTAURANTE EL SABOR DE MI TIERRA', '9831837369', NULL, 'C. MARIA DEL CARMEN GONZALEZ LANDERO',
        'CALLE OTHON P. BLANCO ESQUINA 5 DE MAYO#179', '9831837369', 'mgonzalezlandero@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1272, 'PARTICULAR', '282', NULL, 'C. YUMING SHEREZADA GOMEZ ORTEGA', 'RESTAURANTE LA PRIMERA', '9831075572', NULL, NULL, 'AVENIDA 4 DE MARZO S/N LOCAL 40 Y 41',
        '9831075572', 'CARLOSCSOTO@CCYSDESPACHO.COM.MX', NULL, NULL, 'Quintana Roo', 'Chetumal', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1273, 'PARTICULAR', '283', NULL, 'C. YUMING SHEREZADA GOMEZ ORTEGA', 'RESTAURANT LA PRIMERA', '9831163870', NULL, NULL, 'AVENIDA 4 DE MARZO S/N. LOCAL 40 Y 41',
        '9831163870', 'CARLOSCSOTO@CCYSDESPACHO.COM.MX', NULL, NULL, 'Quintana Roo', 'Chetumal', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1274, 'PARTICULAR', '284', NULL, 'C. ROBERTO ISMAEL EK NAVARRETE', 'PURIFICADORA MANANTIAL DE VIDA', '9831655349', NULL, NULL, 'CALLE PUERTO RICO # 427', '9831655349',
        'RIEN.NAVA@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'Chetumal', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1275, 'PARTICULAR', '285', NULL, 'C. EDITH GONZALEZ BARRIOS', 'ESTANCIA INFANTIL RAFITAS', '9831649263', NULL, NULL,
        'FRANCISCO I. MADERO 306, COLONIA DAVID G. GUTIERREZ RUIZ', '9831649263', 'GOBE7304@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'Chetumal', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1276, 'PARTICULAR', '301', NULL, 'C. MARIO ARTURO BURGOS HERRERA', 'NONOWANA PIZZAS', '9831807437', NULL, 'C. MARIO ARTURO BURGOS HERRERA',
        'AVENIDA CHETUMAL CASI GRACIANO SANCHEZ', '9831807437', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1277, 'PARTICULAR', '300', NULL, 'C. JUHAINA SAPNA BUXANI MORAN', 'WINGMAN CHETUMAL', '9831640806', NULL, 'C. JUHAINA SAPNA BUXANI MORAN', 'AVENIDA BOULEVARD BAHIA N°54',
        '9831640806', 'j.buxani22@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1278, 'INSTITUCIONAL', '302', NULL, 'COMANDANTE ACC. DEL BATALLÓN DE IM. NÚM. 25', 'CAP. NAV. IM. DEM. JOSE DE JESUS BONILLA DOMINGUEZ', '3215287448, 5574773645', NULL,
        'CAP. NAV. IM. DEM. JOSE DE JESUS BONILLA DOMINGUEZ', 'AV. INSURGENTES S/N. ESQ. CHETUMAL, COL. LEONA VICARIO', '3215287448, 5574773645', 'batallon_num25@hotmail.com',
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1279, 'PARTICULAR', '303', NULL, 'C. GIBRAN DE JESUS CUETO AGUILAR', 'AGUA PURIFICADA BACALAGUA', '9838391289', NULL, 'C. GIBRAN DE JESUS CUETO AGUILAR',
        'KM. 2, CARRETERA FEDERAL BACALAR-CHETUMAL', '9838391289', 'bacalagua@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1280, 'PARTICULAR', '286', NULL, 'C. JAIME TILAN PALOMO', 'TORTILLERIA JIMMY', '9831019859', NULL, 'C.JAIME TILAN PALOMO',
        'CALLE POLYUC, MANZANA 379, LOTE 1 ENTRE MAXUXAC Y MANUEL CRECENCIO REJON', '9831019859', 'jimmypalomo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1281, 'PARTICULAR', '304', NULL, 'C. VALENTINA MENDOZA BALAM', 'COCINA ECONOMICA GAEL Y MARIANA', '1449716', NULL, 'C. VALENTINA MENDOZA BALAM',
        'FELIPE CARRILLO PUERTO #534 ENTRE ALTAMIRANO Y ZARCO', '1449716', 'dic.2014@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1282, 'PARTICULAR', '287', NULL, 'C. JOSE ALFREDO NAAL CETINA', 'PIZZERIA CHEPOS', '9831106218', NULL, NULL, 'AVENIDA ISLA CANCUN # 442-A', '9831106218',
        'PIZZACHEPO@HOTMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1283, 'PARTICULAR', '288', NULL, 'C. FLOR JENNIFER CAAMAL TZEC', 'RESTAURANTE LA TERRAZA DE TITO', '9838379259', NULL, NULL,
        'AV. 4 DE MARZO ESQUINA ERICK PAOLO, SUC. PLAZA CAPITAL CENTER', '9838379259', 'TERRAZAQ.CAPITAL@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1284, 'PARTICULAR', '289', NULL, 'C. MARIA LUISA VILLANUEVA CHAVEZ', 'TAQUERIA SAHUAYO', NULL, NULL, NULL, 'AVENIDA JUAREZ # 66', NULL, NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1285, 'PARTICULAR', '305', NULL, 'C. DANIEL REYNALDO MEZQUITA BALAM', 'FUMIGADORA GRUPO ECOSERV', '9838325910', NULL, 'GRUPO ECOSERV',
        'CALLE LUIS CABRERA #151, ENTRE BELICE Y HEROES, COL. FRANCISCO J. MUJICA', '9838325910', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1286, 'PARTICULAR', '295', NULL, 'C. MIRIAN MARIA XIU CANTE', 'FUMITTEK CONTROL DE PLAGAS', '9831114670', NULL, NULL, 'RETORNO 56 MANZANA 79 LOTE 3 COLONIA PAYO OBISPO II',
        '9831114670', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1287, 'PARTICULAR', '306', NULL, 'C. JOSE ABEL SEGURA HERRERA', 'RESTAURANT MAYA BAR', '9838345668 9831105057', NULL, 'C. JOSE ABEL SEGURA HERRERA',
        'AVENIDA MAHAHUAL, MANZANA 4. LOTE 4, COLONIA CENTRO', '9838345668 9831105057', 'pzac2009@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1288, 'PARTICULAR', '306', NULL, 'C. JOSE ABEL SEGURA HERRERA', 'RESTAURANT MAYA BAR', '9838345668 9831105057', NULL, 'C.JOSE ABEL SEGURA HERRERA',
        'AV. MAHAHUAL, MANZANA 4, LOTE 4, COL. CENTRO, MAHAHUAL, Q.ROO', '9838345668 9831105057', 'pzac2009@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1289, 'PARTICULAR', '307', NULL, 'COSAS NATURALES S.A. DE C.V.', 'RESTAURANTE NACIONAL BEACH CLUB', '9838345719, 9831029595', NULL, 'COSAS NATURALES S.A. DE C.V.',
        'AV. MAHAHUAL S/N., LOTE 4, MANZANA 14, COL. CENTRO, MAHAHUAL, Q.ROO', '9838345719, 9831029595', 'evanmckenzie@gmail.com/nacionalbeachclub', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1290, 'PARTICULAR', '309', NULL, 'C. VANESSA MACIAS LUQUE', 'CAFETERIA PLANTEL CECYTE', '9831594380', NULL, 'C. VANESSA MACIAS LUQUE',
        'TEC DE TUXTLA GUTIERREZ ENTRE PALERMO Y 4 DE MARZO', '9831594380', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1291, 'PARTICULAR', '309', NULL, 'C. ORESTES CONTRERAS MENDEZ', 'APPLEBEES RESTAURANTE', '9831682390', NULL, 'C. ORESTES CONTERRAS MENDEZ', 'TZIAUCHE ESQUINA INSURGENTES',
        '9831682390', 'applebees.chetumal@goges.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1292, 'PARTICULAR', '310', NULL, 'C. ISIS ANAIS SAMOS MARTINEZ', 'POLLOS ASADOS MANRIQUE', '9831200253', NULL, 'C. ISIS ANAIS SAMOS MARTINEZ',
        'JUAREZ ENTRE CRISTOBAL COLON Y PRIMO DE VERDAD', '9831200253', 'emymanriquesamos@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1293, 'PARTICULAR', '311', NULL, 'C. ROMAN ANTONIO CHI PAT', 'RESTAURANT KIMBARA', '9837528005', NULL, 'C. ROMAN ANTONIO CHI PAT',
        'AVENIDA MAHAHUAL S/N., MANZANA 4, LOTE 3, MAHAHUAL, Q.ROO', '9837528005', 'roman_2212@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1294, 'PARTICULAR', '312', NULL, 'C. CIRILA TEMBRE RODRIGUEZ', 'ESTANCIA INFANTIL DISNEY', '9837003720', NULL, 'C. CIRILA TEMBRE RODRIGUEZ',
        'CALLE MANTOS #87, COLONIA JARDINES', '9837003720', 'ciri_1508@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1295, 'PARTICULAR', '313', NULL, 'C. LIZBETH CORTEZ MORENO', 'ESTANCIA INFANTIL MUNDO DE JUGUETE', '9831305598', NULL, 'C. LIZBETH CORTEZ MORENO',
        'PRIVADA VENUSTIANO CARRANZA #379-A, COLONIA ITALIA', '9831305598', 'theblakiislove@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1296, 'PARTICULAR', '297', NULL, 'C. ISOLINA ZAPATA GARCIA', 'COCINERAMI COCINA', '9832126003', NULL, NULL, 'SAN SALVADOR # 460, FRACC. FLAMBOYANES', '9832126003', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1297, 'PARTICULAR', '314', NULL, 'C. JUANA BAUTISTA SANCHEZ CHAN', 'LONCHERIA ALEJANDRA', '9831557362', NULL, 'JUANA BAUTISTA SANCHEZ CHAN',
        'AQUILES SERDAN N.18, COLONIA 5 DE ABRIL', '9831557362', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1298, 'PARTICULAR', '319', NULL, 'C. JAIME CUETO STRIMPOPULOS', 'RESTAURANT BLUE REET', '9831544234', NULL, 'C. JAIME CUETO STRIMPOPULOS',
        'AV. MAHAHUAL S/N., COL. CENTRO, MAHAHUAL, Q.ROO', '9831544234', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1299, 'PARTICULAR', '315', NULL, 'C. LIDIA ESTHER EK BALAM', 'COCINA DE LA CLINICA CARRANZA', '8351440', NULL, NULL, 'VENUSTIANO CARRANZA #366, COL. ITALIA', '8351440',
        'Ivttdlrocio@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1300, 'PARTICULAR', '298', NULL, 'C. GERARDO DE JESÚS MARTINEZ GUERRERO', 'TAPATIOS SNACK', '8313401903', NULL, 'C.GERARDO DE JESUS MARTINEZ GUERRERO',
        'CALLE SACXAN ENTRE YAXCOPOIL Y SUBTENIENTE LOPEZ, MANZANA 47, LOTE 1', '8313401903', 'gjmartinezgdeatlas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1301, 'PARTICULAR', '365', NULL, 'C. JOSE LUIS RAMIREZ SOLIS', 'RESTAURANT LA CEIBA', '9831302476', NULL, 'C. JOSE LUIS RAMIREZ SOLIS',
        'AVENIDA BENITO JUAREZ #362, COL. DAVID GUSTAVO', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1302, 'PARTICULAR', '316', NULL, 'C. ROCIO DEL CARMEN DOMINGUEZ MISS', 'ESTANCIA INFANTIL CARITA DE ANGEL', '9831569895', NULL, 'C. ROCIO DEL CARMEN DOMINGUEZ MISS',
        'CALLE POLYUC, MANZANA 134, LOTE 4 ENTRE BUGAMBILIAS Y SUBTENIENTE LOPEZ', '9831569895', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1303, 'PARTICULAR', '317', NULL, 'C. ANGELICA SANCHEZ TUZ', 'ESTANCIA INFANTIL LOS PEQUES DE BACALAR', '9831343286', NULL, 'C. ANGELICA SANCHEZ TUZ',
        'AVENIDA 13 ENTRE 28 Y 30, COLONIA GONZALO GUERRERO', '9831343286', 'angelica_270280@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1304, 'PARTICULAR', '317', NULL, 'C. JENNIFER EUNICE MAÁS MAY', 'ESTANCIA INFANTIL PASITOS', '9831241118', NULL, 'C. JENNIFER EUNICE MACIAS MAY',
        'CALLE KIRICHNA #146 ENTRE AV. JAVIERO ROJO GOMEZ Y CALLE FAISAN', '9831241118', 'jennimaas2210@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1305, 'PARTICULAR', '318', NULL, 'C. ROCIO PINZON CRUZ', 'PANADERIA BELEN', '9831150861', NULL, 'C. ROCIO PINZON CRUZ', 'CALLE HERIBERTO FRIAS #81 CON BELIZARIO DOMINGUEZ',
        '9831150861', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1306, 'PARTICULAR', '319', NULL, 'C. PERLA ELENA GABILONDO POUS', 'PITAYA BEACH', '9838366478', NULL, NULL, 'CALLE MAHAHUAL, LOTE 7, MANZANA 9, MALECON, MAHAHUAL, Q.ROO',
        '9838366478', 'perla@pitayabeach.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1307, 'PARTICULAR', '320', NULL, 'C. DAVID SILENCIO OLAN', 'EMPANADAS PIOLIN', '9831832869', NULL, 'C.DAVID SILENCIO OLAN',
        'MACHUXAC ESQUINA ANTONIO CORIA, LOTE 12, MANZANA 451', '9831832869', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1308, 'PARTICULAR', '393', NULL, 'C. CLAUDIA DELGADILLO MARTINEZ', 'ESTANCIA INFANTIL LIBERMOR', '9831205924', NULL, 'C.CLAUDIA DELGADILLO MARTINEZ',
        'CALLE TELA #320, FRACC. RESIDENCIAL MEDITERRANEO', '9831205924', 'claudia_120676@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1309, 'PARTICULAR', '397', NULL, 'C. JUANA MATILDE TORRES FLORES', 'ESTANCIA INFANTIL CARITAS', '9837005522', NULL, 'C. JUANA MATILDE TORRES FLORES',
        'CALLE CELUL #109 ESQUINA FAISAN, COL. PAYO OBISPO I', '9837005522', 'caritas-chetumal@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1310, 'PARTICULAR', '320', NULL, 'C. OMAR GURGUA LIZARRRAGA', 'EMPANADAS LAS FAVORITAS', '9831179130', NULL, 'C. GURGUA LIZARRRAGA OMAR', 'CALLE SALVADOR ALVARADO #252',
        '9831179130', 'kakshi_013@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1311, 'PARTICULAR', '320', NULL, 'C. MARTIN MELENDEZ PUERTO', 'LONCHERIA LA HORMIGA', '9831090955', NULL, 'C. MARTIN MELENDEZ PUERTO',
        'MERCADO MANUEL ALTAMIRANO, LOCAL 136 Y 122, COL. CENTRO', '9831090955', 'marcemix_1476@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1312, 'PARTICULAR', '321', NULL, 'C. BERNARDO ANGUIANO LAUREN', 'RESTAURANT LOS DE CHETU', '9831302476', NULL, 'C. BERNARDO ANGUIANO LAUREL',
        'AV. INSURGENTES, LOCAL G-15, INT PLAZA LAS AMÉRICAS, KM 5.025, COL. EMANCIPACIÓN', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1313, 'PARTICULAR', '395', NULL, 'C. REINA GUADALUPE OSORIO FLORES', 'ANTOJITOS LUPITA', '9831345040', NULL, NULL,
        'BELIZARIO DOMINGUEZ #80 ENTRE HERIBERTO FRIAS Y LUIS MOYA.', '9831345040', 'REIGPEO@GMAIL.COM', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1314, 'PARTICULAR', '322', NULL, 'C. CRISTIAN ALEJANDRO BERZUNZA CÁMARA', 'PANADERIA CAPY', '9831337879', NULL, NULL,
        'AV. MAXUXAC, MANZANA 249, LOTE 5, ENTRE 1ro. DE MAYO Y 27 DE SEPTIEMBRE', '9831337879', 'camariitashyrma@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1315, 'PARTICULAR', '308', NULL, 'C. ALONDRA GUADALUPE LOPEZ AKE', 'EL JARDIN FOOD PARK', '9831041410', NULL, 'C. ALONDRA LOPEZ AKE',
        'TECNOLÓGICO DE VERACRUZ ENTRE NICOLAS BRAVO # 376', '9831041410', 'superking-obel@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1316, 'PARTICULAR', '391', NULL, 'C. KELLY MARIA BOLIO MOGUEL', 'JALISCO COCINA MEXICANA', '9831557544', NULL, 'C. KELLY MARIA BOLIO MOGUEL',
        'AV. OTHON P. BLANCO ESQUINA ISLA CONTOY # 24', '9831557544', 'kbalio@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1317, 'PARTICULAR', '400', NULL, 'C. ISRAEL HERNANDEZ HERNANDEZ', 'PANADERIA VERACRUZ', '9831055141', NULL, 'C. ISRAEL HERNANDEZ HERNANDEZ',
        'AVENIDA MANUEL EVIA CAMARA #500 FRACCIONAMIENTO LAS AMERICAS', '9831055141', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1318, 'PARTICULAR', '403', NULL, 'C. ARIADNE GERALDINE FERNANDEZ CHAVEZ', 'ESTANCIA INFANTIL LA CASITA DE FRIDA KAHLO', '9831762190', NULL,
        'C. ARIADNE GERALDINE FERNANDEZ CHAVEZ', 'CALLE JAMAICA #367 ENTRE PETCACAB Y POLYUC, FRACC. CARIBE', '9831762190', NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1319, 'PARTICULAR', '401', NULL, 'C. MILEDY RUBI ITURBIDE BRICEÑO', 'TORTILLERIA DANIEL II', '9831090955', NULL, 'C. MILEDY RUBI ITURBIDE BRICEÑO',
        'AV. LUIS MANUEL SEVILLA SANCHEZ #1121 INTERIOR D, COL. LAS AMERICAS', '9831090955', 'marcemix_1976@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1320, 'PARTICULAR', '960', NULL, 'DIF Q.ROO', 'CENTRO INTEGRAL DE PRIMERA INFANCIA MOOTS YA''AXCHE''', '1183062', NULL, 'DIF Q.ROO',
        'CALLE MACHICHE S/N. ENTRE FLAMBOYANES Y CALLE URSULO GALVAN', '1183062', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1321, 'PARTICULAR', '400', NULL, 'C. LUIS BEDRIÑANA BARBOSA', 'VILEX FUMIGACIONES S. DE R.L. DE C.V.', '9838360714', NULL, 'C. LUIS BEDRIÑANA BARBOSA',
        'CALLE SICILIA #224-A', '9838360714', 'fumigasacancun@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1322, 'PARTICULAR', '961', NULL, 'C. JOSE LUIS HUMBERTO COLLI NOH', 'TAQUERIA DOÑA NELLY', '9831019916', NULL, NULL, 'AV. BELICE #292 ENTRE JUSTO SIERRA Y CAMELIAS',
        '9831019916', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1323, 'PARTICULAR', '962', NULL, 'C.MARIA AURORA AGUILAR QUIRIO', 'BABU TE BAR', '9831114743', NULL, NULL, 'AV. BENITO JUAREZ ENTRE PRIMO DE VERDAD Y CRISTOBAL COLON#210',
        '9831114743', 'aaquirio@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1324, 'PARTICULAR', '324', NULL, 'C. ROMAN PECH BORGES', 'POLLO ASADOS LALO''S CHICKEN', '9831206698', NULL, 'C. ROMAN PECH BORGES',
        'AV. 4 DE MARZO #267 ENTRE SAN SALVADOR Y CALLE 7, COL. 8 DE OCTUBRE', '9831206698', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1325, 'INSTITUCIONAL', '327', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS SA DE CV', 'EL MICHOACANO', '9831180277', NULL, NULL, 'CALLE LUCIO BLANCO #87',
        '9831180277', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1326, 'INSTITUCIONAL', '326', NULL, 'GV DE LA PENINSULA SC DE RL DE CV', 'TORTILLERIA EL MICHOACACNO', '9831180277', NULL, NULL,
        'AVENIDA LAZARO CARDENAS #278 ESQUINA MORELOS', '9831180277', 'factura gvchetumal@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1327, 'INSTITUCIONAL', '328', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE SA DE CV', 'TORTILLERIA EL MICHOACANO', '9831180777', NULL, NULL, NULL, '9831180777',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1328, 'PARTICULAR', '3325', NULL, 'G.V. DE LA PENINSULA S.C. DE R.L. DE C.V.', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, 'GV DE LA PENINSULA SC DE RL DE CV',
        'AV. LAZARO CARDENAS #278 ESQUINA MORELOS', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1329, 'PARTICULAR', '329', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, NULL,
        'CALLE COAHUILA # 44 ENTRE QUERETARO Y GUANAJUATO, COL. CENTRO, CALDERITAS, Q.ROO', '9831180277', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1330, 'PARTICULAR', '337', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS SA DE CV', 'TORTILLERIA EL MICHOACANO', '9831108277', NULL, NULL,
        'LOTE 1, MANZANA 35 ESQ. JESÚS CARMINCHES Y JOSE DEL CARMEN, COL. FORJADORES', '9831108277', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1331, 'PARTICULAR', '331', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, NULL,
        'CIRCUITO 3 SUR #11, LOTE 1, MANZANA 63, AV. PACTO OBRERO', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1332, 'PARTICULAR', '333', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '983110277', NULL,
        'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS DE CV', 'AV. LEONA VICARIO S/N., LOTE 2, AND. GPE. VICTORIA, LOCALIDAD ROJO GOMEZ, Q.ROO', '983110277', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1333, 'PARTICULAR', '335', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '983110277', NULL,
        'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS DE CV', 'AV. CNC #135 B ENTRE AV SALVADOR ALVARADO Y AV. HEROES, COL. ADOLFO LOPEZ', '983110277',
        'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1334, 'PARTICULAR', '337', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS DE CV DCS DE CV', 'TORTILLERIA EL MICHOACANO', '983110277', NULL,
        'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS DE CV', 'AVENIDA LEONA VICARIO S/N. LOTE 2 ANDADOR GUADALUPE VICTORIA', '983110277', NULL, NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1335, 'PARTICULAR', '338', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '983119277', NULL, NULL,
        'CALLE LUCIO BLANCO # 87 CON HERIBERTO FRÍAS', '983119277', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1336, 'PARTICULAR', '339', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, NULL,
        'MANZANA-47, LOTE 6 EN LA ENTRADA PRINCIPAL, COL. MILAGROS', '9831180277', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1337, 'PARTICULAR', '399', NULL, 'DISTRIBUIDORA Y COMERCIALIZADORA DEL SURESTE DCS S.A. DE C.V.', 'TORTILLERIA EL MICHOACANO', '9831180277', NULL, NULL,
        'CALLE SICILIA S/N. ENTRE SAN SALVADOR Y CAMELIAS, COL. ASERRADERO', '9831180277', 'facturagvchetumal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1338, 'PARTICULAR', '301', NULL, 'C.MARIA DE LOS ANGELES FLORES JUAREZ', 'COCINA ECONOMICA EL SAZON DE MAMA', '9831546760', NULL, 'C.MARIA DE LOS ANGELES FLORES JUAREZ',
        'AVENIDA HEROES #365', '9831546760', 'soniagoretty@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1339, 'PARTICULAR', '306', NULL, 'C. GABRIELLE CIMATO', 'RESTAURANT BAR PITAYA BEACH', '9838366478', NULL, 'C. GABRIELLE CIMATO',
        'CALLE MAHAHUAL, LOTE 7, MANZANA 9, MAHAHUAL, Q. ROO', '9838366478', 'perla@pitayabeach.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1340, 'PARTICULAR', '341', NULL, 'C. GRECIA MERCEDES AVILA TREJO', 'TORTILLERIA LA MEXICANA', '9831010823', NULL, 'C. GRECIA MERCEDES AVILA TREJO',
        'AVENIDA NICOLAS BRAVO ENTRE CELUL Y RAMONAL', '9831010823', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1341, 'PARTICULAR', '963', NULL, 'C. ESTHER YADIRA QUIJANO HAU', 'LA NEGRA FOOD PARK', '9838381617', NULL, NULL, 'AVENIDA PLUTARCO ELIAS CALLES #213', '9838381617', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1342, 'PARTICULAR', '964', NULL, 'E2 OPERACIONES SAS DE C.V.', 'SUBWAY RESTAURANT', '9831302476', NULL, NULL, 'AVENIDA INSURGENTES #486, LOCAL 1, INTERIOR A.D.O.',
        '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1343, 'PARTICULAR', '965', NULL, 'C. ALEXIS ALBERTO DE LA CRUZ MORALES', 'TORTILLERIA ALEXIS', '9831817786', NULL, 'C. ALEXIS ALBERTO DE LA CRUZ MORALES',
        'JAVIER ROJO GOMEZ, MANZANA 34, LOTE 18', '9831817786', 'agrc6113@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1344, 'PARTICULAR', '966', NULL, 'C. JOSE JOAQUIN GONZALEZ BLANCO', 'POLLERIA CHARY', '9831090955', NULL, NULL,
        'AV. HEROES S/N. LOCAL # 191, COLONIA CENTRO, MERCADO MANUEL ALTAMIRANO', '9831090955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1345, 'PARTICULAR', '967', NULL, 'C. CARLOS ALBERTO OJEDA DE LA FUENTE Y LEÓN', 'ESTANCIA INFANTIL EDUCARE PLAYGROUND', '9838360023', NULL, NULL,
        'HERMINIO AHUMADA #38, COLONIA GONZALO GUERRERO', '9838360023', 'ludotecaplayground@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1346, 'PARTICULAR', '342', NULL, 'E2 OPERACIONES SAS DE C.V.', 'SUBWAY', '9831272436', NULL, NULL, 'AV. ERICK PAOLO MARTINEZ# 120, MANZANA 328, LOTE 1, INTERIOR SORIANA',
        '9831272436', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1347, 'PARTICULAR', '343', NULL, 'E2 OPERACIONES SAS DE C.V.', 'RESTAURANT LE PETIT PARIS', '9831212436', NULL, 'E2 OPERACIONES SAS DE CV',
        'AVENIDA INSURGENTES KM 5.025, INTERIOR PLAZA LAS AMERICAS LOCAL G-4', '9831212436', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1348, 'PARTICULAR', '344', NULL, 'PABLO PEDRO DORANTES NUÑEZ', 'RESTAURANT BAR EL MILAGRO', NULL, NULL, NULL, 'AVENIDA JUAREZ # 331 COLONIA DAVID GUSTAVO', NULL, NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1349, 'PARTICULAR', '345', NULL, 'E2 OPERACIONES SAS DE C.V.', 'RESTAURANT PANDA EXPRESS', '9831272436', NULL, NULL,
        'AVENIDA INSURGENTES KM. 5.025 INTERIOR DE PLAZA LAS AMÉRICAS LOCAL G-3', '9831272436', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1350, 'PARTICULAR', '346', NULL, 'C. ALEJANDRO FERNANDO APELLAMIZ CAMPO', NULL, '9831133768', NULL, NULL, 'CALLE 22 #63 COLONIA CENTRO', '9831133768',
        'alexapellamiz@outlook.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1351, 'PARTICULAR', '347', NULL, 'C. JOSE ENRIQUE ZAVALA INTERIAN', 'RESTUARANT BELIZIAN HOUSE', '9831715280, 9831715860', NULL, NULL,
        'ERICK PAOLO MARTINEZ, MANZANA 128, LOTE 4, LOCAL 1', '9831715280, 9831715860', 'jezi918@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1352, 'PARTICULAR', '348', NULL, 'C. PEDRO HERNANDEZ HERNANDEZ', 'TORTILLERIA ANDREA', '9831568600', NULL, NULL,
        'CALLE ALTOS DE SEVILLA ENTRE NICOLAS BRAVO Y POLYUC, LOTE 15, MANZANA 225', '9831568600', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1353, 'PARTICULAR', '349', NULL, 'C. MEDARDO GIL SANTIAGO', 'LOS CERROS DE IZAMAL', '9831237356', NULL, NULL, 'AV. OAXACA LOCAL #11 ENTRE 24 DE FEBRERO Y CAMPECHE',
        '9831237356', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1354, 'PARTICULAR', '446', NULL, 'C. REYES JESUS NOVELO MORALES', 'DISPENSADOR DE AGUA PURIFICADA AGUA LIA', '9831559009', NULL, 'C. REYES JESUS NOVELO MORALES',
        'AV. MANUEL SEVILLA SANCHEZ #1193, FRACC. LAS AMERICAS II', '9831559009', 'reyes_novelo@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1355, 'PARTICULAR', '350', NULL, 'TOSTIMAYA Y DERIVADOS DEL CARIBE S.A. DE C.V.', 'TORTILLERIA TOSTIMAYA', '9831079347', NULL, NULL,
        'AV. NICOLAS BRAVO, LOTE 14, LOCAL 2 ENTRE AV. MAXUXAC Y MANUEL CRECENCIO REJON', '9831079347', 'tostimaya@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1356, 'PARTICULAR', '351', NULL, 'C. ALEJANDRA ARGUELLO RADILLA', 'AGUA INMACULADA DEL SURESTE', '9837335048', NULL, NULL,
        'CARRETERA FEDERAL, MANZANA 11, POBLADO HUAY PIX, Q.ROO', '9837335048', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1357, 'PARTICULAR', '352', NULL, 'C. JUAN IGNACIO HOIL DOMINGUEZ', 'MARISQUERIA EL CAPI', '9838361448', NULL, NULL, 'HERIBERTO JARA #582', '9838361448', 'Jhoil75@live.com',
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1358, 'PARTICULAR', '953', NULL, 'C. UZZIEL RIVEROLL COTO', 'TORTILLERIA ISAI', '9831373482', NULL, 'C. UZZIEL RIVEROLL COTO',
        'CALLE GRANADA ESQUINA AV. CHETUMAL, COL. FRACC. CARIBE', '9831373482', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1359, 'PARTICULAR', '353', NULL, 'C. MAURICIO IVAN ODRIOZOLA ZAMBRANO', 'HOTEL-RESTAURANTE CASA TORTUGA', '9831551313', NULL, NULL, 'AV. COSTERA #567', '9831551313',
        'mauricio.odriozola@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1360, 'PARTICULAR', '350', NULL, 'C. JONNY EDUARDO CANCHE MAY', 'TAQUERIA LA GUADALUPANA', '8374526', NULL, NULL, 'AVENIDA AARON MERNO FERNANDEZ, MANZANA 55, LOTE 2',
        '8374526', 'jonny86021@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1361, 'PARTICULAR', '354', NULL, 'C. MAURICIO IVAN ODRIOZOLA ZAMBRANO', 'RESTAURANT-BAR BARBANEGRA', '9831551313', NULL, NULL, 'AVENIDA 5 ENTRE CALLE 16', '9831551313',
        'mauricio. odriozola@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1362, 'PARTICULAR', '356', NULL, 'TALLER ANONIMO YUCATAN S.A. DE C.V.', 'RESTAURANTE PUES SI', '9841309133', NULL, NULL, 'CALLE 20 # 68 ENTRE 3 Y 5, BACALAR, Q. ROO',
        '9841309133', 'taydemexico@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1363, 'PARTICULAR', '355', NULL, 'TALLER ANONIMO YUCATAN S.A. DE C.V', 'PORQUE NO PUES SI RESTAURANTE', '984139133', NULL, NULL,
        'CALLE 22 DE ENERO #153 COLONIA PLUTARCO ELIAS CALLES', '984139133', 'taydemexico@gmail.com', NULL, NULL, 'Quintana Roo', 'Chetumal', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1364, 'PARTICULAR', '355', NULL, 'TALLER ANONIMO YUCATAN S.A. DE C.V.', 'RESTAURANTE PORQUE NO PUES SI', '9841309133', NULL, NULL,
        'CALLE 22 DE ENERO #153, COL. PLUTARCO ELIAS CALLES', '9841309133', 'taydemexico@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1365, 'PARTICULAR', '357', NULL, 'C. HERMENEGILDO DIAZ GOMEZ', 'TAQUERIA LOS PRIMOS', '9831546507, 9831549540', NULL, NULL, 'AVENIDA 19 ENTRE 20 Y 22',
        '9831546507, 9831549540', 'herme7667@gamil.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1366, 'PARTICULAR', '358', NULL, 'C. DALLAN SOLEDAD CONTRERAS TAPIA', 'TORTILLERIA DALLAN', '9831014517', NULL, NULL, 'CRISTOBAL COLON ENTRE JUAREZ Y BELIZE', '9831014517',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1367, 'PARTICULAR', '359', NULL, 'C. JULIO ADRIAN JIMENEZ HUERTA', 'BANANA GO S.A. DE C.V.', NULL, NULL, NULL,
        'BOULEVARD COSTERO BACALAR SUR, REGION 1, MANZANA 5, LOTE 1, COL. CENTRO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1368, 'PARTICULAR', '360', NULL, 'C. SANDRO CICCARELLI', 'SAVORA BAKHALAL', NULL, NULL, NULL, 'CALLE 20 NO. 63-A, COL. CENTRO', NULL, NULL, NULL, NULL, 'Quintana Roo',
        'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1369, 'PARTICULAR', '361', NULL, 'C. ANNA ROVERE', 'ISCREAM BAR', NULL, NULL, NULL, 'CALLE 22 NO. 107 ENTRE AV. 5 Y 7, COL. CENTRO, BACALAR, Q. ROO', NULL, NULL, NULL,
        NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1370, 'PARTICULAR', '362', NULL, 'C. JUANA QUINTANILLA KANTUN', 'POLLOS SERGIO II', '9831442545', NULL, NULL, 'GRACIANO SANCHEZ LOTE 15 MANZANA 467', '9831442545', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1371, 'PARTICULAR', '363', NULL, 'C. ALEGRIA MONSERRAT BURGOS GONZALEZ', 'RESTAURANTE EL BARRIL', '9838677102', NULL, NULL, 'AVENIDA 7 ENTRE CALLE 20 Y 22', '9838677102',
        'bonjovanny@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1372, 'PARTICULAR', '364', NULL, 'C. FABIOLA GARCIA ALARCON', 'RESTAURANTE MEXICO GOURMET', '9838677102', NULL, NULL, 'AVENIDA 19 ENTRE CALLES 30 Y 32', '9838677102',
        'bonjovanny@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1373, 'PARTICULAR', '365', NULL, 'C. JOSUE ELIEZER CHAN CHAN', 'RESTAURANTE LA GUAPA', '9838677102', NULL, NULL, 'AVENIDA 5 ENTRE CALLES 20 Y 22', '9838677102',
        'bonjovanny@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1374, 'PARTICULAR', '366', NULL, 'C. ISIS JANNETE TAMAYO AVILA', 'BREW BAR CAFE (RESTAURANTE)', '9838332227', NULL, NULL,
        'AV. INDEPENDENCIA # 189 ENTRE CRISTOBAL COLON Y MAHATMA GANDHI, COL. CENTRO', '9838332227', 'ijta78@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1375, 'PARTICULAR', '367', NULL, 'C. TAN YUESHENG', 'RESTAURANTE DE COMIDA CHINA YING ZHOU', '9831375950', NULL, NULL,
        'AV. ERICK PAOLO MARTINEZ, MANZANA 131, LOTE 5 ESQUINA SACXAN, COL. SOLIDARIDAD', '9831375950', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1376, 'PARTICULAR', '368', NULL, 'C. FAUSTINA MARGARITA PUC ARCEO', 'COCINA BUEN DIA', '9831165044', NULL, NULL, 'PRIVADA VENUSTIANO CARRANZA #243, COLONIA ITALIA',
        '9831165044', 'fmargaritapa@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1377, 'PARTICULAR', '369', NULL, 'C. ANNA ROVERE', 'RESTAURANTE FINISTERRE', '9841056078', NULL, NULL, 'AVENIDA 3 ENTRE CALLE 24 Y 26, COLONIA CENTRO, BACALAR, Q. ROO',
        '9841056078', 'finisterresur@outlook.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1378, 'PARTICULAR', '371', NULL, 'C. ARACELY CRISÓSTOMO CUXIN', 'TAQUERIA Y TORTERIA LA BENDICION DE DIOS', '9831137563', NULL, NULL,
        'AVENIDA 9 ENTRE CALLES 28 Y 30, MANZANA 21 LOTE 13', '9831137563', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1379, 'PARTICULAR', '389', NULL, 'C. SAMANTHA DE JESUS VAZQUEZ ESCOBAR', 'PASTELERIA LAYSA', '9831206945', NULL, 'C. SAMANTHA DE JESUS VAZQUEZ',
        'CALLE 22 ENTRE AVENIDA 5 Y 7', '9831206945', 'sammvaz30@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1380, 'PARTICULAR', '372', NULL, 'C. PEDRO NICOLAS RAMIREZ VALENCIA', 'PICNIC FOOD PARK', '9983296387', NULL, 'C. PEDRO RAMIREZ VALENCIA',
        'CALLE OTHON P. BLANCO #24 ESQUINA ISLA CONTOY', '9983296387', 'luisramirezvalencia@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1381, 'PARTICULAR', '373', NULL, 'C. MARCELO ALEXIS SANCHEZ DELGADO', 'DISPENSADOR DE AGUA PURIFICADA AGUA PURA', '9981928299', NULL, 'C. MARCELO ALEXIS SANCHEZ DELGADO',
        'AV. ERICK PAOLO MARTINEZ, LOTE 24, MANZANA 120, COL. ANDRES QUINTANA ROO', '9981928299', 'alexuz19@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1382, 'PARTICULAR', '374', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT', 'PLANTA PURIFICADORA AQUA ACTIVA', '1182115', NULL, NULL,
        'AV. JAVIER ROJO GOMEZ, MANZANA 263, LOTE 05, COL. PAYO OBISPO II', '1182115', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1383, 'PARTICULAR', '375', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT', 'DISPENSADOR DE AGUA AQUA ACTIVA', '1182115', NULL, NULL,
        'JOSE MARIA LUIS MOYA #475, COL. ADOLFO LOPEZ MATEOS ENTRE INSURGENTES', '1182115', 'aguaactiva2014@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1384, 'PARTICULAR', '376', NULL, 'C. LEONIDES BATUN BAEZA', 'ANTOJITOS Y COCINA ECONOMICA LEO', '9831036293', NULL, NULL,
        'AV. MAGISTERIAL ENTRE CALLE CHABLE Y CALLE REFORMA', '9831036293', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1385, 'PARTICULAR', '377', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT', 'DISPENSADOR DE AGUA PURIFICADA AQUA ACTIVA', '1182115', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT',
        'AVENIDA CHETUMAL, MANZANA 110, LOTE 16, COLONIA ANDRES QUINTANA ROO', '1182115', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1386, 'PARTICULAR', '378', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT', 'PURIFICADORA DE AGUA AQUA ACTIVA', '1182115', NULL, 'C. MELBA MARIA SANCHEZ NAHUALT',
        'AV. CHETUMAL, MANZANA 110, LOTE 16, COL. ANDRES QUINTANA ROO', '1182115', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1387, 'PARTICULAR', '380', NULL, 'C. MELBA MARIA SANCHEZ NAHUAT', 'PURIFICADORA AQUA ACTIVA', '9831182115', NULL, NULL,
        'AV. ROJO GOMEZ, MANZANA 263, LOTE 05, COL. PAYO OBISPO II', '9831182115', 'aguaactiva2014@outlook.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1388, 'PARTICULAR', '381', NULL, 'C. ALEX YAMIR CAMEJO QUIÑONES', 'RESTAURANT XEL HA', '983152500', NULL, 'C. ALEX YAMIR CAMELO QUIÑONES',
        'AV. YUCATAN # 415, COL. YUCATAN, CALDERITAS, Q.ROO', '983152500', 'alex_yamir1@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1389, 'PARTICULAR', '382', NULL, 'C. GERMAN IRAN CAMEJO QUIÑONES', 'RESTAURANT MR. MARLIN', '9837533407', NULL, NULL,
        'CARRETERA OXTANKAH S/N. KM. 1, LOTE 1778 Y 421, CALDERITAS, Q.ROO', '9837533407', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1390, 'PARTICULAR', '383', NULL, 'C.WEN LONG LAI', 'BABU (VENTA DE BEBIDAS REFRESCANTES)', '9831162915', NULL, NULL, 'INTERIOR PLAZA LAS AMERICAS LOCAL G-13', '9831162915',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1391, 'PARTICULAR', '384', NULL, 'C. PI YUN CHEN', 'BABU (VENTA DE BEBIDAS REFRESCANTES)', '9831162915', NULL, NULL,
        'AV. CONSTITUYENTES DEL 74, MANZANA 317 ENTRE SUBTENIENTE LOPEZ Y RAUDALES', '9831162915', 'lilihsiland@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1392, 'PARTICULAR', '385', NULL, 'C. I TSEN LAI', 'BABU (VENTAS DE BEBIDAS REFRESCANTES)', '9831162915', NULL, NULL,
        'AV. JOSE MARIA MORELOS CON ISLA CANCUN #370, COL. LEONA VICARIO', '9831162915', 'lilihsliand@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1393, 'PARTICULAR', '386', NULL, 'C. ABELARDO ARZETA FLORES', 'HAMBURGUESAS Y HOT DOGS FLOY', '9831095909', NULL, 'C. ABELARDO ARZETA FLORES',
        'AV. NICOLAS BRAVO, MANZANA 178, LOTE 2 ENTRE CHABLE Y REFORMA, COL. SOLIDARIDAD', '9831095909', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1394, 'PARTICULAR', '387', NULL, 'C. LUIS EDUARDO AREVALO CORTEZ', 'RESTAURANTE EL SEÑOR DE LAS ALITAS', '9836881023', NULL, 'C. LUIS EDUARDO AREVALO CORTEZ',
        'CARMEN OCHOA DE MERINO # 230', '9836881023', 'reynacantor@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1395, 'PARTICULAR', '0EM19', NULL, 'C. MELBA BEATRIZ HERNANDEZ LAVADORES', 'PIZZERIA VICOS', '9838342040', NULL, NULL,
        'AVENIDA 7 ENTRE 26 Y 28, COLONIA MARIO VILLANUEVA MADRID', '9838342040', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1396, 'PARTICULAR', '388', NULL, 'C. GLORIA MARTINEZ GIL', 'TAQUERIA LA CHINA POBLANA', '9831091367', NULL, 'C. GLORIA MARTINEZ GIL', 'CALZADA VERACRUZ #446', '9831091367',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1397, 'INSTITUCIONAL', '389', NULL, 'C.GLORIA MARTINEZ GIL', 'TAQUERIA LA CHINA POBLANA', '9831091367', NULL, NULL, 'AV. ROJO GOMEZ #452', '9831091367', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1398, 'PARTICULAR', '390', NULL, 'C. GLORIA MARTINEZ GIL', 'TAQUERIA LA CHINA POBLANA', '9831091367', NULL, NULL, 'AV. ROJO GOMEZ #452', '9831091367', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1399, 'PARTICULAR', '391', NULL, 'C. ISABEL ALEJANDRA DAVILA', 'RESTAURANTE EL MANATI BACALAR', '9838342021', NULL, NULL, 'CALLE 22 #116, COL. CENTRO', '9838342021',
        'restaurante.elmanati@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1400, 'PARTICULAR', '393', NULL, 'C. AMBAR STEFANY IRETA ARANA', 'RESTAURANTE Y CAFETERIA ART & COFFEE', '9831090955', NULL, 'C. AMBAR STEFANY IRETA ARANA',
        'AVENIDA HEROES #161 A, COL. CENTRO', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1401, 'PARTICULAR', '394', NULL, 'C. JORGE UC TUZ', 'CENTRO RECREATIVO POPULAR LA CAPILLA', '9831144349', NULL, 'C. JORGE UC TUZ',
        'CALLE 54 #1010, COLONIA EMILIANO ZAPATA', '9831144349', 'balneariolacapilla@hotmail.com', NULL, NULL, 'Quintana Roo', 'FELIPE CARRILLO PUERTO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1402, 'PARTICULAR', '395', NULL, 'C. GIBRAN DE JESUS CUETO AGUILAR', 'DESPACHADOR DE AGUA PURIFICADA BACALAGUA 2', '9838391289', NULL, 'C. GIBRAN DE JESUS CUETO AGUILAR',
        'AVENIDA 22 POR 23 Y 25', '9838391289', 'bacalagua@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1403, 'PARTICULAR', '396', NULL, 'C. LEONORILDA CONSTANTINO EUAN', 'EMPANADAS QUINTANAS', '9831351628', NULL, 'C. LEONORILDA CONSTANTINO EUAN',
        'AV. COAHUILA #433, CALDERITAS, Q.ROO', '9831351628', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1404, 'PARTICULAR', '397', NULL, 'C. RENÉ CHAVEZ GÓMEZ', 'TAQUERIA CHAVEZ', '9232311035', NULL, 'C. RENÉ CHAVEZ GÓMEZ',
        'AV. ERICK PAOLO MARTINEZ, COL. ANDRÉS QUINTANA ROO, MANZANA 119, LOTE 24', '9232311035', 'chavezgomezrene2@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1405, 'PARTICULAR', '401', NULL, 'C. CARLOS JESUS AGUIRRE VILLEGAS', 'POLLO BRUJO', '9848078703', NULL, 'C. CARLOS JESUS AGUIRRE VILLEGAS', 'ALVARO OBREGON #208',
        '9848078703', 'melinaaguirre@pollobrujo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1406, 'PARTICULAR', '402', NULL, 'C. ARMANDO XACUR SALAZAR', 'OPERADORA MARLON S.A. DE C.V. (HOTEL)', '9832853288 / 9831266894/ 9832853279', NULL,
        'C. ARMANDO XACUR SALAZAR', 'AV. BENITO JUAREZ #88 ENTRE PLUTARCO ELIAS CALLES Y ZARAGOZA, COL. CENTRO', '9832853288 / 9831266894/ 9832853279', 'admongrand@hotmail.com',
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1407, 'PARTICULAR', '403', NULL, 'C. ARMANDO XACUR SALAZAR', 'OPERADORA MARLON S.A. DE C.V. (RESTAURANTE COCOA)', '9838321065 / 9831266894', NULL,
        'C. ARMANDO XACUR SALAZAR', 'AV. JUAREZ #87 ENTRE IGNACIO ZARAGOZA Y PLUTARCO ELIAS CALLES, COL. CENTRO', '9838321065 / 9831266894', 'admonmarlon@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1408, 'PARTICULAR', '404', NULL, 'C. RUBI GARCIA LOZANO', 'COMIDA CASERA MAGIL', '9831840870', NULL, 'C. RUBI GARCIA LOZANO',
        'AVENIDA MEXICO ENTRE INDEPENDENCIA Y CONSTITUYENTES #202', '9831840870', 'magil_felix@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1409, 'PARTICULAR', '398', NULL, 'C. LILIANA MENDOZA BUENDIA', 'BIOCLINICOS FLEMING S.A. DE C.V.', '63830083 ext 126', NULL, 'BIOCLINICOS FLEMING S.A. DE C.V.',
        'AV. 16 DE SEPTIEMBRE 202, MANZANA 15, LOTE 4, COLONIA ALFREDO V. BONFIL', '63830083 ext 126', 'liliana.mendoza@laboratoriosbiofleming.c', NULL, NULL, 'México', 'MÉXICO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1410, 'PARTICULAR', '399', NULL, 'C. CARLOS EMILIANO MARIN VAZQUEZ', 'RESTAURANT-BAR PERAZA', '2855627', NULL, 'C. CARLOS EMILIANO MARIN VAZQUEZ',
        'CALLE MAHATMA GANDHI #205', '2855627', 'vhpc.9524@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1411, 'PARTICULAR', '400', NULL, 'C. HERMELINDA GASPAR DE LA CRUZ', 'ASADERO LA JAROCHITA', '9831003566', NULL, 'C. HERMELINDA GASPAR DE LA CRUZ',
        'AVENIDA BUGAMBILIAS ENTRE FLOR DE MAYO Y FLAMBOYANES', '9831003566', 'dacoh94@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1412, 'PARTICULAR', '406', NULL, 'C. YARA PORTILLO NAVARRO', 'TOTO BLUE S.A. DE C.V. (HOTEL)', '9838385924', NULL, 'YARA PORTILLO NAVARRO',
        'AV. 7 ENTRE CALLE 18 Y 20, LOTE 20, MANZANA 1, COL. CENTRO', '9838385924', 'reservacionestotoblue@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1413, 'PARTICULAR', '456', NULL, 'C. LOURDES MAURICIA VILLEGAS LOPEZ', 'POLLO BRUJO', '9848078703', NULL, 'C. LOURDES MAURICIA VILLEGAS LOPEZ',
        'CALLE CAMELIAS #425, ESQUINA NAPOLES, COL. JOSEFA ORTIZ DE DOMINGUEZ', '9848078703', 'melinaaguirre@pollobrujo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1414, 'PARTICULAR', '407', NULL, 'C. ISACC GOMEZ SAENZ', 'PANADERIA II HERMANOS', '9831405234', NULL, 'C. ISACC GOMEZ SAENZ',
        'AV. NICOLAS BRAVO ESQUINA CURACAO, MANZANA 92, LOTE 5', '9831405234', 'alejandrogomeztorres088@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1415, 'PARTICULAR', '123', NULL, 'C. DAVID PEREZ LOPEZ', 'PERLOP ALIMENTOS', '9611155337', NULL, 'C. DAVID PEREZ LOPEZ',
        'INTERIOR DE LA PLATA DE ASA COMBUSTIBLES AEROPUERTO CHETUMAL, COMEDOR DE EMPLEAD', '9611155337', 'perlopalomentosfacturas@hotmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1416, 'PARTICULAR', '213', NULL, 'C. EDITH MAGARITA SANCHEZ JIMENEZ', 'LONCHERIA LA TEKAXEÑA', '9831560879', NULL, 'C. EDITH MAGARITA SANCHEZ JIMENEZ',
        'AVENIDA MACHUXAC, MANZANA 240, LOTE 1', '9831560879', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1417, 'PARTICULAR', '408', NULL, 'OPERADORA NICXA S.A. DE C.V.', 'KENTUCKY FRIED CHICKEN', '9831271117', NULL, 'OPERADORA NICXA SA DE CV',
        'AVENIDA INSURGENTES INTERIOR DE PLAZA LAS AMERICAS LOCAL G-11', '9831271117', 'KFC1346@gruponicxa.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1418, 'PARTICULAR', '409', NULL, 'C. MARIA ELENA ASENCIO VILLAMIL', 'COLEGIO PARTICULAR INCORPORADO PRIMITIVO ALONSO S.C.', '9831118624', NULL,
        'C. MARIA ELENA ASENCIO VILLAMIL', 'AV. CENTENARIO #595 ENTRE JESUS URUETA E IGNACIO COMOFORT', '9831118624', 'admonprimitivoimee@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1419, 'PARTICULAR', '900', NULL, 'C. FRANCISCO JAVIER MCLIVERTY PACHECO', 'COCINA ECONOMICA MAC´S', '9838343225', NULL, 'C. FRANCISCO JAVIER MC LIVERTY PACHECO',
        'CALLE 20, LOTE 8, MANZANA 35, COLONIA GONZALO GUERRERO', '9838343225', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1420, 'PARTICULAR', '963', NULL, 'C. ARGENIS GONGORA VALENCIA', 'KGB GRILL (KANGREBURGER)', '9838675285', NULL, 'C. ARGENIS GONGORA VALENCIA',
        'AVENIDA CARRANZA ENTRE AVENIDA ANDRES QUINTANA ROO', '9838675285', 'gongora@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1421, 'PARTICULAR', '410', NULL, 'C. JOSEFINA TRINIDAD RODRIGUEZ', 'TORTILLERIA LA POBLANITA', '9831810727', NULL, 'C. JOSEFINA TRINIDAD RODRIGUEZ',
        'CALLE MARGARITA MAZA DE JUAREZ, MANZANA 751, LOTE 20 ENTRE NIZUC Y SACXAN', '9831810727', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1422, 'PARTICULAR', '411', NULL, 'C. JORGE ALBERTO HERRERA CETINA', 'TAQUERIA Y TORTERIA EL LECHON DORADO', '9831090955', NULL, 'C. JORGE ALBERTO HERRERA CETINA',
        'CALZADA VERACRUZ #321, COLONIA ADOLFO LOPEZ M.', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1423, 'PARTICULAR', '412', NULL, 'C. JOSE ALBERTO FLORES AREVALO', 'TACO LOCO BACALAR', '9831716840', NULL, 'C. JOSE ALBERTO FLORES AREVALO',
        'CARRETERA FEDERAL BACALAR-XTOMOC, REGION 13, MANZANA 253, LOTE 16 ENTRE CAOBA', '9831716840', 'tacolocobacalar@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1424, 'PARTICULAR', '414', NULL, 'C.NANCY ARGEMY ORTEGA BALLOTE', 'ANTOJITOS NANCY', '9837327584', NULL, 'C.NANCY ARGEMY ORTEGA BALLOTE',
        'CALLE FELIPE CARRILLO PUERTO ESQUINA IGNACIO ALTAMIRANO #530', '9837327584', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1425, 'PARTICULAR', '413', NULL, 'C. JUANA SANTOS TECOMAGUA', 'PURIFICADORA CHAGUITA', '9831255757/6625757/9831826766', NULL, 'C. JUANA SANTOS TECOMAGUA',
        'AVENIDA MICHOACAN CON TABASCO, LA UNION, Q.ROO', '9831255757/6625757/9831826766', 'cancer4580@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1426, 'PARTICULAR', '968', NULL, 'C. JENNY CORTES GUTIERREZ', 'RESTAURANTE ARTACO', '9983366338', NULL, 'C. JENNY CORTEZ GUTIERREZ',
        'AV. INSURGENTES KM 5.025 , LOCAL G-10, COL. EMANCIPACION INFONAVIT', '9983366338', 'jenny.mrs@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1427, 'PARTICULAR', '423', NULL, 'C. HERNAN HERRERA ESTRADA', 'BANQUETAKOS', '9831540748', NULL, 'C. HERNAN HERRERA ESTRADA',
        'PLUTARCO E. CALLES, LOTE 4 ESQUINA BOULEVARD BAHIA', '9831540748', 'hernan_herrera@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1428, 'PARTICULAR', '424', NULL, 'C. PETRONILA DE JESUS POOT UH', 'RESTAURANTE EL ACUARIO', '9838359697', NULL, 'C. PETRONILA DE JESUS POOT UH',
        'AVENIDA OAXACA #12 CALDERITAS Q.ROO', '9838359697', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1429, 'PARTICULAR', '425', NULL, 'C. HECTOR CELIO SOTRES', 'PANADERIA LA TARTALETA BACALAR', '9831547676', NULL, 'C. HECTOR CELIO SOTRES',
        'CALLE 5 ESQUINA CALLE 24 , COLONIA CENTRO', '9831547676', 'hector-celio@yahoo.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1430, 'INSTITUCIONAL', '426', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE', 'PANADERIA LA TARTALETA CHETUMAL', '9831547676', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE',
        'AVENIDA ALVARO OBREGON #195 ESQUINA HEROES', '9831547676', 'hector-celio@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1431, 'PARTICULAR', '427', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE', 'PANADERIA LA TARTALETA CHETUMAL', '9831547676', NULL, 'C. MANUEL ALBERTO ARMAS DUARTE',
        'AVENIDA ALVARO OBREGON #195 ESQUINA HEROES', '9831547676', 'hector-celio@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1432, 'PARTICULAR', '700', NULL, 'C. JOVITA HERRERA BAEZ', 'TORTILLERIA CASTILLO', '9831236770', NULL, 'C. JOVITA HERRERA BAEZ',
        'CERRADA BENITO JUAREZ #2, COLONIA VILLAFLORES, POBLADO JAVIER ROJO GOMEZ, Q.ROO', '9831236770', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1433, 'PARTICULAR', '437', NULL, 'C. CELIA VIRIDIANA REYES CAPISTRAN', 'GRUPO PRODUCTOR ARTESA S.A. DE C.V.', '2855555', NULL, NULL,
        'AVENIDA HEROES #181-A ESQUINA SAN SALVADOR, COL. CENTRO', '2855555', 'chetumal@lamazza.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1434, 'PARTICULAR', '441', NULL, 'C. MARTHA VICTORIA CASTILLO OLIVERA', 'TORTILLERIA LA OBRERA', '9831236770', NULL, 'C. MARTHA VICTORIA CASTILLO OLIVERA',
        'AV. LEONA VICARIO #21, COL. OBRERA, JAVIER ROJO GOMEZ, Q.ROO', '9831236770', 'abarrotescas@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1435, 'INSTITUCIONAL', '442', NULL, 'DIRECTORA EJECUTIVA DE CONTROL ANALITICO', 'IMELDA ROCIO GUZMAN CERVANTES', NULL, NULL, 'IMELDA ROCIO GUZMAN CERVANTES',
        'CIUDAD DE MEXICO', NULL, NULL, NULL, NULL, 'Distrito Federal', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1436, 'PARTICULAR', '443', NULL, 'COMISION PARA LA JUVENTUD Y EL DEPORTE DE QUINTANA ROO', 'COJUDEQ (UNIDAD DEPORTIVA DE CLAVADOS)', '9838330019 Y 20', NULL,
        'COMISION PARA LA JUVENTUD Y EL DEPORTE DE QUINTANA ROO', 'CALZADA VERACRUZ #59 , COLONIA BARRIO BRAVO', '9838330019 Y 20', 'ricardo_hernandezinx@hotmail.com', NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1437, 'PARTICULAR', '444', NULL, 'C. FELIPE TAMAYO COLLADO', 'RESTAURANT BAR LA TOSTADA Y LA GUAYABA', '9838670221', NULL, 'C. FELIPE TAMAYO COLLADO',
        'AVENIDA ADOLFO LOPEZ MATEOS ESQUINA MILAN #405', '9838670221', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1438, 'PARTICULAR', '445', NULL, 'C. LARISSA GABRIELA PEREZ ZAFRA', 'RESTAURANT WINNER''S', '8321737', NULL, 'C. JARISSA GABRIELA PEREZ ZAFRA',
        'CALLE 22 DE ENERO #141, COLONIA PLUTARCO ELIAS CALLES', '8321737', 'ganador_facturas@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1439, 'PARTICULAR', '446', NULL, 'C. RAUL ENRIQUE ANDRADE ANGULO', 'HOTEL NOOR', '9831134361', NULL, 'C. RAUL ENRIQUE ANDRADE ANGULO', 'MORELOS #3 CON BOULEVARD',
        '9831134361', 'rsserico@yahoo.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1440, 'PARTICULAR', '447', NULL, 'INSTITUTO BIBLICO NUEVOS INICIOS', NULL, '9838357027', NULL, NULL, 'CARRETERA XUL-HA - BACALAR, KM. 19', '9838357027', NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1441, 'PARTICULAR', '448', NULL, 'C. DAVID ARTURO MARTINEZ SANCHEZ', 'PURIFICADORA DE AGUA AGUAZUL', '9831206179', NULL, 'C. DAVID ARTURO MARTINEZ SANCHEZ',
        'AV. 9 S/N. ENTRE CALLE 8 Y CALLE 10, COL. SOR JUANA INES', '9831206179', 'negocios.davidmartinez@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1442, 'PARTICULAR', '449', NULL, 'C. DAVID ARTURO MARTINEZ SANCHEZ', 'HOTEL HACIENDA BACALAR', '9832107201', NULL, 'C. DAVID ARTURO MARTINEZ SANCHEZ',
        'CALLE 10, MANZANA 15, LOTE 4, ENTRE AV. 9 Y AV. 9-A, COL. SOR JUANA INES', '9832107201', 'negocios.davidmartinez@hotmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1443, 'PARTICULAR', '596', NULL, 'C. PASTORA GUERRERO MARTINEZ', 'ANTOJITOS EL PRIMO', '9831122237', NULL, 'C. PASTORA GUERRERO MARTINEZ', 'CALZADA VERACRUZ #627',
        '9831122237', 'leonguerrerocristal@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1444, 'PARTICULAR', '901', NULL, 'CORPORATIVO PADEM S.A DE C.V.', 'RESTAURANT VENTOLERA', '2673370', NULL, NULL, 'AVENIDA CARMEN OCHOA DE MERINO #166,COLONIA CENTRO',
        '2673370', 'contabilidad@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1445, 'PARTICULAR', '450', NULL, 'C. EDUARDO ANSTON GONZALEZ DORANTES', 'TORTILLERIA PLUTARCO', '9831084256', NULL, NULL, 'AV. PLUTARCO ELIAS CALLES #368-C, COL. CENTRO',
        '9831084256', 'grupocontable00@yahoo', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1446, 'PARTICULAR', '451', NULL, 'C. JOSE ALFREDO HERNANDEZ RAMOS', 'PESCADERIA EL CHARAL', '9841644218', NULL, NULL, 'CALLE 22 FINAL Y 19 LIBRAMIENTO, COL. BENITO JUAREZ',
        '9841644218', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1447, 'PARTICULAR', '452', NULL, 'C. MELINA MARAVILLA ROMERO', 'AGUA CLARA BACALAR A. C.', '5542907630', NULL, 'C. MELINA MARAVILLA ROMERO', 'BOULEVARD COSTERA',
        '5542907630', NULL, NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1448, 'PARTICULAR', '195', NULL, 'C. CAROLINA VALENCIA HERNANDEZ', 'RESTAURANTE TSUNAMI EXPRESS FOOD', '9831191586', NULL, 'C. CAROLINA VALENCIA HERNANDEZ',
        'AV. ANDRES QUINTANA ROO # 296 A ESQUINA JUSTO SIERRA', '9831191586', 'kro_118@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1449, 'PARTICULAR', '198', NULL, 'C. KARLA CAROLINA PEREZ QUINTERO', 'RESTAURANT BAR PIAZZA MAGGIORE', '9831653855', NULL, NULL,
        'CAMPECHE #87 ENTRE GUERRERO Y COAHUILA, CALDERITAS, Q.ROO', '9831653855', 'chak_nab@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1450, 'PARTICULAR', '453', NULL, 'C. EMMA GUZMAN CARRASCO', 'HOTEL ESPAÑA', '9838360326', NULL, NULL, 'AVENIDA HEROES #407', '9838360326', 'doctorespananovelo@hotmail.com',
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1451, 'INSTITUCIONAL', '454', NULL, 'DIRECTORA EJECUTIVA DE CONTROL ANALITICO', 'M. EN I. IMELDA ROCIO GUZMAN CERVANTES', NULL, NULL, NULL,
        'CALZADA DE TLALPAN NO. 4492, COL. TORIELLO GUERRA, CD. DE MEXICO', NULL, NULL, NULL, NULL, 'México', 'MÉXICO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1452, 'PARTICULAR', '999', NULL, 'C. ANASTACIA MARIA DE LOS ANGELES', 'COCINA ECONOMICA COCINA AZTECA', '9831369884', NULL, NULL,
        'ERCIK PAOLO MARTINEZ ENTRE POLYUC Y PETCACAB, MANZANA 119, LOTE 24', '9831369884', 'elaztecacocina@ hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1453, 'PARTICULAR', '455', NULL, 'C. EFREN MUNGUIA CANELA', 'RESTAURANT EL BORREGO FELIZ', '9831689545', NULL, 'C. EFREN MUNGUIA CANELA',
        'AVENIDA LIBRAMIENTO ENTRE CALLE 38 Y 40', '9831689545', 'toreto418@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1454, 'PARTICULAR', '456', NULL, 'C. ENEYDA LOPEZ MENDOZA', 'PURIFICADORA AGUA INMACULADA', '9837532978', NULL, NULL, 'AVENIDA MACHUXAC #162, COL. PAYO OBISPO II ETAPA',
        '9837532978', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1455, 'PARTICULAR', '457', NULL, 'C. GERTRUDIS MARTINEZ RUIZ', 'GIMNASIO NOHOCH SUKUN', '9831264150', NULL, NULL, 'AV. INSURGENTES #380 ESQUINA CALLE 20 DE NOVIEMBRE',
        '9831264150', 'peketraviesanive@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1456, 'PARTICULAR', '456', NULL, 'C. DORA ELIZABETH MINGUER ALCOCER', 'OPERADORA CARIBE MEXICANO S.A. DE C.V. (RESTAURANT)', '9831292769', NULL, NULL,
        'AV. HEROES # 326, COL. ADOLFO LOPEZ MATEOS', '9831292769', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1457, 'PARTICULAR', '457', NULL, 'C. URBELINA ABIGAIL ZAVALETA PERAZA', 'EMPANADAS DE ORO III', '9838392247', NULL, NULL, 'AV. MAXUXAC, MANZANA 234, LOTE 6', '9838392247',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1458, 'PARTICULAR', '458', NULL, 'C. NABILE NOEMI RODRIGUEZ PAT', 'TORTILLERIA HERMANOS RODRIGUEZ', '9831195077', NULL, NULL,
        'ANDADOR 19 AN53, LOCAL 2, COL. INFONAVIT FIDEL VELAZQUEZ', '9831195077', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1459, 'PARTICULAR', '459', NULL, 'C. WILBERT ANTONIO CANTO CASTILLO', 'HOTEL PRINCIPE', '9838324799', NULL, 'C. WILBERT ANTONIO CANTO CASTILLO', 'PROLONGACION HEROES #326',
        '9838324799', 'principearlequin@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1460, 'PARTICULAR', '460', NULL, 'C. JAVIER GUSMAN CORTES', 'TAQUERIA EL PASTOR MISTECO', '9831090955', NULL, NULL,
        'CARRETERA FEDERAL, MANZANA 37, LOTE 04, MAHAHUAL, Q. ROO', '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1461, 'PARTICULAR', '800', NULL, 'SOCIEDAD COOPERATIVA SELVA VIVA DE R.L. DE C.V.', 'EMPRESA SELVA VIVA 3G', '9831316582', NULL, NULL,
        'CALLE JAVIER ROJO GOMEZ S/N., MANZANA 43, LOTE 01, POBLADO TRES GARANTIAS, Q.ROO', '9831316582', 'selvaviva3g@gmail.com', NULL, NULL, 'Quintana Roo',
        'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1462, 'PARTICULAR', '461', NULL, 'C. MARIELA KARINA GUITIAN CRUZ', 'ESCUELA DE NATACION LA ATLANTIDA', '9831306502', NULL, NULL, 'BUGAMBILIAS ENTRE HEROES Y CAOBAS',
        '9831306502', 'dali1009@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1463, 'PARTICULAR', '482', NULL, 'C. CARLOS HUMBERTO VALLEJOS BARRERA', 'DISPENSADOR AGUA PURIFICADA AGUA DEL VALLE', '9838360501', NULL, NULL,
        'RIO VERDE S/N., MANZANA 233, LOTE 11', '9838360501', 'vallejos_carlos@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1464, 'PARTICULAR', '463', NULL, 'C. OYUKY ASIAK RAMIREZ AVILA', 'POLLO ASADOS NIKI', '9831240955', NULL, NULL,
        'AV. JAVIER ROJO GOMEZ, MANZANA 49, LOTE 2, COL. PAYO OBISPO II', '9831240955', 'iguanasranas21@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1465, 'PARTICULAR', '198', NULL, 'C. JORGE TAMAYO COLLADO', 'REST. BAR SAVORA GRILL Y EJECUTIVO NIGHT CLUB', '45331335124', NULL, NULL,
        'AV. PLUTARCO ELIAS CALLES ESQUINA 5 DE MAYO', '45331335124', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1466, 'PARTICULAR', '464', NULL, 'C. BLANCA ARACELY POOT CHI', 'LONCHERIA MELISSA', '9831831048', NULL, NULL,
        'AV. CHICOZAPOTE #239 ESQUINA MARGARITA MAZA, FRACC. ARBOLEDAS 2', '9831831048', 'notarias_david@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1467, 'INSTITUCIONAL', '465', NULL, 'C. FERNANDO NOE SOSA CRUZ', 'RESTAURANT VENTO', '9831208360', NULL, NULL, 'CIRCUITO 1 SUR, COLONIA PACTOOBRERO #720', '9831208360',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1468, 'PARTICULAR', '466', NULL, 'C. FERNANDO NOE SOSA CRUZ', 'RESTAURANT VENTO', '9831208360', NULL, NULL, 'CIRCUITO 1 SUR B, COLONIA PACTO OBRERO #770', '9831208360',
        NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1469, 'PARTICULAR', '466', NULL, 'E2 OPERACIONES SAS DE C.V.', 'SUBWAY', '983132476', NULL, NULL, 'AV. INSURGENTES, KM. 5.025, INTERIOR PLAZA LAS AMÉRICAS LOCAL G-9',
        '983132476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1470, 'PARTICULAR', '467', NULL, 'E2 OPERACIONES SAS DE C.V.', 'SUBWAY', '9831302476', NULL, NULL,
        'AV. OTHON P. BLANCO #178, ENTRE 5 DE MAYO Y 16 DE SEPTIEMBRE, COL. CENTRO.', '9831302476', 'a.cel@e2.com.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1471, 'PARTICULAR', '468', NULL, 'C. ROSA CARMEN MEDINA PEREZ', 'LONCHERIA CARLITOS', '8371553', NULL, NULL, 'JUSTO SIERRA ESQ. 4 DE MARZO, #524', '8371553', NULL, NULL,
        NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1472, 'PARTICULAR', '469', NULL, 'C. ADRIANA GAYTAN TERRAZAS', 'CASA DE HUÉSPEDES VILLAS COLIBRÍ HOTELERIA', '983126112', NULL, NULL,
        'CALLE 12 ENTRE 19 Y 21, MANZANA 35, LOTE 01, COL. NUEVO PROGRESO', '983126112', 'VILLASCOLIBRI55@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1473, 'PARTICULAR', '470', NULL, 'TALLER ANONIMO YUCATAN S.A. DE C.V.', 'HELADERIA Y PIZZERIA PORQUE NO PUES SI', NULL, NULL, NULL,
        'AV. ANDRES QUINTANA ROO #127 COL. CENTRO', NULL, NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1474, 'PARTICULAR', '409', NULL, 'C. HILARIO CHI COCOM', 'PURIFICADORA MANANTIAL DE VIDA', '9831267928', NULL, NULL,
        'CALLE FRANCISCO I. MADERO ESQ. EMILIANO ZAPATA, POBLADO NICOLAS BRAVO, Q.ROO', '9831267928', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1475, 'PARTICULAR', '111111', NULL, 'RESTAURANT LA FELICIDAD', NULL, NULL, NULL, 'RESTAURANT LA FELICIDAD', 'AVENIDA OAXACA #13, CALDERITAS, Q.ROO', NULL, NULL, NULL, NULL,
        'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1476, 'PARTICULAR', '471', NULL, 'C. MERCEDES GONGORA VAZQUEZ', 'PANADERIA LA ABEJA', '9838326702', NULL, 'C. MERCEDES GONGORA VAZQUEZ',
        'CALLE FRANCISCO J. MUJICA N°507. COLONIA ADOLFO LOPEZ MATEOS', '9838326702', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1477, 'PARTICULAR', '471', NULL, 'C. JOSUE EUTIQUIO FIGUEROA MORALES', NULL, '9831094961', NULL, NULL, 'COROZAL, BELICE', '9831094961', 'facture_matilde@outlook.com', NULL,
        NULL, 'Quintana Roo', 'BELICE', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1478, 'PARTICULAR', '969', NULL, 'C. GUADALUPE DZIB CHAN', 'RESTAURANT FAMILIAR FADICA', '9838395420', NULL, 'C. GUADALUPE DZIB CHAN',
        'AV. CHETUMAL S/N. CON 6 DE OCTUBRE, COL. LAZARO CARDENAS', '9838395420', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1479, 'PARTICULAR', '474', NULL, 'LIC. AIDA MARGARITA RODRIGUEZ HOY', 'ESTANCIA INFANTIL DEL ISSSTE 091', '8321904', NULL, 'LIC. AIDA MARGARITA RODRIGUEZ HOY',
        'AV. CRISTOBAL COLON #201 ESQUINA JOSE MARIA MORELOS', '8321904', 'amargarita.rodriguez@issste.gob.mx', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1480, 'PARTICULAR', '475', NULL, 'C. GILBERTO DOMINGO HENDRICKS DIAZ', 'CABAÑAS EL CHITAL', '9831206322', NULL, 'C. GILBERTO DOMINGO HENDRICKS DIAZ',
        'DOMICILIO CONOCIDO RAUDALES Q. ROO', '9831206322', 'turismo_diaz@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1481, 'PARTICULAR', '476', NULL, 'C. BRENDA VANESA CEH SANCHEZ', 'ABARROTES, CARNES FRÍAS Y LÁCTEOS JAMONELI', '9831216213', NULL, 'C. BRENDA VANESA CEH SANCHEZ',
        'AV. LUIS MANUEL SEVILLA #1060 ESQUINA ECUADOR, FRACC. LAS AMERICAS', '9831216213', 'brendagut@hotmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL,
        NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1482, 'PARTICULAR', '970', NULL, 'C. JUAN MANUEL VELAZQUEZ', 'CENDI DIF', '9831211179', NULL, 'AVENIDA INSURGENTES S/N.', 'AVENIDA INSURGENTES S/N.', '9831211179', NULL,
        NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1483, 'PARTICULAR', '971', NULL, 'C. SANDRA RODRIGUEZ PANIAGUA', 'CAFETERIA ESCOLAR EL MANGO', '9831583383', NULL, 'C. SANDRA RODRIGUEZ PANIAGUA',
        'AVENIDA INSURGENTES, COLONIA RAFAEL E. MELGAR', '9831583383', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1484, 'PARTICULAR', '972', NULL, 'C. CONCEPCION LEOCADIO YAM PUC', 'CABAÑAS MUUL HA (HOSPEDAJE)', '9831178914', NULL, 'C. CONCEPCION LEOCADIO YAM PUC',
        'CALLE AARON MERINO FERNANDEZ S/N. (PRIVADA) POR 10 DE MAYO, XUL HA Q. ROO', '9831178914', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1485, 'PARTICULAR', '578', NULL, 'C. ANTHONY CHEN', 'RESTAURANT PANDA EXPRESS', '9831302476', NULL, 'C. ANTHONY CHEN',
        'AVENIDA INSURGENTES KM 5.025 INTERIOR PLAZA LAS AMERICAS LOCAL G-3', '9831302476', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1486, 'PARTICULAR', '579', NULL, 'COMERCIALIZADORA PORCICOLA MEXICANA S.A. DE C.V.', 'MAXICARNE', '9982793977', NULL, 'COMERCIALIZADORA PORCINA MEXICANA S.A. DE C.V.',
        'AV. JAVIER ROJO GOMEZ, MANZANA 268, LOTE 7, COL. PAYO OBISPO', '9982793977', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1487, 'PARTICULAR', '580', NULL, 'C. LORENZA PAT CANCHE', 'LONCHERIA LA FLOR DEL CARIBE', '9831167738', NULL, 'C. LORENZA PAT CANCHE',
        'MERCADO M. IGNACIO ALTAMIRANO LOCAL 1', '9831167738', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1488, 'PARTICULAR', '590', NULL, 'C. RUBEN ANTONIO ALONSO TORRES', 'CEVICHERIA EL ISLOTE', '9831090955', NULL, NULL, 'GUADALUPE VICTORIA # 124, COL. BARRIO BRAVO',
        '9831090955', 'grupocontable00@yahoo.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1489, 'PARTICULAR', '581', NULL, 'C. JOSE FILIBERTO CHAN TRUJEQUE', 'PURIFICADORA DE AGUA RAUDALES', '9837522323', NULL, 'C. JOSE FILIBERTO CHAN TRUJEQUE',
        'CALLE RAUDALES ESQUINA ALFREDO V. BONFIL MANZANA 157 LOTE 16', '9837522323', 'filibertochantrujeque1969@gmail.com', NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO',
        NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1490, 'PARTICULAR', '582', NULL, 'MBH MAYA BACALAR S.A. DE C.V.', 'RESTAURANTE XANT HA, BY: MAYA BACALAR', '1.99831E+11', NULL, 'MBH MAYA BACALAR S.A DE C.V.',
        'CALLE 40, REGIÓN 16, MANZANA 67, LOTE 6', '1.99831E+11', 'mbh.mayabacalar@gmail.com', NULL, NULL, 'Quintana Roo', 'BACALAR', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1491, 'PARTICULAR', '983', NULL, 'C. JOSE FLAVIO RUIZ ACOSTA', 'RESTAURANT LA PEQUEÑA ROCA DE ORO', '9838367717', NULL, NULL, 'AVENIDA OAXACA #02, CALDERITAS, Q.ROO',
        '9838367717', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1492, 'PARTICULAR', '583', NULL, 'C. JOANA LIZETT RESENDIZ SANTOS', 'CLAKS PIZZA (PIZZERIA)', '9831090955', NULL, 'C. JOANA LIZETT RESENDIZ SANTOS',
        'CALZADA VERACRUZ #376 COLONIA LOPEZ MATEOS', '9831090955', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
INSERT INTO laboratorio_estatal.origenes (id, tipo, clave, iniciales, origen, razon_social, telefono, user_id, director_nombre, director_direccion, director_telefono,
                                          director_email, director_cargo, director_preposicion, director_estado, director_municipio, created_at, updated_at)
VALUES (1493, 'PARTICULAR', '584', NULL, 'C. JULIO ANTONIO JIMENEZ FLORES', 'HOTEL LAGON', '9838345270', NULL, 'C. JULIO ANTONIO JIMENEZ FLORES',
        'AVENIDA MEXICO MANZANA 19 LOTE 1-A, ENTRE REVOLUCION Y OAXACA, SUBTENIENTE LOPEZ', '9838345270', NULL, NULL, NULL, 'Quintana Roo', 'OTHON POMPEYO BLANCO', NULL, NULL);
