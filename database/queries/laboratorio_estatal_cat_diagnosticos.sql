TRUNCATE laboratorio_estatal.cat_diagnosticos;
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (1, 'Brucelosis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (2, 'Cáncer cérvico uterino', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (3, 'Carga viral VIH', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (4, 'Chikungunya', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (5, 'Chlamydia trachomatis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (6, 'Citomegalovirus', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (7, 'Control de calidad Baciloscopia', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (8, 'Control de calidad Chagas', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (9, 'Control de calidad Lesihmania', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (10, 'Control de calidad paludismo', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (11, 'Cuantificación de Linfocitos TCD4/CD8', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (12, 'Dengue', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (13, 'Dengue, Zika, Chikungunya', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (14, 'Diagnóstico de chagas', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (15, 'Enfermedad de chagas', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (16, 'Epstein-Barr', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (17, 'Hepatitis A', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (18, 'Hepatitis B', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (19, 'Hepatitis C', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (20, 'Herpes 1 y 2', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (21, 'Influenza', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (22, 'Leishmaniasis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (23, 'Lepra', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (24, 'Leptospirosis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (25, 'Paludismo', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (26, 'Parvovirus', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (27, 'Rabia', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (28, 'Rotavirus', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (29, 'Rubéola', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (30, 'Sarampión', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (31, 'Sífilis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (32, 'Taxonomía de Larvas', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (33, 'Taxonomía de mosquito adulto', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (34, 'Taxonomía y Parasitológico de Triatomas', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (35, 'Tos ferina', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (36, 'Toxoplasmosis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (37, 'Tuberculosis', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (38, 'Varicela', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (39, 'VIH', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (40, 'VPH', NULL, NULL);
INSERT INTO laboratorio_estatal.cat_diagnosticos (id, descripcion, created_at, updated_at)
VALUES (41, 'ZIKA', NULL, NULL);
