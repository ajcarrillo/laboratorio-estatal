INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (1, 'FISICOQUIMICOS', 'SANITARIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (2, 'MICROBIOLOGICOS', 'SANITARIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (3, 'BACTERIOLOGÍA
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (4, 'BIOLOGÍA MOLECULAR
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (5, 'ARBOVIRUS Y EMERGENTES
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (6, 'ENTOMOLOGÍA
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (7, 'INMUNOLOGÍA
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (8, 'VIROLOGÍA
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (9, 'SEROLOGÍA
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (10, 'MICOBACTERIAS
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (11, 'TRANSMISIBLES POR VECTOR
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (12, 'VIH
', 'EPIDEMIOLOGIA', NULL, NULL);
INSERT INTO laboratorio_estatal.laboratorios (id, descripcion, tipo, created_at, updated_at)
VALUES (13, 'RABIA Y ZOONOSIS', 'EPIDEMIOLOGIA', NULL, NULL);
