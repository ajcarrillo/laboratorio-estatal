INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (1, 'AAA', 'ALIMENTOS DE ALTA ACIDEZ (pH', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (2, 'ABA', 'ALIMENTOS DE BAJA ACIDEZ (pH >= 4.6)', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (3, 'ACO', 'ALIMENTOS PREPARADOS COCIDOS SOLIDOS', 3, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (4, 'ACR', 'ALIMENTOS PREPARADOS COCIDOS LIQUIDOS', 3, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (5, 'ADB', 'AGUA DE BAHIA', 1, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (6, 'ADM', 'AGUA DE MAR', 1, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (7, 'AGBL', 'AGUAS BLANCAS', 1, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (8, 'AMX', 'ALIMENTOS PREPARADOS MIXTOS', 3, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (9, 'AUCH', 'AGUA DE USO Y CONSUMO HUMANO', 1, 'Agua de uso y consumo humano', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (10, 'BAH', 'BEBIDAS ALCOHÓLICAS', 6, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (11, 'BNA', 'BEBIDAS NO ALCOHÓLICAS', 6, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (12, 'BOR', 'ORINA CLENBUETROL', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (13, 'BSA', 'SANGRE CLENBUTEROL', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (14, 'CBV', 'CARNE DE BOVINOS', 8, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (15, 'CCD', 'CARNE DE CERDO', 8, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (16, 'CCM', 'CARNE MOLIDA', 8, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (17, 'CDA', 'CUERPOS DE AGUA DULCE/ALBERCAS', 1, 'Agua', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (18, 'CHI', 'HÍGADO', 36, 'Higado', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (19, 'CIGO', 'CIGUATA (ORINA)', 38, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (20, 'CIGV', 'CIGUATA (VOMITO)', 39, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (21, 'CMV', 'MÚSCULO', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (22, 'CONT', 'VIAL', 3, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (23, 'CPLL', 'CARNE DE POLLO', 8, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (24, 'CPR', 'CÁRNICOS PROCESADOS', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (25, 'ECH', 'CHORIZO', 13, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (26, 'ELO', 'LONGANIZA', 13, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (27, 'ESA', 'SALCHICHA', 13, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (28, 'EVA', 'EMBUTIDOS VARIOS', 13, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (29, 'GHA', 'PESCADOS', 30, 'Pescados', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (30, 'HAP', 'HIELOS Y AGUA PURIFICADOS', 1, 'Agua', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (31, 'HUE', 'HUEVO', 21, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (32, 'LCQ', 'CREMA Y QUESOS SIN PASTEURIZAR', 23, 'Crema y quesos sin pasteurizar', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (33, 'LLE', 'LECHE', 23, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (34, 'LQA', 'QUESOS ARTESANALES', 23, 'Quesos artesanales', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (35, 'MF', 'MUESTRA FORTIFICADA', 40, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (36, 'ODL', 'OTROS DERIVADOS LÁCTEOS', 23, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (37, 'ODLG', 'OTROS DERIVADOS LACTEOS G', 23, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (38, 'PCR', 'CRUSTÁCEOS', 30, 'Crustáceos', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (39, 'PLA', 'PLAYAS', NULL, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (40, 'PLY', 'PLAYAS', 1, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (41, 'PMB', 'MOLUSCOS BIVALVOS', 26, 'Moluscos bivalvos', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (42, 'PMC', 'MOLUSCOS CEFALOGASTERÓPODOS', 30, 'Moluscos cefalogasterópodos', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (43, 'PPS', 'PESCADOS', 30, 'Pescados', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (44, 'SAL', 'SAL', 10, 'Sal', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (45, 'SLS', 'SALSAS CRUDAS Y COCIDAS', 31, NULL, NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (46, 'SPI', 'SUPERFICIES INERTES', 33, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (47, 'SPV', 'SUPERFICIES VIVAS', 34, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (48, 'SUE', 'SUERO', 37, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (49, 'VCF', 'ENSALADAS VERDE, COCIDAS, DE FRUTAS', 15, 'Varios', NULL, NULL, NULL);
INSERT INTO laboratorio_estatal.paquetes (id, clave, descripcion, categoria_id, matriz, registro_calidad, created_at, updated_at)
VALUES (50, 'VRH', 'VERDURAS / HORTALIZAS', NULL, NULL, NULL, NULL, NULL);
