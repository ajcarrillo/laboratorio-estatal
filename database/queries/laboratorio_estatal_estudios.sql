INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (1, 'AV', 'AV', 'GENERAL', '*% Alcohol Volumen', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (2, 'ACI', 'ACI', 'GENERAL', 'Acidez (ácido láctico)', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (3, 'AS', 'AS', 'GENERAL', 'Alcoholes superiores', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (4, 'ALD', 'ALD', 'GENERAL', 'Aldehídos', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (5, 'AD', 'AD', 'GENERAL', 'Aldrin y dieldrin', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (6, 'AVL', 'AVL', 'GENERAL', 'Ameba de vida libre', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (7, 'BUSPA', 'BUSPA', 'GENERAL', 'Busqueda Patogenos', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (8, 'CLEMB', 'CLEMB', 'GENERAL', 'Clenbuterol', NULL, NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (9, 'CLORD', 'CLORD', 'GENERAL', 'Clordano (total ó isómeros)', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (10, 'CRL', 'CRL', 'GENERAL', 'Cloro residual', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (11, 'CLORUS', 'CLORUS', 'GENERAL', '*Cloruros', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (12, 'CF', 'CF', 'GENERAL', '*Coliformes fecales', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (13, 'CT', 'CT', 'GENERAL', '*Coliformes totales', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (14, 'CLN', 'CLN', 'GENERAL', 'Colinesterasa', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (15, 'COLOR', 'COLOR', 'GENERAL', '*Color', '*TA - 17 - 18', NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (16, 'CONDUCT', 'CONDUCT', 'GENERAL', '*Conductividad', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (17, 'DDT', 'DDT', 'GENERAL', 'DDT (total ó isómeros)', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (18, 'DEN', 'DEN', 'GENERAL', 'Densidad', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (19, 'DC', 'DC', 'GENERAL', 'Derivados clorados', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (20, 'DT', 'DT', 'GENERAL', '*Dureza total', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (21, 'E COLI', 'E COLI', 'GENERAL', '*Escherichia coli', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (22, 'ENTER', 'ENTER', 'GENERAL', '*Enterococo', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (23, 'EC', 'EC', 'GENERAL', 'Esterilidad comercial', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (24, 'EXS', 'EXS', 'GENERAL', 'EXANTEMA SUBITO', NULL, NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (25, 'FLUORS', 'FLUORS', 'GENERAL', '*Fluoruros', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (26, 'FORM', 'FORM', 'GENERAL', 'Formaldehido', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (27, 'FUR', 'FUR', 'GENERAL', 'Furfural', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (28, 'GHCH', 'GHCH', 'GENERAL', 'Gamma-HCH (lindano)', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (29, 'GHA', 'GHA', 'GENERAL', '*Gnathostoma', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (30, 'HEH', 'HEH', 'GENERAL', 'Heptacloro y epóxido de heptacloro', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (31, 'HEX', 'HEX', 'GENERAL', 'Hexaclorobenceno', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (32, 'HN', 'HN', 'GENERAL', '*Hongos', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (33, 'HUM', 'HUM', 'GENERAL', 'Humedad', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (34, 'PLAGUI', 'PLAGUI', 'GENERAL', 'DETERMINACIÓN POR PLAGUICIDAS', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (35, 'IV.CH', 'IV.CH', 'GENERAL', 'Investigación de Vibrio cholerae', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (36, 'LEV', 'LEV', 'GENERAL', '*Levaduras', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (37, 'LM', 'LM', 'GENERAL', 'Listeria monocytogenes', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (38, 'ME', 'ME', 'GENERAL', 'Materia extraña', NULL, NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (39, 'META', 'META', 'GENERAL', 'Metanol', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (40, 'METO', 'METO', 'GENERAL', 'Metoxicloro', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (41, 'NITRA', 'NITRA', 'GENERAL', 'Nitratos', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (42, 'NITRI', 'NITRI', 'GENERAL', '*Nitritos', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (43, 'NIAM', 'NIAM', 'GENERAL', 'Nitrógeno amoniacal', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (44, 'NOESPE', 'NOESPE', 'GENERAL', 'No Especifica', NULL, NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (45, 'OXI', 'OXI', 'GENERAL', 'Oxidantes', NULL, NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (46, 'PH', 'PH', 'GENERAL', '*pH', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (47, 'PS', 'PS', 'GENERAL', 'Plomo en sangre', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (48, 'SCA', 'SCA', 'GENERAL', 'Sales cuaternarias de amonio', NULL, NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (49, 'SSED', 'SSED', 'GENERAL', 'Sólidos sedimentables', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (50, 'SDT', 'SDT', 'GENERAL', 'Sólidos totales', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (51, 'SULFATOS', 'SULFATOS', 'GENERAL', 'Sulfatos', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (52, 'TURBIED', 'TURBIED', 'GENERAL', '*Turbiedad', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (53, 'YODATO', 'YODATO', 'GENERAL', '*Yodatos', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (54, 'YODO', 'YODO', 'GENERAL', 'Yodo', NULL, NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (55, 'MAE', 'MAE', 'GENERAL', '*Mesofilicos aerobios', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (56, 'MAE2', 'MAE2', 'GENERAL', '*Mesofilicos aerobios', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (57, 'MAN', 'MAN', 'GENERAL', '*Mesofilicos anaerobios', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (58, 'SALM', 'SALM', 'GENERAL', '*Salmonella', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (59, 'S.A', 'S.A', 'GENERAL', '*Staphylococcus aureus', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (60, 'TAE', 'TAE', 'GENERAL', '*Termofilicos aerobios', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (61, 'TAN', 'TAN', 'GENERAL', '*Termofilicos anaerobios', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (62, 'VP', 'VP', 'GENERAL', '*Vibrio parahaemolyticus', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (63, 'V. CH', 'V. CH', 'GENERAL', '*Vibrio cholerae', '*TA-01-16', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (64, 'Fito', 'Fito', 'GENERAL', 'Fitoplacton', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (65, 'CTO', 'CTO', 'GENERAL', '*Coliformes totales', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (66, 'FUOR', 'FUOR', 'GENERAL', '*Flúor', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (67, 'GNA', 'GNA', 'GENERAL', '*GNATOSTOMA', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (68, 'LEGIO', 'LEGIO', 'GENERAL', 'Legionella', NULL, NULL, NULL, 1, 2, NULL, '10', '20', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (69, 'GRA', 'GRA', 'GENERAL', '*Grado alcohólico', '*TA - 17 - 18', NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (70, 'HIST', 'HIST', 'GENERAL', '*HISTAMINA', '*TA - 17 - 18', NULL, 'CUANTITATIVO', 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (71, 'BRU', 'BRU', 'GENERAL', 'Brucella', NULL, NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (72, 'INBAC', NULL, 'GENERAL', '*INHIBIDORES BACTERIANOS', '*TA - 17 - 18', NULL, NULL, 1, 2, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (73, 'ETXS', NULL, 'GENERAL', '*ENTEROTOXINA ESTAFILOCOCICA', '*TA - 17 - 18', NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (74, 'LEGION', 'LEGION', 'GENERAL', 'Legionella', NULL, NULL, NULL, 1, 2, NULL, '10', '20', 0, NULL, NULL);
INSERT INTO laboratorio_estatal.estudios (id, clave, clave_alterna, formato, descripcion, cofrepris, referencia, tipo_limite, dias_naturales, laboratorio_id, recipiente_id,
                                          tiempo_alerta, tiempo_proceso, validar_resultado, created_at, updated_at)
VALUES (75, 'CIGU', 'CIGUATOXINA', 'GENERAL', '*CIGUATOXINA', '*TA - 17 - 18', NULL, NULL, 1, 1, NULL, '5', '10', 0, NULL, NULL);
