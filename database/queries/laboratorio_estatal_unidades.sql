INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (1, 'En 25 g', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (2, 'UFC/g', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (3, 'NMP/g', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (4, 'Sin unidades', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (5, 'mg/L', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (6, 'UFC/100 mL', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (7, 'µS/cm', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (8, 'Cel/mL', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (9, 'En 25 ml', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (10, '-----', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (11, 'UFC/mL', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (12, 'NMP/mL', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (13, 'U de pH', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (14, 'NMP/MANOS', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (15, 'U/L', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (16, 'UFC/manos', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (17, 'NMP/100 mL', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (18, 'UFC/cm2', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (19, 'UFC/SUP', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (20, 'NMP/cm2', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (21, 'NMP/SUP', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (22, 'EN HISOPO', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (23, 'EN LITRO', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (24, 'mg/Kg', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (25, 'UTN', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (26, 'Unidades (Pt/Co)', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (27, 'mg/100g', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (28, '% ALCOHOL VOL. A 20°C', NULL, NULL);
INSERT INTO laboratorio_estatal.unidades (id, descripcion, created_at, updated_at)
VALUES (29, 'en 50 g', NULL, NULL);
