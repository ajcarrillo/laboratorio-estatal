TRUNCATE laboratorio_estatal.metodos_referencias;
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (1, 'Western Blot VIH (IgG)', 39, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (2, 'Western Blot VIH (IgA)', 39, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (3, 'PCR tiempo real', 3, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (4, 'Citometria de flujo', 11, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (5, 'Baciloscopia por Ziehl Neelsen diagnóstico ', 37, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (6, 'Cultivo en medio sólido por Petroft', 37, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (7, 'Cultivo en medio líquido por MGIT (320)', 37, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (8, 'Identificación del complejo MTB por AgMPT64(inmunocromatografía)', 37, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (9, 'Gene xpert MTB/RIF', 37, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (10, 'PPD', 37, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (11, 'Histopatología', 23, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (12, 'Baciloscopía', 23, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (13, 'ELISA Ag recombinante', 15, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (14, 'ELISA Ag lisado.', 15, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (15, 'Hemaglutinación Indirecta (HAI).', 15, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (16, 'Rosa de bengala', 1, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (17, 'Aglutinación Estándar (SAT)', 1, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (18, 'Aglutinación en presencia de 2-Mercaptoetanol (2-ME)', 1, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (19, 'Microaglutinación (MAT)', 24, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (20, 'Inmunofluorescencia Directa (IFD)', 27, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (21, 'Rotaforesis', 28, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (22, 'Identificación de Virus de Papiloma Humano (VPH) por PCR en Tiempo Real.', 40, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (23, 'Tinción papanicolaou', 2, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (24, 'Citologìa en Base Lìquida', 2, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (25, 'Parasitoscópico en Gota gruesa y extendido fino.', 14, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (26, '1.-Parasitoscópico en impronta.', 22, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (27, '2.-Parasitoscópico en frotis de lesion.', 22, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (28, '3.-Parasitoscópico en extendido de médula ósea.', 22, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (29, 'Microscopia de gota gruesa  y extendido fino diagnóstico ', 25, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (30, 'Identificación Taxonómica de Chinches Hematófagas y Busqueda Parasitológica de Trypanosoma cruzi.', 34, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (31, 'Identificación taxonómica de larvas de mosquitos.', 32, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (32, 'Identificación taxonómica de mosquitos adultos.', 33, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (33, 'Fluorescencia (IgM)', 17, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (34, 'Fluorescencia (HBs Ag)', 18, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (35, 'Fluorescencia (Anti-HBc total II)', 18, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (36, 'Fluorescencia (HBc IgM)', 18, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (37, 'Fluorescencia (IgG)', 19, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (38, 'ELISA en placa (IgM)', 12, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (39, 'ELISA en placa (IgM)', 41, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (40, 'ELISA en placa (IgM)', 4, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (41, 'ELISA en placa (ELISA IgM)', 29, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (42, 'ELISA en placa (ELISA IgM)', 30, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (43, 'Floculación en placa (VDRL)', 31, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (44, 'ELISA en placa IgM', 31, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (45, 'ELISA en placa IgG', 31, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (46, 'ELISA en placa (IgM e IgG)', 20, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (47, 'ELISA IgG', 5, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (48, 'ELISA en placa (Ag-Ab)', 39, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (49, 'Fluorescencia', 39, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (50, 'ELISA en placa (IgM e IgG)', 26, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (51, 'Fluorescencia VCA IgM', 16, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (52, 'Fluorescencia VCA IgG', 16, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (53, 'Fluorescencia EBNA IgG', 16, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (54, 'Fluorescencia IgM', 36, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (55, 'Fluorescencia IgG', 36, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (56, 'Fluorescencia  IgG', 6, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (57, 'Fluorescencia IgM ', 6, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (58, 'ELISA en placa (IgM)', 38, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (59, 'RT-PCR tiempo real', 21, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (60, 'RT-PCR tiempo real', 29, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (61, 'RT-PCR tiempo real', 30, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (62, 'PCR tiempo real', 35, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (63, 'Triplex RT-PCR tiempo real', 13, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (64, 'Serotipos RT-PCR tiempo real', 13, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (65, 'RT-PCR-tiempo real', 41, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (66, 'Baciloscopia por Ziehl Neelsen', 7, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (67, 'Parasitoscópico en gota gruesa y extendido fino.', 8, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (68, 'Parasitoscópico en impronta, frotis de lesion o extendido de Médula ósea ', 9, NULL, NULL);
INSERT INTO laboratorio_estatal.metodos_referencias (id, descripcion, diagnostico_id, created_at, updated_at)
VALUES (69, 'Parasitoscópico en gota gruesa y extendido fino ', 10, NULL, NULL);
