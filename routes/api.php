<?php

use App\Http\Controllers\API\V1\LocalidadController;
use App\Http\Controllers\API\V1\MenuRoleController;
use App\Http\Controllers\API\V1\MunicipioController;
use App\Http\Controllers\API\V1\PermissionController;
use App\Http\Controllers\API\V1\RoleController;
use App\Http\Controllers\API\V1\SistemaRoleController;
use App\Http\Controllers\Auth\UpdatePasswordController;
use App\Http\Controllers\Auth\UserProfileController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('/v1')
    ->name('api.v1.')
    ->group(function () {
        Route::prefix('/user')
            ->name('user.')
            ->group(function () {
                Route::patch('/profile', [ UserProfileController::class, 'update' ])
                    ->name('profile')
                    ->middleware('auth:api');

                Route::patch('/update-password', [ UpdatePasswordController::class, 'update' ])
                    ->name('update.password')
                    ->middleware('auth:api');
            });

        Route::prefix('/catalogos')
            ->name('catalogos.')
            ->middleware('auth:api')
            ->group(function () {
                Route::get('/localidades', [ LocalidadController::class, 'index' ])->name('localidades');
                Route::get('/municipios', [ MunicipioController::class, 'index' ])->name('municipios');
                Route::get('/permissions', [ PermissionController::class, 'index' ])->name('permissions');
                Route::get('/roles', [ RoleController::class, 'index' ])->name('roles');
                Route::post('/roles', [ RoleController::class, 'store' ])->name('roles');
                Route::patch('/roles/{role}/menus', [ MenuRoleController::class, 'update' ])->name('roles.menus');
            });

        Route::apiResource('menu', 'API\V1\MenuController');

        Route::apiResource('pacientes', 'API\V1\PacienteController')
            ->parameter('pacientes', 'paciente');

        Route::apiResource('sistemas', 'API\V1\SistemaController')
            ->parameter('sistemas', 'sistema');

        Route::post('sistemas/{sistema}/roles', [ SistemaRoleController::class, 'store' ])
            ->name('sistemas.roles');

        Route::apiResource('usuarios', 'API\V1\UsuarioController')
            ->parameter('usuarios', 'usuario');
    });


