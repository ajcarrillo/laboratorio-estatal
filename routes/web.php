<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\ElegirAplicacionController;
use App\Http\Controllers\SpaController;

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/home', function () {
    return redirect('/app');
});

Route::get('file/{id}', 'SpaController@getFile');

Route::get('/elegir-aplicacion', [ ElegirAplicacionController::class, 'index' ])
    ->middleware([ 'auth' ])
    ->name('elegir.aplicacion');

Route::post('/elegir-aplicacion', [ ElegirAplicacionController::class, 'store' ])
    ->middleware([ 'auth' ])
    ->name('elegir.aplicacion');

Auth::routes([
    'register' => false,
    'verify'   => false,
    'reset'    => false,
]);

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/app/{any?}', [ SpaController::class, 'index' ])
    ->middleware([ 'auth', 'check.app' ])
    ->where('any', '.*');

Route::middleware('auth')->post('api/ui/set_application', 'SpaController@setApplication');
Route::middleware('auth')->post('api/ui/unset_application', 'SpaController@unsetApplication');
