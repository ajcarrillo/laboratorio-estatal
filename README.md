# Laboratorio estatal de salud pública

# Contribuir

:+1: :tada: Primero que nada gracias por tomarte el tiempo para contribuir a este proyecto :+1: :tada:

### Antes de hacer tu contribución por favor lee la guía de estilo del código [Guía de estilo del código](https://gitlab.com/ajcarrillo/guia_desarrollo/).
El estilo del código es especialmente importante si estamos en un equipo de desarrollo o si nuestro proyecto lo van a usar en algún momento otros desarrolladores. 
Pero, cuando trabajamos en un proyecto propio, también es una buena costumbre usar un estilo de código claro y optimizado. Nos ayudará a revisar mejor el código y 
a entenderlo si en algún momento tenemos que modificarlo o queremos reutilizarlo.

Para contribuir a este proyecto sigue los siguientes pasos:
 
 ```
 $ git clone https://gitlab.com/ajcarrillo/laboratorio-estatal
 $ git checkout -b devTuNombre
 
 // Despues de hacer tu contribución
 $ git pull origin master
 // Corrige conflictos, si los hay
 $ git push origin devTuNombre
 ```
 
 Crea tu merge request en el sitio de gitlab.
 
 * Clona el proyecto
 * Crea tu rama de trabajo
 * Haz tu contribución
 * Baja los últimos cambios en la rama `master`, arregla conflictos si los hay
 * Sube tu contribución
 * Y haz un merge request a la rama `master` del proyecto.
 
 ## Dependencias
 
 * `PHP` versión 7.2 o superior
 * `MariaDb` versión 10.3 o superior
 * `node` versión 10.11.0 o 12.13.0
 * `yarn` última versión
 
 Para instalar `yarn` ejecuta en la terminal:
 
 ```
 $ npm install --global yarn
 ```

## :baby_bottle: Baby Steps I

* Copiar y renombrar el archivo `/.env.example` a `.env`

### Archivo .env

Aquí se encuentra contenidas algunas variables y configuraciones de vital importancia para que el proyecto funcione.

La variable `APP_KEY` se rellena automáticamente ejecutando el comando:

```
$ php artisan key:generate
```

## :baby_bottle: Baby Steps II

### Configuración y creacion de bases de datos.

Puedes crear las bases de datos ejecutando los siguientes scripts:

Base de datos laboratorio_estatal:
```sql
DROP DATABASE IF EXISTS laboratorio_estatal;
CREATE DATABASE laboratorio_estatal
   CHARACTER SET = 'utf8mb4'
   COLLATE = 'utf8mb4_unicode_ci';
```

Base de datos inventarios:
```sql
DROP DATABASE IF EXISTS inventarios;
CREATE DATABASE inventarios
   CHARACTER SET = 'utf8mb4'
   COLLATE = 'utf8mb4_unicode_ci';
```

Si necesitas crear migraciones para que se ejecuten en la base de datos `inventarios` deberás ejecutar con el siguiente comando:
``
```
$ php artisan make:migration your_migration_name --[create|table]=table_name --path=database/migrations/[inventarios]
```

Ejecuta el siguiente comando para ejecutar las migraciones de cada base de datos, según sea el caso:

```
$ php artisan migrate --database=[inventarios] --path=database/migrations/[inventarios] 
```

Para generar datos de prueba para las distintas bases de datos, segundo sea el caso, ejecuta el siguiente comando:
```
$ php artisan db:seed [--class=TableSeederClass] --database=[inventarios]
```

*Nota: una vez creada la base de datos, configura tu conexión*

Para crear migraciones, ejecutar seeder o correr migraciones de la base de datos de `laboratorio_estatal` sigue el proceso normal de creación de dichos componentes.
