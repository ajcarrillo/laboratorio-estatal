<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/15/19
 * Time: 6:29 p. m.
 */

namespace App\Http\Controllers;


use App\Models\Laboratorio;
use App\Models\Menu;
use App\Models\Sistema;
use App\User;
use Calidad\Models\Archivo;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\Categoria;
use RiesgosSanitarios\Models\Estudio;
use RiesgosSanitarios\Models\MuestraSanitaria;
use RiesgosSanitarios\Models\Origen;
use RiesgosSanitarios\Models\Paquete;
use RiesgosSanitarios\Models\Recipiente;
use RiesgosSanitarios\Models\Subcategoria;
use RiesgosSanitarios\Models\TipoMuestreo;

class SpaController extends Controller
{
    public function index(Request $request)
    {
        $user          = $this->getUserInfo();
        $aplications   = $this->getAplications();
        $aplication    = $this->getAplication($request);
        $menu          = $this->getMenu($request);
        $origenes      = $this->getOrigenes();
        $estudios      = $this->getEstudios();
        $paquetes      = $this->getPaquetes();
        $pantallas     = $this->getPantallas();
        $categorias    = $this->getCategorias();
        $recipientes   = $this->getRecipientes();
        $tipoMuestreo  = $this->getTiposMuestreo();
        $subcategorias = $this->getSubcategorias();
        $laboratorios  = $this->getLaboratorios();


        return view('spa', [
            'user'          => $user,
            'aplications'   => $aplications,
            'aplication'    => $aplication,
            'menu'          => $menu,
            'origenes'      => $origenes,
            'estudios'      => $estudios,
            'paquetes'      => $paquetes,
            'pantallas'     => $pantallas,
            'categorias'    => $categorias,
            'recipientes'   => $recipientes,
            'tipoMuestreo'  => $tipoMuestreo,
            'subcategorias' => $subcategorias,
            'laboratorios'  => $laboratorios,
        ]);
    }

    protected function getLaboratorios()
    {
        return Laboratorio::query()
            ->orderBy('descripcion')
            ->get([ 'id', 'descripcion', 'tipo' ]);
    }

    protected function getTiposMuestreo()
    {
        return TipoMuestreo::query()->pluck('descripcion');
    }

    protected function getCapturistasSanitaria()
    {
        return User::query()
            ->whereHas('roles', function ($query) {
                $query->whereName('CAPTURISTA SANITARIA');
            })
            ->get();
    }

    protected function getOrigenes()
    {
        $origenes = Origen::query()
            ->with('domicilio')
            ->orderBy('origen')
            ->get();

        return $origenes;
    }

    protected function getPantallas(): array
    {
        $pantallas = MuestraSanitaria::getPantallas();

        return $pantallas;
    }

    protected function getUserInfo()
    {
        $authUser = get_user();

        return $user = [
            'titulo'           => $authUser->titulo,
            'name'             => $authUser->name,
            'last_name'        => $authUser->last_name,
            'nombre_apellidos' => $authUser->nombre_apellidos,
            'email'            => $authUser->email,
            'roles'            => $authUser->getRoleNames(),
            'permissions'      => $authUser->getPermissionNames(),
        ];
    }

    protected function getRecipientes()
    {
        return Recipiente::orderBy('descripcion')->get([ 'id', 'descripcion' ]);
    }

    protected function getPaquetes()
    {
        return Paquete::query()
            ->orderBy('clave')
            ->with('categoria', 'estudios', 'estudios.pivot.unidad')
            ->withCount('estudios')
            ->get();
    }

    protected function getEstudios()
    {
        return Estudio::orderBy('descripcion')
            ->get();
    }

    protected function getCategorias()
    {
        return Categoria::orderBy('descripcion')
            ->get();
    }

    protected function getSubcategorias()
    {
        return Subcategoria::orderBy('descripcion')
            ->get();
    }

    protected function getAplications()
    {
        $authUser = get_user();
        $roles    = $authUser->roles->pluck('id');

        $apps = Sistema::query()
            ->leftjoin('role_sistema', 'role_sistema.sistema_id', '=', 'sistemas.id')
            ->whereIn('role_id', $roles)->get();

        return $apps;
    }

    protected function getAplication(Request $request)
    {
        $app_id = $this->getCurrenApplication($request);

        $app = Sistema::query()
            ->with('menu')
            ->find($app_id);

        return $app;
    }

    protected function getMenu(Request $request)
    {
        return Menu::parents()
            ->with([ 'childrenMenu' => function ($query) use ($request) {
                $query->whereHas('roles', function (Builder $query) use ($request) {
                    $query->whereIn('roles.name', $request->user()->getRoleNames());
                });
            } ])
            ->whereHas('roles', function (Builder $query) use ($request) {
                $query->whereIn('roles.name', $request->user()->getRoleNames());
            })
            ->get();
    }

    protected function getCurrenApplication(Request $request): int
    {
        return $request->session()->get('app_id');
    }

    public function getFile(Request $request)
    {
        $archivo = Archivo::find($request->id);

        return response()->file(storage_path($archivo->path));
    }
}
