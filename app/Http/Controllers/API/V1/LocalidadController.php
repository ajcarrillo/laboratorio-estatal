<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/1/19
 * Time: 11:35 a. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Localidad;
use Illuminate\Http\Request;

class LocalidadController extends Controller
{
    public function index(Request $request)
    {
        $cveEnt = $request->query('cve_ent');
        $cveMun = $request->query('cve_mun');

        $items = Localidad::query()
            ->where('CVE_ENT', $cveEnt)
            ->where('CVE_MUN', $cveMun)
            ->get();

        return ok(compact('items'));
    }
}
