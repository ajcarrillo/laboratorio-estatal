<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/13/19
 * Time: 4:46 p. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    public function index()
    {
        $permissions = Permission::query()
            ->orderBy('name')
            ->get();

        return compact('permissions');
    }
}
