<?php

namespace App\Http\Controllers\API\V1;

use App\Events\CreatedUser;
use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use App\User;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsuarioController extends Controller
{
    use ExceptionError;

    public function index()
    {
        $usuarios = User::query()
            ->orderBy('name')
            ->with($this->getRelations())
            ->get();

        return ok(compact('usuarios'));
    }

    private function getRelations()
    {
        return [ 'roles:id,name', 'permissions:id,name' ];
    }

    public function store(Request $request)
    {
        try {
            $user = DB::transaction(function () use ($request) {
                $user = User::create([
                    'titulo'           => $request->input('titulo', NULL),
                    'name'             => $request->input('name'),
                    'last_name'        => $request->input('last_name'),
                    'nombre_apellidos' => "{$request->input('name')} {$request->input('last_name')}",
                    'apellidos_nombre' => "{$request->input('last_name')} {$request->input('name')}",
                    'api_token'        => Str::random(60),
                    'email'            => $request->input('email'),
                    'password'         => Hash::make($request->input('password')),
                ]);

                event(new CreatedUser($user, $request->input('roles'), $request->input('permissions')));

                return $user;
            });

            $user->load($this->getRelations());

            return ok(compact('user'));
        } catch (\Throwable $e) {
            return $this->execption($e);
        }
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $user = User::query()->findOrFail($id);


        try {
            DB::transaction(function () use ($request, $user) {
                $user->update($request->except('password'));

                event(new CreatedUser($user, $request->input('roles'), $request->input('permissions')));
            });
        } catch (\Throwable $e) {
            return $this->execption($e);
        }

        $user->load($this->getRelations());

        return compact('user');
    }

    public function destroy($id)
    {
        $user = User::query()->findOrFail($id);

        $user->syncRoles([]);

        $user->load($this->getRelations());

        return ok(compact('user'));
    }
}
