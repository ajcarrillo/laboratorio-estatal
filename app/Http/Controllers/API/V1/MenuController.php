<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\AccesosMenu;
use App\Traits\ExceptionError;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    use ExceptionError;

    public function index()
    {
        $menus = AccesosMenu::query()
            ->with($this->getRelations())
            ->parents()
            ->get();

        return compact('menus');
    }

    public function store(Request $request)
    {
        $menu = new AccesosMenu($request->input());

        $menu->save();

        $menu->load($this->getRelations())
            ->loadCount('childrenMenu');;

        return compact('menu');
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        $menu = AccesosMenu::query()->find($id);

        $menu->update($request->input());

        $menu->load($this->getRelations())
            ->loadCount('childrenMenu');

        return ok(compact('menu'));
    }

    public function destroy($id)
    {
        $eliminar = AccesosMenu::query()
            ->with('childrenMenu')
            ->find($id);

        try {

            if ( ! $eliminar->childrenMenu()->exists()) {
                $eliminar->delete();
            } else {
                $eliminar->childrenMenu()->delete();
                $eliminar->delete();
            }

            return ok();
        } catch (\Exception $e) {
            return $this->execption($e);
        }
    }

    protected function getRelations()
    {
        return [
            'childrenMenu',
            'childrenMenu.sistema:id,titulo',
            'sistema:id,titulo',
        ];
    }
}
