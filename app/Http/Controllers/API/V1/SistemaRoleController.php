<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/16/19
 * Time: 12:22 p. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Sistema;
use Illuminate\Http\Request;

class SistemaRoleController extends Controller
{
    public function store(Request $request, Sistema $sistema)
    {
        $sistema->roles()->sync($request->input('roles'));

        $roles = $sistema->roles()->pluck('name');

        return ok(compact('roles'));
    }
}
