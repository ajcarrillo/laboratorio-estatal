<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/13/19
 * Time: 4:45 p. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::query()
            ->with($this->getRelations())
            ->withCount('menus')
            ->orderBy('name')
            ->get();

        return ok(compact('roles'));
    }

    public function store(Request $request)
    {
        $role = Role::create($request->input());

        $role->load($this->getRelations())
            ->withCount('menus');

        return ok(compact('role'));
    }

    /**
     * @return array
     */
    private function getRelations(): array
    {
        return [ 'permissions:id,name', 'menus', 'menus.childrenMenu' ];
    }
}
