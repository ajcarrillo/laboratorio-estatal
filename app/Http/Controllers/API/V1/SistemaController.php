<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\Models\Sistema;
use Illuminate\Http\Request;

class SistemaController extends Controller
{
    public function index()
    {
        $sistemas = Sistema::query()
            ->orderBy('titulo')
            ->with('menu', 'roles:roles.id,name')
            ->withCount('menu')
            ->get();

        return ok(compact('sistemas'));
    }

    public function store(Request $request)
    {
        $sistema = new Sistema($request->input());

        $sistema->save();

        $sistema->load('menu');

        return ok(compact('sistema'));
    }

    public function show($id)
    {
        //
    }

    public function update(Request $request, Sistema $sistema)
    {
        $sistema->update($request->input());

        $sistema->load('menu');

        return ok(compact('sistema'));
    }

    public function destroy($id)
    {
        //
    }
}
