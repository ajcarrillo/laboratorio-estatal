<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/1/19
 * Time: 6:30 p. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Domicilio;
use App\Models\Filters\PacienteFilters;
use App\Models\Paciente;
use App\Traits\ExceptionError;
use DB;
use Illuminate\Http\Request;
use Throwable;

class PacienteController extends Controller
{
    use ExceptionError;

    public function index(Request $request, PacienteFilters $filters)
    {
        $values = $request->only([ 'nombreapellidos' ]);

        $items = Paciente::query()
            ->with('domicilio')
            ->filterBy($filters, $values)
            ->get();

        return ok(compact('items'));
    }

    public function store(Request $request)
    {
        try {
            $item = DB::transaction(function () use ($request) {
                $item = new Paciente($request->input());

                $item->save();

                $domicilio = new Domicilio($request->input('domicilio'));

                $item->domicilio()->save($domicilio);

                return $item;
            });

            $item->load('domicilio');

            return created([ 'item' => $item ]);
        } catch (Throwable $e) {
            return $this->throwableEx($e);
        }
    }

    public function update(Request $request, Paciente $paciente)
    {
        try {
            $item = DB::transaction(function () use ($request, $paciente) {
                $paciente->update($request->input());
                $paciente->domicilio()->update($request->input('domicilio'));

                return $paciente;
            });

            $item->load('domicilio');

            return ok([ 'item' => $item ]);
        } catch (Throwable $e) {
            return $this->throwableEx($e);
        }
    }
}
