<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/1/19
 * Time: 11:35 a. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\MunicipioView;
use Illuminate\Http\Request;

class MunicipioController extends Controller
{
    public function index(Request $request)
    {
        $cveEnt = $request->query('cve_ent');

        $items = MunicipioView::query()
            ->orderBy('nom_mun')
            ->where('cve_ent', $cveEnt)
            ->get([ 'cve_ent as CVE_ENT', 'cve_mun as CVE_MUN', 'nom_mun as NOM_MUN' ]);

        return ok(compact('items'));
    }
}
