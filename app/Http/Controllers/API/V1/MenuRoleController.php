<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/19/20
 * Time: 5:14 p. m.
 */

namespace App\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Role;
use Illuminate\Http\Request;

class MenuRoleController extends Controller
{
    public function update(Request $request, Role $role)
    {
        $role->menus()->sync($request->input('menu_ids'));

        return ok();
    }
}
