<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/12/20
 * Time: 5:41 p. m.
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Auth;
use Hash;
use Illuminate\Http\Request;
use Str;

class UpdatePasswordController extends Controller
{
    public function update(Request $request)
    {
        $user = get_user_from_api_guard();

        $this->resetPassword($user, $request->only('new_password'));

        return ok();
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password['new_password']);

        $user->setRememberToken(Str::random(60));

        $user->save();
        //$this->guard()->login($user);
    }

    protected function guard()
    {
        return Auth::guard();
    }
}
