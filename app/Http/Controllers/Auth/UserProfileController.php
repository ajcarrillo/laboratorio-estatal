<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/12/20
 * Time: 5:03 p. m.
 */

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function update(Request $request)
    {
        $user = get_user_from_api_guard();

        $user->update($request->input());

        return ok([
            'titulo'           => $user->titulo,
            'name'             => $user->name,
            'last_name'        => $user->last_name,
            'nombre_apellidos' => $user->nombre_apellidos,
            'email'            => $user->email,
            'roles'            => $user->getRoleNames(),
            'permissions'      => $user->getPermissionNames(),
        ]);
    }
}
