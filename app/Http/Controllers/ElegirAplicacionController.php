<?php

namespace App\Http\Controllers;

use App\Models\Sistema;
use Illuminate\Http\Request;

class ElegirAplicacionController extends Controller
{
    protected $redirectTo = '/app';

    public function index(Request $request)
    {
        $user = $request->user();

        $roles = $user->roles()->pluck('name');

        $sistemas = Sistema::query()
            ->whereHas('roles', function ($query) use ($roles) {
                $query->whereIn('name', $roles);
            })->get();

        return view('elegir_aplicacion', compact('sistemas'));
    }

    public function store(Request $request)
    {
        session([
            'app_id' => $request->input('app_id'),
        ]);
        $this->redirectTo = $request->input('app_route');

        return redirect()->intended($this->redirectTo);
    }
}
