<?php

namespace App\Http\Middleware;

use Closure;

class CheckApp
{
    public function handle($request, Closure $next)
    {
        if ($request->session()->has('app_id')) {
            return $next($request);
        }

        return redirect()
            ->route('elegir.aplicacion')
            ->with('error', 'Debe seleccionar una aplicacion');
    }
}
