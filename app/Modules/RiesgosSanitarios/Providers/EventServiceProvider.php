<?php

namespace RiesgosSanitarios\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use RiesgosSanitarios\Events\MuestraCreada;
use RiesgosSanitarios\Events\ReportarResultados;
use RiesgosSanitarios\Listeners\CrearMuestraConEstatusPendiente;
use RiesgosSanitarios\Listeners\CrearRevisionConEstatusResultados;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        MuestraCreada::class      => [
            CrearMuestraConEstatusPendiente::class,
        ],
        ReportarResultados::class => [
            CrearRevisionConEstatusResultados::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
        //
    }
}
