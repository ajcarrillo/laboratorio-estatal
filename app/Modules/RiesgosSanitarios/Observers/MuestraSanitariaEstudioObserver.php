<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/9/19
 * Time: 11:10 a. m.
 */

namespace RiesgosSanitarios\Observers;


use App\Models\Revision;
use Carbon\Carbon;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

class MuestraSanitariaEstudioObserver
{
    public function created(MuestraSanitariaEstudio $muestraEstudio)
    {
        $revision = new Revision([
            'fecha_apertura'      => Carbon::now(),
            'usuario_apertura_id' => get_user_from_api_guard()->id,
            'estatus'             => 'PENDIENTE',
        ]);

        $muestraEstudio->statuses()->save($revision);
    }
}
