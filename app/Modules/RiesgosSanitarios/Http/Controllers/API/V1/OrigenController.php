<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:04 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Domicilio;
use App\Traits\ExceptionError;
use DB;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\Filters\OrigenFilters;
use RiesgosSanitarios\Models\Origen;
use Throwable;

/**
 * Class OrigenController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class OrigenController extends Controller
{
    use ExceptionError;

    /**
     * @param Request $request
     * @param OrigenFilters $filters
     * @return JsonResponse
     */
    public function index(Request $request, OrigenFilters $filters)
    {
        $values = $request->only([ 'origen' ]);

        $items = Origen::query()
            ->with('user', 'domicilio')
            ->filterBy($filters, $values)
            ->get();

        return ok(compact('items'));
    }

    public function store(Request $request)
    {
        try {
            $item = DB::transaction(function () use ($request) {
                $item = new Origen($request->input());

                $item->save();

                $domicilio = new Domicilio($request->input('domicilio'));

                $item->domicilio()->save($domicilio);

                return $item;
            });

            $item->load('domicilio', 'user');

            return created([ 'item' => $item ]);
        } catch (Throwable $e) {
            return $this->throwableEx($e);
        }
    }

    public function update(Request $request, Origen $origen)
    {
        try {
            DB::transaction(function () use ($request, $origen) {
                $origen->update($request->input());

                $origen->domicilio()->update($request->input('domicilio'));
            });

            $origen->load('domicilio', 'user');

            return ok([ 'item' => $origen ]);
        } catch (Throwable $e) {
            return $this->throwableEx($e);
        }
    }
}
