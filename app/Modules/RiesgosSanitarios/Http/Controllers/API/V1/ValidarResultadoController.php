<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/27/19
 * Time: 1:23 a. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use DB;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

class ValidarResultadoController extends Controller
{
    use ExceptionError;

    public function update(Request $request)
    {
        $resultados = $request->input('estudios');

        try {
            DB::transaction(function () use ($resultados) {

                foreach ($resultados as $id) {
                    $muestra = MuestraSanitariaEstudio::query()->find($id);

                    $muestra->validar();
                }
            });

            return ok();
        } catch (\Throwable $e) {
            return $this->throwableEx($e);
        }
    }
}
