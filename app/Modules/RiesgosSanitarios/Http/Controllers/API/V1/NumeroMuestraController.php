<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/2/19
 * Time: 2:09 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\NumeroMuestra;

/**
 * Class NumeroMuestraController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class NumeroMuestraController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $numeros = NumeroMuestra::query()
            ->orderBy('year', 'DESC')
            ->get();

        return ok(compact('numeros'));
    }

    /**
     * @param Request $request
     * @return array
     */
    public function store(Request $request)
    {
        $numero = NumeroMuestra::query()
            ->create($request->input());

        return compact('numero');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Request $request)
    {
        $year = $request->input('year');

        $numeroMuestra = NumeroMuestra::query()
            ->whereYear('year', '=', $year)
            ->first([ 'numero_muestra' ]);

        $numero = str_pad($numeroMuestra->numero_muestra + 1, 6, '0', STR_PAD_LEFT);

        return ok(compact('numero'));
    }
}
