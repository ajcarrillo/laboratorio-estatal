<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/10/19
 * Time: 7:23 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use RiesgosSanitarios\Models\Solicitud;

/**
 * Class TerminarFolioController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class TerminarFolioController extends Controller
{
    /**
     * @param Solicitud $solicitud
     * @return JsonResponse
     */
    public function update(Solicitud $solicitud)
    {
        $solicitud->update([
            'terminado' => 1,
            'estatus'   => 'TERMINADO',
        ]);

        return ok();
    }
}
