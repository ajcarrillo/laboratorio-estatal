<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2/27/20
 * Time: 7:19 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Analista;

class AnalistasActivosController extends Controller
{
    public function index()
    {
        $analistas = Analista::query()
            ->with('user:id,nombre_apellidos', 'laboratorio:id,descripcion')
            ->activo()
            ->labSanitaria()
            ->get();

        return ok(compact('analistas'));
    }
}
