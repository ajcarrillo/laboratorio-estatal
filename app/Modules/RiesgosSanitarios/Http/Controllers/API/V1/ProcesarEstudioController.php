<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/21/19
 * Time: 1:05 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use DB;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

/**
 * Class ProcesarEstudioController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class ProcesarEstudioController extends Controller
{
    use ExceptionError;

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $data = $request->input('estudios');

        try {
            DB::transaction(function () use ($data) {
                foreach ($data as $estudioId) {
                    $estudio = MuestraSanitariaEstudio::query()->find($estudioId);

                    $estudio->procesar();
                }
            });

            return ok();
        } catch (\Throwable $e) {
            return $this->throwableEx($e);
        }
    }
}
