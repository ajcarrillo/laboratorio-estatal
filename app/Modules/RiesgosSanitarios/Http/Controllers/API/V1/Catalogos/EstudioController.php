<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 11/12/19
 * Time: 11:27 a. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1\Catalogos;


use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\Estudio;

class EstudioController extends Controller
{
    use ExceptionError;

    public function index()
    {
        $items = Estudio::query()
            ->with($this->getRelationArray())
            ->get();

        return ok(compact('items'));
    }

    protected function getRelationArray()
    {
        return [
            'laboratorio',
            'recipiente',
        ];
    }

    public function store(Request $request)
    {
        $estudio = new Estudio($request->input());

        $estudio->save();

        $estudio->load($this->getRelationArray());

        return created(compact('estudio'));
    }

    public function update(Request $request, Estudio $estudio)
    {
        $estudio->update($request->input());

        $estudio->load($this->getRelationArray());

        return ok(compact('estudio'));
    }
}
