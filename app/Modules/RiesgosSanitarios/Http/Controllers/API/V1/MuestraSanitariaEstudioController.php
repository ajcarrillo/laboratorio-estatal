<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/20/19
 * Time: 4:58 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use RiesgosSanitarios\Models\Filters\MuestraSanitariaFilters;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

const C = '';

/**
 * Class MuestraSanitariaEstudioController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class MuestraSanitariaEstudioController extends Controller
{
    /**
     * @param Request $request
     * @param MuestraSanitariaFilters $filters
     * @return array
     */
    public function index(Request $request, MuestraSanitariaFilters $filters)
    {
        $values = $this->getValues($request);

        $user = get_user_from_api_guard();

        $method = Str::camel(Str::lower($user->getRoleNames()->first()));

        $query = $this->$method($user);

        $estudios = $query
            ->filterBy($filters, $values)
            ->get();

        return compact('estudios');
    }

    /**
     * @param Request $request
     * @return array
     */
    protected function getValues(Request $request): array
    {
        return $request->only([
            'muestra',
            'paquete',
            'year',
            'from',
            'to',
            'estudio',
            'jurisdiccion',
            'municipio',
            'estado',
        ]);
    }

    /**
     * @return array
     */
    protected function getRelations(): array
    {
        return [
            'estudio',
            'unidad',
            'muestra.paciente',
            'muestra.paciente.domicilio',
            'muestra.paquete',
            'muestra.solicitud:id,folio,fecha_recepcion,es_convenio',
        ];
    }

    /**
     * @return Builder
     */
    protected function getQuery()
    {
        return MuestraSanitariaEstudio::query()
            ->with($this->getRelations())
            ->whereHas('muestra', function ($query) {
                $query->whereHas('solicitud', function ($query) {
                    $query->where('terminado', 1);
                });
            });
    }

    /**
     * @param User $user
     * @return Builder
     */
    protected function analistaSanitaria(User $user)
    {
        $laboratorio = $user->Analistas()->where('activo', 1)->first();

        return $this->filterByLaboratorio($laboratorio);
    }

    /**
     * @param User $user
     * @return Builder
     */
    protected function responsableLaboratorioSanitaria(User $user)
    {
        $laboratorio = $user->Laboratorios()->where('activo', 1)->first();

        return $this->filterByLaboratorio($laboratorio, 'RESULTADOS')
            ->whereIn('estatus', [ 'RESULTADOS', 'VALIDADO' ]);
    }

    /**
     * @param User $user
     * @return Builder
     */
    protected function jefeDepartamentoSanitaria(User $user)
    {
        return $this->getQuery()->whereIn('estatus', [ 'VALIDADO', 'AUTORIZADO' ]);
    }

    /**
     * @param $laboratorio
     * @param string $estatus
     * @return Builder
     */
    protected function filterByLaboratorio($laboratorio, $estatus = 'PENDIENTE'): Builder
    {
        return $this->getQuery()
            ->orderBy('created_at', 'desc')
            ->whereHas('estudio', function ($query) use ($laboratorio) {
                $query->where('laboratorio_id', $laboratorio->id);
            });
    }

    protected function filterByEstatus($estatus = 'PENDIENTE')
    {
        return $this->getQuery()->where('estatus', $estatus);
    }
}
