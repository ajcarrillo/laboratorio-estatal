<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/8/20
 * Time: 1:13 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\Filters\MuestraSanitariaEstudioFilters;
use RiesgosSanitarios\Models\Filters\MuestraSanitariaFilters;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

class AnalistaHojaDeTrabajoController extends Controller
{
    public function index(Request $request, MuestraSanitariaFilters $filters, MuestraSanitariaEstudioFilters $estudioFilters)
    {
        $valuesMuestraSanitaria = $this->getValuesParaMuestraSanitaria($request);
        $valuesEstudios         = $this->getValuesParaMuestraSanitariaEstudios($request);

        $data = MuestraSanitariaEstudio::query()
            ->with($this->getRelations())
            ->whereHas('muestra', function ($query) use ($filters, $valuesMuestraSanitaria) {
                $query->conSolicitudTerminada()
                    ->delAnalista($this->getAnalistaId())
                    ->filterBy($filters, $valuesMuestraSanitaria);
            })
            ->filterBy($estudioFilters, $valuesEstudios)
            ->get();

        return ok(compact('data'));
    }

    private function getValuesParaMuestraSanitaria(Request $request): array
    {
        return $request->only([
            'muestra',
            'paquete',
            'year',
            'from',
            'to',
            'estudio',
            'jurisdiccion',
            'municipio',
        ]);
    }

    private function getValuesParaMuestraSanitariaEstudios(Request $request): array
    {
        return $request->only([
            'estado',
        ]);
    }

    private function getRelations(): array
    {
        return [
            'estudio',
            'unidad',
            'muestra',
            'muestra.paquete',
            'muestra.solicitud:id,folio,fecha_recepcion,es_convenio',
        ];
    }

    private function getAnalistaId()
    {
        return get_user_from_api_guard()->id;
    }
}
