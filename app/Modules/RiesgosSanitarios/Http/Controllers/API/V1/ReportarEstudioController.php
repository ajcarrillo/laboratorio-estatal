<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/21/19
 * Time: 2:04 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RiesgosSanitarios\Events\ReportarResultados;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

/**
 * Class ReportarEstudioController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class ReportarEstudioController extends Controller
{
    /**
     * @param Request $request
     * @param MuestraSanitariaEstudio $estudio
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, MuestraSanitariaEstudio $estudio)
    {
        $reporte = $request->only([
            'fecha_analisis',
            'fecha_resultados',
            'resultados',
        ]);

        $estudio->update($reporte);

        event(new ReportarResultados($estudio, get_user_from_api_guard()));

        return ok();
    }
}
