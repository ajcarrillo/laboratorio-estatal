<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2/21/20
 * Time: 12:12 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\Filters\MuestraSanitariaFilters;
use RiesgosSanitarios\Models\MuestraSanitaria;

class RespLabMuestraSanitariaController extends Controller
{
    public function index(Request $request, MuestraSanitariaFilters $filters)
    {
        $values = $this->getValues($request);

        $muestras = $this->getQuery()
            ->filterBy($filters, $values)
            ->get();

        return ok(compact('muestras'));
    }

    protected function getQuery(): Builder
    {
        return MuestraSanitaria::query()
            ->with('estudios', 'solicitud', 'recibio:id,nombre_apellidos', 'analista:id,nombre_apellidos')
            ->withCount('estudios')
            ->whereHas('solicitud', function (Builder $query) {
                $query->where('terminado', 1);
            });
    }

    private function getValues(Request $request)
    {
        return $request->only([
            'muestra',
            'paquete',
            'year',
            'from',
            'to',
            'estudio',
            'jurisdiccion',
            'municipio',
            'estado',
        ]);
    }
}
