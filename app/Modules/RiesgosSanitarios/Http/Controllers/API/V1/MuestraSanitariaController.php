<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/2/19
 * Time: 5:30 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Models\Revision;
use App\Traits\ExceptionError;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use RiesgosSanitarios\Events\MuestraCreada;
use RiesgosSanitarios\Models\MuestraSanitaria;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;
use RiesgosSanitarios\Models\NumeroMuestra;
use RiesgosSanitarios\Models\Solicitud;
use Throwable;

/**
 * Class MuestraSanitariaController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class MuestraSanitariaController extends Controller
{
    use ExceptionError;

    /**
     * @param Request $request
     * @param Solicitud $solicitud
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(Request $request, Solicitud $solicitud)
    {
        try {
            $muestra = DB::transaction(function () use ($request, $solicitud) {
                $id            = $request->input('id', NULL);
                $numeroMuestra = $request->input('numero_muestra');

                if (is_null($id)) {
                    $numeroMuestraValido = MuestraSanitaria::query()
                        ->where('numero_muestra', $numeroMuestra)
                        ->where('year', Carbon::now()->year)
                        ->exists() ? str_pad(NumeroMuestra::getNumeroMuestra(), 6, '0', STR_PAD_LEFT) : $numeroMuestra;

                    $muestra                 = new MuestraSanitaria($request->input());
                    $muestra->numero_muestra = $numeroMuestraValido;
                    $muestra->year           = Carbon::now()->year;
                    $muestra->recibio_id     = get_user_from_api_guard()->id;

                    $solicitud->muestras()->save($muestra);

                    event(new MuestraCreada($muestra, get_user_from_api_guard(), $numeroMuestraValido));
                } else {
                    $muestra = MuestraSanitaria::updateOrCreate(
                        [ 'id' => $id ],
                        $request->input()
                    );
                }

                $this->deleteRevisiones($muestra);
                $muestra->estudios()->saveMany($this->getEstudiosData($request->input('estudios')));

                return $muestra;
            });

            $muestra->load($this->getRelations());

            return created(compact('muestra'));
        } catch (Exception $e) {
            return $this->execption($e);
        }
    }

    /**
     * @param MuestraSanitaria $muestra
     */
    protected function deleteRevisiones(MuestraSanitaria $muestra)
    {
        $ids = $muestra->estudios()->pluck('id');

        Revision::query()
            ->where('reviewable_type', 'muestras_sanitarias_estudios')
            ->whereIn('reviewable_id', $ids)
            ->delete();

        $muestra->estudios()->delete();
    }

    /**
     * @param $estudios
     * @return Collection
     */
    protected function getEstudiosData($estudios)
    {
        $collection = collect($estudios);

        $map = $collection->map(function ($item, $key) {
            return new MuestraSanitariaEstudio([
                'estudio_id'        => $item['estudio_id'],
                'unidad_id'         => $item['unidad_id'],
                'norma'             => $item['norma'],
                'limite_permisible' => $item['limite_permisible'],
                'orden_impresion'   => $item['orden_impresion'],
                'estatus'           => 'PENDIENTE',
            ]);
        });

        return $map;
    }

    /**
     * @return array
     */
    protected function getRelations(): array
    {
        return [
            'estudios',
            'estudios.estudio',
            'estudios.unidad',
            'paciente',
            'paciente.domicilio',
            'paquete',
            'paquete.estudios',
            'paquete.estudios.pivot.unidad',
        ];
    }
}
