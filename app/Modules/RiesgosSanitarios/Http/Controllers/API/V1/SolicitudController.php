<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:14 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use RiesgosSanitarios\Models\Filters\SolicitudFilters;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;
use RiesgosSanitarios\Models\Solicitud;

/**
 * Class SolicitudController
 * @package RiesgosSanitarios\Http\Controllers\API\V1
 */
class SolicitudController extends Controller
{
    use ExceptionError;

    /**
     * @param Request $request
     * @param SolicitudFilters $filters
     * @return JsonResponse
     */
    public function index(Request $request, SolicitudFilters $filters)
    {
        $items = $this->getQuery($request, $filters)
            ->orderBy('fecha_recepcion', 'DESC')
            ->paginate(50);

        return ok(compact('items'));
    }

    /**
     * @param Request $request
     * @param SolicitudFilters $filters
     * @return Builder
     */
    protected function getQuery(Request $request, SolicitudFilters $filters): Builder
    {
        $values = $request->only([ 'search', 'from', 'to' ]);

        return Solicitud::query()
            ->with($this->getRelations())
            ->filterBy($filters, $values);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if ($this->checkFolioTerminado()) {
            return not_acceptable([], 'No puedes crear otro folio si tienes un folio sin finalizar.');
        }

        $solicitud = Solicitud::updateOrCreate(
            [ 'folio' => $request->input('folio') ],
            [
                'year'               => $request->input('year'),
                'numero'             => $request->input('numero'),
                'fecha_recepcion'    => $request->input('fecha_recepcion'),
                'origen_id'          => $request->input('origen_id'),
                'pantalla_recepcion' => $request->input('pantalla_recepcion'),
                'terminado'          => $request->input('terminado'),
                'estatus'            => $request->input('estatus'),
                'es_convenio'        => $request->input('es_convenio'),
                'capturista_id'      => get_user_from_api_guard()->id,
            ]
        );

        $solicitud->load($this->getRelations());

        return created(compact('solicitud'));
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $solicitud = Solicitud::query()
            ->with($this->getRelations())
            ->find($id);

        return ok(compact('solicitud'));
    }

    /**
     *
     */
    public function update()
    {

    }

    /**
     *
     */
    public function destroy()
    {

    }

    /**
     * @param Request $request
     * @param $input
     * @return array
     */
    protected function getMuestraData(Request $request, $input): array
    {
        return [
            'es_subrogado'        => $input['es_subrogado'],
            'year'                => Carbon::now()->year,
            'recibio_id'          => Auth::guard('api')->user()->id,
            'entrego'             => $input['entrego'],
            'fecha_muestreo'      => $input['fecha_muestreo'],
            'marca_comercial'     => $input['marca_comercial'],
            'descripcion_muestra' => $input['descripcion_muestra'],
            'recipiente_id'       => $input['recipiente_id'],
            'temperatura'         => $input['temperatura'],
            'peso'                => $input['peso'],
            'volumen'             => $input['volumen'],
            'lote'                => $input['lote'],
            'punto_muestreo'      => $input['punto_muestreo'],
            'cve_ent'             => $input['cve_ent'],
            'cve_mun'             => $input['cve_mun'],
            'cve_loc'             => $input['cve_loc'],
            'nombre_entidad'      => $input['nombre_entidad'],
            'nombre_municipio'    => $input['nombre_municipio'],
            'nombre_localidad'    => $input['nombre_localidad'],
            'numero_acta'         => $input['numero_acta'],
            'tipo_muestreo'       => $input['tipo_muestreo'],
            'caducidad'           => $input['caducidad'],
            'envasado_por'        => $input['envasado_por'],
            'lugar_muestreo'      => $input['lugar_muestreo'],
            'ph'                  => $input['ph'],
            'cloro_residual'      => $input['cloro_residual'],
            'paciente_id'         => $request->input('paciente_id', NULL),
            'subcategoria_id'     => $input['subcategoria_id'],
            'paquete_id'          => $request->input('muestra.paquete.id', $request->input('muestra.paquete_id')),
            'observaciones'       => $input['observaciones'],
            'jurisdiccion'        => $input['jurisdiccion'],
            'tamano_lote'         => $input['tamano_lote'],
            'fecha_recoleccion'   => $input['fecha_recoleccion'],
        ];
    }

    /**
     * @param $input
     * @return Collection
     */
    protected function getEstudiosData($input)
    {
        $collection = collect($input['estudios']);

        $map = $collection->map(function ($item, $key) {
            return new MuestraSanitariaEstudio([
                'estudio_id'        => $item['pivot']['estudio_id'],
                'unidad_id'         => $item['pivot']['unidad_id'],
                'norma'             => $item['pivot']['norma'],
                'limite_permisible' => $item['pivot']['limite_permisible'],
                'orden_impresion'   => $item['pivot']['orden_impresion'],
            ]);
        });

        return $map;
    }

    /**
     * @return array
     */
    protected function getRelations(): array
    {
        return [
            'origen',
            'origen.domicilio',
            'muestras' => function ($query) {
                $query->orderBy('created_at', 'desc');
            },
            'muestras.estudios',
            'muestras.estudios.estudio',
            'muestras.estudios.unidad',
            'muestras.paciente',
            'muestras.paciente.domicilio',
            'muestras.paquete',
            'muestras.paquete.estudios',
            'muestras.paquete.estudios.pivot.unidad',
        ];
    }

    protected function checkFolioTerminado()
    {
        return Solicitud::query()
            ->whereHas('capturista', function (Builder $query) {
                $query->where('id', get_user_from_api_guard()->id);
            })
            ->where('terminado', 0)
            ->exists();
    }
}
