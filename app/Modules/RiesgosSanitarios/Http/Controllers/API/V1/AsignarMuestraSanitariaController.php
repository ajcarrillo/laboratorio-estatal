<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2/27/20
 * Time: 11:35 p. m.
 */

namespace RiesgosSanitarios\Http\Controllers\API\V1;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RiesgosSanitarios\Models\MuestraSanitaria;

class AsignarMuestraSanitariaController extends Controller
{
    public function update(Request $request)
    {
        $values     = $request->input('numeros_muestra');
        $analistaId = $request->input('analista_id');

        MuestraSanitaria::query()
            ->whereIn('numero_muestra', $values)
            ->update(
                [
                    'estatus'     => 'ASIGNADA',
                    'analista_id' => $analistaId,
                ]
            );

        return ok();
    }
}
