<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/5/19
 * Time: 5:17 p. m.
 */

namespace RiesgosSanitarios\Listeners;


use App\Models\Revision;
use Carbon\Carbon;
use RiesgosSanitarios\Events\MuestraCreada;
use RiesgosSanitarios\Models\NumeroMuestra;

class CrearMuestraConEstatusPendiente
{
    const ESTATUS = 'PENDIENTE';

    public function handle(MuestraCreada $event)
    {
        // Crear revision con estatus PENDIENTE
        $muestra             = $event->muestra;
        $user                = $event->user;
        $numeroMuestraValido = $event->numeroMuestraValido;

        $revision = new Revision([
            'fecha_apertura'      => Carbon::now(),
            'usuario_apertura_id' => $user->id,
            'estatus'             => self::ESTATUS,
        ]);

        $muestra->statuses()->save($revision);
        $muestra->update([ 'estatus' => self::ESTATUS ]);

        NumeroMuestra::query()
            ->where('year', Carbon::now()->year)
            ->update([ 'numero_muestra' => $numeroMuestraValido ]);
    }
}
