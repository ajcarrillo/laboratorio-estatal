<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/23/19
 * Time: 12:23 a. m.
 */

namespace RiesgosSanitarios\Listeners;


use App\Models\Revision;
use Carbon\Carbon;
use RiesgosSanitarios\Events\ReportarResultados;

class CrearRevisionConEstatusResultados
{
    public function handle(ReportarResultados $event)
    {
        $user    = $event->user;
        $estudio = $event->estudio;

        if ( ! $estudio->statuses()->where('estatus', 'RESULTADOS')->exists()) {
            $revision = new Revision($this->getRevisionData($user));

            $estudio->statuses()->save($revision);
        } else {
            $revision = $estudio->statuses()->where('estatus', 'RESULTADOS')->first();

            $revision->update($this->getRevisionData($user));
        }

        $estudio->update([ 'estatus' => 'RESULTADOS' ]);
    }

    /**
     * @param $user
     * @return array
     */
    protected function getRevisionData($user): array
    {
        return [
            'fecha_apertura'      => Carbon::now(),
            'usuario_apertura_id' => $user->id,
            'estatus'             => 'RESULTADOS',
        ];
    }
}
