<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/23/19
 * Time: 12:20 a. m.
 */

namespace RiesgosSanitarios\Events;

use Illuminate\Queue\SerializesModels;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;

class ReportarResultados
{
    use SerializesModels;

    /**
     * @var MuestraSanitariaEstudio
     */
    public $estudio;
    public $user;

    public function __construct(MuestraSanitariaEstudio $estudio, $user)
    {

        $this->estudio = $estudio;
        $this->user    = $user;
    }
}
