<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/5/19
 * Time: 5:16 p. m.
 */

namespace RiesgosSanitarios\Events;


use Illuminate\Queue\SerializesModels;
use RiesgosSanitarios\Models\MuestraSanitaria;

class MuestraCreada
{
    use SerializesModels;

    public $muestra;
    public $user;
    public $numeroMuestraValido;

    public function __construct(MuestraSanitaria $muestra, $user, $numeroMuestraValido)
    {
        $this->muestra             = $muestra;
        $this->user                = $user;
        $this->numeroMuestraValido = $numeroMuestraValido;
    }
}
