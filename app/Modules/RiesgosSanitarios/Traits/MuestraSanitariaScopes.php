<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/8/20
 * Time: 1:22 p. m.
 */

namespace RiesgosSanitarios\Traits;


trait MuestraSanitariaScopes
{
    public function scopeConSolicitudTerminada($query)
    {
        return $query->whereHas('solicitud', function ($query) {
            $query->where('terminado', 1);
        });
    }

    public function scopeDelAnalista($query, $analistaId)
    {
        return $query->where('analista_id', $analistaId);
    }
}
