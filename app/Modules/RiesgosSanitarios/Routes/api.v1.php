<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:18 p. m.
 */

// path: api/v1
// name: api.v1.


/*Route::resource('solicitudes', 'API\V1\SolicitudController')
    ->parameter('solicitudes', 'solicitud');*/

use RiesgosSanitarios\Http\Controllers\API\V1\AnalistaHojaDeTrabajoController;
use RiesgosSanitarios\Http\Controllers\API\V1\AnalistasActivosController;
use RiesgosSanitarios\Http\Controllers\API\V1\AsignarMuestraSanitariaController;
use RiesgosSanitarios\Http\Controllers\API\V1\AutorizarResultadoController;
use RiesgosSanitarios\Http\Controllers\API\V1\MuestraSanitariaEstudioController;
use RiesgosSanitarios\Http\Controllers\API\V1\NumeroMuestraController;
use RiesgosSanitarios\Http\Controllers\API\V1\ProcesarEstudioController;
use RiesgosSanitarios\Http\Controllers\API\V1\ReportarEstudioController;
use RiesgosSanitarios\Http\Controllers\API\V1\TerminarFolioController;
use RiesgosSanitarios\Http\Controllers\API\V1\ValidarResultadoController;

Route::post('asignar-muestras', [ AsignarMuestraSanitariaController::class, 'update' ])->name('asignar.muestras');
Route::get('analistas-activos-sanitaria', [ AnalistasActivosController::class, 'index' ])->name('analistas.activos');

Route::prefix('/analistas')
    ->name('analistas.')
    ->group(function () {
        Route::prefix('/muestras-sanitarias')
            ->name('muestras.sanitarias.')
            ->group(function () {
                Route::get('/', [ AnalistaHojaDeTrabajoController::class, 'index' ])->name('index');
                Route::patch('/procesar', [ ProcesarEstudioController::class, 'update' ])->name('procesar');
            });
    });

Route::prefix('/catalogos')
    ->name('catalogos.')
    ->group(function () {
        Route::apiResource('estudios', 'API\V1\Catalogos\EstudioController')
            ->parameter('estudios', 'estudio')
            ->only([ 'index', 'store', 'update' ]);
    });

Route::apiResource('muestras-sanitarias', 'API\V1\RespLabMuestraSanitariaController')
    ->only([ 'index' ]);

Route::prefix('muestra-sanitaria-estudios')
    ->name('muestras.sanitarias.estudios.')
    ->group(function () {
        Route::get('/', [ MuestraSanitariaEstudioController::class, 'index' ])->name('index');
        Route::patch('/autorizar', [ AutorizarResultadoController::class, 'update' ])->name('autorizar');
        Route::patch('/reportar/{estudio}', [ ReportarEstudioController::class, 'update' ])->name('reportar');
        Route::patch('/validar', [ ValidarResultadoController::class, 'update' ])->name('validar');
    });

Route::prefix('numero-muestra')
    ->name('numero.muestra.')
    ->group(function () {
        Route::get('/', [ NumeroMuestraController::class, 'index' ])
            ->name('index');
        Route::post('/', [ NumeroMuestraController::class, 'store' ])
            ->name('store');
        Route::get('/show', [ NumeroMuestraController::class, 'show' ])
            ->name('show');
    });

Route::apiResource('origenes', 'API\V1\OrigenController')
    ->parameter('origenes', 'origen');

Route::apiResource('solicitudes', 'API\V1\SolicitudController')
    ->parameter('solicitudes', 'solicitud');

Route::apiResource('solicitudes.muestras-sanitarias', 'API\V1\MuestraSanitariaController')
    ->parameter('muestras-sanitarias', 'id')
    ->parameter('solicitudes', 'solicitud');

Route::post('terminar-folio/{solicitud}', [ TerminarFolioController::class, 'update' ])->name('terminar.folio');
