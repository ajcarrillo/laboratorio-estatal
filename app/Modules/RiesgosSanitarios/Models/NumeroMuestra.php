<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/2/19
 * Time: 2:11 p. m.
 */

namespace RiesgosSanitarios\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class NumeroMuestra extends Model
{
    protected $table    = 'numeros_muestras';
    protected $fillable = [
        'year',
        'numero_muestra',
    ];

    protected static function getUltimoNumeroMuestra($year)
    {
        return self::query()
            ->where('year', $year)
            ->value('numero_muestra');
    }

    public static function getNumeroMuestra($year = NULL)
    {
        if ( ! $year) {
            $year = Carbon::now()->year;
        }

        return self::getUltimoNumeroMuestra($year) + 1;
    }
}
