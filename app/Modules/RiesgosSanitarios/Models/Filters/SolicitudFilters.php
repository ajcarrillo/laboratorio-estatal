<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:45 p. m.
 */

namespace RiesgosSanitarios\Models\Filters;


use App\QueryFilter;

class SolicitudFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'search' => 'filled',
            'from'   => 'filled|date_format:Y-m-d',
            'to'     => 'filled|date_format:Y-m-d',
        ];
    }

    public function search($query, $search)
    {
        $query->where('folio', 'like', "%{$search}%")
            ->orWhereHas('origen', function ($query) use ($search) {
                $query->where('origen', 'like', "%{$search}%");
            });
    }

    public function from($query, $date)
    {
        $query->whereDate('fecha_recepcion', '>=', $date);
    }

    public function to($query, $date)
    {
        $query->whereDate('fecha_recepcion', '<=', $date);
    }
}
