<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:02 p. m.
 */

namespace RiesgosSanitarios\Models\Filters;


use App\QueryFilter;

class OrigenFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'origen' => 'filled',
        ];
    }
}
