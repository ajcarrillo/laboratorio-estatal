<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/20/19
 * Time: 5:29 p. m.
 */

namespace RiesgosSanitarios\Models\Filters;


use App\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class MuestraSanitariaEstudioFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'num_muestra'  => 'filled',
            'paquete'      => 'filled',
            'year'         => 'filled',
            'from'         => 'filled',
            'to'           => 'filled',
            'estudio'      => 'filled',
            'jurisdiccion' => 'filled',
            'municipio'    => 'filled',
            'estado'       => 'filled',
        ];
    }

    public function estado(Builder $query, $estado)
    {
        $query->where('estatus', $estado);
    }

    public function num_muestra(Builder $query, $num_muestra)
    {
        $query->whereHas('muestra', function (Builder $query) use ($num_muestra) {
            $query->where('numero_muestra', $num_muestra);
        });
    }

    public function paquete($query, $paquete)
    {
        $query->whereHas('muestra', function (Builder $query) use ($paquete) {
            $query->where('paquete_id', $paquete);
        });
    }

    public function year($query, $year)
    {
        $query->whereHas('muestra', function (Builder $query) use ($year) {
            $query->where('year', $year);
        });
    }

    public function from($query, $from)
    {
        $query->whereHas('muestra', function (Builder $query) use ($from) {
            $query->where('numero_muestra', '>=', $from);
        });
    }

    public function to($query, $to)
    {
        $query->whereHas('muestra', function (Builder $query) use ($to) {
            $query->where('numero_muestra', '>=', $to);
        });
    }

    public function estudio($query, $estudio)
    {
        $query->where('estudio_id', $estudio);
    }

    public function jurisdiccion($query, $jurisdiccion)
    {
        $query->whereHas('muestra', function (Builder $query) use ($jurisdiccion) {
            $query->where('jurisdiccion', $jurisdiccion);
        });
    }

    public function municipio($query, $municipio)
    {
        $query->whereHas('muestra', function (Builder $query) use ($municipio) {
            $query->where('cve_mun', $municipio);
        });
    }
}
