<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/17/19
 * Time: 12:27 p. m.
 */

namespace RiesgosSanitarios\Models\Filters;


use App\QueryFilter;

class MuestraSanitariaFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'muestra'      => 'filled',
            'paquete'      => 'filled',
            'year'         => 'filled',
            'from'         => 'filled',
            'to'           => 'filled',
            'estudio'      => 'filled',
            'jurisdiccion' => 'filled',
            'municipio'    => 'filled',
            'estado'       => 'filled',
        ];
    }

    public function estado($query, $estado)
    {
        $query->where('estatus', $estado);
    }

    public function muestra($query, $muestra)
    {
        $query->where('numero_muestra', $muestra);
    }

    public function paquete($query, $paquete)
    {
        $query->where('paquete_id', $paquete);
    }

    public function year($query, $year)
    {
        $query->where('year', $year);
    }

    public function from($query, $from)
    {
        $query->where('numero_muestra', '>=', $from);
    }

    public function to($query, $to)
    {
        $query->where('numero_muestra', '<=', $to);
    }

    public function estudio($query, $estudio)
    {
        $query->with([ 'estudios' => function ($query) use ($estudio) {
            $query->where('estudio_id', $estudio);
        } ]);
    }

    public function jurisdiccion($query, $jurisdiccion)
    {
        $query->where('jurisdiccion', $jurisdiccion);
    }

    public function municipio($query, $municipio)
    {
        $query->where('cve_mun', $municipio);
    }
}
