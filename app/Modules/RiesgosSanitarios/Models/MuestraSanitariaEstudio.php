<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:08 p. m.
 */

namespace RiesgosSanitarios\Models;


use App\Models\Revision;
use App\Traits\FilterBy;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class MuestraSanitariaEstudio extends Model
{
    use FilterBy;

    protected $table        = 'muestra_sanitaria_estudios';
    protected $guarded      = [];
    public    $incrementing = true;
    protected $appends      = [
        'status',
    ];


    public function getStatusAttribute()
    {
        $estatus = $this->statuses()
            ->with('usuarioApertura:id,nombre_apellidos')
            ->orderBy('created_at', 'DESC')
            ->first();

        return $estatus;
    }

    public function statuses()
    {
        return $this->morphMany(Revision::class, 'reviewable');
    }

    public function muestra()
    {
        return $this->belongsTo(MuestraSanitaria::class, 'muestra_sanitaria_id');
    }

    public function estudio()
    {
        return $this->belongsTo(Estudio::class, 'estudio_id');
    }

    public function unidad()
    {
        return $this->belongsTo(Unidad::class, 'unidad_id')
            ->withDefault([ 'descripcion' => 'SIN UNIDAD ASIGNADA' ]);
    }

    public function procesar()
    {
        $this->storeRevision('EN PROCESO');
    }

    public function validar()
    {
        $this->storeRevision('VALIDADO');
    }

    public function reportar()
    {
        $this->storeRevision('REPORTADO');
    }

    public function autorizar()
    {
        $this->storeRevision('AUTORIZADO');
    }

    protected function storeRevision($estatus): void
    {
        $revision = new Revision([
            'fecha_apertura'      => Carbon::now(),
            'usuario_apertura_id' => get_user_from_api_guard()->id,
            'estatus'             => $estatus,
        ]);

        $this->statuses()->save($revision);

        $this->update([ 'estatus' => $estatus ]);
    }
}
