<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:13 p. m.
 */

namespace RiesgosSanitarios\Models;


use AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;
use Illuminate\Database\Eloquent\Model;

class Paquete extends Model
{
    use EagerLoadPivotTrait;

    protected $table    = 'paquetes';
    protected $fillable = [
        'clave', 'descripcion', 'categoria_id', 'matriz', 'registro_calidad',
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id')->withDefault([ 'descripcion' => 'SIN CATEGORÍA' ]);
    }

    public function estudios()
    {
        return $this->belongsToMany(
            Estudio::class, 'paquete_estudio', 'paquete_id', 'estudio_id'
        )
            ->using(PaqueteEstudio::class)
            ->withPivot([
                'unidad_id',
                'norma',
                'limite_permisible',
                'orden_impresion',
            ]);
    }
}
