<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/2/19
 * Time: 12:18 p. m.
 */

namespace RiesgosSanitarios\Models;


use Illuminate\Database\Eloquent\Model;

class TipoMuestreo extends Model
{
    public    $incrementing = false;
    protected $table        = 'tipos_muestreo';
    protected $fillable     = [
        'descripcion',
    ];
    protected $primaryKey   = 'descripcion';
}
