<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/3/19
 * Time: 1:42 p. m.
 */

namespace RiesgosSanitarios\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class PaqueteEstudio extends Pivot
{
    public    $incrementing = true;
    protected $table        = 'paquete_estudio';

    public function paquete()
    {
        return $this->belongsTo(Paquete::class, 'paquete_id');
    }

    public function estudio()
    {
        return $this->belongsTo(Estudio::class, 'estudio_id');
    }

    public function unidad()
    {
        return $this->belongsTo(Unidad::class, 'unidad_id')
            ->withDefault([ 'descripcion' => 'SIN UNIDAD ASIGNADA' ]);
    }
}
