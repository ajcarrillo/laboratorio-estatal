<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:13 p. m.
 */

namespace RiesgosSanitarios\Models;


use AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;
use App\Models\Laboratorio;
use Illuminate\Database\Eloquent\Model;

class Estudio extends Model
{
    use EagerLoadPivotTrait;

    protected $table    = 'estudios';
    protected $fillable = [
        'clave', 'clave_alterna', 'formato', 'descripcion',
        'cofrepris', 'referencia', 'tipo_limite', 'dias_naturales',
        'laboratorio_id', 'recipiente_id', 'tiempo_alerta', 'tiempo_proceso',
        'validar_resultado',
    ];

    public function muestras()
    {
        return $this->belongsToMany(MuestraSanitaria::class, 'muestra_sanitaria_estudios', 'estudio_id', 'muestra_sanitaria_id')
            ->using(MuestraSanitariaEstudio::class)
            ->withPivot([
                'unidad_id',
                'norma',
                'limite_permisible',
                'orden_impresion',
                'fecha_analisis',
                'fecha_resultados',
                'resultados',
            ]);
    }

    public function paquetes()
    {
        return $this->belongsToMany(Paquete::class, 'paquete_estudio', 'estudio_id', 'paquete_id')
            ->using(PaqueteEstudio::class)
            ->withPivot([
                'unidad_id',
                'norma',
                'limite_permisible',
                'orden_impresion',
            ]);
    }

    public function laboratorio()
    {
        return $this->belongsTo(Laboratorio::class, 'laboratorio_id');
    }

    public function recipiente()
    {
        return $this->belongsTo(Recipiente::class, 'recipiente_id')->withDefault();
    }

    public function setClaveAttribute($value)
    {
        $this->upperCaseAttribute('clave', $value);
    }

    public function setClaveAlternaAttribute($value)
    {
        $this->upperCaseAttribute('clave_alterna', $value);
    }

    protected function upperCaseAttribute($field, $value)
    {
        $this->attributes[ $field ] = mb_strtoupper($value, 'UTF-8');
    }
}
