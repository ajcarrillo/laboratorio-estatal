<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 1:59 p. m.
 */

namespace RiesgosSanitarios\Models;


use App\Models\Domicilio;
use App\Traits\FilterBy;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Origen extends Model
{
    use FilterBy;

    protected $table    = 'origenes';
    protected $fillable = [
        'tipo',
        'clave',
        'iniciales',
        'origen',
        'razon_social',
        'telefono',
        'director_nombre',
        'director_direccion',
        'director_telefono',
        'director_email',
        'director_cargo',
        'director_preposicion',
        'director_estado',
        'director_municipio',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault([
            'name' => 'Anonymous user',
        ]);
    }

    public function solicitudes()
    {
        return $this->hasMany(Solicitud::class, 'origen_id');
    }

    public function domicilio()
    {
        return $this->morphOne(Domicilio::class, 'addressable');
    }
}
