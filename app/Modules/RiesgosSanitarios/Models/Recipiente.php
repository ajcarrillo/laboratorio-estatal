<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:09 p. m.
 */

namespace RiesgosSanitarios\Models;


use Illuminate\Database\Eloquent\Model;

class Recipiente extends Model
{
    protected $table = 'recipientes';
}
