<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/3/19
 * Time: 1:46 p. m.
 */

namespace RiesgosSanitarios\Models;


use Illuminate\Database\Eloquent\Model;

class Unidad extends Model
{
    protected $table = 'unidades';
}
