<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:10 p. m.
 */

namespace RiesgosSanitarios\Models;


use AjCastro\EagerLoadPivotRelations\EagerLoadPivotTrait;
use App\Models\EntidadView;
use App\Models\Localidad;
use App\Models\MunicipioView;
use App\Models\Paciente;
use App\Models\Revision;
use App\Traits\FilterBy;
use App\User;
use Illuminate\Database\Eloquent\Model;
use RiesgosSanitarios\Traits\MuestraSanitariaScopes;

class MuestraSanitaria extends Model
{
    use EagerLoadPivotTrait, FilterBy, MuestraSanitariaScopes;

    protected        $table     = 'muestras_sanitarias';
    protected        $fillable  = [
        'es_subrogado',
        'solicitud_analisis_id',
        'year',
        'numero_muestra',
        'recibio_id',
        'entrego',
        'fecha_muestreo',
        'marca_comercial',
        'descripcion_muestra',
        'recipiente_id',
        'temperatura',
        'peso',
        'volumen',
        'lote',
        'punto_muestreo',
        'cve_ent',
        'cve_mun',
        'cve_loc',
        'nombre_entidad',
        'nombre_municipio',
        'nombre_localidad',
        'numero_acta',
        'tipo_muestreo',
        'caducidad',
        'envasado_por',
        'lugar_muestreo',
        'ph',
        'cloro_residual',
        'paciente_id',
        'subcategoria_id',
        'paquete_id',
        'observaciones',
        'jurisdiccion',
        'tamano_lote',
        'fecha_recoleccion',
        'estatus',
    ];
    protected static $pantallas = [ 'ALIM, BEB, JUG', 'CEP, AGUAS AMB Y PUCH', 'MTRS. BIOL. HUMANAS' ];
    protected static $status    = [
        'PENDIENTE',
        'REPORTADA',
        'SUPERVISADA',
        'AUTORIZADA',
    ];
    protected        $casts     = [
        'tipo_muestreo' => 'array',
    ];
    protected        $appends   = [
        'status',
    ];

    public function recibio()
    {
        return $this->belongsTo(User::class, 'recibio_id');
    }

    public function analista()
    {
        return $this->belongsTo(User::class, 'analista_id')
            ->withDefault([ 'nombre_apellidos' => 'NO ASIGNADA' ]);
    }

    public function paquete()
    {
        return $this->belongsTo(Paquete::class, 'paquete_id')->withDefault();
    }

    public function getStatusAttribute()
    {
        $estatus = $this->statuses()
            ->orderBy('created_at')
            ->first();

        return $estatus->estatus;
    }

    public function statuses()
    {
        return $this->morphMany(Revision::class, 'reviewable');
    }

    public function getEntidadAttribute()
    {
        return EntidadView::query()
            ->where('cve_ent', $this->cve_ent)
            ->first();
    }

    public function getMunicipioAttribute()
    {
        return MunicipioView::query()
            ->where('cve_ent', $this->cve_ent)
            ->where('cve_mun', $this->cve_mun)
            ->first();
    }

    public function getLocalidadAttribute()
    {
        return Localidad::query()
            ->where('CVE_ENT', $this->cve_ent)
            ->where('CVE_MUN', $this->cve_mun)
            ->where('CVE_LOC', $this->cve_loc)
            ->first();
    }

    public function paciente()
    {
        return $this->belongsTo(Paciente::class, 'paciente_id')->withDefault();
    }

    public function estudios()
    {
        return $this->hasMany(MuestraSanitariaEstudio::class, 'muestra_sanitaria_id');
    }

    public function solicitud()
    {
        return $this->belongsTo(Solicitud::class, 'solicitud_analisis_id');
    }

    public static function getPantallas(): array
    {
        return self::$pantallas;
    }

}
