<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/2/19
 * Time: 1:49 p. m.
 */

namespace RiesgosSanitarios\Models;


use Illuminate\Database\Eloquent\Model;

class DescripcionMuestras extends Model
{
    protected $connection = 'sqlite';
    protected $guarded    = [];
    protected $table      = 'descripcion_muestras';
}
