<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:09 p. m.
 */

namespace RiesgosSanitarios\Models;


use App\Traits\FilterBy;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    use FilterBy;

    protected        $table     = 'solicitud_analisis';
    protected        $fillable  = [
        'year',
        'numero',
        'folio',
        'fecha_recepcion',
        'origen_id',
        'pantalla_recepcion',
        'terminado',
        'estatus',
        'es_convenio',
        'capturista_id',
    ];
    protected static $pantallas = [ 'ALIM, BEB, JUG', 'CEP, AGUAS AMB Y PUCH', 'MTRS. BIOL. HUMANAS' ];
    protected        $casts     = [
        'terminado' => 'boolean',
    ];

    public function capturista()
    {
        return $this->belongsTo(User::class, 'capturista_id');
    }

    public static function getPantallas(): array
    {
        return self::$pantallas;
    }

    public static function setPantallas(array $pantallas): void
    {
        self::$pantallas = $pantallas;
    }

    public function origen()
    {
        return $this->belongsTo(Origen::class, 'origen_id');
    }

    public function muestras()
    {
        return $this->hasMany(MuestraSanitaria::class, 'solicitud_analisis_id');
    }

}
