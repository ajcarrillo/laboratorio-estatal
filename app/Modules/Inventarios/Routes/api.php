<?php

Route::apiResource('inventarios/entradas', 'Almacen\EntradaController');
Route::apiResource('inventarios/areas', 'Almacen\AreaController');
Route::apiResource('inventarios/cat_articulos', 'Almacen\CatArticuloController');
Route::apiResource('inventarios/marcas', 'Almacen\MarcaController');
Route::apiResource('inventarios/proveedores', 'Almacen\ProveedorController');
Route::apiResource('inventarios/tipos_insumo', 'Almacen\TiposInsumoController');
Route::apiResource('inventarios/existencias', 'Almacen\ArticuloController');
Route::apiResource('inventarios/transferencias', 'Transferencias\TransferenciaController');
Route::apiResource('inventarios/solicitudes', 'Almacen\SolicitudController');
Route::apiResource('inventarios/abastecimientos', 'Transferencias\TransferenciaSolicitudesController');


Route::post('inventarios/areas/clues', 'Almacen\AreaController@getCluesEstatales');
Route::post('inventarios/areas/areas', 'Almacen\AreaController@getAreas');

Route::post('inventarios/cat_articulos/marcas', 'Almacen\CatArticuloController@getMarcas');
Route::post('inventarios/cat_articulos/tipos_insumo', 'Almacen\CatArticuloController@getTiposInsumo');

Route::post('inventarios/entradas/tipos_entrada', 'Almacen\EntradaController@getTiposEntrada');
Route::post('inventarios/entradas/tipos_insumo', 'Almacen\EntradaController@getTiposInsumo');
Route::post('inventarios/entradas/proveedores', 'Almacen\EntradaController@getProveedores');
//Route::post('inventarios/entradas/areas', 'Almacen\EntradaController@getAreas');
Route::post('inventarios/entradas/cat_articulos', 'Almacen\EntradaController@getCatArticulos');
Route::post('inventarios/entradas/guardar_articulo', 'Almacen\EntradaController@guardarArticulo');
Route::post('inventarios/entradas/cambiar_estado/{id}', 'Almacen\EntradaController@cambiarEstado');
Route::post('inventarios/entradas/pdf/{id}', 'Almacen\EntradaController@pdf ');
Route::post('inventarios/entradas/get_ordenes_compra', 'Almacen\EntradaController@getOrdenesCompra');
Route::get('inventarios/entradas/tipo/{id}', 'Almacen\EntradaController@getByTipo');

Route::post('inventarios/entradas/remision', 'Almacen\EntradaController@storeRemision');
Route::put('inventarios/entradas/remision/{id}', 'Almacen\EntradaController@updateRemision');
Route::post('inventarios/entradas/remision/articulos', 'Almacen\EntradaController@getArticulos');

Route::post('inventarios/transferencias/tipos_transferencia', 'Transferencias\TransferenciaController@getTiposTransferencia');
Route::post('inventarios/transferencias/articulos', 'Transferencias\TransferenciaController@getArticulos');
Route::post('inventarios/transferencias/cambiar_estado/{id}', 'Transferencias\TransferenciaController@cambiarEstado');
Route::get('inventarios/transferencias/tipo/{id}', 'Transferencias\TransferenciaController@getByTipo');
Route::post('inventarios/transferencias/get_solicitudes', 'Transferencias\TransferenciaController@getSolicitudes');
Route::get('inventarios/transferencias/reporte/{id}', 'Transferencias\TransferenciaController@reporte');

Route::get('inventarios/solicitudes/tipo/{id}', 'Almacen\SolicitudController@getByTipo');
Route::post('inventarios/solicitudes/tipos_solicitud', 'Almacen\SolicitudController@getTiposSolicitud');
Route::post('inventarios/solicitudes/tipos_insumo', 'Almacen\SolicitudController@getTiposInsumo');
Route::post('inventarios/solicitudes/cat_articulos', 'Almacen\SolicitudController@getCatArticulos');
Route::post('inventarios/solicitudes/cambiar_estado/{id}', 'Almacen\SolicitudController@cambiarEstado');


