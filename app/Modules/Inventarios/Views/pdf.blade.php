<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
<style>
    .clearfix:after {
        content: "";
        display: table;
        clear: both;
    }

    .a {
        color: #0087C3;
        text-decoration: none;
    }


    .header {
        padding: 0 1em;
        margin-bottom: 20px;
        border-bottom: 1px solid #AAAAAA;
    }

    .logo {
        float: left;
        margin-top: 8px;
    }

    .right {
        float: right;
        margin-top: 8px;
    }

    .logo img {
        height: 56px;
    }

    #company {
        margin-top: 8px;
        float: left;
        text-align: center;
        line-height: normal;
        font-size: 8pt;
        width: 50%;
        left: 0%;
        position: relative;
    }


    #details {
        margin-bottom: 20px;
    }

    #client {
        padding-left: 6px;
        border-left: 6px solid #0087C3;
        float: left;
        font-size: 10pt;
        line-height: 1rem;
    }

    #client .to {
        color: #777777;
    }

    h2.name {
        font-size: 1.4em;
        font-weight: normal;
        margin: 0;
    }

    #invoice {
        float: right;
        text-align: right;
        font-size: 9pt;
        line-height: 1rem;
    }

    #invoice h1 {
        color: #0087C3;
        font-size: 1.8em;
        line-height: 1em;
        font-weight: normal;
        margin: 0 0 0px 0;
    }

    #invoice .date {
        font-size: 1.1em;
        color: #777777;
    }

    .tablep {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px;
    }

    .tablep th,
    .tablep td {
        padding: 10px;
        background: #EEEEEE;
        text-align: center;
        border-bottom: 1px solid #FFFFFF;
    }

    .tablep th {
        font-weight: bold;
        white-space: nowrap;
    }

    .tablep td {
        text-align: right;
    }

    .tablep td h3 {
        color: #57B223;
        font-size: 1.1em;
        font-weight: normal;
        margin: 0 0 0.2em 0;
    }

    .tablep .no {
        color: #FFFFFF;
        font-size: 1.1em;
        background: #009688;
    }

    .tablep .desc {
        text-align: left;
    }

    .tablep .unit {
        background: #DDDDDD;
    }

    .tablep .qty {
    }

    .tablep .total {
        background: #009688;
        color: #FFFFFF;
    }

    .tablep td.unit,
    .tablep td.qty,
    .tablep td.total {
        font-size: 1.1em;
    }

    .tablep tbody tr:last-child td {
        border: none;
    }

    .tablep tfoot td {
        padding: 10px 20px;
        background: #FFFFFF;
        border-bottom: none;
        font-size: 1.1em;
        white-space: nowrap;
        border-top: 1px solid #AAAAAA;
    }

    .tablep tfoot tr:first-child td {
        border-top: none;
    }

    .tablep tfoot tr:last-child td {
        color: #57B223;
        font-size: 1.2em;
        border-top: 1px solid #57B223;

    }

    .tablep tfoot tr td:first-child {
        border: none;
    }

    #thanks {
        font-size: 1.2em;
        margin-bottom: 20px;
    }

    #notices {
        padding-left: 6px;
        border-left: 6px solid #0087C3;
    }

    #notices .notice {
        font-size: 1.1em;
    }

    .footer {
        color: #777777;
        width: 100%;
        margin-top: 1em;
        height: 30px;
        /*position: absolute;*/
        bottom: 0;
        border-top: 1px solid #AAAAAA;
        padding: 8px 0;
        text-align: center;
    }
    .main{
        padding: 0 1em;
    }
</style>

<header class="clearfix header">
    <div class="logo">
        <img src="{{asset('img/Logo_Salud_transparente_HZ-jun19.png')}}">
    </div>
    <div id="company">
        <div>SERVICIOS ESTATALES DE SALUD</div>
        <div>DIRECCIÓN ADMINISTRATIVA</div>
        <div>SUBDIRECCIÓN</div>
        <div>LABORATORIO ESTATAL DE SALUD</div>
    </div>
    <div class="logo right">
        <img src="{{asset('img/logo.jpg')}}">
    </div>
</header>
<main class="main">
    <div id="details" class="clearfix">
        <div id="client">
            <div class="to">{{ $transferencia->TipoTransferencia->descripcion }} </div>
            {{--            <h2 class="name">John Doe</h2>--}}
            <div class="address">ORIGEN: {{ $transferencia->AreaOrigen->nombre }}</div>
            <div class="address">DESTINO: {{ $transferencia->AreaDestino->nombre }}</div>
            <div class="">OBSERVACIONES: {{ $transferencia->observaciones }}</div>
        </div>
        <div id="invoice">
            <h1>FOLIO #{{$transferencia->id}}</h1>
            <div class="date">FECHA TRANSFERENCIA: {{ $transferencia->fecha }}</div>
            <div class="date">FECHA RECEPCIÓN: {{ $transferencia->fecha }}</div>
            <div class="date">ESTADO: {{ $transferencia->EstadoTransferencia->descripcion }}</div>
        </div>
    </div>
    <table class="tablep" border="0" cellspacing="0" cellpadding="0">
        <thead>
            <tr>
                <th class="no">#</th>
                <th class="desc">CLAVE</th>
                <th class="desc">PRESENTACIÓN</th>
                <th class="desc">UNI MEDIDA</th>
                <th class="desc">LOTE</th>
                <th class="desc">FECHA CADUCIDAD</th>
                <th class="unit">PRECIO</th>
                <th class="unit">IVA</th>
                <th class="qty">CANTIDAD</th>
                <th class="total">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            @forelse($transferencia->TransferenciaArticulos as $articulo)
            <tr>
                <td class="no">{{ $articulo->id }}</td>
                <td class="desc">{{ $articulo->Articulo->CatArticulo->descripcion }}</td>
                <td class="desc">TUBO 25 ML</td>
                <td class="desc">ML</td>
                <td class="desc">L01</td>
                <td class="desc">2019-11-28</td>
                <td class="unit">$10.00</td>
                <td class="unit">$0.00</td>
                <td class="qty">3</td>
                <td class="total">$940.00</td>
            </tr>
            @empty
                <p> No hay registros</p>
            @endforelse
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6"></td>
                <td colspan="2">SUBTOTAL</td>
                <td colspan="2">$5,200.00</td>
            </tr>
            <tr>
                <td colspan="6"></td>
                <td colspan="2">TAX 25%</td>
                <td colspan="2">$1,300.00</td>
            </tr>
            <tr>
                <td colspan="6"></td>
                <td colspan="2">GRAND TOTAL</td>
                <td colspan="2">$6,500.00</td>
            </tr>
        </tfoot>
    </table>
    <div id="thanks">Thank you!</div>
    <div id="notices">
        <div>NOTICE:</div>
        <div class="notice">A finance charge of 1.5% will be made on unpaid balances after 30 days.</div>
    </div>
</main>
<footer class="footer">
    Invoice was created on a computer and is valid without the signature and seal.
</footer>
</html>
