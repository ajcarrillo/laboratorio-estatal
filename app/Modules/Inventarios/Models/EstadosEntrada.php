<?php

namespace Inventarios\Models;

/**
 * Class EstadosEntrada
 * @package Inventarios\Models
 */
class EstadosEntrada extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'estados_entrada';
}
