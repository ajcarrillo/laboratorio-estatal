<?php

namespace Inventarios\Models;

/**
 * Class TransferenciaArticulos
 * @package Inventarios\Models
 */
class TransferenciaArticulos extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'transferencia_articulos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Articulo()
    {
        return $this->belongsTo('Inventarios\Models\Articulo', 'articulo_id', 'id');
    }
}
