<?php

namespace Inventarios\Models;

/**
 * Class EstadosSolicitud
 * @package Inventarios\Models
 */
class EstadosSolicitud extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'estados_solicitud';
}
