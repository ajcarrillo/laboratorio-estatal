<?php

namespace Inventarios\Models;


/**
 * Class Entrada
 * @package Inventarios\Models
 */
class Entrada extends InventariosBaseModel
{
    protected $fillable = [
        'tipo_entrada_id', 'estado_entrada_id', 'tipo_insumo_id', 'proveedor_id', 'area_origen_id'
        , 'area_almacen_destino_id', 'area_destino_id', 'fecha', 'observaciones', 'numero_registros',
        'total_sin_iva',
        'total_iva',
        'fecha_entrega',
    ];
    protected $table    = 'entradas';

    /**
     * @return string
     */
    public function getTag()
    {
        return $this->TipoEntrada->descripcion . ' #' . $this->getKey();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function tipo($id)
    {
        return static::where('tipo_entrada_id', $id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function TipoEntrada()
    {
        return $this->belongsTo('Inventarios\Models\TiposEntrada', 'tipo_entrada_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function EntradaSolicitud()
    {
        return $this->hasOne('Inventarios\Models\EntradaSolicitud', 'entrada_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function EstadoEntrada()
    {
        return $this->belongsTo('Inventarios\Models\EstadosEntrada', 'estado_entrada_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function TipoInsumo()
    {
        return $this->belongsTo('Inventarios\Models\TiposInsumo', 'tipo_insumo_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Proveedor()
    {
        return $this->belongsTo('Inventarios\Models\Proveedor', 'proveedor_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaOrigen()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_origen_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaAlmacenDestino()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_almacen_destino_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaDestino()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_destino_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function EntradaArticulos()
    {
        return $this->hasMany('Inventarios\Models\EntradaArticulos', 'entrada_id', 'id');
    }

    /**
     * @param $query
     * @param $tipo
     * @return mixed
     */
    public function scopeSTipo($query, $tipo)
    {
        return $query->where('tipo_entrada_id', $tipo);
    }

}
