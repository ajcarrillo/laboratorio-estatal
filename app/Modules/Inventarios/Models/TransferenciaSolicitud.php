<?php

namespace Inventarios\Models;

/**
 * Class TransferenciaSolicitud
 * @package Inventarios\Models
 */
class TransferenciaSolicitud extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'transferencias_solicitud';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transferencia()
    {
        return $this->belongsTo('Inventarios\Models\Transferencia', 'transferencia_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Solicitud()
    {
        return $this->belongsTo('Inventarios\Models\Solicitud', 'solicitud_id', 'id');
    }
}
