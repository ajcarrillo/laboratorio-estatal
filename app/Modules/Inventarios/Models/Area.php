<?php

namespace Inventarios\Models;

/**
 * Class Area
 * @package Inventarios\Models
 */
class Area extends InventariosBaseModel
{
    protected $guarded = ['clue', 'parent'];
    protected $table = 'areas';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Clue()
    {
        return $this->belongsTo('Inventarios\Models\Clues', 'clues_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Parent()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'parent_id', 'id');
    }
}
