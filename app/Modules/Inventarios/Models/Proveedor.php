<?php

namespace Inventarios\Models;

/**
 * Class Proveedor
 * @package Inventarios\Models
 */
class Proveedor extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'proveedores';
}
