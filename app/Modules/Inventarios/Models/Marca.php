<?php

namespace Inventarios\Models;

/**
 * Class Marca
 * @package Inventarios\Models
 */
class Marca extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'marcas';
}
