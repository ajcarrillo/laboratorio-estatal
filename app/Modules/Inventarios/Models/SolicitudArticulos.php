<?php

namespace Inventarios\Models;

/**
 * Class SolicitudArticulos
 * @package Inventarios\Models
 */
class SolicitudArticulos extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table   = 'solicitud_articulos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CatArticulo()
    {
        return $this->belongsTo('Inventarios\Models\CatArticulo', 'cat_articulo_id', 'id');
    }
}
