<?php

namespace Inventarios\Models;

/**
 * Class TiposSolicitud
 * @package Inventarios\Models
 */
class TiposSolicitud extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'tipos_solicitud';

}
