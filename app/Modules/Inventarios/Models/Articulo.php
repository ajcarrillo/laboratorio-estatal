<?php

namespace Inventarios\Models;

/**
 * Class Articulo
 * @package Inventarios\Models
 */
class Articulo extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'articulos';

    protected $appends = ['origen_tag'];

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return mixed
     */
    public function getOrigenTagAttribute()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CatArticulo()
    {
        return $this->belongsTo('Inventarios\Models\CatArticulo', 'cat_articulo_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Area()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function origenable()
    {
        return $this->morphTo();
    }
}
