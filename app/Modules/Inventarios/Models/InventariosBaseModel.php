<?php


namespace Inventarios\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class InventariosBaseModel
 * @package Inventarios\Models
 */
class InventariosBaseModel  extends Model
{
    protected $connection = 'inventarios';
}
