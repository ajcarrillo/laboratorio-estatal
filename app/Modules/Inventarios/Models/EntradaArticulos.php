<?php

namespace Inventarios\Models;

/**
 * Class EntradaArticulos
 * @package Inventarios\Models
 */
class EntradaArticulos extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'entrada_articulos';
    protected $appends = ['solicitud_articulo'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function CatArticulo()
    {
        return $this->belongsTo('Inventarios\Models\CatArticulo', 'cat_articulo_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function SolicitudArticulo()
    {
        return $this->belongsTo('Inventarios\Models\SolicitudArticulos', 'solicitud_articulo_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function getSolicitudArticuloAttribute()
    {
        return $this->SolicitudArticulo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Entrada()
    {
        return $this->belongsTo('Inventarios\Models\Entrada', 'entrada_id', 'id');
    }
}
