<?php

namespace Inventarios\Models;


/**
 * Class Solicitud
 * @package Inventarios\Models
 */
class Solicitud extends InventariosBaseModel
{
    protected $fillable = [ 'tipo_solicitud_id', 'estado_solicitud_id', 'tipo_insumo_id', 'area_origen_id'
        , 'area_almacen_destino_id', 'area_destino_id', 'fecha', 'observaciones', 'numero_registros',
        'total_solicitado_sin_iva',
        'proveedor_id',
        'total_solicitado_iva',
        'total_autorizado_sin_iva',
        'total_autorizado_iva',
    ];
    protected $table    = 'solicitudes';
    protected $appends  = [ 'tipo_insumo_text' ];

    /**
     * @return string
     */
    public function getTag()
    {
        return 'Solicitud #' . $this->getKey();
    }

    /**
     * @return string
     */
    public function getOrigen()
    {
        return '[' . $this->AreaDestino->getKey() . '] ' . $this->AreaDestino->nombre;
        //return $this->belongsTo('Inventarios\Models\Area', 'area_origen_id', 'id');
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function tipo($id)
    {
        return static::where('tipo_solicitud_id', $id);
    }

    /**
     * @return mixed
     */
    public function getTipoInsumoTextAttribute()
    {
        return $this->TipoInsumo->descripcion;
    }

    /**
     * @return mixed
     */
    public static function validados()
    {
        return static::where('estado_solicitud_id', 4);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function TipoSolicitud()
    {
        return $this->belongsTo('Inventarios\Models\TiposSolicitud', 'tipo_solicitud_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function EstadoSolicitud()
    {
        return $this->belongsTo('Inventarios\Models\EstadosSolicitud', 'estado_solicitud_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function TipoInsumo()
    {
        return $this->belongsTo('Inventarios\Models\TiposInsumo', 'tipo_insumo_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Proveedor()
    {
        return $this->belongsTo('Inventarios\Models\Proveedor', 'proveedor_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaOrigen()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_origen_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaAlmacenDestino()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_almacen_destino_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaDestino()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_destino_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function SolicitudArticulos()
    {
        return $this->hasMany('Inventarios\Models\SolicitudArticulos', 'solicitud_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function SolicitudArticuloEntrada()
    {
        return $this->hasMany('Inventarios\Models\SolicitudArticulos', 'solicitud_id', 'id')
            ->where('cat_articulo_id', $this->cat_articulo_id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function SolicitudArticulosPendientes()
    {
        return $this->hasMany('Inventarios\Models\SolicitudArticulos', 'solicitud_id', 'id')
            ->whereRaw('(cantidad_solicitada - cantidad_entregada + cantidad_entregar)');
    }

}
