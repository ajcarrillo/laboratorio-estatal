<?php

namespace Inventarios\Models;


/**
 * Class Transferencia
 * @package Inventarios\Models
 */
class Transferencia extends InventariosBaseModel
{
    protected $fillable = ['tipo_transferencia_id', 'estado_transferencia_id', 'area_origen_id'
    ,'area_almacen_destino_id', 'area_destino_id','fecha', 'observaciones', 'numero_registros',
        ];
    protected $table = 'transferencias';

    /**
     * @return string
     */
    public function getTag()
    {
        return 'TRANSFERENCIA #'. $this->getKey();
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function tipo($id)
    {
        return static::where('tipo_transferencia_id', $id);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function TipoTransferencia()
    {
        return $this->belongsTo('Inventarios\Models\TiposTransferencia', 'tipo_transferencia_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function EstadoTransferencia()
    {
        return $this->belongsTo('Inventarios\Models\EstadosTransferencia', 'estado_transferencia_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaOrigen()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_origen_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaAlmacenDestino()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_almacen_destino_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function AreaDestino()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_destino_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function TransferenciaArticulos()
    {
        return $this->hasMany('Inventarios\Models\TransferenciaArticulos', 'transferencia_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function transferenciaSolicitud()
    {
        return $this->hasOne('Inventarios\Models\TransferenciaSolicitud', 'transferencia_id', 'id');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeSSolicitudes($query)
    {
        return $query
            ->where('tipo_entrada_id', 2);
    }

}
