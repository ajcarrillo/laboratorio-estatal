<?php

namespace Inventarios\Models;

/**
 * Class EstadosTransferencia
 * @package Inventarios\Models
 */
class EstadosTransferencia extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'estados_transferencia';
}
