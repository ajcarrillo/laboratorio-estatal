<?php

namespace Inventarios\Models;

/**
 * Class TiposTransferencia
 * @package Inventarios\Models
 */
class TiposTransferencia extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'tipos_transferencia';
}
