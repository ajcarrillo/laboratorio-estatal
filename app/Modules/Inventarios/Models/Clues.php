<?php

namespace Inventarios\Models;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Clues
 * @package Inventarios\Models
 */
class Clues extends Model
{
    protected $connection = 'mysql';
    protected $guarded = [];
    protected $table = 'clues';

    /**
     * @return mixed
     */
    public static function estatales()
    {
        return static::where('cve_institucion_salud', 'SSA')
            ->where('cve_entidad', 23)
            ->where('cve_estatus_unidad', 1);
    }
}
