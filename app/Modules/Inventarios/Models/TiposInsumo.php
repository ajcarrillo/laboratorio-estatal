<?php

namespace Inventarios\Models;

/**
 * Class TiposInsumo
 * @package Inventarios\Models
 */
class TiposInsumo extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'tipos_insumo';
}
