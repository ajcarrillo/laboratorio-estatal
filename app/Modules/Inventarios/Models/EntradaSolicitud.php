<?php

namespace Inventarios\Models;

/**
 * Class EntradaSolicitud
 * @package Inventarios\Models
 */
class EntradaSolicitud extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'entradas_solicitud';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Entrada()
    {
        return $this->belongsTo('Inventarios\Models\Entrada', 'entrada_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Solicitud()
    {
        return $this->belongsTo('Inventarios\Models\Solicitud', 'solicitud_id', 'id');
    }
}
