<?php

namespace Inventarios\Models;

/**
 * Class CatArticulo
 * @package Inventarios\Models
 */
class CatArticulo extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'cat_articulos';
    protected $appends = ['text_tag'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function TipoInsumo()
    {
        return $this->belongsTo('Inventarios\Models\TiposInsumo', 'tipo_insumo_id', 'id');
    }

    /**
     * @return string
     */
    public function getTextTagAttribute()
    {
        return "[".$this->clave."] ".$this->descripcion." | ".$this->presentacion;
    }
}
