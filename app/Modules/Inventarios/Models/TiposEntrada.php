<?php

namespace Inventarios\Models;

/**
 * Class TiposEntrada
 * @package Inventarios\Models
 */
class TiposEntrada extends InventariosBaseModel
{
    protected $guarded = [];
    protected $table = 'tipos_entrada';
}
