<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Marca
 * @package Inventarios\Http\Resources
 */
class Marca extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'descripcion' => $this->descripcion,
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
