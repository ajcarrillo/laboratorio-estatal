<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Transferencia
 * @package Inventarios\Http\Resources
 */
class Transferencia extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo_transferencia_id' => $this->tipo_transferencia_id,
            'tipo_transferencia' => $this->TipoTransferencia->descripcion,

            'estado_transferencia_id' => $this->estado_transferencia_id,
            'estado_transferencia' => $this->EstadoTransferencia->descripcion,

            'area_origen_id' => $this->area_origen_id,
            'area_origen' => ($this->AreaOrigen)?$this->AreaOrigen->nombre:'',

            'area_almacen_destino_id' => $this->area_almacen_destino_id,
            'area_almacen_destino' => ($this->AreaAlmacenDestino)?$this->AreaAlmacenDestino->nombre:'',

            'area_destino_id' => $this->area_destino_id,
            'area_destino' => ($this->AreaDestino)?$this->AreaDestino->nombre:'',

            'fecha' => $this->fecha,
            'observaciones' => $this->observaciones,
            'numero_registros' => (Integer)$this->numero_registros,

            'items' => $this->TransferenciaArticulos()->with('Articulo', 'Articulo.CatArticulo')->get()
        ];
    }
}
