<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Area
 * @package Inventarios\Http\Resources
 */
class Area extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'parent' => isset($this->Parent)?$this->Parent->nombre:'',
            'parent_id' => $this->parent_id,
            'tipo' => $this->tipo,
            'comportamiento' => $this->comportamiento,
            'clue' => $this->Clue->clues,
            'clues_id' => $this->clues_id,
            'nombre' => $this->nombre,
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
