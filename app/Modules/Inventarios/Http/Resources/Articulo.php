<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Articulo
 * @package Inventarios\Http\Resources
 */
class Articulo extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                 => $this->id,
            'area_id'            => $this->area_id,
            'area'               => $this->Area->nombre,
            'clave'              => $this->CatArticulo->clave,
            'descripcion'        => $this->CatArticulo->descripcion,
            'unidad_medida'      => $this->CatArticulo->unidad_medida,
            'presentacion'       => $this->CatArticulo->presentacion,
            'marca_id'           => $this->CatArticulo->marca_id,
            'modelo'             => $this->modelo,
            'tipo_insumo'        => $this->CatArticulo->TipoInsumo->descripcion,
            'tipo_insumo_id'     => $this->CatArticulo->tipo_insumo_id,
            'serie'              => $this->serie,
            'lote'               => $this->lote,
            'fecha_ingreso'      => $this->fecha_ingreso,
            'fecha_caducidad'    => $this->fecha_caducidad,
            'cantidad_existente' => $this->cantidad_existente,
            'cantidad_transito'  => $this->cantidad_transito ?: 0,
            'activo'             => $this->activo,
            'precio'             => '$' . number_format($this->precio, 2, '.', ','),
            'iva'                => $this->iva,
            'origen'             => $this->origenable->getTag(),
        ];
    }
}
