<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class CatArticulo
 * @package Inventarios\Http\Resources
 */
class CatArticulo extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'clave' => $this->clave,
            'descripcion' => $this->descripcion,
            'unidad_medida' => $this->unidad_medida,
            'presentacion' => $this->presentacion,
            'marca' => $this->marca_id,
            'modelo' => $this->modelo,
            'tipo_insumo' => isset($this->TipoInsumo)?$this->TipoInsumo->descripcion:'',
            'tipo_insumo_id' => $this->tipo_insumo_id,
            'serie_obligatorio' => $this->serie_obligatorio,
            'lote_obligatorio' => $this->lote_obligatorio,
            'caducidad_obligatoria' => $this->caducidad_obligatoria,
            'marca_obligatoria' => $this->marca_obligatoria,
            'inventariable' => $this->inventariable,
            'activo' => $this->activo,
            'articulo_desagregado' => $this->cat_articulo_desagreagado_id,
            'unidades_desagregadas' => $this->num_unidades_desagregadas,
            'iva_default' => $this->iva_default,
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
