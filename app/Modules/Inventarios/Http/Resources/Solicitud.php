<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Solicitud
 * @package Inventarios\Http\Resources
 */
class Solicitud extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo_solicitud_id' => $this->tipo_solicitud_id,
            'tipo_solicitud' => $this->TipoSolicitud->descripcion,

            'estado_solicitud_id' => $this->estado_solicitud_id,
            'estado_solicitud' => $this->EstadoSolicitud->descripcion,

            'tipo_insumo_id' => $this->tipo_insumo_id,
            'tipo_insumo_text' => $this->TipoInsumo->descripcion,

            'area_origen_id' => $this->area_origen_id,
            'area_origen' => isset($this->AreaOrigen)?$this->AreaOrigen->nombre:'',

            'area_almacen_destino_id' => $this->area_almacen_destino_id,
            'area_almacen_destino' => isset($this->AreaAlmacenDestino)?$this->AreaAlmacenDestino->nombre:'',

            'area_destino_id' => $this->area_destino_id,
            'area_destino' => isset($this->AreaDestino)?$this->AreaDestino->nombre:'',

            'proveedor_id' => $this->proveedor_id,
            'proveedor' => isset($this->Proveedor)?$this->Proveedor->nombre_comercial:null,

            'fecha' => $this->fecha,
            'observaciones' => $this->observaciones,
            'numero_registros' => (Integer)$this->numero_registros,
            'total_sin_iva' => '$'.number_format((Double)$this->total_autorizado_sin_iva, 2, '.', ','),
            'total_iva' => (Double)$this->total_autorizado_iva,

            'items' => $this->SolicitudArticulos()->with('CatArticulo')->get()
        ];
    }
}
