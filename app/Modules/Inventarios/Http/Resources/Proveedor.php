<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Proveedor
 * @package Inventarios\Http\Resources
 */
class Proveedor extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'rfc' => $this->rfc,
            'nombre_comercial' => $this->nombre_comercial,
            'razon_social' => $this->razon_social,
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
