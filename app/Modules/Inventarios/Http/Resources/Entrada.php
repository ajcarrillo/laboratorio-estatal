<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Entrada
 * @package Inventarios\Http\Resources
 */
class Entrada extends JsonResource
{

    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo_entrada_id' => $this->tipo_entrada_id,
            'tipo_entrada' => $this->TipoEntrada->descripcion,

            'estado_entrada_id' => $this->estado_entrada_id,
            'estado_entrada' => $this->EstadoEntrada->descripcion,

            'tipo_insumo_id' => $this->tipo_insumo_id,
            'tipo_insumo' => $this->TipoInsumo->descripcion,

            'proveedor_id' => $this->proveedor_id,
            'proveedor' => $this->Proveedor->nombre_comercial,

            'area_origen_id' => $this->area_origen_id,
            'area_origen' => ($this->AreaOrigen)?$this->AreaOrigen->nombre:'',

            'area_almacen_destino_id' => $this->area_almacen_destino_id,
            'area_almacen_destino' => ($this->AreaAlmacenDestino)?$this->AreaAlmacenDestino->nombre:'',

            'area_destino_id' => $this->area_destino_id,
            'area_destino' => ($this->AreaDestino)?$this->AreaDestino->nombre:'',

            'fecha' => $this->fecha,
            'fecha_entrega' => $this->fecha_entrega,
            'observaciones' => $this->observaciones,
            'numero_registros' => (Integer)$this->numero_registros,
            'total_sin_iva' => '$'.number_format((Double)$this->total_sin_iva, 2, '.', ','),
            'total_iva' => (Double)$this->total_iva,

            'items' => $this->EntradaArticulos()->with('CatArticulo', 'SolicitudArticulo.CatArticulo')->get(),
            'entrada_solicitud' => $this->EntradaSolicitud()->with('Solicitud')->get() || NULL,
            'solicitud' => ($this->EntradaSolicitud)?$this->EntradaSolicitud->Solicitud:NULL,
            //'solicitud' => $this->EntradaSolicitud->Solicitud()->get()
        ];
    }
}
