<?php

namespace Inventarios\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class TiposInsumo
 * @package Inventarios\Http\Resources
 */
class TiposInsumo extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'clave' => $this->clave,
            'descripcion' => $this->descripcion,
            'updated_at' => $this->updated_at->toDateTimeString(),
        ];
    }
}
