<?php

namespace Inventarios\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Proveedor
 * @package Inventarios\Http\Requests
 */
class Proveedor extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'rfc' => 'string',
            'razon_social' => 'string',
            'nombre_comercial' => 'string',
        ];
    }
}
