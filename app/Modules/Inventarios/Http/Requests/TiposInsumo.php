<?php

namespace Inventarios\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TiposInsumo
 * @package Inventarios\Http\Requests
 */
class TiposInsumo extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'clave' => 'string',
            'descripcion' => 'string',
        ];
    }
}
