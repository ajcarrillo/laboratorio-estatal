<?php

namespace Inventarios\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Entrada
 * @package Inventarios\Http\Requests
 */
class Entrada extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
