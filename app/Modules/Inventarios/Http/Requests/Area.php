<?php

namespace Inventarios\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Area
 * @package Inventarios\Http\Requests
 */
class Area extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'clues' => '',
            'nombre' => 'string',
        ];
    }
}
