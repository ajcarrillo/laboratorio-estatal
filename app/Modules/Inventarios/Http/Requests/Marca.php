<?php

namespace Inventarios\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Marca
 * @package Inventarios\Http\Requests
 */
class Marca extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'string',
        ];
    }
}
