<?php

namespace Inventarios\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Articulo
 * @package Inventarios\Http\Requests
 */
class Articulo extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
