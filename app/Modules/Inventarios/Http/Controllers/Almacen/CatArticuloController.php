<?php

namespace Inventarios\Http\Controllers\Almacen;

use Inventarios\Http\Controllers\InventariosBaseController;
use Inventarios\Http\Requests\CatArticulo as Request;
use Inventarios\Models\CatArticulo as Model;
use Inventarios\Http\Resources\CatArticulo as Resource;
use Inventarios\Models\Marca;
use Inventarios\Models\TiposInsumo;


/**
 * Class CatArticuloController
 * @package Inventarios\Http\Controllers\Almacen
 */
class CatArticuloController extends InventariosBaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return Resource::collection(Model::all());
    }

    /**
     * @param $id
     * @return Resource
     */
    public function show($id)
    {
        $item = Model::find($id);
        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $item = Model::create($request->all());
        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function update(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());
        return new Resource($item);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Model::find($id);
        return response()->json($item->delete(), 202);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|Marca[]
     */
    public function getMarcas()
    {
        return Marca::all()->map(function($item){
            return ['text'=>$item->descripcion, 'value'=>$item->id];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|TiposInsumo[]
     */
    public function getTiposInsumo()
    {
        return TiposInsumo::all()->map(function($item){
            return ['text'=>$item->descripcion, 'value'=>$item->id];
        });
    }
}
