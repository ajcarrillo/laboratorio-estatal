<?php

namespace Inventarios\Http\Controllers\Almacen;

use Inventarios\Http\Controllers\InventariosBaseController;
use Inventarios\Http\Requests\Entrada as Request;
use Inventarios\Models\Area;
use Inventarios\Models\Articulo;
use Inventarios\Models\CatArticulo;
use Inventarios\Models\Entrada as Model;
use Inventarios\Http\Resources\Entrada as Resource;
use Inventarios\Models\EntradaArticulos;
use Inventarios\Models\EntradaSolicitud;
use Inventarios\Models\Proveedor;
use Inventarios\Models\Solicitud;
use Inventarios\Models\TiposEntrada;
use Inventarios\Models\TiposInsumo;

/**
 * Class EntradaController
 * @package Inventarios\Http\Controllers\Almacen
 */
class EntradaController extends InventariosBaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return Resource::collection(Model::all());
    }

    /**
     * @param $tipo
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getByTipo($tipo)
    {
        return Resource::collection(Model::sTipo($tipo)->get());
    }

    /**
     * @param $id
     * @return Resource
     */
    public function show($id)
    {
        $item = Model::find($id);

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $item = Model::create(array_merge($request->all(), [ 'estado_entrada_id' => 1 ]));

        $items = $this->setItems($request, $item);

        $item->numero_registros = $items["num_registros"];
        $item->total_sin_iva    = $items["total_sin_iva"];
        $item->total_iva        = $items["total_iva"];

        $item->save();

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function storeRemision(Request $request)
    {
        $item                           = Model::create(array_merge($request->all(), [ 'estado_entrada_id' => 1 ]));
        $entradaSolicitud               = new EntradaSolicitud();
        $entradaSolicitud->solicitud_id = $request->solicitud["id"];
        $item->tipo_insumo_id           = $request->solicitud["tipo_insumo_id"];
        $item->fecha                    = $request->solicitud["fecha"];
        $item->EntradaSolicitud()->save($entradaSolicitud);


        $items = $this->setItems($request, $item, true);

        $item->numero_registros = $items["num_registros"];
        $item->total_sin_iva    = $items["total_sin_iva"];
        $item->total_iva        = $items["total_iva"];

        $item->save();

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function update(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());

        $item->EntradaArticulos()->delete();

        $items = $this->setItems($request, $item);

        $item->numero_registros = $items["num_registros"];
        $item->total_sin_iva    = $items["total_sin_iva"];
        $item->total_iva        = $items["total_iva"];

        $item->save();

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function updateRemision(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());
        $cantidades_borrar = [];
        foreach ($item->EntradaArticulos as $entrada_articulo) {
            $cantidades_borrar[ $entrada_articulo["solicitud_articulo_id"] ][] = $entrada_articulo->cantidad;
        }
        $request->cantidades_borrar = $cantidades_borrar;
        $item->EntradaArticulos()->delete();

        $items = $this->setItems($request, $item, true);

        $item->numero_registros = $items["num_registros"];
        $item->total_sin_iva    = $items["total_sin_iva"];
        $item->total_iva        = $items["total_iva"];

        $item->save();

        return new Resource($item);
    }

    /**
     * @param $request
     * @param $item
     * @param bool $remision
     * @return array
     */
    private function setItems($request, $item, $remision = false)
    {
        $num_registros = 0;
        $total_sin_iva = 0;
        $total_iva     = 0;

        if (isset($request->items)) {

            foreach ($request->items as $itemEntrada) {
                $entrada_articulo = new EntradaArticulos();
                if ($remision) {
                    $solicitud_articulo = $item->EntradaSolicitud
                        ->Solicitud
                        ->SolicitudArticulos()
                        ->where('cat_articulo_id', $itemEntrada["cat_articulo"]["id"])->first();

                    $entrada_articulo->solicitud_articulo_id = $solicitud_articulo->id;
                    // Nueva cantidad a entregar
                    foreach ($request->cantidades_borrar as $key => $value) {
                        if ($key == $solicitud_articulo->id) {
                            $solicitud_articulo->cantidad_entregar -= array_sum($value);
                        }
                    }

                    $solicitud_articulo->cantidad_entregar += isset($itemEntrada["cantidad"]) ? $itemEntrada["cantidad"] : 0;
                    $solicitud_articulo->save();
                }
                $entrada_articulo->entrada_id            = $item->id;
                $entrada_articulo->cat_articulo_id       = $itemEntrada["cat_articulo"]["id"];
                $entrada_articulo->descripcion_adicional = isset($itemEntrada["descripcion_adicional"]) ? $itemEntrada["descripcion_adicional"] : NULL;
                $entrada_articulo->serie                 = isset($itemEntrada["serie"]) ? $itemEntrada["serie"] : NULL;
                $entrada_articulo->lote                  = isset($itemEntrada["lote"]) ? $itemEntrada["lote"] : NULL;
                $entrada_articulo->color                 = isset($itemEntrada["color"]) ? $itemEntrada["color"] : NULL;
                $entrada_articulo->fecha_caducidad       = isset($itemEntrada["fecha_caducidad"]) ? $itemEntrada["fecha_caducidad"] : NULL;
                $entrada_articulo->cantidad              = isset($itemEntrada["cantidad"]) ? $itemEntrada["cantidad"] : NULL;
                $entrada_articulo->cantidad_entregada    = isset($itemEntrada["cantidad_entregada"]) ? $itemEntrada["cantidad_entregada"] : 0;
                $entrada_articulo->precio                = isset($itemEntrada["precio"]) ? $itemEntrada["precio"] : 0;
                $entrada_articulo->iva                   = isset($itemEntrada["iva"]) ? $itemEntrada["iva"] : 0;
                $entrada_articulo->save();

                $num_registros++;
                if ($entrada_articulo->cantidad_entregada) {
                    $total_sin_iva += $entrada_articulo->precio * $entrada_articulo->cantidad_entregada;
                    $total_iva     += $entrada_articulo->iva * $entrada_articulo->cantidad_entregada;
                } else {
                    $total_sin_iva += $entrada_articulo->precio * $entrada_articulo->cantidad;
                    $total_iva     += $entrada_articulo->iva * $entrada_articulo->cantidad;
                }
            }
        }

        return [ 'num_registros' => $num_registros, 'total_sin_iva' => $total_sin_iva, 'total_iva' => $total_iva ];
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Model::find($id);

        return response()->json($item->delete(), 202);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|Resource
     */
    public function cambiarEstado(Request $request, $id)
    {
        $item     = Model::find($id);
        $remision = false;
        // Si pasará a finalizada, se insertan los articulos en existencias
        if ($request->estado_entrada_id == 5) {
            if ($item->EntradaArticulos->count()) {
                if ($item->EntradaSolicitud) {
                    $remision = true;
                    // Actualizar cantidad entregada
                    //$item->EntradaSolicitud->Solicitud->SolicitudArticulos();
                }
                $this->insertarExistencias($item, $remision);
            } else {
                return [ 'message' => 'La entrada no tiene articulos' ];
            }
        }
        $item->update($request->all());

        return new Resource($item);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|TiposEntrada[]
     */
    public function getTiposEntrada()
    {
        return TiposEntrada::all()->map(function ($item) {
            return [ 'text' => $item->descripcion, 'value' => $item->id ];
        });
    }

    /**
     * @return mixed
     */
    public function getOrdenesCompra()
    {
        return Solicitud::tipo(2)->where('estado_solicitud_id', 4)->get()->map(function ($item) {
            return [
                'text'             => $item->getTag() . " | " . $item->getOrigen(),
                'value'            => $item->id,
                'id'               => $item->id,
                'fecha'            => $item->fecha,
                'numero_registros' => $item->numero_registros,
                'tipo_insumo_text' => $item->TipoInsumo->descripcion,
                'tipo_insumo_id'   => $item->tipo_insumo_id,
                'area_origen_id'   => $item->area_origen_id,
                'area_destino_id'   => $item->area_destino_id,
                'proveedor_id'     => $item->proveedor_id,
            ];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|TiposInsumo[]
     */
    public function getTiposInsumo()
    {
        return TiposInsumo::all()->map(function ($item) {
            return [ 'text' => $item->descripcion, 'value' => $item->id ];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|Proveedor[]
     */
    public function getProveedores()
    {
        return Proveedor::all()->map(function ($item) {
            return [ 'text' => $item->nombre_comercial, 'value' => $item->id ];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|Area[]
     */
    public function getAreas()
    {
        return Area::all()->map(function ($item) {
            return [ 'text' => $item->nombre, 'value' => $item->id ];
        });
    }

    /**
     * @param Request $request
     * @return array
     */
    public function guardarArticulo(Request $request)
    {
        return $request->all();
    }

    /**
     * @param $entrada
     * @param bool $remision
     * @return bool
     */
    private function insertarExistencias($entrada, $remision = false)
    {

        $items = [];
        foreach ($entrada->EntradaArticulos as $art) {
            $item    = [
                'cat_articulo_id'       => $art->cat_articulo_id,
                'area_id'               => $entrada->area_destino_id,
                'descripcion_adicional' => $art->descripcion_adicional,
                'serie'                 => $art->serie,
                'lote'                  => $art->lote,
                'color'                 => $art->color,
                'fecha_ingreso'         => $entrada->fecha,
                'fecha_caducidad'       => $art->fecha_caducidad,
                'cantidad_original'     => ($art->cantidad_entregada) ?: $art->cantidad,
                'cantidad_existente'    => ($art->cantidad_entregada) ?: $art->cantidad,
                'cantidad_transito'     => 0,
                'precio'                => $art->precio,
                'iva'                   => $art->iva,
                'origenable_type'       => 'Inventarios\Models\Entrada',
                'origenable_id'         => $entrada->id,
            ];
            $items[] = $item;
        }

        Articulo::insert($items);

        if ($remision) {
            foreach ($entrada->EntradaArticulos as $art) {
                $solicitud_articulo                     = $entrada->EntradaSolicitud->Solicitud->SolicitudArticulos()->find($art->solicitud_articulo_id);
                $solicitud_articulo->cantidad_entregada += $art->cantidad_entregada;
                $solicitud_articulo->cantidad_entregar  -= $art->cantidad_entregada;
                $solicitud_articulo->save();
            }
        }

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|CatArticulo[]
     */
    public function getCatArticulos()
    {
        return CatArticulo::all()->map(function ($item) {
            return [
                'text'                  => '[' . $item->clave . '] ' . $item->descripcion,
                'id'                    => $item->id,
                'clave'                 => $item->clave,
                'presentacion'          => $item->presentacion,
                'unidad_medida'         => $item->unidad_medida,
                'marca'                 => $item->marca,
                'modelo'                => $item->marca,
                'serie_obligatorio'     => $item->serie_obligatorio,
                'lote_obligatorio'      => $item->lote_obligatorio,
                'marca_obligatoria'     => $item->marca_obligatoria,
                'caducidad_obligatoria' => $item->caducidad_obligatoria,
                'iva_default'           => $item->iva_default,
            ];
        });
    }

    /**
     * @param Request $request
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function getArticulos(Request $request)
    {
        $solicitud = Solicitud::query()
            ->with('SolicitudArticulosPendientes.CatArticulo')
            ->find($request->id);

        return $solicitud;
        /*
        dd($solicitud);
        return CatArticulo::where('area_id', $request->area_id)->get()->map(function ($item) {
            return [
                'text' => '[' . $item->CatArticulo->clave . '] '
                    . $item->CatArticulo->descripcion . ' Lote: '
                    . $item->lote,
                'id' => $item->id,
                'cat_articulo' => [
                    'clave' => $item->CatArticulo->clave,
                    'presentacion' => $item->CatArticulo->presentacion,
                    'unidad_medida' => $item->CatArticulo->unidad_medida,
                    'marca' => $item->CatArticulo->marca,
                    'modelo' => $item->CatArticulo->marca,
                    'serie_obligatorio' => $item->CatArticulo->serie_obligatorio,
                    'lote_obligatorio' => $item->CatArticulo->lote_obligatorio,
                    'marca_obligatoria' => $item->CatArticulo->marca_obligatoria,
                    'caducidad_obligatoria' => $item->CatArticulo->caducidad_obligatoria,
                    'iva_default' => $item->CatArticulo->iva_default,
                ],

                'lote' => $item->lote,
                'cantidad_existente' => $item->cantidad_existente,
                'fecha_caducidad' => $item->fecha_caducidad,
                'precio' => $item->precio,
                'iva' => $item->iva,
                'descripcion_adicional' => $item->descripcion_adicional,
                'origen_tag' => $item->origen_tag,


            ];
        });
        */
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pdf(Request $request, $id)
    {
        $item = Model::find($id);

        // Si pasará a finalizada, se insertan los articulos en existencias
        return view('pdf');
        //return new Resource($item);
    }
}
