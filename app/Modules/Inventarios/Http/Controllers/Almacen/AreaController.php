<?php

namespace Inventarios\Http\Controllers\Almacen;

use Inventarios\Http\Controllers\InventariosBaseController;
use Inventarios\Http\Requests\Area as Request;
use Inventarios\Models\Area as Model;
use Inventarios\Http\Resources\Area as Resource;
use Inventarios\Models\Clues;


/**
 * Class AreaController
 * @package Inventarios\Http\Controllers\Almacen
 */
class AreaController extends InventariosBaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return Resource::collection(Model::all());
    }

    /**
     * @param $id
     * @return Resource
     */
    public function show($id)
    {
        $item = Model::find($id);
        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $item = Model::create($request->all());
        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function update(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());
        return new Resource($item);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Model::find($id);
        return response()->json($item->delete(), 202);
    }

    /**
     * @return mixed
     */
    public function getCluesEstatales()
    {
        return Clues::estatales()->get()->map(function($item){
          return ['text'=>$item->clues, 'value'=>$item->id];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|Model[]
     */
    public function getAreas()
    {
        return Model::all()->map(function($item){
            return ['text'=>$item->nombre, 'value'=>$item->id];
        });
    }
}
