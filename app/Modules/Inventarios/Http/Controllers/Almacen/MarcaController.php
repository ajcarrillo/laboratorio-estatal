<?php

namespace Inventarios\Http\Controllers\Almacen;

use Inventarios\Http\Controllers\InventariosBaseController;
use Inventarios\Http\Requests\Marca as Request;
use Inventarios\Models\Marca as Model;
use Inventarios\Http\Resources\Marca as Resource;


/**
 * Class MarcaController
 * @package Inventarios\Http\Controllers\Almacen
 */
class MarcaController extends InventariosBaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return Resource::collection(Model::all());
    }

    /**
     * @param $id
     * @return Resource
     */
    public function show($id)
    {
        $item = Model::find($id);
        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $item = Model::create($request->all());
        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function update(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());
        return new Resource($item);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Model::find($id);
        return response()->json($item->delete(), 202);
    }
}
