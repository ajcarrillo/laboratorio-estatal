<?php

namespace Inventarios\Http\Controllers\Almacen;

use Inventarios\Http\Controllers\InventariosBaseController;
use Inventarios\Http\Requests\Solicitud as Request;
use Inventarios\Models\Area;
use Inventarios\Models\Articulo;
use Inventarios\Models\CatArticulo;
use Inventarios\Models\Solicitud as Model;
use Inventarios\Http\Resources\Solicitud as Resource;
use Inventarios\Models\SolicitudArticulos;
use Inventarios\Models\TiposSolicitud;
use Inventarios\Models\TiposInsumo;

/**
 * Class SolicitudController
 * @package Inventarios\Http\Controllers\Almacen
 */
class SolicitudController extends InventariosBaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return Resource::collection(Model::all());
    }

    /**
     * @param $tipo
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getByTipo($tipo)
    {
        return Resource::collection(Model::tipo($tipo)->get());
    }

    /**
     * @param $id
     * @return Resource
     */
    public function show($id)
    {
        $item = Model::find($id);

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $item = Model::create(array_merge($request->all(), [ 'estado_solicitud_id' => 1 ]));

        $items = $this->setItems($request, $item);

        $item->numero_registros         = $items["num_registros"];
        $item->total_solicitado_sin_iva = $items["total_sin_iva"];
        $item->total_solicitado_iva     = $items["total_iva"];


        $item->save();

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function update(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());

        $item->SolicitudArticulos()->delete();

        $items = $this->setItems($request, $item);

        $item->numero_registros         = $items["num_registros"];
        $item->total_solicitado_sin_iva = $items["total_sin_iva"];
        $item->total_solicitado_iva     = $items["total_iva"];

        $item->save();

        return new Resource($item);
    }

    /**
     * @param $request
     * @param $item
     * @return array
     */
    private function setItems($request, $item)
    {
        $num_registros            = 0;
        $total_solicitado_sin_iva = 0;
        $total_solicitado_iva     = 0;

        if ($request->input('items', NULL)) {

            foreach ($request->items as $itemSolicitud) {
                $solicitud_articulo                      = new SolicitudArticulos();
                $solicitud_articulo->cantidad_solicitada = isset($itemSolicitud["cantidad_solicitada"]) ? $itemSolicitud["cantidad_solicitada"] : NULL;
                $solicitud_articulo->cantidad_autorizada = isset($itemSolicitud["cantidad_autorizada"]) ? $itemSolicitud["cantidad_autorizada"] : NULL;
                $item->SolicitudArticulos()->save($solicitud_articulo);
                $solicitud_articulo->CatArticulo()->associate(CatArticulo::find($itemSolicitud["cat_articulo"]["id"]));
                $solicitud_articulo->save();

                $num_registros++;
                $total_solicitado_sin_iva += $solicitud_articulo->precio * $solicitud_articulo->cantidad_solicitada;
                $total_solicitado_iva     += $solicitud_articulo->iva * $solicitud_articulo->cantidad_solicitada;
            }
        }

        return [ 'num_registros' => $num_registros, 'total_sin_iva' => $total_solicitado_sin_iva, 'total_iva' => $total_solicitado_iva ];
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Model::find($id);

        return response()->json($item->delete(), 202);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function cambiarEstado(Request $request, $id)
    {
        $item = Model::find($id);
        // Si pasará a finalizada, se insertan los articulos en existencias
        /*if ($request->estado_solicitud_id == 3) {
            if ($item->SolicitudArticulos->count()) {
                $this->insertarExistencias($item);
            } else {
                return [ 'message' => 'La solicitud no tiene articulos' ];
            }
        }*/
        $item->update($request->all());

        return new Resource($item);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|TiposSolicitud[]
     */
    public function getTiposSolicitud()
    {
        return TiposSolicitud::all()->map(function ($item) {
            return [ 'text' => $item->descripcion, 'value' => $item->id ];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|TiposInsumo[]
     */
    public function getTiposInsumo()
    {
        return TiposInsumo::all()->map(function ($item) {
            return [ 'text' => $item->descripcion, 'value' => $item->id ];
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|Area[]
     */
    public function getAreas()
    {
        return Area::all()->map(function ($item) {
            return [ 'text' => $item->nombre, 'value' => $item->id ];
        });
    }

    /**
     * @param Request $request
     * @return array
     */
    public function guardarArticulo(Request $request)
    {
        return $request->all();
    }

    /**
     * @param $solicitud
     * @return bool
     */
    private function insertarExistencias($solicitud)
    {
        $items = [];
        foreach ($solicitud->SolicitudArticulos as $art) {
            $item    = [
                'cat_articulo_id'       => $art->cat_articulo_id,
                'area_id'               => $solicitud->area_destino_id,
                'descripcion_adicional' => $art->descripcion_adicional,
                'serie'                 => $art->serie,
                'lote'                  => $art->lote,
                'color'                 => $art->color,
                'fecha_ingreso'         => $solicitud->fecha,
                'fecha_caducidad'       => $art->fecha_caducidad,
                'cantidad_original'     => $art->cantidad,
                'cantidad_existente'    => $art->cantidad,
                'cantidad_transito'     => 0,
                'precio'                => $art->precio,
                'iva'                   => $art->iva,
                'origenable_type'       => 'Inventarios\Models\Solicitud',
                'origenable_id'         => $solicitud->id,
            ];
            $items[] = $item;
        }

        Articulo::insert($items);

        return true;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|CatArticulo[]
     */
    public function getCatArticulos()
    {
        return CatArticulo::all()->map(function ($item) {
            return [
                'text'                  => '[' . $item->clave . '] ' . $item->descripcion,
                'id'                    => $item->id,
                'clave'                 => $item->clave,
                'presentacion'          => $item->presentacion,
                'unidad_medida'         => $item->unidad_medida,
                'marca'                 => $item->marca,
                'modelo'                => $item->marca,
                'serie_obligatorio'     => $item->serie_obligatorio,
                'lote_obligatorio'      => $item->lote_obligatorio,
                'marca_obligatoria'     => $item->marca_obligatoria,
                'caducidad_obligatoria' => $item->caducidad_obligatoria,
                'iva_default'           => $item->iva_default,
            ];
        });
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pdf(Request $request, $id)
    {
        $item = Model::find($id);

        // Si pasará a finalizada, se insertan los articulos en existencias
        return view('pdf');
        //return new Resource($item);
    }
}
