<?php

namespace Inventarios\Http\Controllers\Transferencias;

use Inventarios\Http\Controllers\InventariosBaseController;
use Inventarios\Http\Requests\Transferencia as Request;
use Inventarios\Models\Area;
use Inventarios\Models\Articulo;
use Inventarios\Models\Solicitud;
use Inventarios\Models\Transferencia;
use Inventarios\Models\Transferencia as Model;
use Inventarios\Http\Resources\Transferencia as Resource;
use Inventarios\Models\TransferenciaArticulos;
use Inventarios\Models\TiposTransferencia;


/**
 * Class TransferenciaController
 * @package Inventarios\Http\Controllers\Transferencias
 */
class TransferenciaController extends InventariosBaseController
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return Resource::collection(Model::all());
    }

    /**
     * @param $tipo
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getByTipo($tipo)
    {
        return Resource::collection(Model::tipo($tipo)->get());
    }

    /**
     * @param $id
     * @return Resource
     */
    public function show($id)
    {
        $item = Model::find($id);

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @return Resource
     */
    public function store(Request $request)
    {
        $item = Model::create(array_merge($request->all(), [ 'estado_transferencia_id' => 1 ]));

        $items = $this->setItems($request, $item);

        $item->numero_registros = $items["num_registros"];

        $item->save();

        return new Resource($item);
    }

    /**
     * @param Request $request
     * @param $id
     * @return Resource
     */
    public function update(Request $request, $id)
    {
        $item = Model::find($id);
        $item->update($request->all());

        $items = $item->TransferenciaArticulos;

        if ($items) {
            foreach ($items as $itemTransferencia) {
                // Update articulos
                $articulo                     = Articulo::find($itemTransferencia->articulo_id);
                $articulo->cantidad_transito  -= $itemTransferencia->cantidad;
                $articulo->cantidad_existente += $itemTransferencia->cantidad;
                $articulo->save();
            }
        }
        $item->TransferenciaArticulos()->delete();

        $items = $this->setItems($request, $item);

        $item->numero_registros = $items["num_registros"];

        $item->save();

        return new Resource($item);
    }

    // REQUEST, TRANSFERENCIA (ITEM)

    /**
     * @param $request
     * @param $item
     * @return array
     */
    private function setItems($request, $item)
    {
        $num_registros = 0;

        if (isset($request->items)) {

            foreach ($request->items as $itemTransferencia) {
                $transferencia_articulo                        = new TransferenciaArticulos();
                $transferencia_articulo->transferencia_id      = $item->id;
                $transferencia_articulo->articulo_id           = $itemTransferencia["articulo"]["id"];
                $transferencia_articulo->cantidad              = isset($itemTransferencia["cantidad"]) ? $itemTransferencia["cantidad"] : NULL;
                $transferencia_articulo->cantidad_recibida     = isset($itemTransferencia["cantidad_recibida"]) ? $itemTransferencia["cantidad_recibida"] : 0;
                $transferencia_articulo->descripcion_adicional = isset($itemTransferencia["descripcion_adicional"]) ? $itemTransferencia["descripcion_adicional"] : NULL;

                if ($transferencia_articulo->save()) {
                    $num_registros++;
                    // Update articulos
                    $articulo                     = Articulo::find($transferencia_articulo->articulo_id);
                    $articulo->cantidad_transito  += $transferencia_articulo->cantidad;
                    $articulo->cantidad_existente -= $transferencia_articulo->cantidad;
                    $articulo->save();
                }
            }
        }

        return [ 'num_registros' => $num_registros ];
    }

    /**
     * @param $request
     * @param $item
     */
    private function setItemsUpdate($request, $item)
    {
        $items = $item->TransferenciaArticulos;
        dd($items);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Model::find($id);

        return response()->json($item->delete(), 202);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|Resource
     */
    public function cambiarEstado(Request $request, $id)
    {
        $item = Model::find($id);
        // Si pasará a finalizada, se insertan y modifican los articulos en existencias
        if ($request->estado_transferencia_id == 4) {
            if ($item->TransferenciaArticulos->count()) {

                $this->modificarExistencias($item);
                // Solo inserta las nuevas existencias si es un transferencia almacen
                if ($item->tipo_transferencia_id == 1){
                    $this->insertarExistencias($item);
                }
            } else {
                return [ 'message' => 'La transferencia no tiene articulos' ];
            }
        }
        $item->update($request->all());

        return new Resource($item);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|TiposTransferencia[]
     */
    public function getTiposTransferencia()
    {
        return TiposTransferencia::all()->map(function ($item) {
            return [ 'text' => $item->descripcion, 'value' => $item->id ];
        });
    }

    /**
     * @param $transferencia
     * @return bool
     */
    private function modificarExistencias($transferencia)
    {
        foreach ($transferencia->TransferenciaArticulos as $art) {
            $articulo = Articulo::find($art->articulo_id);


            //Aplicar condicion para descargas de consumo
            if ($transferencia->tipo_transferencia_id == 2) {
                //$articulo->cantidad_existente -= $art->cantidad;
            } else {
                if ($art->cantidad_recibida < $art->cantidad) {
                    $articulo->cantidad_existente += $art->cantidad - $art->cantidad_recibida;
                }
            }

            $articulo->cantidad_transito -= $art->cantidad;

            $articulo->save();
        }

        return true;
    }

    /**
     * @param $transferencia
     * @return bool
     */
    private function insertarExistencias($transferencia)
    {
        $items = [];

        foreach ($transferencia->TransferenciaArticulos as $art) {
            $item    = [
                'cat_articulo_id'       => $art->Articulo->cat_articulo_id,
                'area_id'               => $transferencia->area_destino_id,
                'descripcion_adicional' => $art->descripcion_adicional,
                'serie'                 => $art->Articulo->serie,
                'lote'                  => $art->Articulo->lote,
                'color'                 => $art->Articulo->color,
                'fecha_ingreso'         => $transferencia->fecha,
                'fecha_caducidad'       => $art->Articulo->fecha_caducidad,
                'cantidad_original'     => $art->cantidad_recibida,
                'cantidad_existente'    => $art->cantidad_recibida,
                'cantidad_transito'     => 0,
                'precio'                => $art->Articulo->precio,
                'iva'                   => $art->Articulo->iva,
                'origenable_type'       => 'Inventarios\Models\Transferencia',
                'origenable_id'         => $transferencia->id,
            ];
            $items[] = $item;
        }

        Articulo::insert($items);

        return true;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getArticulos(Request $request)
    {
        return Articulo::where('area_id', $request->area_id)->get()->map(function ($item) {
            return [
                'text'         => '[' . $item->CatArticulo->clave . '] '
                    . $item->CatArticulo->descripcion . ' Lote: '
                    . $item->lote,
                'id'           => $item->id,
                'cat_articulo' => [
                    'clave'                 => $item->CatArticulo->clave,
                    'presentacion'          => $item->CatArticulo->presentacion,
                    'unidad_medida'         => $item->CatArticulo->unidad_medida,
                    'marca'                 => $item->CatArticulo->marca,
                    'modelo'                => $item->CatArticulo->marca,
                    'serie_obligatorio'     => $item->CatArticulo->serie_obligatorio,
                    'lote_obligatorio'      => $item->CatArticulo->lote_obligatorio,
                    'marca_obligatoria'     => $item->CatArticulo->marca_obligatoria,
                    'caducidad_obligatoria' => $item->CatArticulo->caducidad_obligatoria,
                    'iva_default'           => $item->CatArticulo->iva_default,
                ],

                'lote'                  => $item->lote,
                'cantidad_existente'    => $item->cantidad_existente,
                'fecha_caducidad'       => $item->fecha_caducidad,
                'precio'                => $item->precio,
                'iva'                   => $item->iva,
                'descripcion_adicional' => $item->descripcion_adicional,
                'origen_tag'            => $item->origen_tag,
            ];
        });
    }

    /**
     * @return mixed
     */
    public function getSolicitudes()
    {
        return Solicitud::tipo(1)->where('estado_solicitud_id', 4)->get()->map(function ($item) {
            return [
                'text'             => $item->getTag() . " | " . $item->getOrigen(),
                'value'            => $item->id,
                'id'               => $item->id,
                'fecha'            => $item->fecha,
                'numero_registros' => $item->numero_registros,
                'tipo_insumo_text' => $item->TipoInsumo->descripcion,
                'tipo_insumo_id'   => $item->tipo_insumo_id,
                'area_origen_id'   => $item->area_origen_id,
                'area_destino_id'  => $item->area_destino_id,
                'proveedor_id'     => $item->proveedor_id,
            ];
        });
    }

    public function reporte($id)
    {
        $transferencia = Model::find($id);

        //$transferencia = new Resource($item);
        //$transferencia = new Resource(Transferencia::findOrFail($id));
        //dd($transferencia->AreaOrigen);
        return view('pdf', compact('transferencia'));
    }

    public function reportePdf($id)
    {
        $transferencia = Model::find($id);

        //$transferencia = new Resource($item);
        //$transferencia = new Resource(Transferencia::findOrFail($id));
        //dd($transferencia->AreaOrigen);
        //return view('pdf', compact('transferencia'));
        return \PDF::loadView('pdf', compact('transferencia'))->inline('Vcl.pdf');

    }


}
