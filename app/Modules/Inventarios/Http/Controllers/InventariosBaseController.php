<?php


namespace Inventarios\Http\Controllers;
use App\Http\Controllers\Controller;

/**
 * Class InventariosBaseController
 * @package Inventarios\Http\Controllers
 */
class InventariosBaseController extends Controller
{
    /**
     * InventariosBaseController constructor.
     */
    public function __construct()
    {
    }
}
