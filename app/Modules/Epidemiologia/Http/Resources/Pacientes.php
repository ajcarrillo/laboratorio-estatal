<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Pacientes
 * @package Epidemiologia\Http\Resources
 */
class Pacientes extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'nombre'=>$this->nombre_apellidos,
        ];
    }
}
