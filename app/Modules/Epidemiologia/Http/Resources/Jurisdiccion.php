<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Jurisdiccion
 * @package Epidemiologia\Http\Resources
 */
class Jurisdiccion extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'nombre'=>$this->cve_jurisdiccion.' - '.$this->nombre_unidad,
            'cve'=>$this->cve_jurisdiccion
        ];
    }
}
