<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Diagnosticos
 * @package Epidemiologia\Http\Resources
 */
class Diagnosticos extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'nombre'=>$this->label,
            'parent'=>$this->parent_label,
            'cve'=>$this->cie_superior
        ];
    }
}
