<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Responsables
 * @package Epidemiologia\Http\Resources
 */
class Responsables extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'nombre_apellidos'=>$this->titulo." ".$this->nombre_apellidos
        ];
    }
}
