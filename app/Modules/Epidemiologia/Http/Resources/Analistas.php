<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Analistas
 * @package Epidemiologia\Http\Resources
 */
class Analistas extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        $status = (!isset($this->activo))?'S/A':$this->activo;
        if (isset($this->activo)){
            $status = ($this->activo == 1)?'Activo':'Inactivo';
        }
        return [
            'nombre'=>$this->titulo." ".$this->nombre_apellidos,
            'analista_id' => $this->user_id,
            'laboratorio'=> $this->laboratorio,
            'fecha_inicio'=> $this->inicio,
            'fecha_fin'=> $this->fin,
            'estatus'=> $status,
        ];
    }
}
