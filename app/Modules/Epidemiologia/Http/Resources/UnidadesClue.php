<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class UnidadesClue
 * @package Epidemiologia\Http\Resources
 */
class UnidadesClue extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'nombre'=> $this->nombre_unidad,
            'direccion' => $this->colonia." ".$this->num_interior." ".$this->num_exterior

        ];
    }
}
