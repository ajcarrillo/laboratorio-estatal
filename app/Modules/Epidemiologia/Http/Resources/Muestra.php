<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Muestra
 * @package Epidemiologia\Http\Resources
 */
class Muestra extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id,
            'unidad_administrativa' => $this->unidadAdministrativa->nombre_unidad,
            'unidad_medica'         => $this->unidadMedica->nombre_unidad,
            'diagnosticos'          => $this->diagnosticos->descripcion,
            'diagnostico_id'        => $this->diagnosticos->id,
            'paciente'              => $this->pacientes->nombre_apellidos,
            'fecha_toma'            => (!empty($this->fecha_toma))?\Carbon\Carbon::parse($this->fecha_toma)->format('Y/m/d'):'',
            'fecha_recepcion'       => (!empty($this->fecha_recepcion))?\Carbon\Carbon::parse($this->fecha_recepcion)->format('Y/m/d'):'',
            'fecha_resultados'      => (!empty($this->fecha_resultados))?\Carbon\Carbon::parse($this->fecha_resultados)->format('Y/m/d'):'',
            'estatus'               => $this->status,
            'edit'                  => ($this->usuario_apertura == get_user_from_api_guard()->id),
            'numero_muestra'        => $this->numero_muestra
        ];
    }
}
