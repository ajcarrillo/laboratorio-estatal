<?php

namespace Epidemiologia\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class Laboratorio
 * @package Epidemiologia\Http\Resources
 */
class Laboratorio extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'nombre' => $this->descripcion,
            'tipo' => $this->tipo,
            'tipo_id' => ($this->tipo=="EPIDEMIOLOGIA")?"1":"2",
            'responsable' => ($this->responsable->isEmpty())?array():$this->responsable[0],
        ];
    }
}
