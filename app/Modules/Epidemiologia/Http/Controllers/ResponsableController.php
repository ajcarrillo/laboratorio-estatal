<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ExceptionError;
use App\Models\Laboratorio;
use Epidemiologia\Http\Resources\Muestra as MuestraRe;
use App\User;
use Epidemiologia\Models\MuestraEpidemiologica;
use Epidemiologia\Models\Resultado;
use App\Models\Revision;
/**
 * Class ResponsableController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class ResponsableController extends Controller
{
    use ExceptionError;

    public function index()
    {
        $labsAnalistas = Laboratorio::whereHas('Responsable',function($q){
                    $q->Where('user_id', '=', get_user_from_api_guard()->id)
                    ->Where('activo', '=', 1);
                })->with('analistas')->first();
        $muestras = $labsAnalistas->analistas()->with('muestras')->first();
        $listMuestRespon = $muestras->muestras()->with('unidadMedica')->with('pacientes')->with('diagnosticos')->get();
        // return response()->json($listMuestRespon);
        return MuestraRe::collection($listMuestRespon);
    }

    public function show($id)
    {}

    public function store(Request $request)
    {}

    public function update(Request $request, $id)
    {
    }

    public function changeEstatus(Request $request,$id)
    {
        try {
            // $this->checkUserPermission('ANALISTA EPIDEMIOLOGIA');
            $Muestra = MuestraEpidemiologica::find($id);
            $estatus = $request->estatus;
            $resivision =  new Revision(['fecha_apertura'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s'),'usuario_apertura_id'=>get_user_from_api_guard()->id,'estatus'=>$estatus]);
            $Muestra->statuses()->save($resivision);
            return ok();
        } catch (\Throwable  $e) {
            return $this->throwableEx($e);
        }

    }
}
