<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;
use App\Http\Controllers\Controller;
use App\Models\Laboratorio as Laboratorio;
use App\Traits\ExceptionError;
use Epidemiologia\Http\Requests\Laboratorio as Request;
use Epidemiologia\Http\Resources\Laboratorio as Resource;
use Epidemiologia\Http\Resources\Responsables as ResourceResponsables;

/**
 * Class LaboratorioController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class LaboratorioController extends Controller
{
    use ExceptionError;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $Lab = Laboratorio::with([ 'responsable' => function ($q) {
            $q->where('activo', '=', 1);
        } ])->orderBy('descripcion')->get();
        return Resource::collection($Lab);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function show($id)
    {
        $Laboratorio = Laboratorio::with(['responsable' => function($q){$q->where('activo', '=', 1);}])->where('id',$id)->get();
        return Resource::collection($Laboratorio);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $this->checkResponsable($request->responsable);
            $Laboratorio = Laboratorio::create($request->all());
            $Laboratorio->Responsable()->attach($request->responsable,['fecha_inicio'=>\Carbon\Carbon::now()->format('Y-m-d')]);
            \DB::commit();
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $Laboratorio = Laboratorio::with(['Responsable' => function($q){$q->where('activo', '=', 1);}])->where("id","=",$id)->first();
            $Laboratorio->descripcion = $request->descripcion;
            $Laboratorio->tipo = $request->tipo;
            if (!$Laboratorio->Responsable->isEmpty()) {
                if ($request->responsable != $Laboratorio->Responsable[0]->id) {
                    $Laboratorio->Responsable[0]->pivot->activo = 0;
                    $Laboratorio->Responsable[0]->pivot->fecha_fin = \Carbon\Carbon::now()->format('Y-m-d');
                    $Laboratorio->Responsable[0]->pivot->save();
                    $Laboratorio->Responsable()->attach($request->responsable,['fecha_inicio'=>\Carbon\Carbon::now()->format('Y-m-d')]);
                }

            } else {
                $Laboratorio->Responsable()->attach($request->responsable,['fecha_inicio'=>\Carbon\Carbon::now()->format('Y-m-d')]);
            }
            $Laboratorio->save();
            \DB::commit();
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    public function destroy($id)
    {
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getResponsables()
    {
        $users = \App\User::with('Laboratorios')->whereDoesntHave('Laboratorios',function($q){
            $q->where('activo', '=', 1);
        })->get();
        return ResourceResponsables::collection($users);
    }

    /**
     * @param $user_id
     * @throws \Exception
     */
    private function checkResponsable($user_id)
    {
        $users = \App\User::with('Laboratorios')->whereDoesntHave('Laboratorios',function($q){
            $q->where('activo', '=', 1);
        })->where("id","=",$user_id)->get();
        if ($users->isEmpty()) {
            throw new \Exception("El responsable ya esta asignado a un Laboratorio", 1);
        }
    }
}
