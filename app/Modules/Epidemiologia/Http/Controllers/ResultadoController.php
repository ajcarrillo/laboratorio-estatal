<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ExceptionError;
use Epidemiologia\Models\Resultado;
use Epidemiologia\Models\MuestraEpidemiologica;
use App\User;
use App\Models\Revision;

/**
 * Class ResultadoController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class ResultadoController extends Controller
{
    use ExceptionError;

    public function index()
    {}

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $muestra = MuestraEpidemiologica::with('diagnosticos','referencia')->where('id',$id)->first();
        return response()->json($muestra);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            $this->checkUserPermission('ANALISTA EPIDEMIOLOGIA');
            $muestra = MuestraEpidemiologica::find($request->id);
            $resultado = "";
            if (empty($muestra)) {
                throw new \Exception("Numero de muestra incorrecta", 1);
            }
            if (!empty($request->resultado_id)) {
                $resultado = Resultado::find($request->resultado_id)->descripcion;
            } else {
                $this->newResultado($request->resultado,$muestra->diagnostico_id);
                $resultado = $request->resultado;
            }
            $muestra->fecha_analisis = $request->fecha_analisis;
            $muestra->fecha_resultados = $request->fecha_resultado;
            $muestra->resultado = $resultado;
            $muestra->metodo_referencia_id = $request->referencia;
            $muestra->comentarios = $request->comentarios;
            $muestra->save();

            $resivision =  new Revision([
                'fecha_apertura'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'usuario_apertura_id'=>get_user_from_api_guard()->id,
                'estatus'=>'RESULTADOS',
                'comentarios'=>$request->comentarios,
            ]);
            $muestra->statuses()->save($resivision);

            \DB::commit();
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    /**
     * @param $resultado
     * @param $diagnostico_id
     * @return bool
     */
    private function newResultado($resultado,$diagnostico_id)
    {
        $hash = strtolower($resultado);
        $hash = str_replace (' ','',$hash);
        $hash = md5($hash);
        $resul = Resultado::where('hash',$hash)->first();
        if (empty($resul)) {
            $fields = [
                'diagnostico_id' => $diagnostico_id,
                'descripcion' =>$resultado,
                'hash' => $hash
            ];
            Resultado::create($fields);
        }
        return true;
    }

    public function update(Request $request, $id)
    {
    }

    /**
     * @param string $role
     * @param bool $bool
     * @return bool
     * @throws \Exception
     */
    private function checkUserPermission($role="",$bool = false)
    {
        $user = User::find(get_user_from_api_guard()->id);
        if (!$user->hasRole($role)) {
            if ($bool){
                return false;
            }
            throw new \Exception("Operacion no permitida", 1);
        }
        return true;
    }

    public function destroy($id)
    {
    }
}
