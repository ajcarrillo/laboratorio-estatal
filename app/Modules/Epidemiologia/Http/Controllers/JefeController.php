<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\ExceptionError;
use App\Models\Laboratorio;
use Epidemiologia\Http\Resources\Muestra as MuestraRe;
use App\User;
use Epidemiologia\Models\MuestraEpidemiologica;
use Epidemiologia\Models\Resultado;
use App\Models\Revision;

class JefeController extends Controller
{
    use ExceptionError;

    public function index()
    {
        $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                ->with('unidadMedica')
                ->with('pacientes')
                ->with('diagnosticos')
                ->get();
        return MuestraRe::collection($muestra);
    }

    public function show($id)
    {}

    public function store(Request $request)
    {}

    public function update(Request $request, $id)
    {
    }

    public function searchByDate(Request $request)
    {
        $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                ->with('unidadMedica')
                ->with('pacientes')
                ->with('diagnosticos');
        if (!empty($request->date_start)) {
            if (!empty($request->date_end)) {
                $muestra->whereBetween('fecha_resultados',[$request->date_start,$request->date_end]);
            } else {
                $muestra->where('fecha_resultados','>=',$request->date_start);
            }
        }
        return MuestraRe::collection($muestra->get());
    }
    public function searchByYear(Request $request)
    {
        $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                ->with('unidadMedica')
                ->with('pacientes')
                ->with('diagnosticos');
        if (!empty($request->anio)) {
            $muestra->where('created_at','>=',$request->anio);
        }
         return MuestraRe::collection($muestra->get());
    }

    public function changeEstatus(Request $request,$id)
    {
        try {
            // $this->checkUserPermission('ANALISTA EPIDEMIOLOGIA');
            $Muestra = MuestraEpidemiologica::find($id);
            $estatus = $request->estatus;
            $resivision =  new Revision(['fecha_apertura'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s'),'usuario_apertura_id'=>get_user_from_api_guard()->id,'estatus'=>$estatus]);
            $Muestra->statuses()->save($resivision);
            return ok();
        } catch (\Throwable  $e) {
            return $this->throwableEx($e);
        }

    }
}
