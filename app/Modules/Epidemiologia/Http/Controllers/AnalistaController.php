<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;

use App\Http\Controllers\Controller;
use App\Models\Laboratorio as Laboratorio;
use App\Traits\ExceptionError;
use App\User;
use Carbon\Carbon;
use DB;
use Epidemiologia\Http\Resources\Analistas as Resource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Throwable;

/**
 * Class AnalistaController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class AnalistaController extends Controller
{
    use ExceptionError;

    /**
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $users = User::select('users.id as user_id','analistas.inicio','analistas.fin','analistas.activo','analistas.activo','laboratorios.descripcion as laboratorio','users.titulo','users.nombre_apellidos')
            ->whereHas('roles',function($q){
                $q->whereIn('name', [ 'ANALISTA EPIDEMIOLOGIA', 'ANALISTA SANITARIA' ]);
            })
            ->leftJoin("analistas", function ($join) {
                $join->on('users.id', '=', 'analistas.user_id');
            })
            ->leftJoin('laboratorios','laboratorios.id','=','analistas.laboratorio_id')
            ->where(function ($query) {
                $query->where('analistas.activo','=',1)
                    ->orWhereNull('analistas.id');
            })
            ->orderBy('users.id','asc')
            ->get();
        return Resource::collection($users);
    }

    /**
     * @param $option
     * @return AnonymousResourceCollection
     */
    public function show($option)
    {
        if ($option == 1) {
            $users = User::select('users.id as user_id','analistas.inicio','analistas.fin','analistas.activo','analistas.activo','laboratorios.descripcion as laboratorio','users.titulo','users.nombre_apellidos')->join("analistas", function ($join) {
                $join->on('users.id', '=', 'analistas.user_id');
            })->whereHas('Analistas',function($q) use($option){
                $q->Where('activo', '=', 1);
            })->whereHas('roles',function($q){
                $q->whereIn('name', [ 'ANALISTA EPIDEMIOLOGIA', 'ANALISTA SANITARIA' ]);
            })->leftJoin('laboratorios','laboratorios.id','=','analistas.laboratorio_id')->groupBy('users.id')->get();
        }
        if ($option == 0) {
            $users = User::select('users.id as user_id','analistas.inicio','analistas.fin','analistas.activo','analistas.activo','laboratorios.descripcion as laboratorio','users.titulo','users.nombre_apellidos')->join("analistas", function ($join) {
                $join->on('users.id', '=', 'analistas.user_id');
            })->whereDoesntHave('Analistas',function($q) use($option){
                $q->Where('activo', '=', 1);
            })->whereHas('roles',function($q){
                $q->whereIn('name', [ 'ANALISTA EPIDEMIOLOGIA', 'ANALISTA SANITARIA' ]);
            })->leftJoin('laboratorios','laboratorios.id','=','analistas.laboratorio_id')->groupBy('users.id')->get();
        }
        return Resource::collection($users);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $id = $request->analista_id;
            $users = User::with(['Analistas' => function($q){$q->where('activo', '=', 1);}])->where("id","=",$id)->first();
            $newRespon = false;
            if(!$users->Analistas->isEmpty()){
                $analista = $users->Analistas->first();
                if ($request->labform != $analista->id) {
                    $users->Analistas[0]->pivot->activo = 0;
                    $users->Analistas[0]->pivot->fin    = Carbon::now()->format('Y-m-d');
                    $users->Analistas[0]->pivot->save();
                    $users->Analistas()->attach($request->labform, [ 'inicio' => Carbon::now()->format('Y-m-d'), 'activo' => 1 ]);
                }
            } else {
                $users->Analistas()->attach($request->labform, [ 'inicio' => Carbon::now()->format('Y-m-d') ]);
            }
            DB::commit();
            return ok();
        } catch (Throwable $e) {
            DB::rollback();
            return $this->throwableEx($e);
        }
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $users                              = User::with(['Analistas' => function($q){$q->where('activo', '=', 1);}])->where("id","=",$id)->first();
            $users->Analistas[0]->pivot->activo = 0;
            $users->Analistas[0]->pivot->fin    = Carbon::now()->format('Y-m-d');
            $users->Analistas[0]->pivot->save();
            return ok();
        } catch (Throwable $e) {
            return $this->throwableEx($e);
        }
    }

    /**
     * @return JsonResponse
     */
    public function getLabs()
    {
        $labs = Laboratorio::query()
            ->orderBy('descripcion')
            ->get();

        return response()->json($labs);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function getHistoryLabs($id)
    {
        $User = User::with('Analistas')->where("id","=",$id)->first();
        return response()->json($User);
    }
}
