<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;

use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use Illuminate\Http\Request;
use Epidemiologia\Models\MetodoReferencia;
use App\User;

/**
 * Class MuestraController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class MetodoReferenciaController extends Controller
{
    use ExceptionError;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function index()
    {
        $MetodoReferencia = MetodoReferencia::with('diagnostico')->get();
        return response()->json($MetodoReferencia);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $MetodoReferencia = MetodoReferencia::find($id);
        return response()->json($MetodoReferencia);
        // $muestra = MuestraEpidemiologica::with('pacientes')->with('diagnosticos')->where('id',$id)->first();
        // return ok($muestra);
    }

    /**
     * @param RequestMuestra $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $MetodoReferencia = MetodoReferencia::create(
                [
                    'descripcion'=>$request->descripcion,
                    'diagnostico_id'=>$request->diagnostico,
                ]
            );
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    public function getMetRef($diag_id)
    {
        $MetodoReferencia = MetodoReferencia::where('diagnostico_id',$diag_id)->get();
        return response()->json($MetodoReferencia);
    }

    /**
     * @param RequestMuestra $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $MetodoReferencia = MetodoReferencia::find($id);
            $MetodoReferencia->descripcion = $request->descripcion;
            $MetodoReferencia->diagnostico_id = $request->diagnostico;
            $MetodoReferencia->save();
            \DB::commit();
            return ok();
        } catch (\Throwable  $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    public function destroy($id)
    {

    }
}
