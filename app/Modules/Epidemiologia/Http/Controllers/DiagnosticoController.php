<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;

use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use Illuminate\Http\Request;
use Epidemiologia\Models\CatDiagnostico;
use App\User;

/**
 * Class MuestraController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class DiagnosticoController extends Controller
{
    use ExceptionError;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function index()
    {
        $catDiagnostico = CatDiagnostico::all();
        return response()->json($catDiagnostico);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $catDiagnostico = CatDiagnostico::find($id);
        return response()->json($catDiagnostico);
        // $muestra = MuestraEpidemiologica::with('pacientes')->with('diagnosticos')->where('id',$id)->first();
        // return ok($muestra);
    }

    public function getSearch($term)
    {
        $catDiagnostico = CatDiagnostico::where('descripcion','like','%'.$term.'%')->get();
        return response()->json($catDiagnostico);
        // dd($catDiagnostico);
    }

    /**
     * @param RequestMuestra $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $catDiagnostico = CatDiagnostico::create(['descripcion'=>$request->descripcion]);
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    /**
     * @param RequestMuestra $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $catDiagnostico = CatDiagnostico::find($id);
            $catDiagnostico->descripcion = $request->descripcion;
            $catDiagnostico->save();
            \DB::commit();
            return ok();
        } catch (\Throwable  $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    public function destroy($id)
    {

    }
}
