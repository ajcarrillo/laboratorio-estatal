<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;

use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use Illuminate\Http\Request;
use App\Models\Clue as Clue;
use Epidemiologia\Http\Requests\Muestras as RequestMuestra;
use Epidemiologia\Http\Resources\Jurisdiccion as JurisdiccionRe;
use Epidemiologia\Http\Resources\Diagnosticos as DiagnosticosRe;
use Epidemiologia\Http\Resources\UnidadesClue as UnidadesClueRe;
use Epidemiologia\Http\Resources\Pacientes as PacientesRe;
use Epidemiologia\Http\Resources\Muestra as MuestraRe;
use Epidemiologia\Models\MuestraEpidemiologica;
use Epidemiologia\Models\Diagnostico;
use Epidemiologia\Models\Resultado;
use Epidemiologia\Models\Referencia;
use App\Models\Paciente;
use App\Models\Revision;
use App\Models\Domicilio;
use App\Models\Genero;
use App\User;

/**
 * Class MuestraController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class MuestraController extends Controller
{
    use ExceptionError;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function index()
    {
        if (!$this->checkUserPermission('CAPTURISTA EPIDEMIOLOGIA',true)) {
            return;
        }
        $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                ->with('unidadMedica')
                ->with('pacientes')
                ->with('diagnosticos')
                ->get();
        return MuestraRe::collection($muestra);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function listAnalista()
    {
        if (!$this->checkUserPermission('ANALISTA EPIDEMIOLOGIA',true)) {
            return;
        }
         $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                    ->with('unidadMedica')
                    ->with('pacientes')
                    ->with('diagnosticos')
                    ->where('analista_id',get_user_from_api_guard()->id)
                    ->get();
        return MuestraRe::collection($muestra);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function searchList(Request $request)
    {
        if (!$this->checkUserPermission('CAPTURISTA EPIDEMIOLOGIA',true)) {
            return;
        }
        $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                ->with('unidadMedica')
                ->with('pacientes')
                ->with('diagnosticos');
        if ($request->solo) {
            $muestra->whereHas('statuses',function($q){
                    $q->Where('usuario_apertura_id', '=', get_user_from_api_guard()->id);
                });
        }
        if (!empty($request->date_start)) {
            if (!empty($request->date_end)) {
                $muestra->whereBetween('fecha_recepcion',[$request->date_start,$request->date_end]);
            } else {
                $muestra->where('fecha_recepcion','>',$request->date_start);
            }
        }
        return MuestraRe::collection($muestra->get());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function searchListAnalista(Request $request)
    {
        if (!$this->checkUserPermission('ANALISTA EPIDEMIOLOGIA',true)) {
            return;
        }
        $muestra = MuestraEpidemiologica::with('unidadAdministrativa')
                ->with('unidadMedica')
                ->with('pacientes')
                ->with('diagnosticos');
        if (!empty($request->date_start)) {
            if (!empty($request->date_end)) {
                $muestra->whereBetween('fecha_resultados',[$request->date_start,$request->date_end]);
            } else {
                $muestra->where('fecha_resultados','>',$request->date_start);
            }
        }
        $muestra->where('analista_id',get_user_from_api_guard()->id);
        return MuestraRe::collection($muestra->get());
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $muestra = MuestraEpidemiologica::with(['pacientes','diagnosticos','tipomuestra'])->where('id',$id)->first();
        return ok($muestra);
    }

    /**
     * @param RequestMuestra $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(RequestMuestra $request)
    {
        \DB::beginTransaction();
        try {
            $this->checkUserPermission('CAPTURISTA EPIDEMIOLOGIA');
            $paciente_id = $request->paciente_id;
            $muestraFields = $request->all();
            if (empty($paciente_id)) {
                $pacienteFields = $request->paciente;
                $paciente = Paciente::create($pacienteFields);
                $paciente_id = $paciente->id;

                $domicilio =  new Domicilio(['direccion'=>$request->direccion,'codigo_postal'=>$request->codigo_postal]);
                $paciente->domicilio()->save($domicilio);
                unset($muestraFields['paciente']);
            }
            $muestraFields['paciente_id'] = $paciente_id;
            $Muestra = MuestraEpidemiologica::create($muestraFields);
            $resivision =  new Revision(['fecha_apertura'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s'),'usuario_apertura_id'=>get_user_from_api_guard()->id,'estatus'=>'RECEPCIONADA']);

            $Muestra->statuses()->save($resivision);
            \DB::commit();
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    /**
     * @param RequestMuestra $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(RequestMuestra $request, $id)
    {
        \DB::beginTransaction();
        try {
            $this->checkUserPermission('CAPTURISTA EPIDEMIOLOGIA');
            $muestras = MuestraEpidemiologica::find($id);
            $paciente_id = $request->paciente_id;
            if (empty($paciente_id)) {
                $pacienteFields = $request->paciente;
                $paciente = Paciente::create($pacienteFields);
                $paciente_id = $paciente->id;

                $domicilio =  new Domicilio(['direccion'=>$request->direccion,'codigo_postal'=>$request->codigo_postal]);
                $paciente->domicilio()->save($domicilio);
            }
            $muestraFields = $request->all();
            $muestraFields['paciente_id'] = $paciente_id;
            unset($muestraFields['numero_muestra']);
            $muestras->update($muestraFields);
            \DB::commit();
            return ok();
        } catch (\Throwable  $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    public function destroy($id)
    {

    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeEstatus(Request $request,$id)
    {
        try {
            $this->checkUserPermission('ANALISTA EPIDEMIOLOGIA');
            $Muestra = MuestraEpidemiologica::find($id);
            $estatus = $request->estatus;
            $resivision =  new Revision(['fecha_apertura'=>\Carbon\Carbon::now()->format('Y-m-d H:i:s'),'usuario_apertura_id'=>get_user_from_api_guard()->id,'estatus'=>$estatus]);
            $Muestra->statuses()->save($resivision);
            return ok();
        } catch (\Throwable  $e) {
            return $this->throwableEx($e);
        }

    }

    /**
     * @param string $role
     * @param bool $bool
     * @return bool
     * @throws \Exception
     */
    private function checkUserPermission($role="",$bool = false)
    {
        $user = User::find(get_user_from_api_guard()->id);
        if (!$user->hasRole($role)) {
            if ($bool){
                return false;
            }
            throw new \Exception("No tienes permisos para realizar esta acción", 1);
        }
        return true;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getJurisdiccion()
    {
        $juris = Clue::where('cve_entidad','=',23)->where('nombre_unidad','like','%JURISDICCIÓN SANITARIA%')->get();
        return JurisdiccionRe::collection($juris);
    }

    /**
     * @param $cve_id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getUnidades($cve_id)
    {
        $cve_juris = Clue::find($cve_id);
        $juris = Clue::where('cve_entidad','=',23)->where('cve_jurisdiccion','=',$cve_juris->cve_jurisdiccion)->get();
        return UnidadesClueRe::collection($juris);
    }

    /**
     * @param $search
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getDiagnosticos($search)
    {
        if (empty($search)) {
            $diag = \DB::table('diagnosticos as CC')->where('CC.cie_categoria','4')
                ->join('diagnosticos as C19','C19.clave','=','CC.cie_superior')
                ->orderBy('CC.clave','asc')
                ->select('CC.id',\DB::raw('CONCAT(CC.clave," - ",CC.descripcion) as label'),'CC.descripcion','CC.lsex','CC.linf','CC.lsup','CC.cie_superior','C19.descripcion as parent',\DB::raw('CONCAT(C19.clave," - ",C19.descripcion) as parent_label'), \DB::raw('CONCAT(C19.clave," - ",C19.descripcion," - ",CC.clave," - ",CC.descripcion) as buscar'))  
                ->get();
        } else {
            $diag = \DB::table('diagnosticos as CC')->where('CC.cie_categoria','4')
                ->join('diagnosticos as C19','C19.clave','=','CC.cie_superior')
                ->orderBy('CC.clave','asc')
                ->select('CC.id',\DB::raw('CONCAT(CC.clave," - ",CC.descripcion) as label'),'CC.descripcion','CC.lsex','CC.linf','CC.lsup','CC.cie_superior','C19.descripcion as parent',\DB::raw('CONCAT(C19.clave," - ",C19.descripcion) as parent_label'), \DB::raw('CONCAT(C19.clave," - ",C19.descripcion," - ",CC.clave," - ",CC.descripcion) as buscar'))  
                ->having('buscar','like','%'.$search.'%')
                ->get();
        }
        return DiagnosticosRe::collection($diag);

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAnalistas()
    {
        $analistas = User::select('id',\DB::raw('CONCAT(titulo,nombre_apellidos) as nombre_completo'))->whereHas('Analistas',function($q){
                $q->Where('activo', '=', 1);
            })->whereHas('roles',function($q){
                $q->where('name', 'ANALISTA EPIDEMIOLOGIA');
            })->get();
        return response()->json($analistas);
    }

    /**
     * @param $nombre
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getPacientes($nombre)
    {
        $paciente = Paciente::where('nombre_apellidos','like','%'.$nombre.'%')->get();
        return PacientesRe::collection($paciente);
    }

    /**
     * @param $diagnostico_id
     * @param $search
     * @return \Illuminate\Http\JsonResponse
     */
    public function getResultado($diagnostico_id,$search)
    {
        $result = Resultado::where('descripcion','like','%'.$search.'%')->where('diagnostico_id',$diagnostico_id)->get();
        return response()->json($result);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMetodo()
    {
        $ref = Referencia::all();
        return response()->json($ref);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGenero()
    {
        $gereno = Genero::all();
        return response()->json($gereno);
    }
}
