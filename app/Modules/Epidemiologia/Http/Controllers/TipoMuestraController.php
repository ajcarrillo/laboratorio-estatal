<?php

namespace Epidemiologia\Http\Controllers\Epidemiologia;

use App\Http\Controllers\Controller;
use App\Traits\ExceptionError;
use Illuminate\Http\Request;
use Epidemiologia\Models\TipoMuestra;
use App\User;

/**
 * Class MuestraController
 * @package Epidemiologia\Http\Controllers\Epidemiologia
 */
class TipoMuestraController extends Controller
{
    use ExceptionError;

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection|void
     * @throws \Exception
     */
    public function index()
    {
        $TipoMuestra = TipoMuestra::all();
        return response()->json($TipoMuestra);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $TipoMuestra = TipoMuestra::find($id);
        return response()->json($TipoMuestra);
        // $muestra = MuestraEpidemiologica::with('pacientes')->with('diagnosticos')->where('id',$id)->first();
        // return ok($muestra);
    }

    public function search($term)
    {
        $TipoMuestra = TipoMuestra::where('descripcion','like','%'.$term.'%')->get();
        return response()->json($TipoMuestra);
    }

    /**
     * @param RequestMuestra $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        \DB::beginTransaction();
        try {
            \DB::commit();
            $TipoMuestra = TipoMuestra::create(['descripcion'=>$request->descripcion]);
            return ok();
        } catch (\Throwable $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    /**
     * @param RequestMuestra $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {
        \DB::beginTransaction();
        try {
            $TipoMuestra = TipoMuestra::find($id);
            $TipoMuestra->descripcion = $request->descripcion;
            $TipoMuestra->save();
            \DB::commit();
            return ok();
        } catch (\Throwable  $e) {
            \DB::rollback();
            return $this->throwableEx($e);
        }
    }

    public function destroy($id)
    {

    }
}
