<?php

namespace Epidemiologia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Laboratorio
 * @package Epidemiologia\Http\Requests
 */
class Laboratorio extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'descripcion' => 'string',
            'responsable' => 'int',
            'tipo' => 'int',
        ];
    }
}
