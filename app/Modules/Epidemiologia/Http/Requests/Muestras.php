<?php

namespace Epidemiologia\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class Muestras
 * @package Epidemiologia\Http\Requests
 */
class Muestras extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    /**
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance()
    {
        $numero_muestra = \Carbon\Carbon::now()->format('y').\Carbon\Carbon::now()->format('m').\Carbon\Carbon::now()->format('d').\Carbon\Carbon::now()->format('H').\Carbon\Carbon::now()->format('i').\Carbon\Carbon::now()->format('s');

        $data = $this->all();
        $fields["unidad_administrativa_id"] = $data["jurisdiccion"];
        $fields["unidad_medica_id"]         = $data["unidad"];
        $fields["numero_muestra"]           = $numero_muestra;
        $fields["diagnostico_id"]           = $data["diagnostico"];
        $fields["tipo_muestra_id"]          = $data["tip_muestra"];
        $fields["metodo_referencia"]        = json_encode($data["metodo_referencia"]);
        $fields["analista_id"]              = $data["analista"];
        $fields["fecha_toma"]               = $data["fecha_toma"];
        $fields["fecha_recepcion"]          = $data["fecha_recepcion"];
        $fields["paciente_id"]              = "";
        if (isset($data["paciente"])) {
            $fields["paciente_id"] = $data["paciente"];
        } else {
            $fields["direccion"]              = $data["direccion"];
            $fields["codigo_postal"]          = (isset($data["cpp"])?$data["cpp"]:"");
            $fields['paciente']['nombre'] = $data["nombre_paciente"];
            $fields['paciente']['apellidos'] = $data["ap"];
            $fields['paciente']['nombre_apellidos'] = $data["nombre_paciente"]." ".$data["ap"];
            $fields['paciente']['apellidos_nombre'] = $data["ap"]." ".$data["nombre_paciente"];
            $fields['paciente']['fecha_nacimiento'] = $data["fecha_nacimiento"];
            $fields['paciente']['edad'] = \Carbon\Carbon::parse($data["fecha_nacimiento"])->age;
            $fields['paciente']['genero'] = $data["sexo"];
        }
        // $data['date_of_birth'] = 'test';
        $this->getInputSource()->replace($fields);

        /*modify data before send to validator*/

        return parent::getValidatorInstance();
    }
}
