<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:09 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Analista
 * @package Epidemiologia\Models
 */
class Analista extends Model
{
	protected $table = "analistas";

}
