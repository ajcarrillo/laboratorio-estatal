<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:09 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Resultado
 * @package Epidemiologia\Models
 */
class Resultado extends Model
{
	protected $table = "resultados";
	protected $fillable = ['diagnostico_id','descripcion','hash'];

}
