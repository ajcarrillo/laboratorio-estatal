<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:04 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Diagnostico
 * @package Epidemiologia\Models
 */
class CatDiagnostico extends Model
{
	protected $table    = 'cat_diagnosticos';
	protected $fillable = ['descripcion'];

}
