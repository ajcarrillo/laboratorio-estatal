<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:04 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Diagnostico
 * @package Epidemiologia\Models
 */
class MetodoReferencia extends Model
{
	protected $table    = 'metodos_referencias';
	protected $fillable = ['descripcion','diagnostico_id'];

	/**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function diagnostico()
	{
		return $this->belongsTo('Epidemiologia\Models\CatDiagnostico','diagnostico_id','id');
	}

}
