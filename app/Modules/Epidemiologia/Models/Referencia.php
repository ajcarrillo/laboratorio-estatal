<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:09 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Referencia
 * @package Epidemiologia\Models
 */
class Referencia extends Model
{
	protected $table = "metodos_referencias";
	protected $fillable = ['descripcion'];
}
