<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:08 p. m.
 */

namespace Epidemiologia\Models;

use App\Models\Revision;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MuestraEpidemiologica
 * @package Epidemiologia\Models
 */
class MuestraEpidemiologica extends Model
{
	protected $table = "muestras_epidemiologicas" ;
	protected $fillable = ['unidad_administrativa_id','unidad_medica_id','numero_muestra','diagnostico_id','analista_id','fecha_toma','fecha_recepcion','paciente_id','diagnostico_id','metodo_referencia','tipo_muestra_id'];

	protected        $appends   = [
        'status','usuario_apertura'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
	public function statuses()
    {
        return $this->morphMany(Revision::class, 'reviewable');
    }

    /**
     * @return mixed
     */
    public function getStatusAttribute()
    {
        $estatus = $this->statuses()
            ->orderBy('fecha_apertura','desc')
            ->first();

        return $estatus->estatus;
    }

    /**
     * @return mixed
     */
     public function getUsuarioAperturaAttribute()
    {
        $estatus = $this->statuses()
            ->orderBy('fecha_apertura','desc')
            ->first();

        return $estatus->usuario_apertura_id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unidadAdministrativa()
	{
		return $this->belongsTo('App\Models\Clue','unidad_administrativa_id','id');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function unidadMedica()
	{
		return $this->belongsTo('App\Models\Clue','unidad_medica_id','id');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function diagnosticos()
	{
		return $this->belongsTo('Epidemiologia\Models\CatDiagnostico','diagnostico_id','id');
	}

    public function tipomuestra()
    {
        return $this->belongsTo('Epidemiologia\Models\TipoMuestra','tipo_muestra_id','id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function pacientes()
	{
		return $this->belongsTo('App\Models\Paciente','paciente_id','id');
	}

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
	public function referencia()
	{
		return $this->belongsTo('Epidemiologia\Models\Referencia','metodo_referencia_id','id');
	}

}
