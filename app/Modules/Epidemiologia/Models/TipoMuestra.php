<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:04 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Diagnostico
 * @package Epidemiologia\Models
 */
class TipoMuestra extends Model
{
	protected $table    = 'tipos_muestra';
	protected $fillable = ['descripcion'];

}
