<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:04 p. m.
 */

namespace Epidemiologia\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class Diagnostico
 * @package Epidemiologia\Models
 */
class Diagnostico extends Model
{
	protected $table    = 'diagnosticos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
	public function resultado()
	{
		return $this->hasOne('Epidemiologia\Models\Referencia', 'diagnostico_id');
	}
}
