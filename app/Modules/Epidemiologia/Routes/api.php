<?php

use Illuminate\Support\Facades\Route;

// DB::listen(function ($query){
//     Log::info($query->sql);
//     Log::info($query->bindings);
// });


Route::post('epidemiologia/muestra/estatus/{id}', 'Epidemiologia\MuestraController@changeEstatus');
Route::get('epidemiologia/muestra/jurisdicciones', 'Epidemiologia\MuestraController@getJurisdiccion');
Route::get('epidemiologia/muestra/diagnosticos/{search}', 'Epidemiologia\MuestraController@getDiagnosticos');
Route::get('epidemiologia/diagnostico/search/{search}', 'Epidemiologia\DiagnosticoController@getSearch');
Route::get('epidemiologia/metodosferencia/searchdiag/{search}', 'Epidemiologia\MetodoReferenciaController@getMetRef');
Route::get('epidemiologia/tipomuestra/search/{search}', 'Epidemiologia\TipoMuestraController@search');
Route::get('epidemiologia/muestra/unidades/{cve}', 'Epidemiologia\MuestraController@getUnidades');
Route::get('epidemiologia/muestra/analista', 'Epidemiologia\MuestraController@getAnalistas');
Route::get('epidemiologia/muestra/pacientes/{nombre}', 'Epidemiologia\MuestraController@getPacientes');
Route::get('epidemiologia/muestra/genero', 'Epidemiologia\MuestraController@getGenero');
Route::get('epidemiologia/muestra/lista/analista', 'Epidemiologia\MuestraController@listAnalista');
Route::get('epidemiologia/muestra/resultado/{diagnostico_id}/{search}', 'Epidemiologia\MuestraController@getResultado');
Route::get('epidemiologia/muestra/referencias', 'Epidemiologia\MuestraController@getMetodo');
Route::post('epidemiologia/muestra/lisfilters', 'Epidemiologia\MuestraController@searchList');

Route::post('epidemiologia/muestra/lisfilters/analista', 'Epidemiologia\MuestraController@searchListAnalista');

Route::post('epidemiologia/responsable/estatus/{id}', 'Epidemiologia\ResponsableController@changeEstatus');
Route::post('epidemiologia/jefe/estatus/{id}', 'Epidemiologia\JefeController@changeEstatus');
Route::post('epidemiologia/jefe/search/date', 'Epidemiologia\JefeController@searchByDate');
Route::post('epidemiologia/jefe/search/year', 'Epidemiologia\JefeController@searchByYear');

Route::apiResource('epidemiologia/metodosferencia', 'Epidemiologia\MetodoReferenciaController');
Route::apiResource('epidemiologia/tipomuestra', 'Epidemiologia\TipoMuestraController');
Route::apiResource('epidemiologia/diagnostico', 'Epidemiologia\DiagnosticoController');
Route::apiResource('epidemiologia/resultado', 'Epidemiologia\ResultadoController');
Route::apiResource('epidemiologia/laboratorio', 'Epidemiologia\LaboratorioController');
Route::apiResource('epidemiologia/analista', 'Epidemiologia\AnalistaController');
Route::apiResource('epidemiologia/muestra', 'Epidemiologia\MuestraController');
Route::apiResource('epidemiologia/responsable', 'Epidemiologia\ResponsableController');
Route::apiResource('epidemiologia/jefe', 'Epidemiologia\JefeController');

Route::post('epidemiologia/laboratorio/responsables', 'Epidemiologia\LaboratorioController@getResponsables');
Route::post('epidemiologia/analista/laboratorios', 'Epidemiologia\AnalistaController@getLabs');
Route::get('epidemiologia/analista/historial/{id}', 'Epidemiologia\AnalistaController@getHistoryLabs');