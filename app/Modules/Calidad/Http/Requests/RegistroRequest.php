<?php

namespace Calidad\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class RegistroRequest
 * @package Calidad\Http\Requests
 */
class RegistroRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'medio_proteccion'=> '',
            'medio_disposicion'=> '',
            'medio_almacenamiento'=> '',
            'medio_soporte'=> '',
            'medio_recuperacion'=> '',
        ];
    }
}
