<?php

namespace Calidad\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class NoConformidadRequest
 * @package Calidad\Http\Requests
 */
class NoConformidadRequest extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
}
