<?php

namespace Calidad\Http\Controllers;

use Calidad\Http\Controllers\CalidadBaseController;
use Calidad\Http\Requests\RegistroRequest;
use Calidad\Models\Archivo;
use Calidad\Models\Registro;
use Calidad\Http\Resources\RegistroResource;

/**
 * Class RegistrosController
 * @package Calidad\Http\Controllers
 */
class RegistrosController extends CalidadBaseController
{
    /**
     * @var string
     */
    private $model = 'Calidad\Models\Registro';

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return RegistroResource::collection(Registro::all());
    }

    /**
     * @param $id
     * @return RegistroResource
     */
    public function show($id)
    {
        $item = Registro::find($id);

        return new RegistroResource($item);
    }

    /**
     * @param RegistroRequest $request
     * @return RegistroResource
     */
    public function store(RegistroRequest $request)
    {
        $item = Registro::create($request->except('file', 'fileUrl', 'file_upload'));
        // Save file
        if ($request->file_upload) {
            $file = $request->file('file_upload');
            $this->storeFile($file, $item);
        }

        return new RegistroResource($item);
    }

    /**
     * @param RegistroRequest $request
     * @param $id
     * @return RegistroResource
     */
    public function update(RegistroRequest $request, $id)
    {
        $item = Registro::find($id);
        $item->update($request->except('file', 'file_upload'));

        if ($request->file('file_upload')) {
            $file = $request->file('file_upload');
            $this->storeFile($file, $item);
        }

        return new RegistroResource($item);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Registro::find($id);
        // Buscar archivo anterior
        $this->deleteFile($item);

        return response()->json($item->delete(), 202);
    }

    /**
     * @param $item
     */
    private function deleteFile($item)
    {
        $oldFile = Archivo::sOrigen($this->model, $item->id)->first();
        if ($oldFile) {
            // Si se encuentra, eliminar registro y archivo
            if (file_exists(storage_path() . "/" . $oldFile->path)) {
                unlink(storage_path() . "/" . $oldFile->path);
                $oldFile->delete();
            }
        }
    }

    /**
     * @param $file
     * @param $item
     * @return bool
     */
    private function storeFile($file, $item)
    {
        $this->deleteFile($item);
        $path = 'app/calidad';
        $filename        = $file->getClientOriginalName();
        $destinationPath = storage_path($path);

        // Crear archivo en tabla
        $archivo                  = new Archivo();
        $archivo->folder          = $path;
        $archivo->filename        = $filename;
        $archivo->path            = $path ."/". $filename;
        $archivo->type            = $file->getMimeType();
        $archivo->mime            = $file->getClientMimeType();
        $archivo->size            = $file->getSize();
        $archivo->origenable_type = $this->model;
        $archivo->origenable_id   = $item->id;
        $archivo->save();

        // Mover archivo al storage
        $file->move($destinationPath, $filename);
        return true;
    }
}
