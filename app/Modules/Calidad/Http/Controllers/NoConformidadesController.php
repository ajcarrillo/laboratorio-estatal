<?php

namespace Calidad\Http\Controllers;

use Calidad\Http\Controllers\CalidadBaseController;
use Calidad\Http\Requests\NoConformidadRequest;
use Calidad\Http\Resources\NoConformidadResource;
use Calidad\Models\AccionNC;
use Calidad\Models\Archivo;
use Calidad\Models\ClasificacionNC;
use Calidad\Models\Departamento;
use Calidad\Models\FuenteNC;
use Calidad\Models\NoConformidad;
use Calidad\Http\Resources\DocumentoResource;
use Illuminate\Http\Request;

/**
 * Class NoConformidadesController
 * @package Calidad\Http\Controllers
 */
class NoConformidadesController extends CalidadBaseController
{
    /**
     * @var string
     */
    private $model = 'Calidad\Models\NoConformidad';

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return NoConformidadResource::collection(NoConformidad::all());
    }

    /**
     * @param $id
     * @return NoConformidadResource()
     */
    public function show($id)
    {
        $item = NoConformidad::find($id);

        return new NoConformidadResource($item);
    }

    /**
     * @param NoConformidadRequest $request
     * @return NoConformidadResource()
     */
    public function store(NoConformidadRequest $request)
    {
        $item = NoConformidad::create($request->except('file', 'acciones'));

        foreach ($request->input('acciones', []) as $accionR) {
            $accion = AccionNC::create($accionR);
            $item->acciones()->save($accion);
        }

        return new NoConformidadResource($item);
    }

    /**
     * @param NoConformidadRequest $request
     * @param $id
     * @return NoConformidadResource()
     */
    public function update(NoConformidadRequest $request, $id)
    {
        $item = NoConformidad::find($id);
        $item->update($request->except('file', 'acciones'));

        $item->acciones()->delete();

        foreach ($request->input('acciones', []) as $accionR) {
            $accion = AccionNC::create($accionR);
            $item->acciones()->save($accion);
        }

        /*if ($request->file('file_upload')) {
            $file = $request->file('file_upload');
            $this->storeFile($file, $item);
        }*/

        return new NoConformidadResource($item);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = NoConformidad::find($id);
        // Buscar archivo anterior
        $this->deleteFile($item);

        return response()->json($item->delete(), 202);
    }

    /**
     * @param $item
     */
    private function deleteFile($item)
    {
        $oldFile = Archivo::sOrigen($this->model, $item->id)->first();
        if ($oldFile) {
            // Si se encuentra, eliminar registro y archivo
            if (file_exists(storage_path() . "/" . $oldFile->path)) {
                unlink(storage_path() . "/" . $oldFile->path);
                $oldFile->delete();
            }
        }
    }

    /**
     * @param $file
     * @param $item
     * @return bool
     */
    private function storeFile($file, $item)
    {
        $this->deleteFile($item);
        $path            = 'app/calidad';
        $filename        = $file->getClientOriginalName();
        $destinationPath = storage_path($path);

        // Crear archivo en tabla
        $archivo                  = new Archivo();
        $archivo->folder          = $path;
        $archivo->filename        = $filename;
        $archivo->path            = $path . "/" . $filename;
        $archivo->type            = $file->getMimeType();
        $archivo->mime            = $file->getClientMimeType();
        $archivo->size            = $file->getSize();
        $archivo->origenable_type = $this->model;
        $archivo->origenable_id   = $item->id;
        $archivo->save();

        // Mover archivo al storage
        $file->move($destinationPath, $filename);

        return true;
    }

    public function getSources()
    {
        $data["clasificaciones"] = ClasificacionNC::all()->map(function ($item) {
            return [ 'text' => $item->descripcion . " (" . $item->clave . ")", 'value' => $item->id ];
        });

        $data["fuentes"] = FuenteNC::all()->map(function ($item) {
            return [ 'text' => $item->descripcion . " (" . $item->clave . ")", 'value' => $item->id ];
        });

        $data["areas"] = Departamento::all()->map(function ($item) {
            return [ 'text' => $item->descripcion . " (" . $item->clave . ")", 'value' => $item->id ];
        });

        return $data;
    }

    public function evidencias(Request $request)
    {
        dd($request->all());

    }
}
