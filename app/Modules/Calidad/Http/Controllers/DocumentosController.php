<?php

namespace Calidad\Http\Controllers;

use Calidad\Http\Controllers\CalidadBaseController;
use Calidad\Http\Requests\DocumentoRequest;
use Calidad\Models\Archivo;
use Calidad\Models\Documento;
use Calidad\Http\Resources\DocumentoResource;

/**
 * Class DocumentosController
 * @package Calidad\Http\Controllers
 */
class DocumentosController extends CalidadBaseController
{
    /**
     * @var string
     */
    private $model = 'Calidad\Models\Documento';

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        return DocumentoResource::collection(Documento::all());
    }

    /**
     * @param $id
     * @return DocumentoResource()
     */
    public function show($id)
    {
        $item = Documento::find($id);

        return new DocumentoResource($item);
    }

    /**
     * @param DocumentoRequest $request
     * @return DocumentoResource()
     */
    public function store(DocumentoRequest $request)
    {
        $item = Documento::create($request->except('file', 'fileUrl', 'file_upload'));
        // Save file
        if ($request->file_upload) {
            $file = $request->file('file_upload');
            $this->storeFile($file, $item);
        }

        return new DocumentoResource($item);
    }

    /**
     * @param DocumentoRequest $request
     * @param $id
     * @return DocumentoResource()
     */
    public function update(DocumentoRequest $request, $id)
    {
        $item = Documento::find($id);
        $item->update($request->except('file', 'file_upload'));

        if ($request->file('file_upload')) {
            $file = $request->file('file_upload');
            $this->storeFile($file, $item);
        }

        return new DocumentoResource($item);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $item = Documento::find($id);
        // Buscar archivo anterior
        $this->deleteFile($item);

        return response()->json($item->delete(), 202);
    }

    /**
     * @param $item
     */
    private function deleteFile($item)
    {
        $oldFile = Archivo::sOrigen($this->model, $item->id)->first();
        if ($oldFile) {
            // Si se encuentra, eliminar registro y archivo
            if (file_exists(storage_path() . "/" . $oldFile->path)) {
                unlink(storage_path() . "/" . $oldFile->path);
                $oldFile->delete();
            }
        }
    }

    /**
     * @param $file
     * @param $item
     * @return bool
     */
    private function storeFile($file, $item)
    {
        $this->deleteFile($item);
        $path            = 'app/calidad';
        $filename        = $file->getClientOriginalName();
        $destinationPath = storage_path($path);

        // Crear archivo en tabla
        $archivo                  = new Archivo();
        $archivo->folder          = $path;
        $archivo->filename        = $filename;
        $archivo->path            = $path . "/" . $filename;
        $archivo->type            = $file->getMimeType();
        $archivo->mime            = $file->getClientMimeType();
        $archivo->size            = $file->getSize();
        $archivo->origenable_type = $this->model;
        $archivo->origenable_id   = $item->id;
        $archivo->save();

        // Mover archivo al storage
        $file->move($destinationPath, $filename);

        return true;
    }
}
