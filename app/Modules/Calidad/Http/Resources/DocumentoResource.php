<?php

namespace Calidad\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class DocumentoResource
 * @package Calidad\Http\Resources
 */
class DocumentoResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                     => $this->id,
            'clave'                  => $this->clave,
            'nombre'                 => $this->nombre,
            'revision'               => $this->revision,
            'fecha_proxima_revision' => ($this->fecha_proxima_revision),
            'estatus'                => $this->estatus,
            'copias_impresas'        => $this->copias_impresas,
            'medio_soporte'          => $this->medio_soporte,
            'medio_proteccion'       => $this->medio_proteccion,
            'medio_almacenamiento'   => $this->medio_almacenamiento,
            'medio_recuperacion'     => $this->medio_recuperacion,
            'medio_disposicion'      => $this->medio_disposicion,
            'responsable'            => $this->responsable,
            'tiempo_retencion'       => $this->tiempo_retencion,
            'file'                   => $this->file,
        ];
    }
}
