<?php

namespace Calidad\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * Class NoConformidadResource
 * @package Calidad\Http\Resources
 */
class NoConformidadResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'clave'            => $this->clave,
            'hallazgo'         => $this->hallazgo,
            'clasificacion_id' => $this->clasificacion_id,
            'clasificacion'    => ($this->clasificacion)?$this->clasificacion->descripcion:'',
            'fuente_id'        => $this->fuente_id,
            'fuente'           => ($this->fuente)?$this->fuente->descripcion:'',
            'departamento_id'  => $this->departamento_id,
            'departamento'     => ($this->departamento)?$this->departamento->descripcion:'',
            'area_id'          => $this->area_id,
            'area'             => ($this->area)?$this->area->descripcion:'',
            'fecha_deteccion'  => $this->fecha_deteccion,

            'fecha_acciones'   => $this->fecha_acciones,
            'observaciones'    => $this->observaciones,
            'resolucion'       => $this->resolucion,
            'fecha_cierre'     => $this->fecha_cierre,
            'evidencias'       => $this->files,

            'acciones'         => $this->acciones,
        ];
    }
}
