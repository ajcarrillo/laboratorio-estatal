<?php

Route::apiResource('calidad/registros', 'RegistrosController');
Route::apiResource('calidad/documentos', 'DocumentosController');
Route::apiResource('calidad/no_conformidades', 'NoConformidadesController');

Route::post('calidad/no_conformidades/sources', 'NoConformidadesController@getSources');

Route::post('calidad/no_conformidades/evidencias', 'NoConformidadesController@evidencias');
