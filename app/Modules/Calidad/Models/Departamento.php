<?php

namespace Calidad\Models;

/**
 * Class Departamento
 * @package Calidad\Models
 */
class Departamento extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'departamentos';

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return mixed
     */
    public function getOrigenTagAttribute()
    {
        return $this->origenable->getTag();
    }

    public function scopeSOrigen($query, $model, $id)
    {
        return $query->where('origenable_type', $model)->where('origenable_id', $id);
    }
}
