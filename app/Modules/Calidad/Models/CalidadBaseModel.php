<?php


namespace Calidad\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CalidadBaseModel
 * @package Calidad\Models
 */
class CalidadBaseModel  extends Model
{
    /**
     * @var string
     */
    protected $connection = 'calidad';
}
