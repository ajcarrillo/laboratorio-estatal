<?php

namespace Calidad\Models;

use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class NoConformidad
 * @package Calidad\Models
 */
class NoConformidad extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = ['clasificacion', 'fuente', 'area', 'departamento', 'fecha_acciones', 'observaciones', 'evidencias'];
    /**
     * @var string
     */
    protected $table = 'no_conformidades';

    //protected $appends = ['origen_tag'];

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return BelongsTo
     */
    /*public function Area()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_id', 'id');
    }*/

    /**
     * @return mixed
     */
    public function files()
    {
        return $this->hasMany('Calidad\Models\Archivo', 'origenable_id', 'id')
            ->whereOrigenableType(self::class);
    }

    /**
     * @return mixed
     */
    public function acciones()
    {
        return $this->hasMany('Calidad\Models\AccionNC', 'no_conformidad_id', 'id');
    }

    /**
     * @return mixed
     */
    public function clasificacion()
    {
        return $this->belongsTo('Calidad\Models\ClasificacionNC', 'clasificacion_id', 'id');
    }

    /**
     * @return mixed
     */
    public function fuente()
    {
        return $this->belongsTo('Calidad\Models\FuenteNC', 'fuente_id', 'id');
    }

    /**
     * @return mixed
     */
    public function departamento()
    {
        return $this->belongsTo('Calidad\Models\Departamento', 'departamento_id', 'id');
    }

    /**
     * @return mixed
     */
    public function area()
    {
        return $this->belongsTo('Calidad\Models\Departamento', 'area_id', 'id');
    }

}
