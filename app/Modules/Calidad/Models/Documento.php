<?php

namespace Calidad\Models;

/**
 * Class Documento
 * @package Calidad\Models
 */
class Documento extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'documentos';
    /**
     * @var array
     */

    /**
     * @param $value
     * @return array
     */

    public function getMedioDisposicionAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     * @return array
     */

    public function getMedioAlmacenamientoAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     * @return array
     */

    public function getMedioProteccionAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     * @return array
     */

    public function getMedioRecuperacionAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     * @return array
     */

    public function getMedioSoporteAttribute($value)
    {
        return explode(',', $value);
    }

    //protected $appends = ['origen_tag'];

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Area()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_id', 'id');
    }

    /**
     * @return mixed
     */
    public function file()
    {
        return $this->hasOne('Calidad\Models\Archivo', 'origenable_id', 'id')
            ->whereOrigenableType(self::class);
    }

}
