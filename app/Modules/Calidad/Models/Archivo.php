<?php

namespace Calidad\Models;

/**
 * Class Archivo
 * @package Calidad\Models
 */
class Archivo extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'archivos';

    //protected $appends = ['origen_tag'];

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return mixed
     */
    public function getOrigenTagAttribute()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Area()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function origenable()
    {
        return $this->morphTo();
    }

    public function scopeSOrigen($query, $model, $id)
    {
        return $query->where('origenable_type', $model)->where('origenable_id', $id);
    }
}
