<?php

namespace Calidad\Models;

/**
 * Class ClasificacionNC
 * @package Calidad\Models
 */
class ClasificacionNC extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'clasificaciones_nc';

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return mixed
     */
    public function getOrigenTagAttribute()
    {
        return $this->origenable->getTag();
    }

    public function scopeSOrigen($query, $model, $id)
    {
        return $query->where('origenable_type', $model)->where('origenable_id', $id);
    }
}
