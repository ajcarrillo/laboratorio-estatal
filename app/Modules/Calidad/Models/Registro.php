<?php

namespace Calidad\Models;

/**
 * Class Registro
 * @package Calidad\Models
 */
class Registro extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'registros';
    /**
     * @var array
     */
    /*
    protected $casts = [
        'medio_disposicion'    => 'array',
        'medio_almacenamiento' => 'array',
        'medio_proteccion'     => 'array',
        'medio_recuperacion'   => 'array',
        'medio_soporte'        => 'array',
    ];
    */
    /**
     * @param $value
     */
    /*public function setMedioDisposicionAttribute($value)
    {
        $this->attributes['medio_disposicion'] = implode(',', $value);
    }*/

    /**
     * @param $value
     * @return array
     */

    public function getMedioDisposicionAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     */
    /*
    public function setMedioAlmacenamientoAttribute($value)
    {
        $this->attributes['medio_almacenamiento'] = implode(',', (array)$value);
    }
    */

    /**
     * @param $value
     * @return array
     */

    public function getMedioAlmacenamientoAttribute($value)
    {
        return explode(',', $value);
    }


    /**
     * @param $value
     */
    /*
    public function setMedioProteccionAttribute($value)
    {
        $this->attributes['medio_proteccion'] = $value;
    }
    */

    /**
     * @param $value
     * @return array
     */

    public function getMedioProteccionAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     */
    /*
    public function setMedioRecuperacionAttribute($value)
    {
        $this->attributes['medio_recuperacion'] = implode(',', (array)$value);
    }
    */
    /**
     * @param $value
     * @return array
     */

    public function getMedioRecuperacionAttribute($value)
    {
        return explode(',', $value);
    }

    /**
     * @param $value
     */
    /*
    public function setMedioSoporteAttribute($value)
    {
        $this->attributes['medio_soporte'] = implode(',', (array)$value);
    }
    */
    /**
     * @param $value
     * @return array
     */

    public function getMedioSoporteAttribute($value)
    {
        return explode(',', $value);
    }

    //protected $appends = ['origen_tag'];

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Area()
    {
        return $this->belongsTo('Inventarios\Models\Area', 'area_id', 'id');
    }

    /**
     * @return mixed
     */
    public function file()
    {
        return $this->hasOne('Calidad\Models\Archivo', 'origenable_id', 'id')
            ->whereOrigenableType(self::class);
    }

}
