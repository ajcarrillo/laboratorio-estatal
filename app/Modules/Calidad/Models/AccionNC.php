<?php

namespace Calidad\Models;

/**
 * Class AccionNC
 * @package Calidad\Models
 */
class AccionNC extends CalidadBaseModel
{
    /**
     * @var array
     */
    protected $guarded = [];
    /**
     * @var string
     */
    protected $table = 'acciones_nc';

    /**
     * @return mixed
     */
    public function tag()
    {
        return $this->origenable->getTag();
    }

    /**
     * @return mixed
     */
    public function getOrigenTagAttribute()
    {
        return $this->origenable->getTag();
    }

    public function scopeSOrigen($query, $model, $id)
    {
        return $query->where('origenable_type', $model)->where('origenable_id', $id);
    }
}
