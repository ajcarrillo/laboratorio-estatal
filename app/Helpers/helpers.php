<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-07-27
 * Time: 16:24
 */

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable;

if ( ! function_exists('get_user')) {
    function get_user(): Authenticatable
    {
        return Auth::user();
    }
}

if ( ! function_exists('get_user_from_api_guard')) {
    function get_user_from_api_guard(): Authenticatable
    {
        return Auth::guard('api')->user();
    }
}

if ( ! function_exists('get_current_date')) {
    function get_current_date(): string
    {
        return Carbon::now()->format('Y-m-d');
    }
}
