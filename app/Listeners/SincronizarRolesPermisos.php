<?php

namespace App\Listeners;

use App\Events\CreatedUser;

class SincronizarRolesPermisos
{

    public function handle(CreatedUser $event)
    {
        $user = $event->user;

        $user->syncRoles($event->roles);
        $user->syncPermissions($event->permissions);
    }
}
