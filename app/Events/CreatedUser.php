<?php

namespace App\Events;

use App\User;

class CreatedUser
{
    public $user;
    public $roles;
    public $permissions;

    public function __construct(User $user, $roles, $permissions)
    {
        $this->user        = $user;
        $this->roles       = $roles;
        $this->permissions = $permissions;
    }
}
