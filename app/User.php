<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use RiesgosSanitarios\Models\MuestraSanitaria;
use RiesgosSanitarios\Models\Solicitud;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    protected $connection = 'mysql';

    use Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'name',
        'last_name',
        'email',
        'password',
        'nombre_apellidos',
        'apellidos_nombre',
        'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [ 'stringRoles' ];

    public function muestrasSanitarias()
    {
        return $this->hasMany(MuestraSanitaria::class, 'analista_id');
    }

    public function soliciutdesAnalisis()
    {
        return $this->hasMany(Solicitud::class, 'capturista_id');
    }

    public function Laboratorios()
    {
        return $this->belongsToMany('App\Models\Laboratorio', 'laboratorio_responsable', 'user_id', 'laboratorio_id');
    }

    public function Analistas()
    {
        return $this->belongsToMany('App\Models\Laboratorio', 'analistas', 'user_id', 'laboratorio_id')->withPivot('inicio', 'fin', 'activo', 'id');
    }

    public function muestras()
    {
        return $this->hasMany('Epidemiologia\Models\MuestraEpidemiologica', 'analista_id');
    }

    public function getStringRolesAttribute()
    {
        return $this->roles->implode('name', ', ');
    }
}
