<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/18/20
 * Time: 11:27 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'sistema_menus';

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'menu_role', 'menu_id', 'role_id');
    }

    public function childrenMenu()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function scopeParents($query)
    {
        $query->withCount('childrenMenu')
            ->whereNull('parent_id');
    }
}
