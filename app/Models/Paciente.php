<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:11 p. m.
 */

namespace App\Models;


use App\Traits\FilterBy;
use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    use FilterBy;

    protected $table    = 'pacientes';
    protected $fillable = [
        'expediente_id',
        'nombre',
        'apellidos',
        'nombre_apellidos',
        'apellidos_nombre',
        'fecha_nacimiento',
        'genero',
        'telefono',
        'email',
        'edad',
    ];

    public function domicilio()
    {
        return $this->morphOne(Domicilio::class, 'addressable')->withDefault();
    }

    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = mb_strtoupper($value, 'UTF-8');
    }

    public function setApellidosAttribute($value)
    {
        $this->attributes['apellidos'] = mb_strtoupper($value, 'UTF-8');
    }

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            self::concatNombreApellidos($model);
        });

        self::updating(function ($model) {
            self::concatNombreApellidos($model);
        });
    }

    protected static function concatNombreApellidos($model)
    {
        $model->nombre_apellidos = "{$model->nombre} {$model->apellidos}";
        $model->apellidos_nombre = "{$model->apellidos} {$model->nombre}";
    }
}
