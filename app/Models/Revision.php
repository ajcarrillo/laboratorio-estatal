<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:10 p. m.
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Model;

class Revision extends Model
{
    protected $table   = 'revisiones';
    protected $guarded = [];

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function usuarioApertura()
    {
        return $this->belongsTo(User::class, 'usuario_apertura_id');
    }

    public function usuarioRevision()
    {
        return $this->belongsTo(User::class, 'usuario_revision_id');
    }
}
