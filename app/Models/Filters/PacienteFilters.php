<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/1/19
 * Time: 6:33 p. m.
 */

namespace App\Models\Filters;


use App\QueryFilter;

class PacienteFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'nombreapellidos' => 'filled',
        ];
    }

    public function nombreapellidos($query, $nombreapellidos)
    {
        $query->where('nombre_apellidos', 'like', "%{$nombreapellidos}%")
            ->orWhere('apellidos_nombre', 'like', "%{$nombreapellidos}%")
            ->orWhere('email', 'like', "%{$nombreapellidos}%")
            ->orWhere('telefono', 'like', "%{$nombreapellidos}%");
    }
}
