<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:05 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Laboratorio extends Model
{
	protected $table = 'laboratorios';

	protected $fillable = ['descripcion', 'tipo'];

	public function Responsable()
	{
		return $this->belongsToMany('App\User','laboratorio_responsable','laboratorio_id','user_id');
	}

	public function analistas()
	{
		return $this->belongsToMany('App\User','analistas','laboratorio_id','user_id');
	}
}
