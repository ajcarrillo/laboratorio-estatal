<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/1/19
 * Time: 11:45 a. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    protected $table = 'estados_municipios_localidades';
}
