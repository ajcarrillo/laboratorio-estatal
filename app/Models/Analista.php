<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2/27/20
 * Time: 7:26 p. m.
 */

namespace App\Models;


use App\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Analista extends Model
{
    protected $table = 'analistas';

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function laboratorio()
    {
        return $this->belongsTo(Laboratorio::class, 'laboratorio_id');
    }

    public function scopeActivo($query)
    {
        return $query->where('activo', 1);
    }

    public function scopeLabSanitaria($query)
    {
        return $query->whereHas('laboratorio', function (Builder $query) {
            $query->where('tipo', 'SANITARIA');
        });
    }
}
