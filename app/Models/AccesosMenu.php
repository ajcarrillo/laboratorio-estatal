<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:03 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class AccesosMenu extends Model
{
    protected $table    = 'sistema_menus';
    protected $fillable = [
        'descripcion',
        'parent_id',
        'icon',
        'path',
        'orden',
        'sistema_id',
        'activo',
    ];
    protected $casts    = [
        'activo' => 'boolean',
    ];

    public function sistema()
    {
        return $this->belongsTo(Sistema::class, 'sistema_id');
    }

    public function childrenMenu()
    {
        return $this->hasMany(self::class, 'parent_id');
    }

    public function scopeParents($query)
    {
        $query->withCount('childrenMenu')
            ->whereNull('parent_id');
    }
}
