<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 2:11 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Domicilio extends Model
{
    protected $table    = 'domicilios';
    protected $fillable = [
        'direccion', 'codigo_postal', 'cve_ent',
        'nombre_entidad', 'cve_mun', 'nombre_municipio','addressable_type','addressable_id',
    ];

    public function addressable()
    {
        return $this->morphTo();
    }
}
