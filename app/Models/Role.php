<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 3/18/20
 * Time: 11:38 p. m.
 */

namespace App\Models;


class Role extends \Spatie\Permission\Models\Role
{
    protected $table = 'roles';

    public function menus()
    {
        return $this->belongsToMany(Menu::class, 'menu_role', 'role_id', 'menu_id');
    }
}
