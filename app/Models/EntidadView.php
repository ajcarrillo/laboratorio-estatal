<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/4/19
 * Time: 10:41 a. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class EntidadView extends Model
{
    protected $table = 'estados_view';
}
