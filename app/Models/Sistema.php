<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:03 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Sistema extends Model
{
    protected $table      = 'sistemas';
    protected $fillable   = [
        'titulo',
        'subtitulo',
        'route',
    ];
    public    $timestamps = false;

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_sistema', 'sistema_id', 'role_id');
    }

    public function menu()
    {
        return $this->hasMany(AccesosMenu::class, 'sistema_id')
            ->with('childrenMenu')
            ->whereNull('parent_id');
    }
}
