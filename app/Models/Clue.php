<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:03 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Clue extends Model
{
    protected $table    = 'clues';
}
