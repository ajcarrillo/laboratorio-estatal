<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:04 p. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Genero extends Model
{
    protected $table = 'generos';
    protected $primaryKey = 'descripcion';
    public $incrementing = false;
    protected $keyType = 'string';
}
