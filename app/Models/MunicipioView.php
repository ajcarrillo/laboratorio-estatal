<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 10/1/19
 * Time: 11:40 a. m.
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class MunicipioView extends Model
{
    protected $table = 'municipios_view';
}
