<?php

namespace App\Providers;

use App\Models\Paciente;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use RiesgosSanitarios\Models\MuestraSanitaria;
use RiesgosSanitarios\Models\MuestraSanitariaEstudio;
use RiesgosSanitarios\Models\Origen;
use RiesgosSanitarios\Observers\MuestraSanitariaEstudioObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Relation::morphMap([
            'origenes'                     => Origen::class,
            'pacientes'                    => Paciente::class,
            'muestras_sanitarias'          => MuestraSanitaria::class,
            'muestras_sanitarias_estudios' => MuestraSanitariaEstudio::class,
        ]);

        MuestraSanitariaEstudio::observe(MuestraSanitariaEstudioObserver::class);
    }
}
