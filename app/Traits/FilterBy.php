<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 1:59 p. m.
 */

namespace App\Traits;


use App\QueryFilter;

trait FilterBy
{
    public function scopeFilterBy($query, QueryFilter $filters, array $data)
    {
        return $filters->applyTo($query, $data);
    }
}
