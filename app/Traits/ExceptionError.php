<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 9/28/19
 * Time: 11:50 p. m.
 */

namespace App\Traits;


use Exception;
use Throwable;

trait ExceptionError
{
    public function throwableEx(Throwable $e)
    {
        return unprocessable_entity([
            'trace' => $e->getTraceAsString(),
            'error' => $e->getMessage(),
            'line'  => $e->getLine(),
            'file'  => $e->getFile(),
        ], $e->getMessage());
    }

    public function execption(Exception $e)
    {
        return unprocessable_entity([
            'trace' => $e->getTraceAsString(),
            'error' => $e->getMessage(),
            'line'  => $e->getLine(),
            'file'  => $e->getFile(),
        ], $e->getMessage());
    }
}
