const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    resolve: {
        alias: {
            '@': __dirname + '/resources/js'
        },
    },
});

mix.js('resources/js/app.js', 'public/js');

mix.copy('resources/img/logo.jpg', 'public/img')
    .copy('resources/img/Logo_Salud_transparente_HZ-jun19.png', 'public/img')
    .copy('resources/img/administracion.png', 'public/img')
    .copy('resources/img/epidemiologia.jpg', 'public/img')
    .copy('resources/img/inventarios.jpeg', 'public/img')
    .copy('resources/img/riesgos_sanitarios_1.jpg', 'public/img')
    .copy('resources/js/materialize.min.js', 'public/js')
    .copy('resources/css/materialize.min.css', 'public/css')
    .copy('resources/fonts/iconfont/', 'public/fonts/vendor/meterialicons');


if (mix.inProduction()) {
    mix.version();
} else {
    mix.browserSync({
        proxy: 'laboratorio-estatal.test',
        files: [
            'resources/**/*',
        ],
        browser: ['google chrome'],
        snippetOptions: {
            rule: {
                match: /<\/body>/i,
                fn: function (snippet, match) {
                    return snippet + match;
                }
            }
        }
    });
}
